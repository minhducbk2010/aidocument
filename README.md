# AI LEARNING[softmax,neuron network]

0. BoardCasting of Numpy 

[﻿BoardCasting](https://app.eraser.io/workspace/5c4WXQqJWDKO776lgy5O?elements=QL4DL5hmg0Gh_xG2JvjaUA) 

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/t8vtxagw85q4LVjWvLeYN.png?ixlib=js-3.7.0 "image.png")



Có thể hiểu ở đây là khi thực hiện tính toán thì numpy sẻ tự động dãng  (stretch ) một thành phần nhỏ sao cho nó có thể tương đồng 1 thành phần lớn 

ví dụ như từ vector sẻ chuyển thành matran

# Thuật toán tối ưu hóa 
Đây là cốt lỏi của các mô hình học sâu hiện đại. Với 1 bái toán cở bản nhất  

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/sIqVmm0cf54E3B4lEq3gz.png?ixlib=js-3.7.0 "image.png")

Nghĩa rằng có 1 hàm bật 1 biểu diển sự phân bố của data. Với lượng data có ta có thể dung các công thức tôi ưu hóa để tìm được bô beta0 và beta1 1 cách hợp lý nhất phương pháp đại diện có thể là SGD . Giải thích bằng lời ở đây có nghĩa rằng có tồn tại 1 bộ tham số mà khi so sánh nó với bộ tham số trước đó trong quá trình tối ưu thì thay đổi ít nhất có nghĩa rằng bộ tham số phù hợp nhất.

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/zUDNEwmhFTnF6gOGnP8ER.png?ixlib=js-3.7.0 "image.png")

Theo công thức thì rỏ ràng phần khó nhất ở đây là tìm gradient của hàm loss ở đây sau khi chứng minh công thức ta có 

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/PG1EkC_BbHGZ6n5swnrOi.png?ixlib=js-3.7.0 "image.png")

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/-yi1UTCXlkrWq_iesrKk3.png?ixlib=js-3.7.0 "image.png")

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/l_iOt_eCUTJUI1Ds0w3zT.png?ixlib=js-3.7.0 "image.png")

với việc áp dụng công thứ ta có thể tìm được bo beta0 va beta1 phù hợp nhất với data đầu vàvào.

# 1. Softmax
[﻿Figure 3](https://app.eraser.io/workspace/5c4WXQqJWDKO776lgy5O?elements=bMHO7M0A4bEmQSkadeME4Q) 

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/ukhVHnaLnFDSfqY04xD3U.png?ixlib=js-3.7.0 "image.png")



Mô tả mô hình như sau với X mà la trận data nhập vào còn ma trận θ (theta) ở đây có giá trị trung gian z là tích vô hương của ma trận X và ma trận transfer θ  sau đó đưa qua hàm sigmoid  



[﻿Diagram](https://app.eraser.io/workspace/5c4WXQqJWDKO776lgy5O?elements=3wUoYb209G9an1tIi9ldlQ) 

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/he1X81MOaxoQt9117irTO.png?ixlib=js-3.7.0 "image.png")



[﻿chat.openai.com/share/b2bf14e0-34d3-433a-82a0-52a1a647b7e5](https://chat.openai.com/share/b2bf14e0-34d3-433a-82a0-52a1a647b7e5) 

[﻿math summary](https://app.eraser.io/workspace/5c4WXQqJWDKO776lgy5O?elements=JgJTUugFyBmnsDx3JQM6fw) 



![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/8FXVLH_c7JySSyeubdlaq.png?ixlib=js-3.7.0 "image.png")



[﻿Figure 4](https://app.eraser.io/workspace/5c4WXQqJWDKO776lgy5O?elements=nqpbwfaMlOgi-DQCsOepmg) 

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/5xy3oULT-eHepIqg8pqn8.png?ixlib=js-3.7.0 "image.png")



# 2. Neron
Trong mạng nơ-ron nhân tạo, "feedforward" và "backpropagation" là hai quá trình quan trọng trong quá trình huấn luyện mô hình, giúp mô hình học và tối ưu hóa để dự đoán tốt hơn. Dưới đây là giải thích chi tiết về từng khái niệm:

### 1. Feedforward (Lan truyền xuôi)
Feedforward là quá trình đưa dữ liệu đầu vào qua các lớp của mạng nơ-ron để tạo ra đầu ra dự đoán.

- **Quá trình**: Dữ liệu đầu vào sẽ được đưa qua các lớp ẩn (nếu có) rồi tới lớp đầu ra, nơi mạng sẽ tạo ra một dự đoán.
- **Công thức**: Tại mỗi lớp, giá trị đầu vào được nhân với trọng số của từng nơ-ron trong lớp và cộng với một hệ số bias, sau đó đi qua hàm kích hoạt để tạo ra giá trị đầu ra.
- **Mục đích**: Feedforward cho phép mạng tính toán giá trị dự đoán dựa trên trọng số hiện tại. Kết quả từ feedforward sẽ được so sánh với đầu ra thực tế để tính toán sai số (loss).
### 2. Backpropagation (Lan truyền ngược)
Backpropagation là quá trình điều chỉnh trọng số của mạng dựa trên sai số tính toán được từ bước feedforward.

- **Quá trình**: Dựa trên sai số của đầu ra dự đoán so với đầu ra mong muốn, backpropagation sử dụng thuật toán gradient descent để điều chỉnh trọng số của các nơ-ron nhằm giảm sai số.
- **Công thức**: Sử dụng đạo hàm để tính gradient của hàm lỗi theo từng trọng số, sau đó cập nhật trọng số theo hướng ngược lại với gradient để giảm thiểu sai số.
- **Mục đích**: Quá trình này giúp mạng tối ưu hóa và điều chỉnh dần dần các trọng số để mô hình học chính xác hơn và giảm thiểu sai số tổng thể.
### 3. Tóm tắt quy trình chung:
1. **Bước 1**: Dữ liệu đầu vào được đưa qua mạng (feedforward) để dự đoán.
2. **Bước 2**: So sánh đầu ra dự đoán với đầu ra thực tế để tính sai số.
3. **Bước 3**: Lan truyền ngược sai số qua các lớp của mạng để điều chỉnh trọng số (backpropagation).
4. **Lặp lại**: Các bước trên được lặp lại nhiều lần qua nhiều epoch để mạng học chính xác hơn.
Trong huấn luyện AI, feedforward và backpropagation cùng giúp mô hình ngày càng chính xác và tối ưu hơn trong việc dự đoán.

## 1. Feed forward lan truyền thuận (Tính theo chìu thuận) 
Mỗi node (nút, hoặc neuron) thường bao gồm hai thành phần chính:

1. **Phần tuyến tính (Linear component)**: Đây là bước đầu tiên trong tính toán của mỗi node. Nó bao gồm phép nhân giữa các đầu vào (input) với các trọng số (weights) tương ứng và sau đó là một phép cộng với một giá trị bù (bias). Công thức cơ bản cho phần tuyến tính là:[
z = w . x + b
]trong đó:
    - ( x ) là vector đầu vào,
    - ( w ) là vector trọng số,
    - ( b ) là giá trị bù.
2. **Phần phi tuyến tính (Non-linear component)**: Sau khi có kết quả tuyến tính, giá trị này sẽ đi qua một hàm kích hoạt phi tuyến tính (activation function) để tạo ra đầu ra của node. Các hàm kích hoạt phổ biến bao gồm:Các hàm kích hoạt phi tuyến tính này giúp mạng nơ-ron có khả năng học được các mẫu phức tạp, vì nếu không có phần phi tuyến tính, mạng sẽ chỉ là một mô hình tuyến tính và không thể học được các quan hệ phi tuyến tính trong dữ liệu.
    - **ReLU** (Rectified Linear Unit): ( \text{ReLU}(z) = \max(0, z) )
    - **Sigmoid**: ( \text{Sigmoid}(z) = \frac{1}{1 + e^{-z}} )
    - **Tanh**: ( \text{Tanh}(z) = \frac{e^{z} - e^{-z}}{e^{z} + e^{-z}} )
Vì vậy, mỗi node thường sẽ có cả hai thành phần này để hoạt động đúng trong việc mô phỏng khả năng học hỏi từ dữ liệu.

[﻿Neuron](https://app.eraser.io/workspace/5c4WXQqJWDKO776lgy5O?elements=mFcw2h2SyLuJKFgxGSZO6w) 

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/P4qolFZ02pY3ThEEPuOFf.png?ixlib=js-3.7.0 "image.png")



Mô hình toán Học của mạng neuro

từ vector feature x cần tìm** vector z (activation vector)** thông qua ma trận **W (weight matrix) **và **vector bias b **

1. **Nhân Ma Trận:** Vector đặc trưng đầu vào được nhân với một ma trận trọng số. Ma trận này chứa các trọng số liên kết từ các nơ-ron trong lớp đầu vào tới các nơ-ron trong lớp ẩn. Giả sử vector đặc trưng đầu vào là **x** và ma trận trọng số là _W_, thì phép nhân này có thể được biểu diễn là **x**×_W_.
2. **Thêm Bias:** Sau khi thực hiện phép nhân ma trận, một vector bias (thường ký hiệu là **b**) thường được cộng vào kết quả. Vector bias này giúp điều chỉnh đầu ra của lớp mạng để có thể tốt hơn biểu diễn dữ liệu đầu vào. Công thức sau khi thêm bias sẽ là **x**×_W_+**b**.
3. **Áp dụng Hàm Kích Hoạt:** Cuối cùng, một hàm kích hoạt (activation function) được áp dụng lên kết quả trên. Hàm kích hoạt có thể là hàm ReLU, sigmoid, tanh, v.v. Hàm này giúp thêm tính phi tuyến vào mô hình, cho phép mạng nơ-ron có khả năng học được các mối quan hệ phức tạp hơn. Vector _z_ sau khi áp dụng hàm kích hoạt sẽ là _f_(**x**×_W_+**b**), với _f_ là hàm kích hoạt.
## 2. Thuật toán lan truyền ngược Backpropagation 
Mục tiêu ở đây là tương tự nhưng bài toán toán khác ta cần tính được sự thay đổi của loss function dựa trên tham số hiện tại (ma trận W) vì vậy điều cần thiết ở đây chính là tìm được gradien của hàm mất mát J với w mới được đưa vào 

[﻿Mô hình hóa tham số trong giải thuật thuận neuron](https://app.eraser.io/workspace/5c4WXQqJWDKO776lgy5O?elements=iG5agp8qbGBTw_OHw3KCkw) 

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/7DsG159LQ356YwiokdXJM.png?ixlib=js-3.7.0 "image.png")



Công thức tổng quát cuối cùng 

[﻿Công thức tổng quát về lan truyền ngược](https://app.eraser.io/workspace/5c4WXQqJWDKO776lgy5O?elements=6Q1t-dtkOXjRye-OkAektA) 

[![E_Cal](https://app.eraser.io/workspace/5c4WXQqJWDKO776lgy5O/preview?elements=QjteIEI_1LoKU3eP8jty3w&type=embed)](https://app.eraser.io/workspace/5c4WXQqJWDKO776lgy5O?elements=QjteIEI_1LoKU3eP8jty3w)

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/jiTTT5yPWD1kVIIGdfMLF.png?ixlib=js-3.7.0 "image.png")



Công thức tính giá trị trung gian E trên mỗi hidden layor 

hàm mất mát 

[﻿Figure 5](https://app.eraser.io/workspace/5c4WXQqJWDKO776lgy5O?elements=vCpb_dIJ7HuSYJRtVMMo7g) 

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/qTTGzxIpGQd1-g1ydwl-6.png?ixlib=js-3.7.0 "image.png")



[﻿Backward](https://app.eraser.io/workspace/5c4WXQqJWDKO776lgy5O?elements=DzW_l4H-bVRvWzlqjvEfnQ) 

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/fmkJyZBRnmIz7QQQWCB0Q.png?ixlib=js-3.7.0 "image.png")



---

# 3. Mạng tích chập - Convolutional neural network
## 1. Vấn đề cơ bản 
Vấn đề nếu chỉ xứ lỷ bằng cách duổi 1 ảnh thành 1 vector thì sẻ gặp vấn đề là nếu bức ảnh lớn hơn vật thể thì ko nhận định được mà sẻ nhận định thành 1 ảnh khác. giải pháp là mạng tích chập Convolutional neural network 

## 2. Cấu trúc Convolutionnal 
Quy trình tính 1 lớp Tích chập gồm 3 bước 



[﻿protonx.coursemind.io/courses/64ec4a0dbc3ca0001a11d6d7/topics/6593fa31ed23150012aad5b4?activeAId=6593fa32ed23150012aad5e0](https://protonx.coursemind.io/courses/64ec4a0dbc3ca0001a11d6d7/topics/6593fa31ed23150012aad5b4?activeAId=6593fa32ed23150012aad5e0) 

[﻿Quy trình tính lớp Convolutionail](https://app.eraser.io/workspace/5c4WXQqJWDKO776lgy5O?elements=4fONDRSUU9E-QSt_A8wtHg) 



![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/yv7VY1HaOtCf6DXtnrNk8.png?ixlib=js-3.7.0 "image.png")



- Một mạng tích chập (Convolutional Neural Network - CNN) thường bao gồm các thành phần chính theo cấu trúc như sau:
    1. **Affine Transform (Convolutional Layer)**: Đây là bước biến đổi tuyến tính, thực hiện qua phép tích chập (convolution) bằng các bộ lọc (filter hoặc kernel) lên ảnh đầu vào. Các bộ lọc này giúp trích xuất các đặc trưng cục bộ từ ảnh, như cạnh, góc, hoặc các kết cấu khác. Mỗi bộ lọc sẽ dịch chuyển qua ảnh và thực hiện phép nhân từng phần tử, rồi cộng dồn để tạo ra một đầu ra đặc trưng mới.
đặt điểm của Convolutional là đặt tính Equivariance translation 
        1. Equivariance thường được sử dụng trong các mô hình mạng nơ-ron tích chập (CNN) khi làm việc với các phép biến đổi như dịch chuyển, quay, hoặc phản chiếu của ảnh. Mô hình **equivariant** với một phép biến đổi sẽ duy trì cấu trúc không gian của đầu vào sau biến đổi, tức là nếu đầu vào biến đổi, đầu ra cũng sẽ biến đổi tương ứng. Ví dụ:Điều này rất hữu ích vì mô hình có thể nhận diện được các đặc trưng bất kể sự dịch chuyển, mà không cần phải học lại từ đầu.
        2. Trong mạng CNN, phép tích chập là equivariant với phép dịch chuyển (translational equivariance): nếu dịch ảnh đầu vào, đầu ra của tích chập cũng dịch tương ứng.
    2. **Detector Stage (Activation Layer)**: Sau khi áp dụng convolution, ta thường áp dụng một hàm kích hoạt (activation function) để thêm tính phi tuyến tính vào mô hình, giúp mạng học được các đặc trưng phức tạp hơn. Hàm kích hoạt phổ biến nhất trong CNN là ReLU (Rectified Linear Unit), giúp chuyển các giá trị âm thành 0, tạo nên tính phi tuyến cho mô hình. -> **vì thực tế việc nhận định nó ko phải mang tính tuyến tính mà mang tính tính lủy có thể hình dung rằng với đầu vào là analog ta muốn nó trả về 1, 0 biến đổi này chính là biến đối phi tuyến** 
    3. **Pooling Layer (Lớp Pool)**: Đây là bước giảm kích thước của output từ lớp Detector, giúp giảm số lượng tham số và tăng tính bất biến của mạng với các thay đổi nhỏ của đối tượng trong ảnh. Lớp Pool không có tham số học, mà chỉ thực hiện biến đổi cố định trên đầu ra của lớp trước đó. Max Pooling là một ví dụ phổ biến, giúp giữ lại giá trị lớn nhất trong một vùng và giảm độ phân giải của ảnh, qua đó tăng khả năng nhận diện các vật thể ở các vị trí khác nhau.




Dưới đây là ví dụ về cách khai báo một lớp tích chập đơn giản (convolutional layer) sử dụng TensorFlow và PyTorch. Ví dụ sẽ bao gồm ba lớp chính: **Convolutional Layer**, **Activation Layer** (ReLU), và **Pooling Layer**.

### 1. Sử dụng TensorFlow (Keras)
```python
pythonCopy codeimport tensorflow as tf
from tensorflow.keras import layers, models

# Khởi tạo mô hình Sequential
model = models.Sequential()

# Thêm lớp tích chập (Convolutional Layer)
model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=(64, 64, 3)))  # 32 filters, kernel size 3x3, input size 64x64 với 3 kênh (RGB)

# Thêm lớp pooling (Pooling Layer)
model.add(layers.MaxPooling2D((2, 2)))  # Max pooling với kích thước 2x2

# Thêm một vài lớp tích chập và pooling nữa
model.add(layers.Conv2D(64, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))

model.add(layers.Conv2D(128, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))

# Hiển thị cấu trúc mô hình
model.summary()
```
Trong ví dụ trên:

- `**Conv2D**`  là lớp tích chập, áp dụng phép tích chập với 32 filters và kích thước kernel là 3x3.
- `**activation='relu'**`  là hàm kích hoạt phi tuyến (ReLU).
- `**MaxPooling2D**`  là lớp pooling để giảm kích thước không gian.
### 2. Sử dụng PyTorch
```python
pythonCopy codeimport torch
import torch.nn as nn
import torch.nn.functional as F

# Định nghĩa kiến trúc mạng
class SimpleCNN(nn.Module):
    def __init__(self):
        super(SimpleCNN, self).__init__()
        # Lớp tích chập đầu tiên
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=32, kernel_size=3)  # Input RGB image (3 channels), 32 filters, kernel size 3x3
        # Lớp tích chập thứ hai
        self.conv2 = nn.Conv2d(32, 64, 3)
        # Lớp tích chập thứ ba
        self.conv3 = nn.Conv2d(64, 128, 3)
        # Lớp pooling
        self.pool = nn.MaxPool2d(2, 2)  # Max pooling với kernel size 2x2

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))  # Conv1 + ReLU + Max Pooling
        x = self.pool(F.relu(self.conv2(x)))  # Conv2 + ReLU + Max Pooling
        x = self.pool(F.relu(self.conv3(x)))  # Conv3 + ReLU + Max Pooling
        return x

# Khởi tạo mô hình
model = SimpleCNN()

# Kiểm tra kiến trúc mô hình
print(model)
```
[﻿Pooling](https://app.eraser.io/workspace/5c4WXQqJWDKO776lgy5O?elements=cU0i-HL4_B3-bZcyX1seHg) 

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/FPXxdvV7L6ynWgDKbdEAk.png?ixlib=js-3.7.0 "image.png")



Mô tả việc thay đổi chiều qua mạng tích chập như sau 

 Đoạn mã này mô tả một mô hình mạng neuron tích chập (Convolutional Neural Network - CNN) sử dụng Keras trong TensorFlow.

```python
model = Sequential( [Conv2D(8, (3, 3), activation='relu', input_shape=(150, 150, 3)), MaxPooling2D(2, 2), 
Conv2D(16, (3, 3), activation='relu'), MaxPooling2D(2, 2), Conv2D(32, (3, 3), 
activation='relu'), 
MaxPooling2D(2, 2), ] )
```
1. **Input Layer**:
    - **Input Shape**: (150, 150, 3)
    - Đây là hình ảnh đầu vào với kích thước 150x150 pixels và 3 kênh màu (RGB).
2. **Conv2D Layer (8 filters, kernel size 3x3)**:
    - **Output Channels**: 8 (do sử dụng 8 bộ lọc)
    - **Output Dimensions**: Mỗi bộ lọc có kích thước 3x3, vì vậy kích thước đầu ra sẽ giảm xuống một chút do tính chập, nhưng không đáng kể nếu không sử dụng padding. Nếu sử dụng padding 'same', kích thước sẽ giữ nguyên là 150x150.
    - **Output Shape**: (148, 148, 8) hoặc (150, 150, 8) tùy thuộc vào padding.
3. **MaxPooling2D Layer (pool size 2x2)**:
    - **Output Dimensions**: Kích thước sẽ giảm đi một nửa do pooling 2x2.
    - **Output Shape**: (74, 74, 8) hoặc (75, 75, 8) tùy thuộc vào cách xử lý biên.
4. **Conv2D Layer (16 filters, kernel size 3x3)**:
    - **Output Channels**: 16
    - **Output Dimensions**: Tương tự như trên, kích thước sẽ giảm xuống một chút hoặc giữ nguyên tùy thuộc vào padding.
    - **Output Shape**: (72, 72, 16) hoặc (74, 74, 16) tùy thuộc vào padding.
5. **MaxPooling2D Layer (pool size 2x2)**:
    - **Output Dimensions**: Kích thước giảm một nửa.
    - **Output Shape**: (36, 36, 16) hoặc (37, 37, 16).
6. **Conv2D Layer (32 filters, kernel size 3x3)**:
    - **Output Channels**: 32
    - **Output Dimensions**: Tương tự như trên.
    - **Output Shape**: (34, 34, 32) hoặc (36, 36, 32).
7. **MaxPooling2D Layer (pool size 2x2)**:
    - **Output Dimensions**: Kích thước giảm một nửa.
    - **Output Shape**: (17, 17, 32) hoặc (18, 18, 32).


[﻿Neural Network Architecture](https://app.eraser.io/workspace/5c4WXQqJWDKO776lgy5O?elements=le1bXRoqodTMNfnD2r7-mw) 

[![Chi tiết](https://app.eraser.io/workspace/5c4WXQqJWDKO776lgy5O/preview?elements=_EWQgq6qvvWhPmSDkyQ37g&type=embed)](https://app.eraser.io/workspace/5c4WXQqJWDKO776lgy5O?elements=_EWQgq6qvvWhPmSDkyQ37g)

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/DqNPEq0s6dImrj5T1ZxLc.png?ixlib=js-3.7.0 "image.png")



# 4. Inception Module
[﻿Inception Module](https://app.eraser.io/workspace/5c4WXQqJWDKO776lgy5O?elements=ysBs6Oa1XACFKf3lneZ5mw) 

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/iwkf2w-1_wIL0vlUoZVS-.png?ixlib=js-3.7.0 "image.png")



# 5. ResNet
[﻿Resnet](https://app.eraser.io/workspace/5c4WXQqJWDKO776lgy5O?elements=xE-A7oNdm0Z_EJ5UuyhBAA) 

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/-oNK8o3DziQeO7-LTlhkx.png?ixlib=js-3.7.0 "image.png")



# 6. Convolution 1x 1 
[﻿Conv 1x1](https://app.eraser.io/workspace/5c4WXQqJWDKO776lgy5O?elements=bIwi1_aEp213vsQc-IM1Sg) 



![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/UiZBQWfexEcn4X1SX9h0K.png?ixlib=js-3.7.0 "image.png")



---

# 7. Mô hình ngôn ngữ 
Nói nôm na thì nó sẻ hiểu được ngôn ngữ theo thứ tự của các từ đưa vào 

Nhưng mô hình phổ biến và nỗi tiếng RNN , LSTM , Tranformer 

RNN hiện tại có vẻ đã lỗi thời nhưng bài toán của nó rất đáng để học hỏi 

LSTM thì nhấn mạnh về việc dự đoán theo từ tự của câu và không cần label 

[﻿Overview](https://app.eraser.io/workspace/5c4WXQqJWDKO776lgy5O?elements=V3A6MwSNw5-9h29YJf1RkA) 

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/1pr5ILtLwl7Trh6UWOVE7.png?ixlib=js-3.7.0 "image.png")



## 1. Embedding 
Ở đây mục tiêu là để convert tokent thành dạng vector ở đay trong quá trình lịch sử thì có nhiều phương pháp như **Skip-gram ,CBOW (**Continuous Bag of Words**) , **mục tiêu ở đây là với mỗi tokent sẻ được xác định 1 cách chính xác trong toàn bộ vẳn bản.

### 1. CBOW 
Mô hình này sẻ giả định vector nhúng embedding matrix tồn tài để có thể encode được từng tokent (ở dạng vector one hot ) với tokent khác trong câu. khi đo  nhân vector one hot đại diện cho tolent đó với embedding vector thì sẻ ra được vector embedding đại diện cho tokent đó. và theo lý thuyết thì có sẻ decode ngược lại ra được vector onehot ban đầu ở đây phân decode sẻ dùng dence với sofmax để và tiến thành train  để tìm W2 

[﻿www.kdnuggets.com/2018/04/implementing-deep-learning-methods-feature-engineering-text-data-cbow.html](https://www.kdnuggets.com/2018/04/implementing-deep-learning-methods-feature-engineering-text-data-cbow.html) 

[﻿CBOW Model](https://app.eraser.io/workspace/5c4WXQqJWDKO776lgy5O?elements=-TKBLycSs6F0nXfmo7MUAg) 

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/vjWoqqJDlIDBHWSU8p471.png?ixlib=js-3.7.0 "image.png")



# 2. Mô hình ngôn ngữ 
Với vector embedding vừa tìm được từ phương pháp CBOW hoặc các phương pháp khác ta đã có thể đưa nó vào mô hình để thực hiện các mục tiêu khác nhau. 

## 1. RNN,LSTM ,GRU
### RNN 
[﻿Docs](https://drive.google.com/file/d/1xPbLXHJHTvSSPLAf7sKSIw68vhVjajVs/view) 

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/XwWDcwRSfTJkGjWAG9S2K.png?ixlib=js-3.7.0 "image.png")

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/80pfsyReYEXyUZz76mJou.png?ixlib=js-3.7.0 "image.png")

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/RyvhA7x0gWyk2hhH3VJe3.png?ixlib=js-3.7.0 "image.png")

![image.png](https://eraser.imgix.net/workspaces/5c4WXQqJWDKO776lgy5O/52hcnUlxcQgcrIUNXxUQ2fbBZD32/NvJzLIiTg_ZfXVoHBq8rZ.png?ixlib=js-3.7.0 "image.png")



ở đây mô hình RNN sẻ bao gốm vector h và ma trận W gồm 3 thành W(s), W(hh),W(hx) mối W sẻ đại diện cho 1 ý nghĩa sau 

** **W(hx)  **Trọng số giữa lớp đầu vào và lớp ẩn (Input-to-Hidden Weights)**:

- Đây là tập hợp trọng số kết nối giữa đầu vào xt​ tại thời điểm t và lớp ẩn ht​.
- Các trọng số này sẽ xác định cách đầu vào hiện tại ảnh hưởng đến trạng thái của lớp ẩn.
W(hh)  **Trọng số giữa lớp ẩn và lớp ẩn (Hidden-to-Hidden Weights)**:

- Những trọng số này kết nối trạng thái ẩn ht−1​ từ thời điểm trước đó với trạng thái ẩn hiện tại ht​.
- Các trọng số này giúp mô hình giữ lại thông tin từ các bước trước đó trong chuỗi, cho phép mô hình ghi nhớ và dự đoán các giá trị trong tương lai.
W(s) **Trọng số giữa lớp ẩn và lớp đầu ra (Hidden-to-Output Weights)**:

- Đây là trọng số giữa trạng thái ẩn ht​ và lớp đầu ra yt​.
- Các trọng số này giúp điều chỉnh cách thông tin từ lớp ẩn chuyển đổi thành đầu ra cuối cùng.


Dưới đây là nội dung về mô hình Seq2Seq và Attention bằng tiếng Việt, bao gồm mô hình toán học và sơ đồ minh họa.

## Mô hình Seq2Seq (Sequence to Sequence)

Mô hình **Seq2Seq** thường được sử dụng trong các bài toán như dịch máy hoặc tóm tắt văn bản, nơi chuỗi đầu vào được chuyển thành chuỗi đầu ra. Các thành phần chính của mô hình này bao gồm:

- **Cấu trúc Encoder-Decoder (Bộ mã hóa - Bộ giải mã)**:
    - **Encoder (Bộ mã hóa)**: Bộ mã hóa tiếp nhận chuỗi đầu vào và nén nó thành một vector ngữ cảnh (context vector), hay còn gọi là biểu diễn tóm tắt của chuỗi đầu vào.
    - **Decoder (Bộ giải mã)**: Từ vector ngữ cảnh này, bộ giải mã tạo ra chuỗi đầu ra, tức là dự đoán từng phần tử của chuỗi.

- **Cơ chế Attention (Tập trung)**:
    - Attention trong mô hình Seq2Seq cho phép bộ giải mã tập trung vào các phần khác nhau của chuỗi đầu vào tại mỗi bước thời gian. Điều này giúp cải thiện độ chính xác, đặc biệt với các chuỗi dài.

## 2. Cơ chế Attention

Cơ chế Attention đã cách mạng hóa xử lý ngôn ngữ tự nhiên (NLP) bằng cách cho phép mô hình tập trung vào các phần liên quan của chuỗi đầu vào khi tạo ra mỗi phần tử của chuỗi đầu ra.

- **Attention Toàn cục (Global Attention)**: Sử dụng toàn bộ các đầu ra của bộ mã hóa để tạo ra đầu ra cuối cùng, thông qua một kết hợp trọng số. Mô hình học được việc nhấn mạnh phần nào của chuỗi đầu vào cần được tập trung hơn.
- **Self-Attention (Tự Tập trung)**: Quan trọng trong mô hình **Transformer**, nó tính toán sự chú ý trong chuỗi, cho phép mô hình nắm bắt được các phụ thuộc giữa các phần tử xa nhau trong chuỗi một cách hiệu quả.

## 3.Mô hình Toán học cho Seq2Seq và Attention

1. **Biểu diễn Encoder-Decoder**:
   - **Encoder**: Biểu diễn chuỗi đầu vào $\{x_1, x_2, \dots, x_T\}$ thành vector ngữ cảnh $C$.
   - **Decoder**: Tạo ra chuỗi đầu ra $\{y_1, y_2, \dots, y_{T'}\}$ từ vector ngữ cảnh $C$ và các đầu ra trước đó.

2. **Cơ chế Attention**:
   - **Tính Điểm Chú Ý (Attention Score)**: Điểm chú ý giữa trạng thái của bộ giải mã $s_t$ và đầu ra của bộ mã hóa $h_i$ được tính bằng:
     \[
     score(s_t, h_i) = s_t^T W_a h_i
     \]
   - **Hệ Số Chú Ý (Attention Weights)**: Sử dụng softmax để chuẩn hóa các điểm chú ý thành trọng số:
     \[
     \alpha_{t,i} = \frac{\exp(score(s_t, h_i))}{\sum_{j=1}^{T} \exp(score(s_t, h_j))}
     \]
   - **Vector Ngữ Cảnh Có Trọng Số (Weighted Context Vector)**: Tính toán vector ngữ cảnh có trọng số từ các hệ số chú ý:
     \[
     c_t = \sum_{i=1}^{T} \alpha_{t,i} h_i
     \]
   - **Tạo Đầu Ra**: Dùng vector ngữ cảnh $c_t$ cùng với trạng thái của bộ giải mã $s_t$ để tạo ra đầu ra $y_t$:
     \[
     y_t = g(s_t, c_t)
     \]
## 3. Muti head Attention 
Mô hình Multi-Head Attention (MHA) là một trong những thành phần quan trọng của Transformer, được sử dụng rộng rãi trong các mô hình học sâu hiện đại như BERT và GPT. Multi-Head Attention giúp mô hình hiểu các quan hệ phức tạp giữa các từ trong câu thông qua cơ chế chú ý.

### Thành phần chính của Multi-Head Attention:
1. **Scaled Dot-Product Attention**:
   - MHA dựa trên cơ chế chú ý Dot-Product, trong đó mỗi từ trong câu được biến đổi thành ba ma trận:
     - **Query (Q)**: Đại diện cho từ đang cần "tìm kiếm".
     - **Key (K)**: Đại diện cho tất cả các từ mà từ đang tìm kiếm có thể liên kết tới.
     - **Value (V)**: Đại diện cho thông tin tương ứng với các từ.
   - Công thức chú ý Dot-Product:
     \[
     \text{Attention}(Q, K, V) = \text{softmax}\left(\frac{QK^T}{\sqrt{d_k}}\right) V
     \]
     Trong đó, \( d_k \) là kích thước của vector Key, và phần chia căn bậc hai nhằm giảm thiểu việc giá trị quá lớn, làm cho softmax trở nên khó kiểm soát.

2. **Multi-Head Attention**:
   - **Multi-Head Attention** kết hợp nhiều "đầu chú ý" (attention heads) để mô hình có thể học các mối quan hệ từ khác nhau từ các khía cạnh khác nhau. Điều này giúp cải thiện khả năng chú ý của mô hình đối với các từ trong ngữ cảnh cụ thể.
   - Để xây dựng Multi-Head Attention, mô hình chia các vector \(Q\), \(K\), và \(V\) thành \(h\) phần khác nhau, và áp dụng chú ý riêng cho từng phần này. Sau đó, các kết quả của từng đầu chú ý được kết hợp và chiếu ngược trở lại thành một không gian vector duy nhất:
     \[
     \text{MultiHead}(Q, K, V) = \text{Concat}(\text{head}_1, \text{head}_2, \ldots, \text{head}_h) W^O
     \]
     - Mỗi đầu chú ý (\(\text{head}_i\)) được tính bằng cách sử dụng phiên bản khác nhau của \(W^Q_i\), \(W^K_i\), và \(W^V_i\).
     - Sau khi kết hợp các đầu chú ý, mô hình áp dụng ma trận trọng số \(W^O\) để biến đổi kết quả cuối cùng.

### Lợi ích của Multi-Head Attention:
- **Khám phá các mối quan hệ khác nhau**: Các đầu chú ý khác nhau giúp mô hình phát hiện nhiều mối quan hệ khác nhau giữa các từ, bao gồm các liên kết ngữ pháp và ngữ nghĩa khác nhau.
- **Học ngữ cảnh phong phú hơn**: Việc sử dụng nhiều đầu chú ý giúp mô hình hiểu rõ hơn ngữ cảnh của từ trong câu, đặc biệt khi câu có cấu trúc phức tạp hoặc dài.
  
### Ví dụ cụ thể:
- Khi bạn có một câu như "The cat sat on the mat", mô hình với Multi-Head Attention có thể có một đầu chú ý để hiểu rằng "the cat" là chủ ngữ, một đầu khác để hiểu quan hệ giữa "sat" và "on", và một đầu khác để liên kết từ "mat" với hành động "sat on".

Multi-Head Attention là một trong những yếu tố giúp Transformer trở nên mạnh mẽ và hiệu quả hơn so với các mô hình tuần tự truyền thống như RNN, vì nó cho phép mô hình xử lý mọi từ trong câu một cách song song, thay vì tuần tự. Điều này cũng giúp mô hình tận dụng tốt hơn các khả năng tính toán của GPU.