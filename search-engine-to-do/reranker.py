from underthesea import word_tokenize
from transformers import AutoTokenizer, AutoModel
import torch
from datasets import Dataset
import os
import rawSearch
# Set environment variable to allow duplicate OpenMP runtime
os.environ['KMP_DUPLICATE_LIB_OK'] = 'TRUE'

# Import underthesea for word segmentation

# Load model and tokenizer from PhoBERT
model = AutoModel.from_pretrained('vinai/phobert-base')
tokenizer = AutoTokenizer.from_pretrained('vinai/phobert-base')

# Ensure special tokens are set correctly
tokenizer.bos_token = '<s>'
tokenizer.eos_token = '</s>'
tokenizer.sep_token = '</s>'
tokenizer.cls_token = '<s>'
tokenizer.unk_token = '<unk>'
tokenizer.pad_token = '<pad>'
tokenizer.mask_token = '<mask>'

# Resize model embeddings to match tokenizer
model.resize_token_embeddings(len(tokenizer))

# Function to get embedding of a text segment


def get_embedding(item):
    # Perform word segmentation
    item = word_tokenize(item, format='text')

    # Tokenize the text
    inputs = tokenizer(
        item,
        return_tensors='pt',
        max_length=256,
        truncation=True,
        padding="max_length",
        return_token_type_ids=False  # PhoBERT does not use token_type_ids
    )

    input_ids = inputs['input_ids']
    attention_mask = inputs['attention_mask']

    # Check for out-of-range token IDs
    if input_ids.max() >= model.config.vocab_size:
        raise ValueError("Token IDs exceed vocabulary size.")

    # Get embeddings from the model
    with torch.no_grad():
        outputs = model(
            input_ids=input_ids,
            attention_mask=attention_mask
        )

    # Use mean pooling to get the embedding
    embedding = outputs.last_hidden_state.mean(dim=1).squeeze()
    return embedding


class ReRanker:
    def __init__(self):
        pass

    def rank(self, query, docs):
        # Get embedding for the query
        query_embedding = get_embedding(query)

        # Get document texts from docs (ignore TF-IDF scores)
        doc_texts = [doc[1] for doc in docs]  # doc[1] is the document content

        # Get embeddings for each document
        doc_embeddings = [get_embedding(doc_text) for doc_text in doc_texts]

        # Compute cosine similarity between query and document embeddings
        scores = [torch.nn.functional.cosine_similarity(query_embedding, doc_emb, dim=0).item()
                  for doc_emb in doc_embeddings]

        # Combine scores with docs and sort in descending order
        ranked_docs = sorted(zip(scores, doc_texts),
                             key=lambda x: x[0], reverse=True)

        # Return the ranked list of documents
        return [{"doc": doc, "score": score} for score, doc in ranked_docs]


if __name__ == '__main__':
    # Example usage of ReRanker
    # Assume `docs` is the output of `search` in TF-IDF containing documents and their TF-IDF scores

    reranker = ReRanker()
    query = "hà  nội"
    # Initialize TFIDF class with paths to JSON files
    tfidf = rawSearch.TFIDF('tf_idf_list.json', 'docs.json', 'ds.json')

    # Perform search
    tfidf_output = tfidf.search(query, 10)
    ranked_results = reranker.rank(query, tfidf_output)

    for result in ranked_results:
        print(f"Document: {result['doc']}, Score: {result['score']}")
