from flask import Flask, request, jsonify
from reranker import ReRanker
from rawSearch import TFIDF
import os

app = Flask(__name__)

# Khởi tạo các lớp TFIDF và ReRanker
tf_idf = TFIDF('tf_idf_list.json',
               'docs.json', 'ds.json')
re_ranker = ReRanker()


@app.route('/api/search', methods=['POST'])
def handle_query():
    data = request.get_json()
    query = data.get('query')

    # Kiểm tra xem truy vấn có tồn tại không
    if not query:
        return jsonify({'error': 'No query provided'}), 400

    # Xử lý truy vấn (có thể cải thiện bằng cách loại bỏ ký tự không cần thiết)
    clean_query = process_query(query)

    # Bước 1: Tìm kiếm bằng TF-IDF
    filtered_results = tf_idf.search(clean_query, 10)

    # Bước 2: Re-rank kết quả bằng AI (PhoBERT)
    scores = re_ranker.rank(query, filtered_results)

    # Trả về danh sách tài liệu sau khi xếp hạng
    return jsonify({'response': scores})


def process_query(query):
    # Chuyển đổi hoặc làm sạch truy vấn, nếu cần
    return query.strip()


if __name__ == '__main__':
    # Đặt biến môi trường để tránh lỗi OpenMP
    os.environ['KMP_DUPLICATE_LIB_OK'] = 'TRUE'
    app.run(debug=True)
