import json
import math
from collections import defaultdict
from typing import List, Tuple


class TFIDF:
    def __init__(self, tfidf_path: str, doc_path: str, ds_path: str):
        # Load TF-IDF data, document list, and ds from JSON files
        with open(tfidf_path, 'r', encoding='utf-8') as f:
            self.tf_idf_list = json.load(f)

        with open(doc_path, 'r', encoding='utf-8') as f:
            self.documents = json.load(f).get("docs", [])

        with open(ds_path, 'r', encoding='utf-8') as f:
            self.ds = json.load(f)

    def search(self, query: str, k: int = 10) -> List[Tuple[float, str]]:
        """
        Perform a TF-IDF search for the query and return top k documents along with their scores.

        Returns:
            A list of tuples: (score, document_content)
        """
        # Convert query to lowercase and split into terms
        query_terms = query.lower().split()

        # Calculate cosine similarity score for each document
        finals = []
        for doc_id in range(len(self.documents)):
            score = 0.0
            for term in query_terms:
                # Calculate score if term is in tf_idf_list and ds
                if term in self.tf_idf_list and str(doc_id) in self.tf_idf_list[term]:
                    tf_idf = self.tf_idf_list[term][str(doc_id)]
                    d = self.ds[str(doc_id)]
                    score += tf_idf / d
            # Only include documents with a non-zero score
            if score > 0:
                finals.append((score, self.documents[doc_id]))

        # Sort and return top k documents with highest scores
        sorted_results = sorted(finals, key=lambda x: -x[0])[:k]
        return sorted_results


if __name__ == '__main__':
    # Initialize TFIDF class with paths to JSON files
    tfidf = TFIDF('tf_idf_list.json', 'docs.json', 'ds.json')

    # Perform search
    top_docs = tfidf.search("Hà Nội", 10)
    for score, doc, doc_id in top_docs:
        print(
            f"Document ID: {doc_id}, Score: {score}\nDocument Content: {doc}\n")
