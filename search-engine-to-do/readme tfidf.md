### Lý thuyết về TF-IDF

**TF-IDF** (Term Frequency-Inverse Document Frequency) là một trọng số được sử dụng trong xử lý ngôn ngữ tự nhiên và khai thác thông tin để đánh giá mức độ quan trọng của một từ trong tài liệu so với toàn bộ tập hợp tài liệu. Phương pháp này kết hợp hai yếu tố:

1. **TF (Term Frequency)** - Tần số của một từ trong tài liệu:
   - Đo lường tần suất xuất hiện của từ trong một tài liệu. Công thức phổ biến để tính TF:
     \[
     TF = \log(1 + f_{t,d})
     \]
   - Trong đó \(f_{t,d}\) là số lần từ \(t\) xuất hiện trong tài liệu \(d\).

2. **IDF (Inverse Document Frequency)** - Tần số nghịch đảo của tài liệu chứa từ đó:
   - Đo lường mức độ phổ biến của từ trong toàn bộ tập hợp tài liệu. Nếu từ xuất hiện trong nhiều tài liệu, IDF của từ đó sẽ thấp và ngược lại. Công thức tính IDF:
     \[
     IDF = \log\left(\frac{N}{df_t}\right)
     \]
   - Trong đó \(N\) là tổng số tài liệu và \(df_t\) là số lượng tài liệu chứa từ \(t\).

3. **TF-IDF Score**:
   - TF-IDF score là tích của TF và IDF:
     \[
     TF\text{-}IDF = TF \times IDF
     \]
   - Trọng số này giúp lọc ra những từ quan trọng (tần suất cao trong tài liệu và xuất hiện ít trong tài liệu khác) để tăng độ chính xác của việc tìm kiếm và phân tích văn bản.

### Cách tính TF-IDF và độ tương đồng cosine trong `TFIDF` class

#### Class `TFIDF`
Class `TFIDF` trong ví dụ này hoạt động theo các bước sau:

1. **Chuẩn bị dữ liệu**:
   - Dữ liệu bao gồm ba file: 
     - `tf_idf_list.json`: chứa điểm TF-IDF cho từng từ trong mỗi tài liệu.
     - `docs.json`: chứa nội dung của các tài liệu.
     - `ds.json`: chứa norm (chuẩn hóa) cho mỗi tài liệu, là căn bậc hai của tổng bình phương các giá trị TF-IDF trong tài liệu.

2. **Tính toán cosine similarity**:
   - Khi nhận một truy vấn, `TFIDF` sẽ tính độ tương đồng cosine giữa truy vấn và từng tài liệu. Độ tương đồng cosine giữa truy vấn và tài liệu được tính bằng công thức:
     \[
     \text{cosine\_similarity} = \frac{\sum (\text{TF-IDF của truy vấn và tài liệu})}{\|q\| \times \|d\|}
     \]
   - Tại đây, `ds[doc_id]` là norm (độ dài) của vector TF-IDF của tài liệu, giúp chuẩn hóa và so sánh điểm số giữa các tài liệu.

3. **Hàm `search`**:
   - Khi truy vấn được đưa vào, `search` sẽ:
     - Tính toán điểm cho mỗi tài liệu bằng cách lấy tổng điểm TF-IDF của từ trong truy vấn.
     - Sắp xếp các tài liệu theo điểm và trả về top `k` tài liệu có điểm cao nhất.

#### Mã của `TFIDF` class

Dưới đây là mã Python cho class `TFIDF`:

```python
import json
import math
from collections import defaultdict
from typing import List, Tuple

class TFIDF:
    def __init__(self, tfidf_path: str, doc_path: str, ds_path: str):
        # Tải dữ liệu TF-IDF, danh sách tài liệu và ds từ các file JSON
        with open(tfidf_path, 'r', encoding='utf-8') as f:
            self.tf_idf_list = json.load(f)
        
        with open(doc_path, 'r', encoding='utf-8') as f:
            self.documents = json.load(f).get("docs", [])
        
        with open(ds_path, 'r', encoding='utf-8') as f:
            self.ds = json.load(f)

    def search(self, query: str, k: int = 10) -> List[Tuple[float, str]]:
        # Chuyển truy vấn thành dạng tf-idf và tính điểm cosine similarity
        finals = []
        query_terms = query.lower().split()
        
        # Tính điểm cosine similarity cho mỗi tài liệu
        for doc_id in range(len(self.documents)):
            score = 0
            for term in query_terms:
                # Tính điểm nếu từ có trong tf_idf_list và ds
                if term in self.tf_idf_list and doc_id in self.tf_idf_list[term]:
                    score += self.tf_idf_list[term][doc_id] / self.ds[str(doc_id)]
            finals.append((score, self.documents[doc_id]))

        # Sắp xếp và trả về top k tài liệu có điểm cao nhất
        sorted_results = sorted(finals, key=lambda x: -x[0])[:k]
        return sorted_results

# Khởi tạo lớp TFIDF với các đường dẫn đến file JSON
tfidf = TFIDF('path/to/tf_idf_list.json', 'path/to/docs.json', 'path/to/ds.json')

# Thực hiện tìm kiếm
top_docs = tfidf.search("miền trung", 10)
print(top_docs)
```

### Tóm lại
- `TFIDF` class giúp xây dựng một công cụ tìm kiếm dựa trên điểm số TF-IDF và cosine similarity, giúp trả về các tài liệu phù hợp nhất với truy vấn.
- `ds.json` đóng vai trò chuẩn hóa điểm để so sánh tài liệu dễ dàng hơn theo độ tương đồng cosine.
