from flask import Flask, request, jsonify
from flasgger import Swagger, swag_from
from reranker import ReRanker
from rawSearch import TFIDF
import os

app = Flask(__name__)
swagger = Swagger(app)

# Initialize TFIDF and ReRanker classes
tf_idf = TFIDF('tf_idf_list.json', 'docs.json', 'ds.json')
re_ranker = ReRanker()


@app.route('/api/search', methods=['POST'])
@swag_from({
    'tags': ['Search'],
    'description': 'API tìm kiếm tài liệu với TF-IDF và re-ranking sử dụng AI. Trả về danh sách tài liệu trước và sau khi re-rank.',
    'parameters': [
        {
            'name': 'query',
            'in': 'body',
            'type': 'string',
            'required': True,
            'description': 'Truy vấn tìm kiếm'
        }
    ],
    'responses': {
        200: {
            'description': 'Danh sách tài liệu xếp hạng trước và sau khi re-rank',
            'examples': {
                'application/json': {
                    'initial_results': [
                        {"doc": "Tài liệu 1 về miền trung", "score": 0.8},
                        {"doc": "Tài liệu 2 về miền trung", "score": 0.75},
                    ],
                    'reranked_results': [
                        {"doc": "Tài liệu 2 về miền trung", "score": 0.95},
                        {"doc": "Tài liệu 1 về miền trung", "score": 0.89},
                    ]
                }
            }
        },
        400: {
            'description': 'Lỗi nếu không có truy vấn'
        }
    }
})
def handle_query():
    data = request.get_json(silent=True)
    if not data or not isinstance(data, dict):
        return jsonify({'error': 'Invalid JSON input'}), 400

    query = data.get('query')
    if not query:
        return jsonify({'error': 'No query provided'}), 400

    clean_query = process_query(query)
    filtered_results = tf_idf.search(clean_query, 10)

    # Extract initial results before re-ranking
    initial_results = [{"doc": doc[1], "score": doc[0]}
                       for doc in filtered_results]

    # Re-rank the results using the AI-based ReRanker
    try:
        # Prepare data for ReRanker
        # ReRanker expects a list of tuples: (score, document_content)
        reranker_input = [(doc['score'], doc['doc'])
                          for doc in initial_results]

        reranked_results = re_ranker.rank(query, reranker_input)
    except TypeError as e:
        print("Error:", e)
        return jsonify({'error': 'Unexpected data structure in filtered_results'}), 500

    return jsonify({'initial_results': initial_results, 'reranked_results': reranked_results})


def process_query(query):
    return query.strip()


if __name__ == '__main__':
    os.environ['KMP_DUPLICATE_LIB_OK'] = 'TRUE'
    app.run(debug=True)
