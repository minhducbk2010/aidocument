import unittest
import SimplePentatily_Final as sp
import matplotlib.pyplot as plt
import numpy as np
import threading
# detaxvalue = 1
# detayvalue = 1
# NxValue = 3
# NyValue = 3
# M1Value = 0.04
# M2Value = 0.04
# F_maxValue = 10
# F_minValue = 0
# lambda_maxValue = 2.0
# lambda_minValue = 0.0
class TestStringMethods(unittest.TestCase):

    def DrawGrid(self):
                # Hiển thị biểu đồ
                x = np.linspace(0, sp.detaxvalue *sp.NxValue, sp.NxValue+1)
                y = np.linspace(0, sp.detayvalue *sp.NyValue, sp.NyValue+1)    
                X, Y = np.meshgrid(x, y)
                
                # Vẽ lưới
                pl=plt.figure(figsize=(6, 6))
                plt.plot(X, Y, marker='.', color='k', linestyle='none')
                for i in range(sp.NxValue+1):
                    plt.plot([X[i, 0], X[i, -1]], [Y[i, 0], Y[i, -1]], 'k-')
                for i in range(sp.NyValue+1):
                    plt.plot([X[0, i], X[-1, i]], [Y[0, i], Y[-1, i]], 'k-')                  
                RowCount = sp.NyValue
                X,Y,Z =   sp.ConvertVectorToXYZ(self.F_optimal, sp.NxValue, sp.NyValue,sp.detaxvalue,sp.detayvalue)

                for row in self.Rows:
                    CellIndexCount = 0
                    for cell in row:
                        Fijx = CellIndexCount * sp.detaxvalue 
                        Fijy = RowCount * sp.detayvalue 
                        plt.plot(Fijx, Fijy, "bo")  # Vẽ điểm dữ liệu
                        plt.text(Fijx+0.04, Fijy+0.04, str(cell.Fij), color='blue')
                        Fiplus1jx =Fijx + sp.detaxvalue
                        Fiplus1jy = Fijy
                        plt.plot(Fiplus1jx, Fiplus1jy, 'bo')  # Vẽ điểm dữ liệu
                        plt.text(Fiplus1jx+0.04, Fiplus1jy+0.04, str(cell.Fijplus1), color='blue')
                        Fijplus1x = Fijx
                        Fijplus1y =  Fijy - sp.detayvalue
                        plt.plot(Fijplus1x, Fijplus1y, 'bo')  # Vẽ điểm dữ liệu
                        plt.text(Fijplus1x+0.04, Fijplus1y+0.04, str(cell.Fiplus1j), color='blue')
                        Fiplus1jplus1x = Fijx + sp.detaxvalue 
                        Fiplus1jplus1y =  Fijy - sp.detayvalue
                        plt.plot(Fiplus1jplus1x, Fiplus1jplus1y, 'bo')  # Vẽ điểm dữ liệu
                        plt.text(Fiplus1jplus1x+0.04, Fiplus1jplus1y+0.04, str(cell.Fiplus1jplus1), color='blue')
                        CellIndexCount = CellIndexCount + 1
                    RowCount = RowCount  - 1
                # Vẽ dữ liệu
                for datum in sp.MicDatasetValue:
                    plt.plot(datum.x, datum.y, 'ro')  # Vẽ điểm dữ liệu
                    plt.text(datum.x+0.05, datum.y+0.05, str(datum.value))  # Thêm chú thích

                # Tùy chỉnh các giá trị hiển thị trên trục
                plt.xticks(x)
                plt.yticks(y)

                plt.show()

    def test_Simple_4point (self):        
        sp.MicDatasetValue =sp.CreateSimpleMicData(4)
        self.F_optimal =   sp.fit()
        ListFData = sp.convert_vector_to_fdata_list(self.F_optimal, sp.NxValue, sp.NyValue)
        self.Rows=sp.describe_grid_value(ListFData,sp.NxValue)
        for Micdata in sp.MicDatasetValue:
                xik = Micdata.xik
                yik = Micdata.yik
                zenta = sp.CaclulateZeta( Micdata.x, xik, sp.detaxvalue)
                eta =  sp.CaclulateEta( Micdata.y, yik, sp.detayvalue)
                Micdataindex = sp.CaclulatMicIndex(Micdata.x,Micdata.y,sp.detaxvalue,sp.detayvalue,sp.NxValue,sp.NyValue)
                Fdata = sp.SelectFdataFromList(ListFData,Micdataindex)
                # CaclulateConstraintValue(Fij, Fiplus1j, Fijplus1, Fiplus1jplus1, zenta, eta, z)
                reslult= sp.CaclulateConstraintValue(Fdata.Fij, Fdata.Fiplus1j, Fdata.Fijplus1, Fdata.Fiplus1jplus1, zenta, eta, Micdata.value)
                #unit compare assert near 0 deffrent +-0.1 OK
                self.assertAlmostEqual(abs(reslult), 0, delta=1)
                #create thread and run DrawGrid function
        self.DrawGrid()
                # thread = threading.Thread(target=self.DrawGrid)
                # thread.start()
                # thread.join()

    def test_CreateMicDataWithSoundAttenuation (self):
            sp.NxValue = 3
            sp.NyValue = 3
            sp.M1Value = 6
            sp.M2Value = 100
            sp.lambda_minValue = sp.lambda_maxValue   =100.0
            sp.NxValue = 10
            sp.NyValue = 10
            sp.detaxvalue = 3/sp.NxValue
            sp.detayvalue = 3/sp.NyValue
            sp.tolvalue=4
            sp.MicDatasetValue =sp.CreateMicDataWithSoundAttenuation(200) 
            self.F_optimal =   sp.fit()
            ListFData = sp.convert_vector_to_fdata_list(self.F_optimal, sp.NxValue, sp.NyValue)
            self.Rows=sp.describe_grid_value(ListFData,sp.NxValue)
            X,Y,Z =   sp.ConvertVectorToXYZ(self.F_optimal, sp.NxValue, sp.NyValue,sp.detaxvalue,sp.detayvalue)
            x0 = sp.MicDatasetValue[0].x
            y0 =  sp.MicDatasetValue[0].y
            for x,y,z in zip(X,Y,Z):
                distance = sp.calculateDistance(x,y,x0,y0)
                TrueZ = sp.calculate_sound_attenuation( 10, distance)
                Error=TrueZ-z
                # self.assertLessEqual(abs(Error)/TrueZ, 0.3)
                pass
            # self.F_optimal print
            print("F_optimal = ",self.F_optimal)
            self.DrawGrid()
    def test_realTest (self):
            sp.NxValue = 3
            sp.NyValue = 3
            sp.M1Value = 30
            sp.M2Value = 100
            sp.lambda_minValue = sp.lambda_maxValue   =100.0
            sp.NxValue = 5
            sp.NyValue = 5
            sp.detaxvalue = 2/sp.NxValue
            sp.detayvalue = 2/sp.NyValue
            sp.tolvalue= 1
            sp.MicDatasetValue =sp.CreateMicDataForRealTest(10) 
            self.F_optimal =   sp.fit()
            ListFData = sp.convert_vector_to_fdata_list(self.F_optimal, sp.NxValue, sp.NyValue)
            self.Rows=sp.describe_grid_value(ListFData,sp.NxValue)
            X,Y,Z =   sp.ConvertVectorToXYZ(self.F_optimal, sp.NxValue, sp.NyValue,sp.detaxvalue,sp.detayvalue)
            x0 = sp.MicDatasetValue[0].x
            y0 =  sp.MicDatasetValue[0].y
            for x,y,z in zip(X,Y,Z):
                distance = sp.calculateDistance(x,y,x0,y0)
                TrueZ = sp.calculate_sound_attenuation( 10, distance)
                Error=TrueZ-z
                # self.assertLessEqual(abs(Error)/TrueZ, 0.3)
                pass

            self.DrawGrid()    
    def test_J1 (self):
        #  List_Fdata 1 to 16
        List_Fdata = [1,1,1,1
                      ,1,1,1,1,
                      1,1,1,1,
                      1,1,1,1]    
        matrix= sp.convert_vector_to_fdata_list(List_Fdata, 3, 3)   
        Result=   sp.CaclulateJ1(matrix, 3,3)
        self.assertEqual(Result, 0)
    def test_J2GroupA (self):
        #  List_Fdata 1 to 16
        List_Fdata = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]    
        matrix= sp.convert_vector_to_fdata_list(List_Fdata, 3, 3)   
        Result=   sp.J2GroupA(matrix, 3)
        self.assertEqual(Result, 0)
    def test_J2GroupB (self):
        #  List_Fdata 1 to 16
        List_Fdata = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]    
        matrix= sp.convert_vector_to_fdata_list(List_Fdata, 3, 3)   
        Result=   sp.J2GroupA(matrix, 3)
        self.assertEqual(Result, 0)

    def test_J2GroupC (self):
        #  List_Fdata 1 to 16
        List_Fdata = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]    
        matrix= sp.convert_vector_to_fdata_list(List_Fdata, 3, 3)   
        Result=   sp.J2GroupC(matrix, 3)
        self.assertEqual(Result, 0)
if __name__ == '__main__':
    unittest.main()