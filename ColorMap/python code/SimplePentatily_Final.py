
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import differential_evolution
from typing import List
import math
# import random
import random
F_maxValue = 10
F_minValue = 0

M1Value = 1
M2Value = 9
lambda_minValue = 1.0
lambda_maxValue = 2.0
NxValue = 3
NyValue = 3
detaxvalue = 2/NxValue
detayvalue = 2/NyValue

K = 1
tolvalue = 1


def ConvertVectorToXYZ(Vector, Nx, Ny, detax, detay):
    x = []
    y = []
    z = []
    CellIndexCount = 0
    RowCount = Ny
    for i in range(0, (Nx+1)):
        for j in range(0, (Ny+1)):
            y.append(((Ny)-i) * detay)
            x.append(j * detax)
            z.append(Vector[CellIndexCount])
            CellIndexCount = CellIndexCount + 1
        RowCount = RowCount - 1
        # CellIndexCount = RowCount * Nx
    return x, y, z


def CaclulatMicIndex(x, y, detax, detay, Nx, Ny):
    Xindex = math.floor(x/detax)
    Yindex = math.floor(y/detay)
    Result = Xindex + ((Ny-1)-Yindex)*Nx
    return Result


class MicData:
    def __init__(self, x, y, z, detax, detay, Nx, Ny):
        self.x = x
        self.y = y
        self.value = z
        self.detax = detax
        self.detay = detay
        self.xik = x//detax * detax
        self.yik = y//detay * detay
        self.K = K
        self.zenta = (x - self.xik) / detax
        self.eta = (y - self.yik) / detay

        pass


class FData:
    def __init__(self, Fij, Fiplus1j, Fijplus1, Fiplus1jplus1, index):
        self.index = index
        self.Fij = Fij
        self.Fiplus1j = Fiplus1j
        self.Fijplus1 = Fijplus1
        self.Fiplus1jplus1 = Fiplus1jplus1
# from detaxvalue and detayvalue and NxValue and NyValue calculate xmax and ymax
# define Caclulatexmax


def Caclulatexmax(detax, Nx):
    return detax*Nx
# define Caclulateymax


def Caclulateymax(detay, Ny):
    return detay*Ny


def CreateSimpleMicData(NoteNumber):
    # Create a list of MicData
    ListMicData = []
    # Create a list of FData
    for j in range(0, NoteNumber):
        for i in range(0, NoteNumber):
            # create random value for Fij Fijplus1 Fiplus1j Fiplus1jplus1 range 0 to 10
            # from detaxvalue and detayvalue and NxValue and NyValue calculate xmax and ymax
            x = 0.7 + i * 0.5
            y = 0.7 + j * 0.5
            z = 0.5 + i * 0.5

            # create FData
            MicDataNew = MicData(x, y, z, detaxvalue,
                                 detayvalue, NxValue, NyValue)
            ListMicData.append(MicDataNew)
    return ListMicData


def CreateMicDatawithRandomValue(NoteNumber):
    # Create a list of MicData
    ListMicData = []
    # Create a list of FData
    for j in range(0, NoteNumber):
        # create random value for Fij Fijplus1 Fiplus1j Fiplus1jplus1 range 0 to 10
        # from detaxvalue and detayvalue and NxValue and NyValue calculate xmax and ymax
        x = random.uniform(0, NxValue * detaxvalue)
        y = random.uniform(0,  NyValue * detayvalue)
        z = random.uniform(F_minValue,   F_maxValue)
        # create FData
        MicDataNew = MicData(x, y, z, detaxvalue, detayvalue, NxValue, NyValue)
        ListMicData.append(MicDataNew)
    return ListMicData


def calculate_sound_attenuation(L0, d):
    attenuation = L0 - 2 * d / 1
    if attenuation < 0:
        attenuation = 0
    return attenuation


def calculateDistance(x1, y1, x2, y2):
    return math.sqrt((x1-x2)**2 + (y1-y2)**2)


def CreateMicDataWithSoundAttenuation(NoteNumber):
    # Create a list of MicData
    ListMicData = []
    # x0=random.uniform(0, NxValue * detaxvalue)
    # y0=random.uniform(0,  NyValue * detayvalue)
    x0 = 1.5
    y0 = 1.5
    z0 = 10
    MicDataNew = MicData(x0, y0, z0, detaxvalue, detayvalue, NxValue, NyValue)

    # Create a list of FData
    for j in range(0, NoteNumber):
        # create random value for Fij Fijplus1 Fiplus1j Fiplus1jplus1 range 0 to 10
        # from detaxvalue and detayvalue and NxValue and NyValue calculate xmax and ymax
        x = random.uniform(0, NxValue * detaxvalue)
        y = random.uniform(0,  NyValue * detayvalue)
        distance = calculateDistance(x, y, x0, y0)
        z = calculate_sound_attenuation(z0, distance)
        # create FData
        MicDataNew = MicData(x, y, z, detaxvalue, detayvalue, NxValue, NyValue)
        ListMicData.append(MicDataNew)
    return ListMicData


def CreateMicDataForRealTest(NoteNumber):
    # Create a list of MicData
    ListMicData = []
    # 0.16	-0.96	-300
    MicDataNew = MicData(1+(0.16), 1 + (-0.96), -300,
                         detaxvalue, detayvalue, NxValue, NyValue)
    ListMicData.append(MicDataNew)
    # 0.78	-0.6	-150
    MicDataNew = MicData(1+(0.78), 1 + (-0.6), -150,
                         detaxvalue, detayvalue, NxValue, NyValue)
    ListMicData.append(MicDataNew)
    # 0.78	0.55	0
    MicDataNew = MicData(1+(0.78), 1 + (0.55), 0,
                         detaxvalue, detayvalue, NxValue, NyValue)
    ListMicData.append(MicDataNew)
    # 0.16	0.9	150
    MicDataNew = MicData(1+(0.16), 1 + (0.9), 150,
                         detaxvalue, detayvalue, NxValue, NyValue)
    ListMicData.append(MicDataNew)
    # -0.83	0.32	300
    MicDataNew = MicData(1+(-0.83), 1 + (0.32), 300,
                         detaxvalue, detayvalue, NxValue, NyValue)
    ListMicData.append(MicDataNew)
    # -0.4	-0.83	350
    MicDataNew = MicData(1+(-0.4), 1 + (-0.83), 350,
                         detaxvalue, detayvalue, NxValue, NyValue)
    ListMicData.append(MicDataNew)

    # -0.65	-0.26	-350
    MicDataNew = MicData(1+(-0.65), 1 + (-0.26), -350,
                         detaxvalue, detayvalue, NxValue, NyValue)
    ListMicData.append(MicDataNew)
    # -0.07	0.74	10
    MicDataNew = MicData(1+(-0.07), 1 + (0.74), 10,
                         detaxvalue, detayvalue, NxValue, NyValue)
    ListMicData.append(MicDataNew)
    # 0.5	-0.26	15
    MicDataNew = MicData(1+(0.5), 1 + (-0.26), 15,
                         detaxvalue, detayvalue, NxValue, NyValue)
    ListMicData.append(MicDataNew)
    # -0.1	0.1	-10
    MicDataNew = MicData(1+(-0.1), 1 + (0.1), -10,
                         detaxvalue, detayvalue, NxValue, NyValue)
    ListMicData.append(MicDataNew)

    return ListMicData


MicDatasetValue = CreateMicDataWithSoundAttenuation(40)


def describe_grid_value(vector: List[FData], num_elements_row):
    # Create an empty list to hold the rows of the grid
    grid = []
    # Iterate over the vector in steps of num_elements_row
    for i in range(0, len(vector), num_elements_row):
        # Append the next num_elements_row elements to the grid as a new row
        grid.append(vector[i:i+num_elements_row])
    return grid


def compute_number_of_edges_and_corners(Nx, Ny):
    return (Nx + 1) * (Ny + 1)


def CaclulateZeta(xk, xik, detax):
    return (xk - xik)/detax


def CaclulateEta(yk, yik, detay):
    return (yk - yik)/detay


def Caclulatexik(index, detax):
    return index*detax


def Caclulateyik(index, detay):
    return index*detay


def SelectFdataFromList(ListFData: List[FData], index):
    for i in ListFData:
        if i.index == index:
            return i
    return None


def CaclulateConstraintValue(Fij, Fiplus1j, Fijplus1, Fiplus1jplus1, zenta, eta, z):
    return (1-zenta)*(1-eta)*Fij + (1-zenta)*eta*Fijplus1 + zenta*(1-eta)*Fiplus1j + (zenta)*(eta)*Fiplus1jplus1 - z


def constraint(Full_Grib_Data: List[FData], MicDataset: List[MicData], detax, detay) -> float:
    Result = []
    for Micdata in MicDataset:
        Micdataindex = CaclulatMicIndex(
            Micdata.x, Micdata.y, detaxvalue, detayvalue, NxValue, NyValue)
        Fdata = SelectFdataFromList(Full_Grib_Data, Micdataindex)
        try:
            Fij = Fdata.Fij
            Fiplus1j = Fdata.Fiplus1j
            Fijplus1 = Fdata.Fijplus1
            Fiplus1jplus1 = Fdata.Fiplus1jplus1
            xik = Micdata.xik
            yik = Micdata.yik
            zenta = CaclulateZeta(Micdata.x, xik, detax)
            eta = CaclulateEta(Micdata.y, yik, detay)
            z = Micdata.value
            constraint_value = CaclulateConstraintValue(
                Fij, Fiplus1j, Fijplus1, Fiplus1jplus1, zenta, eta, z)
            # if abs(constraint_value) >1:
            #      constraint_value = 1000 * constraint_value
        except Exception as e:
            # rasie expetion "fail to  detecte index "
            raise Exception("fail to  detecte index ")
        Result.append(constraint_value)
    violation_sum_squared = K * np.linalg.norm(Result, ord=2) ** 2
    return violation_sum_squared

# N is number of point


def CaclulateJ1(List_F_Data: List[FData], Nx, Ny) -> float:
    # create GroupAResult and GroupBResult
    # if Nx Ny are != error
    if Nx != Ny:
        raise ValueError("Nx must be equal Ny")
    N = Nx
    GroupAResult = 0
    GroupBResult = 0
    rows = describe_grid_value(List_F_Data, N)
    for row in rows:
        for k in row:
            # Get index of point K
            # Get value of F at point K
            Fij = k.Fij
            Fiplus1j = k.Fiplus1j
            Fijplus1 = k.Fijplus1
            Fiplus1jplus1 = k.Fiplus1jplus1
            # Calculate J1 at point K
            GroupAResult = GroupAResult+(Fiplus1j-Fij) ** 2
            GroupBResult = GroupBResult + (Fijplus1-Fij) ** 2

    return GroupAResult+GroupBResult


def J2GroupA(VectorF: List[FData], N):

    rows = describe_grid_value(VectorF, N)
    # loop 2 element in row
    index = 0
    Buffer = 0
    A = 0
    B = 0
    C = 0
    D = 0
    for row in rows:
        index = 0
        while index < len(row)-1:
            Data = row[index:index+2]
            nplus1Grid = Data[1]
            nGrid = Data[0]
            A = nplus1Grid.Fiplus1j
            B = nplus1Grid.Fij
            C = nGrid.Fiplus1j
            D = nGrid.Fij
            Buffer = Buffer + ((A-B) - (C-D))**2
            index += 1
    return Buffer


def J2GroupB(VectorF: List[FData], N):
    rows = describe_grid_value(VectorF, N)
    index = 0
    Buffer = 0
    rowindex = 1
    for row in rows[1:]:
        while index < len(row):
            nplus1Grid = rows[rowindex][index]
            nGrid = rows[rowindex-1][index]
            Buffer = Buffer + \
                ((nplus1Grid.Fijplus1-nplus1Grid.Fij) - (nGrid.Fijplus1-nGrid.Fij))**2
            index += 1
        rowindex += 1
    return Buffer


def J2GroupC(VectorF: List[FData], N):
    Buffer = 0
    rows = describe_grid_value(VectorF, N)
    for row in rows:
        for Grid in row:
            Buffer = Buffer + (((Grid.Fiplus1jplus1-Grid.Fiplus1j)-(Grid.Fiplus1j-Grid.Fij))+(
                (Grid.Fiplus1jplus1-Grid.Fijplus1)-(Grid.Fijplus1-Grid.Fij)))**2
    return Buffer
# Define caclulate J2


def CaclulateJ2(VectorF: List[FData], Nx, Ny) -> float:
    # if Nx Ny are != error
    if Nx != Ny:
        raise ValueError("Nx must be equal Ny")
    N = Nx
    # create GroupAResult and GroupBResult
    ResultA = J2GroupA(VectorF, N)
    ResultB = J2GroupB(VectorF, N)
    ResultC = J2GroupC(VectorF, N)

    return ResultA + ResultB + ResultC


def CutVectorTo4PointSet(vector: List[float]):
    for i in range(0, len(vector), 2):
        # Append the next num_elements_row elements to the grid as a new row
        grid.append(vector[i:i+4])
    return grid


def convert_vector_to_fdata_list(vector, Nx, Ny):
    # Tạo list trống để chứa dữ liệu FData
    FData_list = []
    Nx = Nx + 1
    Ny = Ny + 1
    # Số lượng các điểm FData phải bằng Nx * Ny
    assert len(vector) == (
        Nx) * (Ny), "Invalid number of points. Number of points should be equal to Nx * Ny."
    # Chuyển list F thành một ma trận 2D có Nx hàng và Ny cột
    F_matrix = np.array(vector).reshape((Nx, Ny))
    # Lặp qua ma trận và tạo FData
    for i in range(Nx-1):
        for j in range(Ny-1):
            Fij = F_matrix[i][j]
            Fiplus1j = F_matrix[i+1][j]
            Fijplus1 = F_matrix[i][j+1]
            Fiplus1jplus1 = F_matrix[i+1][j+1]
            FData_list.append(FData(Fij, Fiplus1j, Fijplus1,
                              Fiplus1jplus1, i*(Nx-1) + j))

    return FData_list

# loss_function L(F, λ) = J(F) + λ * constraint(F)


def loss_function(vector: List[float], MicDataset: List[MicData], detax, detay, Nx, Ny, m1, m2, lambda_value):
    Full_Grib_Data = convert_vector_to_fdata_list(vector, NxValue, NyValue)
    objective = m1*CaclulateJ1(Full_Grib_Data, Nx, Ny) + \
        m2*CaclulateJ2(Full_Grib_Data, Nx, Ny)
    constraint_violation = constraint(Full_Grib_Data, MicDataset, detax, detay)
    return objective + lambda_value * constraint_violation


# MicDatasetValue = CreateSimpleMicData(5)
def objective_function(x):
    # Trả về giá trị của hàm mục tiêu và hàm ràng buộc tại x
    # Ở đây, x là một vector chứa các giá trị của biến tối ưu hóa, giả sử x = [F, lambda_value]
    Full_Grib_Data = x[:-1]
    lambda_value = x[-1]
    # Đọc dữ liệu input
    Result = loss_function(Full_Grib_Data, MicDatasetValue, detaxvalue,
                           detayvalue, NxValue, NyValue, M1Value, M2Value, lambda_value)
    print("Result = ", Result)
    return Result


def fit():
    Fnumber = compute_number_of_edges_and_corners(NxValue, NyValue)
    bounds = [(F_minValue, F_maxValue)]*Fnumber + \
        [(lambda_minValue, lambda_maxValue)]

    result = differential_evolution(
        objective_function, bounds, tol=tolvalue, strategy="best1exp")
    F_optimal = result.x[:-1]
    lambda_optimal = result.x[-1]
    print("F_optimal = ", F_optimal)
    print("lambda_optimal = ", lambda_optimal)
    print("objective_function = ", objective_function(result.x))
    return F_optimal


if __name__ == "__main__":
    # F_maxValue = 10
    # F_minValue = 0
    # detaxvalue = 1
    # detayvalue = 1
    # NxValue = 6
    # NyValue = 6
    # M1Value = 0.5
    # M2Value = 0.5
    # lambda_maxValue = lambda_minValue = 3.0
    # tolvalue = 0.01
    MicDatasetValue = CreateMicDataWithSoundAttenuation(10)
    fit()
