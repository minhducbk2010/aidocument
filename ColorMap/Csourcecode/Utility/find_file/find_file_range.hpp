#pragma once

// Model of:
//   Readable and Single Pass (Input) Range of WIN32_FIND_DATA

#include <cassert>
#include <boost/mpl/bool.hpp>
#include <boost/noncopyable.hpp>
#include <boost/range/iterator_range.hpp>
#include "find_file_iterator.hpp"

namespace find_file {

	namespace detail {

		// See: Boost.BaseFromMember
		struct find_file_range_initializer
		{
		protected:
			HANDLE m_hFind;
			WIN32_FIND_DATA m_data;

			explicit find_file_range_initializer(LPCTSTR pszName)
			{
				m_hFind = ::FindFirstFile(pszName, &m_data);
			}

			~find_file_range_initializer()
			{
				if (m_hFind != INVALID_HANDLE_VALUE)
					::FindClose(m_hFind);
			}
		};

	} // namespace detail

struct find_file_range  :
	private boost::noncopyable,
	private detail::find_file_range_initializer,
	boost::iterator_range<find_file_iterator>
{
private:
	typedef boost::iterator_range<find_file_iterator> super_t;

public:
	explicit find_file_range(LPCTSTR pszName) :
		find_file_range_initializer(pszName),
		super_t(
			find_file_iterator(m_hFind, m_data),
			find_file_iterator()
		)
	{ }
};

} // namespace find_file

// Workaround:
//   As find_file_range is derived from iterator_range,
//   foreach-engine mistakes find_file_range for iterator_range
//   that is copyable. Caution!
namespace boost { namespace foreach_detail_ {

inline boost::mpl::false_ *cheap_copy(find_file::find_file_range *) { return 0; }

} } // namespace boost::foreach_detail_
