#pragma once

#include <cassert>
#include <boost/iterator/iterator_facade.hpp>

namespace find_file {

struct find_file_iterator :
	boost::iterator_facade<
		find_file_iterator,
		WIN32_FIND_DATA,
		boost::single_pass_traversal_tag
	>
{
	// See: sgi.STL::istream_iterator

	find_file_iterator() : // the end iterator
		m_found(false)
	{ }

	find_file_iterator(HANDLE hFind, WIN32_FIND_DATA& data) :
		m_hFind(hFind), m_pdata(&data), m_found(hFind != INVALID_HANDLE_VALUE)
	{ }

private:
	HANDLE m_hFind;
	WIN32_FIND_DATA *m_pdata;
	bool m_found;

	void find_next_file()
	{
		m_found = (::FindNextFile(m_hFind, m_pdata) != FALSE);

		if (!m_found)
			assert(::GetLastError() == ERROR_NO_MORE_FILES);
	}

	bool equal_aux(find_file_iterator const& other) const
	{
		return m_hFind == other.m_hFind && m_pdata == other.m_pdata;
	}

// iterator_facade implementation
	friend class boost::iterator_core_access;

	void increment()
	{
		assert(m_found && _T("find_file::find_file_iterator - iterator is invalid."));

		find_next_file();
	}

	bool equal(find_file_iterator const& other) const
	{
		return (m_found == other.m_found) && (!m_found || equal_aux(other));
	}

	WIN32_FIND_DATA& dereference() const
	{
		assert(m_found && _T("find_file::find_file_iterator - iterator gets an access violation"));

		return *m_pdata;
	}
};

} // namespace find_file
