//===========================================================================
/*! @file
 */
//===========================================================================
#pragma once
//---------------------------------------------------------------------------
#include <deque>
#include <vector>
#include <algorithm>
#include <functional>
#include <boost/bind.hpp>
//---------------------------------------------------------------------------
struct ST_XYDATA
{
    double x;
    double y;
};

//! 補間クラスの抽象クラス
class OCInterpolate
{
public:
	virtual ~OCInterpolate() {}

    //! 初期化（標本数設定）
    virtual void Init(int n, double* x, double* y)
    {
        SortForXdata(x, y, n);

        m_N = n;
        m_X = x;
        m_Y = y;
    }

    //! 前処理
    virtual bool Prepare() = 0;

    //! 補間計算
    virtual double Calc(double xVal) = 0;

protected:
	struct xcomparator {
		bool operator()(ST_XYDATA const& a, ST_XYDATA const& b) {
			return a.x < b.x;
		}
	};
	struct ycomparator {
		bool operator()(ST_XYDATA const& a, ST_XYDATA const& b) {
			return a.y < b.y;
		}
	};

    void SortForXdata(double* x, double* y, int n)
    {
        if(n < 2) { return; }

        std::vector<ST_XYDATA> xy;
		xy.reserve(n);

        for(int i = 0; i < n; ++i) {
			ST_XYDATA temp = {x[i], y[i]};
            xy.push_back(temp);
        }

        // ソートする
        // Yを昇順で並べてから...
        std::sort(xy.begin(), xy.end(), ycomparator());
		std::stable_sort(xy.begin(), xy.end(), xcomparator());

        for(int i = 0; i < n; ++i)
        {   /* ソートした結果を引数 x, y に戻す */
            x[i] = xy[i].x;
            y[i] = xy[i].y;
        }
    }

    //! X値の重複を削除し、対応するYに平均値を設定
    /*! X値は前もってソートされていることが必要。削除後の標本数を返す */
    virtual int AverageSameData(double* x, double* y, int n)
    {
        if(n < 2) { return n; }

        std::vector<ST_XYDATA>	xy;
        ST_XYDATA	tmp;
        int		rep = 0;
        double	yVal = 0;

        // 修正内容を xy に順次追加
        for (int i = 0; i < n; i++) {
            rep++;
            yVal += y[i];
            if (i == n-1 || x[i] != x[i+1]) {
                tmp.x = x[i];
                tmp.y = yVal / rep;
                xy.push_back(tmp);
                rep = 0;
                yVal = 0;
            }
        }

        // 結果を引数 x, y に渡す
        for (size_t i = 0; i < xy.size(); i++) {
            x[i] = xy[i].x;
            y[i] = xy[i].y;
        }

        return (int)(xy.size());
    }

protected:
    int m_N;		//!< 標本点数
    double* m_X;	//!< 標本点の配列
    double* m_Y;	//!< 標本点に対応するf(x)の配列
};

