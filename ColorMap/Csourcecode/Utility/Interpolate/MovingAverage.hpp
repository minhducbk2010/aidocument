/*!	@file
	@author	wada
	@date	2008-03-05

	@brief	移動平均クラスの宣言
 */
#pragma once

#include <vector>
#include "Interpolate.hpp"
#include "utility.h"

class OCMovingAverage : public OCInterpolate
{
public:
    OCMovingAverage() : m_count(3) {}
    ~OCMovingAverage() {}

    virtual void Init(int n, double* x, double* y)
    {
        // 区間データ点数を超えないようにする
        m_count = std::max<int>(std::min<int>(m_count, n - 1), 2);
        __super::Init(n, x, y);
    }

    void SetAverageCount(int count)
    {
        _ASSERT(count >= 2);
        m_count = count;
    }
    virtual bool Prepare()
    {
        // 平均の算出
        m_average2.clear();
        for (int i = 0; i < m_N; ++i)
        {
            if (i - m_count + 1 < 0) { continue; }

            double dSum = 0.;
            for (int j = 0; j < m_count; j++)
            {
                int const idx = i - m_count + j + 1;
                dSum += m_Y[idx];
            }
            double dAve = dSum / m_count;
            m_average2.push_back(OCPoint(m_X[i], dAve));
        }
        return true;
    }
    virtual double Calc(double x)
    {
        if (m_average2.empty() || m_average2[0].x > x) { return DBL_MAX; }

        size_t index = 0;
        for (size_t i = 0; i < m_average2.size() - 1; ++i) {
            if (m_average2[i].x <= x && x <= m_average2[i + 1].x) {
                index = i;
                break;
            }
        }

        // x前後の値を使用して直線補間します
        double const deno = m_average2[index + 1].x - m_average2[index].x;
        if (IsZero(deno)) {
            return m_average2[index].y;
        }
        double const slope = (m_average2[index + 1].y - m_average2[index].y) / deno;

        double const y1 = slope * x;
        double const y2 = slope * m_average2[index].x;
        return y1 + (m_average2[index].y - y2);
    }

    int GetCount() const { return m_count; }

private:
    int m_count;
    std::vector<OCPoint> m_average2;
};
