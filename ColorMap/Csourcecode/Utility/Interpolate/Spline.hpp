/*!	@file
	@brief	3次スプラインクラスの宣言
*/
#pragma once
#include "Interpolate.hpp"
#include <vector>
#include <cfloat>
//---------------------------------------------------------------------------

template<class Iter>
inline bool is_unique(Iter first, Iter last)
{
	double a = *first;
	++first;
	for (; first != last; ++first) {
		if (a == *first) { return false; }
	}
	return true;
}

class OCSpline : public OCInterpolate
{

public:
	OCSpline() : m_Z(0) {}
	virtual ~OCSpline() { delete[] m_Z; }

    bool Prepare()
	{
		delete[] m_Z;
		m_Z = 0;

		// Xの値ユニークでない場合はNG
		if (!is_unique(m_X, m_X + m_N)) {
			return false;
		}

		m_Z = new double[ m_N ];

		maketable( m_X, m_Y, m_Z );

		return true;
	}

    double Calc( double xVal )
	{
		if (!m_Z) {
			return DBL_MAX;
		}
		return spline(xVal, m_X, m_Y, m_Z);
	}

private:
    void    maketable(double *x, double *y, double *z)
	{
		int    i;
		double t;
		double *h = new double[ m_N ];
		double *d = new double[ m_N ];

		z[0]       = 0;
		z[m_N - 1] = 0;  /* 両端点での y''(x) / 6 */

		for ( i = 0; i < m_N - 1; i++ )
		{
			h[i    ] =  x[i + 1] - x[i];
			d[i + 1] = (y[i + 1] - y[i]) / h[i];
		}

		z[1] = d[2] - d[1] - h[0] * z[0];
		d[1] = 2 * (x[2] - x[0]);

		for (i = 1; i < m_N - 2; i++)
		{
			t = h[i] / d[i];
			z[i + 1] = d[i + 2] - d[i + 1] - z[i] * t;
			d[i + 1] = 2 * (x[i + 2] - x[i]) - h[i] * t;
		}

		z[m_N - 2] -= h[m_N - 2] * z[m_N - 1];

		for (i = m_N - 2; i > 0; i--)
		{
			z[i] = (z[i] - h[i] * z[i + 1]) / d[i];
		}

		delete[] h;
		delete[] d;
	}
    double  spline(double t, double *x, double *y, double *z)
	{
		int     i, j, k;
		double  d, h;

		i = 0;
		j = m_N - 1;

		while (i < j)
		{
			k = (i + j) / 2;

			if (x[k] < t)
			{
				i = k + 1;
			}
			else
			{
				j = k;
			}
		}

		if (i > 0)
		{
			i--;
		}

		h = x[i + 1] - x[i];
		d = t - x[i];

		double dRet = (
						( (z[i + 1] - z[i]) * d / h + z[i] * 3              ) * d
					  + ( (y[i + 1] - y[i]) / h - (z[i] * 2 + z[i + 1]) * h )
					  ) * d + y[i];
		return dRet;
	}

    double* m_Z;
};
 