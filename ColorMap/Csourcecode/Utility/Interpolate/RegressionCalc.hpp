//===========================================================================
/*! @file
 */
//===========================================================================
#pragma once
//---------------------------------------------------------------------------
#include <vector>
#include <boost/algorithm/minmax_element.hpp>
#include <sstream>
#define _USE_MATH_DEFINES
#include <math.h>
#include "Regress.hpp"
//---------------------------------------------------------------------------
class OCRegressionCalc : public OCInterpolate
{
public:
    enum Type
    {
        Regression,		//!< 回帰曲線
        Linear,			//!< 線形近似
        Logarithm,		//!< 対数近似
        Exponent,		//!< 指数近似
        Power,			//!< 累乗近似
    };

public:
    OCRegressionCalc() : m_type(Regression), m_nDegree(2) {}

    virtual bool Prepare()
    {
        m_vRec.clear();

        int const degree = m_type == Regression ? m_nDegree : 1;

        //!@todo 負数の扱い
        std::vector<double> dx(m_X, m_X + m_N), dy(m_Y, m_Y + m_N);
        switch (m_type)
        {
        case Logarithm:
            std::transform(dx.begin(), dx.end(), dx.begin(), std::logf);
            break;
        case Exponent:
            std::transform(dy.begin(), dy.end(), dy.begin(), std::logf);
            break;
        case Power:
            std::transform(dx.begin(), dx.end(), dx.begin(), std::logf);
            std::transform(dy.begin(), dy.end(), dy.begin(), std::logf);
            break;
        }

        ST_REGRSLT stRet;
        stRet.nOrder = degree;
        int n = OCRegress().Execute(stRet.nOrder, &dx[0], &dy[0],
                                    m_N, stRet.b, stRet.col, stRet.s, stRet.t);
        if(n) {
            m_vRec.assign(stRet.b, stRet.b + n);
            m_nOrder = n;
        }
        return true;
    }
    virtual double Calc(double x)
    {
        double y = m_vRec[0];

        switch (m_type)
        {
        case Regression:
            {
                if(m_vRec.size() >= 1) {
                    y += m_vRec[1] * x;
                }
                for(int i = 2; i < m_nOrder; ++i) {
                    y += m_vRec[i] * ::pow(x, i);
                }

                return y;
            }
        case Linear:
            if (m_vRec.size() > 1) {
                return y + m_vRec[1] * x;
            }
            break;
        case Logarithm:
            if (x <= 0.0) { return DBL_MAX; }
            if (m_vRec.size() > 1) {
                return y + m_vRec[1] * std::log(x);
            }
            break;
        case Exponent:
            if (m_vRec.size() > 1) {
                return std::exp(y) * std::exp(m_vRec[1] * x);
            }
            break;
        case Power:
            if (m_vRec.size() > 1) {
                return std::exp(y) * std::pow(x, m_vRec[1]);
            }
            break;
        default:
            assert(false);
        }
        return DBL_MAX;
    }

    void SetType(Type type)
    {
        m_type = type;
    }
    void SetDegree(int degree)
    {
        m_nDegree = degree;
    }

    std::wstring GetFormula() const
    {
        if(m_vRec.empty()) { return std::wstring(); }

        std::wstring expr(_T(" y = "));

        switch (m_type)
        {
            // 多項式と線形
        case Regression:
        case Linear:
            // 後ろから...
            for(int i = (int)m_vRec.size() - 1; i >= 0; --i) {
                expr += GetFormula(m_vRec[i], i);
                if(i != 0) {
                    expr += _T(" + ");
                }
            }
            break;
            // 対数
        case Logarithm:
            if (m_vRec.size() > 1) {
                std::wostringstream woss;
                // streamでカンマ区切りが生じないようにstreamのロケールを一時的に変更
                woss.imbue(std::locale("japanese").combine<std::numpunct<wchar_t>>(std::locale::classic()));
                woss << _T("log(x) * (") << m_vRec[1] << _T(") + (") << m_vRec[0] << _T(")");
                expr += woss.str();
            }
            break;
            // 指数
        case Exponent:
            if (m_vRec.size() > 1) {
                double k0 = std::exp( m_vRec[0] );
                std::wostringstream woss;
                // streamでカンマ区切りが生じないようにstreamのロケールを一時的に変更
                woss.imbue(std::locale("japanese").combine<std::numpunct<wchar_t>>(std::locale::classic()));
                woss << _T("exp{(") << m_vRec[1] << _T(") * x } * (") << k0 << _T(")");
                expr += woss.str();
            }
            break;
            // 累乗
        case Power:
            if (m_vRec.size() > 1) {
                double k0 = std::exp( m_vRec[0] );
                std::wostringstream woss;
                // streamでカンマ区切りが生じないようにstreamのロケールを一時的に変更
                woss.imbue(std::locale("japanese").combine<std::numpunct<wchar_t>>(std::locale::classic()));
                woss << _T("x^(") << m_vRec[1] << _T(") * (") << k0 << _T(")");
                expr += woss.str();
            }
            break;
        }

        return expr;
    }

private:
    std::wstring GetFormula(double d, int n) const
    {
        std::wostringstream woss;
        // streamでカンマ区切りが生じないようにstreamのロケールを一時的に変更
        woss.imbue(std::locale("japanese").combine<std::numpunct<wchar_t>>(std::locale::classic()));

        // 係数を書く (定数項の場合を含む)
        woss << _T("(") << d << _T(")");
        if( n >= 1 ){
            // xを書く
            woss << _T(" * x");
            if( n >= 2 ){
                // 2次以上は、次数も書く
                woss << _T("^") << n;
            }
        }

        return woss.str();
    }

private:
    Type m_type;
    std::vector<double> m_vRec;
    int m_nDegree;
    int m_nOrder;
};
