/*! @file
 */
#pragma once
//---------------------------------------------------------------------------
#include "Interpolate.hpp"
//---------------------------------------------------------------------------

class OCBSpline : public OCInterpolate
{
public:
    bool Prepare() { return true; }

    double Calc( double xVal )
    {
        double cn, x, y, t;
        int i, j,k;

		int xUniqMaxIndex = 0;
		double xUniqMaxVal = m_X[0];

        for ( i = 0; i < m_N; i++ )
        {
			if (xUniqMaxVal < m_X[i]) {
				xUniqMaxIndex = i;
				xUniqMaxVal = m_X[i];
			}
            if ( xVal < m_X[i] )
                break;
        }

		if ( xVal == m_X[m_N - 1] ) {
			i = xUniqMaxIndex;
			if (i == 0) { return DBL_MAX; }
		}

        if ( i <= 0 || i >= m_N ) {
            return DBL_MAX;
        }

        t = i - 1 + ( ( xVal - m_X[i - 1] ) / ( m_X[i] - m_X[i - 1] ) );
        x = 0.0;
        y = 0.0;

        for(j = (int)t - 2 ; j <= (int)t + 2 ; j++)
        {
            k = j;
            if(j < 0) k = 0;
            if(j > m_N - 1 ) k = m_N - 1;
            cn = Coefficent(t - j);
            x += m_X[k] * cn;
            y += m_Y[k] * cn;
        }

        return y;
    }

private:
    double Coefficent(double t)
    {
        double	r,d;

        if(t < 0.0) {
            t = -t;
        }
        
        if(t < 1.0) {
            r = (3.0 * t * t * t -6.0 * t * t + 4.0) / 6.0;
        }
        else if(t < 2.0) {
            d = t - 2.0;
            r = -d * d * d / 6.0;
        }
        else {
            r = 0.0;
        }

        return r;
    }
};
