#pragma once

//#include "Interpolate.hpp"
#include <deque>

class OCLineInterpolate
{
public:
    void Init(int nCnt, double* pXData, double* pYData)
    {
        m_vXData.clear();
        m_vYData.clear();
        m_vSlope.clear();

        double dSlope;
        for(int i = 0; i < nCnt; ++i)
        {
            m_vXData.push_back(*(pXData + i));
            m_vYData.push_back(*(pYData + i));

            if(i < (nCnt - 1))
            {
                // データ間の傾きを求める
                double dDeno = *(pXData + i + 1) - *(pXData + i);  // 傾きの分母
                if(IsZero(dDeno))
                {
                    dSlope = DBL_MAX;
                }
                else
                {
                    dSlope = (*(pYData + i + 1) - *(pYData + i)) / dDeno;
                }
                m_vSlope.push_back(dSlope);
            }
        }
    }

    double Calc(double x)
    {
        unsigned long nIndex = 0;
        // どこのデータ間に入るか調べる
        for(unsigned int i = 0; i < m_vXData.size() - 1 ; ++i)
        {
            double x1 = m_vXData[i];
            double x2 = m_vXData[i + 1];
            if((x >= x1 && x <= x2))
            {
                nIndex = i;
                break;
            }
        }

        if (m_vSlope.size() <= nIndex) { return 0; } // ?

        // yを求める（原点を通る直線）
        if(m_vSlope[nIndex] == DBL_MAX)
        {
            // 傾きがMaxDoubleの場合は、Y値をそのまま返す
            return m_vYData[nIndex];
        }
        double y = m_vSlope[nIndex] * x;

        double y1 = m_vSlope[nIndex] * m_vXData[nIndex];
        double diff = m_vYData[nIndex] - y1;
        y += diff;
		return y;
    }

	// Xが昇順にソートしてあるときに限り使用可能
	// 登録してあるXの範囲外のときも、範囲外になる前の傾きを使って外挿が可能
	double CalcForSortX(double x)
	{
		// 傾きを求めるデータがない
		if (m_vXData.size() == 0 || m_vYData.size() == 0) { return 0; }

        unsigned long nIndex = 0;

		// Xデータは昇順で並んでいるはず
		// Xデータ内の最小値未満
		if (x < m_vXData[0])
		{
			// 1点目と2点目の傾きを使う
			nIndex = 0;
		}
		// Xデータ内の最大値より大きい
		else if (x > m_vXData[m_vXData.size() - 1])
		{
			// 最後の一点前と最後の点の傾きを使う
			nIndex = m_vSlope.size() - 1;
		}
		// Xデータ内
		else
		{
			// どこのデータ間に入るか調べる
			for(unsigned int i = 0; i < m_vXData.size() - 1 ; ++i)
			{

				double x1 = m_vXData[i];
				double x2 = m_vXData[i + 1];
				if((x >= x1 && x <= x2))
				{
					nIndex = i;
					break;
				}
			}
		}

        if (m_vSlope.size() <= nIndex) { return 0; } // ?

        // yを求める（原点を通る直線）
        if(m_vSlope[nIndex] == DBL_MAX)
        {
            // 傾きがMaxDoubleの場合は、Y値をそのまま返す
            return m_vYData[nIndex];
        }
        double y = m_vSlope[nIndex] * x;

        double y1 = m_vSlope[nIndex] * m_vXData[nIndex];
        double diff = m_vYData[nIndex] - y1;
        y += diff;
		return y;
    }

private:
    std::deque<double> m_vXData;
    std::deque<double> m_vYData;
    std::deque<double> m_vSlope;
};
