#pragma once
#include <vector>
#include "Interpolate.hpp"
#include <cmath>
//---------------------------------------------------------------------------
class OCHermite : public OCInterpolate
{
public:

    bool Prepare()
    {
        // エルミート補間には4点以上必要
        if (m_N <= 3) { return false; }

        int i;
        double CM1, CM2, CM3, CM4, CM43, CM21, dx;

        // 係数配列領域の確保
        m_C.resize( 5 );
        for ( int i = 1; i < 5; i++ )
        {
            m_C[i].resize( m_N + 1 );
        }

        // C[1]の計算
        for ( i = 0; i < m_N - 1; i++ )
        {
            if ( m_X[i + 1] == m_X[i] ) {
                if ( i == 0 )
                    m_C[1][i] = 0.0;
                else
                    m_C[1][i] = m_C[1][i-1];
            }
            else
                m_C[1][i] = ( m_Y[i + 1] - m_Y[i] ) / ( m_X[i + 1] - m_X[i] );
        }

        // １次係数の作成
        for ( i = 2; i < m_N - 2; i++ )
        {
            CM43 = std::abs( m_C[1][i + 1] - m_C[1][i] );
            CM21 = std::abs( m_C[1][i - 1] - m_C[1][i - 2] );

            if ( CM43 != 0 || CM21 != 0 )
            {
                m_C[2][i] = ( ( CM43 * m_C[1][i - 1] ) + ( CM21 * m_C[1][i] ) ) / ( CM43 + CM21 );
            }
            else
            {
                m_C[2][i] = ( m_C[1][i] + m_C[1][i - 1] ) * 0.5;
            }
        }

        i = m_N - 1;
        CM3 = ( 2.0 * m_C[1][i - 1] ) - m_C[1][i - 2];
        CM4 = ( 3.0 * m_C[1][i - 1] ) - ( 2.0 * m_C[1][i - 2] );
        CM43 = std::abs( CM4 - m_C[1][i] );
        CM21 = std::abs( m_C[1][i - 1] - m_C[1][i - 2] );

        if ( CM43 != 0 || CM21 != 0 )
        {
            m_C[2][i] = ( ( CM43 * m_C[1][i - 1] ) + ( CM21 * CM3 ) ) / ( CM43 + CM21 );
        }
        else
        {
            m_C[2][i] = ( CM3 + m_C[1][i - 1] ) * 0.5;
        }

        i = m_N - 2;
        CM4 = CM3;
        CM43 = std::abs( CM4 - m_C[1][i] );
        CM21 = std::abs( m_C[1][i - 1] - m_C[1][i - 2] );

        if ( CM43 != 0 || CM21 != 0 )
        {
            m_C[2][i] = ( ( CM43 * m_C[1][i - 1] ) + ( CM21 * m_C[1][i] ) ) / ( CM43 + CM21 );
        }
        else
        {
            m_C[2][i] = ( m_C[1][i] + m_C[1][i - 1] ) * 0.5;
        }

        i = 0;
        CM1 = ( 3.0 * m_C[1][i] ) - ( 2.0 * m_C[1][i + 1] );
        CM2 = ( 2.0 * m_C[1][i] ) - m_C[1][i + 1];
        CM43 = std::abs( m_C[1][i + 1] - m_C[1][i] );
        CM21 = std::abs( CM2 - CM1 );

        if ( CM43 != 0 || CM21 != 0 )
        {
            m_C[2][i] = ( ( CM43 * CM2 ) + ( CM21 * m_C[1][i] ) ) / ( CM43 + CM21 );
        }
        else
        {
            m_C[2][i] = ( m_C[1][i] + CM2 ) * 0.5;
        }

        i = 1;
        CM1 = CM2;
        CM43 = std::abs( m_C[1][i + 1] - m_C[1][i] );
        CM21 = std::abs( m_C[1][i - 1] - CM1 );

        if ( CM43 != 0 || CM21 != 0 )
        {
            m_C[2][i] = ( ( CM43 * m_C[1][i - 1] ) + ( CM21 * m_C[1][i] ) ) / ( CM43 + CM21 );
        }
        else
        {
            m_C[2][i] = ( m_C[1][i] + m_C[1][i - 1] ) * 0.5;
        }

        // ２次、３次の係数作成
        for ( i = 0; i < m_N - 1; i++ )
        {
            dx = m_X[i + 1] - m_X[i];

            if ( dx == 0.0 ) {
                if ( i == 0 ) {
                    m_C[3][i] = 0.0;
                    m_C[4][i] = 0.0;
                }
                else {
                    m_C[3][i] = m_C[3][i-1];
                    m_C[4][i] = m_C[4][i-1];
                }
            }
            else {
                m_C[3][i] = ( ( 3 * m_C[1][i] ) - ( 2 * m_C[2][i] ) - m_C[2][i + 1] ) / dx;
                m_C[4][i] = ( m_C[2][i] + m_C[2][i + 1] - ( 2 * m_C[1][i] ) ) / ( dx * dx );
            }
        }
        return true;
    }

    double Calc( double xVal )
    {
        int i;

        for ( i = 0; i < m_N; i++ )
        {
            if ( xVal < m_X[i] )
                break;
        }

        if ( i == 0 )
            return 0.0;

        double h = xVal - m_X[--i];

        return m_Y[i] + ( h * ( m_C[2][i] + ( h * ( m_C[3][i] + ( h * m_C[4][i] ) ) ) ) );
    }

private:
    std::vector<std::vector< double > > m_C;
};
