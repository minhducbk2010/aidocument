//===========================================================================
/*!
	@file
	@author	����
	@date	2004/12/27
 */
//===========================================================================
#pragma once
//---------------------------------------------------------------------------
#define MAX_ORDER (9 + 1)
#define swap(a, i, j, t) t = a[i]; a[i] = a[j]; a[j] = t;
//---------------------------------------------------------------------------
static const double EPS = 1E-6;
//---------------------------------------------------------------------------
struct ST_REGRSLT
{
    long nOrder;
    double b[MAX_ORDER];
    int col[MAX_ORDER];
    double s[MAX_ORDER];
    double t[MAX_ORDER];
};
//---------------------------------------------------------------------------
class OCRegress
{
public:
    OCRegress(){};
    ~OCRegress(){};

    int Execute(int ord, double* srcX, double* srcY, int length,
                double* b, int* col, double* s, double* t)
    {
        int p = ord + 1;
        int n = length;

        double** x = new double*[p + 1];
        for(int i = 0; i < p + 1; i++)
        {
            x[i] = new double[n];
        }

        double* normsq = new double[p];
        double* initnormsq = new double[p];

        for(int i = 0; i < p; i++)
        {
            for(int j = 0; j < n; j++)
            {
                if(i == 0)
                {
                    x[i][j] = 1.0;
                }
                else if(i == 1)
                {
                    x[i][j] = (double)srcX[j];
                }
                else
                {
                    x[i][j] = pow(srcX[j], i);
                }
            }
        }
        for(int i = 0; i < n; i++)
        {
            x[p][i] = srcY[i];
        }

        int r = lsq(n, p, x, b, col, initnormsq, normsq);
        /*double rss = */this->innerproduct(n - r, &x[p][r], &x[p][r]);
        this->invr(r, x);
#if 0
        double tt;
        for(int i = 0; i < r; i++)
        {
            tt = this->innerproduct(r - i, &x[i][i], &x[i][i]);
            s[i] = sqrt(tt * rss / (n - r));
            if(s[i] > 0)
            {
                t[i] = fabs(b[i] / s[i]);
            }
            else
            {
                t[i] = 0;
            }
        }
#endif
        this->Sort(r, b, col, s, t);

        delete[] initnormsq;
        delete[] normsq;
        for(int i = 0; i < p + 1; i++)
        {
            delete[] x[i];
        }
        delete[] x;

        return r;
    }

private:
    double innerproduct(int n, double* u, double* v)
    {
        int i, n5;
        double s;

        s = 0;
        n5 = n % 5;

        for(i = 0; i < n5; i++)
        {
            s += u[i] * v[i];
        }

        for(i = n5; i < n; i += 5)
        {
            s += u[i] * v[i] + u[i + 1] * v[i + 1] + u[i + 2] * v[i + 2]
                + u[i + 3] * v[i + 3] + u[i + 4] * v[i + 4];
        }

        return s;
    }
    int lsq(int n, int m, double** x, double* b,
            int* col, double* initnormsq, double* normsq)
    {
        int i, j, r;
        double s, t, u;
        double* v, * w;

        for(j = 0; j < m; j++)
        {
            col[j] = j;
            normsq[j] = this->innerproduct(n, x[j], x[j]);
            initnormsq[j] = (normsq[j] != 0) ? normsq[j] : -1;
        }

        for(r = 0; r < m; r++)
        {
            if(r != 0)
            {
                j = r;
                u = 0;
                for(i = r; i < m; i++)
                {
                    t = normsq[i] / initnormsq[i];
                    if(t > u)
                    {
                        u = t;
                        j = i;
                    }
                }
                swap(col, j, r, i);
                swap(normsq, j, r, t);
                swap(initnormsq, j, r, t);
                swap(x, j, r, v);
            }
            v = x[r];
            u = this->innerproduct(n - r, &v[r], &v[r]);
            if(u / initnormsq[r] < EPS * EPS)
            {
                break;
            }
            u = sqrt(u);
            if(v[r] < 0)
            {
                u = -u;
            }
            v[r] += u;
            t = 1 / (v[r] * u);
            for(j = r + 1; j <= m; j++)
            {
                w = x[j];
                s = t * this->innerproduct(n - r, &v[r], &w[r]);
                for(i = r; i < n; i++)
                {
                    w[i] -= s * v[i];
                }
                if(j < m)
                {
                    normsq[j] -= w[r] * w[r];
                }
            }
            v[r] = -u;
        }
        for(j = r - 1; j >= 0; j--)
        {
            s = x[m][j];
            for(i = j + 1; i < r; i++)
            {
                s -= x[i][j] * b[i];
            }
            b[j] = s / x[j][j];
        }
        return r;
    }
    void invr(int r, double** x)
    {
        double s;

        for(int k = 0; k < r; k++)
        {
            x[k][k] = 1 / x[k][k];

            for(int j = k - 1; j >= 0; j--)
            {
                s = 0;

                for(int i = j + 1; i <= k; i++)
                {
                    s -= x[i][j] * x[i][k];
                }

                x[j][k] = s * x[j][j];
            }
        }

    }
    void Sort(int r, double* b, int* col, double* s, double* t)
    {
        double dTmp;
        int nTmp;

        for(int i = 0; i < r - 1; i++)
        {
            for(int j = i + 1; j < r; j++)
            {
                if(col[i] > col[j])
                {
                    dTmp = b[i];
                    b[i] = b[j];
                    b[j] = dTmp;

                    nTmp = col[i];
                    col[i] = col[j];
                    col[j] = nTmp;

                    dTmp = s[i];
                    s[i] = s[j];
                    s[j] = dTmp;

                    dTmp = t[i];
                    t[i] = t[j];
                    t[j] = dTmp;
                }
            }
        }
    }
};
#undef swap
//---------------------------------------------------------------------------
