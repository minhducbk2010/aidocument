#pragma once
#include "Interpolate.hpp"
#include <vector>

//---------------------------------------------------------------------------
struct ST_LAGRANGE
{
    double	bunsi;
    double	bunbo;
};

class OCLagrange : public OCInterpolate
{
public:
    bool Prepare()
    {
        // X値の重複を削除（Y値は平均化する）
        m_rX.resize(m_N);
        m_rY.resize(m_N);
        for (int i = 0; i < m_N; i++) {
            m_rX[i] = m_X[i];
            m_rY[i] = m_Y[i];
        }
        m_rN = AverageSameData(&m_rX[0], &m_rY[0], m_N);
        m_rX.resize(m_rN);
        m_rY.resize(m_rN);
        m_L.resize(m_rN);

        // Lagrange補間多項式の分母を計算
        for (int i = 0; i < m_rN; i++) {
            m_L[i].bunbo = 1.0;
            for (int j = 0; j < m_rN; j++) {
                if (i != j) { m_L[i].bunbo *= m_rX[i] - m_rX[j]; }
            }
            // オーバーフローの判定
            if (!((-DBL_MAX <= m_L[i].bunbo && m_L[i].bunbo <= -DBL_MIN) ||
                  (DBL_MIN <= m_L[i].bunbo && m_L[i].bunbo <= DBL_MAX))) {
                return false;
            }
        }

        return true;
    }

    double Calc(double xVal)
    {
        // Lagrange補間多項式の分子を計算
        double	product = 1.0;
        for (int i = 0; i < m_rN; i++) {
            if (xVal == m_rX[i]) { return (m_rY[i]); }
            product *= xVal - m_rX[i];
        }
        for (int i = 0; i < m_rN; i++) {
            m_L[i].bunsi = product / (xVal - m_rX[i]);
        }

        // 補間値を計算
        double	yVal = 0.0;
        for (int i = 0; i < m_rN; i++) {
            yVal += m_rY[i] * m_L[i].bunsi / m_L[i].bunbo;
        }

        return(yVal);
    }


private:
    // X値の重複を省いた標本値とその標本数
    std::vector<double>		m_rX;
    std::vector<double>		m_rY;
    int						m_rN;

    // ラグランジュ補間多項式
    std::vector<ST_LAGRANGE> m_L;
};
