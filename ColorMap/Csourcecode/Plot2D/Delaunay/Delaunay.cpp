//===========================================================================
/*!
	@file
	@author	小島
	@date	2005/03/18

	@class	ODelaunay	ODelaunay.h
*/
//===========================================================================
#include "stdafx.h"
#include <float.h>
#include "Delaunay.h"
#include "dDelaunay.h"
//---------------------------------------------------------------------------

//===========================================================================
//!	Constructor
//===========================================================================
ODelaunay::ODelaunay() : m_pDelaunay(0), m_stDTri(0),
	m_nTriCount(0), m_nIndividual(0)
{
	m_fXdata = 0;
    m_fYdata = 0;
    m_fZdata = 0;
    m_nDtCount = 0;

    m_fOrgX = 0;
    m_fOrgY = 0;
    m_fOrgZ = 0;
}

//===========================================================================
//!	Destructor
//===========================================================================
ODelaunay::~ODelaunay()
{
	if(m_pDelaunay)
    {
    	delete m_pDelaunay;
    }

    if(m_fXdata)
    {
    	delete []m_fXdata;
    }
    if(m_fYdata)
    {
    	delete []m_fYdata;
    }
    if(m_fZdata)
    {
    	delete []m_fZdata;
    }

    if(m_stDTri)
    {
    	delete []m_stDTri;
    }

    if(m_fOrgX)
    {
    	delete []m_fOrgX;
    }
    if(m_fOrgY)
    {
    	delete []m_fOrgY;
    }
    if(m_fOrgZ)
    {
    	delete []m_fOrgZ;
    }
}

//===========================================================================
//===========================================================================
void ODelaunay::Init(const float* x, const float* y,
	const float* z, unsigned int n)
{
	if(m_fXdata)
    {
    	delete []m_fXdata;
    }
    if(m_fYdata)
    {
    	delete []m_fYdata;
    }
    if(m_fZdata)
    {
    	delete []m_fZdata;
    }

    m_nDtCount = n;
    m_fXdata = new double[n];
    m_fYdata = new double[n];
    m_fZdata = new double[n];

    if(m_fOrgX)
    {
    	delete []m_fOrgX;
    }
    if(m_fOrgY)
    {
    	delete []m_fOrgY;
    }
    if(m_fOrgZ)
    {
    	delete []m_fOrgZ;
    }

    m_fOrgX = new double[n];
    m_fOrgY = new double[n];
    m_fOrgZ = new double[n];

    unsigned int i;
    for(i = 0; i < n; i++)
    {
    	m_fXdata[i] = m_fOrgX[i] = x[i];
        m_fYdata[i] = m_fOrgY[i] = y[i];
        m_fZdata[i] = m_fOrgZ[i] = z[i];
    }
}

//===========================================================================
//===========================================================================
void ODelaunay::Prepare()
{
	if(m_pDelaunay)
	{
		delete m_pDelaunay;
	}

	m_pDelaunay = new dDelaunay;
	//m_pDelaunay->InputData(m_fXdata, m_fYdata, m_fZdata, m_nDtCount,
	//	m_nIndividual);
	m_pDelaunay->Calculation();

	m_nTriCount = m_pDelaunay->GetTrianlgeCount();

	if(m_stDTri)
	{
		delete []m_stDTri;
	}

	m_stDTri = new stDelaunayTriangle[m_nTriCount];
	m_pDelaunay->GetResult((stDelaunayTriangle*)m_stDTri);
}

//===========================================================================
//===========================================================================
double ODelaunay::Calc(double x, double y)
{
	unsigned int i, j, pn;
    dPoint Tp[3];
    dPoint p;
    double z;

    p.x = x;
    p.y = y;

    stDelaunayTriangle* pstDTri;

    for(i = 0; i < m_nTriCount; i++)
    {
    	pstDTri = (stDelaunayTriangle*)m_stDTri;

    	for(j = 0; j < 3; j++)
        {
        	switch(j)
            {
            case 0:
            	pn = pstDTri[i].p1;
                break;
            case 1:
            	pn = pstDTri[i].p2;
                break;
            case 2:
            	pn = pstDTri[i].p3;
            }

            Tp[j].n = pn;
            Tp[j].x = m_fOrgX[pn];
            Tp[j].y = m_fOrgY[pn];
            Tp[j].z = m_fOrgZ[pn];
        }

		if(this->IsInvolving(&Tp[0], &Tp[1], &Tp[2], &p) <= 0)
        {
        	break;
        }
    }

    if(i >= m_nTriCount)
    {
    	return DBL_MAX;
    }
    else
    {
    	z = this->CalcPlane(&Tp[0], &Tp[1], &Tp[2], &p);
    }

    return z;
}

//===========================================================================
//!	内包判定
/*!
	指定の点が三角形内に含まれるかどうか調べます.
	@param p	判定店
	@retval 0以下	点は三角形内にある
	@retval 0以上	点は三角形外にある
	@retval 0		点は三角形辺上にある
*/
//===========================================================================
double ODelaunay::IsInvolving(const void* p1, const void* p2,
	const void* p3, const void* p)
{
	double min, max;
	double a, b, c, r;

    dPoint tmpP[4];
    tmpP[0] = *((dPoint*)p1);
    tmpP[1] = *((dPoint*)p2);
    tmpP[2] = *((dPoint*)p3);
    tmpP[3] = *((dPoint*)p);

    double XMax, XMin, YMax, YMin;

    XMax = tmpP[0].x;
    XMin = tmpP[0].x;
    YMax = tmpP[0].y;
    YMin = tmpP[0].y;

    unsigned int i;
    for(i = 1; i < 4; i++)
    {
    	if(XMax < tmpP[i].x)
        {
        	XMax = tmpP[i].x;
        }
        if(XMin > tmpP[i].x)
        {
        	XMin = tmpP[i].x;
        }
        if(YMax < tmpP[i].y)
        {
        	YMax = tmpP[i].y;
        }
        if(YMin > tmpP[i].y)
        {
        	YMin = tmpP[i].y;
        }
    }

    for(i = 0; i < 4; i++)
    {
    	tmpP[i].x = tmpP[i].x / (XMax - XMin) * 2.0 - 1.0;
        tmpP[i].y = tmpP[i].y / (YMax - YMin) * 2.0 - 1.0;
    }

    // 頂点X座標の最小値と最大値を求める
    if(tmpP[0].x < tmpP[1].x)
    {
    	min = tmpP[0].x;
        max = tmpP[1].x;
    }
    else
    {
    	min = tmpP[1].x;
        max = tmpP[0].x;
    }
    if(tmpP[2].x < min)
    {
    	min = tmpP[2].x;
    }
    if(tmpP[2].x > max)
    {
    	max = tmpP[2].x;
    }

	r = 1.0;

    // 点が頂点X座標の最小値と最大値内
    if(tmpP[3].x >= min && tmpP[3].x <= max)
    {
    	// 頂点Y座標の最小値と最大値を求める
        if(tmpP[0].y < tmpP[1].y)
        {
        	min = tmpP[0].y;
            max = tmpP[1].y;
        }
        else
        {
        	min = tmpP[1].y;
            max = tmpP[0].y;
        }
        if(tmpP[2].y < min)
        {
        	min = tmpP[2].y;
        }
        if(tmpP[2].y > max)
        {
        	max = tmpP[2].y;
        }

		// 点が頂点Y座標の最小値と最大値内
		if(tmpP[3].y >= min && tmpP[3].y <= max)
		{
        	// 点は各辺に対して常に同じ方向になければならない
			a = (tmpP[0].x - tmpP[3].x) * (tmpP[1].y - tmpP[3].y) -
				(tmpP[0].y - tmpP[3].y) * (tmpP[1].x - tmpP[3].x);

			b = (tmpP[1].x - tmpP[3].x) * (tmpP[2].y - tmpP[3].y) -
				(tmpP[1].y - tmpP[3].y) * (tmpP[2].x - tmpP[3].x);

			c = (tmpP[2].x - tmpP[3].x) * (tmpP[0].y - tmpP[3].y) -
				(tmpP[2].y - tmpP[3].y) * (tmpP[0].x - tmpP[3].x);

			if(a <= 0.0 && b <= 0.0 && c <= 0.0)
			{
				r = -1.0;
			}
		}
	}

	return r;
}

//===========================================================================
//===========================================================================
double ODelaunay::CalcPlane(const void* p1, const void* p2,
	const void* p3, const void* p)
{
	dPoint tmpP[4];
    tmpP[0] = *((dPoint*)p1);
    tmpP[1] = *((dPoint*)p2);
    tmpP[2] = *((dPoint*)p3);
    tmpP[3] = *((dPoint*)p);

    double a, b, c;
    a = (tmpP[1].y - tmpP[0].y) * (tmpP[2].z - tmpP[0].z) -
        (tmpP[2].y - tmpP[0].y) * (tmpP[1].z - tmpP[0].z);
    b = (tmpP[1].z - tmpP[0].z) * (tmpP[2].x - tmpP[0].x) -
        (tmpP[2].z - tmpP[0].z) * (tmpP[1].x - tmpP[0].x);
    c = (tmpP[1].x - tmpP[0].x) * (tmpP[2].y - tmpP[0].y) -
        (tmpP[2].x - tmpP[0].x) * (tmpP[1].y - tmpP[0].y);

    double z;
    if(c != 0)
    {
    	z = -(a / c * tmpP[3].x + b / c * tmpP[3].y -
            (a * tmpP[0].x + b * tmpP[0].y + c * tmpP[0].z) / c);
    }
    else
    {
    	z = DBL_MAX;
    }

    return z;
}

//===========================================================================
//===========================================================================
int ODelaunay::SetParam(UINT Type, WPARAM /*wParam*/, LPARAM lParam)
{
	int ret = -1;

	switch(Type)
	{
	case DLN_NORMINDV:
		m_nIndividual = (int)lParam;
		ret = Type;
	}

	return ret;
}
