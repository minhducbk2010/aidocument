//===========================================================================
/*!
	@file
	@author	小島
	@date	2005/03/18
*/
//===========================================================================
#pragma once
//---------------------------------------------------------------------------
class dDelaunay;
//---------------------------------------------------------------------------
// Parameter Type
// Delaunay Triangulation
#define DLN_NORMINDV		0x00000001
//---------------------------------------------------------------------------
class ODelaunay
{
public:
	ODelaunay();
	virtual ~ODelaunay();

	void Init(const float* x, const float* y, const float* z, unsigned int n);
	void Prepare();
	double Calc(double x, double y);

	int SetParam(UINT Type, WPARAM wParam, LPARAM lParam);

private:
	unsigned int m_nDtCount;
	double* m_fXdata;
	double* m_fYdata;
	double* m_fZdata;

	dDelaunay* m_pDelaunay;

	double* m_fOrgX;
	double* m_fOrgY;
	double* m_fOrgZ;

	unsigned int m_nTriCount;
	void* m_stDTri;

	int m_nIndividual;

	double __fastcall IsInvolving(const void* p1, const void* p2,
    	const void* p3, const void* p);
	double __fastcall CalcPlane(const void* p1, const void* p2,
    	const void* p3, const void* p);
};


struct OUTSIDE
{
	int		nRow;
	int		nStartPos;
	int		nEndPos;
	double	dStartDat;
	int		nStartPos2;
	int		nEndPos2;
	double	dEndDat;
};
inline int Extrapolation(double	*pData,
								 int	XDim,
								 int	YDim,
								 double	*pExtrapolation,
								 double	*pEcuZ)
{
	int nRetVal = 0, nflg = 0, nflg2 = 0;
	OUTSIDE Outside;
	std::vector<OUTSIDE>	m_vOutside;

// 外挿補填するセルがまったく無かった時、
// 元の外挿属性をそのまま維持するために、
// 表示属性のクリアは中止する
/*
	// 表示属性のクリア
	for ( int i = 0; i < (XDim * YDim); i++ ) {
		pExtrapolation[i] = 0;
	}
*/

	// クリア
	m_vOutside.clear();

	// X軸方向のポジションとデータ
	for (int ARow = 0; ARow < YDim; ARow++) {
		nflg = 0;
		nflg2 = 0;
		Outside.nRow = ARow;
		Outside.nStartPos = -1;
		Outside.nEndPos = -1;
		Outside.dStartDat = -1;
		Outside.nStartPos2 = -1;
		Outside.nEndPos2 = -1;
		Outside.dEndDat = -1;

		for (int ACol = 0; ACol < XDim; ACol++) {
			// 左側の最初のセルが空の時
			// 左側開始セルポジションの設定
			if ( nflg == 0 && (pData[ACol * YDim + ARow] == DBL_MAX) ) {
				nflg = 1;
				Outside.nStartPos = ACol;
			}
			// 左側の最初のセルがデータ有りの時
			// 左側セルの検出終わり
			else if ( nflg == 0 && (pData[ACol * YDim + ARow] != DBL_MAX) ) {
				nflg = 4;
			}
			// 左側の空データを検出してから、初めてデータ有りのセルになった時
			// 左側終了ポジションの設定
			// 左側補填データの設定
			else if ( nflg == 1 && (pData[ACol * YDim + ARow] != DBL_MAX) ) {
				nflg = 2;
				Outside.nEndPos = ACol - 1;
				Outside.dStartDat = pData[ACol * YDim + ARow];
			}
			// 左側セルの検出が終わってから、右側のセルで初めて空セルになった時
			// 右側開始セルポジションの設定
			// 右側補填データの設定
			else if ( nflg == 2 && (pData[ACol * YDim + ARow] == DBL_MAX) ) {
				nflg = 3;
				Outside.nStartPos2 = ACol;
				Outside.dEndDat = pData[(ACol - 1) * YDim + ARow];
			}
			// 右側終了ポジションの設定
			// 最後のセルになるまで、入れ換える
			else if ( nflg == 3 ) {
				if ( pData[ACol * YDim + ARow] != DBL_MAX ) {			// 登録の取り消し
					nflg = 5;
					Outside.nStartPos2 = -1;
					Outside.nEndPos2 = -1;
					Outside.dEndDat = -1;
				}
				else {													// 続ける
					Outside.nEndPos2 = ACol;
				}
			}
			// 左側に空セルがなくて、右側で初めて空セルを検出した時
			// 右側開始セルポジションの設定
			// 右側補填データの設定
			else if ( nflg == 4 && (pData[ACol * YDim + ARow] == DBL_MAX) ) {
				nflg = 5;
				Outside.nStartPos2 = ACol;
				Outside.dEndDat = pData[(ACol - 1) * YDim + ARow];
			}
			// 左側に空セルがが無い時の右側終了ポジションの設定
			// 最後のセルになるまで、入れ換える
			else if ( nflg == 5 ) {
				if ( pData[ACol * YDim + ARow] != DBL_MAX ) {			// 登録の取り消し
					nflg2 = 1;
					Outside.nStartPos2 = -1;
					Outside.nEndPos2 = -1;
					Outside.dEndDat = -1;
				}
				else {													// 続ける
					if ( nflg2 == 1 ) {
						nflg2 = 0;
						Outside.nStartPos2 = ACol;
						Outside.nEndPos2 = ACol;
						Outside.dEndDat = pData[(ACol - 1) * YDim + ARow];
					}
					else {
						Outside.nEndPos2 = ACol;
					}
				}
			}
		}

		// 一つの行の補填データ情報の設定
		if ( Outside.nStartPos2 != -1 && Outside.nEndPos2 == -1) {
			Outside.nEndPos2 = XDim - 1;
		}
		m_vOutside.push_back(Outside);
	}

	// X軸方向に外挿を埋める
	int nSize = (int)m_vOutside.size();
	for ( int i = 0; i < nSize; i++ ) {
		// 左側の空きセルを Outside.dStartDat で埋める
		if ( m_vOutside[i].nStartPos != -1 && m_vOutside[i].nEndPos != -1 ) {
			int ARow = m_vOutside[i].nRow;
			for ( int ACol = m_vOutside[i].nStartPos; ACol <= m_vOutside[i].nEndPos; ACol++ ) {
				pExtrapolation[ACol * YDim + ARow] = 2;
				pData[ACol * YDim + ARow] = m_vOutside[i].dStartDat;
			}
		}

		// 右側の空きセルを Outside.dEndDat 埋める
		if ( m_vOutside[i].nStartPos2 != -1 && m_vOutside[i].nEndPos2 != -1 ) {
			int ARow = m_vOutside[i].nRow;
			for ( int ACol = m_vOutside[i].nStartPos2; ACol <= m_vOutside[i].nEndPos2; ACol++ ) {
				pExtrapolation[ACol * YDim + ARow] = 2;
				pData[ACol * YDim + ARow] = m_vOutside[i].dEndDat;
			}
		}
	}

	// クリア
	m_vOutside.clear();

	// Y軸方向のポジションとデータ
	for (int ACol = 0; ACol < XDim; ACol++) {
		nflg = 0;
		nflg2 = 0;
		Outside.nRow = ACol;
		Outside.nStartPos = -1;
		Outside.nEndPos = -1;
		Outside.dStartDat = -1;
		Outside.nStartPos2 = -1;
		Outside.nEndPos2 = -1;
		Outside.dEndDat = -1;

		for (int ARow = 0; ARow < YDim; ARow++) {
			// 上側の最初のセルが空の時
			// 上側開始セルポジションの設定
			if ( nflg == 0 && (pData[ACol * YDim + ARow] == DBL_MAX) ) {
				nflg = 1;
				Outside.nStartPos = ARow;
			}
			// 上側の最初のセルがデータ有りの時
			// 上側セルの検出終わり
			else if ( nflg == 0 && (pData[ACol * YDim + ARow] != DBL_MAX) ) {
				nflg = 4;
			}
			// 上側の空データを検出してから、初めてデータ有りのセルになった時
			// 上側終了ポジションの設定
			// 上側補填データの設定
			else if ( nflg == 1 && (pData[ACol * YDim + ARow] != DBL_MAX) ) {
				nflg = 2;
				Outside.nEndPos = ARow - 1;
				Outside.dStartDat = pData[ACol * YDim + ARow];
			}
			// 上側セルの検出が終わってから、下側のセルで初めて空セルになった時
			// 下側開始セルポジションの設定
			// 下側補填データの設定
			else if ( nflg == 2 && (pData[ACol * YDim + ARow] == DBL_MAX) ) {
				nflg = 3;
				Outside.nStartPos2 = ARow;
				Outside.dEndDat = pData[ACol * YDim + ARow - 1];
			}
			// 下側終了ポジションの設定
			// 最後のセルになるまで、入れ換える
			else if ( nflg == 3 ) {
				if ( pData[ACol * YDim + ARow] != DBL_MAX ) {			// 登録の取り消し
					nflg = 5;
					Outside.nStartPos2 = -1;
					Outside.nEndPos2 = -1;
					Outside.dEndDat = -1;
				}
				else {													// 続ける
					Outside.nEndPos2 = ARow;
				}
			}
			// 上側に空セルがなくて、下側で初めて空セルを検出した時
			// 下側開始セルポジションの設定
			// 下側補填データの設定
			else if ( nflg == 4 && (pData[ACol * YDim + ARow] == DBL_MAX) ) {
				nflg = 5;
				Outside.nStartPos2 = ARow;
				Outside.dEndDat = pData[ACol * YDim + ARow - 1];
			}
			// 上側に空セルがが無い時の下側終了ポジションの設定
			// 最後のセルになるまで、入れ換える
			else if ( nflg == 5 ) {
				if ( pData[ACol * YDim + ARow] != DBL_MAX ) {			// 登録の取り消し
					nflg2 = 1;
					Outside.nStartPos2 = -1;
					Outside.nEndPos2 = -1;
					Outside.dEndDat = -1;
				}
				else {													// 続ける
					if ( nflg2 == 1 ) {
						nflg2 = 0;
						Outside.nStartPos2 = ARow;
						Outside.nEndPos2 = ARow;
						Outside.dEndDat = pData[ACol * YDim + ARow - 1];
					}
					else {
						Outside.nEndPos2 = ARow;
					}
				}
			}
		}

		// 一つの列の補填データ情報の設定
		if ( Outside.nStartPos2 != -1 && Outside.nEndPos2 == -1) {
			Outside.nEndPos2 = YDim - 1;
		}
		m_vOutside.push_back(Outside);
	}

	// Y軸方向に外挿を埋める
	nSize = (int)m_vOutside.size();
	for ( int i = 0; i < nSize; i++ ) {
		// 上側の空きセルを Outside.dStartDat 埋める
		if ( m_vOutside[i].nStartPos != -1 && m_vOutside[i].nEndPos != -1 ) {
			int ACol = m_vOutside[i].nRow;
			for ( int ARow = m_vOutside[i].nStartPos; ARow <= m_vOutside[i].nEndPos; ARow++ ) {
				pExtrapolation[ACol * YDim + ARow] = 2;
				pData[ACol * YDim + ARow] = m_vOutside[i].dStartDat;
			}
		}

		// 下側の空きセルを Outside.dEndDat 埋める
		if ( m_vOutside[i].nStartPos2 != -1 && m_vOutside[i].nEndPos2 != -1 ) {
			int ACol = m_vOutside[i].nRow;
			for ( int ARow = m_vOutside[i].nStartPos2; ARow <= m_vOutside[i].nEndPos2; ARow++ ) {
				pExtrapolation[ACol * YDim + ARow] = 2;
				pData[ACol * YDim + ARow] = m_vOutside[i].dEndDat;
			}
		}
	}

	// 戻り値の設定
	for ( int i = 0; i < (XDim * YDim); i++ ) {
		pEcuZ[i] = pData[i];
	}

	// 戻り値
	return ( nRetVal );

}