//===========================================================================
/*!
	@file
	@author	����
	@date	2005/02/24
*/
//===========================================================================
#ifndef dDelaunayDataH
#define dDelaunayDataH
//---------------------------------------------------------------------------
typedef struct
{
	unsigned int n;
	double x;
	double y;
	double z;
}
dPoint;
//---------------------------------------------------------------------------
class dTri
{
private:
	double CheckCw(const dPoint* p1, const dPoint* p2, const dPoint* p3);
	void SortVertex();

public:
	dPoint* Pt[3];	//!< ���_
	dTri* Ad[3];	//!< �אڎO�p�`

	dTri(dPoint* p1, dPoint* p2, dPoint* p3);

	int SearchVertex(const dPoint* p);
	double IsInvolving(const dPoint* p);
};
//---------------------------------------------------------------------------
#endif
