//===========================================================================
/*!
	@file
	@author	����
	@date	2005/02/24
*/
//===========================================================================
#pragma once
//---------------------------------------------------------------------------
typedef struct
{
	unsigned int p1;
	unsigned int p2;
	unsigned int p3;
}
stDelaunayTriangle;
//---------------------------------------------------------------------------
#include <vector>
#include <stack>
//---------------------------------------------------------------------------
#include "dDelaunayData.h"
//---------------------------------------------------------------------------
class dDelaunay
{
private:
	std::vector<dPoint*> vP;
	std::vector<dTri*> vT;

	std::stack<dTri*> vStack;

	double xRatio, yRatio;

	double CheckCw(const dPoint* p1, const dPoint* p2, const dPoint* p3);
	double GetDistance(const dPoint* p1, const dPoint* p2);
	int GetSharedVertex(const dTri* t1, const dTri* t2, dPoint** sp1,
    	dPoint** sp2, dPoint** ns1, dPoint** ns2);
	int DivTriangle(dTri* t1, dPoint* p, dTri** t2, dTri** t3);
	int DivDoubleTriangle(dTri* t1, dPoint* p, dTri** t2, dTri** t3,
    	dTri** t4);
	int CompDiagonal(const dTri* t1, const dTri* t2);
	int SwapDiagonal(dTri* t1, dTri* t2);

	void Init();
	void CreateInitTri();

	void Normalize(double* x, double* y, double* z, unsigned int n,
    	int Individual);

public:
	dDelaunay();
	~dDelaunay();

	void InputData(double* x, double* y, double* z, unsigned int n,
    	int Individual = 0);
	void Calculation();

	unsigned int GetPointCount();
	unsigned int GetTrianlgeCount();

	int GetResult(stDelaunayTriangle* t);

	double GetXRatio(){ return xRatio; };
	double GetYRatio(){ return yRatio; };
};
