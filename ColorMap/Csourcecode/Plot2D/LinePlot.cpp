/*!	@file
	@date	2008-04-01
	@author	wada

	@brief	折れ線グラフの実装
*/
#include "StdAfx.h"
#include <boost/algorithm/minmax_element.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/scoped_array.hpp>
#include <boost/foreach.hpp>
#include "Message.h"
#include "IMessageReceiver.h"
#include "ICanvas.h"
#include "IPlotData.h"
#include "ISerializer.h"
#include "property.h"
#include "propertycreator.hpp"
#include "PropertyHelper.hpp"
#include "LinePlot.h"
#include "Interpolate/LineInterpolate.hpp"
#include "RectClipping.h"
#include "AxisEnum.h"

// 邪魔なので消す
#undef min
#undef max

///////////////////////
class LineWrapper
{
public:
	LineWrapper(IPlotData* data, bool interpolate, bool accumulate) : m_data(data)
	{
		std::vector<OCPoint> tmp;
		float y = 0;
		for (size_t i = 0, n = data->GetDataCount(); i < n; ++i) {
			float const x = data->GetData(0, i);
			float const yTmp = data->GetData(1, i);
			if (x == FLT_MAX || yTmp == FLT_MAX) {
				if (!interpolate) {
					// 欠損値の補間をしない場合無効値のある部分で分割する
					if (!tmp.empty()) {
						m_src.push_back(tmp);
						tmp.clear();
					}
				}
			}
			else {
				y = accumulate ? y + yTmp : yTmp;
				tmp.push_back(OCPoint(x, y));
			}
		}
		if (!tmp.empty()) {
			m_src.push_back(tmp);
		}
	}

	bool Calc(OCRect const& rect, OCPoint const& target, OCPlotImpl::SearchValue* ret)
	{
		DrawArgs arg;
		arg.xMax = m_data->GetMaximum(0);
		arg.xMin = m_data->GetMinimum(0);
		arg.yMax = m_data->GetMaximum(1);
		arg.yMin = m_data->GetMinimum(1);

		OCLineInterpolate inter;
		std::vector<OCPlotImpl::SearchValue> pass;

		double tx[2], ty[2];
		for (size_t i = 0; i < m_src.size(); ++i) {
			for (size_t j = 0; j < m_src[i].size() - 1; ++j) {
				tx[0] = m_src[i][j].x;
				tx[1] = m_src[i][j + 1].x;
				ty[0] = m_src[i][j].y;
				ty[1] = m_src[i][j + 1].y;

				// 指定されたXの値を含む区間である時のみ、Yを求める
				if (target.x >= tx[0] && target.x < tx[1] ||
					target.x <= tx[0] && target.x >= tx[1])
				{
					inter.Init(2, tx, ty);

					double const x = target.x;
					double const y = inter.Calc(x);
					double const xPos = rect.Width() * arg.XNormalize(x) + rect.left;
					double const yPos = rect.Height() - rect.Height() * arg.YNormalize(y) + rect.top;

					OCPlotImpl::SearchValue sv = {OCPoint(xPos, yPos), OCPoint3D(x, y, -1)};
					pass.push_back(sv);
				}
			}
		}

		if (pass.empty()) { return false; }

		// 指定されたYの値と一番近いところを探す
		*ret = pass[0];
		double interval = std::abs(ret->value.y - target.y);
		for (size_t i = 1; i < pass.size(); ++i) {
			double const diff = std::abs(pass[i].value.y - target.y);
			if (interval > diff) {
				*ret = pass[i];
				interval = diff;
			}
		}
		return true;
	}

//private:
	IPlotData* m_data;
	std::vector<std::vector<OCPoint>> m_src;
};
//////////////////////////////////////

//! Constructor
OCLinePlot::OCLinePlot(HWND hWnd, IMessageReceiver* plot)
	: OCPlotImpl(hWnd, plot), m_filling(false), m_fillTransparent(0)
	, m_interpolate(true)
{
}

//! Destructor
OCLinePlot::~OCLinePlot()
{
}

struct sort_xy_pair
{
	bool operator()(std::pair<float, float> const& lhs, std::pair<float, float> const& rhs)
	{
		return lhs.first < rhs.first;
	}
};

// [begin, end] の線が (X, Y) を通るかを調べます
bool PtInLine(OCPoint begin, OCPoint end, double X, double Y, double range)
{
    OCPoint tpnt[2] = { begin, end };

    double sx = tpnt[0].x;
    double sy = tpnt[0].y;
    double ex = tpnt[1].x;
    double ey = tpnt[1].y;
    double tx = X;
    double ty = Y;
    double maxY, minY;
    double maxX, minX;

    if(IsZero(ex - sx))
    {
		maxY = std::max<double>(sy, ey);
        minY = std::min<double>(sy, ey);
        if(ty >= minY && ty <= maxY)
        {
            if(sx - tx <= range && sx - tx >= -range)
            {
                return(true);
            }
        }
    }
    else if(IsZero(ey - sy))
    {
        maxX = std::max<double>(sx, ex);
        minX = std::min<double>(sx, ex);
        if(tx >= minX && tx <= maxX)
        {
            if (sy - ty <= range && sy - ty >= -range)
            {
                return(true);
            }
        }
    }
    else
    {
        // ライン y = ax + b
        double a = (ey - sy) / (ex - sx);
        double b = (sy - sx * a);

        // ラインと垂直且つクリックポイントを通る直線とラインの交点を求める
        double crossx = (ty + tx / a - b) / (a + 1 / a);
        double crossy = a * crossx + b;

        // クリックポイントと交点の距離を求める
        double d = sqrt( pow( (tx - crossx), 2) + pow( (ty - crossy), 2) );

        maxX = std::max<double>(sx, ex);
        minX = std::min<double>(sx, ex);
        maxY = std::max<double>(sy, ey);
        minY = std::min<double>(sy, ey);

        if((crossx >= minX && crossx <= maxX) && (crossy >= minY && crossy <= maxY))
        {
            if(d <= range)
            {   // クリックポイントと交点の距離が１以内ならヒット
                return true;
            }
        }
    }
	return false;
}

//! 当たり判定
bool OCLinePlot::HitTest(OCRect const& rect, DrawArgs const& arg, double x, double y)
{
	if (m_data->GetDataCount() == 0) { return false; }

	PointArray arr;
	GetDrawPoints(arr, rect, arg);

	BOOST_FOREACH(std::vector<OCPoint> const& pts, arr) {
		if (m_filling) {
			std::vector<POINT> px;
			for (size_t i = 0; i < pts.size(); ++i) {
				POINT p = {MMToPx(pts[i].x), MMToPx(pts[i].y)};
				px.push_back(p);
			}

			HRGN rgn = ::CreatePolygonRgn(&px[0], static_cast<int>(px.size()), WINDING);
			BOOL ret = ::PtInRegion(rgn, MMToPx(x), MMToPx(y));
			::DeleteObject(rgn);
			if (ret) {
				return true;
			} // falseであれば次に進む
		}
		else {
			for (size_t i = 0; i < pts.size() - 1; ++i) {
				if (PtInLine(pts[i], pts[i + 1], x, y, 2)) { return true; }
			}
		}
	}

	return false;
}

/*!	折れ線図を描画します
	@p
	パフォーマンスを稼ぐために描画時間引き処理を行っています
	ただし、横軸データがソートされていないと綺麗に間引かれないため、
	データによってはパフォーマンスが低下する事があります
*/
void OCLinePlot::Draw(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const
{
	DrawImpl(false, g, rect, arg);
}

void OCLinePlot::DrawSelected(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const
{
	if (m_selected) {
		DrawImpl(true, g, rect, arg);
	}
}

void OCLinePlot::DrawImpl(bool selected, ICanvas* g, OCRect const& rect, DrawArgs const& arg) const
{
	if (!IsVisible()) { return; }
	if (!m_data->GetDataCount()) { return; }


	RectClipping clipper(g, m_clippingType, rect);
	g->SetPen(m_lineColor, m_lineWidth, m_lineStyle, m_lineRoundType);
	g->SetBackground(m_fillColor, FS_SOLID, m_fillTransparent);

	// 線分描画
//	if (!m_interpolate) {
		PointArray arr;
		GetDrawPoints(arr, rect, arg);
		for (size_t i = 0; i < arr.size(); ++i) {
			std::vector<OCPoint> const& pts = arr[i];
			if (selected) {
				g->SetPen(RGB(0, 255, 0), 1, PS_DOT, 1);
				g->DrawRubberLines(&pts[0], pts.size());
			}
			else {
				if (m_filling) {
					g->FillPolygon(&pts[0], pts.size());
				}
				g->DrawLines(&pts[0], pts.size());
			}
		}
	//}
	//else {
	//	std::vector<OCPoint> pts;
	//	GetDrawPoints2(pts, rect, arg);
	//	if (selected) {
	//		g->SetPen(RGB(0, 255, 0), 1, PS_DOT);
	//		g->DrawRubberLines(&pts[0], pts.size());
	//	}
	//	else {
	//		if (m_filling) {
	//			g->FillPolygon(&pts[0], pts.size());
	//		}
	//		g->DrawLines(&pts[0], pts.size());
	//	}
	//}
}

void OCLinePlot::GetDrawPoints2(std::vector<OCPoint>& pts, OCRect const& rect, DrawArgs const& arg) const
{
	stop_watch sw(_T("OCLinePlot::GetDrawPoints2"));
	double const yMax = arg.yMax;
	double const yMin = arg.yMin;

	pts.reserve(m_data->GetDataCount());

	float yValue = 0;
	for (size_t i = 0; i < m_data->GetDataCount(); ++i) {
		float const xv = m_data->GetRealData(0, (int)i);
		float const yv = m_data->GetData(1, (int)i);

		if (xv == FLT_MAX) { continue; }

		double const xNorm = arg.XNormalize(xv);
		double const x = rect.Width() * xNorm + rect.left;

		if (yv == FLT_MAX) {
			continue;
		}

		yValue = m_accumulative ? yValue + yv : yv;
		double yNorm = arg.yReverse ? 1 - (yValue - yMin) / (yMax - yMin) : (yValue - yMin) / (yMax - yMin);
		double y = rect.Height() - rect.Height() * yNorm + rect.top;

		pts.push_back(OCPoint(x, y));
	}
}

//! 描画に使用する座標を作成・取得します
/*!
	@param[out]	arr		描画座標
	@param[in]	rect	折れ線図の矩形座標
	@param[in]	arg		描画に必要な諸設定
*/
void OCLinePlot::GetDrawPoints(PointArray& arr, OCRect const& rect, DrawArgs const& arg) const
{
	if (m_plot->SendMessage(PI_GET_PLOT_SORT, 0, 0)) {
		GetCompressedDrawPoints(arr, rect, arg);
	}
	else {
		GetNormalDrawPoints(arr, rect, arg);
	}

	// 塗り潰すときは閉じておく
	if (m_filling) {
		for (size_t i = 0; i < arr.size(); ++i) {
			std::vector<OCPoint>& pts = arr[i];
			pts.insert(pts.begin(), OCPoint(pts.front().x, rect.bottom));
			pts.push_back(OCPoint(pts.back().x, rect.bottom));
		}
	}
}

//! 描画に使用する座標を作成・取得します
/*!
	描画時間引きを行います
	ここで行う間引き処理はデータがX方向にソートされていないと効果がありません
*/
void OCLinePlot::GetCompressedDrawPoints(PointArray& arr, OCRect const& rect, DrawArgs const& arg) const
{
	double const yMax = arg.yMax;
	double const yMin = arg.yMin;

	std::vector<OCPoint> pts, temp;
	temp.reserve(m_data->GetDataCount());

	double prevX = 0, threshold = PxToMM(0.6);
	bool first = true;
	float yValue = 0;
	for (size_t i = 0; i < m_data->GetDataCount(); ++i) {
		float const xv = m_data->GetRealData(0, (int)i);
		float const yv = m_data->GetData(1, (int)i);

		if (xv == FLT_MAX) { continue; }

		double const xNorm = arg.XNormalize(xv);
		double const x = rect.Width() * xNorm + rect.left;

		if (yv == FLT_MAX) {
			// 補間:OFFの場合は欠損値が来たら描画する
			// 補間:ONの場合はここでは描かずに繋げて描く(直線補間と同じ事になる)
			if (!m_interpolate && !(temp.empty() && pts.empty())) {
				if (std::abs(prevX - x) > threshold) {
					prevX = x;
					CompressPoint(temp, &pts);
					temp.clear();
				}

				if (pts.size() > 1) {
					arr.push_back(pts);
				}
				temp.clear();
				pts.clear();
			}
			continue;
		}

		yValue = m_accumulative ? yValue + yv : yv;
		double yNorm = arg.yReverse ? 1 - (yValue - yMin) / (yMax - yMin) : (yValue - yMin) / (yMax - yMin);
		double y = rect.Height() - rect.Height() * yNorm + rect.top;

		if (first) { // i == 0の時の値が無効値の場合もあるのでフラグで管理している
			prevX = x;
			first = false;
		}

		if (std::abs(prevX - x) > threshold) {
			prevX = x;
			CompressPoint(temp, &pts);
			temp.clear();
		}
		temp.push_back(OCPoint(x, y));
	}

	// 余りがあれば...
	CompressPoint(temp, &pts);

	if (pts.size() > 1) {
		arr.push_back(pts);
	}
}

/*!	temp(座標値)を二点以下に間引いてptsに格納します
	@pre	temp内のX座標はすべてthreshold範囲内に収まっていること

	@param[in]	temp	描画座標のリスト
	@param[out]	pts		間引いたあとの座標を格納する配列
*/
void OCLinePlot::CompressPoint(std::vector<OCPoint> const& temp, std::vector<OCPoint>* pts) const
{
	if (temp.empty()) { return; }

	struct PointAndIndex {
		PointAndIndex(OCPoint const& pt) : index(0), pt(pt) {}
		size_t index;
		OCPoint pt;
	};

	// ひとつのX座標につき2点まで描画に使用する(その区間での最大、最小値を使用)
	PointAndIndex min(temp[0]), max(temp[0]);
	for (size_t j = 1; j < temp.size(); ++j) {
		if (temp[j].y > max.pt.y) {
			max.pt = temp[j];
			max.index = j;
		}
		if (temp[j].y < min.pt.y) {
			min.pt = temp[j];
			min.index = j;
		}
	}

	// データの並びは維持する
	pts->push_back(max.index > min.index ? min.pt : max.pt);
	if (temp.size() > 1) {
		pts->push_back(max.index > min.index ? max.pt : min.pt);
	}
}

//! 描画に使用する座標を作成・取得します
/*!
	描画時間引きは行いません
	@param[out]	arr		描画座標
	@param[in]	rect	折れ線図の矩形座標
	@param[in]	arg		描画に必要な諸設定
*/
void OCLinePlot::GetNormalDrawPoints(PointArray& arr, OCRect const& rect, DrawArgs const& arg) const
{
	std::vector<OCPoint> pts;
	pts.reserve(m_data->GetDataCount());

	float yValue = 0;
	for (size_t i = 0; i < m_data->GetDataCount(); ++i) {
		float const xv = m_data->GetRealData(0, (int)i);
		float const yv = m_data->GetRawData(1, (int)i);

		if (xv == FLT_MAX) { continue; }

		double const xNorm = arg.XNormalize(xv);
		double const x = rect.Width() * xNorm + rect.left;

		if (yv == FLT_MAX) {
			// 補間:OFFの場合は欠損値が来たら描画する
			// 補間:ONの場合はここでは描かずに繋げて描く(直線補間と同じ事になる)
			if (!m_interpolate && !pts.empty()) {
				if (pts.size() > 1) {
					arr.push_back(pts);
				}
				pts.clear();
			}
			continue;
		}

		yValue = m_accumulative ? yValue + yv : yv;
		// yValueは常にリニアなので、スケールに合わせて値を変換してから座標を求める
		double const sv = m_data->GetScaleType(1) == ST_CONSTANT
			? m_data->GetRealData(1, (int)i)
			: m_data->ConvertValue(1, yValue);
		if (sv == FLT_MAX) { 
			// 補間:OFFの場合は欠損値が来たら描画する
			// 補間:ONの場合はここでは描かずに繋げて描く(直線補間と同じ事になる)
			if (!m_interpolate && !pts.empty()) {
				if (pts.size() > 1) {
					arr.push_back(pts);
				}
				pts.clear();
			}
			continue;
		}
		double const yNormB = arg.YNormalize(sv);
		double const yNorm = arg.yReverse ? 1 - yNormB : yNormB;
		double const y = rect.Height() - rect.Height() * yNorm + rect.top;

		pts.push_back(OCPoint(x, y));
	}
	if (pts.size() > 1) {
		arr.push_back(pts);
	}
}

void OCLinePlot::EnableOnlineMode(bool enable)
{
	m_inter.reset();
	if (enable) {
		size_t const n = m_data->GetDataCount();
		std::vector<double> dx, dy;
		dx.reserve(n);
		dy.reserve(n);

		m_max = -DBL_MAX, m_min = DBL_MAX;
		for (size_t i = 0; i < n; ++i) {
			float const x = m_data->GetData(0, i);
			float const y = m_data->GetData(1, i);
			if (x != FLT_MAX && y != FLT_MAX) {
				dx.push_back(x);
				dy.push_back(y);
				m_max = std::max<double>(dx.back(), m_max);
				m_min = std::min<double>(dx.back(), m_min);
			}
		}
		if (dx.size() > 1) {
			m_inter.reset(new OCLineInterpolate());
			m_inter->Init(static_cast<int>(n), &dx[0], &dy[0]);
		}
	}
}

double OCLinePlot::GetYPosition(double x, OCRect const& rect, DrawArgs const& arg) const
{
	double xMax = arg.xMax;
	double xMin = arg.xMin;
	double yMax = arg.yMax;
	double yMin = arg.yMin;

	for (size_t i = 0; i < m_data->GetDataCount(); ++i) {
		double ax = rect.Width() * (m_data->GetRealData(0, (int)i) - xMin) / (xMax - xMin) + rect.left;
		if (x < ax) {
			double y = rect.Height() - rect.Height() * (m_data->GetData(1, (int)i) - yMin) / (yMax - yMin) + rect.top;
			return y;
		}
	}

	return rect.Height() - rect.Height() * (m_data->GetData(1, (int)m_data->GetDataCount() - 1) - yMin) / (yMax - yMin) + rect.top;
}

bool OCLinePlot::GetNearPosition(OCPoint* p, OCPoint* value, OCRect const& rect, DrawArgs const& arg, bool next) const
{
	double const xMax = arg.xMax;
	double const xMin = arg.xMin;
	double const yMax = arg.yMax;
	double const yMin = arg.yMin;

	size_t minIndex, maxIndex;
	m_data->FindMinMaxIndex(0, &minIndex, &maxIndex); //!@todo パフォーマンスに難あり
	double const xRealMax = m_data->GetData(0, maxIndex);
	double const xRealMin = m_data->GetData(0, minIndex);

	bool notInter = !m_inter.get();
	if (notInter) { const_cast<OCLinePlot*>(this)->EnableOnlineMode(true); }

	if (m_inter.get()) {
		// X座標からX値を取得し、そこからYの値を得る
		// 最大-最小の範囲からはみ出ないようにする
		while (true) {
			double const x = (p->x - rect.left) / rect.Width() * (xMax - xMin) + xMin;
			double const y = m_inter->Calc(x);
			p->x = rect.Width() * (x - xMin) / (xMax - xMin) + rect.left;
			p->y = rect.Height() - rect.Height() * (y - yMin) / (yMax - yMin) + rect.top;
			value->x = x;
			value->y = y;

			// Y方向にクリッピングする場合、Yの値がY軸の範囲外にある個所にはヒットさせない
			if (m_clippingType == plot::AllClip || m_clippingType == plot::YClip) {
				if (y > yMax || y < yMin) {
					double const offset = 0.05; // オフセット量(mm)
					if (next)	{ p->x += offset; }
					else		{ p->x -= offset; }

					if (x >= xRealMax || x <= xRealMin) {
						return false;
					}
					continue;
				}
			}

			break; // クリッピングしない場合はループしない
		}
	}

	if (notInter) { const_cast<OCLinePlot*>(this)->EnableOnlineMode(false); }

	return true;
}

void OCLinePlot::GetSearchPointFromValue(OCPoint* point, OCPoint* value, OCRect const& rect) const
{
	bool notInter = !m_inter.get();
	if (notInter) { const_cast<OCLinePlot*>(this)->EnableOnlineMode(true); }

	double xMax = m_data->GetMaximum(0);
	double xMin = m_data->GetMinimum(0);
	double yMax = m_data->GetMaximum(1);
	double yMin = m_data->GetMinimum(1);

	if (m_inter.get()) {
		double const y = m_inter->Calc(value->x);
		point->x = rect.Width() * (value->x - xMin) / (xMax - xMin) + rect.left;
		point->y = rect.Height() - rect.Height() * (y - yMin) / (yMax - yMin) + rect.top;
		value->y = y;
	}

	if (notInter) { const_cast<OCLinePlot*>(this)->EnableOnlineMode(false); }
}

void OCLinePlot::GetPosition(size_t index, double* x, double* y, OCRect const& rect, DrawArgs const& arg) const
{
	__super::GetPosition(index, x, y, rect, arg);
}

IPlotDrawer* OCLinePlot::Clone()
{
	OCLinePlot* clone = new OCLinePlot(m_owner, m_plot);
	return clone;
}

void OCLinePlot::AddProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode)
{
	{
		IPropertyGroup* group = prop->CreateGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_POLYGONAL_LINE"), PI_LINE_VISIBLE, m_visible, !plotMode);
		/*if (m_visible)*/
		{
			bool readOnly = !plotMode || !m_visible;

			group->AddBoolProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_ASSISTANT_TERVAL_DEFICIT_VALUE"), PI_LINE_INTERPOLATE, m_interpolate, readOnly);

			group->AddBoolProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SORT"), PI_DATA_SORT, !!m_plot->SendMessage(PI_GET_PLOT_SORT, 0, 0), readOnly);

			IPropertyGroup* lineGr = group->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_LINE"));
			{
				MakeLineProperty(m_plot, lineGr, g, readOnly, m_lineStyle, PI_LINE_LINESTYLE);
				lineGr->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_KANJI"), PI_LINE_LINECOLOR, m_lineColor, readOnly || m_lineStyle == PS_NULL);
				lineGr->AddSelectorPropertyR(::GetResourceString(m_plot,"ID_SOURCE_TEXT_THICKNESS_PT"), PI_LINE_LINEWIDTH, readOnly || m_lineStyle == PS_NULL, 
					m_lineWidth, 9, 0.25, 0.5, 0.75, 1.0, 1.5, 2.25, 3.0, 4.5, 6.0);

				std::wstring corner1 = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_ROUND");
				std::wstring corner2 = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_SQUARE");
				lineGr->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SHAPE_OF_CORNER"), PI_LINE_LINEROUNDTYPE, m_lineRoundType, readOnly || m_lineStyle == PS_NULL,
						2, corner1.c_str(), corner2.c_str());
			}

			IPropertyGroup* fillGr = group->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_PAINT_FILL"), PI_LINE_FILLING, m_filling, readOnly);
			/*if (m_filling)*/ {
				fillGr->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_KANJI"), PI_LINE_FILLCOLOR, m_fillColor, readOnly || !m_filling);
				fillGr->AddTransparentProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_TRANSPARENCY_PAR"), PI_LINE_FILL_TRANSPARENT, m_fillTransparent, readOnly || !m_filling);
			}
		}
	}
}

void OCLinePlot::AddTabProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode)
{
	IPropertyGroup* parent;
	IPropertyGroup* group;
	{
		group=prop->CreateGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_POLYGONAL_LINE"));
		parent=group->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_POLYGONAL_LINE"), PI_LINE_VISIBLE, m_visible, !plotMode);
		/*if (m_visible)*/
		{
			bool readOnly = !plotMode || !m_visible;

			parent->AddBoolProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_ASSISTANT_TERVAL_DEFICIT_VALUE"), PI_LINE_INTERPOLATE, m_interpolate, readOnly);

			parent->AddBoolProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SORT"), PI_DATA_SORT, !!m_plot->SendMessage(PI_GET_PLOT_SORT, 0, 0), readOnly);

			IPropertyGroup* lineGr = parent->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_LINE"));
			{
				MakeLineProperty(m_plot, lineGr, g, readOnly, m_lineStyle, PI_LINE_LINESTYLE);
				lineGr->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_KANJI"), PI_LINE_LINECOLOR, m_lineColor, readOnly || m_lineStyle == PS_NULL);
				lineGr->AddSelectorPropertyR(::GetResourceString(m_plot,"ID_SOURCE_TEXT_THICKNESS_PT"), PI_LINE_LINEWIDTH, readOnly || m_lineStyle == PS_NULL, 
					m_lineWidth, 9, 0.25, 0.5, 0.75, 1.0, 1.5, 2.25, 3.0, 4.5, 6.0);

				std::wstring corner1 = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_ROUND");
				std::wstring corner2 = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_SQUARE");
				lineGr->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SHAPE_OF_CORNER"), PI_LINE_LINEROUNDTYPE, m_lineRoundType, readOnly || m_lineStyle == PS_NULL,
						2, corner1.c_str(), corner2.c_str());
			}

			IPropertyGroup* fillGr = parent->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_PAINT_FILL"), PI_LINE_FILLING, m_filling, readOnly);
			/*if (m_filling)*/ {
				fillGr->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_KANJI"), PI_LINE_FILLCOLOR, m_fillColor, readOnly || !m_filling);
				fillGr->AddTransparentProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_TRANSPARENCY_PAR"), PI_LINE_FILL_TRANSPARENT, m_fillTransparent, readOnly || !m_filling);
			}
		}
	}
}

bool OCLinePlot::SetProperty(long id, LPARAM value, LPARAM* source)
{
	switch (id)
	{
	case PI_LINE_VISIBLE:
		::SetPropertyHelper(m_visible, value, source);
		return true;
	case PI_LINE_LINESTYLE:
		::SetPropertyHelper(m_lineStyle, value, source);
		return true;
	case PI_LINE_LINECOLOR:
		::SetPropertyHelper(m_lineColor, value, source);
		return true;
	case PI_LINE_LINEWIDTH:
		if (!::SetPropertyHelper(m_lineWidth, value, source, 0.1f, 100.0f)) {
			std::wstring msg = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_INPUT_THICKNESS_LINE_RANGE_01_100");
			::MessageBox(m_owner, msg.c_str(), ::GetResourceString(m_plot,"ID_SOURCE_TEXT_O_CHART"), MB_OK|MB_ICONINFORMATION);
			return false;
		}
		return true;
	case PI_LINE_LINEROUNDTYPE:
		::SetPropertyHelper(m_lineRoundType, value, source);
		return true;
	case PI_LINE_FILLING:
		::SetPropertyHelper(m_filling, value, source);
		return true;
	case PI_LINE_FILLCOLOR:
		::SetPropertyHelper(m_fillColor, value, source);
		return true;
	case PI_LINE_FILL_TRANSPARENT:	
		return ::SetPropertyHelper(m_fillTransparent, value, source, (short)0, (short)100);
	case PI_LINE_INTERPOLATE:
		::SetPropertyHelper(m_interpolate, value, source);
		return true;
	}

	return __super::SetProperty(id, value, source);
}

bool OCLinePlot::GetProperty(long id, LPARAM* ret) const
{
	switch (id)
	{
	case PI_LINE_FILLING:
		*ret = m_filling;
		return true;
	case PI_LINE_FILLCOLOR:
		*ret = m_fillColor;
		return true;
	case PI_LINE_FILL_TRANSPARENT:
		*ret = m_fillTransparent;
		return true;
	}
	return __super::GetProperty(id, ret);
}

void OCLinePlot::CopyFrom(IPlotDrawer const* src)
{
	__super::CopyFrom(src);
	if (OCLinePlot const* self = dynamic_cast<OCLinePlot const*>(src)) {
		m_filling = self->m_filling;
		m_interpolate = self->m_interpolate;
	}
}

//! プロットの情報をXML形式で出力します
void OCLinePlot::Serialize(ISerializer* ar)
{
	__super::Serialize(ar);

	ar->Serialize(_T("Filling"), m_filling);
	ar->Serialize(_T("FillTransparent"), m_fillTransparent, 0);
	ar->Serialize(_T("Interpolate"), m_interpolate, true);
}

//! サーチモードを開始します
void OCLinePlot::BeginSearch(DrawArgs const& arg, bool interpolate, ExclusionScale exclude, OCRect const& rect)
{
	if (!interpolate) {
		__super::BeginSearch(arg, interpolate, exclude, rect);
		return;
	}

	m_searchArray.clear();

	LineWrapper wrapper(m_data, m_interpolate, m_accumulative);

	OCLineInterpolate inter;

	double tx[2], ty[2], gap = (arg.xMax - arg.xMin) / 1000.0;
	for (size_t i = 0; i < wrapper.m_src.size(); ++i) {
		for (size_t j = 0; j < wrapper.m_src[i].size() - 1; ++j) {
			tx[0] = wrapper.m_src[i][j].x;
			tx[1] = wrapper.m_src[i][j + 1].x;
			ty[0] = wrapper.m_src[i][j].y;
			ty[1] = wrapper.m_src[i][j + 1].y;
			inter.Init(2, tx, ty);
			float const increment = (tx[0] < tx[1]) ? gap : -gap;
			for (; ;) {
				float const x = tx[0];
				float const y = inter.Calc(x);

				double const xPos = rect.Width() * arg.XNormalize(x) + rect.left;
				double const yPos = rect.Height() - rect.Height() * arg.YNormalize(y) + rect.top;

				tx[0] += increment;

				if (exclude & ExcludeX) {
					if (x > arg.xMax || x < arg.xMin) { goto LAST; }
				}
				if (exclude & ExcludeY) {
					if (y > arg.yMax || y < arg.yMin) { goto LAST; }
				}

				{
					SearchValue sv = { OCPoint(xPos, yPos), OCPoint3D(x, y, -1), m_searchArray.size() };
					m_searchArray.push_back(sv);
				}

LAST:
				if (increment < 0) {
					if (tx[0] < tx[1]) { break; }
				}
				else {
					if (tx[0] > tx[1]) { break; }
				}
			}
		}
	}
}

//!	(X, Y)値を元に補間データのある座標と値を取得する
/*!
	@param[in]	rect	プロットの矩形座標
	@param[in]	target	補間対象の値
	@param[out]	point	補間して得られた座標
	@param[out]	value	補間して得られた値

	折れ線図の場合、ひとつのX値につき複数のYの値が存在することがある。
	その場合、target.yの値と最も近い値を採用する。
*/
void OCLinePlot::GetPointByValue(OCRect const& rect, OCPoint const& target, OCPoint* point, OCPoint3D* value)
{
	LineWrapper wrapper(m_data, m_interpolate, m_accumulative);
	SearchValue ret;
	if (wrapper.Calc(rect, target, &ret)) {
		*point = ret.point;
		value->x = ret.value.x;
		value->y = ret.value.y;
	}
}
