/*!	@file
	@date	2008-04-21
	@author	wada

	@brief	3Dマップ計算クラスのインターフェイス
*/
#pragma once

#include "EnumDefine.h"
#include "type_safe_enum.h"

class ICanvas;
class IPlotData;
class IMessageReceiver;
class ISerializer;
struct OCRect;
class IPropertyCreator;
class IPropertyGroup;
class OCProperty;
class OCColorPattern;
class OCSliceLevel;

struct ExtrapolationType_def {
	enum type {
		All,
		XAxis,
		YAxis,
		None,
	};
};
typedef safe_enum<ExtrapolationType_def> ExtrapolationType;

class IMapImage
{
public:
	IMapImage(IMessageReceiver* owner)
		: m_plot(owner)
		, m_fillType(ContourFilling::ON), m_sliceType(0), m_selected(true)
	{}
	virtual ~IMapImage() = 0 {}

	void SetOwner(IMessageReceiver* owner) { m_plot = owner; }
	IMessageReceiver* GetOwner() const { return m_plot; }

    void SetColorPattern(OCColorPattern* pattern) { m_pattern = pattern; }

	// 描画するオブジェクトが選択(フォーカス)状態になっているか
	void SetSelected(bool select) { m_selected = select; }

	virtual void Click(OCRect const& /*rect*/, double /*x*/, double /*y*/) {}
	virtual bool Drag(ICanvas* /*g*/, OCRect const& /*rect*/, double /*x*/, double /*y*/) {return false;}

	virtual bool Interpolation(IPlotData* data, double div, OCRect const& rect, bool displayProgressbar = true, bool reCalcSlice = true) = 0;
	virtual void BitBlt(HDC hdc, int x, int y, int width, int height, DWORD rop) const = 0;
	virtual void DrawImage(ICanvas* g, double x, double y, double width, double height, short alpha) const = 0;

	virtual bool HitTest(OCRect const& rect, double x, double y) const = 0;

	void SetFillType(ContourFilling::FillType fillType) { m_fillType = fillType; }
	void SetSliceType(int sliceType) { m_sliceType = sliceType; }
	virtual void AddTabProperty(IPropertyGroup* prop, bool readOnly = false, bool plotMode = true)=0;
	virtual void AddProperty(IPropertyGroup* prop, bool readOnly = false, bool plotMode = true) = 0;
	virtual bool SetProperty(long id, LPARAM value, LPARAM* source, bool& reCalc) = 0;
	virtual bool GetProperty(long id, LPARAM* ret) const = 0;

	virtual ExtrapolationType GetExtrapolation() const { return ExtrapolationType::All; }

	virtual size_t GetSliceLevelCount() const { return 0; }

	virtual void CopyFrom(IMapImage* src) = 0;

	virtual void Serialize(ISerializer* ar) {}
	virtual void AfterSerialize() {}

	virtual bool IsEmpty() const { return false; }
	virtual void Destroy() = 0;

	// event
	boost::signals2::signal<void(OCSliceLevel*)> AddSliceLevel;
	boost::signals2::signal<void(OCSliceLevel*)> RemoveSliceLevel;

protected:
	IMessageReceiver* m_plot;
	ContourFilling::FillType m_fillType;
	int m_sliceType;
	bool m_selected;

    OCColorPattern* m_pattern;
};