/*!	@file
	@date	2008-05-06
	@author	wada

	@brief	マーカーを描画するための関数を提供します
*/
#pragma once

#include "ICanvas.h"

namespace Marker
{

//! マーカー種別
enum Type
{
	//None,
	Dot,
	Rectangle,
	Diamond,
	TriangleDown,
	Triangleup,
	Circle,
	Cross,
	Plus,
	DoubleCircle,
};

/*! マーカーを描画します
	@param[in]	g		マーカーを描画するのに使用するキャンバス
	@param[in]	type	マーカー種別
	@param[in]	x		X座標(左端)
	@param[in]	y		Y座標(上端)
	@param[in]	size	マーカーのサイズ

	x, y, size は mm 単位
*/
static inline void DrawMarker(ICanvas* g, Type type, double x, double y, double size)
{
	switch (type)
	{
	case Dot:
	case Circle:
		{
			// bFill = true => 必要ならば円の中を塗りつぶす
			g->DrawMarkerCircle(x, y, size, true);
		}
		break;
	case Rectangle:
		{
			g->DrawMarkerRectangle(x, y, size);
		}
		break;
	case Diamond:
		{
			// flag = 0 => Diamond
			g->DrawMarkerDiamondTriangle(x, y, size, 0);
		}
		break;
	case TriangleDown:
		{
			// flag = 2 => TriangleDown
			g->DrawMarkerDiamondTriangle(x, y, size, 2);
		}
		break;
	case Triangleup:
		{
			// flag = 1 => TriangleUp
			g->DrawMarkerDiamondTriangle(x, y, size, 1);
		}
		break;
	case Cross:
		{
			// flag = 0 => Cross
			g->DrawMarkerCrossPlus(x, y, size, 0);
		}
		break;
	case Plus:
		{
			// flag = 1 => Plus
			g->DrawMarkerCrossPlus(x, y, size, 1);
		}
		break;
	case DoubleCircle:
		{
			// bFill = true => 必要ならば円の中を塗りつぶす
			g->DrawMarkerCircle(x, y, size, true);

			double inner = size / 2;
			// bFill = false => 円の中は塗りつぶさない(2重円の内側のため)
			g->DrawMarkerCircle(x, y, inner, false);
		}
		break;
	}

/*
	g->SetRoundMode(false);
	//!@todo ループ内での分岐は効率が悪いのでテーブルを作るなどする(この関数は基本的にループ内で呼ばれる)
	switch (type)
	{
	case Dot:
		{
			//size = 0.2;//1.5; // 点の時はサイズ固定
			OCRect r(x - size, y - size, x + size, y + size);
			g->DrawEllipse(r);
			g->FillEllipse(r);
		}
		break;
	case Circle:
		{
			OCRect r(x - size, y - size, x + size, y + size);
			g->DrawEllipse(r);
			g->FillEllipse(r);
		}
		break;
	case Rectangle:
		{
			OCRect r(x - size, y - size, x + size, y + size);
			g->DrawRectangle(r);
			g->FillRectangle(r);
		}
		break;
	case Diamond:
		{
			OCPoint pts[] = {
				OCPoint(x, y - size),
				OCPoint(x - size, y),
				OCPoint(x, y + size),
				OCPoint(x + size, y),
			};
			g->DrawPolygon(pts, 4);
			g->FillPolygon(pts, 4);
		}
		break;
	case TriangleDown:
		{
			OCPoint pts[] = {
				OCPoint(x - size, y - size),
				OCPoint(x + size, y - size),
				OCPoint(x, y + size),
			};
			g->DrawPolygon(pts, 3);
			g->FillPolygon(pts, 3);
		}
		break;
	case Triangleup:
		{
			OCPoint pts[] = {
				OCPoint(x, y - size),
				OCPoint(x - size, y + size),
				OCPoint(x + size, y + size),
			};
			g->DrawPolygon(pts, 3);
			g->FillPolygon(pts, 3);
		}
		break;
	case Cross:
		{
			g->DrawLine(OCPoint(x - size, y - size), OCPoint(x + size, y + size));
			g->DrawLine(OCPoint(x - size, y + size), OCPoint(x + size, y - size));
		}
		break;
	case Plus:
		{
			g->DrawLine(OCPoint(x, y - size), OCPoint(x, y + size));
			g->DrawLine(OCPoint(x - size, y), OCPoint(x + size, y));
		}
		break;
	case DoubleCircle:
		{
			OCRect r(x - size, y - size, x + size, y + size);
			g->DrawEllipse(r);
			g->FillEllipse(r);

			double const inner = size / 2;
			r = OCRect(x - inner, y - inner, x + inner, y + inner);
			g->DrawEllipse(r);
			g->FillEllipse(r);
		}
		break;
	}
	g->SetRoundMode(true);
*/
}

//! マーカーに対する当たり判定
static inline bool HitTest(Type type, double x, double y, double size, OCPoint const& pt)
{
	if (type == Dot || type == Circle || type == DoubleCircle) {
		// 円だけ別処理
		double x1 = pt.x - ((x - size) + (x + size)) / 2.0;
		double y1 = pt.y - ((y - size) + (y + size)) / 2.0;
        double div2 = size;
        double x2, y2;
        x2 = y2 = div2 ? div2 : 1;

        return (((x1 * x1) / (x2 * x2)) + ((y1 * y1) / (y2 * y2))) <= 1;
	}
	// それ以外は矩形座標で判定する
	OCRect const r(x - size, y - size, x + size, y + size);
	return r.Contains(pt.x, pt.y);
}

} // namespace
