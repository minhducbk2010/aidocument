// Plot2D.cpp : DLL アプリケーションのエントリ ポイントを定義します。
//

#include "stdafx.h"


#ifdef _MANAGED
#pragma managed(push, off)
#endif

#include "PlotFactory.h"

extern HMODULE g_handle; // VectorPlot.cpp

// defined export functions
#define DLLEXPORT __declspec(dllexport)
extern "C"
{
	//! プラグインのインスタンスを作成します
	DLLEXPORT IPlotFactory* WINAPI CreateInstance(HWND hWnd)
	{
		return new OCPlotFactory(hWnd);
	}

	//! プラグインを解放します
	DLLEXPORT void WINAPI ReleaseInstance(IPlotFactory* ptr)
	{
		ptr->Destroy();
	}
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
	{
		g_handle = hModule;
	}
    return TRUE;
}

#ifdef _MANAGED
#pragma managed(pop)
#endif

