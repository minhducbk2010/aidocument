/*!	@file
	@berief	�J���[�U�z�}
*/
#pragma once

#include "PlotLib/PlotImpl3D.h"
#include "MarkerDrawer.h"

class OCColorPattern;

class OCScatter3D : public OCPlotImpl3D
{
public:
	OCScatter3D(HWND hWnd, IMessageReceiver* plot);
	~OCScatter3D();

	virtual void Destroy() { delete this; }

	virtual void Draw(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const;
	virtual void DrawSelected(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const;
	virtual void DrawLegend(ICanvas* g, OCRect const& rect, int ratio) const;

	virtual bool HitTest(OCRect const& rect, DrawArgs const& arg, double x, double y);

	virtual void AddProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode);
	virtual void AddTabProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode);
	virtual bool SetProperty(long id, LPARAM value, LPARAM* source);
	virtual bool GetProperty(long id, LPARAM* ret) const;

	virtual void CopyFrom(IPlotDrawer const* src);
	virtual void Serialize(ISerializer* ar);

protected:
	Marker::Type m_type;
	double m_size;
	short m_transparent;

    OCColorPattern* m_pattern;
};
