/*!	@file
	@date	2008-04-21
	@author	wada

	@brief	三角メッシュ
*/
#include "StdAfx.h"

#if 0
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <wtl/atlapp.h>
#include <wtl/atlgdi.h>
#include "utility.h"
#include "TriangleImage.h"
#include "Delaunay/dDelaunay.h"
#include "IPlotData.h"
#include "PlotLib/ColorPattern.h"

OCTriangleImage::OCTriangleImage(IMessageReceiver* owner)
	: IMapImage(owner)
{
}

OCTriangleImage::~OCTriangleImage()
{
	if (!m_image.IsNull()) {
		m_image.Destroy();
	}
}

int GetXPos(float v, CRect const& r, float xMin, float xMax)
{
	return int(r.Width() * (v - xMin) / (xMax - xMin) + r.left);
}
int GetYPos(float v, CRect const& r, float yMin, float yMax)
{
	return int(r.Height() - r.Height() * (v - yMin) / (yMax - yMin) + r.top);
}

void OCTriangleImage::Interpolation(IPlotData* data, OCRect const& rect)
{
	double xMax = data->GetMaximum(0);
	double xMin = data->GetMinimum(0);
	double yMax = data->GetMaximum(1);
	double yMin = data->GetMinimum(1);
	double zMax = data->GetMaximum(2);
	double zMin = data->GetMinimum(2);

	float const* xData = data->GetXData();
	float const* yData = data->GetYData();
	float const* zData = data->GetDataByDimension(2);

	// calc
	std::vector<double> srcX(xData, xData + data->GetDataCount());
	std::vector<double> srcY(yData, yData + data->GetDataCount());
	std::vector<double> srcZ(zData, zData + data->GetDataCount());

	dDelaunay tri;
	tri.InputData(&srcX[0], &srcY[0], &srcZ[0], static_cast<unsigned int>(data->GetDataCount()), 1);
	tri.Calculation();
	std::vector<stDelaunayTriangle> result(tri.GetTrianlgeCount());
	tri.GetResult(&result[0]);

	// draw

	CRect r(0, 0, (int)MMToPx(rect.Width()), (int)MMToPx(rect.Height()));

	if (!m_image.IsNull()) {
		m_image.Destroy();
	}

	m_image.Create(r.Width(), r.Height(), 24);
	WTL::CDC dc; dc.Attach(m_image.GetDC());

	boost::function<int(float v)> getX = boost::bind(GetXPos, _1, r, static_cast<float>(xMin), static_cast<float>(xMax));
	boost::function<int(float v)> getY = boost::bind(GetYPos, _1, r, static_cast<float>(yMin), static_cast<float>(yMax));

	for (size_t i = 0; i < result.size(); ++i) {
		stDelaunayTriangle const& t = result[i];
		CPoint pts[] = {
			CPoint(getX(xData[t.p1]), getY(yData[t.p1])),
			CPoint(getX(xData[t.p2]), getY(yData[t.p2])),
			CPoint(getX(xData[t.p3]), getY(yData[t.p3])),
		};

		float z = std::max<float>(std::max<float>(zData[t.p1], zData[t.p2]), zData[t.p3]);

		CBrush br; br.CreateSolidBrush(m_pattern->GetColor(z));
		HBRUSH old = dc.SelectBrush(br);
		dc.Polygon(pts, _countof(pts));
		dc.SelectBrush(old);
	}

	dc.Detach();
	m_image.ReleaseDC();
}

void OCTriangleImage::BitBlt(HDC hdc, int x, int y, int width, int height, DWORD rop) const
{
	_ASSERT(!m_image.IsNull());
	if (!m_image.IsNull()) {
		m_image.StretchBlt(hdc, x, y, width, height, rop);
	}
}

#include "ICanvas.h"
void OCTriangleImage::DrawImage(ICanvas* g, double x, double y, double width, double height, short alpha) const
{
	g->DrawBitmap(m_image, x, y, width, height, alpha);
}

#endif