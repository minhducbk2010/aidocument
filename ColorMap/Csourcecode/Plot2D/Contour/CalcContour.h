/*! @file
*/
#pragma once
#include <windows.h>
#include "Message.h"

//! 格子の最大値
#define	MAX_MATX	50
#define	MAX_MATY	50

class CContour
{
public:
    CContour(ProgressCallback progress);
    void    GriddingParam(double Pm1, double Pm2, double Alpha);

    long   Gridding(long Nd,   double *Xd,  double *Yd, double *Zd,
                     double Xmin, double Xmax, double Ymin,
                     double Ymax, long Nx,   long Ny,  double FAR *f);

private:
    double	*A[MAX_MATX*MAX_MATY +1], *B;
    long	NBAND, N1, N2, L1, L2, L3, L4;
    long	MA, MB;
    double	PM1, PM2, ALPHA;
    long	ND, NX, NY;
    double	WT,	C1, C2, C3, C4, V;
    double	DX, DY;
    double  *XD, *YD, *ZD, XMIN, XMAX, YMIN, YMAX, *XG, *YG, *F;
    long	I, J, K;

    BOOL    MATRIX_1(void);
	void    JX_1(void);
	void    JY_1(void);
	void    JXX_1(void);
	void    JYY_1(void);
	void    JXY_1(void);
	void    BMAT_1(void);
	BOOL    HMAT_1(void);
    BOOL    CHOLESKI_BAND(void);

private:
	// プログレスバー用
	ProgressCallback m_progress;
	int m_pos;
	double m_percent, m_interval;
};
