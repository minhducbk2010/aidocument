/*!	@file

	@brief	コンターマップを画像として保持するクラスの実装
*/
#include "stdafx.h"
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <float.h>
#include <boost/lexical_cast.hpp>
#include <boost/foreach.hpp>
#include <boost/bind.hpp>
#include <boost/utility.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/range.hpp>
#include <wtl/atlapp.h>
#include <wtl/atlgdi.h>
#include "ISerializer.h"
#include "IMessageReceiver.h"
#include "Message.h"
#include "PlotLib/ColorPattern.h"
#include "ContourImage.h"
#include "IPlotData.h"
#include "SliceSetting.h"
#include "SliceLevel.h"

#include "IShape.h"

double const INVALID_VALUE = DBL_MAX;

#pragma warning(disable : 4996)

class OContourUtil
{
    //===========================================================================
    //! ラインの座標情報から描画 (Polyline) で使用するための構造体を作成します
    /*!
        @param[in] defs ラインの座標情報構造体を格納する配列
     */
    //===========================================================================
    std::vector<OPolygonDefX*> LineToPolyline(std::vector<OCLineDefX*>& defs)
    {
        std::vector<OPolygonDefX*> vResult;
        // ひとつずつ置き換える
        std::vector<OCPoint*> pts;
        // 配列内の要素がが無くなるまで処理する
        while(!defs.empty())
        {
            // 先頭の項目から処理していく
            OCLineDefX* LDef = defs.front();
            pts.push_back(new OCPoint(LDef->p1.x, LDef->p1.y));
            pts.push_back(new OCPoint(LDef->p2.x, LDef->p2.y));
            // 先頭の項目を削除
            delete LDef;
            defs.erase(defs.begin());

            bool bFound = true;
            while(bFound)
            {
                bFound = false;

                OCPoint* p = this->FindLDef(pts.back(), defs);
                if(p)
                {
                    pts.push_back(p);
                    bFound = true;
                }

                p = this->FindLDef(pts.front(), defs);
                if(p)
                {
                    pts.insert(pts.begin(), p);
                    bFound = true;
                }
            }

            // PolygonDefX 構造体に格納していく
            OPolygonDefX* poly = new OPolygonDefX();
            poly->size = (int)pts.size();
            poly->p = new OCPoint[poly->size];

            for(size_t i = 0; i < pts.size(); ++i)
            {
                poly->p[i] = *pts[i];
            }

            vResult.push_back(poly);

            // pts配列を初期化(全クリア)
            std::for_each(pts.begin(), pts.end(), &boost::checked_delete<OCPoint>);
            pts.clear();
        }
        return vResult;
    }

    //===========================================================================
    //! 引数のポイントと結合するデータ(ポイント)があればそれを返す
    /*!
        @retval 0(NULL)     データ無し
     */
    //===========================================================================
    OCPoint* FindLDef(const OCPoint* p, std::vector<OCLineDefX*>& defs)
    {
        OCPoint* fp = NULL;

        // 他の要素の座標と比べる
        for(std::vector<OCLineDefX*>::iterator iter = defs.begin();
            iter != defs.end(); ++iter)
        {
            if((*iter)->p1.x == p->x && (*iter)->p1.y == p->y)
            {
                fp = new OCPoint((*iter)->p2.x, (*iter)->p2.y);
                delete *iter;
                defs.erase(iter);
                break;
            }
            else if((*iter)->p2.x == p->x && (*iter)->p2.y == p->y)
            {
                fp = new OCPoint((*iter)->p1.x, (*iter)->p1.y);
                delete *iter;
                defs.erase(iter);
                break;
            }
        }

        return fp;
    }
    friend std::vector<OPolygonDefX*> LineToPolyline(std::vector<OCLineDefX*>& defs);
};

//! スケールからスライスレベルの値を計算します
std::vector<double> CalcSliceLevel(double dMax, double dMin, double dDiv)
{
    std::vector<double> arr;
    double dLevel = dMin;
    for(int i = 0; dLevel < dMax; ++i)
    {
        dLevel = dMin + (dDiv / 2) * i;
        if(dLevel > dMax) { break; }

        if(IsZero(dLevel))
        { // ゼロに限りなく近いときはゼロにする
            dLevel = 0.0;
        }
        arr.push_back(dLevel);
    }
    return arr;
}

//===========================================================================
//! 
//===========================================================================
std::vector<OPolygonDefX*> LineToPolyline(std::vector<OCLineDefX*>& defs)
{
    return OContourUtil().LineToPolyline(defs);
}

//===========================================================================
//!
//===========================================================================
std::vector<CPoint> PolylineToPoint(const std::vector<OPolygonDefX*>& poly)
{
    std::vector<CPoint> vResult;
    for(size_t i = 0; i < poly.size(); ++i)
    {
    	OPolygonDefX* Poly = poly[i];
        for(int j = 0; j < Poly->size; ++j)
        {
			vResult.push_back(CPoint((int)Poly->p[j].x, (int)Poly->p[j].y));
        }
    }
    return vResult;
}

//*****************************************************************************

template<class Range, class T>
int find_index(Range const& r, T value)
{
	typedef boost::range_iterator<Range>::type iter_t;
	//iter_t it = std::find(boost::begin(r), boost::end(r), value);
	T const* it = std::find(boost::begin(r), boost::end(r), value);
	if (it != boost::end(r)) {
		return std::distance(boost::begin(r), it);
	}
	return -1;
}

//! Constructor
OCContourImage::OCContourImage(IMessageReceiver* owner)
	: IMapImage(owner)
	, m_gradation(true), m_bContPaint(true), m_nSplitNum(2)
    , m_fXMeasData(0), m_fYMeasData(0), m_fZMeasData(0)
    , m_fXGridPoint(0), m_fYGridPoint(0), m_fZGridPoint(0)
	, m_dPm1(0), m_dPm2(1), m_dAlpha(32)
	, m_lNx(20), m_lNy(20)
	, m_mesh(2), m_alpha(2), m_extrapolation(ExtrapolationType::All)
	, m_showValue(false)
	, m_levelFontColor(0)
	, m_lineColor(0), m_levelFilling(false), m_levelPaintColor(RGB(255, 255, 255)), m_levelDigit(1)
	, m_legendIndex(0)
	, m_imageCreation(false), m_calculation(false)
	, m_modify(true), m_levelModified(false)
	, m_limitedRange(true)
	, m_bChangeGradation(true) //グラデーション選択状況は再計算の必要がある為Trueをセット
	, m_serializing(false), m_levelModifiedNotCopy(false)
{
	::memset(&m_levelFont, 0, sizeof(m_levelFont));
	m_levelFont.lfHeight = PointToDeviceUnit(9);
	::wcscpy_s(m_levelFont.lfFaceName, _countof(m_levelFont.lfFaceName), ::GetResourceString(m_plot,"ID_SOURCE_TEXT_MS_GOTHIC"));

	//
	int meshs[] = {5, 10, 20, 30, 50};
	int alphas[] = {2, 8, 32, 128, 1024,};
	int meshNum = owner->SendMessage(PI_MSG_GET_CONF_CONTOUR_MESH, 0, 0);
	int alphaNum = owner->SendMessage(PI_MSG_GET_CONF_CONTOUR_ALPHA, 0, 0);

	m_mesh = find_index(meshs, meshNum);
	if (m_mesh == -1) { m_mesh = 2; }
	else { UpdateMesh(); }

	m_alpha = find_index(alphas, alphaNum);
	if (m_alpha == -1) { m_alpha = 2; }
	else { UpdateAlpha(); }

	SetGriddingParam(m_dPm1, m_dPm2, m_dAlpha);
	SetGrid(m_lNx, m_lNy);
}

//! Destructor
OCContourImage::~OCContourImage()
{
	if (!m_image.IsNull()) {
		m_image.Destroy();
	}

    // 格子点座標のメモリ開放
    delete[] m_fXGridPoint;
    delete[] m_fYGridPoint;
    delete[] m_fZGridPoint;

    // データ点数分のメモリ開放
    delete[] m_fXMeasData;
    delete[] m_fYMeasData;
    delete[] m_fZMeasData;
}

//! 補間計算を実行しビットマップを作成します
bool OCContourImage::Interpolation(IPlotData* data, double div, OCRect const& rect, bool displayProgressbar/* = true*/, bool reCalcSlice/* = true*/)
{
	m_data = data;
	m_scaleInterval = div;
	m_rect = rect;

	UpdateMesh();
	UpdateAlpha();

	double const xMax = data->GetMaximum(0);
	double const xMin = data->GetMinimum(0);
	double const yMax = data->GetMaximum(1);
	double const yMin = data->GetMinimum(1);
	double const zMax = data->GetMaximum(2);
	double const zMin = data->GetMinimum(2);

	// 最大値、最小値が不正の場合は、何もしない
	// Interpolationが2回呼ばれて、1度目にこのような変なデータで呼ばれることが多い
	if (xMax <= xMin || yMax <= yMin || zMax <= zMin) {
		return false;
	}

	// 再計算の必要がない場合は何もしない
	if (!m_modify && IsEqual(m_fZInterval, div) && !CheckDataDifference(data) && !m_bChangeGradation) { return true; }
	m_fMeasXMax = xMax;
	m_fMeasXMin = xMin;
	m_fMeasYMax = yMax;
	m_fMeasYMin = yMin;
	m_fMeasZMax = zMax;
	m_fMeasZMin = zMin;
	m_fZInterval = div;
	m_currentPattern = m_pattern->GetPatternFileName(m_pattern->GetCurrentPattern());
	m_modify = false;

	if (!SetMeasData(data)) {
		m_image.Destroy();
		return false;
	}
	SetGrid(m_lNx, m_lNy);
	SetGridPoint(xMin, xMax, yMin, yMax, zMin, zMax);

	if (!CalContour(displayProgressbar)) {
		// 計算できなかったので画像は消す
		m_image.Destroy();
		return false; 
	}

	if (reCalcSlice) {
		std::vector<double> levels = CalcSliceLevel(zMax, zMin, div);
		SetSliceLevel(&levels[0], levels.size());
	}

	// 上下逆さにする
	CRect r(0, (int)MMToPx(rect.Height()), (int)MMToPx(rect.Width()), 0);
	CalRatio(r, xMin, xMax, yMin, yMax);
	CreateImage(zMin, zMax, div, m_gradation);

	m_serializing = false;

	return true;
}

//プロパティのグラデーション（ON・OFF）の選択状態が変わっているか判断
void OCContourImage::SetGradation(bool gradation) 
{ 
	if(m_gradation == gradation)	//前と今の選択状態が同一
	{
		m_bChangeGradation=false;	//更新する必要なし
	}
	else
	{
		m_bChangeGradation=true;	//更新の必要あり
	}

	m_gradation = gradation;		//グラデーションの状態をセット
}

//===========================================================================
/*!
    グリッディング時の設定
 */
//===========================================================================
void OCContourImage::SetGriddingParam(double dPm1, double dPm2, double dAlpha)
{
    m_dPm1      = dPm1;
    m_dPm2      = dPm2;
    m_dAlpha    = dAlpha;
}

//===========================================================================
/*!
     格子数を設定し、格子点の総数を返す
     @param lNx            x方向ｸﾞﾘｯﾄﾞ数
     @param lNy            y方向ｸﾞﾘｯﾄﾞ数
     @return    格子点の総数
 */
//===========================================================================
long OCContourImage::SetGrid(long lNx, long lNy)
{
    // classの内部変数に格納
    m_lNx = lNx;                    // x方向ｸﾞﾘｯﾄﾞ数(右端部分のｺﾝﾀｰﾏｯﾌﾟが描けないので+1)
    m_lNy = lNy;                    // y方向ｸﾞﾘｯﾄﾞ数(上端部分のｺﾝﾀｰﾏｯﾌﾟが描けないので+1)
    m_lGridCount = m_lNx * m_lNy;   // 格子点の総数

    // 格子点座標のﾒﾓﾘ確保
	delete[] m_fXGridPoint;
	delete[] m_fYGridPoint;
	delete[] m_fZGridPoint;
    m_fXGridPoint = new double[m_lGridCount];
    m_fYGridPoint = new double[m_lGridCount];
    m_fZGridPoint = new double[m_lGridCount];

    return m_lGridCount;
}

//===========================================================================
/*!
     格子点の座標を計算する
     @param dXmin   x最小値
     @param dXmax   x最大値
     @param dYmin   y最小値
     @param dYmax   y最大値
     @param dZmin   z最小値
     @param dZmax   z最大値
 */
//===========================================================================
void OCContourImage::SetGridPoint(double dXmin, double dXmax,
                             double dYmin, double dYmax,
                             double dZmin, double dZmax)
{
    m_fXGridMax = dXmax;
    m_fXGridMin = dXmin;
    m_fYGridMax = dYmax;
    m_fYGridMin = dYmin;
    m_fZGridMax = dZmax;
    m_fZGridMin = dZmin;

    m_fXIntvl = (m_fXGridMax - m_fXGridMin) / (m_lNx - 1);    // x座標間隔(右端+1の修正で-1)
    m_fYIntvl = (m_fYGridMax - m_fYGridMin) / (m_lNy - 1);    // y座標間隔(上端+1の修正で-1)

    int k = 0;
    for (int i = 0; i < m_lNy; ++i)
    {
        for (int j = 0; j < m_lNx; ++j)
        {
            m_fXGridPoint[k] = dXmin + double(m_fXIntvl * j);
            m_fYGridPoint[k] = dYmin + double(m_fYIntvl * i);
            ++k;
        }
    }
}
//=======================================================================
/*!
    描画に使う座標変換率とｵﾌｾｯﾄを計算する
    @param rClient     描画するｸﾗｲｱﾝﾄの座標
    @param dXmin       x最小値
    @param dXmax       x最大値
    @param dYmin       y最小値
    @param dYmax       y最大値
 */
//=======================================================================
void OCContourImage::CalRatio(RECT rClient, double dXmin, double dXmax,
                         double dYmin, double dYmax)
{
    m_fWdRatio = static_cast<double>((rClient.right  - rClient.left) / (dXmax - dXmin));
    m_fHgRatio = static_cast<double>((rClient.bottom - rClient.top ) / (dYmax - dYmin));
    m_fXOffSet = - dXmin * m_fWdRatio + rClient.left;
    m_fYOffSet = - dYmax * m_fHgRatio + rClient.bottom;

	m_width = rClient.right - rClient.left;
	m_height = rClient.bottom - rClient.top;
}

//===========================================================================
/*!	@brief	計測データをメンバに格納する
	@return	データに有効な値が存在していなければfalse, それ以外はtrueを返します
 */
//===========================================================================
/*
void OCContourImage::SetMeasData(float const* fXMeasData, float const* fYMeasData, float const* fZMeasData, long nMeasCount)
{
    // 初期値の設定----------------------------------------------------------
    m_nMeasCount    = nMeasCount;               // ﾃﾞｰﾀ点数を設定
    m_fXMeasData    = new float[m_nMeasCount];  // ﾃﾞｰﾀ点数分のﾒﾓﾘ作成
    m_fYMeasData    = new float[m_nMeasCount];  // ﾃﾞｰﾀ点数分のﾒﾓﾘ作成
    m_fZMeasData    = new float[m_nMeasCount];  // ﾃﾞｰﾀ点数分のﾒﾓﾘ作成

    std::copy(fXMeasData, fXMeasData + m_nMeasCount, m_fXMeasData);
    std::copy(fYMeasData, fYMeasData + m_nMeasCount, m_fYMeasData);
    std::copy(fZMeasData, fZMeasData + m_nMeasCount, m_fZMeasData);
}
*/
bool OCContourImage::SetMeasData(IPlotData* data)
{
	// 無効値を取り除いたデータ配列を作成します
	std::vector<float> vx, vy, vz;
	for (size_t i = 0, n = data->GetDataCount(); i < n; ++i) {
		float const x = data->GetRealData(0, i); // ログ設定、デシベル設定は、変換後の値を持ってくるはず
		float const y = data->GetRealData(1, i);
		float const z = data->GetRealData(2, i);
		if (x != FLT_MAX && y != FLT_MAX && z != FLT_MAX) {
			vx.push_back(x);
			vy.push_back(y);
			vz.push_back(z);
		}
	}
	if (vx.empty()) { return false; }

    delete[] m_fXMeasData; m_fXMeasData = 0;
    delete[] m_fYMeasData; m_fYMeasData = 0;
    delete[] m_fZMeasData; m_fZMeasData = 0;

    // 初期値の設定----------------------------------------------------------
	m_nMeasCount    = static_cast<long>(vx.size());     // ﾃﾞｰﾀ点数を設定

	if (!m_nMeasCount) { return false; }

    m_fXMeasData    = new float[m_nMeasCount];  // ﾃﾞｰﾀ点数分のﾒﾓﾘ作成
    m_fYMeasData    = new float[m_nMeasCount];  // ﾃﾞｰﾀ点数分のﾒﾓﾘ作成
    m_fZMeasData    = new float[m_nMeasCount];  // ﾃﾞｰﾀ点数分のﾒﾓﾘ作成

	std::copy(vx.begin(), vx.end(), m_fXMeasData);
	std::copy(vy.begin(), vy.end(), m_fYMeasData);
	std::copy(vz.begin(), vz.end(), m_fZMeasData);
	return true;
}

bool OCContourImage::CheckDataDifference(IPlotData* data)
{
	if (m_currentPattern != m_pattern->GetPatternFileName(m_pattern->GetCurrentPattern())) {
		return true;
	}

	// データが変わったかどうか調べる
	float const xMax = data->GetMaximum(0);
	float const xMin = data->GetMinimum(0);
	float const yMax = data->GetMaximum(1);
	float const yMin = data->GetMinimum(1);
	float const zMax = data->GetMaximum(2);
	float const zMin = data->GetMinimum(2);

	bool different = true;
	if (m_nMeasCount == static_cast<long>(data->GetDataCount())) {
		different = false;
		if (!IsEqual(xMax, m_fMeasXMax) || !IsEqual(xMin, m_fMeasXMin) ||
			!IsEqual(yMax, m_fMeasYMax) || !IsEqual(yMin, m_fMeasYMin) ||
			!IsEqual(zMax, m_fMeasZMax) || !IsEqual(zMin, m_fMeasZMin))
		{
			different = true;
		}
		else {
			for (int i = 0; i < m_nMeasCount; ++i) {
				//if (!IsEqual(vx[i], m_fXMeasData[i]) || !IsEqual(vy[i], m_fYMeasData[i]) || !IsEqual(vz[i], m_fZMeasData[i])) {
				if (!IsEqual(data->GetRealData(0, i), m_fXMeasData[i]) ||
					!IsEqual(data->GetRealData(1, i), m_fYMeasData[i]) ||
					!IsEqual(data->GetRealData(2, i), m_fZMeasData[i])) {
					different = true;
					break;
				}
			}
		}
	}
	m_fMeasXMax = xMax;
	m_fMeasXMin = xMin;
	m_fMeasYMax = yMax;
	m_fMeasYMin = yMin;
	m_fMeasZMax = zMax;
	m_fMeasZMin = zMin;

	return different;
}

//===========================================================================
/*!
    コンターマップの演算(Z軸の値を求める)
    @return true = 成功, false = 失敗
 */
//===========================================================================
bool OCContourImage::CalContour(bool displayProgressbar)
{
	m_calculation = true;

    // 一時変数を作成し計測データをコピー(演算に使用)
	std::vector<double> dX(m_fXMeasData, m_fXMeasData + m_nMeasCount);
    std::vector<double> dY(m_fYMeasData, m_fYMeasData + m_nMeasCount);
    std::vector<double> dZ(m_fZMeasData, m_fZMeasData + m_nMeasCount);

	ProgressCallback progress = displayProgressbar
		? reinterpret_cast<ProgressCallback>(m_plot->SendMessage(PI_START_PROGRESSBAR, reinterpret_cast<WPARAM>(::GetResourceString(m_plot,"ID_SOURCE_TEXT_CALCULATION_BETWEEN_ASSISTANT")), 0))
		: 0;

    double* df = new double[m_lGridCount];
    // ｺﾝﾀｰﾏｯﾌﾟの計算(格子点のZ値を求める)
	CContour contour(progress);
    contour.GriddingParam(m_dPm1, m_dPm2, m_dAlpha);
    int nR = contour.Gridding(m_nMeasCount, &dX[0], &dY[0], &dZ[0],
                                 m_fXGridMin, m_fXGridMax, m_fYGridMin, m_fYGridMax,
                                 m_lNx, m_lNy, df);

	m_plot->SendMessage(PI_EXIT_PROGRESSBAR, 0, 0);

    if(nR == 0) {
        for(int i = 0; i < m_lGridCount; ++i)
        {
            m_fZGridPoint[i] = df[i];
        }
    }
    else {
        // error
//        std::fill(m_fZGridPoint, m_fZGridPoint + m_lGridCount, INVALID_VALUE); 
    }
    delete [] df;

    if(nR != 0) {
        return false;
    }

    // 補間
    Interpolation(m_nSplitNum);

	m_calculation = false;

	return true;
}

//===========================================================================
/*!
    等高線の描画
    @param dContLevel   スライスレベル
    @param LineDefs     座標情報を格納する配列
 */
//===========================================================================
void OCContourImage::DrawContLine(COLORREF PainCOLORREF,
								  double dContLevel, std::vector<OCLineDefX*>* LineDefs )
{
    OCLineDefX* LDef;

    for( int i = 0; i < m_lNy - 1; ++i )
    {
        for( int j = 0; j < m_lNx - 1; ++j )
        {
            int l = this->CrossPointCount(dContLevel, j, i); // 通過本数(内部で通過点の座標も計算される)

            // 線を引く------------------------------------------------------------------
            switch(l)
            {
            case 0: // 0点は、描かない
                break;
            case 1: // 1点は、点を打つ
                //!@todo ???????????????
                break;
            case 2: // 2点は、点と点を結ぶ
                LDef = new OCLineDefX();
                LDef->color = PainCOLORREF;
                LDef->p1 = OCPoint( m_fX[0], m_fY[0] );
                LDef->p2 = OCPoint( m_fX[1], m_fY[1] );
                LDef->z = dContLevel;
                LineDefs->push_back( LDef );
                break;
            case 3: // 3点は、描かない
                int n1, n2;
                if ( m_fX[0] == m_fX[1] && m_fY[0] == m_fY[1])
                {
                    n1 = 1;	n2 = 2;
                }
                else if ( m_fX[0] == m_fX[2] && m_fY[0] == m_fY[2 ])
                {
                    n1 = 0;	n2 = 1;
                }
                else if ( m_fX[1] == m_fX[2] && m_fY[1] == m_fY[2] )
                {
                    n1 = 0;	n2 = 1;
                }
                else
                {
                    break;
                }
                LDef = new OCLineDefX();
                LDef->color = PainCOLORREF;
                LDef->p1 = OCPoint( m_fX[n1], m_fY[n1] );
                LDef->p2 = OCPoint( m_fX[n2], m_fY[n2] );
                LDef->z = dContLevel;
                LineDefs->push_back( LDef );
                break;
            case 4: // 4点は、隣り合う点と点で、距離の短いもの同士を結ぶ
                // d0とd1の距離
                double dDist01 = (m_fX[0] - m_fX[1]) * (m_fX[0] - m_fX[1]) + (m_fY[0] - m_fY[1]) * (m_fY[0] - m_fY[1]);
                // d0とd3の距離
                double dDist03 = (m_fX[0] - m_fX[3]) * (m_fX[0] - m_fX[3]) + (m_fY[0] - m_fY[3]) * (m_fY[0] - m_fY[3]);

                if(dDist01 < dDist03)
                { // d0とd1の距離の方が、d0とd3の距離より近いときはd0とd1、d2とd3を結ぶ
                    LDef = new OCLineDefX();
                    LDef->color = PainCOLORREF;
                    LDef->p1 = OCPoint( m_fX[0], m_fY[0] );
                    LDef->p2 = OCPoint( m_fX[1], m_fY[1] );
                    LDef->z = dContLevel;
                    LineDefs->push_back( LDef );

                    LDef = new OCLineDefX();
                    LDef->color = PainCOLORREF;
                    LDef->p1 = OCPoint( m_fX[2], m_fY[2] );
                    LDef->p2 = OCPoint( m_fX[3], m_fY[3] );
                    LDef->z = dContLevel;
                    LineDefs->push_back( LDef );
                }
                else
                { // d0とd3の距離の方が、d0とd1の距離より近いときはd0とd3、d1とd2を結ぶ
                    LDef = new OCLineDefX();
                    LDef->color = PainCOLORREF;
                    LDef->p1 = OCPoint( m_fX[0], m_fY[0] );
                    LDef->p2 = OCPoint( m_fX[3], m_fY[3] );
                    LDef->z = dContLevel;
                    LineDefs->push_back( LDef );

                    LDef = new OCLineDefX();
                    LDef->color = PainCOLORREF;
                    LDef->p1 = OCPoint( m_fX[1], m_fY[1] );
                    LDef->p2 = OCPoint( m_fX[2], m_fY[2] );
                    LDef->z = dContLevel;
                    LineDefs->push_back( LDef );
                }
                break;
            }
        }
    }
}

//===========================================================================
/*!
    等高線塗りつぶし
    @param [in]		dContLevel		線のレベル
    @param [in]		PainCOLORREF	線の色
    @param [out]	bPrinter		印刷フラグ
 */
//===========================================================================
void OCContourImage::DrawContPaint(double dContLevel,
                              COLORREF PainCOLORREF,
                              std::vector<OPolygonDefX*>* PolygonDefs )
{
    if(!m_bContPaint) return;

    // 初期値の設定----------------------------------------------------------
    int    i, j, k;          // 汎用ﾙｰﾌﾟｶｳｳﾝﾀ
    double dDist01, dDist03;    // 交点が４点あるときは距離を比較して、近いもの同士を結ぶ
    OCPoint points[6];           //　

	PolygonDefs->reserve(m_lNy * m_lNx);

    for(i = 0; i < m_lNy - 1; i++)
    {
        for(j = 0; j < m_lNx - 1; j++)
        {
			OPolygonDefX* PDef = 0;
            int l = CrossPointCount(dContLevel, j, i); // 通過本数(内部で通過点の座標も計算される)

            // 色で塗りつぶし--------------------------------------------------------
            switch(l)
            {
            case 0: // 0点
                //---------------------------------------------------------------------------
                // 格子点0のﾚﾍﾞﾙが等高線より大きければ、全てを塗りつぶす。
                //---------------------------------------------------------------------------
                if(dContLevel <= m_fCornerZ[0])
                {   // 四角形塗りつぶし
                    points[0] = OCPoint(m_PCorner[0].x, m_PCorner[0].y);
                    points[1] = OCPoint(m_PCorner[1].x, m_PCorner[1].y);
                    points[2] = OCPoint(m_PCorner[2].x, m_PCorner[2].y);
                    points[3] = OCPoint(m_PCorner[3].x, m_PCorner[3].y);

                    PDef = new OPolygonDefX();
                    PDef->color = PainCOLORREF;
                    PDef->size = 4;
                    PDef->p = new OCPoint[4];
                    PDef->p[0] = OCPoint(m_PCorner[0].x, m_PCorner[0].y);
                    PDef->p[1] = OCPoint(m_PCorner[1].x, m_PCorner[1].y);
                    PDef->p[2] = OCPoint(m_PCorner[2].x, m_PCorner[2].y);
                    PDef->p[3] = OCPoint(m_PCorner[3].x, m_PCorner[3].y);
                    PolygonDefs->push_back( PDef );
                }
                break;

            case 2: // 2点
                //---------------------------------------------------------------------------
                // 隣接する辺と辺を通過する時　
                // 3角形と5角形が出来る。ﾚﾍﾞﾙの大きな方を塗りつぶす。
                //---------------------------------------------------------------------------
                for(k = 0; k < 3; k++)
                {
                    if(m_nSide[0] == k && m_nSide[1] == k + 1)
                    {
                        if(dContLevel >= m_fCornerZ[k + 1])
                        {   // 五角形塗りつぶし
                            points[0] = OCPoint(m_fX[0], m_fY[0]);
                            points[1] = OCPoint(m_fX[1], m_fY[1]);
                            points[2] = OCPoint(m_PCorner[(k+2)%4].x, m_PCorner[(k+2)%4].y);
                            points[3] = OCPoint(m_PCorner[(k+3)%4].x, m_PCorner[(k+3)%4].y);
                            points[4] = OCPoint(m_PCorner[(k+0)%4].x, m_PCorner[(k+0)%4].y);

                            PDef = new OPolygonDefX();
                            PDef->color = PainCOLORREF;
                            PDef->size = 5;
                            PDef->p = new OCPoint[5];
                            PDef->p[0] = OCPoint(m_fX[0], m_fY[0]);
                            PDef->p[1] = OCPoint(m_fX[1], m_fY[1]);
                            PDef->p[2] = OCPoint(m_PCorner[(k+2)%4].x, m_PCorner[(k+2)%4].y);
                            PDef->p[3] = OCPoint(m_PCorner[(k+3)%4].x, m_PCorner[(k+3)%4].y);
                            PDef->p[4] = OCPoint(m_PCorner[(k+0)%4].x, m_PCorner[(k+0)%4].y);
                            PolygonDefs->push_back( PDef );
                        }
                        else
                        {   // 三角形塗りつぶし
                            points[0] = OCPoint(m_fX[0], m_fY[0]);
                            points[1] = OCPoint(m_fX[1], m_fY[1]);
                            points[2] = OCPoint(m_PCorner[(k+1)%4].x, m_PCorner[(k+1)%4].y);

                            PDef = new OPolygonDefX();
                            PDef->color = PainCOLORREF;
                            PDef->size = 3;
                            PDef->p = new OCPoint[3];
                            PDef->p[0] = OCPoint(m_fX[0], m_fY[0]);
                            PDef->p[1] = OCPoint(m_fX[1], m_fY[1]);
                            PDef->p[2] = OCPoint(m_PCorner[(k+1)%4].x, m_PCorner[(k+1)%4].y);
                            PolygonDefs->push_back( PDef );
                        }
                    }
                }
                if(m_nSide[0] == 0 && m_nSide[1] == 3)
                {
                    if(dContLevel >= m_fCornerZ[0])
                    {   // 五角形塗りつぶし
                        points[0] = OCPoint(m_fX[0], m_fY[0]);
                        points[1] = OCPoint(m_fX[1], m_fY[1]);
                        points[2] = OCPoint(m_PCorner[3].x, m_PCorner[3].y);
                        points[3] = OCPoint(m_PCorner[2].x, m_PCorner[2].y);
                        points[4] = OCPoint(m_PCorner[1].x, m_PCorner[1].y);

						PDef = new OPolygonDefX();
                        PDef->color = PainCOLORREF;
                        PDef->size = 5;
                        PDef->p = new OCPoint[5];
                        PDef->p[0] = OCPoint(m_fX[0], m_fY[0]);
                        PDef->p[1] = OCPoint(m_fX[1], m_fY[1]);
                        PDef->p[2] = OCPoint(m_PCorner[3].x, m_PCorner[3].y);
                        PDef->p[3] = OCPoint(m_PCorner[2].x, m_PCorner[2].y);
                        PDef->p[4] = OCPoint(m_PCorner[1].x, m_PCorner[1].y);
                        PolygonDefs->push_back( PDef );
                    }
                    else
                    {   // 三角形塗りつぶし
                        points[0] = OCPoint(m_fX[0], m_fY[0]);
                        points[1] = OCPoint(m_fX[1], m_fY[1]);
                        points[2] = OCPoint(m_PCorner[0].x, m_PCorner[0].y);

						PDef = new OPolygonDefX();
                        PDef->color = PainCOLORREF;
                        PDef->size = 3;
                        PDef->p = new OCPoint[3];
                        PDef->p[0] = OCPoint(m_fX[0], m_fY[0]);
                        PDef->p[1] = OCPoint(m_fX[1], m_fY[1]);
                        PDef->p[2] = OCPoint(m_PCorner[0].x, m_PCorner[0].y);
                        PolygonDefs->push_back( PDef );
                    }
                }
                //---------------------------------------------------------------------------
                // 対辺を通過する時
                // 4角形が2つ出来る。ﾚﾍﾞﾙの大きな方を塗りつぶす。
                //---------------------------------------------------------------------------
                for(k = 0; k < 2; k++)
                {
                    if(m_nSide[0] == k && m_nSide[1] == k + 2)
                    {
                        if(dContLevel >= m_fCornerZ[0])
                        {   // 四角形塗りつぶし
                            points[0] = OCPoint(m_fX[0], m_fY[0]);
                            points[1] = OCPoint(m_fX[1], m_fY[1]);
                            points[2] = OCPoint(m_PCorner[(k+2)%4].x, m_PCorner[(k+2)%4].y);
                            points[3] = OCPoint(m_PCorner[(k+1)%4].x, m_PCorner[(k+1)%4].y);

							PDef = new OPolygonDefX();
                            PDef->color = PainCOLORREF;
                            PDef->size = 4;
                            PDef->p = new OCPoint[4];
                            PDef->p[0] = OCPoint(m_fX[0], m_fY[0]);
                            PDef->p[1] = OCPoint(m_fX[1], m_fY[1]);
                            PDef->p[2] = OCPoint(m_PCorner[(k+2)%4].x, m_PCorner[(k+2)%4].y);
                            PDef->p[3] = OCPoint(m_PCorner[(k+1)%4].x, m_PCorner[(k+1)%4].y);
                            PolygonDefs->push_back( PDef );
                        }
                        else
                        {
                            points[0] = OCPoint(m_fX[0], m_fY[0]);
                            points[1] = OCPoint(m_fX[1], m_fY[1]);
                            points[2] = OCPoint(m_PCorner[(k+3)%4].x, m_PCorner[(k+3)%4].y);
                            points[3] = OCPoint(m_PCorner[(k+0)%4].x, m_PCorner[(k+0)%4].y);

							PDef = new OPolygonDefX();
                            PDef->color = PainCOLORREF;
                            PDef->size = 4;
                            PDef->p = new OCPoint[4];
                            PDef->p[0] = OCPoint(m_fX[0], m_fY[0]);
                            PDef->p[1] = OCPoint(m_fX[1], m_fY[1]);
                            PDef->p[2] = OCPoint(m_PCorner[(k+3)%4].x, m_PCorner[(k+3)%4].y);
                            PDef->p[3] = OCPoint(m_PCorner[(k+0)%4].x, m_PCorner[(k+0)%4].y);
                            PolygonDefs->push_back( PDef );
                        }
                    }
                }
                break;

            case 4: // 4点は、隣り合う点と点で、距離の短いもの同士を結ぶ
                dDist01 = (m_fX[0] - m_fX[1]) * (m_fX[0] - m_fX[1]) + (m_fY[0] - m_fY[1]) * (m_fY[0] - m_fY[1]);// d0とd1の距離
                dDist03 = (m_fX[0] - m_fX[3]) * (m_fX[0] - m_fX[3]) + (m_fY[0] - m_fY[3]) * (m_fY[0] - m_fY[3]);// d0とd3の距離

                if(dDist01 < dDist03)
                { // d0とd1の距離の方が、d0とd3の距離より近いときはd0とd1、d2とd3を結ぶ
                    if(dContLevel >= m_fCornerZ[0])
                    {   // 三角形を２つ塗りつぶし
                        points[0] = OCPoint(m_fX[0], m_fY[0]);
                        points[1] = OCPoint(m_fX[1], m_fY[1]);
                        points[2] = OCPoint(m_PCorner[1].x, m_PCorner[1].y);

						PDef = new OPolygonDefX();
                        PDef->color = PainCOLORREF;
                        PDef->size = 3;
                        PDef->p = new OCPoint[3];
                        PDef->p[0] = OCPoint(m_fX[0], m_fY[0]);
                        PDef->p[1] = OCPoint(m_fX[1], m_fY[1]);
                        PDef->p[2] = OCPoint(m_PCorner[1].x, m_PCorner[1].y);
                        PolygonDefs->push_back( PDef );
                        points[0] = OCPoint(m_fX[2], m_fY[2]);
                        points[1] = OCPoint(m_fX[3], m_fY[3]);
                        points[2] = OCPoint(m_PCorner[3].x, m_PCorner[3].y);

						PDef = new OPolygonDefX();
                        PDef->color = PainCOLORREF;
                        PDef->size = 3;
                        PDef->p = new OCPoint[3];
                        PDef->p[0] = OCPoint(m_fX[2], m_fY[2]);
                        PDef->p[1] = OCPoint(m_fX[3], m_fY[3]);
                        PDef->p[2] = OCPoint(m_PCorner[3].x, m_PCorner[3].y);
                        PolygonDefs->push_back( PDef );
                    }
                    else
                    {   // 六角形塗りつぶし
                        points[0] = OCPoint(m_fX[0], m_fY[0]);
                        points[1] = OCPoint(m_fX[1], m_fY[1]);
                        points[2] = OCPoint(m_PCorner[2].x, m_PCorner[2].y);
                        points[3] = OCPoint(m_fX[2], m_fY[2]);
                        points[4] = OCPoint(m_fX[3], m_fY[3]);
                        points[5] = OCPoint(m_PCorner[0].x, m_PCorner[0].y);

						PDef = new OPolygonDefX();
                        PDef->color = PainCOLORREF;
                        PDef->size = 6;
                        PDef->p = new OCPoint[6];
                        PDef->p[0] = OCPoint(m_fX[0], m_fY[0]);
                        PDef->p[1] = OCPoint(m_fX[1], m_fY[1]);
                        PDef->p[2] = OCPoint(m_PCorner[2].x, m_PCorner[2].y);
                        PDef->p[3] = OCPoint(m_fX[2], m_fY[2]);
                        PDef->p[4] = OCPoint(m_fX[3], m_fY[3]);
                        PDef->p[5] = OCPoint(m_PCorner[0].x, m_PCorner[0].y);
                        PolygonDefs->push_back( PDef );
                    }
                }
                else
                { // d0とd3の距離の方が、d0とd1の距離より近いときはd0とd3、d1とd2を結ぶ
                    if(dContLevel >= m_fCornerZ[1])
                    {   // 三角形を２つ塗りつぶし
                        points[0] = OCPoint(m_fX[0], m_fY[0]);
                        points[1] = OCPoint(m_fX[3], m_fY[3]);
                        points[2] = OCPoint(m_PCorner[0].x, m_PCorner[0].y);

						PDef = new OPolygonDefX();
                        PDef->color = PainCOLORREF;
                        PDef->size = 3;
                        PDef->p = new OCPoint[3];
                        PDef->p[0] = OCPoint(m_fX[0], m_fY[0]);
                        PDef->p[1] = OCPoint(m_fX[3], m_fY[3]);
                        PDef->p[2] = OCPoint(m_PCorner[0].x, m_PCorner[0].y);
                        PolygonDefs->push_back( PDef );
                        points[0] = OCPoint(m_fX[1], m_fY[1]);
                        points[1] = OCPoint(m_fX[2], m_fY[2]);
                        points[2] = OCPoint(m_PCorner[2].x, m_PCorner[2].y);

						PDef = new OPolygonDefX();
                        PDef->color = PainCOLORREF;
                        PDef->size = 3;
                        PDef->p = new OCPoint[3];
                        PDef->p[0] = OCPoint(m_fX[1], m_fY[1]);
                        PDef->p[1] = OCPoint(m_fX[2], m_fY[2]);
                        PDef->p[2] = OCPoint(m_PCorner[2].x, m_PCorner[2].y);
                        PolygonDefs->push_back( PDef );
                    }
                    else
                    {   // 六角形塗りつぶし
                        points[0] = OCPoint(m_fX[0], m_fY[0]);
                        points[1] = OCPoint(m_fX[3], m_fY[3]);
                        points[2] = OCPoint(m_PCorner[3].x, m_PCorner[3].y);
                        points[3] = OCPoint(m_fX[2], m_fY[2]);
                        points[4] = OCPoint(m_fX[1], m_fY[1]);
                        points[5] = OCPoint(m_PCorner[1].x, m_PCorner[1].y);

						PDef = new OPolygonDefX();
                        PDef->color = PainCOLORREF;
                        PDef->size = 6;
                        PDef->p = new OCPoint[6];
                        PDef->p[0] = OCPoint(m_fX[0], m_fY[0]);
                        PDef->p[1] = OCPoint(m_fX[3], m_fY[3]);
                        PDef->p[2] = OCPoint(m_PCorner[3].x, m_PCorner[3].y);
                        PDef->p[3] = OCPoint(m_fX[2], m_fY[2]);
                        PDef->p[4] = OCPoint(m_fX[1], m_fY[1]);
                        PDef->p[5] = OCPoint(m_PCorner[1].x, m_PCorner[1].y);
                        PolygonDefs->push_back( PDef );
                    }
                }
                break;
            }

        }
    }
}

//=======================================================================
/*!
    等高線が４つの格子点（nCorner[0],nCorner[1],nCorner[2],nCorner[3])でできた
    四角形を何点（l）通過するかを調べる

    @param dContLevel  等高線のﾚﾍﾞﾙ
    @param nX          対象とする４つの格子点の場所(左から何番目）　
    @param nY          対象とする４つの格子点の場所(上から何番目）
    @return 通過本数
 */
//=======================================================================
int OCContourImage::CrossPointCount(double dContLevel, int nX, int nY)
{
    // 初期値の設定----------------------------------------------------------
    int nCorner[4];

    nCorner[0] = m_lNx *  nY      +  nX;          // 左上の配列番号
    nCorner[1] = m_lNx *  nY      + (nX + 1);     // 右上の配列番号
    nCorner[2] = m_lNx * (nY + 1) + (nX + 1);     // 右下の配列番号
    nCorner[3] = m_lNx * (nY + 1) +  nX;          // 左下の配列番号

    for( int i = 0; i < 4; ++i )
    {
        m_fCornerZ[i]  = m_fZGridPoint[nCorner[i]];                                      // 格子点のZ値
        m_PCorner[i].x = float(m_fXGridPoint[nCorner[i]] * m_fWdRatio + m_fXOffSet + 0.5); // 格子点のx座標
        m_PCorner[i].y = float(m_fYGridPoint[nCorner[i]] * m_fHgRatio + m_fYOffSet + 0.5); // 格子点のy座標
    }

    // 通過本数検出-----------------------------------------------------------
    int nCpCount = 0; // 等高線が４つの格子点でできた四角形の辺を通過する点数

    for( int i = 0; i < 4; ++i )
    {
        if((m_fCornerZ[(i+0)%4] <= dContLevel && dContLevel <= m_fCornerZ[(i+1)%4]) ||
           (m_fCornerZ[(i+0)%4] >= dContLevel && dContLevel >= m_fCornerZ[(i+1)%4]))
        {
            m_nSide[nCpCount] = i;   // 対象の辺
            switch(i)
            {
            case 0: // 格子点0と格子点1の間を通過　Corner 0 - 1
                m_fX[nCpCount] = static_cast<float>(m_fXGridPoint[nCorner[(i+0)%4]] +
                    (dContLevel - m_fCornerZ[(i+0)%4]) / (m_fCornerZ[(i+1)%4] - m_fCornerZ[(i+0)%4]) * m_fXIntvl);
                m_fY[nCpCount] = static_cast<float>(m_fYGridPoint[nCorner[(i+0)%4]]);
                break;
            case 1: // 格子点1と格子点2の間を通過　Corner 1 - 2
                m_fX[nCpCount] = static_cast<float>(m_fXGridPoint[nCorner[(i+0)%4]]);
                m_fY[nCpCount] = static_cast<float>(m_fYGridPoint[nCorner[(i+0)%4]] +
                    (dContLevel - m_fCornerZ[(i+0)%4]) / (m_fCornerZ[(i+1)%4] - m_fCornerZ[(i+0)%4]) * m_fYIntvl);
                break;
            case 2: // 格子点2と格子点3の間を通過　Corner 2 - 3
                m_fX[nCpCount] = static_cast<float>(m_fXGridPoint[nCorner[(i+1)%4]] +
                    (dContLevel - m_fCornerZ[(i+1)%4]) / (m_fCornerZ[(i+0)%4] - m_fCornerZ[(i+1)%4]) * m_fXIntvl);
                m_fY[nCpCount] = static_cast<float>(m_fYGridPoint[nCorner[(i+0)%4]]);
                break;
            case 3: // 格子点3と格子点0の間を通過　Corner 3 - 0
                m_fX[nCpCount] = static_cast<float>(m_fXGridPoint[nCorner[(i+0)%4]]);
                m_fY[nCpCount] = static_cast<float>(m_fYGridPoint[nCorner[(i+1)%4]] +
                    (dContLevel - m_fCornerZ[(i+1)%4]) / (m_fCornerZ[(i+0)%4] - m_fCornerZ[(i+1)%4]) * m_fYIntvl);
                break;
            }
            m_fX[nCpCount] = static_cast<float>(m_fX[nCpCount] * m_fWdRatio + m_fXOffSet);    // 等高線が４つの格子点でできた四角形の辺を通過するx座標
            m_fY[nCpCount] = static_cast<float>(m_fY[nCpCount] * m_fHgRatio + m_fYOffSet);    // 等高線が４つの格子点でできた四角形の辺を通過するy座標
            nCpCount++;
        }
    }
    return nCpCount;   // 通過本数
}

//===========================================================================
//!
/*!
    @date   2005-11-14作成(Wada)
*/
//===========================================================================
double OCContourImage::ZLevel(int x, int y)
{
    return m_fZGridPoint[m_lNx * y + x];
}

//===========================================================================
/*!
    補間計算
 */
//===========================================================================
void OCContourImage::Interpolation(int nDistr)
{
    // 初期値の設定----------------------------------------------------------
    int     i, j;                // 汎用ﾙｰﾌﾟｶｳｳﾝﾀ
    double  dRequest;               // 補間で求めたい位置

    long    lSx, lSy;
    lSx = (m_lNx - 1) * nDistr + 1;
    lSy = (m_lNy - 1) * nDistr + 1;

    // classの内部変数に格納
    m_lGridCount = lSx * lSy;       // 格子点の総数

    double  *dXTempPoint, *dYTempPoint, *dZTempPoint;
    dXTempPoint = new double[m_lGridCount];
    dYTempPoint = new double[m_lGridCount];
    dZTempPoint = new double[m_lGridCount];

    // X方向の補間-----------------------------------------------------------
    for(j = 0; j < m_lNy; j++)
    {
        OCInterpolate* Inter = new OCSpline();
        Inter->Init( m_lNx, &m_fXGridPoint[m_lNx * j], &m_fZGridPoint[m_lNx * j]);
        Inter->Prepare();

        for(i = 0; i < lSx; i++)
        {
            dRequest                          = m_fXGridMin + m_fXIntvl / double(nDistr) * double(i);  // 補間で求めるｘ値
            dXTempPoint[lSx * nDistr * j + i] = dRequest;
            dYTempPoint[lSx * nDistr * j + i] = m_fYGridPoint[m_lNx * j];
            dZTempPoint[lSx * nDistr * j + i] = Inter->Calc( dRequest );
        }

        delete Inter;
    }

    // Y方向の補間-----------------------------------------------------------
    double *dYTemp, *dZTemp;
    dYTemp = new double[m_lNy];
    dZTemp = new double[m_lNy];

    for(j = 0; j < lSx; j++)
    {
        // Y値（縦並び）の配列を作る--------------------------------------------
        for(i = 0; i < m_lNy; i++)
        {
            dYTemp[i] = dYTempPoint[lSx * nDistr * i + j];
            dZTemp[i] = dZTempPoint[lSx * nDistr * i + j];
        }

        OCInterpolate* Inter = new OCSpline();
        Inter->Init( m_lNx, &dYTemp[0], &dZTemp[0]);
        Inter->Prepare();

        for(i = 0; i < lSy; i++)
        {
            dRequest                 = m_fYGridMin + m_fYIntvl / double(nDistr) * double(i);  // 補間で求めるY値
            dXTempPoint[lSx * i + j] = dXTempPoint[j];
            dYTempPoint[lSx * i + j] = dRequest;
            dZTempPoint[lSx * i + j] = Inter->Calc( dRequest );
        }

        delete Inter;
    }

    delete[] dZTemp;
    delete[] dYTemp;

    // 補間後のｸﾞﾘｯﾄﾞ数でﾒﾓﾘを再確保realloc-------------------------------------
    delete[] m_fXGridPoint;
    delete[] m_fYGridPoint;
    delete[] m_fZGridPoint;

    m_fXGridPoint = new double[m_lGridCount];
    m_fYGridPoint = new double[m_lGridCount];
    m_fZGridPoint = new double[m_lGridCount];

    // ﾃﾝﾎﾟﾗﾘに入れてある補間ﾃﾞｰﾀをｸﾞﾘｯﾄﾞにｺﾋﾟｰ---------------------------------
    for( i = 0; i < m_lGridCount; i++)
    {
        m_fXGridPoint[i] = dXTempPoint[i];
        m_fYGridPoint[i] = dYTempPoint[i];
        m_fZGridPoint[i] = dZTempPoint[i];
    }

    // ﾃﾝﾎﾟﾗﾘ補間ﾃﾞｰﾀのﾒﾓﾘを開放------------------------------------------------
    delete[] dXTempPoint;
    delete[] dYTempPoint;
    delete[] dZTempPoint;

    // ｸﾞﾘｯﾄﾞ数とｸﾞﾘｯﾄﾞ間隔を補間値にする---------------------------------------
    m_lNx = lSx;                    // x方向ｸﾞﾘｯﾄﾞ数(右端部分のｺﾝﾀｰﾏｯﾌﾟが描けないので+1)
    m_lNy = lSy;                    // y方向ｸﾞﾘｯﾄﾞ数(上端部分のｺﾝﾀｰﾏｯﾌﾟが描けないので+1)
    m_fXIntvl = (m_fXGridMax - m_fXGridMin) / (lSx - 1);    // x座標間隔(右端+1の修正で-1)
    m_fYIntvl = (m_fYGridMax - m_fYGridMin) / (lSy - 1);    // y座標間隔(上端+1の修正で-1)
}

//*************************************************************************************************

//! スライスレベルを設定します
void OCContourImage::SetSliceLevel(double* levels, size_t count)
{
	m_sliceLevels.clear();
	if (count != 0) {
		m_sliceLevels.assign(levels, levels + count);
	}
}

//! コンターマップの画像を作成します
void OCContourImage::CreateImage(double zMin, double zMax, double zDiv, bool /*gradation*/)
{
	if (m_calculation) { return; }

	bool const first = m_slices.empty();
	m_zMin = zMin;
	m_zMax = zMax;
	m_zDiv = zDiv;

	if (!m_image.IsNull()) {
		m_image.Destroy();
	}

	if (m_width == 0 || m_height == 0) {
		m_width = 50;
		m_height = 50;
	}

	m_imageCreation = true;
	m_image.Create(m_width, m_height, 24);
	CDC dc; dc.Attach(m_image.GetDC());

	{
		CRect r(0, 0, std::abs(m_width), std::abs(m_height));
		CBrush br; br.CreateSolidBrush(m_pattern->GetColor(zMin));
		dc.FillRect(r, br);
	}

	std::vector<OPolygonDefX*> arr;
	if (m_gradation) {
		int count = 128;
		double div = (zMax - zMin) / count;
		for (int i = 0; i < count; ++i) {
			double level = i * div + zMin;
			DrawContPaint(level, m_pattern->GetColor(level), &arr);
		}
	}
	else {
		BOOST_FOREACH(double l, m_sliceLevels) {
			DrawContPaint(l, m_pattern->GetColor(l), &arr);
		}
	}

	CPen pen; pen.CreatePen(PS_NULL, 1, RGB(0, 0, 0));
	HPEN oldPen = dc.SelectPen(pen);
	CPoint pts[6];
	for(std::size_t i = 0; i < arr.size(); ++i) {
		OPolygonDefX* PDef = arr[i];

		for(int j = 0; j < PDef->size; ++j) {
			pts[j].x = int(PDef->p[j].x);
			pts[j].y = int(PDef->p[j].y);
		}

		CBrush br; br.CreateSolidBrush(PDef->color);
		HBRUSH old = dc.SelectBrush(br);
		dc.Polygon(pts, PDef->size);
		dc.SelectBrush(old);

		delete PDef;
	}
	dc.SelectPen(oldPen);

	// 等高線ライン
	if (m_slices.empty() || !m_levelModified) {
		std::vector<boost::shared_ptr<OCSliceLevel>> cache = m_slices;
		m_slices.clear();

		// なければ作る
		for (size_t j = 0, n = m_sliceLevels.size(); j < n; ++j) {
			double const l = m_sliceLevels[j];
			std::vector<OCLineDefX*> LineDefs;
			DrawContLine(0x000000, l, &LineDefs);
			std::vector<OPolygonDefX*> polys = LineToPolyline(LineDefs);
			boost::shared_ptr<OCSliceLevel> level(new OCSliceLevel(l, m_pattern->GetColor(l), this));
			for (size_t i = 0; i < polys.size(); ++i) {
				OPolygonDefX* def = polys[i];

				level->AddPoints(def->p, def->size, m_width, m_height);
			}

			if (j < cache.size()) {
				level->SetLineColor(cache[j]->GetLineColor());
				level->SetLevelVisible(cache[j]->IsLevelVisible());
				level->SetFilling(cache[j]->IsFilling());
				level->SetDigit(cache[j]->GetDigit());
				level->SetFillColor(m_levelPaintColor);
			}
			else {
				level->SetLineColor(m_lineColor);
				level->SetLevelVisible(m_showValue);
			}

			m_slices.push_back(level);

			std::for_each(polys.begin(), polys.end(), &boost::checked_delete<OPolygonDefX>);
			std::for_each(LineDefs.begin(), LineDefs.end(), &boost::checked_delete<OCLineDefX>);
		}
	}
	else {
		// すでに存在していれば座標だけ更新する
		BOOST_FOREACH(boost::shared_ptr<OCSliceLevel> const& level, m_slices) {
			{
				std::vector<OCLineDefX*> LineDefs;
				DrawContLine(0x000000, level->GetLevel(), &LineDefs);
				std::vector<OPolygonDefX*> polys = LineToPolyline(LineDefs);

				level->ClearPoints();
				for (size_t i = 0; i < polys.size(); ++i) {
					OPolygonDefX* def = polys[i];
					level->AddPoints(def->p, def->size, m_width, m_height);
				}

				std::for_each(polys.begin(), polys.end(), &boost::checked_delete<OPolygonDefX>);
				std::for_each(LineDefs.begin(), LineDefs.end(), &boost::checked_delete<OCLineDefX>);
			}
			level->SetLevelColor(m_pattern->GetColor(level->GetLevel()));
		}
	}

	dc.Detach();
	m_image.ReleaseDC();

	if (first) {
		std::for_each(m_slices.begin(), m_slices.end(), boost::bind(&OCSliceLevel::SetLineColor, _1, m_lineColor));
	}
	m_imageCreation = false;
}

void OCContourImage::Click(OCRect const& rect, double x, double y)
{
	//m_selectLevel.reset();
	//BOOST_FOREACH(boost::shared_ptr<OCSliceLevel> const& level, m_slices) {
	//	if (level->HitTest(rect, x, y)) {
	//		m_selectLevel = level;
	//	}
	//}
}

bool OCContourImage::Drag(ICanvas* g, OCRect const& rect, double x, double y)
{
	//if (m_selectLevel) {
	//	m_selectLevel->DrawSelectRect(g);
	//	m_selectLevel->Move(rect, x, y);
	//	m_selectLevel->DrawSelectRect(g);
	//	m_levelModified = true;
	//	return true;
	//}
	return false;
}

//! 作成済みのコンターマップのイメージをHDCに転送します
void OCContourImage::BitBlt(HDC hdc, int x, int y, int width, int height, DWORD rop) const
{
	_ASSERT(!m_image.IsNull());
	if (!m_image.IsNull()) {
		m_image.StretchBlt(hdc, x, y, width, height, rop);
	}
}

#include "ICanvas.h"
#include "property.h"
#include "PropertyCreator.hpp"
void OCContourImage::DrawImage(ICanvas* g, double x, double y, double width, double height, short alpha) const
{
	if (m_imageCreation) { return; }

	// 塗り潰さない場合はイメージを描画しない
	// 選択中の場合も
	if (m_fillType != ContourFilling::OFF && !m_selected) {
		if (m_fillType == ContourFilling::NoMinPaint) {
			// 最小値以下塗りつぶしOFF
			// 最小値の色を透過色にすることで対応
			g->DrawTransparentBitmap(m_image, x, y, width, height, alpha, m_pattern->GetColorByIndex(0));
		}
		else {
			g->DrawBitmap(m_image, x, y, width, height, alpha);
		}
	}

	// 等高線は別途描く(ビットマップ内に描いておくと拡大縮小時に荒くなるので)
	if (m_sliceType != 2) {
		g->SetFont(&m_levelFont);
		g->SetFontColor(m_levelFontColor);

		if (!m_selected) {
			// 最初に等高線のレベル値テキストだけをまとめて描画する
			// 等高線を描画する際に、すべてのレベル値テキストとのかぶり判定を行うため
			// すべてのレベル値テキストの座標を等高線を引く前に確定しておく必要がある
			BOOST_FOREACH(boost::shared_ptr<OCSliceLevel> const& level, m_slices) {
				if (m_fillType == ContourFilling::NoMinPaint) {
					if (level->GetLevel() <= m_zMin) {
						// 最小値以下塗りつぶしOFFの時、最小値より下のラインも描画しない
						continue;
					}
				}
				level->DrawLevelText(g, x, y, width, height, m_sliceType == 1);
			}

			BOOST_FOREACH(boost::shared_ptr<OCSliceLevel> const& level, m_slices) {
				if (m_fillType == ContourFilling::NoMinPaint) {
					if (level->GetLevel() <= m_zMin) {
						// 最小値以下塗りつぶしOFFの時、最小値より下のラインも描画しない
						continue;
					}
				}
				level->Draw(g, x, y, width, height, m_sliceType == 1);
			}
		}

		g->SetFontColor(0); // 他の個所でまだ対応していないので元に戻しておく
	}
}

//! 当たり判定
/*!
	@return	等高線があれば、等高線上にマウスがある場合, そうでなければ矩形座標内に(x, y)があればtrueを返します
			どちらにもあてはまらなければfalseを返します
*/
bool OCContourImage::HitTest(OCRect const& rect, double x, double y) const
{
	bool levelVisible = false;
	if (m_sliceType == 0 || (m_sliceType == 1 && m_fillType == 1)) {
		for (size_t i = 0; i < m_slices.size(); ++i) {
			if ((levelVisible = m_slices[i]->IsVisible()) != false) {
				break;
			}
		}
	}

	if (!levelVisible) {
		return rect.Contains(x, y);
	}

	BOOST_FOREACH(boost::shared_ptr<OCSliceLevel> const& level, m_slices) {
		if (level->IsVisible() && level->HitTestLine(rect, x, y)) { return true; }
	}
	return false;
}

//! p1p2を端点とする線分がすべてのレベル値テキストとかぶるかを判定
bool OCContourImage::IntersectAllSliceLevelText(OCPoint const& p1, OCPoint const& p2)
{
	BOOST_FOREACH(boost::shared_ptr<OCSliceLevel> const& level, m_slices) {
		if( level->IntersectLevelText(p1,p2) ){
			return true;
		}
	}
	return false;
}

//! 領域rがすべてのレベル値テキストとかぶるかを判定
bool OCContourImage::IntersectAllSliceLevelText(OCRect const& r)
{
	BOOST_FOREACH(boost::shared_ptr<OCSliceLevel> const& level, m_slices) {
		if( level->IntersectLevelText(r) ){
			return true;
		}
	}
	return false;
}

OCRect OCContourImage::GetRect() const
{
	if (IShape* s = dynamic_cast<IShape*>(m_plot)) {
		OCRect r;
		s->GetRect(&r.left, &r.top, &r.right, &r.bottom);
		return r;
	}
	return OCRect();
}

//! スライスレベルが作られた
void OCContourImage::CreatedSliceLevel(OCSliceLevel* slice) const
{
	AddSliceLevel(slice);
}
//! スライスレベルが削除された
void OCContourImage::DeletedSliceLevel(OCSliceLevel* slice) const
{
	RemoveSliceLevel(slice);
}

void OCContourImage::AddProperty(IPropertyGroup* prop, bool readOnly, bool plotMode)
{
	{
		IPropertyGroup* grp = prop->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SLICE_LEVEL"));
		grp->AddCustomDialogProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SLICE_LEVEL"), PI_CONTOUR_SLICE_SETTING,
			boost::lexical_cast<std::wstring>(static_cast<unsigned int>(m_slices.size())).c_str(), 0, readOnly || !plotMode);
		grp->AddBoolProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_LEVEL_VALUE_INDICATION"), PI_CONTOUR_SLICE_VALUE, m_showValue, readOnly);
		/*if (m_showValue)*/ {
			bool disableValue = readOnly || !m_showValue;
			m_levelFontProp = m_levelFont;
			m_levelFontProp.lfWidth = m_levelFontColor;
			grp->AddFontProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_FONT"), PI_FONT_TYPE, m_levelFontProp, disableValue);

			std::wstring decimalProp1 = GetResourceString(m_plot, "ID_SOURCE_TEXT_AUTO");
			std::wstring decimalProp2 = GetResourceString(m_plot, "ID_RESOURCE_0");
			std::wstring decimalProp3 = GetResourceString(m_plot, "ID_RESOURCE_1");
			std::wstring decimalProp4 = GetResourceString(m_plot, "ID_RESOURCE_2");
			std::wstring decimalProp5 = GetResourceString(m_plot, "ID_RESOURCE_3");
			std::wstring decimalProp6 = GetResourceString(m_plot, "ID_RESOURCE_4");
			std::wstring decimalProp7 = GetResourceString(m_plot, "ID_RESOURCE_FIVE");
			grp->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_DECIMAL_DIGITS"), PI_CONTOUR_SLICE_DIGIT, m_levelDigit, disableValue, 7, 
				decimalProp1.c_str(), decimalProp2.c_str(), decimalProp3.c_str(), decimalProp4.c_str(), decimalProp5.c_str(), decimalProp6.c_str(), decimalProp7.c_str());

			grp->AddBoolProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_PAINT_FILL"), PI_CONTOUR_SLICE_FILLING, m_levelFilling, disableValue);
			grp->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_PAINT_FILL_COLOR"), PI_CONTOUR_SLICE_PAINTCOLOR, m_levelPaintColor, !m_levelFilling || disableValue);
		}
	}
	{
		IPropertyGroup* grp = prop->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_MAP_OPERATION"));

		std::wstring latticeProp1 = GetResourceString(m_plot, "ID_SOURCE_TEXT_COARSE");
		std::wstring latticeProp2 = GetResourceString(m_plot, "ID_SOURCE_TEXT_SLIGHTLY_COARSE");
		std::wstring latticeProp3 = GetResourceString(m_plot, "ID_SOURCE_TEXT_NORMAL");
		std::wstring latticeProp4 = GetResourceString(m_plot, "ID_SOURCE_TEXT_SLIGHTLY_PARTICULAR");
		std::wstring latticeProp5 = GetResourceString(m_plot, "ID_SOURCE_TEXT_PARTICULAR");
		grp->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_LATTICE"), PI_CONTOUR_PM, m_mesh, readOnly, 5, 
			latticeProp1.c_str(), latticeProp2.c_str(), latticeProp3.c_str(), latticeProp4.c_str(), latticeProp5.c_str());

		std::wstring surfaceProp1 = GetResourceString(m_plot, "ID_SOURCE_TEXT_SMOOTH");
		std::wstring surfaceProp2 = GetResourceString(m_plot, "ID_SOURCE_TEXT_SLIGHTLY_SMOOTH");
		std::wstring surfaceProp3 = GetResourceString(m_plot, "ID_SOURCE_TEXT_NORMAL");
		std::wstring surfaceProp4 = GetResourceString(m_plot, "ID_SOURCE_TEXT_SLIGHTLY_SHARP");
		std::wstring surfaceProp5 = GetResourceString(m_plot, "ID_SOURCE_TEXT_SHARP");
		grp->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_CURVED_SURFACE"), PI_CONTOUR_ALPHA, m_alpha, readOnly, 5, 
			surfaceProp1.c_str(), surfaceProp2.c_str(), surfaceProp3.c_str(), surfaceProp4.c_str(), surfaceProp5.c_str());
	}
}

void OCContourImage::AddTabProperty(IPropertyGroup* prop, bool readOnly, bool plotMode)
{
	{
		IPropertyGroup* grp = prop->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SLICE_LEVEL"));
		grp->AddCustomDialogProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SLICE_LEVEL"), PI_CONTOUR_SLICE_SETTING,
			boost::lexical_cast<std::wstring>(static_cast<unsigned int>(m_slices.size())).c_str(), 0, readOnly || !plotMode);
		grp->AddBoolProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_LEVEL_VALUE_INDICATION"), PI_CONTOUR_SLICE_VALUE, m_showValue, readOnly);
		/*if (m_showValue)*/ {
			bool disableValue = readOnly || !m_showValue;
			m_levelFontProp = m_levelFont;
			m_levelFontProp.lfWidth = m_levelFontColor;
			grp->AddFontProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_FONT"), PI_FONT_TYPE, m_levelFontProp, disableValue);

			std::wstring decimalProp1 = GetResourceString(m_plot, "ID_SOURCE_TEXT_AUTO");
			std::wstring decimalProp2 = GetResourceString(m_plot, "ID_RESOURCE_0");
			std::wstring decimalProp3 = GetResourceString(m_plot, "ID_RESOURCE_1");
			std::wstring decimalProp4 = GetResourceString(m_plot, "ID_RESOURCE_2");
			std::wstring decimalProp5 = GetResourceString(m_plot, "ID_RESOURCE_3");
			std::wstring decimalProp6 = GetResourceString(m_plot, "ID_RESOURCE_4");
			std::wstring decimalProp7 = GetResourceString(m_plot, "ID_RESOURCE_FIVE");
			grp->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_DECIMAL_DIGITS"), PI_CONTOUR_SLICE_DIGIT, m_levelDigit, disableValue, 7, 
				decimalProp1.c_str(), decimalProp2.c_str(), decimalProp3.c_str(), decimalProp4.c_str(), decimalProp5.c_str(), decimalProp6.c_str(), decimalProp7.c_str());
			grp->AddBoolProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_PAINT_FILL"), PI_CONTOUR_SLICE_FILLING, m_levelFilling, disableValue);
			grp->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_PAINT_FILL_COLOR"), PI_CONTOUR_SLICE_PAINTCOLOR, m_levelPaintColor, !m_levelFilling || disableValue);
		}
	}
	{
		IPropertyGroup* grp = prop->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_MAP_OPERATION"));

		std::wstring latticeProp1 = GetResourceString(m_plot, "ID_SOURCE_TEXT_COARSE");
		std::wstring latticeProp2 = GetResourceString(m_plot, "ID_SOURCE_TEXT_SLIGHTLY_COARSE");
		std::wstring latticeProp3 = GetResourceString(m_plot, "ID_SOURCE_TEXT_NORMAL");
		std::wstring latticeProp4 = GetResourceString(m_plot, "ID_SOURCE_TEXT_SLIGHTLY_PARTICULAR");
		std::wstring latticeProp5 = GetResourceString(m_plot, "ID_SOURCE_TEXT_PARTICULAR");
		grp->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_LATTICE"), PI_CONTOUR_PM, m_mesh, readOnly, 5, 
			latticeProp1.c_str(), latticeProp2.c_str(), latticeProp3.c_str(), latticeProp4.c_str(), latticeProp5.c_str());

		std::wstring surfaceProp1 = GetResourceString(m_plot, "ID_SOURCE_TEXT_SMOOTH");
		std::wstring surfaceProp2 = GetResourceString(m_plot, "ID_SOURCE_TEXT_SLIGHTLY_SMOOTH");
		std::wstring surfaceProp3 = GetResourceString(m_plot, "ID_SOURCE_TEXT_NORMAL");
		std::wstring surfaceProp4 = GetResourceString(m_plot, "ID_SOURCE_TEXT_SLIGHTLY_SHARP");
		std::wstring surfaceProp5 = GetResourceString(m_plot, "ID_SOURCE_TEXT_SHARP");
		grp->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_CURVED_SURFACE"), PI_CONTOUR_ALPHA, m_alpha, readOnly, 5, 
			surfaceProp1.c_str(), surfaceProp2.c_str(), surfaceProp3.c_str(), surfaceProp4.c_str(), surfaceProp5.c_str());
		//grp->AddArrayProperty(_T("外挿補間"), PI_CONTOUR_EXTRAPOLATION, m_extrapolation.underlying(), readOnly, 4, _T("全体"), _T("X方向"), _T("Y方向"), _T("無し"));
	}
}
bool OCContourImage::SetProperty(long id, LPARAM value, LPARAM* source, bool& reCalc)
{
	reCalc = false;
	switch (id)
	{
	case PI_CONTOUR_PM:
		::SetPropertyHelper(m_mesh, value, source);
		UpdateMesh();
		m_modify = reCalc = true;
		return true;
	case PI_CONTOUR_ALPHA:
		::SetPropertyHelper(m_alpha, value, source);
		UpdateAlpha();
		m_modify = reCalc = true;
		return true;
	case PI_CONTOUR_EXTRAPOLATION:
		{
			//::SetPropertyHelper(m_extrapolation, value, source);
			int n = m_extrapolation.underlying();
			::SetPropertyHelper(n, value, source);
			m_extrapolation = static_cast<ExtrapolationType_def::type>(n);
		}
		//m_modify = reCalc = true;
		return true;
	case PI_CONTOUR_SLICE_VALUE:
		::SetPropertyHelper(m_showValue, value, source);
		std::for_each(m_slices.begin(), m_slices.end(), boost::bind(&OCSliceLevel::SetLevelVisible, _1, m_showValue));
		return true;
	case PI_CONTOUR_SLICE_PAINTCOLOR:
		::SetPropertyHelper(m_levelPaintColor, value, source);
		std::for_each(m_slices.begin(), m_slices.end(), boost::bind(&OCSliceLevel::SetFillColor, _1, m_levelPaintColor));
		return true;
	case PI_CONTOUR_SLICE_DIGIT:
		::SetPropertyHelper(m_levelDigit, value, source);
		std::for_each(m_slices.begin(), m_slices.end(), boost::bind(&OCSliceLevel::SetDigit, _1, m_levelDigit));
		return true;
	case PI_CONTOUR_SLICE_FILLING:
		::SetPropertyHelper(m_levelFilling, value, source);
		std::for_each(m_slices.begin(), m_slices.end(), boost::bind(&OCSliceLevel::SetFilling, _1, m_levelFilling));
		return true;
	case PI_FONT_TYPE:
		{
			m_levelFont = *reinterpret_cast<LOGFONT*>(value);
			m_levelFontColor = m_levelFont.lfWidth; // Widthに色が入っている...
			m_levelFont.lfWidth = 0;
		}
		return true;
	case PI_CONTOUR_LINECOLOR:
		{
			::SetPropertyHelper(m_lineColor, value, 0);
			std::for_each(m_slices.begin(), m_slices.end(), boost::bind(&OCSliceLevel::SetLineColor, _1, m_lineColor));
		}
		return true;
	case PI_CONTOUR_SLICE_SETTING:
		{
			if (source) { GetSliceLevelSetting(*reinterpret_cast<ShapeSliceList*>(source)); }
			ShapeSliceList* setting = reinterpret_cast<ShapeSliceList*>(value);
			UpdateSliceLevel(setting->levels, (int)setting->size, setting->legendIndex, !!setting->limitedRange, !!setting->autoSetting);
		}
		return true;
	case PI_CONTOUR_SLICE_SETTING_DIRECT:
		{
			ShapeSliceList* setting = reinterpret_cast<ShapeSliceList*>(value);
			UpdateSliceLevel(setting->levels, (int)setting->size, setting->legendIndex, !!setting->limitedRange, !!setting->autoSetting);
		}
		return false;
	case PI_CONTOUR_SLICELINE_ALL_STYLE:
		if (!m_levelModified) {
			int style;
			::SetPropertyHelper(style, value, source);
			std::for_each(m_slices.begin(), m_slices.end(), boost::bind(&OCSliceLevel::SetLineStyle, _1, style));
		}
		return true;
	case PI_CONTOUR_SLICELINE_ALL_COLOR:
		if (!m_levelModified) {
			::SetPropertyHelper(m_lineColor, value, source);
			std::for_each(m_slices.begin(), m_slices.end(), boost::bind(&OCSliceLevel::SetLineColor, _1, m_lineColor));
		}
		return true;
	case PI_CONTOUR_SLICELINE_ALL_WIDTH:
		if (!m_levelModified) {
			float width;
			::SetPropertyHelper(width, value, source);
			std::for_each(m_slices.begin(), m_slices.end(), boost::bind(&OCSliceLevel::SetLineWidth, _1, width));
		}
		return true;
	case PI_CONTOUR_SLICELINE_ALL_ROUNDTYPE:
		//CreateProperty未実装
		return true;
	case PI_CONTOUR_SLICELEVE_MODIFIED:
		::SetPropertyHelper(m_levelModifiedNotCopy, value, source);
		return true;
	}

	return false;
}

bool OCContourImage::GetProperty(long id, LPARAM* ret) const
{
	switch (id)
	{
	case PI_CONTOUR_SLICE_SETTING:
		{
			ShapeSliceList& setting = *reinterpret_cast<ShapeSliceList*>(ret);
			GetSliceLevelSetting(setting);
		}
		return true;
	}
	return false;
}

void OCContourImage::GetSliceLevelSetting(ShapeSliceList& setting) const
{
	std::vector<SliceLevel> levels;
	for (size_t i = 0; i < m_slices.size(); ++i) {
		SliceLevel level;
		level.visible = m_slices[i]->IsVisible();
		level.level = m_slices[i]->GetLevel();
		level.showLevel = m_slices[i]->IsLevelVisible();
		level.style = m_slices[i]->GetLineStyle();
		level.width = m_slices[i]->GetLineWidth();
		level.color = m_slices[i]->GetLineColor();
		levels.push_back(level);
	}

	setting.plot = dynamic_cast<IShape*>(m_plot);
	std::copy(levels.begin(), levels.end(), setting.levels);
	setting.size = static_cast<int>(levels.size());
	setting.minimum = m_zMin;
	setting.maximum = m_zMax;
	setting.division = m_zDiv;
	setting.legendIndex = m_legendIndex;
	setting.limitedRange = m_limitedRange;
	setting.autoSetting = !m_levelModified;
}

void OCContourImage::UpdateSliceLevel(SliceLevel* levels, int size, size_t legendIndex, bool limitedRange, bool autoSetting)
{
	m_sliceLevels.clear();
	if (levels && size > 0) {
		for (int i = 0; i < size; ++i) {
			m_sliceLevels.push_back(levels[i].level);
		}

		m_modify = true; // マップを再計算する
		m_levelModified = false; // スライスレベルを作り直す
		Interpolation(m_data, m_scaleInterval, m_rect, true, false);

		// 全レベルのレベル値表示が無効だったらコンターの「レベル値表示」もOFFにする
		bool all_false = true, all_true = true;
		for (int i = 0; i < size; ++i) {
			boost::shared_ptr<OCSliceLevel>& sliceLevel = m_slices[i];
			sliceLevel->SetVisible(levels[i].visible);
			sliceLevel->SetLevelVisible(levels[i].showLevel);
			sliceLevel->SetLineStyle(levels[i].style);
			sliceLevel->SetLineColor(levels[i].color);
			sliceLevel->SetLineWidth(levels[i].width);
			
			if (levels[i].showLevel) { all_false = false; }
			else { all_true = false; }
		}
		if (all_false) {
			m_showValue = false;
		}
		else if (all_true) {
			m_showValue = true;
		}
		m_legendIndex = legendIndex;
		m_limitedRange = limitedRange;
	}

	// 編集フラグを立てる(以降データ項目などを変更してもレベル値が自動的に更新されなくなる)
	m_levelModified = !autoSetting;
}

void OCContourImage::UpdateMesh()
{
	int mesh[] = {5, 10, 20, 30, 50};
	//SetGrid(mesh[m_mesh], mesh[m_mesh]);
    m_lNx = mesh[m_mesh];
    m_lNy = mesh[m_mesh];
    m_lGridCount = m_lNx * m_lNy;   // 格子点の総数
}

void OCContourImage::UpdateAlpha()
{
	double alpha[] = {2, 8, 32, 128, 1024,};
	m_dAlpha = alpha[m_alpha];
}

//!
void OCContourImage::CopyFrom(IMapImage* src)
{
	if (OCContourImage* img = dynamic_cast<OCContourImage*>(src)) {
		m_mesh = img->m_mesh;
		m_alpha = img->m_alpha;
		m_extrapolation = img->m_extrapolation;
		m_levelFont = img->m_levelFont;
		m_levelFontColor = img->m_levelFontColor;
		m_lineColor = img->m_lineColor;
		m_showValue = img->m_showValue;
		m_levelDigit = img->m_levelDigit;
		m_levelFilling = img->m_levelFilling;
		m_levelPaintColor = img->m_levelPaintColor;
		m_legendIndex = img->m_legendIndex;
		if (!m_levelModifiedNotCopy) {
			m_levelModified = img->m_levelModified;
		}

		for (size_t i = 0; i < img->m_slices.size(); ++i) {
			boost::shared_ptr<OCSliceLevel> ptr(img->m_slices[i]->Clone(this));
			m_slices.push_back(ptr);
		}
	}
}

void OCContourImage::Serialize(ISerializer* ar)
{
	if (dynamic_cast<IReadable*>(ar)) {
		m_serializing = true;
	}

	ar->Serialize(_T("Mesh"), m_mesh, 2);
	ar->Serialize(_T("Alpha"), m_alpha, 2);
	/*{
		int n = m_extrapolation.underlying();
		ar->Serialize(_T("Extrapolation"), n, n);
		m_extrapolation = static_cast<ExtrapolationType_def::type>(n);
	}*/
	ar->Serialize(_T("ShowLevel"), m_showValue);
	ar->Serialize(_T("LevelDigit"), m_levelDigit);
	ar->Serialize(_T("LevelFilling"), m_levelFilling);
	ar->Serialize(_T("LevelPaintColor"), m_levelPaintColor);
	ar->Serialize(_T("LevelFont"), m_levelFont);
	ar->Serialize(_T("LevelFontColor"), m_levelFontColor);
	ar->Serialize(_T("LineColor"), m_lineColor);
	ar->Serialize(_T("LegendIndex"), m_legendIndex);
	ar->Serialize(_T("LevelModified"), m_levelModified, true);
	ar->Serialize(_T("LimitedRange"), m_limitedRange, true);

	bool old; ar->Serialize(_T("Old"), old);
	if (old) {
		int meshNum, alphaNum;
		ar->Serialize(_T("MeshNum"), meshNum);
		ar->Serialize(_T("AlphaNum"), alphaNum);

		int meshs[] = {5, 10, 20, 30, 50};
		int alphas[] = {2, 8, 32, 128, 1024,};
		for (int i = 0; i < _countof(meshs); ++i) {
			if (meshs[i] == meshNum) {
				m_mesh = i;
				break;
			}
		}
		for (int i = 0; i < _countof(alphas); ++i) {
			if (alphas[i] == alphaNum) {
				m_alpha = i;
				break;
			}
		}
	}

	ISerializer* child = ar->ChildNode(_T("SliceLevels"));
	if (IReadable* reader = dynamic_cast<IReadable*>(child)) {
		m_slices.clear();
		for (size_t i = 0, n = reader->ChildCount(); i < n; ++i) {
			ISerializer* levelNode = reader->ChildNode(i);

			double value;		levelNode->Serialize(_T("Level"), value);
			COLORREF color;		levelNode->Serialize(_T("Color"), color);

			boost::shared_ptr<OCSliceLevel> level(new OCSliceLevel(value, color, this));
			level->Serialize(levelNode);
			m_slices.push_back(level);
		}
	}
	else {
		BOOST_FOREACH(boost::shared_ptr<OCSliceLevel> const& level, m_slices) {
			level->Serialize(child->ChildNode(_T("SliceLevel")));
		}
	}

	UpdateMesh();
	UpdateAlpha();
}

void OCContourImage::AfterSerialize()
{
	std::for_each(m_slices.begin(), m_slices.end(), boost::bind(&OCSliceLevel::SetFillColor, _1, m_levelPaintColor));
	std::for_each(m_slices.begin(), m_slices.end(), boost::bind(&OCSliceLevel::SetDigit, _1, m_levelDigit));
	std::for_each(m_slices.begin(), m_slices.end(), boost::bind(&OCSliceLevel::SetFilling, _1, m_levelFilling));
}

bool OCContourImage::IsEmpty() const
{
	return m_image.IsNull();
}

void OCContourImage::Destroy()
{
	if (!m_image.IsNull()) {
		m_image.Destroy();
	}
	m_modify = true;
}

COLORREF OCContourImage::GetSelectLineColor() const
{
	return m_slices.size() <= m_legendIndex ? 0 : m_slices[m_legendIndex]->GetLineColor();
}

float OCContourImage::GetSelectLineWidth() const
{
	return m_slices.size() <= m_legendIndex ? 0 : m_slices[m_legendIndex]->GetLineWidth();
}

int OCContourImage::GetSelectLineStyle() const
{
	return m_slices.size() <= m_legendIndex ? 0 : m_slices[m_legendIndex]->GetLineStyle();
}
