//===========================================================================
/*! @file
    @date   2005-07-13
    @author wada
*/
//===========================================================================
#pragma once
#define _USE_MATH_DEFINES
#include <cmath>
#include <vector>
#include <boost/bind.hpp>
#include <atltypes.h>
#include "utility.h"
//---------------------------------------------------------------------------
class IPlotData;
//typedef boost::dynamic_bitset<> BitSet;
typedef std::vector<bool> BitSet;
//---------------------------------------------------------------------------

enum ContourClippingType
{
	CCT_NONE,
	CCT_UPPER,
	CCT_LOWER,
	CCT_ALL,
};

enum ContourClippingLineType
{
	CCLT_LINE,
	CCLT_CURVE,
};

//! クリッピングマップのクリッピングポイント計算クラス
class OCCalcClipping
{
    //! X, Yデータを保持する構造体
    struct DataPair
    {
        double X;
        double Y;
        //! Constructor
        DataPair() : X(0), Y(0) {}
        //! Constructor
        DataPair(float X, float Y)
        {
            this->X = X;
            this->Y = Y;
        }
        //! Copy Constructor
        DataPair(const DataPair& d)
            : X(d.X), Y(d.Y)
        {}

        //! 代入演算子
        DataPair& operator= (const DataPair& d)
        {
            this->X = d.X;
            this->Y = d.Y;
            return *this;
        }

        //! 無効値が入っていればtrueを返す
        bool IsInvalid()
        {
            return X == FLT_MAX || Y == FLT_MAX;
        }
    };
    typedef std::vector<DataPair> DataArray;

public: // Public Access Method
    OCCalcClipping();
    ~OCCalcClipping();

	void SetType(ContourClippingType type, bool isAll);
	void SetLineType(ContourClippingLineType type) { m_lineType = type; }
    void SetData(IPlotData* data);
	IPlotData* GetData() const { return m_data; }

    void CalcClipping(double XMax, double XMin, double YMax, double YMin, bool bXLog, bool bYLog, OCRect rcAxis);

    double RotPer() const                  { return m_dRotPer; }
    void SetRotPer(double nRotPer);
    //int Interpolation() const           { return m_nInterpolation; }
    //void SetInterpolation(int nType)    { m_nInterpolation = nType; }

    /*! @name */
    //@{
    //! 指定位置の座標を取得する
    //inline CPoint Point(std::size_t n) const
    //{
    //    return CPoint(X(n), Y(n));
    //}
	//! 指定位置の座標を取得する
	inline OCPoint Point(std::size_t n) const
	{
		return m_ptClip[n];
	}
    //! 指定位置のX座標を取得する
    inline int X(std::size_t n) const
    {
        return static_cast<int>(m_ptClip[n].x);
    }
    //! 指定位置のY座標を取得する
    inline int Y(std::size_t n) const
    {
        return static_cast<int>(m_ptClip[n].y);
    }
    //! クリッピングポイントの数を取得する
    inline std::size_t PointSize() const
    {
        return m_ptClip.size();
    }
    //@}

    int GetClipPointCount() const
    {
		if (m_type == CCT_UPPER && !m_upperData.empty()) { return m_upperData.size(); }
		if (m_type == CCT_LOWER && !m_lowerData.empty()) { return m_lowerData.size(); }
        return static_cast<int>(m_fYData.size());
    }
    const std::vector<OCPoint> GetSectionData() const;
    void SetSectionData(const std::vector<OCPoint>&);

	void SetUpperData(std::vector<OCPoint> const& data) { m_upperData = data; }
	void SetLowerData(std::vector<OCPoint> const& data) { m_lowerData = data; }
	std::vector<OCPoint> GetUpperDataCopy() const;
	std::vector<OCPoint> GetLowerDataCopy() const;

	void ClearInputData() { m_upperData.clear(); m_lowerData.clear(); }

    bool IsPointEffective(std::size_t n){ return m_bCheck[n]; }
    const BitSet& GetEffective() const  { return m_bCheck; }
    void SetEffective(const BitSet& set){ m_bCheck = set; }

    void Clear();

    OCCalcClipping* Clone()
    {
        OCCalcClipping* clone = new OCCalcClipping(*this);
        return clone;
    }

private: // Private Method    
    //! first - last 間の最大値(Y基準)を求める
    template <class Iter>
    inline DataPair& GetMaxData(Iter first, Iter last)
    {
        using boost::bind;
        return *std::max_element(
            first, last, bind(std::less<double>(),
                bind(&DataPair::Y, _1), bind(&DataPair::Y, _2))
        );
    }

    //! first - last 間の最小値(Y基準)を求める
    template <class Iter>
    inline DataPair& GetMinData(Iter first, Iter last)
    {
        using boost::bind;
		return *std::min_element(
            first, last, bind(std::less<double>(),
                bind(&DataPair::Y, _1), bind(&DataPair::Y, _2))
        );
    }

    inline double GetValue(bool bLog, double dVal)
    {
        if (bLog && !dVal) { return 0; }
        return bLog ? std::log10(dVal) : dVal;
    }

    inline void PushData(const DataPair& d)
    {
        m_fXData.push_back(d.X);
        m_fYData.push_back(d.Y);
        m_bCheck.push_back(true);
    }

    void SetEffectiveData(std::vector<double>&, std::vector<double>&);

    void CalcClippingUpper();
	void CalcClippingLower();
	bool CalcCenter();
    bool CalcCurve(DataArray& curve, double, double, bool, bool);
	bool CalcLineInter(DataArray& line, double, double, bool, bool);
    DataArray CalcLine(double, double, bool bXLog, bool bYLog, const std::vector<double>&, const std::vector<double>&);

private: // Private Member
    double m_dRotPer;                  //!< 許容値

    //int m_nInterpolation;           //!< 補間タイプ
    std::vector<OCPoint> m_ptClip;   //!< クリッピング座標

    // 下記配列のサイズは全て同一であるものとします.
    BitSet m_bCheck;                //!< 下記区間を使用するかどうか
    std::vector<double> m_fXData;   //!< 各区間の最大値(X)
    std::vector<double> m_fYData;   //!< 各区間の最大値(Y)

	std::vector<OCPoint> m_upperData;	//!< 外部入力されたデータ(あれば優先的に使用)
	std::vector<OCPoint> m_lowerData;	//!< 外部入力されたデータ(あれば優先的に使用)

	ContourClippingType m_type;
	ContourClippingLineType m_lineType;

    IPlotData* m_data;
    double m_dXMax;
    double m_dXMin;
    double m_dYMax;
    double m_dYMin;

    bool m_bXLog;
    bool m_bYLog;

    double m_dLeft;
    double m_dTop;
    double m_dWidth;
    double m_dHeight;

	bool m_isAll;	//!< 実際のクリッピング範囲が全体かどうか
};
