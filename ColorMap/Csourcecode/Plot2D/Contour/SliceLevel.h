/*!	@file
	@date	2008-06-11
	@author	wada

	@brief	スライスレベル描画クラス
*/
#pragma once

#include "NullableShape.h"

class ICanvas;
class ISerializer;
class OCContourImage;
struct OCPoint;
struct PropertyNode;

//! 等高線やレベル値の描画を行います
class OCSliceLevel : public NullableShape
{
public:
	OCSliceLevel(double level, COLORREF color, OCContourImage* parent);
	~OCSliceLevel();

	bool IsVisible() const				{ return m_visible; }
	void SetVisible(bool visible)		{ m_visible = visible; }
	bool IsLevelVisible() const			{ return m_showLevel; }
	void SetLevelVisible(bool visible)  { m_showLevel = visible; }
	double GetLevel() const				{ return m_level; }
	void SetLevel(double level)			{ m_level = level; }

	int GetLineStyle() const			{ return m_lineStyle; }
	void SetLineStyle(int style)		{ m_lineStyle = style; }
	COLORREF GetLineColor() const		{ return m_lineColor; }
	void SetLineColor(COLORREF color)	{ m_lineColor = color; }
	float GetLineWidth() const			{ return m_lineWidth; }
	void SetLineWidth(float width)		{ m_lineWidth = width; }

	void SetFilling(bool filling)		{ m_filling = filling; }
	bool IsFilling() const				{ return m_filling; }
	void SetFillColor(COLORREF color)	{ m_fillColor = color; }
	int GetDigit() const				{ return m_digit; }
	void SetDigit(int digit)			{ m_digit = digit; }

	COLORREF GetLevelColor() const		{ return m_levelColor; }
	void SetLevelColor(COLORREF color)	{ m_levelColor = color; }

	bool UsePoint() const { return !m_lines.empty(); }
	void ClearPoints();
	void AddPoints(OCPoint* points, size_t count, double width, double height);
	void SetDefaultSize(double width, double height);
	void Draw(ICanvas* g, double x, double y, double width, double height, bool levelColor);
	void DrawLevelText(ICanvas* g, double x, double y, double width, double height, bool levelColor);
	void DrawSelected(ICanvas* g);
	void DrawSelectRect(ICanvas* g);
	bool IntersectLevelText(OCPoint const& p1, OCPoint const& p2);
	bool IntersectLevelText(OCRect const& r);
	bool HitTest(OCRect const& r, double x, double y) const;
	bool HitTestLine(OCRect const& r, double x, double y) const;
	bool Move(OCRect const& rect, double x, double y);

	OCSliceLevel* Clone(OCContourImage* parent) const;

	void Serialize(ISerializer* ar);

// IShape
	virtual void Destroy() { delete this; }
	virtual bool IsEffective() const  { return false; } // オブジェクトウィンドウには出さない

	virtual void DrawSelected();

	virtual int HitTest(double x, double y) const;
	virtual LPTSTR GetCursor(double x, double y) const;
	virtual void BeginDrag(int type, double x, double y);
	virtual void Drag(double x, double y);
	virtual long EndDrag(double x, double y);

private:
	size_t Contains(double x, double y) const;
	void AdjustPosition(int width, int height);
	bool IntersectAllSliceLevelText(OCPoint const& p1, OCPoint const& p2);
	bool IntersectAllSliceLevelText(OCRect const& r);

private:
	OCContourImage* m_parent;
	bool m_visible;
	bool m_showLevel;
	double m_level;
	COLORREF m_levelColor;

	// 設定されたものを参照するだけ
	bool m_filling;
	COLORREF m_fillColor;
	int m_digit;

	int m_lineStyle;
	COLORREF m_lineColor;
	float m_lineWidth;

	//! 等高線
	struct Line {
		std::vector<OCPoint> points;	//!< 等高線を構成するデータ点の配列
		size_t index;					//!< レベル値テキストを表示するデータ点の点番号
		bool bSetRect;					//!< レベル値テキストの矩形座標を設定したかどうか
		OCRect textRect;				//!< レベル値テキストの矩形座標
	};
	std::vector<Line> m_lines;
	std::vector<Line> m_cacheLines;

	size_t mutable m_hitIndex;

	int m_width;
	int m_height;
	double m_width_org;
	double m_height_org;

	OCPoint m_movePt;
	bool m_dragging;
};