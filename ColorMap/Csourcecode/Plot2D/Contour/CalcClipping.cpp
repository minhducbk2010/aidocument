//===========================================================================
/*! @file

	@date	2008-03-25 VC++に移植
    @date   2005-07-13 作成
    @author wada
*/
//===========================================================================
#include "stdafx.h"
#include <algorithm>
#include <memory>
#include <float.h>
#include <boost/assert.hpp>
#include "IPlotData.h"
#include "CalcClipping.h"
#include "Interpolate/Spline.hpp"
#include "Interpolate/LineInterpolate.hpp"

//---------------------------------------------------------------------------

//===========================================================================
//! Constructor
//===========================================================================
OCCalcClipping::OCCalcClipping() : m_dRotPer(50), m_type(CCT_UPPER), m_data(0), m_lineType(CCLT_CURVE)
{}

//===========================================================================
//! Destructor
//===========================================================================
OCCalcClipping::~OCCalcClipping()
{
}

void OCCalcClipping::SetType(ContourClippingType type, bool isAll)
{
    m_type = type;
	m_isAll = isAll;
    SetData(m_data);
}

//===========================================================================
//! 元となる実数データからクリッピングに使用する各区間の最大値を求める 
/*!
    @param[in]	data	元データ
*/
//===========================================================================
void OCCalcClipping::SetData(IPlotData* data)
{
    m_data = data;
    if (!data || data->GetDataCount() == 0) { return; } // データが無ければ中止
	BitSet store = m_bCheck;
    Clear();

	DataArray before;
	// 外部入力されたデータを使う場合(上限)
	if (m_type == CCT_UPPER && !m_upperData.empty()) {
		for (size_t i = 0, n = m_upperData.size(); i < n; ++i) {
			PushData(DataPair(m_upperData[i].x, m_upperData[i].y));
		}
		for (size_t i = 0, n = std::min<size_t>(store.size(), m_bCheck.size()); i < n; ++i) { m_bCheck[i] = store[i]; }
		return;
	}
	// 外部入力されたデータを使う場合(下限)
	else if (m_type == CCT_LOWER && !m_lowerData.empty()) {
		for (size_t i = 0, n = m_lowerData.size(); i < n; ++i) {
			PushData(DataPair(m_lowerData[i].x, m_lowerData[i].y));
		}
		for (size_t i = 0, n = std::min<size_t>(store.size(), m_bCheck.size()); i < n; ++i) { m_bCheck[i] = store[i]; }
		return;
	}
	else {
		for(std::size_t i = 0; i < data->GetDataCount(); ++i)
		{
			float const xv = data->GetData(0, (int)i);
			float const yv = data->GetData(1, (int)i);
			if(xv != FLT_MAX && yv != FLT_MAX) {
				before.push_back(DataPair(xv, yv));
			}
		}
	}
	// すべて無効値だった場合、空になることがある
	if (before.empty()) { return; }

    using boost::bind;
    // 挿入されたデータをXデータを基準に昇順にソートする
    std::sort(before.begin(), before.end(),
        bind(std::less<double>(), 
            bind(&DataPair::X, _1), bind(&DataPair::X, _2)
        )
    );

    // 各区間の最大値を求めメンバに格納する
    DataArray clip;
    std::size_t nRealSize = before.size();
    for(std::size_t i = 0; i < nRealSize - 1; /*...*/)
    {
        clip.push_back(DataPair(before[i]));

        std::size_t j = i + 1;
        for(/*...*/; j < nRealSize; ++j)
        {
            bool isBreak = false;
            if(before[j].X - before[i].X >= m_dRotPer)
            { // 許容値を超えた
                // 現在のYデータの最大値をメンバに追加
				PushData(m_type == CCT_UPPER
					? GetMaxData(clip.begin(), clip.end())
					: GetMinData(clip.begin(), clip.end()));

                i = j;
                clip.clear();
                isBreak = true;
            }
            clip.push_back(before[j]);
            if(isBreak) { break; }
        }
        if(j == nRealSize)
        {
            break;
        }
    }
    // ループを終了した時にも最大値を抜き出す
    if(!clip.empty()) {
        PushData(m_type == CCT_UPPER ? GetMaxData(clip.begin(), clip.end()) : GetMinData(clip.begin(), clip.end()));
    }

	for (size_t i = 0, n = std::min<size_t>(store.size(), m_bCheck.size()); i < n; ++i) {
		m_bCheck[i] = store[i];
	}
}

//===========================================================================
//! 軸の最大最小値と座標を元にクリッピングポイントを求める
/*!
    @param  pXAxis  グラフで使用するX軸
    @param  pYAxis  グラフで使用するY軸
    @todo GetValueを修正する(デシベルに対応してない)
*/
//===========================================================================
void OCCalcClipping::CalcClipping(double XMax, double XMin, double YMax, double YMin, bool bXLog, bool bYLog,
    OCRect rcAxis)
{
    if (m_fXData.size() <= 1)
    {
        m_ptClip.clear();
        return;
    }

    // 最大最小値を取得
    m_bXLog = bXLog;
    m_bYLog = bYLog;
    m_dXMax = GetValue(m_bXLog, XMax);
    m_dXMin = GetValue(m_bXLog, XMin);
    m_dYMax = GetValue(m_bYLog, YMax);
    m_dYMin = GetValue(m_bYLog, YMin);

    m_dLeft    = rcAxis.left;
    m_dTop     = rcAxis.top;
    m_dHeight  = rcAxis.Height();
    m_dWidth   = rcAxis.Width();

	if (m_type == CCT_UPPER) {
		CalcClippingUpper();
	}
	else {
		CalcClippingLower();
	}
}

//===========================================================================
//! クリッピングポイントを計算する
//===========================================================================
void OCCalcClipping::CalcClippingUpper()
{
    m_ptClip.clear();
    
    // クリッピングポイント(座標)の計算

    // 1. 最初の点は原点(左下)
	m_ptClip.push_back(OCPoint(m_dLeft, m_dTop + m_dHeight + 1));
    
	if (!CalcCenter()) {
		m_ptClip.clear();

		// 曲線補間に失敗しても、使用できるデータが2点あれば直線補間する
		std::vector<double> vx, vy;
		SetEffectiveData(vx, vy);
		if (vx.size() == 2) {
			// CalcLine()はDataArrayを返すが、m_ptClipに直接入れているので受け取る必要はない？
			CalcLine(m_dXMin, m_dXMax, m_bXLog, m_bYLog, vx, vy);
		}
		return;
	}

    // 4.最後の点はX軸の最大点(右下)
	if (m_isAll) {
		m_ptClip.push_back(OCPoint(m_dLeft + m_dWidth, m_dTop + m_dHeight));
	}
	else {
		m_ptClip.push_back(OCPoint(m_dLeft + m_dWidth + 1, m_dTop + m_dHeight + 1));
	}
}

void OCCalcClipping::CalcClippingLower()
{
    m_ptClip.clear();
    
    // クリッピングポイント(座標)の計算

    // 1. 最初の点は原点(左上)
	m_ptClip.push_back(OCPoint(m_dLeft, m_dTop));
    
	if (!CalcCenter()) {
		m_ptClip.clear();

		// 曲線補間に失敗しても、使用できるデータが2点あれば、直線補間する
		std::vector<double> vx, vy;
		SetEffectiveData(vx, vy);
		if (vx.size() == 2) {
			CalcLine(m_dXMin, m_dXMax, m_bXLog, m_bYLog, vx, vy);
		}
		return;
	}

    // 4.最後の点はX軸の最大点(右上)
	if (m_isAll) {
		m_ptClip.push_back(OCPoint(m_dLeft + m_dWidth, m_dTop));
	}
	else {
		m_ptClip.push_back(OCPoint(m_dLeft + m_dWidth + 1, m_dTop));
	}
}


bool OCCalcClipping::CalcCenter()
{
    // クリッピングポイントを求めるための曲線を作る
    DataArray curve; //< 曲線/直線の各値を格納する配列
	const bool calcSuccess = m_lineType == CCLT_CURVE ? CalcCurve(curve, m_dXMin, m_dXMax, m_bXLog, m_bYLog) : CalcLineInter(curve, m_dXMin, m_dXMax, m_bXLog, m_bYLog);

    if(!calcSuccess)
    {
        return false;
    }

	// 2. 次の点は最大負荷1点目と2点目を結んだ直線とX軸最小値上の交点
    double x1 = curve[0].X;
    double y1 = curve[0].Y;
    double x2 = curve[1].X;
    double y2 = curve[1].Y;
    double x0 = m_dXMin;
    double y0 = (y2 - y1) / (x2 - x1) * (x0 - x1) + y1;
    if(y0 > m_dYMax)
    {
		m_ptClip.push_back(OCPoint(m_dLeft, m_dTop));
        m_ptClip.push_back(OCPoint(m_dLeft, m_dTop)); // クリッピング位置の先頭
    }
    else if(y0 > m_dYMin)
    {
		m_ptClip.push_back(OCPoint(m_dLeft,
		    m_dTop + m_dHeight - (y0 - m_dYMin) / (m_dYMax - m_dYMin) * m_dHeight));
        // クリッピングの線の先頭
        m_ptClip.push_back(OCPoint(m_dLeft,
            m_dTop + m_dHeight - (y0 - m_dYMin) / (m_dYMax - m_dYMin) * m_dHeight));
    }
    else
    { // クリッピングラインがY軸最小値を下回る場合、Y軸との交点までとする
        y0 = m_dYMin;
// 仮処理--
        double div = y2 - y1;
        if(IsZero(div)) { div = 1.; }
// --
        x0 = (y0 - y1) * (x2 - x1) / div + x1;

        // クリッピングの線の先頭を2点目にする為に、同じ点を2点入れる
        OCPoint p = OCPoint(m_dLeft + (x0 - m_dXMin) / (m_dXMax - m_dXMin) * m_dWidth,
            m_dTop + m_dHeight - (y0 - m_dYMin) / (m_dYMax - m_dYMin) * m_dHeight);
        m_ptClip.push_back(p);
        m_ptClip.push_back(p);
    }

    // ここからは、中間点
    for(std::size_t i = 0; i < curve.size(); ++i)
    {
        double x = curve[i].X;
        double y = curve[i].Y;

        // Xの値が最小以下か最大以上だった場合,描画できないので飛ばす
        if(x < m_dXMin || x > m_dXMax) { continue; }
        // Yの値が最小以下であれば最小値に,最大以上であれば最大値に補正します.
        if(y < m_dYMin) { y = m_dYMin; }
        if(y > m_dYMax) { y = m_dYMax; }

        double xl1 = m_dWidth * (x - m_dXMin) / (m_dXMax - m_dXMin);
        double yl1 = m_dHeight - m_dHeight * (y - m_dYMin) / (m_dYMax - m_dYMin);
		m_ptClip.push_back(OCPoint(m_dLeft + xl1, m_dTop + yl1));
    }
    // ここまで中間点

    // 3. 最後のひとつ前の点は最大負荷最後の点と
    //    そのひとつ前の点を結んだ直線とX軸最大値上の交点
    std::size_t size = curve.size();
    x1 = curve[size - 2].X;
    y1 = curve[size - 2].Y;
    x2 = curve.back().X;
    y2 = curve.back().Y;
    x0 = m_dXMax;
    y0 = (y2 - y1) / (x2 - x1) * (x0 - x1) + y1;
    // クリッピングの線の終端
    if(y0 > m_dYMax)
    {
        m_ptClip.push_back(OCPoint(m_dLeft + m_dWidth , m_dTop));
		if (m_isAll) {
			m_ptClip.push_back(OCPoint(m_dLeft + m_dWidth, m_dTop));
		}
		else {	// 上限、下限の場合はクリッピング幅を少し広げる
			m_ptClip.push_back(OCPoint(m_dLeft + m_dWidth + 1, m_dTop));
		}
    }
    else if(y0 > m_dYMin)
    {
		m_ptClip.push_back(OCPoint(m_dLeft + m_dWidth,
			m_dTop + m_dHeight - (y0 - m_dYMin) / (m_dYMax - m_dYMin) * m_dHeight));		//<クリップラインの最後

		if (m_isAll) {
			m_ptClip.push_back(OCPoint(m_dLeft + m_dWidth,
				m_dTop + m_dHeight - (y0 - m_dYMin) / (m_dYMax - m_dYMin) * m_dHeight));	//<垂線に使う値
		}
		else {
			m_ptClip.push_back(OCPoint(m_dLeft + m_dWidth /*+ 1*/,
				m_dTop + m_dHeight - (y0 - m_dYMin) / (m_dYMax - m_dYMin) * m_dHeight));	//<クリッピングでしか使用しない
		}
    }
    else
    {	// クリッピングラインがY軸最小値を下回る場合、Y軸との交点までとする
        y0 = m_dYMin;
        if(IsZero(y2 - y1)) {
            x0 = m_dXMax;
        }
        else {
            x0 = (y0 - y1) * (x2 - x1) / (y2 - y1) + x1;
        }
        // クリッピングの線の終端を最後から2点目にする為に、同じ点を2点入れる
        OCPoint p = OCPoint(m_dLeft + (x0 - m_dXMin) / (m_dXMax - m_dXMin) * m_dWidth,
            m_dTop + m_dHeight - (y0 - m_dYMin) / (m_dYMax - m_dYMin) * m_dHeight);

        m_ptClip.push_back(p);
        m_ptClip.push_back(p);
    }

	return true;
}

//===========================================================================
//! メンバに保持しているデータ中で使用フラグが有効なもののみ引数の配列に抽出します.
/*!
    @param[out] vx, vy  抽出したデータを格納する配列への参照
*/
//===========================================================================
void OCCalcClipping::SetEffectiveData(std::vector<double>& vx, std::vector<double>& vy)
{   
    for(std::size_t i = 0; i < m_fXData.size(); ++i)
    {
        if(m_bCheck[i])
        {
            vx.push_back(m_fXData[i]);
            vy.push_back(m_fYData[i]);
        }
    }
}

//===========================================================================
//! クリッピングラインに使用する曲線を求める
/*!
    @param[out] curve   求めた曲線の各データを格納する配列(out)
*/
//===========================================================================
bool OCCalcClipping::CalcCurve(DataArray& curve,
    double dStart, double dStop, bool bXLog, bool bYLog)
{
    // 使用するデータのみ抽出して補間計算に使用します.
    std::vector<double> vx, vy;
    SetEffectiveData(vx, vy);
    if(vx.size() <= 1) { return false; } // 計算できない
    if(vx.size() == 2) {
        //curve = CalcLine(dStart, dStop, bXLog, bYLog, vx, vy);	// ここでは計算しない
        return false;//curve.size();
    }

	const std::auto_ptr<OCInterpolate> Inter(new OCSpline());
    Inter->Init((int)vx.size(), &vx[0], &vy[0]);
    Inter->Prepare();

    double dGap = (dStop - dStart) / 1E3;
    for(double d = dStart; d < dStop; d += dGap)
    {
        double x = bXLog ? std::pow(10, d) : d;
		double const calc = Inter->Calc(x);
		if (calc == DBL_MAX) { continue; }
		double y = GetValue(bYLog, calc);

        curve.push_back(DataPair((float)x, (float)y));
    }
	return !curve.empty();
}

//===========================================================================
//! クリッピングラインに使用する直線を求める
/*!
    @param[out] line   求めた直線の各データを格納する配列(out)
*/
//===========================================================================
bool OCCalcClipping::CalcLineInter(DataArray& line,
    double dStart, double dStop, bool bXLog, bool bYLog)
{
    // 使用するデータのみ抽出して補間計算に使用します.
    std::vector<double> vx, vy;
    SetEffectiveData(vx, vy);
    if(vx.size() <= 1) { return false; } // 計算できない
    if(vx.size() == 2) {
        //curve = CalcLine(dStart, dStop, bXLog, bYLog, vx, vy);	// ここでは計算しない
        return false;//curve.size();
    }

	const std::auto_ptr<OCLineInterpolate> Inter(new OCLineInterpolate());
    Inter->Init((int)vx.size(), &vx[0], &vy[0]);
    //Inter->Prepare();

    double dGap = (dStop - dStart) / 1E3;
    for(double d = dStart; d < dStop; d += dGap)
    {
        double x = bXLog ? std::pow(10, d) : d;
		double const calc = Inter->CalcForSortX(x);
		if (calc == DBL_MAX) { continue; }
        double y = GetValue(bYLog, calc);

        line.push_back(DataPair((float)x, (float)y));
    }
	return !line.empty();
}


double adjust(double, double, double val)
{
    /*if(max < val) {
        val = max;
    }
    if(min > val) {
        val = min;
    }*/
    return val;
}

//===========================================================================
//! 直線補間を実行します
//! クリップラインが 2 点しか無い場合、スプライン補間は実行できないので直線
//! 補間をしてクリップラインを作成します
//===========================================================================
OCCalcClipping::DataArray OCCalcClipping::CalcLine(double /*dStart*/, double /*dStop*/, bool /*bXLog*/, bool /*bYLog*/,
    const std::vector<double>& x, const std::vector<double>& y)
{
    if(x.front() == x.back()) { return DataArray(); }

    const double x1 = x.front(), x2 = x.back();
    const double y1 = y.front(), y2 = y.back();
    const double slope = (y2 - y1) / (x2 - x1);

	if (m_type == CCT_UPPER) {
		m_ptClip.push_back(OCPoint(m_dLeft, m_dTop + m_dHeight + 1));
	}
	else {
		m_ptClip.push_back(OCPoint(m_dLeft, m_dTop));
	}

    DataArray arr;
    {
        double dx = m_dXMin;
        double dy = slope * (dx - x1) + y1;
        if(dy < m_dYMin) {
            dx = (dy - y1) / (y2 - y1) * (x2 - x1) + x1;
        }
        arr.push_back(DataPair((float)dx, (float)dy));

        double top = m_dTop + m_dHeight - (dy - m_dYMin) / (m_dYMax - m_dYMin) * m_dHeight;
        top = adjust(m_dTop, m_dTop + m_dHeight, top);
        m_ptClip.push_back(OCPoint(m_dLeft, top));
        m_ptClip.push_back(OCPoint(m_dLeft, top));
    }
    {
        double dx = m_dXMax;
        double dy = slope * (dx - x1) + y1;
        if(dy < m_dYMin) {
            dx = (dy - y1) / (y2 - y1) * (x2 - x1) + x1;
        }
        arr.push_back(DataPair((float)dx, (float)dy));

        double bottom = m_dTop + m_dHeight - (dy - m_dYMin) / (m_dYMax - m_dYMin) * m_dHeight;
        bottom = adjust(m_dTop, m_dTop + m_dHeight, bottom);
        m_ptClip.push_back(OCPoint(m_dLeft + m_dWidth, bottom));
        m_ptClip.push_back(OCPoint(m_dLeft + m_dWidth, bottom));
    }

	if (m_type == CCT_UPPER) {
		if (m_isAll) {
			m_ptClip.push_back(OCPoint(m_dLeft + m_dWidth, m_dTop + m_dHeight));
		}
		else {
			m_ptClip.push_back(OCPoint(m_dLeft + m_dWidth + 1, m_dTop + m_dHeight + 1));
		}
	}
	else {
		if (m_isAll) {
			m_ptClip.push_back(OCPoint(m_dLeft + m_dWidth, m_dTop));
		}
		else {
			m_ptClip.push_back(OCPoint(m_dLeft + m_dWidth + 1, m_dTop));
		}
	}

    return arr;
}

//===========================================================================
//! データを初期化(クリア)
//===========================================================================
void OCCalcClipping::Clear()
{
    m_fXData.clear();
    m_fYData.clear();
    m_bCheck.clear();
}

//===========================================================================
//===========================================================================
const std::vector<OCPoint> OCCalcClipping::GetSectionData() const
{
    BOOST_ASSERT(m_fXData.size() == m_fYData.size());

    std::vector<OCPoint> Result(m_fYData.size());
    for(std::size_t i = 0; i < m_fXData.size(); ++i)
    {
        Result[i] = OCPoint(m_fXData[i], m_fYData[i]);
    }
    return Result;
}

//===========================================================================
//===========================================================================
void OCCalcClipping::SetSectionData(const std::vector<OCPoint>& Data)
{
    Clear();
    for(std::size_t i = 0; i < Data.size(); ++i)
    {
        m_fXData.push_back(Data[i].x);
        m_fYData.push_back(Data[i].y);
        m_bCheck.push_back(true);
    }
}

//===========================================================================
//===========================================================================
void OCCalcClipping::SetRotPer(double per)
{
    if(IsZero(per)) { per = 1; }
    m_dRotPer = per;
}

std::vector<OCPoint> OCCalcClipping::GetUpperDataCopy() const
{
	std::vector<OCPoint> pts;
	for (size_t i = 0; i < m_upperData.size(); ++i)
	{
		pts.push_back(m_upperData[i]);
	}
	return pts;
}

std::vector<OCPoint> OCCalcClipping::GetLowerDataCopy() const
{
	std::vector<OCPoint> pts;
	for (size_t i = 0; i < m_lowerData.size(); ++i)
	{
		pts.push_back(m_lowerData[i]);
	}
	return pts;
}