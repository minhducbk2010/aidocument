//===========================================================================
/*! @file
    @brief  コンターマップ描画クラス
*/
//===========================================================================
#pragma once
#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/signals2.hpp>
#include <atlimage.h>
#include "CalcContour.h"
#include "utility.h"
#include "Message.h"
#include "Interpolate/Spline.hpp"
#include "../IMapImage.h"
#include "../../../../Common/SliceSetting.h"

class IMessageReceiver;
class IPlotData;
class OCSliceLevel;
struct SliceLevel;

//*****************************************************************************

class OPolygonDefX
{
public:
    COLORREF color;
    int size;
    OCPoint*  p;

	OPolygonDefX() : p(0) {}
    ~OPolygonDefX()  { delete[] p; };
};

struct OCLineDefX
{
    COLORREF color;
    OCPoint p1;
    OCPoint p2;
    double z;

    //! 代入演算子
    OCLineDefX& operator=(const OCLineDefX& rhs)
    {
        this->color = rhs.color;
        this->p1 = rhs.p1;
        this->p2 = rhs.p2;
        this->z = rhs.z;
        return *this;
    }
};


//===========================================================================
/*!
    コンターマップを描画する為のクラス

    @note コンターマップを描画の手順
    - 1. CalRatioで座標変換率とｵﾌｾｯﾄを計算
    - 2. SetMeasDataで計測ﾃﾞｰﾀをｾｯﾄ
    - 3. SetGridで格子数を設定
    - 4. SetGridPointで格子点の座標を計算　
    - 5. CalContourで格子点のz値を求める

    @note ｵﾌﾟｼｮﾝ: // ※ O-Chartでは使用していません
    DrawGrid 格子点を描画する
    DrawMeas 実計測ﾃﾞｰﾀを描画する
*/
//===========================================================================
class OCContourImage : public IMapImage
{
public:
	OCContourImage(IMessageReceiver* owner);
    virtual ~OCContourImage();

    int m_nSplitNum;
    bool m_bContPaint;

	virtual bool Interpolation(IPlotData* data, double div, OCRect const& rect, bool displayProgressbar = true, bool reCalcSlice = true);
	void SetGradation(bool gradation);

	virtual void Click(OCRect const& rect, double x, double y);
	virtual bool Drag(ICanvas* g, OCRect const& rect, double x, double y);

	virtual void BitBlt(HDC hdc, int x, int y, int width, int height, DWORD rop) const;
	virtual void DrawImage(ICanvas* g, double x, double y, double width, double height, short alpha) const;

	virtual bool HitTest(OCRect const& rect, double x, double y) const;

	virtual void AddProperty(IPropertyGroup* prop, bool readOnly = false, bool plotMode = true);
	virtual void AddTabProperty(IPropertyGroup* prop, bool readOnly = false, bool plotMode = true);
	virtual bool SetProperty(long id, LPARAM value, LPARAM* source, bool& reCalc);
	virtual bool GetProperty(long id, LPARAM* ret) const;

	virtual ExtrapolationType GetExtrapolation() const { return m_extrapolation; }

	virtual size_t GetSliceLevelCount() const { return m_sliceLevels.size(); }

	virtual void CopyFrom(IMapImage* src);

	virtual void Serialize(ISerializer* ar);
	virtual void AfterSerialize();

	virtual bool IsEmpty() const;
	virtual void Destroy();

	COLORREF GetSelectLineColor() const;
	float GetSelectLineWidth() const;
	int GetSelectLineStyle() const;

	bool IntersectAllSliceLevelText(OCPoint const& p1, OCPoint const& p2);
	bool IntersectAllSliceLevelText(OCRect const& r);

	OCRect GetRect() const;

	// event invoker
	void CreatedSliceLevel(OCSliceLevel* slice) const;
	void DeletedSliceLevel(OCSliceLevel* slice) const;

private:
	void SetSliceLevel(double* levels, size_t count);
	void CreateImage(double zMin, double zMax, double zDiv, bool gradation);

    void Interpolation(int nDistr);

    void SetGriddingParam(double dPm1, double dPm2, double dAlpha);

	double ZLevel(int x, int y);

	void CalRatio(RECT rClient, double dXmin, double dXmax, double dYmin, double dYmax);
	bool SetMeasData(IPlotData *data);
    long SetGrid(long lNx, long lNy);
    void SetGridPoint(double dXmin, double dXmax, double dYmin, double dYmax, double dZmin, double dZmax);
    bool CalContour(bool displayProgressbar);

	bool CheckDataDifference(IPlotData* data);

	int CrossPointCount(double dContLevel, int nX, int nY);                     // 通過本数検出
	void DrawContPaint(double dContLevel, COLORREF PainCOLORREF, std::vector<OPolygonDefX*>* PolygonDefs);
    void DrawContLine(COLORREF PainCOLORREF, double dContLevel, std::vector<OCLineDefX*>* LineDefs);

private:
	CImage m_image;
	std::vector<double> m_sliceLevels;
	int m_width;
	int m_height;
	bool m_gradation;
	bool m_bChangeGradation; //グラデーションの選択状態が変化しているか

    long m_lNx;              // x方向格子数
    long m_lNy;              // y方向格子数

    double m_fXGridMax;        // x軸格子最大値
    double m_fXGridMin;        // x軸格子最小値
    double m_fYGridMax;        // y軸格子最大値
    double m_fYGridMin;        // y軸格子最小値
    double m_fZGridMax;        // z軸格子最大値
    double m_fZGridMin;        // z軸格子最小値

    double m_dPm1;
    double m_dPm2;
    double m_dAlpha;

    long m_lGridCount;       // 格子点の総数

    double* m_fXGridPoint;     // 格子点Xの座標
    double* m_fYGridPoint;     // 格子点Yの座標
    double* m_fZGridPoint;     // 格子点Zの座標

    double m_fXIntvl;          // x座標間隔
    double m_fYIntvl;          // y座標間隔

    double m_fWdRatio;         // x軸値座標変換率
    double m_fHgRatio;         // y軸値座標変換率
    double m_fXOffSet;          // x軸ｵﾌｾｯﾄ
    double m_fYOffSet;          // y軸ｵﾌｾｯﾄ

    float m_fX[4], m_fY[4];   // 交差位置(配列番号は 交差した番号)
    int m_nSide[4];         // 交差した辺の番号
                                       // (辺番号は 0:左上,1:右上,2:右下,3:左下。配列番号は 交差した番号)
    double m_fCornerZ[4];      // 格子点のZ値 (配列番号は 0:左上,1:右上,2:右下,3:左下)
    OCPoint m_PCorner[4];       // 格子点の座標(配列番号は 0:左上,1:右上,2:右下,3:左下)

    float* m_fXMeasData;      // 計測ﾃﾞｰﾀのx値
    float* m_fYMeasData;      // 計測ﾃﾞｰﾀのy値
    float* m_fZMeasData;      // 計測ﾃﾞｰﾀのz値
    long m_nMeasCount;       // 計測点数

	float m_fMeasXMax, m_fMeasXMin;
	float m_fMeasYMax, m_fMeasYMin;
	float m_fMeasZMax, m_fMeasZMin;
	double m_fZInterval;
	std::string m_currentPattern;
	bool m_modify;

private:
	double m_zMin, m_zMax, m_zDiv;

	int m_mesh;				//<! 格子化
	int m_alpha;			//!< 曲面
	ExtrapolationType m_extrapolation;	//!< 外挿

	std::vector<boost::shared_ptr<OCSliceLevel>> m_slices;
	LOGFONT m_levelFont;
	LOGFONT m_levelFontProp;
	COLORREF m_levelFontColor;
	COLORREF m_lineColor;
	bool m_showValue;			//!< レベル値表示設定
	int m_levelDigit;			//!< レベル値の小数点桁数
	bool m_levelFilling;		//!< レベル値の塗りつぶし
	COLORREF m_levelPaintColor;	//!< レベル値の背景色
	size_t m_legendIndex;		//!< 凡例に使用する等高線ライン
	bool m_limitedRange;		//!< データ範囲で制限

	bool m_calculation;
	bool m_imageCreation;

	void UpdateSliceLevel(SliceLevel* levels, int size, size_t legendIndex, bool limitedRange, bool autoSetting);
	void UpdateMesh();
	void UpdateAlpha();

	void GetSliceLevelSetting(ShapeSliceList& setting) const;

	bool m_levelModified;	//!< スライスレベルの設定が変更されたか?

	// 再計算用(cache)
	IPlotData* m_data;
	double m_scaleInterval;
	OCRect m_rect;

	//
	bool m_serializing;
	bool m_levelModifiedNotCopy; // CopyFromメソッドでm_levelModifiedをコピーしないようにする
};
