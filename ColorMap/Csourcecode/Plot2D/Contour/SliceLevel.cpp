/*!	@file
	@date	2008-06-11
	@author	wada

	@brief	スライスレベル描画クラス
*/
#include "stdafx.h"
#include <boost/format.hpp>
#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include "utility.h"
#include "ISerializer.h"
#include "ICanvas.h"
#include "IMessageReceiver.h"
#include "ResizeHandler.hpp"
#include "SliceLevel.h"
#include "ContourImage.h"

//! Constructor
OCSliceLevel::OCSliceLevel(double level, COLORREF color, OCContourImage* parent)
	: m_visible(true), m_showLevel(false)
	, m_level(level), m_levelColor(color), m_parent(parent)
	, m_lineStyle(PS_SOLID), m_lineColor(0), m_lineWidth(1)
	, m_filling(false), m_fillColor(RGB(255, 255, 255)), m_digit(1)
	, m_hitIndex(size_t(-1))
	, m_dragging(false)
{
	m_parent->CreatedSliceLevel(this);
}

//! Destructor
OCSliceLevel::~OCSliceLevel()
{
	m_parent->DeletedSliceLevel(this);
}

//! 等高線ラインの座標をすべて削除します
void OCSliceLevel::ClearPoints()
{
	m_cacheLines = m_lines;
	m_lines.clear();
}

//! 等高線ラインの座標を設定します
void OCSliceLevel::AddPoints(OCPoint* points, size_t count, double width, double height)
{
	if (width < 0) { width = -width; }
	if (height < 0) { height = -height; }

	// ピクセル値からミリメートルへ単位変換
	m_width_org = PxToMM(width);
	m_height_org = PxToMM(height);

	std::vector<OCPoint> pts(points, points + count);

	typedef OCPoint ( * PxToMMT)(OCPoint const&);
	std::transform(pts.begin(), pts.end(), pts.begin(), static_cast<PxToMMT>(PxToMM));

	// 3番目のメンバ(bSetRect)は、まだtextRectを設定していないので、falseに設定
	Line line = {pts, pts.size() / 2, false};
	m_lines.push_back(line);

	// テキストの位置は前回のものを復元(あれば)
	size_t const idx = m_lines.size() - 1;
	if (m_cacheLines.size() > idx) {
		if (m_cacheLines[idx].index < pts.size()) {
			m_lines[idx].index = m_cacheLines[idx].index;
		}
	}

	m_width = m_height = -1;
}

void OCSliceLevel::SetDefaultSize(double width, double height)
{
	if (width < 0) { width = -width; }
	if (height < 0) { height = -height; }

	// ピクセル値からミリメートルへ単位変換
	m_width_org = PxToMM(width);
	m_height_org = PxToMM(height);

	m_width = m_height = -1;
}

//! 等高線のレベル値を描画する
void OCSliceLevel::DrawLevelText(ICanvas* g, double x, double y, double width, double height, bool levelColor)
{
	// 等高線がなかったり、表示設定がOFFの場合は何もしない
	if (m_lines.empty()) { return; }
	if (!IsVisible() || !m_showLevel) { return; }

	AdjustPosition(width, height); // 座標調整

	// 小数点桁数をもとに書式指定子を作成する
	wchar_t buf[36], fmt[12];
	::swprintf_s(fmt, _countof(fmt), _T("%%.%df "), m_digit); 

	g->SetPen(levelColor ? m_levelColor : m_lineColor, m_lineWidth, m_lineStyle, 1);
	g->SetBackground(levelColor ? m_levelColor : m_fillColor, m_filling ? FS_SOLID : FS_NONE, 0);

	BOOST_FOREACH(Line& line, m_lines) {
		std::vector<OCPoint> pts = line.points;
		std::transform(pts.begin(), pts.end(), pts.begin(), boost::bind(std::plus<OCPoint>(), _1, OCPoint(x, y)));

		// レベル値表示位置データ点の座標がレベル値表示矩形の中心になるように配置する
		OCPoint mean = pts[line.index];	// テキストの左上座標

		// 表示するテキストを作成
		::swprintf_s(buf, _countof(buf), fmt, m_level);
		std::wstring text(buf);

		OCPoint size = g->MeasureString(text.c_str(), (long)text.length());
		// Textの矩形の中心をmean(データ点座標)にする
		line.textRect = OCRect(mean.x - size.x / 2, mean.y - size.y / 2, mean.x + size.x / 2, mean.y + size.y / 2);

		// textRectを初めて設定したときには、他のレベルのレベル値テキストとぶつからないかチェックする
		if (!line.bSetRect) {
			// 検索開始点のセット
			size_t index = line.index;
			// テキスト領域がかぶっていた場合
			while (IntersectAllSliceLevelText(line.textRect)) {
				// 次の点から探す
				++index;
				// 点集合の最後の要素まで到達したら、最初の要素に戻る
				if (index >= pts.size()) { index = 0; }
				// 次のデータ点の座標
				mean = pts[index];
				// Textの矩形の中心をmean(データ点座標)にする
				line.textRect = OCRect(mean.x - size.x / 2, mean.y - size.y / 2, mean.x + size.x / 2, mean.y + size.y / 2);
				// 検索開始点に戻ってきたら、あきらめる
				if (index == line.index) { break; }
			}
			// ラベル値テキスト座標の対象点番号をセット
			line.index = index;
		}
		line.bSetRect = true;	// textRectを設定した
		OCPoint const textLeftTop(line.textRect.left, line.textRect.top);

		if (m_filling) {
			g->FillRectangle(line.textRect);
		}
		g->DrawString(text.c_str(), (long)text.length(), textLeftTop);
	}
}
//! 等高線を描画します
void OCSliceLevel::Draw(ICanvas* g, double x, double y, double width, double height, bool levelColor)
{
	if (m_lines.empty()) { return; }
	if (!IsVisible()) { return; }

	AdjustPosition(width, height);

	g->SetPen(levelColor ? m_levelColor : m_lineColor, m_lineWidth, m_lineStyle, 1);
	g->SetBackground(levelColor ? m_levelColor : m_fillColor, m_filling ? FS_SOLID : FS_NONE, 0);

	BOOST_FOREACH(Line& line, m_lines) {
		std::vector<OCPoint> pts = line.points;;
		std::transform(pts.begin(), pts.end(), pts.begin(), boost::bind(std::plus<OCPoint>(), _1, OCPoint(x, y)));

		// レベル値表示の有無に関係なくレベル値テキストのかぶり判定は必要
		// (他のレベルがレベル値を表示しているときにラインがかぶらないようにするため)
		// 描画すべき点の集合
		std::vector<OCPoint> ptsDraw;
		// i-1とiで線分を表現するため、iは1始まりn-1終わり
		for (size_t i = 1; i < pts.size(); ++i) {
			// 線分がレベル値とかぶる場合
			//if (line.textRect.IntersectLine(pts[i-1], pts[i])) {
			if (IntersectAllSliceLevelText(pts[i-1], pts[i])) {
				// 今までに描画するべき線分があった場合
				if( !ptsDraw.empty() ){
					// 描画するべき線分を繋げた折れ線を描く
					g->DrawLines(&ptsDraw[0], ptsDraw.size());
					// 線分を描画したので、描画すべき点は無くなった
					ptsDraw.clear();
				}
				// かぶっている線分は描画しない
			}
			// 線分がレベル値とかぶらない場合
			else {
				if( ptsDraw.empty() ){
					// 描画するべき点に線分の両端点を加える
					ptsDraw.push_back( pts[i-1] );
					ptsDraw.push_back( pts[i] );
				}
				else {
					// 描画するべき点に線分の両端点を加える
					// 一方の点はすでに入っているので、入っていない方のみ追加する
					ptsDraw.push_back( pts[i] );
				}
			}
		}
		// 描画すべき線分が残っていたら、描画する
		if( !ptsDraw.empty() ){
			// 描画するべき線分を繋げた折れ線を描く
			g->DrawLines(&ptsDraw[0], ptsDraw.size());
			// 線分を描画したので、描画すべき点は無くなった
			ptsDraw.clear();
		}
	}
}

//! p1p2を端点とする線分がレベル値テキストとかぶるかを判定
bool OCSliceLevel::IntersectLevelText(OCPoint const& p1, OCPoint const& p2)
{
	// 等高線表示かつレベル値表示の場合のみかぶり判定を行う
	// (かぶり判定を行わないときは、かぶりなし(false)を返す)
	if(m_visible && m_showLevel) {
		BOOST_FOREACH(Line const& line, m_lines) {
			// 等高線のレベル値テキスト座標がセットされている場合のみかぶり判定を行う
			if( line.bSetRect && line.textRect.IntersectLine(p1,p2) ){
				return true;
			}
		}
	}
	return false;
}

//! 領域rがレベル値テキストとかぶるかを判定
bool OCSliceLevel::IntersectLevelText(OCRect const& r)
{
	// 等高線表示かつレベル値表示の場合のみかぶり判定を行う
	// (かぶり判定を行わないときは、かぶりなし(false)を返す)
	if(m_visible && m_showLevel) {
		BOOST_FOREACH(Line const& line, m_lines) {
			// 等高線のレベル値テキスト座標がセットされている場合のみかぶり判定を行う
			if( line.bSetRect && line.textRect.IntersectRects(r) ){
				return true;
			}
		}
	}
	return false;
}

//! p1p2を端点とする線分がすべてのレベル値テキストとかぶるかを判定
bool OCSliceLevel::IntersectAllSliceLevelText(OCPoint const& p1, OCPoint const& p2)
{
	if( m_parent == NULL ){
		return false;
	}
	return m_parent->IntersectAllSliceLevelText(p1,p2);
}

//! 領域rがすべてのレベル値テキストとかぶるかを判定
bool OCSliceLevel::IntersectAllSliceLevelText(OCRect const& r)
{
	if( m_parent == NULL ){
		return false;
	}
	return m_parent->IntersectAllSliceLevelText(r);
}

//! 選択中の枠を描く
void OCSliceLevel::DrawSelected(ICanvas* g)
{
	if (m_hitIndex >= m_lines.size()) { return; }

	OCRect const& r = m_lines[m_hitIndex].textRect;
	g->SetPen(RGB(0, 0, 255), 1, PS_SOLID, 1);
	g->DrawRubberBand(r);
	// m_movePtには、ずらす前の値を入れておく必要がある
	// OCSliceLevel::Draw中で、textRectは、ターゲットの座標から - OCPoint(2, 2)分ずらされている。
	//m_movePt.SetPoint(r.left + 2, r.top + 2);
	// m_movePtには、中心になっているデータ点の座標が入るため、レベル値表示矩形の中心座標にする
	m_movePt.SetPoint((r.left + r.right) / 2, (r.top + r.bottom) / 2);
}

//! 選択中の枠を描く(ドラッグ中に使用する)
/*!
	m_rectTextの座標は使用しない(ドラッグ中は実際の座標と異なる値になっているため)
	@param[in]	g		キャンバス

	@bug 位置がズレる
*/
void OCSliceLevel::DrawSelectRect(ICanvas* g)
{
	if (m_hitIndex >= m_lines.size()) { return; }

	g->SetPen(RGB(0, 0, 255), 1, PS_SOLID, 1);
	OCRect const& r = m_lines[m_hitIndex].textRect;
	// OCSliceLevel::Draw中で、textRectは、ターゲットの座標から - OCPoint(2, 2)分ずらされている。
	// (m_movePtは、マウスポインタに最も近いデータ点の座標を表している。)
	//OCPoint lefttop = m_movePt - OCPoint(2, 2);
	//g->DrawRubberBand(OCRect(lefttop.x, lefttop.y, lefttop.x + r.Width(), lefttop.y + r.Height()));
	// マウスポインタに最も近いデータ点を中心にした矩形座標にラバーバンドを表示する
	g->DrawRubberBand(OCRect(m_movePt.x - r.Width() / 2, m_movePt.y - r.Height() / 2, m_movePt.x + r.Width() / 2, m_movePt.y + r.Height() / 2));
}

bool OCSliceLevel::HitTest(OCRect const& /*r*/, double x, double y) const
{
	if (!IsVisible()) { return false; }

	m_hitIndex = Contains(x, y);
	return m_hitIndex != static_cast<size_t>(-1);
}

size_t OCSliceLevel::Contains(double x, double y) const
{
	for (size_t i = 0; i < m_lines.size(); ++i) {
		Line const& line = m_lines[i];
		if (line.textRect.Contains(x, y)) {
			return i;
		}
	}
	return static_cast<size_t>(-1);
}

bool PtInLine(OCPoint begin, OCPoint end, double X, double Y, double range);
//! 等高線が(x, y)を通るか判定します
bool OCSliceLevel::HitTestLine(OCRect const& r, double x, double y) const
{
	BOOST_FOREACH(Line const& line, m_lines) {
		std::vector<OCPoint> pts = line.points;
		std::transform(pts.begin(), pts.end(), pts.begin(), boost::bind(std::plus<OCPoint>(), _1, OCPoint(r.left, r.top)));
		for (int i = 0, n = static_cast<int>(pts.size() - 1); i < n; ++i) {
			OCPoint const& cur = pts[i], next = pts[i + 1];
			if (PtInLine(cur, next, x, y, 2)) { return true; }
		}
	}
	return false;
}

//! 保持しているポイントの中で一番近い点に移動します
/*!
	@retval	true	移動した
	@retval	false	移動していない
*/
bool OCSliceLevel::Move(OCRect const& rect, double x, double y)
{
	if (m_hitIndex >= m_lines.size()) { return false; }

	//!@todo 効率が悪い
	std::vector<OCPoint> pts = m_lines[m_hitIndex].points;
	std::transform(pts.begin(), pts.end(), pts.begin(), boost::bind(std::plus<OCPoint>(), _1, OCPoint(rect.left, rect.top)));

	size_t index = 0;
	double currentOffset = DBL_MAX;
	for (size_t i = 0; i < pts.size(); ++i) {
		OCPoint const& pt = pts[i];
		double diff = pt.Distance(OCPoint(x, y));
		if (currentOffset > diff) {
			currentOffset = diff;
			index = i;
			m_movePt = pt;
		}
	}
	bool move = m_lines[m_hitIndex].index != index;
	m_lines[m_hitIndex].index = index;
	return move;
}

OCSliceLevel* OCSliceLevel::Clone(OCContourImage* parent) const
{
	OCSliceLevel* clone = new OCSliceLevel(m_level, m_lineColor, parent);
	clone->m_visible = m_visible;
	clone->m_level = m_level;
	clone->m_levelColor = m_levelColor;
	clone->m_showLevel = m_showLevel;
	clone->m_lineStyle = m_lineStyle;
	clone->m_lineColor = m_lineColor;
	clone->m_lineWidth = m_lineWidth;
	clone->m_filling = m_filling;
	clone->m_fillColor = m_fillColor;
	clone->m_digit = m_digit;
	clone->m_lines = m_lines;
	return clone;
}

//! オブジェクトの情報をシリアル化します
void OCSliceLevel::Serialize(ISerializer* ar)
{
	ar->Serialize(_T("Visible"), m_visible);
	ar->Serialize(_T("Level"), m_level);
	ar->Serialize(_T("Color"), m_levelColor);
	ar->Serialize(_T("LevelVisible"), m_showLevel);

	ar->Serialize(_T("LineStyle"), m_lineStyle);
	ar->Serialize(_T("LineColor"), m_lineColor);
	ar->Serialize(_T("LineWidth"), m_lineWidth);

	ar->Serialize(_T("Width"), m_width);
	ar->Serialize(_T("Height"), m_height);

	if (dynamic_cast<IReadable*>(ar)) {
		if (IReadable* child = dynamic_cast<IReadable*>(ar->ChildNode(_T("Lines")))) {
			for (size_t i = 0; i < child->ChildCount(); ++i) {
				ISerializer* lineNode = child->ChildNode(i);

				Line l;
				lineNode->Serialize(_T("Index"), l.index);
				lineNode->Serialize(_T("SetRect"), l.bSetRect);
				lineNode->Serialize(_T("TextRect"), l.textRect);

				if (IReadable* pts = dynamic_cast<IReadable*>(lineNode->ChildNode(_T("Points")))) {
					for (size_t j = 0; j < pts->ChildCount(); ++j) {
						OCPoint p;
						pts->ChildNode(j)->Serialize(_T("X"), p.x);
						pts->ChildNode(j)->Serialize(_T("Y"), p.y);
						l.points.push_back(p);
					}
					if (!l.points.empty()) {
						if (l.points.size() < l.index) {
							l.index = l.points.size() - 1;
						}
						m_lines.push_back(l);
					}
				}
			}
		}
	}
	else {
		ISerializer* writer = ar->ChildNode(_T("Lines"));
		BOOST_FOREACH(Line& l, m_lines) {
			ISerializer* line = writer->ChildNode(_T("Line"));
			line->Serialize(_T("Index"), l.index);
			line->Serialize(_T("SetRect"), l.bSetRect);
			line->Serialize(_T("TextRect"), l.textRect);

			ISerializer* pts = line->ChildNode(_T("Points"));
			for (size_t i = 0; i < l.points.size(); ++i) {
				pts->Serialize(_T("Point"), l.points[i]);
			}
		}
	}
}

void OCSliceLevel::AdjustPosition(int width, int height)
{
	// 初期値の場合も、出力画像の大きさとオリジナル画像の大きさとで調整する
	if (m_width == -1 || m_height == -1) {
		double const xRate = static_cast<double>(width) / m_width_org;
		double const yRate = static_cast<double>(height) / m_height_org;
		for (size_t i = 0, n = m_lines.size(); i < n; ++i) {
			std::vector<OCPoint>& pts = m_lines[i].points;
			std::transform(pts.begin(), pts.end(), pts.begin(), boost::bind(std::multiplies<OCPoint>(), _1, OCPoint(xRate, yRate)));
		}
	}
	// サイズが変わっていたら各点の座標を調整する
	else if (m_width != width || m_height != height) {
		double const xRate = static_cast<double>(width) / m_width;
		double const yRate = static_cast<double>(height) / m_height;
		for (size_t i = 0, n = m_lines.size(); i < n; ++i) {
			std::vector<OCPoint>& pts = m_lines[i].points;
			std::transform(pts.begin(), pts.end(), pts.begin(), boost::bind(std::multiplies<OCPoint>(), _1, OCPoint(xRate, yRate)));
		}
	}
	m_width = width;
	m_height = height;
}

void OCSliceLevel::DrawSelected()
{
	DrawSelected(m_canvas);
}

//! 当たり判定
int OCSliceLevel::HitTest(double x, double y) const
{
	return HitTest(OCRect(), x, y) ? HIT_MOVE : HIT_NONE;
}

LPWSTR OCSliceLevel::GetCursor(double x, double y) const
{
	if (IsLock()) { return IDC_ARROW; }

	if (Contains(x, y) != static_cast<size_t>(-1)) {
		return IDC_SIZEALL;
	}
	return IDC_ARROW;
}

void OCSliceLevel::BeginDrag(int /*type*/, double /*x*/, double /*y*/)
{
	m_dragging = true;
}
void OCSliceLevel::Drag(double x, double y)
{
	if (!m_dragging) { return; }

	DrawSelectRect(m_canvas);
	Move(m_parent->GetRect(), x, y);
	DrawSelectRect(m_canvas);
}
long OCSliceLevel::EndDrag(double /*x*/, double /*y*/)
{
	m_dragging = false;
	return 0;
}

