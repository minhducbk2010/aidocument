/*!	@file
	@date	2008-04-21
	@author	wada

	@brief	三角メッシュの補間クラス
*/
#pragma once

#if 0

#include <atlimage.h>
#include "IMapImage.h"

class IPlotData;

class OCTriangleImage : public IMapImage
{
public:
	OCTriangleImage(IMessageReceiver* owner);
	virtual ~OCTriangleImage();

	virtual void Interpolation(IPlotData* data, OCRect const& rect);
	virtual void BitBlt(HDC hdc, int x, int y, int width, int height, DWORD rop) const;
	virtual void DrawImage(ICanvas* g, double x, double y, double width, double height, short alpha) const;

	virtual void AddProperty(IPropertyGroup* prop, bool readOnly = false) {}
	virtual bool SetProperty(long id, LPARAM value, LPARAM* source, bool&) {return false;}

private:
	ATL::CImage m_image;
};

#endif