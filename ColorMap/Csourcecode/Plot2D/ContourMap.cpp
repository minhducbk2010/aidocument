/*!	@file
	@date	2008-04-21
	@author	wada

	@brief	コンターマップの実装
*/
#include "StdAfx.h"
#include <boost/function.hpp>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/tuple/tuple.hpp>
#include <wtl/atlapp.h>
#include <wtl/atlgdi.h>
#include "utility.h"
#include "ICanvas.h"
#include "IPlotData.h"
#include "IFilter.h"
#include "PluginUtility/pluginutil.h"
#include "Property.h"
#include "PropertyCreator.hpp"
#include "PropertyHelper.hpp"
#include "ISerializer.h"
#include "IMessageReceiver.h"
#include "Message.h"
#include "PlotLib/ColorPattern.h"
#include "ContourMap.h"
#include "Contour/ContourImage.h"
#include "TriangleImage.h"
#include "Scatter.h"
#include "IFile.h"
#include "Contour/SliceLevel.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
class OCClipExtractor : public IClipExtractor
{
public:
	OCClipExtractor(OCCalcClipping* impl) : m_impl(impl) {}

	virtual OCPoint* Extract(double threshold, unsigned long* size)
	{
		double const old = m_impl->RotPer();
		std::vector<OCPoint> oldUpperData = m_impl->GetUpperDataCopy();
		std::vector<OCPoint> oldLowerData = m_impl->GetLowerDataCopy();
		m_impl->ClearInputData();

		m_impl->SetRotPer(threshold);
		m_impl->SetData(m_impl->GetData());

		std::vector<OCPoint> points(m_impl->GetSectionData());
		*size = static_cast<unsigned long>(points.size());

		// restore
		m_impl->SetRotPer(old);
		m_impl->SetUpperData(oldUpperData);
		m_impl->SetLowerData(oldLowerData);
		m_impl->SetData(m_impl->GetData());

		OCPoint* ptr = new OCPoint[points.size()];
		std::copy(points.begin(), points.end(), ptr);
		return ptr;
	}

	virtual void Free(OCPoint* ptr)
	{
		delete[] ptr;
	}

private:
	OCCalcClipping* m_impl;
};
///////////////////////////////////////////////////////////////////////////////////////////////////

//! Constructor
OCContourMap::OCContourMap(HWND wnd, IMessageReceiver* plot)
	: OCPlotImpl3D(wnd, plot)
	, m_image(new OCContourImage(m_plot))
	, m_marker(new OCScatter(wnd, plot))
    , m_pattern(0)
	, m_clipLine(new OCCalcClipping())
	, m_clipLineType(CCLT_CURVE)
	, m_clipType(CCT_NONE)
	, m_clipLineStyle(PS_SOLID), m_clipLineWidth(2), m_clipLineColor(RGB(255, 0, 0)), m_clipLineRoundType(RT_ELLIPSE)
	, m_mapType(0)
	, m_fillType(ContourFilling::ON), m_sliceType(Contour::UserColor)
	, m_transparent(0)
{
	m_image->SetOwner(m_plot);
	m_image->AddSliceLevel.connect(boost::bind(&OCContourMap::AddSliceLevel, this, _1));
	m_image->RemoveSliceLevel.connect(boost::bind(&OCContourMap::RemoveSliceLevel, this, _1));

	m_clipSetting.extractor = new OCClipExtractor(m_clipLine.get());
	m_clipSetting.points.threshold = *reinterpret_cast<double*>(plot->SendMessage(PI_MSG_GET_CONF_CONTOUR_ACCEPTVALUE, 0, 0));
	m_clipLine->SetRotPer(m_clipSetting.points.threshold);
	m_clipLineStyle = plot->SendMessage(PI_MSG_GET_CONF_CONTOUR_CLIPLINESTYLE, 0, 0);
	m_clipLineColor = plot->SendMessage(PI_MSG_GET_CONF_CONTOUR_CLIPLINECOLOR, 0, 0);
	m_clipLineWidth = *reinterpret_cast<double*>(plot->SendMessage(PI_MSG_GET_CONF_CONTOUR_CLIPLINEWIDTH, 0, 0));

	m_marker->EnableZData();
}

//! Destructor
OCContourMap::~OCContourMap()
{
	delete m_clipSetting.extractor;
}

//! 表示ON/OFF切り替え
void OCContourMap::Visible(BOOL visible)
{
	m_visible = !!visible;
	if (m_plot) {
		m_plot->SendMessage(PI_SET_MARKER_COLOR, m_visible ? 0 : 1, 0);
	}
}

void OCContourMap::SetData(IPlotData* data)
{
	__super::SetData(data);
	m_marker->SetData(data);

	// クリップラインの設定を初期化
	m_clipLine->SetData(data);

	m_clipSetting.type = ClippingSetting::Point;

	// Point
	std::vector<OCPoint> pts = m_clipLine->GetSectionData();
	for (size_t i = 0, n = pts.size(); i < n; ++i) {
		m_clipSetting.points.extractPoints.push_back(
			std::make_pair(m_clipLine->IsPointEffective(i), pts[i]));
	}

	// File
	m_clipSetting.file.fileId = -1;
	m_clipSetting.file.xItemNo = -1;
	m_clipSetting.file.yItemNo = -1;
}

//! 当たり判定
/*!
	選択中にもう一度呼ばれたらスライスレベルに対しての操作か調べる
*/
bool OCContourMap::HitTest(OCRect const& rect, DrawArgs const& arg, double x, double y)
{
	//if (m_selected) {
	//	if (::GetAsyncKeyState(VK_LBUTTON) < 0 && !m_dragging) {
	//		m_image->Click(rect, x, y);
	//	}
	//}

	if (m_data->GetDataCount() == 0) { return false; }

	return m_image->HitTest(rect, x, y) || m_marker->HitTest(rect, arg, x, y);
}

//! コンターマップを描画します
void OCContourMap::Draw(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const
{
	DrawImpl(false, g, rect, arg);
}

void OCContourMap::DrawSelected(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const
{
	if (m_selected) {
		DrawImpl(true, g, rect, arg);
	}
}

// <min, max>
std::pair<double, double> GetMinMax(size_t dim, IPlotData* data)
{
	size_t minIdx, maxIdx;
	data->FindMinMaxIndex(dim, &minIdx, &maxIdx);
	return std::make_pair(data->GetData((int)dim, minIdx), data->GetData((int)dim, maxIdx));
}

void OCContourMap::DrawImpl(bool const selected, ICanvas* g, OCRect const& rect, DrawArgs const& arg) const
{
	if (!IsVisible()) { return; }

	// クリッピング
	std::vector<std::vector<OCPoint>> clipLines;
	if (m_mapType == 0) {
		Clipping(g, rect, &clipLines);
	}

	// 外挿補填しない場合はクリッピングして外挿部分を隠す
	if (m_clippingType != plot::None) {
		double xMax, xMin, yMax, yMin;
		boost::tie(xMin, xMax) = ::GetMinMax(0, m_data);
		boost::tie(yMin, yMax) = ::GetMinMax(1, m_data);

		OCRect clipRect = rect;
		if (m_clippingType == plot::AllClip || m_clippingType == plot::YClip) {
			clipRect.left	= arg.XNormalize(xMin) * rect.Width() + rect.left;
			clipRect.right	= arg.XNormalize(xMax) * rect.Width() + rect.left;
		}
		if (m_clippingType == plot::AllClip || m_clippingType == plot::XClip) {
			clipRect.top	= rect.Height() - rect.Height() * arg.YNormalize(yMax) + rect.top;
			clipRect.bottom = rect.Height() - rect.Height() * arg.YNormalize(yMin) + rect.top;
		}
		g->AddClipping(clipRect);
	}

	if (m_image->IsEmpty()) {
		if (!selected) {
			g->SetBackground(RGB(255, 255, 255), FS_SOLID, 0);
			g->FillRectangle(rect);
		}
	}
	else {
		// 今回の描画が選択状態(フォーカス)かをイメージ生成側へ伝える
		m_image->SetSelected(selected);
		m_image->DrawImage(g, rect.left, rect.top, rect.Width(), rect.Height(), m_transparent);
		m_image->SetSelected(false);
	}
	g->ResetClipping();

	if (!selected) {
		// クリップラインを描画(画像の後ろに隠れないよう最後に描画する)
		g->SetPen(m_clipLineColor, m_clipLineWidth, m_clipLineStyle, m_clipLineRoundType);
		BOOST_FOREACH(std::vector<OCPoint> const& pts, clipLines) {
			if(!pts.empty()) {
				g->DrawLines(&pts[0], pts.size());
			}
		}
	}

	// 最後にマーカーを描く
	if (selected) {
		m_marker->DrawSelected(g, rect, arg);
	}
	else {
		m_marker->Draw(g, rect, arg);
	}

	// グリッドが見えるように、自分の上にグリッドを書く
	if (!selected) { m_plot->SendMessage(PI_MSG_DRAW_GRID, 0, 0); }
}

//! 凡例内プロットを描画します
void OCContourMap::DrawLegend(ICanvas* g, OCRect const& rect, int /*ratio*/) const
{
	if (!IsVisible()) { return; }

	if (OCContourImage* c = dynamic_cast<OCContourImage*>(m_image.get())) {
		g->SetPen(c->GetSelectLineColor(), c->GetSelectLineWidth(), c->GetSelectLineStyle(), 1);
	}
	else {
		g->SetPen(m_lineColor, m_lineWidth, m_lineStyle, 1);
	}
	g->DrawLine(
		OCPoint(rect.left + 1,	rect.top + rect.Height() / 2), 
		OCPoint(rect.right - 1, rect.top + rect.Height() / 2));
}

void SetClipPoint(std::vector<OCPoint> const& src,
				  size_t firstIndex, size_t lastIndex,
				  std::vector<std::vector<OCPoint>>* pts,
				  std::vector<OCPoint>* ptsLeft, std::vector<OCPoint>* ptsRight)
{
	if (firstIndex < lastIndex) {
		pts->push_back(std::vector<OCPoint>(&src[firstIndex], &src[lastIndex]));
	}

	if (ptsLeft) {
		ptsLeft->push_back(src[firstIndex]);
	}
	if (ptsRight) {
		ptsRight->push_back(src[src.size() - (firstIndex + 1)]);
	}
}

//! クリップラインを作成し、その範囲でクリッピングをかけます
/*!
	@param[in]	g		キャンバス(このインスタンスを使用してクリッピングを行います)
	@param[in]	rect	コンターマップの矩形座標
	@param[in]	pts		クリップラインを描画する時に使用する座標

	ここの関数ではクリッピングのみを行い、クリップラインの描画は行いません
	pts引数に描画用の座標を格納し、呼び出し元でラインを描画します
	(クリップライン自体がクリッピングされないようにするため)
*/
void OCContourMap::Clipping(ICanvas* g, OCRect const& rect, std::vector<std::vector<OCPoint>>* pts) const
{
	double xMax = m_data->GetMaximum(0);
	double xMin = m_data->GetMinimum(0);
	double yMax = m_data->GetMaximum(1);
	double yMin = m_data->GetMinimum(1);

	// クリッピング領域を格納する配列
	std::vector<OCPoint> ptsUpper;	// 上限クリッピング
	std::vector<OCPoint> ptsLower;	// 下限クリッピング
	// 上限クリッピング領域の算出
	if (m_clipType == CCT_UPPER || m_clipType == CCT_ALL)
	{
		m_clipLine->SetType(CCT_UPPER, m_clipType == 3);
		m_clipLine->SetLineType(m_clipLineType);
		m_clipLine->CalcClipping(xMax, xMin, yMax, yMin, false, false, rect);

		for (size_t i = 0, size = m_clipLine->PointSize(); i < size; ++i) {
			ptsUpper.push_back(m_clipLine->Point(i));
		}
	}
	// 下限クリッピング領域の算出
	if (m_clipType == CCT_LOWER || m_clipType == CCT_ALL)
	{
		m_clipLine->SetType(CCT_LOWER, m_clipType == 3);
		m_clipLine->SetLineType(m_clipLineType);
		m_clipLine->CalcClipping(xMax, xMin, yMax, yMin, false, false, rect);

		for (size_t i = 0, size = m_clipLine->PointSize(); i < size; ++i) {
			ptsLower.push_back(m_clipLine->Point(i));
		}
	}
	// クリッピング領域を描く＆クリッピング領域の設定
	// 全体クリッピングの場合
	if(m_clipType == CCT_ALL) {
		// uppper/lowerは点数が同じで、[n].xの値も一致する(同じ方法で補間しているので)
		if (ptsUpper.empty() || ptsLower.empty()) { return; }
		BOOST_ASSERT(ptsUpper.size() == ptsLower.size());

		// クリップラインを描くときに上のラインと下のラインが交差しないよう調整する
		// [n].yが逆転した時点で交差したものとみなして、それ以前/以降は無視する
		size_t firstIndex = ptsUpper.size() - 2;
		size_t lastIndex = 1;
		for (size_t i = 1; i < ptsUpper.size() - 1; ++i) {
			// クリップラインが交差しないとき
			if (ptsUpper[i].y < ptsLower[i].y) {
				firstIndex = std::min<size_t>(firstIndex, i);
				lastIndex = std::max<size_t>(lastIndex, i);
			}
		}
		bool const leftOk = firstIndex == 1;
		bool const rightOk = lastIndex == ptsUpper.size() - 2;

		// はじめに、領域の線をすべて描いてから、クリッピング領域の設定を行う
		std::vector<OCPoint> ptsLeft;	// 左端の線の配列
		std::vector<OCPoint> ptsRight;	// 右端の線の配列

		SetClipPoint(ptsUpper, firstIndex, lastIndex, pts, leftOk ? &ptsLeft : nullptr, rightOk ? &ptsRight : nullptr);
		SetClipPoint(ptsLower, firstIndex, lastIndex, pts, leftOk ? &ptsLeft : nullptr, rightOk ? &ptsRight : nullptr);

		// 左端の線を描く
		if (!ptsLeft.empty()) {
			pts->push_back(ptsLeft);
		}
		// 右端の線を描く
		if (!ptsRight.empty()) {
			pts->push_back(ptsRight);
		}
		// 上限のクリッピング領域の設定
		g->SetClipping(&ptsUpper[0], ptsUpper.size());
		// 下限のクリッピング領域の設定
		g->SetClipping(&ptsLower[0], ptsLower.size());
	}
	// 上限クリッピングもしくは下限クリッピングの場合
	else {
		// クリッピング曲線を描く際、前2点と後ろの2点は省く
		// 上限クリッピングの線の描画＆領域設定
		if (ptsUpper.size() > 4) {
			pts->push_back(std::vector<OCPoint>(&ptsUpper[2], &ptsUpper[ptsUpper.size() - 2]));
		}
		if (!ptsUpper.empty()) {
			g->SetClipping(&ptsUpper[0], ptsUpper.size());
		}
		// 下限クリッピングの線の描画＆領域設定
		if (ptsLower.size() > 4) {
			pts->push_back(std::vector<OCPoint>(&ptsLower[2], &ptsLower[ptsLower.size() - 2]));
		}
		// 下限クリッピングの線の領域設定
		if (!ptsLower.empty()) {
			g->SetClipping(&ptsLower[0], ptsLower.size());
		}
	}
}

//! マップを作り直します
void OCContourMap::RecreateImage()
{
	switch (m_mapType)
	{
	case 0:
		// 型が変わった場合のみ生成しなおす
		// BK(具象クラスへのキャストは行わないようにすること)
		if (!dynamic_cast<OCContourImage*>(m_image.get()))
		{
			OCContourImage* img = new OCContourImage(m_plot);
			m_image.reset(img);
		}
		break;
	case 1:
		//m_image.reset(new OCTriangleImage(m_plot));
		assert(false);
		break;
	}

	m_image->SetFillType(m_fillType);
	m_image->SetSliceType(m_sliceType);
	if (OCContourImage* img = dynamic_cast<OCContourImage*>(m_image.get())) {
		img->SetGradation(!!m_plot->SendMessage(PI_GET_ZAXIS_GRADATION, 0, 0));
	}
}

//! マップを更新します(クリップラインも作り直す)
void OCContourMap::Update(bool silent)
{
	RecreateImage();

	Calculate(!silent);
}

//! シリアライズで複製されたデータでマップイメージを作ります(クリップラインは作り直さない)
void OCContourMap::AfterSerializeUpdate()
{
	RecreateImage();

	// クリップラインは作り直さない(シリアライズしたものを使用する)
	Calculate(false, false);
}

//! マップ計算を実行します
void OCContourMap::Calculate(bool displayProgressbar, bool recreateClipLine)
{
	if (!IsVisible()) {
		// visible = trueになった時点で計算するよう画像を削除しておく
		m_image->Destroy();
		return;
	}

	// 本当はm_data->GetItemId(0,1,2)まで見たいが、
	// 他の動作での影響が分からないのでコメントで残すにとどめる
	if (m_data->GetDataCount() == 0
		/*|| m_data->GetItemId(0) < 0
		|| m_data->GetItemId(1) < 0
		|| m_data->GetItemId(2) < 0*/) {
		m_image->Destroy();
		m_clipLine->ClearInputData();
		return;
	}

	if (m_pattern) {
	    m_pattern->SetMinMax(m_data->GetMinimum(2), m_data->GetMaximum(2));
		m_image->SetColorPattern(m_pattern);

		// コンターマップの画像は 10x10cm で作成しておく
		if (recreateClipLine) { m_clipLine->ClearInputData(); }
		if (!m_image->Interpolation(m_data, *(double*)m_plot->SendMessage(PI_GET_ZAXIS_INTERVAL, 0, 0), OCRect(0, 0, 100, 100), displayProgressbar)) {
			// エラーメッセージを表示すると全体の再描画が走ってしまう(WM_PAINTを送るため)
			// そうなるとデータを更新していない他のグラフの再描画も走って、
			// ファイル入れ替え等をしていると消えたファイルにアクセスして落ちる可能性がある
			// なのでメッセージボックスは出さないようにした(Redmine欠陥 #406)
			// http://cmmisvr1.yoko.onosokki.co.jp/redmine2/issues/406?issue_count=15&issue_position=1&next_issue_id=403
			//HWND wnd = reinterpret_cast<HWND>(m_plot->SendMessage(PI_MSG_GET_VIEW_HANDLE, 0, 0));
			//std::wstring msg = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_FAILED_CALCULATION_KON_TAHA_MAP");
			//::MessageBox(wnd, msg.c_str(), ::GetResourceString(m_plot,"ID_SOURCE_TEXT_O_CHART"), MB_OK|MB_ICONERROR);

			// 失敗したので画像は消しておく
			m_image->Destroy();
		}
		else {
			if (recreateClipLine) { UpdateCliplineSetting(); }
		}
	}
}

float max_element2(float const* first, float const* last)
{
	float f = *first;
	while (first++ != last) {
		float t = *first;
		if (f == FLT_MAX) {
			f = t;
		}
		else if (t != FLT_MAX) {
			f = std::max<float>(f, t);
		}
	}
	return f;
}

std::pair<float, float> minmax_element(float* first, float* last)
{
	float min = *first, max = *first;
	float* second = ++first;
	if (min == FLT_MAX) {
		while (*second != FLT_MAX) { ++second; }
		min = max = *second;
	}

	while (second != last) {
		float t = *second;
		if (t != FLT_MAX) {
			min = std::min<float>(*second, min);
			max = std::max<float>(*second, max);
		}
	}
	return std::make_pair(min, max);
}

//! 固有のプロパティを追加します
void OCContourMap::AddProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode)
{
	IPropertyGroup* grp = prop->CreateGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_CONTOUR_MAP"), PI_CONTOUR_VISIBLE, m_visible);
	/*if (m_visible)*/ {
		// 初回バージョンでは補間タイプの切り替えには対応しない
		//grp->AddArrayProperty(_T("補間タイプ"), PI_CONTOUR_MAPTYPE, m_mapType, !m_visible, 2, _T("格子"), _T("三角分割"));

		std::wstring fillProp1 = ::GetResourceString(m_plot,"ID_RESOURCE_ON");
		std::wstring fillProp2 = ::GetResourceString(m_plot,"ID_RESOURCE_OFF_ENG");
		std::wstring fillProp3 = GetResourceString(m_plot, "ID_SOURCE_TEXT_OFF_LESS_MIN");
		grp->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_PAINT_FILL"), PI_CONTOUR_FILLING, m_fillType, !m_visible, 3, 
			fillProp1.c_str(), fillProp2.c_str(), fillProp3.c_str());

		grp->AddTransparentProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_TRANSPARENCY_PAR"), PI_TRANSPARENT, m_transparent, !m_visible);

		std::wstring typeProp1 = GetResourceString(m_plot, "ID_SOURCE_TEXT_DESIGNATED_COLOR");
		std::wstring typeProp2 = GetResourceString(m_plot, "ID_SOURCE_TEXT_LEVEL_COLOR");
		std::wstring typeProp3 = GetResourceString(m_plot, "ID_SOURCE_TEXT_NONE");
		grp->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_CONTOUR_LINE_TYPE"), PI_CONTOUR_SLICE_TYPE, m_sliceType, !m_visible, 3, 
			typeProp1.c_str(), typeProp2.c_str(), typeProp3.c_str());

		std::wstring clippingProp1 = GetResourceString(m_plot, "ID_SOURCE_TEXT_NONE");
		std::wstring clippingProp2 = GetResourceString(m_plot, "ID_SOURCE_TEXT_UPPER_LIMIT");
		std::wstring clippingProp3 = GetResourceString(m_plot, "ID_SOURCE_TEXT_LOWER_LIMIT");
		std::wstring clippingProp4 = GetResourceString(m_plot, "ID_SOURCE_TEXT_WHOLE");
		grp->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_CLIPPING"), PI_CONTOUR_CLIPPING, m_clipType, !m_visible, 4, 
			clippingProp1.c_str(), clippingProp2.c_str(), clippingProp3.c_str(), clippingProp4.c_str());

		/*if (m_clipType != CCT_NONE && m_clipType != CCT_ALL)*/ {
			bool disable = !m_visible || m_clipType == CCT_NONE;

			IPropertyGroup* clipGroup = grp->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_CLIPPING"));
			std::wstring v = boost::lexical_cast<std::wstring>(m_clipLine->GetClipPointCount());
			clipGroup->AddCustomDialogProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_CLIP_LINE"), PI_CONTOUR_CLIPLINE, v.c_str(), 0, disable || m_clipType == CCT_ALL);

			std::wstring line1 = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_STRAIGHT_LINE");
			std::wstring line2 = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_CURVE");
			clipGroup->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_LINE_TYPE"), PI_CLIPLINE_LINETYPE, m_clipLineType, disable, 2, 
				line1.c_str(), line2.c_str());

			MakeLineProperty(m_plot, clipGroup, g, disable, m_clipLineStyle, PI_CLIPLINE_STYLE);
			clipGroup->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_KANJI"), PI_CLIPLINE_COLOR, m_clipLineColor, disable || m_clipLineStyle == PS_NULL);
			clipGroup->AddSelectorPropertyR(::GetResourceString(m_plot,"ID_SOURCE_TEXT_THICKNESS_PT"), PI_CLIPLINE_WIDTH, disable || m_clipLineStyle == PS_NULL,
				m_clipLineWidth, 9, 0.25, 0.5, 0.75, 1.0, 1.5, 2.25, 3.0, 4.5, 6.0);

			std::wstring corner1 = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_ROUND");
			std::wstring corner2 = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_SQUARE");
			clipGroup->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SHAPE_OF_CORNER"), PI_CLIPLINE_ROUNDTYPE, m_clipLineRoundType, disable || m_clipLineStyle == PS_NULL,
				2, corner1.c_str(), corner2.c_str());
		}

		m_marker->AddProperty(grp, !m_visible, g);

		m_image->AddProperty(grp, !m_visible, plotMode);
	}
}

//! 固有のプロパティを追加します
void OCContourMap::AddTabProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode)
{
	IPropertyGroup* grp = prop->CreateGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_CONTOUR_MAP"));
	IPropertyGroup* parent = grp->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_CONTOUR_MAP"), PI_CONTOUR_VISIBLE, m_visible);
	/*if (m_visible)*/ {
		// 初回バージョンでは補間タイプの切り替えには対応しない
		//grp->AddArrayProperty(_T("補間タイプ"), PI_CONTOUR_MAPTYPE, m_mapType, !m_visible, 2, _T("格子"), _T("三角分割"));

		std::wstring fillProp1 = ::GetResourceString(m_plot,"ID_RESOURCE_ON");
		std::wstring fillProp2 = ::GetResourceString(m_plot,"ID_RESOURCE_OFF_ENG");
		std::wstring fillProp3 = GetResourceString(m_plot, "ID_SOURCE_TEXT_OFF_LESS_MIN");
		parent->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_PAINT_FILL"), PI_CONTOUR_FILLING, m_fillType, !m_visible, 3, 
			fillProp1.c_str(), fillProp2.c_str(), fillProp3.c_str());

		parent->AddTransparentProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_TRANSPARENCY_PAR"), PI_TRANSPARENT, m_transparent, !m_visible);

		std::wstring typeProp1 = GetResourceString(m_plot, "ID_SOURCE_TEXT_DESIGNATED_COLOR");
		std::wstring typeProp2 = GetResourceString(m_plot, "ID_SOURCE_TEXT_LEVEL_COLOR");
		std::wstring typeProp3 = GetResourceString(m_plot, "ID_SOURCE_TEXT_NONE");
		parent->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_CONTOUR_LINE_TYPE"), PI_CONTOUR_SLICE_TYPE, m_sliceType, !m_visible, 3, 
			typeProp1.c_str(), typeProp2.c_str(), typeProp3.c_str());

		std::wstring clippingProp1 = GetResourceString(m_plot, "ID_SOURCE_TEXT_NONE");
		std::wstring clippingProp2 = GetResourceString(m_plot, "ID_SOURCE_TEXT_UPPER_LIMIT");
		std::wstring clippingProp3 = GetResourceString(m_plot, "ID_SOURCE_TEXT_LOWER_LIMIT");
		std::wstring clippingProp4 = GetResourceString(m_plot, "ID_SOURCE_TEXT_WHOLE");
		parent->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_CLIPPING"), PI_CONTOUR_CLIPPING, m_clipType, !m_visible, 4, 
			clippingProp1.c_str(), clippingProp2.c_str(), clippingProp3.c_str(), clippingProp4.c_str());

		/*if (m_clipType != CCT_NONE && m_clipType != CCT_ALL)*/ {
			bool disable = !m_visible || m_clipType == CCT_NONE;

			IPropertyGroup* clipGroup = parent->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_CLIPPING"));
			std::wstring v = boost::lexical_cast<std::wstring>(m_clipLine->GetClipPointCount());
			clipGroup->AddCustomDialogProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_CLIP_LINE"), PI_CONTOUR_CLIPLINE, v.c_str(), 0, disable || m_clipType == CCT_ALL);

			std::wstring line1 = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_STRAIGHT_LINE");
			std::wstring line2 = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_CURVE");
			clipGroup->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_LINE_TYPE"), PI_CLIPLINE_LINETYPE, m_clipLineType, disable, 2, 
				line1.c_str(), line2.c_str());

			MakeLineProperty(m_plot, clipGroup, g, disable, m_clipLineStyle, PI_CLIPLINE_STYLE);
			clipGroup->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_KANJI"), PI_CLIPLINE_COLOR, m_clipLineColor, disable || m_clipLineStyle == PS_NULL);
			clipGroup->AddSelectorPropertyR(::GetResourceString(m_plot,"ID_SOURCE_TEXT_THICKNESS_PT"), PI_CLIPLINE_WIDTH, disable || m_clipLineStyle == PS_NULL,
				m_clipLineWidth, 9, 0.25, 0.5, 0.75, 1.0, 1.5, 2.25, 3.0, 4.5, 6.0);

			std::wstring corner1 = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_ROUND");
			std::wstring corner2 = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_SQUARE");
			clipGroup->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SHAPE_OF_CORNER"), PI_CLIPLINE_ROUNDTYPE, m_clipLineRoundType, disable || m_clipLineStyle == PS_NULL,
				2, corner1.c_str(), corner2.c_str());
		}

		m_marker->AddTabProperty(parent, !m_visible, g);

		m_image->AddTabProperty(parent, !m_visible, plotMode);
	}
}

//! プロパティを設定します
bool OCContourMap::SetProperty(long id, LPARAM value, LPARAM* source)
{
	switch (id)
	{
	case PI_CONTOUR_VISIBLE:
		::SetPropertyHelper(m_visible, value, source);
		Visible(m_visible);
		if (m_visible && m_image->IsEmpty()) {
			Calculate();
		}
		return true;
	case PI_CONTOUR_CLIPPING:
		m_clipType = static_cast<ContourClippingType>(value);
		m_clipLine->ClearInputData();
		if (m_clipType == CCT_ALL) {
			UpdateCliplineSetting();
		}
		return true;
	case PI_CONTOUR_CLIPLINE:
		if (source) {
			// ポインタを渡してやる(コピー先で、メモリを確保してコピーすること)
			*reinterpret_cast<ClippingSetting**>(source) = &const_cast<ClippingSetting&>(m_clipSetting);
		}
		m_clipSetting = *reinterpret_cast<ClippingSetting*>(value);
		SetCliplineSetting(m_clipSetting);
		return true;
	case PI_CONTOUR_MAPTYPE:
		m_mapType = static_cast<int>(value);
		Update(false);
		return true;
	case PI_TRANSPARENT:
		::SetPropertyHelper(m_transparent, value, source);
		m_plot->SendMessage(PI_SET_ZAXIS_TRANSPARENT, m_transparent, 0);
		return true;
	case PI_CONTOUR_FILLING:
		::SetPropertyHelper(m_fillType, value, source);
		m_image->SetFillType(m_fillType);
		SendColorBarVisible();
		return true;
	case PI_CONTOUR_SLICE_TYPE:
		::SetPropertyHelper(m_sliceType, value, source);
		m_image->SetSliceType(m_sliceType);
		SendColorBarVisible();
		return true;
    case PI_COLOR_PATTERN:
        {
            m_pattern = reinterpret_cast<OCColorPattern*>(value);
        }
        return true;
	case PI_CLIPLINE_LINETYPE:
		m_clipLineType = static_cast<ContourClippingLineType>(value);
		if (m_clipType == CCT_ALL) {
			UpdateCliplineSetting();
		}
		return true;
	case PI_CLIPLINE_STYLE:
		::SetPropertyHelper(m_clipLineStyle, value, source);
		return true;
	case PI_CLIPLINE_COLOR:
		::SetPropertyHelper(m_clipLineColor, value, source);
		return true;
	case PI_CLIPLINE_WIDTH:
		::SetPropertyHelper(m_clipLineWidth, value, source);
		return true;
	case PI_CLIPLINE_ROUNDTYPE:
		::SetPropertyHelper(m_clipLineRoundType, value, source);
		return true;
	default:
		{
			bool calc = false;
			if (m_image->SetProperty(id, value, source, calc)) {
				if (calc) { Calculate(); }
				return true;
			}
		}
	}

	bool success = m_marker->SetProperty(id, value, source);
	success |= __super::SetProperty(id, value, source);
	return success;
}

bool OCContourMap::GetProperty(long id, LPARAM* ret) const
{
	switch (id)
	{
	case PI_CONTOUR_CLIPLINE:
		const_cast<OCContourMap*>(this)->UpdateCliplineSetting();
		*reinterpret_cast<ClippingSetting**>(ret) = &const_cast<ClippingSetting&>(m_clipSetting);
		return true;
	case PI_SCATTER_VISIBLE:
		*ret = m_marker->IsVisible() ? TRUE : FALSE;
		return true;
	case PI_SCATTER_SIZE:
		return m_marker->GetProperty(id, ret);
	case PI_CONTOUR_FILLING:
		*ret = m_fillType;
		return true;
	case PI_CONTOUR_SLICE_TYPE:
		*ret = m_sliceType;
		return true;
	default:
		return m_image->GetProperty(id, ret);
	}
	return false;
}

void OCContourMap::CopyFrom(IPlotDrawer const* src)
{
	__super::CopyFrom(src);
	if (OCContourMap const* impl = dynamic_cast<OCContourMap const*>(src)) {
		m_clipLineStyle = impl->m_clipLineStyle;
		m_clipLineWidth = impl->m_clipLineWidth;
		m_clipLineColor = impl->m_clipLineColor;

		m_mapType = impl->m_mapType;
		m_clipType = impl->m_clipType;
		m_fillType = impl->m_fillType;
		m_sliceType = impl->m_sliceType;
		m_transparent = impl->m_transparent;

		m_clipLine.reset(impl->m_clipLine->Clone());
		m_clipLine->SetData(m_data);

		m_image->CopyFrom(impl->m_image.get());
		m_marker->CopyFrom(impl->m_marker.get());
	}
}

void OCContourMap::SetCliplineSetting(ClippingSetting const& setting)
{
	// プロットで使用しているファイルと別のファイルでクリッピングする場合、
	// 一時的に使用ファイルを切り替える(ちょっと効率悪いかも)
	struct FileChange {
		FileChange(IPlotData* data, ClippingFile const& file) : m_data(data)
		{
			m_oldFileId = m_data->GetFileId(0);
			m_oldXItem = m_data->GetItemId(0);
			m_oldYItem = m_data->GetItemId(1);
			m_oldZItem = m_data->GetItemId(2);

			m_data->SetLink(0, file.fileId, file.xItemNo);
			m_data->SetLink(1, file.fileId, file.yItemNo);
			// Z項目のアイテムIDも変更しないとデータ点数が食い違ってしまうことがある
			// 変更前と後のファイルのデータ点数が異なる場合、前のファイルの点数が使用されてしまう
			m_data->SetLink(2, file.fileId, file.xItemNo);

			m_data->LinkDataSet();
		}
		~FileChange()
		{
			m_data->SetLink(0, m_oldFileId, m_oldXItem);
			m_data->SetLink(1, m_oldFileId, m_oldYItem);
			m_data->SetLink(2, m_oldFileId, m_oldZItem);
			m_data->LinkDataSet();
		}
		IPlotData* m_data;
		int m_oldFileId, m_oldXItem, m_oldYItem, m_oldZItem;
	};

	m_clipLine->SetRotPer(setting.points.threshold);
	m_clipLine->ClearInputData();

	std::vector<OCPoint> data;
	if (setting.type == ClippingSetting::Point ||
		(setting.file.fileId == -1 || setting.file.xItemNo == -1 || setting.file.yItemNo == -1))
	{
		// 指定データを使用
		BitSet effective;
		BOOST_FOREACH(ClippingPoint::point_t const& pt, setting.points.extractPoints) {
			data.push_back(pt.second);
			effective.push_back(pt.first);
		}
		m_clipLine->SetEffective(effective);
	}
	else if (setting.file.filter) {
		// ファイルを使用、フィルタあり
		FileChange fc_(m_data, setting.file);

		BitSet effective;
		size_t index = setting.file.filter->FindFirst();
		while (index != static_cast<size_t>(-1)) {
			// 欠損値は除外する
			if (m_data->GetData(0, index) != FLT_MAX && m_data->GetData(1, index) != FLT_MAX)
			{
				data.push_back(OCPoint(
					m_data->GetData(0, index),
					m_data->GetData(1, index)));
				effective.push_back(true);
			}

			index = setting.file.filter->FindNext();
		}
		m_clipLine->SetEffective(effective);
	}
	else {
		// ファイルを使用、フィルタなし
		FileChange fc_(m_data, setting.file);
		BitSet effective;

		for (size_t i = 0, n = m_data->GetDataCount(); i < n; ++i) {
			// 欠損値は除外する
			if (m_data->GetData(0, i) != FLT_MAX && m_data->GetData(1, i) != FLT_MAX)
			{
				data.push_back(OCPoint(
					m_data->GetData(0, i),
					m_data->GetData(1, i)));
				effective.push_back(true);
			}
		}
		m_clipLine->SetEffective(effective);
	}

	if (m_clipType == CCT_UPPER) {
		m_clipLine->SetUpperData(data);
	}
	else if (m_clipType == CCT_LOWER) {
		m_clipLine->SetLowerData(data);
	}
}

//! クリップラインの設定を更新します
void OCContourMap::UpdateCliplineSetting()
{
	m_clipLine->SetRotPer(m_clipSetting.points.threshold);
	m_clipLine->SetData(m_data);
	m_clipLine->SetType(m_clipType, m_clipType == 3);

	m_clipSetting.points.extractPoints.clear();
	std::vector<OCPoint> pts = m_clipLine->GetSectionData();
	for (size_t i = 0, n = pts.size(); i < n; ++i) {
		m_clipSetting.points.extractPoints.push_back(
			std::make_pair(m_clipLine->IsPointEffective(i), pts[i]));
	}
}

//! カラーバーにコンターマップの状態を通知します
void OCContourMap::SendColorBarVisible()
{
	// コンターマップの状態に応じてカラーバーを表示するかどうかを切り替えるので
	// 塗り潰し設定などが変わったら通知する必要がある
	// 塗り潰し有効であるか、等高線がレベルカラーの場合に表示する
	m_plot->SendMessage(PI_ZAXIS_AUTOVISIBLECHANGE, 0, 0);
}

//! オブジェクトの状態をXMLで出力します
void OCContourMap::Serialize(ISerializer* ar)
{
	__super::Serialize(ar);

    // {Generated Code(by gen.rb)
    ar->Serialize(_T("MapType"), m_mapType, int());
	MAKE_ENUM_SERIALIZE(ar, _T("ClipType"), m_clipType, ContourClippingType);
	MAKE_ENUM_SERIALIZE(ar, _T("FillType"), m_fillType, ContourFilling::FillType);
	MAKE_ENUM_SERIALIZE(ar, _T("SliceType"), m_sliceType, Contour::SliceType);
    ar->Serialize(_T("Transparent"), m_transparent, short());

	MAKE_ENUM_SERIALIZE2(ar, _T("ClipLineLineType"), m_clipLineType, ContourClippingLineType, CCLT_CURVE);
	ar->Serialize(_T("ClipLineStyle"), m_clipLineStyle);
	ar->Serialize(_T("ClipLineWidth"), m_clipLineWidth);
	ar->Serialize(_T("ClipLineColor"), m_clipLineColor);
	ar->Serialize(_T("ClipLineRoundType"), m_clipLineRoundType, m_clipLineRoundType);
    // }

	{
		ISerializer* child = ar->ChildNode(_T("ClipSetting"));
		MAKE_ENUM_SERIALIZE(child, _T("Type"), m_clipSetting.type, ClippingSetting::Type);
		child->Serialize(_T("threshold"), m_clipSetting.points.threshold);
		child->Serialize(_T("FileId"), m_clipSetting.file.fileId);
		child->Serialize(_T("XItemNo"), m_clipSetting.file.xItemNo);
		child->Serialize(_T("YItemNo"), m_clipSetting.file.yItemNo);


		if (IsReadable(ar)) {
			// 3.2から、ファイル番号ではなくIDを記録するように変更した
			int fileNo;
			child->Serialize(_T("FileNo"), fileNo, -1);
			if (fileNo != -1) {
				// 3.1
				if (IFile* file = reinterpret_cast<IFile*>(m_plot->SendMessage(PI_MSG_GET_FILE, fileNo, 0))) {
					m_clipSetting.file.fileId = file->GetId();
				}
			}
		}
	}

	std::vector<OCPoint> points;
	std::vector<bool> effectives;
	if (IReadable* reader = dynamic_cast<IReadable*>(ar)) {
		if (IReadable* pointsNode = dynamic_cast<IReadable*>(reader->ChildNode(_T("Points")))) {
			for (size_t i = 0, n = pointsNode->ChildCount(); i < n; ++i) {
				ISerializer* pt = pointsNode->ChildNode(i);
				double x, y;
				bool effective;
				pt->Serialize(_T("X"), x);
				pt->Serialize(_T("Y"), y);
				pt->Serialize(_T("Effective"), effective);
				points.push_back(OCPoint(x, y));
				effectives.push_back(effective);
			}
		}
	}
	else {
		UpdateCliplineSetting();
		ISerializer* pointsNode = ar->ChildNode(_T("Points"));
		for (size_t i = 0, n = m_clipSetting.points.extractPoints.size(); i < n; ++i) {
			ISerializer* pt = pointsNode->ChildNode(_T("Point"));
			pt->Serialize(_T("X"), m_clipSetting.points.extractPoints[i].second.x);
			pt->Serialize(_T("Y"), m_clipSetting.points.extractPoints[i].second.y);
			pt->Serialize(_T("Effective"), m_clipSetting.points.extractPoints[i].first);
		}
	}

	m_image->Serialize(ar->ChildNode(_T("Image")));
	m_image->SetSliceType(m_sliceType);

	m_marker->Serialize(ar->ChildNode(_T("Marker")));

	if (dynamic_cast<IReadable*>(ar)) {
		UpdateCliplineSetting();
	}

	if (points.empty()) {
		SetCliplineSetting(m_clipSetting);
	}
	else {
		assert(points.size() == effectives.size());
		m_clipLine->ClearInputData();
		if (m_clipType == CCT_UPPER) {
			m_clipLine->SetUpperData(points);
		}
		else if(m_clipType == CCT_LOWER) {
			m_clipLine->SetLowerData(points);
		}
		m_clipLine->SetSectionData(points);
		m_clipLine->SetEffective(effectives);
		
		m_clipSetting.points.extractPoints.clear();
		for (size_t i = 0, n = points.size(); i < n; ++i) {
			m_clipSetting.points.extractPoints.push_back(std::make_pair(effectives[i], points[i]));
		}
	}

	// 旧バージョンのプロジェクトを読み込んだ時の処理
	if (IReadable* reader = dynamic_cast<IReadable*>(ar)) {
		// 2.02以前のバージョンでは透明度は 0-255 の値で保存されている
		if (reader->GetMajorVersion() <= 2 && reader->GetMinorVersion() <= 2) {
			m_transparent = 100 - static_cast<short>(m_transparent / (255.0 / 100.0));
		}
	}

	m_image->SetFillType(m_fillType);
	m_image->SetSliceType(m_sliceType);
}

void OCContourMap::AfterSerialize()
{
	m_plot->SendMessage(PI_SET_ZAXIS_TRANSPARENT, m_transparent, 0);
	m_image->AfterSerialize();

	//SendColorBarVisible();
}

//! スライスレベルが作成された
void OCContourMap::AddSliceLevel(OCSliceLevel* slice)
{
	if (IGroup* parent = dynamic_cast<IGroup*>(m_plot)) {
		slice->SetParent(parent);
	}
	ICanvas* canvas = reinterpret_cast<ICanvas*>(m_plot->SendMessage(PI_MSG_GET_CANVAS, 0, 0));
	slice->SetCanvas(canvas);

	m_plot->SendMessage(PI_MSG_ADD_CHILDOBJECT, reinterpret_cast<LPARAM>(slice), 0);
}
//! スライスレベルが削除された
void OCContourMap::RemoveSliceLevel(OCSliceLevel* slice)
{
	m_plot->SendMessage(PI_MSG_REMOVE_CHILDOBJECT, reinterpret_cast<LPARAM>(slice), 0);
}