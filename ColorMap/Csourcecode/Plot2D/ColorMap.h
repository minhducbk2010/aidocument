/*!	@file
	@date	2008-04-21
	@author	wada

	@brief	カラーマップ
*/
#pragma once

#include <atlimage.h>
#include "PlotLib/PlotImpl3D.h"

class OCColorPattern;

class OCColorMap : public OCPlotImpl3D
{
public:
	OCColorMap(HWND wnd, IMessageReceiver* owner);
	~OCColorMap();

	virtual void Destroy() { delete this; }

	virtual void Draw(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const;
	virtual void DrawSelected(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const;

	virtual void AddProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode);
	virtual void AddTabProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode);
    virtual bool SetProperty(long id, LPARAM value, LPARAM* source);

	virtual void CopyFrom(IPlotDrawer const* src);
	virtual void Serialize(ISerializer* ar);

	virtual void Update(bool silent);
	virtual void AfterSerializeUpdate();

private:
	void RecreateImage();

	mutable CImage m_image;
    OCColorPattern* m_pattern;
	bool m_visibleLine;
};
