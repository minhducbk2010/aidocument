#pragma once

struct RectClipping
{
	RectClipping(ICanvas* g, plot::ClippingType type, OCRect clipRect)
		: m_canvas(g)
	{
		switch (type)
		{
		case plot::XClip:
			clipRect.top = -9999;
			clipRect.bottom = 9999;
			m_canvas->SetClipping(clipRect);
			break;
		case plot::YClip:
			clipRect.left = -9999;
			clipRect.right = 9999;
			m_canvas->SetClipping(clipRect);
			break;
		case plot::AllClip:
			m_canvas->SetClipping(clipRect);
			break;
		}
	}

	void Reset()
	{
		m_canvas->ResetClipping();
	}

	~RectClipping()
	{
		Reset();
	}

	ICanvas* m_canvas;
};