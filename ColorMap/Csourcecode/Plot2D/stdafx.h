// stdafx.h : 標準のシステム インクルード ファイルのインクルード ファイル、または
// 参照回数が多く、かつあまり変更されない、プロジェクト専用のインクルード ファイル
// を記述します。
//

#pragma once

// 下で指定された定義の前に対象プラットフォームを指定しなければならない場合、以下の定義を変更してください。
// 異なるプラットフォームに対応する値に関する最新情報については、MSDN を参照してください。
#ifndef WINVER				// Windows XP 以降のバージョンに固有の機能の使用を許可します。
#define WINVER 0x0501		// これを Windows の他のバージョン向けに適切な値に変更してください。
#endif

#ifndef _WIN32_WINNT		// Windows XP 以降のバージョンに固有の機能の使用を許可します。                   
#define _WIN32_WINNT 0x0501	// これを Windows の他のバージョン向けに適切な値に変更してください。
#endif						

#ifndef _WIN32_WINDOWS		// Windows 98 以降のバージョンに固有の機能の使用を許可します。
#define _WIN32_WINDOWS 0x0410 // これを Windows Me またはそれ以降のバージョン向けに適切な値に変更してください。
#endif

#ifndef _WIN32_IE			// IE 6.0 以降のバージョンに固有の機能の使用を許可します。
#define _WIN32_IE 0x0600	// これを IE. の他のバージョン向けに適切な値に変更してください。
#endif

#define WIN32_LEAN_AND_MEAN		// Windows ヘッダーから使用されていない部分を除外します。
// Windows ヘッダー ファイル:
#include <windows.h>

#pragma warning(push)
#pragma warning(disable : 4127)
#include <atlcomcli.h>
#pragma warning(pop)
#include <atlbase.h>
#include <atlstr.h>
#include <Gdiplus.h>

#include <cstdlib>
#define _USE_MATH_DEFINES
#include <cmath>

// STL
#include <map>
#include <vector>
#include <algorithm>
#include <memory>

// Boost
#include <boost/shared_ptr.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/scoped_array.hpp>
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <boost/format.hpp>
#include <boost/assert.hpp>
#include <boost/foreach.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/range.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/algorithm/minmax_element.hpp>
#include <boost/utility.hpp>
#pragma warning(push)
#pragma warning(disable:4819)
#pragma warning(disable:4127)
#include <boost/lexical_cast.hpp>
#pragma warning(pop)

// O-Chart
#include "utility.h"

//#ifdef _DEBUG
//	// xdebug, xlocaleヘッダ内でエラーが起こるので、先にincludeしておかなければならない
//	#include <string>
//	// メモリ監視(メモリリーク検出)
//	#define _CRTDBG_MAP_ALLOC
//	#include <crtdbg.h>
//
//	#define new ::new(_NORMAL_BLOCK, __FILE__, __LINE__)
//
//	//inline void * (operator new)(size_t nSize, int nType, LPCSTR lpszFileName, int nLine, bool)
//	//{
//	//	char* str = (char*)HeapAlloc(GetProcessHeap(), 0, _MAX_PATH);
//	//	::strcpy_s(str, _MAX_PATH, lpszFileName);
//	//	return operator new(nSize, _NORMAL_BLOCK, str, nLine);
//	//}
//
//	//#define new ::new(_NORMAL_BLOCK, __FILE__, __LINE__, false)
//#endif