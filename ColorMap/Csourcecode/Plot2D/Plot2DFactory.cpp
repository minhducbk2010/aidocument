#include "StdAfx.h"
#include "Plot2DFactory.h"
#include "Scatter.h"
#include "LinePlot.h"
#include "BarPlot.h"
#include "CurvePlot.h"

OCPlot2DFactory::OCPlot2DFactory()
{
}

OCPlot2DFactory::~OCPlot2DFactory()
{
}

unsigned long OCPlot2DFactory::GetPlotCount() const
{
	return 4;
}

//wchar_t const* OCPlot2DFactory::GetPlotName(unsigned long index) const
//{
//	switch (index)
//	{
//	case 0: return ::GetResourceString(m_owner,"ID_SOURCE_TEXT_SCATTER_DIAGRAM");
//	case 1: return ::GetResourceString(m_owner,"ID_SOURCE_TEXT_FIGURE_LINE");
//	case 3: return ::GetResourceString(m_owner,"ID_SOURCE_TEXT_FIGURE_CURVE");
//	case 2: return ::GetResourceString(m_owner,"ID_SOURCE_TEXT_BAR_GRAPH");
//	}
//	return _T("");
//}

IPlotDrawer* OCPlot2DFactory::Create(HWND owner, IMessageReceiver* plot, unsigned long index) const
{
	switch (index)
	{
	case 0: return new OCScatter(owner, plot);
	case 1: return new OCLinePlot(owner, plot);
	case 2: return new OCCurvePlot(owner, plot);
	case 3: return new OCBarPlot(owner, plot);
	}
	return 0;
}