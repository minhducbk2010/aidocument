/*!	@file
	@date	2008-03-04
	@author	wada

	@brief	散布図
*/
#include "StdAfx.h"
#include <boost/algorithm/minmax_element.hpp>
#include <boost/scoped_array.hpp>
#include "ICanvas.h"
#include "IPlotData.h"
#include "property.h"
#include "PropertyCreator.hpp"
#include "PropertyHelper.hpp"
#include "ISerializer.h"
#include "PluginUtility/pluginutil.h"
#include "Scatter.h"
#include "DrawComboBoxMarker.h"

//! Constructor
OCScatter::OCScatter(HWND hWnd, IMessageReceiver* plot)
	: OCPlotImpl(hWnd, plot)
	, m_type(Marker::Dot), m_size(2), m_transparent(100)
	, m_checkZData(false)
{
}

//! Destructor
OCScatter::~OCScatter()
{
}

//! 散布図を描画します
void OCScatter::Draw(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const
{
	if (!IsVisible()) { return; }
	if (!m_data->GetDataCount()) { return; }

	double xMax = arg.xMax;
	double xMin = arg.xMin;
	double yMax = arg.yMax;
	double yMin = arg.yMin;

	// 点の時は透過しない
	g->SetBackground(m_fillColor, FS_SOLID, m_type == Marker::Dot ? 0 : m_transparent);
	g->SetPenStyle(m_lineStyle);
	g->SetPenColor(m_lineColor);
	g->SetPenWidth(m_lineWidth);

	// m_size はポイント単位なのでmmに変換する
	double const per = 1;//0.352778;
	double const size = m_type == Marker::Dot ? 0.5/*PxToMM(2)*/ : (m_size / 2) * per;

	// 座標計算時に四捨五入を行わないようにする(マーカーの大きさが位置によって変わるのを防ぐため)
	double yValue = 0;
	// 最後尾マーカーを点滅させる場合は最後のマーカーは書かない(ただし、印刷時は除く)
	size_t drawDataCount = m_blink && !g->IsPrinting() ? m_data->GetDataCount() - 1 : m_data->GetDataCount();
	for (size_t i = 0, count = 0; i < drawDataCount; ++i, ++count) {
		float const xv = m_data->GetRealData(0, (int)i);
		float const yv = m_data->GetData(1, (int)i);

		if (xv == FLT_MAX || yv == FLT_MAX) { continue; }
		if ((m_clippingType == plot::AllClip || m_clippingType == plot::XClip) && (xv > xMax || xv < xMin)) {
			continue;
		}
		if ((m_clippingType == plot::AllClip || m_clippingType == plot::YClip) && (yv > yMax || yv < yMin)) {
			continue;
		}

		yValue = m_accumulative ? yValue + yv : yv;
		double x = rect.Width() * arg.XNormalize(xv) + rect.left;
		double y = rect.Height() - rect.Height() * arg.YNormalize(yValue) + rect.top;
		
		if (m_checkZData && m_data->GetData(2, i) == FLT_MAX) {
			continue;
		}

		Marker::DrawMarker(g, m_type, x, y, size);
	}
}

void OCScatter::DrawSelected(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const
{
}

//! 凡例を描画します
void OCScatter::DrawLegend(ICanvas* g, OCRect const& rect, int ratio) const
{
	if (!IsVisible()) { return; }

	g->SetPen(m_lineColor, m_lineWidth, m_lineStyle, 1);
	g->SetBackground(m_fillColor, FS_SOLID, m_type == Marker::Dot ? 0 : m_transparent);

	double const per = ratio / 100.0;
	double const size = m_type == Marker::Dot ? 0.4 : m_size / 2 * per;
	OCPoint center(rect.left + rect.Width() / 2, rect.top + rect.Height() / 2);
	Marker::DrawMarker(g, m_type, center.x, center.y, size);
}

//! 点滅するマーカー(最後尾マーカーを消灯→点滅させます)
void OCScatter::DrawBlink(ICanvas* g, OCRect const& rect, DrawArgs const& arg)
{
	if (!IsVisible()) { return; }
	if (!m_data->GetDataCount()) { return; }

	if(m_blink)
	{
		double xMax = arg.xMax;
		double xMin = arg.xMin;
		double yMax = arg.yMax;
		double yMin = arg.yMin;

		// 点の時は透過しない
		g->SetBackground(m_fillColor, FS_SOLID, m_type == Marker::Dot ? 0 : m_transparent);
		g->SetPenStyle(m_lineStyle);
		g->SetPenColor(m_lineColor);
		g->SetPenWidth(m_lineWidth);

		// m_size はポイント単位なのでmmに変換する
		double const per = 1;//0.352778;
		double const size = m_type == Marker::Dot ? 0.5/*PxToMM(2)*/ : (m_size / 2) * per;

		// 座標計算時に四捨五入を行わないようにする(マーカーの大きさが位置によって変わるのを防ぐため)
		double yValue = 0;

		float const xv = m_data->GetRealData(0, (int)m_data->GetDataCount() - 1);
		float const yv = m_data->GetData(1, (int)m_data->GetDataCount() - 1);

		if (xv == FLT_MAX || yv == FLT_MAX) { return; }
		if ((m_clippingType == plot::AllClip || m_clippingType == plot::XClip) && (xv > xMax || xv < xMin)) {
			return;
		}
		if ((m_clippingType == plot::AllClip || m_clippingType == plot::YClip) && (yv > yMax || yv < yMin)) {
			return;
		}

		yValue = m_accumulative ? yValue + yv : yv;
		double x = rect.Width() * arg.XNormalize(xv) + rect.left;
		double y = rect.Height() - rect.Height() * arg.YNormalize(yValue) + rect.top;
		
		if (m_checkZData && m_data->GetData(2, (int)m_data->GetDataCount() - 1) == FLT_MAX) {
			return;
		}

		Marker::DrawMarker(g, m_type, x, y, size);
	}
}

//! 散布図の当たり判定
/*!
	@param[in]	rect	プロットの矩形座標
	@param[in]	arg		.
	@param[in]	x, y	マウス座標
	@return	(x, y)位置にマーカーがあればtrueを、そうでなければfalseを返します
*/
bool OCScatter::HitTest(OCRect const& rect, DrawArgs const& arg, double x, double y)
{
	if (!IsVisible()) { return false; }

	double const xMax = arg.xMax, xMin = arg.xMin, yMax = arg.yMax, yMin = arg.yMin;
	double const size = m_type == Marker::Dot ? 0.8 : (m_size / 2);

	double yValue = 0;
	for (size_t i = 0, count = 0; i < m_data->GetDataCount(); ++i, ++count) {
		float const xv = m_data->GetRealData(0, (int)i);
		float const yv = m_data->GetData(1, (int)i);

		if (xv == FLT_MAX || yv == FLT_MAX) { continue; }
		if ((m_clippingType == plot::AllClip || m_clippingType == plot::XClip) && (xv > xMax || xv < xMin)) {
			continue;
		}
		if ((m_clippingType == plot::AllClip || m_clippingType == plot::YClip) && (yv > yMax || yv < yMin)) {
			continue;
		}

		yValue = m_accumulative ? yValue + yv : yv;
		double px = rect.Width() * arg.XNormalize(xv) + rect.left;
		double py = rect.Height() - rect.Height() * arg.YNormalize(yValue) + rect.top;

		if (Marker::HitTest(m_type, px, py, size, OCPoint(x, y))) {
			return true;
		}
	}
	return false;
}

//! プロパティを作成します
void OCScatter::AddProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode)
{
	{
		IPropertyGroup* group = prop->CreateGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_MARKER"), PI_SCATTER_VISIBLE, m_visible, !plotMode);
		/*if (m_visible)*/
		{
			bool const readOnly = !m_visible || !plotMode;
			bool const dotSelect = m_type == Marker::Dot;
			bool const noFill = m_type == Marker::Cross || m_type == Marker::Plus;

			//group->AddArrayProperty(GetResourceString(m_plot, "ID_SOURCE_TEXT_TYPE"), PI_SCATTER_TYPE, m_type, readOnly, 9,
			//	/*_T("なし"), */_T("・"), _T("□"), _T("◇"), _T("▽"), _T("△") ,_T("○"), _T("×"), _T("＋"), _T("◎"));
			group->AddDrawComboProperty(GetResourceString(m_plot, "ID_SOURCE_TEXT_TYPE"), PI_SCATTER_TYPE, m_type, 9, DrawComboMarker);

			/*if (m_type != Marker::Dot)*/ {
				group->AddSelectorPropertyR(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SIZE_MM"), PI_SCATTER_SIZE, readOnly || dotSelect, m_size, 9, 0.1, 0.5, 1.0, 1.5, 2.0, 3.0, 4.0, 5.0, 10.0);
			}
			// ライン
			{
				IPropertyGroup* lineGroup = group->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_LINE"));
				MakeLineProperty(m_plot, lineGroup, g, readOnly, m_lineStyle, PI_SCATTER_LINESTYLE);
				lineGroup->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_KANJI"), PI_SCATTER_LINE_COLOR, m_lineColor, readOnly); // 旧 線の色
				lineGroup->AddSelectorPropertyR(::GetResourceString(m_plot,"ID_SOURCE_TEXT_THICKNESS_PT"), PI_SCATTER_LINE_WIDTH, readOnly, m_lineWidth, 9, 0.25, 0.5, 0.75, 1.0, 1.5, 2.25, 3.0, 4.5, 6.0); // 旧 線の太さ
			}
			
			// 塗りつぶし
			{
				IPropertyGroup* fillGroup = group->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_PAINT_FILL"));
				fillGroup->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_KANJI"), PI_SCATTER_FILL_COLOR, m_fillColor, readOnly || noFill); // 旧 塗りつぶし色
				fillGroup->AddTransparentProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_TRANSPARENCY_PAR"), PI_SCATTER_TRANSPARENT, m_transparent, readOnly || dotSelect || noFill);
			}
		}
	}
}

//! 散布図のプロパティをpropに追加します
void OCScatter::AddProperty(IPropertyGroup* prop, bool readOnly, ICanvas* g)
{
	// ここのマーカーは平面コンターのサブグループに属する(カラー散布図ではない)
	IPropertyGroup* group = prop->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_MARKER"), PI_SCATTER_VISIBLE, m_visible, readOnly);
	//parent = group->AddGroup(_T("マーカー"), PI_SCATTER_VISIBLE, m_visible, readOnly);
	/*if (m_visible)*/
	{
		bool const disable = !m_visible || readOnly;
		bool const dotSelect = m_type == Marker::Dot;
		bool const noFill = m_type == Marker::Cross || m_type == Marker::Plus;

		//group->AddArrayProperty(GetResourceString(m_plot, "ID_SOURCE_TEXT_TYPE"), PI_SCATTER_TYPE, m_type, disable, 9,
		//	/*_T("なし"), */_T("・"), _T("□"), _T("◇"), _T("▽"), _T("△") ,_T("○"), _T("×"), _T("＋"), _T("◎"));
		group->AddDrawComboProperty(GetResourceString(m_plot, "ID_SOURCE_TEXT_TYPE"), PI_SCATTER_TYPE, m_type, 9, DrawComboMarker);

		/*if (m_type != Marker::Dot)*/ {
			group->AddSelectorPropertyR(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SIZE_MM"), PI_SCATTER_SIZE, disable || dotSelect,
				m_size, 9, 0.1, 0.5, 1.0, 1.5, 2.0, 3.0, 4.0, 5.0, 10.0);
		}
		// ライン
		{
			IPropertyGroup* lineGroup = group->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_LINE"));
			MakeLineProperty(m_plot, lineGroup, g, readOnly, m_lineStyle, PI_SCATTER_LINESTYLE);
			lineGroup->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_KANJI"), PI_SCATTER_LINE_COLOR, m_lineColor, readOnly); // 旧 線の色
			lineGroup->AddSelectorPropertyR(::GetResourceString(m_plot,"ID_SOURCE_TEXT_THICKNESS_PT"), PI_SCATTER_LINE_WIDTH, readOnly, m_lineWidth, 9, 0.25, 0.5, 0.75, 1.0, 1.5, 2.25, 3.0, 4.5, 6.0); // 旧 線の太さ
		}
		
		// 塗りつぶし
		{
			IPropertyGroup* fillGroup = group->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_PAINT_FILL"));
			fillGroup->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_KANJI"), PI_SCATTER_FILL_COLOR, m_fillColor, readOnly || noFill); // 旧 塗りつぶし色
			fillGroup->AddTransparentProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_TRANSPARENCY_PAR"), PI_SCATTER_TRANSPARENT, m_transparent, readOnly || dotSelect || noFill);
		}
	}
}

//! タブ版　プロパティを作成します
void OCScatter::AddTabProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode)
{
		IPropertyGroup* group;
		IPropertyGroup* parent;

		group =prop->CreateGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_MARKER"));
		parent = group->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_MARKER"), PI_SCATTER_VISIBLE, m_visible, !plotMode);
		//IPropertyGroup* parent = group->AddGroup(_T("マーカー"), PI_SCATTER_VISIBLE, m_visible);
		/*if (m_visible)*/
		{
			bool const readOnly = !m_visible || !plotMode;
			bool const dotSelect = m_type == Marker::Dot;
			bool const noFill = m_type == Marker::Cross || m_type == Marker::Plus;

			//parent->AddArrayProperty(GetResourceString(m_plot, "ID_SOURCE_TEXT_TYPE"), PI_SCATTER_TYPE, m_type, readOnly, 9,
			//	/*_T("なし"), */_T("・"), _T("□"), _T("◇"), _T("▽"), _T("△") ,_T("○"), _T("×"), _T("＋"), _T("◎"));
			parent->AddDrawComboProperty(GetResourceString(m_plot, "ID_SOURCE_TEXT_TYPE"), PI_SCATTER_TYPE, m_type, 9, DrawComboMarker);

			/*if (m_type != Marker::Dot)*/ {
				parent->AddSelectorPropertyR(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SIZE_MM"), PI_SCATTER_SIZE, readOnly || dotSelect, m_size, 9, 0.1, 0.5, 1.0, 1.5, 2.0, 3.0, 4.0, 5.0, 10.0);
			}
			// ライン
			{
				IPropertyGroup* lineGroup = parent->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_LINE"));
				MakeLineProperty(m_plot, lineGroup, g, readOnly, m_lineStyle, PI_SCATTER_LINESTYLE);
				lineGroup->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_KANJI"), PI_SCATTER_LINE_COLOR, m_lineColor, readOnly); // 旧 線の色
				lineGroup->AddSelectorPropertyR(::GetResourceString(m_plot,"ID_SOURCE_TEXT_THICKNESS_PT"), PI_SCATTER_LINE_WIDTH, readOnly, m_lineWidth, 9, 0.25, 0.5, 0.75, 1.0, 1.5, 2.25, 3.0, 4.5, 6.0); // 旧 線の太さ
			}
			
			// 塗りつぶし
			{
				IPropertyGroup* fillGroup = parent->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_PAINT_FILL"));
				fillGroup->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_KANJI"), PI_SCATTER_FILL_COLOR, m_fillColor, readOnly || noFill); // 旧 塗りつぶし色
				fillGroup->AddTransparentProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_TRANSPARENCY_PAR"), PI_SCATTER_TRANSPARENT, m_transparent, readOnly || dotSelect || noFill);
			}
		}
}

//! タブ版　散布図のプロパティをpropに追加します
void OCScatter::AddTabProperty(IPropertyGroup* prop, bool readOnly, ICanvas* g)
{	
	IPropertyGroup* parent;
	
	parent = prop->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_MARKER"), PI_SCATTER_VISIBLE, m_visible, readOnly);
	// ここのマーカーは平面コンターのサブグループに属する(カラー散布図ではない)

	/*if (m_visible)*/
	{
		bool const disable = !m_visible || readOnly;
		bool const dotSelect = m_type == Marker::Dot;
		bool const noFill = m_type == Marker::Cross || m_type == Marker::Plus;

		
		//parent->AddArrayProperty(GetResourceString(m_plot, "ID_SOURCE_TEXT_TYPE"), PI_SCATTER_TYPE, m_type, disable, 9,
		//	/*_T("なし"), */_T("・"), _T("□"), _T("◇"), _T("▽"), _T("△") ,_T("○"), _T("×"), _T("＋"), _T("◎"));
		parent->AddDrawComboProperty(GetResourceString(m_plot, "ID_SOURCE_TEXT_TYPE"), PI_SCATTER_TYPE, m_type, 9, DrawComboMarker);

		/*if (m_type != Marker::Dot)*/ {
			parent->AddSelectorPropertyR(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SIZE_MM"), PI_SCATTER_SIZE, disable || dotSelect,
				m_size, 9, 0.1, 0.5, 1.0, 1.5, 2.0, 3.0, 4.0, 5.0, 10.0);
		}

		// ライン
		{
			IPropertyGroup* lineGroup = parent->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_LINE"));
			MakeLineProperty(m_plot, lineGroup, g, readOnly, m_lineStyle, PI_SCATTER_LINESTYLE);
			lineGroup->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_KANJI"), PI_SCATTER_LINE_COLOR, m_lineColor, readOnly); // 旧 線の色
			lineGroup->AddSelectorPropertyR(::GetResourceString(m_plot,"ID_SOURCE_TEXT_THICKNESS_PT"), PI_SCATTER_LINE_WIDTH, readOnly, m_lineWidth, 9, 0.25, 0.5, 0.75, 1.0, 1.5, 2.25, 3.0, 4.5, 6.0); // 旧 線の太さ
		}
		
		// 塗りつぶし
		{
			IPropertyGroup* fillGroup = parent->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_PAINT_FILL"));
			fillGroup->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_KANJI"), PI_SCATTER_FILL_COLOR, m_fillColor, readOnly || noFill); // 旧 塗りつぶし色
			fillGroup->AddTransparentProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_TRANSPARENCY_PAR"), PI_SCATTER_TRANSPARENT, m_transparent, readOnly || dotSelect || noFill);
		}
	}
}

//! プロパティを更新します
bool OCScatter::SetProperty(long id, LPARAM value, LPARAM* source)
{
	switch (id)
	{
	case PI_SCATTER_VISIBLE:
		if (source) { *source = m_visible ? 1 : 0; }
		m_visible = value != 0;
		return true;
	case PI_SCATTER_TYPE:
		::SetPropertyHelper(m_type, value, source);
		return true;
	case PI_SCATTER_SIZE:
		::SetPropertyHelper(m_size, value, source);
		return true;
	case PI_SCATTER_FILL_COLOR:
		if (source) { *source = m_fillColor; }
		m_fillColor = COLORREF(value);
		return true;
	case PI_SCATTER_TRANSPARENT:
		return ::SetPropertyHelper(m_transparent, value, source, (short)0, (short)100);
	case PI_SCATTER_LINE_COLOR:
		if (source) { *source = m_lineColor; }
		m_lineColor = COLORREF(value);
		return true;
	case PI_SCATTER_LINE_WIDTH:
		if (!::SetPropertyHelper(m_lineWidth, value, source, 0.1f, 100.0f)) {
			std::wstring msg = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_INPUT_THICKNESS_LINE_RANGE_01_100");
			::MessageBox(m_owner, msg.c_str(), ::GetResourceString(m_plot,"ID_SOURCE_TEXT_O_CHART"), MB_OK|MB_ICONINFORMATION);
			return false;
		}
		return true;
	case PI_SCATTER_BLINK:
		::SetPropertyHelper(m_blink, value, source);
		return true;
	case PI_SCATTER_LINESTYLE:
		::SetPropertyHelper(m_lineStyle, value, source);
		return true;
	}

	return __super::SetProperty(id, value, source);
}

//! idに対応するプロパティの現在の値を取得します
bool OCScatter::GetProperty(long id, LPARAM* ret) const
{
	switch (id)
	{
	case PI_SCATTER_SIZE:
		*ret = reinterpret_cast<LPARAM>(&m_size);
		return true;
	}
	return false;
}

//!
void OCScatter::CopyFrom(IPlotDrawer const* src)
{
	__super::CopyFrom(src);
	if (OCScatter const* self = dynamic_cast<OCScatter const*>(src)) {
		m_type = self->m_type;
		m_size = self->m_size;
		m_transparent = self->m_transparent;
	}
}

//! オブジェクトの状態をXMLで出力します
void OCScatter::Serialize(ISerializer* ar)
{
	__super::Serialize(ar);

	MAKE_ENUM_SERIALIZE(ar, _T("Type"), m_type, Marker::Type);
	// 旧バージョンの場合、m_typeが-1(None)の時がある
	if (m_type < 0) {
		m_visible = false;
		m_type = Marker::Dot;
	}
	ar->Serialize(_T("Size"), m_size);
	ar->Serialize(_T("Transparent"), m_transparent);

	if (IReadable* reader = dynamic_cast<IReadable*>(ar)) {
		// 3.5以前はスタイル設定がないので実線にする
		if ((reader->GetMajorVersion() == 3 && reader->GetMinorVersion() <= 5) || (reader->GetMajorVersion() < 3))
		{
			m_lineStyle = PS_SOLID;
		}
	}
}
