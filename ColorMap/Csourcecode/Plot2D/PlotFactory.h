#pragma once

#include <map>
#include "../../../Common/IPlotFactory.h"

class IFactory;
class IMessageReceiver;

class OCPlotFactory : public IPlotFactory
{
public:
	OCPlotFactory(HWND hWnd);
	~OCPlotFactory();

	virtual void Destroy() { delete this; }

	virtual unsigned long GetId() const;
	virtual unsigned long GetDimension() const;
	virtual unsigned long GetPlotCount(unsigned long dimension) const;
	virtual IPlotDrawer* Create(HWND owner, IMessageReceiver* plot, unsigned long dimension, unsigned long index) const;
	virtual IPlotDrawer* CreateBuildUpper() const;

//	virtual wchar_t const* GetPlotName(unsigned long dimension, unsigned long index) const;

private:
	typedef std::map<int, IFactory*> FactoryMap;
	FactoryMap m_factories;
};
