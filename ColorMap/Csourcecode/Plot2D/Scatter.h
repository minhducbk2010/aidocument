/*!	@file
	@date	2008-03-04
	@author	wada

	@brief	散布図
*/
#pragma once

#include "PlotLib/PlotImpl.h"
#include "MarkerDrawer.h"

class IPropertyGroup;

class OCScatter : public OCPlotImpl
{
public:
	OCScatter(HWND hWnd, IMessageReceiver* plot);
	~OCScatter();

	virtual void Destroy() { delete this; }

	virtual void Draw(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const;
	virtual void DrawSelected(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const;
	virtual void DrawLegend(ICanvas* g, OCRect const& rect, int ratio) const;
	virtual void DrawBlink(ICanvas* g, OCRect const& rect, DrawArgs const& arg);

	virtual bool HitTest(OCRect const& rect, DrawArgs const& arg, double x, double y);

	virtual void AddProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode);
	virtual void AddProperty(IPropertyGroup* prop, bool readOnly, ICanvas* g);
	virtual void AddTabProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode);
	virtual void AddTabProperty(IPropertyGroup* prop, bool readOnly, ICanvas* g);
	virtual bool SetProperty(long id, LPARAM value, LPARAM* source);
	virtual bool GetProperty(long id, LPARAM* ret) const;

	virtual void CopyFrom(IPlotDrawer const* src);
	virtual void Serialize(ISerializer* ar);

	void EnableZData() { m_checkZData = true; }

protected:

private:
	Marker::Type m_type;
	double m_size;
	short m_transparent;

	bool m_checkZData; //!< Z値が空欄であれば描画しない
};
