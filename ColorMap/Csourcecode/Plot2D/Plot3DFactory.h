#pragma once

#include "IFactory.h"

class OCPlot3DFactory : public IFactory
{
public:
	OCPlot3DFactory();
	~OCPlot3DFactory();

	virtual unsigned long GetPlotCount() const;
	//virtual wchar_t const* GetPlotName(unsigned long index) const;
	virtual IPlotDrawer* Create(HWND owner, IMessageReceiver* plot, unsigned long index) const;
};
