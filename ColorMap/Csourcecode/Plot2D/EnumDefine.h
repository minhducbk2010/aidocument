/*!	@file
	@date	2008-11-20
*/
#pragma once

namespace ContourFilling {
	enum FillType
	{
		ON,			//!< ON
		OFF,		//!< OFF
		NoMinPaint,	//!< 最小値以下塗りつぶしOFF
	};
}

namespace Contour {
	enum SliceType
	{
		UserColor,	//!< 指定色
		LevelColor,	//!< レベルカラー
		None,		//!< なし(表示しない)
	};
}