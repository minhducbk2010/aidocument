/*!	@file
	@date	2008-04-28
	@author	wada

	@brief	ベクトル図
*/
#include "StdAfx.h"
#include <atlimage.h>
#include <wtl/atlapp.h>
#include <wtl/atlgdi.h>
#include "ICanvas.h"
#include "IPlotData.h"
#include "ISerializer.h"
#include "PropertyCreator.hpp"
#include "VectorPlot.h"
#include "resource.h"
#include "IMessageReceiver.h"
#include "Message.h"
#include "geometry.h"

//! 配列中の絶対値の最大値を取得します
float abs_max(IPlotData* data, int dim)
{
	float f = FLT_MAX;
	for (size_t i = 0, count = data->GetDataCount(); i < count; ++i) {
		const float c = data->GetRawData(dim, i);
		const float abs_c = std::abs(c);
		if (f == FLT_MAX || (c != FLT_MAX && f < abs_c)) {
			f = abs_c;
		}
	}
	return f;
}

//! Constructor
OCVectorPlot::OCVectorPlot(HWND wnd, IMessageReceiver* plot)
	: OCPlotImpl3D(wnd, plot)
	, m_threshold(0.3)
	, m_arrowStyle(0), m_arrowSize(5), m_arrowAngle(25)
	, m_pointCenter(true)
{
	// O-Chart2.04と同じ値で同じ結果にならない?
	m_threshold = 0.2;
	m_arrowSize = 15;
}

//! Destructor
OCVectorPlot::~OCVectorPlot()
{
}

//! プロットで使用するデータを設定します
void OCVectorPlot::SetData(IPlotData* data)
{
	__super::SetData(data);

	if (data && data->GetDataCount()) {
		m_uMax = abs_max(data, 3);
		m_vMax = abs_max(data, 4);
		m_norm = std::sqrt(m_uMax * m_uMax + m_vMax * m_vMax);
	}
}

//! ベクトル図を描画します
void OCVectorPlot::Draw(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const
{
	DrawImpl(false, g, rect, arg);
}

void OCVectorPlot::DrawSelected(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const
{
	if (m_selected) {
		DrawImpl(true, g, rect, arg);
	}
}

void OCVectorPlot::DrawImpl(bool selected, ICanvas* g, OCRect const& rect, DrawArgs const& arg) const
{
	if (!IsVisible()) { return; }
	if (!m_data->GetDataCount()) { return; }

	if (selected) {
		g->SetPen(RGB(0, 0, 255), m_lineWidth, m_lineStyle, 1);
	}
	else {
		g->SetPen(m_lineColor, m_lineWidth, m_lineStyle, m_lineRoundType);
	}
	g->SetBackground(m_lineColor, FS_SOLID, 0);

    // 描画するたびに求めるのは無駄が多いので前もってやっておく
    float uvMax = 0;
    for (size_t i = 0, size = m_data->GetDataCount(); i < size; ++i) {
        float const u = m_data->GetRealData(3, (int)i);
		float const v = m_data->GetRealData(4, (int)i);
		if (u == FLT_MAX || v == FLT_MAX || IsZero(u) || IsZero(v)) { continue; }
        float const uv = std::sqrt(u * u + v * v);
        uvMax = std::max<float>(uv, uvMax);
    }

	double xMax = arg.xMax;
	double xMin = arg.xMin;
	double yMax = arg.yMax;
	double yMin = arg.yMin;

	// x,yがそれぞれ1のときのx,yの座標上の長さ
	double const xBase = rect.Width() * 1.0 / (xMax - xMin);
	double const yBase = rect.Height() * 1.0 / (yMax - yMin);

	for (size_t i = 0, size = m_data->GetDataCount(); i < size; ++i) {
		float const xValue = m_data->GetRealData(0, (int)i);
		float const yValue = m_data->GetData(1, (int)i);
		float const u = m_data->GetData(3, (int)i);
		float const v = m_data->GetData(4, (int)i);

		if (xValue == FLT_MAX || yValue == FLT_MAX || u == FLT_MAX || v == FLT_MAX) { continue; }
		
		// (X, Y)
		double xPos = rect.Width() * (xValue - xMin) / (xMax - xMin) + rect.left;
		double yPos = rect.Height() - rect.Height() * (yValue - yMin) / (yMax - yMin) + rect.top;
        OCPoint pos(xPos, yPos);

        double uv = std::sqrt(u * u + v * v);
        if (uvMax * m_threshold > uv) { continue; }

        // (X, Y)を起点に、線を引く
        //float const uRate = static_cast<float>((std::abs(u) / m_norm) * (u > 0 ? 1 : -1));
        //float const vRate = static_cast<float>((std::abs(v) / m_norm) * (v < 0 ? 1 : -1)); // 座標系が反対なのでY軸の符号は反転する

        // 線を描く
        // 矢印の先端座標
        double coef = uv / m_norm;
		// 描画する座標を考慮して角度を決める(軸の最大最小値で実際に描画するベクトルの角度が変わるため)
        double rad = std::atan2((double)(v * yBase), (double)(u * xBase));
        double x = std::cos(rad) * m_arrowSize * coef;
        double y = std::sin(rad) * m_arrowSize * coef * -1; // 符号は反転する
        OCPoint edge = OCPoint(pos.x + x, pos.y + y);

        OCPoint p[] = { pos, edge, };
		double distance = p[0].Distance(p[1]);
        if (distance < 3) { continue; }

        if (m_pointCenter) {
            // (X, Y)を矢の中心に置く場合、それぞれの座標をずらす
            double diffX = std::abs(p[1].x - p[0].x) / 2.0;
            if (u > 0) {
                diffX *= -1;
            }
            double diffY = std::abs(p[1].y - p[0].y) / 2.0;
            if (v < 0) {
                diffY *= -1;
            }

            p[0].x += diffX;
            p[0].y += diffY;
            p[1].x += diffX;
            p[1].y += diffY;
        }

        // 矢じり
        int const arrow = static_cast<int>(p[0].Distance(p[1]) / 3 + 1); // 矢じりの長さ
        OCPoint arrow_points[] = {
            RotatePoint(GetDistancePoint(p[0], p[1], arrow), p[1], m_arrowAngle),
            p[1],
            RotatePoint(GetDistancePoint(p[0], p[1], arrow), p[1], -m_arrowAngle),
            RotatePoint(GetDistancePoint(p[0], p[1], arrow), p[1], m_arrowAngle),
        };

		// 端点形状が中抜きの三角の場合はクリッピングすることで中抜きする
		if (m_arrowStyle == 1) {
			SetClipRegion(g, arrow_points, 3);
		}

		if (selected) {
			g->DrawRubberLines(p, 2);
		}
		else {
			g->DrawLines(p, 2);
		}

		if (m_arrowStyle == 1) {
			// クリッピングを解除する
			g->ResetClipping();
		}

        switch (m_arrowStyle)
		{
		case 0:
			if (selected) {
				g->DrawRubberLines(arrow_points, 3);
			}
			else {
				g->DrawLines(arrow_points, 3); // 最後の点は描かない
			}
            break;
        case 1:
			if (selected) {
				g->DrawRubberLines(arrow_points, 4);
			}
			else {
				g->DrawLines(arrow_points, 4);
			}
            break;
        case 2:
			if (selected) {
				g->DrawRubberLines(arrow_points, 4);
			}
			else {
				g->FillPolygon(arrow_points, 4);
			}
            break;
        }
    }
}

void OCVectorPlot::SetClipRegion(ICanvas* g, OCPoint* arrow_points, size_t size) const
{
	g->SetExcludeClipping(arrow_points, size);
}

//! プロパティを作成します
void OCVectorPlot::AddProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode)
{
	bool const readOnly = !plotMode;
	{
		IPropertyGroup* grp = prop->CreateGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_ARROW"));
		grp->AddProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_STANDARD_VECTOR"), PI_VECTOR_STD_VECTOR, m_norm);
		grp->AddProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_LENGTH_ARROW_MM"), PI_VECTOR_ARROW_LENGTH, m_arrowSize);
		grp->AddBoolProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_CENTERING"), PI_VECTOR_CENTERING, m_pointCenter);
		//grp->AddArrowStyleProperty(_T("矢じりの形状"), PI_VECTOR_ARROW_EDGE, m_arrowStyle);
		grp->AddDrawComboProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SHAPE_ARROW"), PI_VECTOR_ARROW_EDGE, m_arrowStyle, 3, OnDrawComboValue);
		grp->AddSelectorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_ANGLE_ARROW"), PI_VECTOR_ARROW_ANGLE, m_arrowAngle, 8, 15, 25, 35, 45, 55, 65, 75, 85);
	}
	{
		IPropertyGroup* grp = prop->CreateGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_THRESHOLD"));
		grp->AddProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_THRESHOLD"), PI_VECTOR_THRESHOLD, m_threshold);
	}
	{
		IPropertyGroup* grp = prop->CreateGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_LINE"));
		grp->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_KANJI"), PI_VECTOR_LINECOLOR, m_lineColor, readOnly || m_lineStyle == PS_NULL);
		grp->AddSelectorPropertyR(::GetResourceString(m_plot,"ID_SOURCE_TEXT_THICKNESS_PT"), PI_VECTOR_LINEWIDTH, readOnly || m_lineStyle == PS_NULL, 
			m_lineWidth, 9, 0.25, 0.5, 0.75, 1.0, 1.5, 2.25, 3.0, 4.5, 6.0);

		std::wstring corner1 = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_ROUND");
		std::wstring corner2 = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_SQUARE");
		grp->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SHAPE_OF_CORNER"), PI_VECTOR_LINEROUNDTYPE, m_lineRoundType, readOnly || m_lineStyle == PS_NULL,
			2, corner1.c_str(), corner2.c_str());
	}
}

//! プロパティを作成します
void OCVectorPlot::AddTabProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode)
{
	bool const readOnly = !plotMode;
	IPropertyGroup* grp = prop->CreateGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_VECTOR"));
	IPropertyGroup* parent;
	{	
		parent=grp->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_ARROW"));
		parent->AddProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_STANDARD_VECTOR"), PI_VECTOR_STD_VECTOR, m_norm);
		parent->AddProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_LENGTH_ARROW_MM"), PI_VECTOR_ARROW_LENGTH, m_arrowSize);
		parent->AddBoolProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_CENTERING"), PI_VECTOR_CENTERING, m_pointCenter);
		//grp->AddArrowStyleProperty(_T("矢じりの形状"), PI_VECTOR_ARROW_EDGE, m_arrowStyle);
		parent->AddDrawComboProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SHAPE_ARROW"), PI_VECTOR_ARROW_EDGE, m_arrowStyle, 3, OnDrawComboValue);
		parent->AddSelectorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_ANGLE_ARROW"), PI_VECTOR_ARROW_ANGLE, m_arrowAngle, 8, 15, 25, 35, 45, 55, 65, 75, 85);
	}
	{
		parent=grp->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_THRESHOLD"));
		parent->AddProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_THRESHOLD"), PI_VECTOR_THRESHOLD, m_threshold);
	}
	{
		parent=grp->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_LINE"));
		parent->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_KANJI"), PI_VECTOR_LINECOLOR, m_lineColor, readOnly || m_lineStyle == PS_NULL);
		parent->AddSelectorPropertyR(::GetResourceString(m_plot,"ID_SOURCE_TEXT_THICKNESS_PT"), PI_VECTOR_LINEWIDTH, readOnly || m_lineStyle == PS_NULL, 
			m_lineWidth, 9, 0.25, 0.5, 0.75, 1.0, 1.5, 2.25, 3.0, 4.5, 6.0);

		std::wstring corner1 = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_ROUND");
		std::wstring corner2 = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_SQUARE");
		parent->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SHAPE_OF_CORNER"), PI_VECTOR_LINEROUNDTYPE, m_lineRoundType, readOnly || m_lineStyle == PS_NULL,
			2, corner1.c_str(), corner2.c_str());
	}
}

//! プロパティを設定します
bool OCVectorPlot::SetProperty(long id, LPARAM value, LPARAM* source)
{
	switch (id)
	{
	case PI_VECTOR_STD_VECTOR:
		::SetPropertyHelper(m_norm, value, source);
		return true;
	case PI_VECTOR_ARROW_LENGTH:
		{
			double real = *reinterpret_cast<double*>(value);
			if (real > 0.0 && real < 1000.0) {
				if (source) { *(double*)source = m_arrowSize; }
				m_arrowSize = real;
				return true;
			}
			std::wstring msg = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_INPUT_MORE_0_LESS_1000_INTO_ARROW_LENGTH");
			::MessageBox(m_owner, msg.c_str(), ::GetResourceString(m_plot,"ID_SOURCE_TEXT_O_CHART"), MB_OK | MB_ICONINFORMATION);
		}
		return false;
	case PI_VECTOR_CENTERING:
		::SetPropertyHelper(m_pointCenter, value, source);
		return true;
	case PI_VECTOR_ARROW_EDGE:
		::SetPropertyHelper(m_arrowStyle, value, source);
		return true;
	case PI_VECTOR_ARROW_ANGLE:
		if (!::SetPropertyHelper(m_arrowAngle, value, source, 0, 90)) {
			std::wstring msg = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_INPUT_RANGE_0_90_INTO_ARROW_ANGLE");
			::MessageBox(m_owner, msg.c_str(), ::GetResourceString(m_plot,"ID_SOURCE_TEXT_O_CHART"), MB_OK | MB_ICONINFORMATION);
			return false;
		}
		return true;
	case PI_VECTOR_THRESHOLD:
		if (!::SetPropertyHelper(m_threshold, value, source, 0.0, 1.0)) {
			std::wstring msg = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_INPUT_RANGE_0_1_INTO_THRESHOLD");
			::MessageBox(m_owner, msg.c_str(), ::GetResourceString(m_plot,"ID_SOURCE_TEXT_O_CHART"), MB_OK | MB_ICONINFORMATION);
			return false;
		}
		return true;
	case PI_VECTOR_LINECOLOR:
		::SetPropertyHelper(m_lineColor, value, source);
		return true;
	case PI_VECTOR_LINEWIDTH:
		if (!::SetPropertyHelper(m_lineWidth, value, source, 0.1f, 100.0f)) {
			std::wstring msg = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_INPUT_THICKNESS_LINE_RANGE_01_100");
			::MessageBox(m_owner, msg.c_str(), ::GetResourceString(m_plot,"ID_SOURCE_TEXT_O_CHART"), MB_OK|MB_ICONINFORMATION);
			return false;
		}
		return true;
	case PI_VECTOR_LINEROUNDTYPE:
		::SetPropertyHelper(m_lineRoundType, value, source);
		return true;
	}

	return false;
}

HMODULE g_handle; // Plot2D.cpp

//! プロパティダイアログ内のコンボボックスの中身を描画します
void __stdcall OCVectorPlot::OnDrawComboValue(HDC dc, int index, RECT const& rect, UINT state)
{
	if ((state & ODS_SELECTED) != 0) {
		WTL::CBrush br(::CreateSolidBrush(RGB(128, 128, 128)));
		::FillRect(dc, &rect, br);
	}
	else  {
		::FillRect(dc, &rect, reinterpret_cast<HBRUSH>(::GetStockObject(WHITE_BRUSH)));
	}

	// 矢印の形状を描画する(ビットマップとしてリソースに組み込まれている)
	int const bmpWidth = 23;	// ビットマップの幅
	int const bmpHeight = 11;	// ビットマップの高さ
	HBITMAP bmp = reinterpret_cast<HBITMAP>(::LoadImage(g_handle, MAKEINTRESOURCE(index + IDB_TRIANGLE), IMAGE_BITMAP, bmpWidth, bmpHeight, LR_SHARED));
	assert(bmp);
	if (bmp) {
		CImage img;
		img.Attach(bmp);
		img.TransparentBlt(dc, rect.left + 4, rect.top + 4, bmpWidth, bmpHeight, 0xffffff);
		img.Detach();
	}
}

void OCVectorPlot::CopyFrom(IPlotDrawer const* src)
{
	__super::CopyFrom(src);
	if (OCVectorPlot const* self = dynamic_cast<OCVectorPlot const*>(src)) {
		m_threshold = self->m_threshold;
		m_norm = self->m_norm;
		m_arrowStyle = self->m_arrowStyle;
		m_arrowSize = self->m_arrowSize;
		m_arrowAngle = self->m_arrowAngle;
		m_pointCenter = self->m_pointCenter;
		m_lineColor = self->m_lineColor;
		m_lineWidth = self->m_lineWidth;
	}
}

//! 保存と復元
void OCVectorPlot::Serialize(ISerializer* ar)
{
	ar->Serialize(_T("Threshold"), m_threshold, 0.2);
	ar->Serialize(_T("Norm"), m_norm, m_norm); // デフォルトは3.0.1以前のもののため
	ar->Serialize(_T("ArrowStyle"), m_arrowStyle);
	ar->Serialize(_T("ArrowSize"), m_arrowSize, 15);
	ar->Serialize(_T("ArrowAngle"), m_arrowAngle, 25);
	ar->Serialize(_T("PointCenter"), m_pointCenter, true);
	ar->Serialize(_T("LineColor"), m_lineColor, 0);
	ar->Serialize(_T("LineWidth"), m_lineWidth, 1.0);
}