/*!	@file
	@date	2008-04-28

	@brief	ベクトル図
*/
#pragma once

#include "PlotLib/PlotImpl3D.h"


class OCVectorPlot : public OCPlotImpl3D
{
public:
	OCVectorPlot(HWND wnd, IMessageReceiver* plot);
	~OCVectorPlot();

	virtual void Destroy() { delete this; }

	virtual void SetData(IPlotData* data);

	virtual void Draw(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const;
	virtual void DrawSelected(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const;

	virtual void AddProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode);
	virtual void AddTabProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode);
	virtual bool SetProperty(long id, LPARAM value, LPARAM* source);

	virtual void CopyFrom(IPlotDrawer const* src);
	virtual void Serialize(ISerializer* ar);

private:
	void DrawImpl(bool selected, ICanvas* g, OCRect const& rect, DrawArgs const& arg) const;

	static void __stdcall OnDrawComboValue(HDC dc, int index, RECT const& rect, UINT state);

	void SetClipRegion(ICanvas* g, OCPoint* arrow_points, size_t size) const;

private:
	float m_uMax, m_vMax;	//!< ベクトルの最大、最小値
	double m_threshold;		//!< しきい値
	double m_norm;			//!< 基準ベクトル
	int m_arrowStyle;		//!< 矢の形状
	double m_arrowSize;		//!< 矢の長さ
	int m_arrowAngle;		//!< 矢の角度
	bool m_pointCenter;		//!< センタリング
};
