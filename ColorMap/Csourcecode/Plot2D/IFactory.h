#pragma once

class IPlotDrawer;
class IMessageReceiver;

class IFactory
{
public:
	virtual unsigned long GetPlotCount() const = 0;
	//virtual wchar_t const* GetPlotName(unsigned long index) const = 0;
	virtual IPlotDrawer* Create(HWND owner, IMessageReceiver* plot, unsigned long index) const = 0;
};