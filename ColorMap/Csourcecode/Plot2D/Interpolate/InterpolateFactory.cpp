#include "stdafx.h"
#include "InterpolateFactory.h"
#include "Interpolate/Spline.hpp"
#include "Interpolate/BSpline.hpp"
#include "Interpolate/Hermite.hpp"
#include "Interpolate/Lagrange.hpp"
#include "Interpolate/RegressionCalc.hpp"
#include "Interpolate/MovingAverage.hpp"
#include "Message.h"

struct OCInterpolateFactory::Creator
{
	typedef OCInterpolate* (__stdcall * CreateFunc)();

	Creator(std::wstring const& name, CreateFunc f)
		: name(name), create(f)
	{}

	std::wstring name;
	CreateFunc create;
};

template<class T>
OCInterpolate* __stdcall make_new()
{
	return new T();
}

//! Constructor
OCInterpolateFactory::OCInterpolateFactory(IMessageReceiver* owner)
{
	m_creator.push_back(boost::shared_ptr<Creator>(new Creator(::GetResourceString(owner,"ID_SOURCE_TEXT_THIRD_SPLINE"), make_new<OCSpline>)));
	m_creator.push_back(boost::shared_ptr<Creator>(new Creator(::GetResourceString(owner,"ID_SOURCE_TEXT_B_SPLINE"), make_new<OCBSpline>)));
	m_creator.push_back(boost::shared_ptr<Creator>(new Creator(::GetResourceString(owner,"ID_SOURCE_TEXT_L_MEAT"), make_new<OCHermite>)));
	m_creator.push_back(boost::shared_ptr<Creator>(new Creator(::GetResourceString(owner,"ID_SOURCE_TEXT_REGRESSION_CURVE"), make_new<OCRegressionCalc>)));
	m_creator.push_back(boost::shared_ptr<Creator>(new Creator(::GetResourceString(owner,"ID_SOURCE_TEXT_MOVING_AVERAGE"), make_new<OCMovingAverage>)));
	// 最初のバージョンでは使用しない(永久欠番かも)
	//m_creator.push_back(boost::shared_ptr<Creator>(new Creator(_T("ラグランジュ"), make_new<OCLagrange>)));
}

//! 指定位置の補間クラスを生成します
OCInterpolate* OCInterpolateFactory::Create(int index) const
{
	return m_creator[index]->create();
}

//! 指定位置の補間名称を取得します
std::wstring OCInterpolateFactory::GetName(int index) const
{
	return m_creator[index]->name;
}

//! 生成可能な補間クラスの数を取得します
int OCInterpolateFactory::GetCount() const
{
	return static_cast<int>(m_creator.size());
}