#pragma once

#include <vector>
#include <boost/shared_ptr.hpp>

class OCInterpolate;
class IMessageReceiver;

namespace Interpolation
{
	enum Type
	{
		Spline,			//!< 3次スプライン
		BSpline,		//!< Bスプライン
		Hermite,		//!< エルミート
		Regress,		//!< 回帰曲線
		MovingAverage,	//!< 移動平均
	};
};

class OCInterpolateFactory
{
public:
	OCInterpolateFactory(IMessageReceiver* owner);

	OCInterpolate* Create(int index) const;

	std::wstring GetName(int index) const;
	int GetCount() const;

private:
	struct Creator;
	std::vector<boost::shared_ptr<Creator>> m_creator;
};