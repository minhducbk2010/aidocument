#pragma once

#include "IFactory.h"

class OCPlot2DFactory : public IFactory
{
public:
	OCPlot2DFactory();
	~OCPlot2DFactory();

	virtual unsigned long GetPlotCount() const;
	//virtual wchar_t const* GetPlotName(unsigned long index) const;
	virtual IPlotDrawer* Create(HWND owner, IMessageReceiver* plot, unsigned long index) const;
};
