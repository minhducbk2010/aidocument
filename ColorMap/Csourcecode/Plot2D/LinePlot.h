/*!	@file
	@date	2008-04-01
	@author	wada

	@brief	折れ線グラフ
*/
#pragma once

#include <memory>
#include "PlotLib/PlotImpl.h"
#include "IOnlineSearchable.h"

class OCLineInterpolate;


class OCLinePlot : public OCPlotImpl, public IOnlineSearchable
{
	typedef std::vector<std::vector<OCPoint>> PointArray;

public:
	OCLinePlot(HWND hWnd, IMessageReceiver* plot);
	~OCLinePlot();

	virtual void Destroy() { delete this; }

	virtual void Draw(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const;
	virtual void DrawSelected(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const;

	virtual double GetYPosition(double x, OCRect const& rect, DrawArgs const& arg) const;
	virtual void GetPosition(size_t index, double* x, double* y, OCRect const& rect, DrawArgs const& arg) const;

	virtual bool HitTest(OCRect const& rect, DrawArgs const& arg, double x, double y);

	virtual void AddProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode);
	virtual void AddTabProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode);
	virtual bool SetProperty(long id, LPARAM value, LPARAM* source);
	virtual bool GetProperty(long id, LPARAM* ret) const;

	virtual void CopyFrom(IPlotDrawer const* src);
	virtual void Serialize(ISerializer* ar);

	virtual IPlotDrawer* Clone();

// Overrides OnlineSearchable
	virtual void EnableOnlineMode(bool enable);
	virtual bool GetNearPosition(OCPoint* p, OCPoint* value, OCRect const& rect, DrawArgs const& arg, bool next) const;
	virtual void GetSearchPointFromValue(OCPoint* point, OCPoint* value, OCRect const& rect) const;
	virtual void GetPointByValue(OCRect const& rect, OCPoint const& target, OCPoint* point, OCPoint3D* value);

// Overrides ISearchable
	virtual void BeginSearch(DrawArgs const& arg, bool interpolate, ExclusionScale exclude, OCRect const& rect);

protected:
	void DrawImpl(bool selected, ICanvas* g, OCRect const& rect, DrawArgs const& arg) const;

	void GetDrawPoints2(std::vector<OCPoint>& pts, OCRect const& rect, DrawArgs const& arg) const;

	void GetDrawPoints(PointArray& arr, OCRect const& rect, DrawArgs const& arg) const;
	void GetCompressedDrawPoints(PointArray& arr, OCRect const& rect, DrawArgs const& arg) const;
	void CompressPoint(std::vector<OCPoint> const& temp, std::vector<OCPoint>* pts) const;
	void GetNormalDrawPoints(PointArray& arr, OCRect const& rect, DrawArgs const& arg) const;

private:
	bool m_filling;
	short m_fillTransparent;			//!< 透明度
	bool m_interpolate;

	std::auto_ptr<OCLineInterpolate> m_inter;
	double m_max, m_min;

	friend class LineWrapper;
};
