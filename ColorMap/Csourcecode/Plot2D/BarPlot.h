/*!	@file
	@date	2008-03-06
	@author	wada

	@brief	棒グラフクラスの宣言
*/
#pragma once

#include "PlotLib/PlotImpl.h"

enum BASE_LINE
{
	BL_STANDARD,
	BL_MINIMUM,
	BL_MAXIMUM,
	BL_NONE,
};

class OCBarPlot : public OCPlotImpl
{
public:
	OCBarPlot(HWND hWnd, IMessageReceiver* owner);
	~OCBarPlot();

	virtual void Destroy() { delete this; }

	virtual void Draw(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const;
	virtual void DrawSelected(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const;
	virtual void DrawLegend(ICanvas* g, OCRect const& rect, int ratio) const;

	virtual void GetPosition(size_t index, double* x, double* y, OCRect const& rect, DrawArgs const& arg) const;
	virtual bool GetPointBySearchIndex(OCRect const& rect, int index, OCPoint* point, OCPoint3D* value);
	virtual void GetXMinMax(float* min, float* max) const;
	virtual void GetYMinMax(float* min, float* max) const;

	virtual bool HitTest(OCRect const& rect, DrawArgs const& arg, double x, double y);

	virtual void AddProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode);
	virtual void AddTabProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode);
	virtual bool SetProperty(long id, LPARAM value, LPARAM* source);

	virtual void CopyFrom(IPlotDrawer const* src);
	virtual void Serialize(ISerializer* ar);

protected:

private:
	void DrawImpl(bool selected, ICanvas* g, OCRect const& rect, DrawArgs const& arg) const;
	typedef std::vector<std::pair<bool, OCRect>> BarRectArray;
	void GetBarRect(OCRect const& rect, DrawArgs const& arg, BarRectArray* arr) const;

	float GetXData(size_t index, bool& sign) const;
	float GetYData(size_t index, bool& sign) const;
	double GetStandardValue() const;
	double GetCenterPositionX(OCRect const& rect, double min, double max, double offset, bool reverse) const;
	double GetCenterPositionY(OCRect const& rect, double yMin, double yMax, double offset, bool reverse) const;

	bool IsReturned() const;

private:
	//@{Serializable
	COLORREF m_patternColor;		//!< パターン色
	int m_gradationDirection;		//!< グラデーション方向
	COLORREF m_fillMinusColor;		//!< 逆方向の塗りつぶし色
	COLORREF m_minusPatternColor;	//!< 逆方向のパターン色
	COLORREF m_minusLineColor;		//!< 逆方向の線色
	COLORREF m_stdColor;			//!< 基準線の色
	
	bool m_zeroCenter;
	double m_stdValue;				//!< 基準値
	bool m_abs;						//!< 折り返し
	bool m_sort;
	int/*BASE_LINE*/ m_baseStyle;	//!< 基準線
	double m_barWidth;				//!< バーの幅
	bool m_cube;					//!< 立体表示?
	int m_direction; //0:縦, 1:横
	short m_transparent;			//!< 透明度
	//@}

	int m_current;					//
	int m_barCount;					//
};
