/*!	@file
	@date	2008-03-06
	@author	wada

	@brief	棒グラフの実装
*/
#include "StdAfx.h"
#include <boost/foreach.hpp>
#include "ICanvas.h"
#include "IPlotData.h"
#include "property.h"
#include "propertycreator.hpp"
#include "ISerializer.h"
#include "Message.h"
#include "PluginUtility/pluginutil.h"
#include "BarPlot.h"
#include "AxisEnum.h"

//! Constructor
OCBarPlot::OCBarPlot(HWND hWnd, IMessageReceiver* owner)
	: OCPlotImpl(hWnd, owner)
	, m_fillMinusColor(RGB(255, 0 ,0)), m_patternColor(0), m_gradationDirection(0), m_minusPatternColor(0)
	, m_abs(false), m_zeroCenter(true)
	, m_baseStyle(BL_NONE), m_stdValue(0)
	, m_current(0), m_barCount(1)
	, m_barWidth(2), m_cube(false), m_direction(0)
	, m_stdColor(0), m_minusLineColor(0), m_transparent(0)
{
	m_fillStyle = FS_SOLID;
}

//! Destructor
OCBarPlot::~OCBarPlot()
{
}

void OCBarPlot::GetPosition(size_t index, double* x, double* y, OCRect const& rect, DrawArgs const& arg) const
{
	__super::GetPosition(index, x, y, rect, arg);

	double const barWidth = m_barWidth / 2.0; // バーの幅(1/2) mm
	double const delta = barWidth * (m_barCount - 1) - (barWidth * 2 * m_current);
	*x -= delta;
}

bool OCBarPlot::GetPointBySearchIndex(OCRect const& rect, int index, OCPoint* point, OCPoint3D* value)
{
	if (__super::GetPointBySearchIndex(rect, index, point, value)) {
		double const barWidth = m_barWidth / 2.0; // バーの幅(1/2) mm
		double const delta = barWidth * (m_barCount - 1) - (barWidth * 2 * m_current);
		point->x -= delta;

		return true;
	}
	return false;
}

//! X軸の最大/最小値を取得します
//! 横向きになっていたらX軸の最小値が0になります
void OCBarPlot::GetXMinMax(float* min, float* max) const
{
	if (m_direction == 1) {
		if (IsReturned()) {
			float tmp = std::abs(m_data->GetData(0, 0));
			for (size_t i = 1, size = m_data->GetDataCount(); i < size; ++i) {
				tmp = std::max<float>(tmp, std::abs(m_data->GetData(0, (int)i)));
			}
			*max = tmp;
			*min = 0;
		}
		else {
			__super::GetXMinMax(min, max);
			// 全部正数だったら min = 0 にする
			if (*min >= 0) {
				*min = 0;
			}
		}
	}
	else {
		__super::GetXMinMax(min, max);
	}
}

//! Y軸の最大/最小値を取得します
//! 棒グラフの場合縦軸の最小値は基本的にゼロになります
void OCBarPlot::GetYMinMax(float* min, float* max) const
{
	if (m_direction == 0) {
		if (IsReturned()) {
			float tmp = std::abs(m_data->GetData(1, 0));
			for (size_t i = 1, size = m_data->GetDataCount(); i < size; ++i) {
				tmp = std::max<float>(tmp, std::abs(m_data->GetData(1, (int)i)));
			}
			*max = tmp;
			*min = 0;
		}
		else {
			__super::GetYMinMax(min, max);
			// ログ以外の場合、全部正数だったら min = 0 にする
			int scaleType = m_data->GetScaleType(1);
			if (*min >= 0 && scaleType != ST_LOG) {
				*min = 0;
			}
		}
	}
	else {
		__super::GetYMinMax(min, max);
	}
}

//! 棒グラフを描画します
void OCBarPlot::Draw(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const
{
	DrawImpl(false, g, rect, arg);
}

void OCBarPlot::DrawSelected(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const
{
	if (m_selected) {
		DrawImpl(true, g, rect, arg);
	}
}

void OCBarPlot::DrawImpl(bool selected, ICanvas* g, OCRect const& rect, DrawArgs const& arg) const
{
	if (!IsVisible()) { return; }
	if (!m_data->GetDataCount()) { return; }

	// 各バーの座標を求める
	std::vector<std::pair<bool, OCRect>> rects; // <sign, barRect>
	GetBarRect(rect, arg, &rects);

	typedef std::pair<bool, OCRect> pair_t;
	// 立体表示の場合は影を描く
	if (m_cube) {
		double const offsetMM = 1;
		BOOST_FOREACH(pair_t const& val, rects) {
			OCRect const& r = val.second;
			g->SetBackground(RGB(192, 192, 192), FS_SOLID, 0);
			OCPoint pts1[] = {
				OCPoint(r.left, r.top), OCPoint(r.left + offsetMM, r.top - offsetMM),
				OCPoint(r.right + offsetMM, r.top - offsetMM), OCPoint(r.right, r.top)};
			g->FillPolygon(pts1, _countof(pts1));

			g->SetBackground(RGB(128, 128, 128), FS_SOLID, 0);
			OCPoint pts2[] = {
				OCPoint(r.right, r.top), OCPoint(r.right + offsetMM, r.top - offsetMM),
				OCPoint(r.right + offsetMM, r.bottom - offsetMM), OCPoint(r.right, r.bottom)};
			g->FillPolygon(pts2, _countof(pts2));
		}
	}

	// 棒グラフを描画(影の後に描かないと影の下に出てしまう)
	for (size_t i = 0, n = rects.size(); i < n; ++i) {
		pair_t const& r = rects[i];

		// 同じ色だったら変更しないとか、負数のみ描いてから正数のバーを描くとか
		g->SetPen(r.first ? m_lineColor : m_minusLineColor, 1, PS_SOLID, 1); // 線色以外は固定
		COLORREF const color = r.first ? m_fillColor : m_fillMinusColor;
		g->SetBackground(color, m_fillStyle, m_transparent);
		g->SetGradationStyle((GRADATION_TYPE)m_gradationDirection, color, r.first ? m_patternColor : m_minusPatternColor);

		if (selected) {
			g->DrawRubberBand(r.second);
		}
		else {
			g->FillRectangle(r.second);
			g->DrawRectangle(r.second);
		}
	}

	// 基準値のライン
	if (m_direction == 0 && m_baseStyle != BL_NONE && arg.yMin < GetStandardValue()) {
		double std = std::min<double>(std::max<double>(GetStandardValue(), arg.yMin), arg.yMax);
		double base = rect.Height() - rect.Height() * arg.YNormalize(std) + rect.top;
		g->SetPenColor(m_stdColor);
		g->DrawLine(rect.left, base, rect.right, base);
	}
	else if (m_direction == 1 && m_baseStyle != BL_NONE && arg.xMin < GetStandardValue()) {
		double std = std::min<double>(std::max<double>(GetStandardValue(), arg.xMin), arg.xMax);
		double base = rect.Width() * arg.XNormalize(std) + rect.left;
		g->SetPenColor(m_stdColor);
		g->DrawLine(base, rect.top, base, rect.bottom);
	}
}

//! 凡例を描画します
void OCBarPlot::DrawLegend(ICanvas* g, OCRect const& rect, int ratio) const
{
	if (!IsVisible()) { return; }

	g->SetPen(m_lineColor, m_lineWidth, m_lineStyle, 1);
	g->SetBackground(m_fillColor, m_fillStyle, /*m_transparent*/0);
	g->SetGradationStyle((GRADATION_TYPE)m_gradationDirection, m_fillColor, m_patternColor);

	double per = ratio / 100.0;
	double pp = (1.0 / 3.0 / 2.0);
	double xOffset = rect.Width() * pp;
	double yOffset = rect.Height() * (1 - per) / 2; 
	yOffset += rect.Height() * pp;
	if (yOffset * 2 > rect.Height()) {
		yOffset = rect.Height() / 2 - 0.15;		//0.15はバーの最小の高さ
	}

	g->FillRectangle(OCRect(rect.left + xOffset, rect.top + yOffset,
		rect.right - xOffset, rect.bottom - yOffset));
}

//! 各バーの座標を作成します
void OCBarPlot::GetBarRect(OCRect const& rect, DrawArgs const& arg, BarRectArray* arr) const
{
	arr->reserve(m_data->GetDataCount());

	double const xMax = arg.xMax;
	double const xMin = arg.xMin;
	double const yMax = arg.yMax;
	double const yMin = arg.yMin;

	double const barWidth = m_barWidth / 2.0; // バーの幅(1/2) mm
	double const delta = barWidth * (m_barCount - 1) - (barWidth * 2 * m_current);

	double sumValue = 0;
	for (size_t i = 0; i < m_data->GetDataCount(); ++i) {
		bool sign = true;
		double /*const*/ xv = GetXData(i, sign);
		double /*const*/ yv = GetYData(i, sign);
		if (xv == FLT_MAX || yv == FLT_MAX) { continue; }

		// クリッピング
		if ((m_clippingType == plot::AllClip || m_clippingType == plot::XClip) && (xv > xMax || xv < xMin)) {
			if (m_direction == 0) { continue; }
			xv = std::max<double>(std::min<double>(xv, xMax), xMin);
		}
		if ((m_clippingType == plot::AllClip || m_clippingType == plot::YClip) && (yv > yMax || yv < yMin)) {
			if (m_direction == 1) { continue; }
			yv = std::max<double>(std::min<double>(yv, yMax), yMin);
		}

		//// 基準が「なし」の時は最小値以下のバーは描画しない
		if (m_baseStyle == BL_NONE && yv <= yMin) {
			continue;
		}

		if (m_direction == 0) {
			// 縦
			double x = rect.Width() * arg.XNormalize(xv) + rect.left - delta;
			sumValue = m_accumulative ? sumValue + yv : yv;
			double y = rect.Height() - rect.Height() * arg.YNormalize(sumValue) + rect.top;

			double center = GetCenterPositionY(rect, yMin, yMax,
				m_data->UseReferenceData() ? (yv - m_data->GetData(1, (int)i)) : DBL_MAX, !!arg.yReverse);

			OCRect r = OCRect(x - barWidth, y, x + barWidth, center);
			r.Normalize();
			arr->push_back(std::make_pair(sign, r));
		}
		else {
			// 横
			sumValue = m_accumulative ? sumValue + xv : xv;
			double x = rect.Width() * arg.XNormalize(sumValue) + rect.left;
			double y = rect.Height() - rect.Height() * arg.YNormalize(yv) + rect.top - delta;

			double center = GetCenterPositionX(rect, xMin, xMax,
				m_data->UseReferenceData() ? (xv - m_data->GetData(0, (int)i)) : DBL_MAX, !!arg.yReverse);

			OCRect r = OCRect(center, y - barWidth, x, y + barWidth);
			r.Normalize();
			arr->push_back(std::make_pair(sign, r));
		}
	}
}

//! Xデータを取得します
float OCBarPlot::GetXData(size_t index, bool& sign) const
{
	double const std = GetStandardValue();

	float v = m_data->GetRealData(0, index);

	if (IsReturned()) {
		v = m_data->GetRealData(0, index);
		if (v < std) {
			v = std + std::abs(v);
		}
	}

	if (m_direction == 1) {
		sign = v >= std;
	}
	return v;
}

/*!	@brief	Y軸データを取得します
	@param[in]	index	取得したいデータのインデックス
	@param[in]	sign	向き(true:正, false:逆)
*/
float OCBarPlot::GetYData(size_t index, bool& sign) const
{
	double const std = GetStandardValue();

	float v = m_data->GetRealData(1, index);
	for (size_t i = 0; i < m_data->GetReferenceDataCount(); ++i) {
		float tmp = m_data->GetReferenceData(i)->GetRealData(1, (int)index);
		if (v > 0 == tmp> 0) { // 符号が同じであれば
			v += tmp;
		}
	}

	if (IsReturned()) {
		v = m_data->GetRealData(1, index);
		if (v < std) {
			v = std + std::abs(v);
		}
	}

	if (m_direction == 0) {
		sign = v >= std;
	}
	return v;
}

//! 基準値を取得します
double OCBarPlot::GetStandardValue() const
{
	switch (m_baseStyle)
	{
	case BL_STANDARD:
		return m_stdValue;
	case BL_MINIMUM:
		return m_data->GetMinimum(m_direction == 0 ? 1 : 0);
	case BL_MAXIMUM:
		return m_data->GetMaximum(m_direction == 0 ? 1 : 0);
	case BL_NONE:
		return 0;
	}
	return 0;
}

double GetTargetValue(double min, double max, double std, int baseStyle)
{
	if (baseStyle == BL_NONE) { return min; }

	double value = std::min<double>(std::max<double>(std, min), max);
	if (baseStyle == BL_MAXIMUM) {
		value = max;
	}
	else if (baseStyle == BL_MINIMUM) {
		value = min;
	}

	//if (offset != DBL_MAX && offset != 0) {
	//	yValue = offset;
	//}
	return value;
}

double OCBarPlot::GetCenterPositionX(OCRect const& rect, double min, double max, double offset, bool reverse) const
{
	double xValue = GetTargetValue(min, max, m_stdValue, m_baseStyle);
	double norm = reverse ? 1 - (xValue - min) / (max - min) : (xValue - min) / (max - min);
	return rect.Width() * norm + rect.left;
}

double OCBarPlot::GetCenterPositionY(OCRect const& rect, double yMin, double yMax, double offset, bool reverse) const
{
	double yValue = GetTargetValue(yMin, yMax, m_stdValue, m_baseStyle);
	double norm = reverse ? 1 - (yValue - yMin) / (yMax - yMin) : (yValue - yMin) / (yMax - yMin);
	return rect.Height() - rect.Height() * norm + rect.top;
}

//! バーを折り返すかどうかを取得します
bool OCBarPlot::IsReturned() const
{
	if (m_baseStyle == BL_STANDARD) {
		if (m_data->GetMaximum(1) <= m_stdValue) {
			return false;
		}
	}

	return m_abs && m_baseStyle != BL_NONE && m_baseStyle != BL_MAXIMUM;
}

//! 当たり判定
bool OCBarPlot::HitTest(OCRect const& rect, DrawArgs const& arg, double x, double y)
{
	if (!IsVisible()) { return false; }

	BarRectArray rects;
	GetBarRect(rect, arg, &rects);

	for (size_t i = 0, n = rects.size(); i < n; ++i) {
		if (rects[i].second.Contains(x, y)) { return true; }
	}
	return false;
}

//! 棒グラフのプロパティを追加します
void OCBarPlot::AddProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode)
{
	{
		IPropertyGroup* group = prop->CreateGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_BAR"), PI_BAR_VISIBLE, m_visible, !plotMode);
		/*if (m_visible)*/
		{
			bool readOnly = !plotMode || !m_visible;

			std::wstring styleProp1 = GetResourceString(m_plot, "ID_SOURCE_TEXT_SOLID");
			std::wstring styleProp2 = GetResourceString(m_plot, "ID_SOURCE_TEXT_NONE_KANJI");
			std::wstring styleProp3 = GetResourceString(m_plot, "ID_SOURCE_TEXT_HORIZONTAL_HATCH");
			std::wstring styleProp4 = GetResourceString(m_plot, "ID_SOURCE_TEXT_VERTICAL_HATCH");
			std::wstring styleProp5 = GetResourceString(m_plot, "ID_SOURCE_TEXT_45_DEGREES_DOWNWARD_HATCH");
			std::wstring styleProp6 = GetResourceString(m_plot, "ID_SOURCE_TEXT_45_DEGREES_UPWARD_HATCH");
			std::wstring styleProp7 = GetResourceString(m_plot, "ID_SOURCE_TEXT_HORIZONTAL_AND_VERTICAL_HATCH");
			std::wstring styleProp8 = GetResourceString(m_plot, "ID_SOURCE_TEXT_45_DEGREES_CROSSHATCH");
			std::wstring styleProp9 = GetResourceString(m_plot, "ID_SOURCE_TEXT_GRADATION");
			group->AddArrayProperty(::GetResourceString(m_plot, "ID_SOURCE_TEXT_STYLE"), PI_BAR_FILLSTYLE, m_fillStyle, readOnly, 9, 
				styleProp1.c_str(), styleProp2.c_str(), styleProp3.c_str(), styleProp4.c_str(), styleProp5.c_str(), styleProp6.c_str(), styleProp7.c_str(), styleProp8.c_str(), styleProp9.c_str());

			std::wstring directProp1 = GetResourceString(m_plot, "ID_SOURCE_TEXT_RIGHT");
			std::wstring directProp2 = GetResourceString(m_plot, "ID_SOURCE_TEXT_RIGHT_DIAGONAL_UNDER");
			std::wstring directProp3 = GetResourceString(m_plot, "ID_SOURCE_TEXT_ON_RIGHT_DIAGONAL");
			std::wstring directProp4 = GetResourceString(m_plot, "ID_SOURCE_TEXT_LEFT");
			std::wstring directProp5 = GetResourceString(m_plot, "ID_SOURCE_TEXT_LEFT_DIAGONAL_UNDER");
			std::wstring directProp6 = GetResourceString(m_plot, "ID_SOURCE_TEXT_ON_LEFT_DIAGONAL");
			std::wstring directProp7 = GetResourceString(m_plot, "ID_SOURCE_TEXT_BOTTOM");
			std::wstring directProp8 = GetResourceString(m_plot, "ID_SOURCE_TEXT_TOP");
			group->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_GRADATION_DIRECTION"), PI_BAR_GRADATION_DIRECTION, m_gradationDirection, readOnly || m_fillStyle != FS_GRADATION,
				8, directProp1.c_str(), directProp2.c_str(), directProp3.c_str(), directProp4.c_str(), directProp5.c_str(), directProp6.c_str(), directProp7.c_str(), directProp8.c_str());

			group->AddTransparentProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_TRANSPARENCY"), PI_BAR_TRANSPARENT, m_transparent);
			{
				IPropertyGroup* plusGr = group->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_POSITIVE_DIRECTION"));
				plusGr->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_FRAME_LINE"), PI_BAR_LINECOLOR, m_lineColor, readOnly);
				plusGr->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_PAINT_FILL_COLOR"), PI_BAR_FILLCOLOR, m_fillColor, readOnly || m_fillStyle == FS_NONE);
				plusGr->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_PATTERN_COLOR"), PI_BAR_PATTERNCOLOR, m_patternColor, readOnly || m_fillStyle != FS_GRADATION);
			}
			bool const disasbleStd = m_baseStyle == BL_NONE;
			{
				IPropertyGroup* baseGr = group->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_BASE_LINE"));

				std::wstring standardsProp1 = GetResourceString(m_plot, "ID_SOURCE_TEXT_STANDARD_VALUE");
				std::wstring standardsProp2 = GetResourceString(m_plot, "ID_SOURCE_TEXT_MIN");
				std::wstring standardsProp3 = GetResourceString(m_plot, "ID_SOURCE_TEXT_MAX");
				std::wstring standardsProp4 = GetResourceString(m_plot, "ID_SOURCE_TEXT_NONE");
				baseGr->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_STANDARDS"), PI_BAR_BASE_LINE, m_baseStyle, readOnly, 4, 
					standardsProp1.c_str(), standardsProp2.c_str(), standardsProp3.c_str(), standardsProp4.c_str());

				baseGr->AddProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_STANDARD_VALUE"), PI_BAR_STANDARD_VALUE, m_stdValue, readOnly || disasbleStd);
				baseGr->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_LINE"), PI_BAR_STANDARD_COLOR, m_stdColor, readOnly || disasbleStd);
			}
			{
				IPropertyGroup* minusGr = group->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_OPPOSITE_DIRECTION"));
				minusGr->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_FRAME_LINE"), PI_BAR_MINUS_LINECOLOR, m_minusLineColor, readOnly || disasbleStd);
				minusGr->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_PAINT_FILL_COLOR"), PI_BAR_MINUS_COLOR, m_fillMinusColor, readOnly || disasbleStd || m_fillStyle == FS_NONE);
				minusGr->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_PATTERN_COLOR"), PI_BAR_MINUSPATTERNCOLOR, m_minusPatternColor, readOnly || disasbleStd || m_fillStyle != FS_GRADATION);
			}
			group->AddBoolProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_LAPEL"), PI_BAR_ABS, m_abs, readOnly || m_baseStyle != BL_STANDARD);		//旧絶対値
			group->AddSelectorPropertyR(::GetResourceString(m_plot,"ID_SOURCE_TEXT_WIDTH_MM"), PI_BAR_WIDTH, readOnly, m_barWidth, 5, 0.25, 1.0, 2.0, 3.0, 5.0);
			group->AddBoolProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_STEREOSCOPIC_DISPLAY"), PI_BAR_CUBE, m_cube, readOnly);

			std::wstring sideProp1 = GetResourceString(m_plot, "ID_SOURCE_TEXT_VERTICAL");
			std::wstring sideProp2 = GetResourceString(m_plot, "ID_SOURCE_TEXT_SIDE");
			group->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_DIRECTION"), PI_BAR_DIRECTION, m_direction, readOnly, 2, 
				sideProp1.c_str(), sideProp2.c_str());
		}
	}
}

//! 棒グラフのプロパティを追加します
void OCBarPlot::AddTabProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode)
{
	{
		IPropertyGroup* group = prop->CreateGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_BAR"));
		IPropertyGroup* parent=group->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_BAR"), PI_BAR_VISIBLE, m_visible, !plotMode);
		/*if (m_visible)*/
		{
			bool readOnly = !plotMode || !m_visible;

			std::wstring styleProp1 = GetResourceString(m_plot, "ID_SOURCE_TEXT_SOLID");
			std::wstring styleProp2 = GetResourceString(m_plot, "ID_SOURCE_TEXT_NONE_KANJI");
			std::wstring styleProp3 = GetResourceString(m_plot, "ID_SOURCE_TEXT_HORIZONTAL_HATCH");
			std::wstring styleProp4 = GetResourceString(m_plot, "ID_SOURCE_TEXT_VERTICAL_HATCH");
			std::wstring styleProp5 = GetResourceString(m_plot, "ID_SOURCE_TEXT_45_DEGREES_DOWNWARD_HATCH");
			std::wstring styleProp6 = GetResourceString(m_plot, "ID_SOURCE_TEXT_45_DEGREES_UPWARD_HATCH");
			std::wstring styleProp7 = GetResourceString(m_plot, "ID_SOURCE_TEXT_HORIZONTAL_AND_VERTICAL_HATCH");
			std::wstring styleProp8 = GetResourceString(m_plot, "ID_SOURCE_TEXT_45_DEGREES_CROSSHATCH");
			std::wstring styleProp9 = GetResourceString(m_plot, "ID_SOURCE_TEXT_GRADATION");
			parent->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_STYLE"), PI_BAR_FILLSTYLE, m_fillStyle, readOnly, 9, 
				styleProp1.c_str(), styleProp2.c_str(), styleProp3.c_str(), styleProp4.c_str(), styleProp5.c_str(), styleProp6.c_str(), styleProp7.c_str(), styleProp8.c_str(), styleProp9.c_str());

			std::wstring directProp1 = GetResourceString(m_plot, "ID_SOURCE_TEXT_RIGHT");
			std::wstring directProp2 = GetResourceString(m_plot, "ID_SOURCE_TEXT_RIGHT_DIAGONAL_UNDER");
			std::wstring directProp3 = GetResourceString(m_plot, "ID_SOURCE_TEXT_ON_RIGHT_DIAGONAL");
			std::wstring directProp4 = GetResourceString(m_plot, "ID_SOURCE_TEXT_LEFT");
			std::wstring directProp5 = GetResourceString(m_plot, "ID_SOURCE_TEXT_LEFT_DIAGONAL_UNDER");
			std::wstring directProp6 = GetResourceString(m_plot, "ID_SOURCE_TEXT_ON_LEFT_DIAGONAL");
			std::wstring directProp7 = GetResourceString(m_plot, "ID_SOURCE_TEXT_BOTTOM");
			std::wstring directProp8 = GetResourceString(m_plot, "ID_SOURCE_TEXT_TOP");
			parent->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_GRADATION_DIRECTION"), PI_BAR_GRADATION_DIRECTION, m_gradationDirection, readOnly || m_fillStyle != FS_GRADATION,
				8, directProp1.c_str(), directProp2.c_str(), directProp3.c_str(), directProp4.c_str(), directProp5.c_str(), directProp6.c_str(), directProp7.c_str(), directProp8.c_str());

			parent->AddTransparentProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_TRANSPARENCY"), PI_BAR_TRANSPARENT, m_transparent);
			{
				IPropertyGroup* plusGr = parent->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_POSITIVE_DIRECTION"));
				plusGr->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_FRAME_LINE"), PI_BAR_LINECOLOR, m_lineColor, readOnly);
				plusGr->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_PAINT_FILL_COLOR"), PI_BAR_FILLCOLOR, m_fillColor, readOnly || m_fillStyle == FS_NONE);
				plusGr->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_PATTERN_COLOR"), PI_BAR_PATTERNCOLOR, m_patternColor, readOnly || m_fillStyle != FS_GRADATION);
			}
			bool const disasbleStd = m_baseStyle == BL_NONE;
			{
				IPropertyGroup* baseGr = parent->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_BASE_LINE"));

				std::wstring standardsProp1 = GetResourceString(m_plot, "ID_SOURCE_TEXT_STANDARD_VALUE");
				std::wstring standardsProp2 = GetResourceString(m_plot, "ID_SOURCE_TEXT_MIN");
				std::wstring standardsProp3 = GetResourceString(m_plot, "ID_SOURCE_TEXT_MAX");
				std::wstring standardsProp4 = GetResourceString(m_plot, "ID_SOURCE_TEXT_NONE");
				baseGr->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_STANDARDS"), PI_BAR_BASE_LINE, m_baseStyle, readOnly, 4, 
					standardsProp1.c_str(), standardsProp2.c_str(), standardsProp3.c_str(), standardsProp4.c_str());

				baseGr->AddProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_STANDARD_VALUE"), PI_BAR_STANDARD_VALUE, m_stdValue, readOnly || disasbleStd);
				baseGr->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_LINE"), PI_BAR_STANDARD_COLOR, m_stdColor, readOnly || disasbleStd);
			}
			{
				IPropertyGroup* minusGr = parent->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_OPPOSITE_DIRECTION"));
				minusGr->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_FRAME_LINE"), PI_BAR_MINUS_LINECOLOR, m_minusLineColor, readOnly || disasbleStd);
				minusGr->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_PAINT_FILL_COLOR"), PI_BAR_MINUS_COLOR, m_fillMinusColor, readOnly || disasbleStd || m_fillStyle == FS_NONE);
				minusGr->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_PATTERN_COLOR"), PI_BAR_MINUSPATTERNCOLOR, m_minusPatternColor, readOnly || disasbleStd || m_fillStyle != FS_GRADATION);
			}
			parent->AddBoolProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_LAPEL"), PI_BAR_ABS, m_abs, readOnly || m_baseStyle != BL_STANDARD);		//旧絶対値
			parent->AddSelectorPropertyR(::GetResourceString(m_plot,"ID_SOURCE_TEXT_WIDTH_MM"), PI_BAR_WIDTH, readOnly, m_barWidth, 5, 0.25, 1.0, 2.0, 3.0, 5.0);
			parent->AddBoolProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_STEREOSCOPIC_DISPLAY"), PI_BAR_CUBE, m_cube, readOnly);

			std::wstring sideProp1 = GetResourceString(m_plot, "ID_SOURCE_TEXT_VERTICAL");
			std::wstring sideProp2 = GetResourceString(m_plot, "ID_SOURCE_TEXT_SIDE");
			parent->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_DIRECTION"), PI_BAR_DIRECTION, m_direction, readOnly, 2, 
				sideProp1.c_str(), sideProp2.c_str());
		}
	}
}

bool OCBarPlot::SetProperty(long id, LPARAM value, LPARAM* source)
{
	switch (id)
	{
	case PI_BAR_VISIBLE:
		::SetPropertyHelper(m_visible, value, source);
		return true;
	case PI_BAR_FILLSTYLE:
		::SetPropertyHelper(m_fillStyle, value, source);
		return true;
	case PI_BAR_FILLCOLOR:
		::SetPropertyHelper(m_fillColor, value, source);
		return true;
	case PI_BAR_LINECOLOR:
		::SetPropertyHelper(m_lineColor, value, source);
		return true;
	case PI_BAR_MINUS_LINECOLOR:
		::SetPropertyHelper(m_minusLineColor, value, source);
		return true;
	case PI_BAR_MINUS_COLOR:
		::SetPropertyHelper(m_fillMinusColor, value, source);
		return true;
	case PI_BAR_PATTERNCOLOR:
		::SetPropertyHelper(m_patternColor, value, source);
		return true;
	case PI_BAR_GRADATION_DIRECTION:
		::SetPropertyHelper(m_gradationDirection, value, source);
		return true;
	case PI_BAR_MINUSPATTERNCOLOR:
		::SetPropertyHelper(m_minusPatternColor, value, source);
		return true;
	case PI_BAR_ABS:
		::SetPropertyHelper(m_abs, value, source);
		::SendMessage(m_owner, PI_MSG_UPDATE_GRAPH_FROM_PLOT, 0, reinterpret_cast<LPARAM>(this));
		return true;
	case PI_BAR_BASE_LINE:
		::SetPropertyHelper(m_baseStyle, value, source);
		return true;
	case PI_BAR_STANDARD_VALUE:
		::SetPropertyHelper(m_stdValue, value, source);
		return true;
	case PI_BAR_STANDARD_COLOR:
		::SetPropertyHelper(m_stdColor, value, source);
		return true;
	case PI_BAR_ZERO_CENTER:
		if (source) { *source = m_zeroCenter ? 1 : 0; }
		m_zeroCenter = value != 0;
		::SendMessage(m_owner, PI_MSG_UPDATE_GRAPH_FROM_PLOT, 0, reinterpret_cast<LPARAM>(this));
		return true;
	case PI_PLOT_SORT:
		if (source) { *source = m_sort ? 1 : 0; }
		m_sort = value != 0;
		return true;
	case PI_BAR_WIDTH:
		{
			double cur = m_barWidth;
			::SetPropertyHelper(cur, value, source);
			if (cur < 0.25 || IsZero(cur)) {
				std::wstring msg = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_INPUT_MORE_0.25_INTO_STICK_WIDTH");
				::MessageBox(m_owner, msg.c_str(), ::GetResourceString(m_plot,"ID_SOURCE_TEXT_O_CHART"), MB_OK|MB_ICONWARNING);
				return false;
			}
			else if (cur > 50) {
				std::wstring msg = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_INPUT_LESS_50_INTO_STICK_WIDTH");
				::MessageBox(m_owner, msg.c_str(), ::GetResourceString(m_plot,"ID_SOURCE_TEXT_O_CHART"), MB_OK|MB_ICONWARNING);
				return false;
			}
			m_barWidth = cur;
		}
		return true;
	case PI_BAR_CUBE:
		::SetPropertyHelper(m_cube, value, source);
		return true;
	case PI_BAR_DIRECTION:
		::SetPropertyHelper(m_direction, value, source);
		return true;
	case PI_BAR_TRANSPARENT:
		::SetPropertyHelper(m_transparent, value, source);
		return true;
		
	case PI_BAR_NUMBER:
		::SetPropertyHelper(m_current, value, source);
		return true;
	case PI_BAR_ALLCOUNT:
		::SetPropertyHelper(m_barCount, value, source);
		return true;
	}

	return __super::SetProperty(id, value, source);
}

void OCBarPlot::CopyFrom(IPlotDrawer const* src)
{
	__super::CopyFrom(src);
	if (OCBarPlot const* self = dynamic_cast<OCBarPlot const*>(src)) {
		m_fillMinusColor = self->m_fillMinusColor;
		m_patternColor = self->m_patternColor;
		m_gradationDirection = self->m_gradationDirection;
		m_zeroCenter = self->m_zeroCenter;
		m_stdValue = self->m_stdValue;
		m_abs = self->m_abs;
		m_sort = self->m_sort;
		m_baseStyle = self->m_baseStyle;
		m_barWidth = self->m_barWidth;
		m_cube = self->m_cube;
		m_direction = self->m_direction;
		m_stdColor = self->m_stdColor;
		m_minusLineColor = self->m_minusLineColor;
		m_transparent = self->m_transparent;
	}
}

//! プロットの情報をXML形式で出力します
void OCBarPlot::Serialize(ISerializer* ar)
{
	__super::Serialize(ar);

    // {Generated Code(by gen.rb)
    ar->Serialize(_T("FillMinusColor"), m_fillMinusColor, COLORREF());
    ar->Serialize(_T("PatternColor"), m_patternColor, COLORREF());
    ar->Serialize(_T("GradationDirection"), m_gradationDirection, int());
    ar->Serialize(_T("MinusPatternColor"), m_minusPatternColor, COLORREF());
    ar->Serialize(_T("ZeroCenter"), m_zeroCenter, bool());
    ar->Serialize(_T("StdValue"), m_stdValue, double());
    ar->Serialize(_T("Abs"), m_abs, bool());
    ar->Serialize(_T("Sort"), m_sort, bool());
    ar->Serialize(_T("BaseStyle"), m_baseStyle, int/*BASE_LINE*/());
    ar->Serialize(_T("BarWidth"), m_barWidth, double());
    ar->Serialize(_T("Cube"), m_cube, bool());
    ar->Serialize(_T("Direction"), m_direction, int());
	ar->Serialize(_T("StdColor"), m_stdColor, COLORREF());
	ar->Serialize(_T("MinusLineColor"), m_minusLineColor, COLORREF());
	ar->Serialize(_T("Transparent"), m_transparent);
    // }

	if (IReadable* reader = dynamic_cast<IReadable*>(ar)) {
		if (reader->GetMajorVersion() == 2 ||
			reader->GetMajorVersion() == 3 && reader->GetMinorVersion() == 0 && reader->GetReleaseVersion() < 29)
		{
			m_baseStyle = BL_MINIMUM;
		}
	}

	// いらないかも(数え直しているので)
	ar->Serialize(_T("Current"), m_current);
	ar->Serialize(_T("BarCount"), m_barCount, 1);
}
