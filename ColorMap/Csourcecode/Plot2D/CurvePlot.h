/*!	@file
	@date	2008-03-04
	@author	wada

	@brief	曲線図
*/
#pragma once

#include <boost/shared_ptr.hpp>
#include "PlotLib/PlotImpl.h"
#include "IOnlineSearchable.h"

class OCInterpolate;
class OCInterpolateFactory;
class OCRegressionCalc;

class OCCurvePlot : public OCPlotImpl, public IOnlineSearchable
{
public:
	OCCurvePlot(HWND hWnd, IMessageReceiver* plot);
	virtual ~OCCurvePlot();

	virtual void Destroy() { delete this; }

	virtual void Draw(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const;
	virtual void DrawSelected(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const;

	virtual bool HitTest(OCRect const& rect, DrawArgs const& arg, double x, double y);

	virtual void AddProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode);
	virtual void AddTabProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode);
	virtual bool SetProperty(long id, LPARAM value, LPARAM* source);
	virtual bool GetProperty(long id, LPARAM* ret) const;

	virtual void CopyFrom(IPlotDrawer const* src);
	virtual void Serialize(ISerializer* ar);

// Overrides OnlineSearchable
	virtual void EnableOnlineMode(bool enable);
	virtual bool GetNearPosition(OCPoint* p, OCPoint* value, OCRect const& rect, DrawArgs const& arg, bool next) const;
	virtual void GetSearchPointFromValue(OCPoint* point, OCPoint* value, OCRect const& rect) const;
	virtual void GetPointByValue(OCRect const& rect, OCPoint const& target, OCPoint* point, OCPoint3D* value);

// Overrides ISearchable
	virtual void BeginSearch(DrawArgs const& arg, bool interpolate, ExclusionScale exclude, OCRect const& rect);

private:
	void DrawImpl(bool selected, ICanvas* g, OCRect const& rect, DrawArgs const& arg) const;
	void GetDrawPoints(OCRect const& rect, DrawArgs const& arg, std::vector<OCPoint>* arr) const;
	void GetDrawRange(DrawArgs const& arg, double* start, double* stop, double sourceBegin, double sourceEnd) const;

	bool InitInterpolate(OCInterpolate* inter);
	bool InitInterpolate(OCInterpolate* inter, std::vector<double>& xTmp, std::vector<double>& yTmp);

	void SetInterpolationType(int type, bool validate);
	void CheckRegress(OCRegressionCalc* regress, bool dispMessage = true);
	void CheckRegress(OCRegressionCalc* regress, size_t dataCount, bool dispMessage);

private:
	boost::shared_ptr<OCInterpolateFactory> m_factory;
	boost::shared_ptr<OCInterpolate> m_inter;

	//@{Serializable
	int m_interType;
	// Regression
	int m_order;
	int m_extrapolation; // 0:None, 1:前方, 2:後方, 3:両方
	int m_regType;
	bool m_showFormula;

	// MovingAverage
	int m_range;
	//@}

	bool m_online;
	double m_max, m_min;

	bool m_dispMessage;	//!< メッセージボックスを出すかどうか

	std::vector<double> m_xTemp, m_yTemp;
};