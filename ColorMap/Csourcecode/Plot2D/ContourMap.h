/*!	@file
	@date	2008-04-21
	@author	wada

	@brief	コンターマップ
*/
#pragma once

#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/scoped_ptr.hpp>
#include <atlimage.h>
#include "PlotLib/PlotImpl3D.h"
#include "Contour/CalcClipping.h" // enum
#include "ClipPoint.h"
#include "EnumDefine.h"

class IMapImage;
class OCSliceLevel;
class OCColorPattern;
class OCScatter;

class OCContourMap : public OCPlotImpl3D
{
public:
	OCContourMap(HWND wnd, IMessageReceiver* plot);
	~OCContourMap();

	virtual void Destroy() { delete this; }

	virtual void Visible(BOOL visible);
	virtual void SetData(IPlotData* data);

	virtual bool HitTest(OCRect const& rect, DrawArgs const& arg, double x, double y);

	virtual void Draw(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const;
	virtual void DrawSelected(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const;
	virtual void DrawLegend(ICanvas* g, OCRect const& rect, int ratio) const;
	virtual void Update(bool silent);
	virtual void AfterSerializeUpdate();

	virtual void AddProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode);
	virtual void AddTabProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode);
	virtual bool SetProperty(long id, LPARAM value, LPARAM* source);
	virtual bool GetProperty(long id, LPARAM* ret) const;

	virtual void CopyFrom(IPlotDrawer const* src);

	virtual void Serialize(ISerializer* ar);
	virtual void AfterSerialize();

	// event
	void AddSliceLevel(OCSliceLevel* slice);
	void RemoveSliceLevel(OCSliceLevel* slice);

private:
	void Clipping(ICanvas* g, OCRect const& rect, std::vector<std::vector<OCPoint>>* pts) const;
	void DrawImpl(bool selected, ICanvas* g, OCRect const& rect, DrawArgs const& arg) const;

	void Calculate(bool displayProgressbar = true, bool recreateClipLine = true);
	void Gridding(OCRect const& rect) const;
	void Triangulation(OCRect const& rect);

	void SetCliplineSetting(ClippingSetting const& setting);
	void UpdateCliplineSetting();

	void SendColorBarVisible();

	void RecreateImage();

private:
	boost::shared_ptr<IMapImage> m_image;	//!< 等高線描画クラス
	boost::scoped_ptr<OCScatter> m_marker;	//!< マーカー
    OCColorPattern* m_pattern;				//!< カラーパターン
	boost::scoped_ptr<OCCalcClipping> m_clipLine;
	ClippingSetting m_clipSetting;

	int m_clipLineStyle;
	float m_clipLineWidth;
	COLORREF m_clipLineColor;
	int m_clipLineRoundType;

	int m_mapType;
	ContourClippingType m_clipType;
	ContourClippingLineType m_clipLineType; // 0:直線、1:曲線
	ContourFilling::FillType m_fillType;
	Contour::SliceType m_sliceType; // 指定色, レベルカラー, なし
	short m_transparent;
};
