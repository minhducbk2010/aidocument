/*!	@file
	@berief	カラー散布図
*/
#include "StdAfx.h"
#include "ICanvas.h"
#include "IPlotData.h"
#include "property.h"
#include "PropertyCreator.hpp"
#include "ISerializer.h"
#include "PluginUtility/pluginutil.h"
#include "PlotLib/ColorPattern.h"
#include "Scatter3D.h"
#include "DrawComboBoxMarker.h"

//! Constructor
OCScatter3D::OCScatter3D(HWND hWnd, IMessageReceiver* plot)
	: OCPlotImpl3D(hWnd, plot)
	, m_type(Marker::Dot), m_size(2), m_transparent(100)
    , m_pattern(0)
{

}

//! Destructor
OCScatter3D::~OCScatter3D()
{
}

//! カラー散布図を描画します
void OCScatter3D::Draw(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const
{
	if (!IsVisible()) { return; }
	if (!m_data->GetDataCount()) { return; }

    if (!m_pattern) { return; }

	g->SetPenStyle(PS_SOLID);
	g->SetPenWidth(m_lineWidth);

	double xMax = arg.xMax;
	double xMin = arg.xMin;
	double yMax = arg.yMax;
	double yMin = arg.yMin;

	double zMax = m_data->GetMaximum(2);
	double zMin = m_data->GetMinimum(2);

	m_pattern->SetMinMax(zMin, zMax);

	// m_size はポイント単位なのでmmに変換する
	double const per = 1;//0.352778;
	double const size = m_type == Marker::Dot ? PxToMM(2) : (m_size / 2) * per;

	for (size_t i = 0, count = 0; i < m_data->GetDataCount(); ++i, ++count) {
		float const xv = m_data->GetRealData(0, (int)i);//xData[i];
		float const yv = m_data->GetData(1, (int)i);
		float const zv = m_data->GetData(2, (int)i);
		
		if (xv == FLT_MAX || yv == FLT_MAX || zv == FLT_MAX) { continue; }

		if ((m_clippingType == plot::AllClip || m_clippingType == plot::XClip) && (xv > xMax || xv < xMin)) {
			continue;
		}
		if ((m_clippingType == plot::AllClip || m_clippingType == plot::YClip) && (yv > yMax || yv < yMin)) {
			continue;
		}

		COLORREF const color = m_pattern->GetColor(zv);
		g->SetBackground(color, FS_SOLID, m_type != Marker::Dot ? m_transparent : 0);
		g->SetPenColor(color);

		double x = rect.Width() * (xv - xMin) / (xMax - xMin) + rect.left;
		double y = rect.Height() - rect.Height() * (yv - yMin) / (yMax - yMin) + rect.top;
		Marker::DrawMarker(g, m_type, x, y, size);
	}
}

void OCScatter3D::DrawSelected(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const
{
}

//! 凡例を描画します
void OCScatter3D::DrawLegend(ICanvas* g, OCRect const& rect, int ratio) const
{
	if (!IsVisible()) { return; }

	g->SetPen(m_lineColor, m_lineWidth, PS_SOLID, 1);
	g->SetBackground(m_fillColor, FS_SOLID, m_transparent);

	double const per = ratio / 100.0;
	double const size = m_type == Marker::Dot ? 0.4 : m_size / 2 * per;
	OCPoint center(rect.left + rect.Width() / 2, rect.top + rect.Height() / 2);
	Marker::DrawMarker(g, m_type, center.x, center.y, size);
}

bool OCScatter3D::HitTest(OCRect const& rect, DrawArgs const& arg, double x, double y)
{
	if (!IsVisible()) { return false; }

	double const xMax = arg.xMax, xMin = arg.xMin, yMax = arg.yMax, yMin = arg.yMin;
	double const size = m_type == Marker::Dot ? 0.8 : (m_size / 2);

	double yValue = 0;
	for (size_t i = 0, count = 0; i < m_data->GetDataCount(); ++i, ++count) {
		float const xv = m_data->GetRealData(0, (int)i);
		float const yv = m_data->GetData(1, (int)i);

		if (xv == FLT_MAX || yv == FLT_MAX) { continue; }
		if ((m_clippingType == plot::AllClip || m_clippingType == plot::XClip) && (xv > xMax || xv < xMin)) {
			continue;
		}
		if ((m_clippingType == plot::AllClip || m_clippingType == plot::YClip) && (yv > yMax || yv < yMin)) {
			continue;
		}

		yValue = m_accumulative ? yValue + yv : yv;
		double px = rect.Width() * arg.XNormalize(xv) + rect.left;
		double py = rect.Height() - rect.Height() * arg.YNormalize(yValue) + rect.top;

		if (Marker::HitTest(m_type, px, py, size, OCPoint(x, y))) {
			return true;
		}
	}
	return false;
}

//! プロパティを作成します
void OCScatter3D::AddProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode)
{
	{
		IPropertyGroup* group = prop->CreateGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_SCATTER_DIAGRAM"), PI_SCATTER3D_VISIBLE, m_visible);

		/*if (m_visible)*/
		{
			bool const readOnly = !m_visible || !plotMode;
			bool const dotSelect = m_type == Marker::Dot;
			bool const noFill = m_type == Marker::Cross || m_type == Marker::Plus;

			//group->AddArrayProperty(GetResourceString(m_plot, "ID_SOURCE_TEXT_TYPE"), PI_SCATTER3D_TYPE, m_type, !m_visible, 9,
			//	/*_T("なし"), */_T("・"), _T("□"), _T("◇"), _T("▽"), _T("△") ,_T("○"), _T("×"), _T("＋"), _T("◎"));
			group->AddDrawComboProperty(GetResourceString(m_plot, "ID_SOURCE_TEXT_TYPE"), PI_SCATTER3D_TYPE, m_type, 9, DrawComboMarker);

			/*if (m_type != Marker::Dot)*/ {
				group->AddSelectorPropertyR(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SIZE_MM"), PI_SCATTER3D_SIZE, !m_visible || m_type == Marker::Dot, 
					m_size, 9, 0.1, 0.5, 1.0, 1.5, 2.0, 3.0, 4.0, 5.0, 10.0);
			}
			group->AddTransparentProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_TRANSPARENCY_PAR"), PI_SCATTER3D_TRANSPARENT, m_transparent, readOnly || dotSelect || noFill);
		}
	}
}

//! プロパティを作成します
void OCScatter3D::AddTabProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode)
{
	{
		IPropertyGroup* group = prop->CreateGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_SCATTER_DIAGRAM"));
		IPropertyGroup* parent;
		/*if (m_visible)*/
		{
			bool const readOnly = !m_visible || !plotMode;
			bool const dotSelect = m_type == Marker::Dot;
			bool const noFill = m_type == Marker::Cross || m_type == Marker::Plus;
			parent = group->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_SCATTER_DIAGRAM"), PI_SCATTER3D_VISIBLE, m_visible);
			//parent->AddArrayProperty(GetResourceString(m_plot, "ID_SOURCE_TEXT_TYPE"), PI_SCATTER3D_TYPE, m_type, !m_visible, 9,
			//	/*_T("なし"), */_T("・"), _T("□"), _T("◇"), _T("▽"), _T("△") ,_T("○"), _T("×"), _T("＋"), _T("◎"));
			parent->AddDrawComboProperty(GetResourceString(m_plot, "ID_SOURCE_TEXT_TYPE"), PI_SCATTER3D_TYPE, m_type, 9, DrawComboMarker);

			/*if (m_type != Marker::Dot)*/ {
				parent->AddSelectorPropertyR(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SIZE_MM"), PI_SCATTER3D_SIZE, !m_visible || m_type == Marker::Dot, 
					m_size, 9, 0.1, 0.5, 1.0, 1.5, 2.0, 3.0, 4.0, 5.0, 10.0);
			}
			parent->AddTransparentProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_TRANSPARENCY_PAR"), PI_SCATTER3D_TRANSPARENT, m_transparent, readOnly || dotSelect || noFill);
		}
	}
}

//! プロパティを更新します
/*!
	OCScatterクラスと使用している定数が異なる点に注意
*/
bool OCScatter3D::SetProperty(long id, LPARAM value, LPARAM* source)
{
	switch (id)
	{
	case PI_SCATTER3D_VISIBLE:
		::SetPropertyHelper(m_visible, value, source);
		return true;
	case PI_SCATTER3D_TYPE:
		::SetPropertyHelper(m_type, value, source);
		return true;
	case PI_SCATTER3D_SIZE:
		::SetPropertyHelper(m_size, value, source);
		return true;
	case PI_SCATTER3D_TRANSPARENT:
		::SetPropertyHelper(m_transparent, value, source);
		return true;
    case PI_COLOR_PATTERN:
        m_pattern = reinterpret_cast<OCColorPattern*>(value);
        return true;
	}

	return __super::SetProperty(id, value, source);
}

//! idに対応するプロパティの現在の値を取得します
bool OCScatter3D::GetProperty(long id, LPARAM* ret) const
{
	switch (id)
	{
	case PI_SCATTER_SIZE:
		*ret = reinterpret_cast<LPARAM>(&m_size);
		return true;
	}
	return false;
}

void OCScatter3D::CopyFrom(IPlotDrawer const* src)
{
	__super::CopyFrom(src);
	if (OCScatter3D const* self = dynamic_cast<OCScatter3D const*>(src)) {
		m_type = self->m_type;
		m_size = self->m_size;
		m_transparent = self->m_transparent;
	}
}

//! オブジェクトの状態をXMLで出力します
void OCScatter3D::Serialize(ISerializer* ar)
{
	__super::Serialize(ar);

	MAKE_ENUM_SERIALIZE(ar, _T("MarkerType"), m_type, Marker::Type);
	// 旧バージョンの場合、m_typeが-1(None)の時がある
	if (m_type < 0) {
		m_visible = false;
		m_type = Marker::Dot;
	}
	ar->Serialize(_T("Size"), m_size);
	ar->Serialize(_T("MarkerTransparent"), m_transparent, 100);
}

