#include "StdAfx.h"
#include "Plot3DFactory.h"
#include "Scatter3D.h"
#include "ColorMap.h"
#include "ContourMap.h"
#include "VectorPlot.h"

OCPlot3DFactory::OCPlot3DFactory()
{
}

OCPlot3DFactory::~OCPlot3DFactory()
{
}

unsigned long OCPlot3DFactory::GetPlotCount() const
{
	return 3;
}

// 使ってない
//wchar_t const* OCPlot3DFactory::GetPlotName(unsigned long index) const
//{
//	switch (index)
//	{
//	case 0: return ::GetResourceString(m_owner,"ID_SOURCE_TEXT_3D_SCATR_PLOT");
//	case 1: return ::GetResourceString(m_owner,"ID_SOURCE_TEXT_COLOR_MAP");
//	case 2: return ::GetResourceString(m_owner,"ID_SOURCE_TEXT_CONTOUR_MAP");
//	}
//	return _T("");
//}

IPlotDrawer* OCPlot3DFactory::Create(HWND owner, IMessageReceiver* plot, unsigned long index) const
{
	switch (index)
	{
	case 0: return new OCScatter3D(owner, plot);
	case 1: return new OCContourMap(owner, plot);
	case 2: return new OCColorMap(owner, plot);
//	case 3: return new OCVectorPlot(owner);
	}
	return 0;
}