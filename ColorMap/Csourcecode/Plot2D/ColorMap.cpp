/*!	@file
	@date	2008-04-21
	@author	wada

	@brief	カラーマップ(格子グラフ)の実装
*/
#include "StdAfx.h"
#include <boost/foreach.hpp>
#include <map>
#define _ATL_NO_AUTOMATIC_NAMESPACE
#define _WTL_NO_AUTOMATIC_NAMESPACE
#include <wtl/atlapp.h>
#include <wtl/atlmisc.h>
#include "utility.h"
#include "property.h"
#include "PropertyCreator.hpp"
#include "ICanvas.h"
#include "IPlotData.h"
#include "IMessageReceiver.h"
#include "Message.h"
#include "PlotLib/ColorPattern.h"
#include "ColorMap.h"
#include "ISerializer.h"
#include "AxisEnum.h"

//! Constructor
OCColorMap::OCColorMap(HWND wnd, IMessageReceiver* owner)
    : OCPlotImpl3D(wnd, owner)
    , m_pattern(0), m_visibleLine(false)
{
}

//! Destructor
OCColorMap::~OCColorMap()
{
	if (!m_image.IsNull()) {
		m_image.Destroy();
	}
}

struct yz
{
	double y, z;
};
struct sort_yz
{
	bool operator ()(yz const& a, yz const& b)
	{
		return a.y < b.y;
	}
};

typedef std::vector<yz> vec;
typedef std::map<double, vec> map_t;

map_t create_map_str_x(IPlotData* data, bool const uniqueString)
{
	size_t const n = data->GetDataCount();

	TCHAR buf[20] = {};
	std::vector<float> uniq; {
		std::vector<float> tmp;
		tmp.reserve(n);
		for (size_t i = 0; i < n; ++i) {
			float const y = data->GetData(1, i);
			if (y != FLT_MAX) {
				tmp.push_back(y);
			}
		}
		std::sort(tmp.begin(), tmp.end());
		uniq.assign(tmp.begin(), std::unique(tmp.begin(), tmp.end()));
	}

	// 横一列に並べる
	map_t lines;
	{
		std::map<std::wstring, size_t> cache;
		for (size_t i = 0; i < n; ++i) {
			float const v = data->GetRealData(0, i);
			TCHAR const* str = uniqueString ? data->GetStrData(0, i) : ::_itot(v, buf, 10);

			cache[str] = v;

			float const y = data->GetData(1, i);
			float const z = data->GetData(2, i);
			if (y != FLT_MAX && z != FLT_MAX) {
				yz a = {y, z};
				lines[cache[str]].push_back(a);
			}
		}
	}
	// 欠損部分を埋める
	BOOST_FOREACH(map_t::value_type& a, lines)
	{
		vec& v = a.second;
		std::sort(v.begin(), v.end(), sort_yz());

		std::vector<size_t> ins;
		std::vector<float> ume;
		for (size_t i = 0, n = 0; i < uniq.size() && n < v.size(); ++i) {
			if (uniq[i] == v[n].y) {
				++n;
			}
			else {
				ins.push_back(n);
				ume.push_back(uniq[i]);
			}
		}
		for (int i = (int)ins.size() - 1; i >= 0; --i) {
			yz a = {ume[i], FLT_MIN};
			v.insert(v.begin() + ins[i], a);
		}
		size_t diff = (uniq.size() > v.size()) ? uniq.size() - v.size() : 0;
		for (size_t i = 0; i < diff; ++i) {
			size_t idx = uniq.size() - diff + i;
			yz a = {uniq[idx], FLT_MIN};
			v.push_back(a);
		}
	}
	return lines;
}

map_t create_map_str_y(IPlotData* data, bool const uniqueString)
{
	size_t const n = data->GetDataCount();

	float const xMax = data->GetMaximum(0);
	float const xMin = data->GetMinimum(0);
	float const yMax = data->GetMaximum(1);
	float const yMin = data->GetMinimum(1);

	TCHAR buf[20] = {};
	std::vector<float> uniq; {
		std::vector<std::wstring> tmp;
		tmp.reserve(n);
		for (size_t i = 0; i < n; ++i) {
			float const y = data->GetRealData(1, i);
			if (yMin > y || yMax < y) { continue; }
			
			TCHAR const* str = uniqueString ? data->GetStrData(1, i) : ::_itot(data->GetRealData(1, i), buf, 10);
			if(!uniqueString || std::find(tmp.begin(), tmp.end(), str) == tmp.end()) {
				tmp.push_back(str);
				uniq.push_back(uniq.size());
			}
		}
	}

	std::map<std::wstring, size_t> ymap;
	{
		for (size_t i = 0; i < n; ++i) {
			float const y = data->GetRealData(1, i);
			if (yMin > y || yMax < y) { continue; }

			float const v = data->GetRealData(1, i);
			TCHAR const* str = uniqueString ? data->GetStrData(1, i) : ::_itot(v, buf, 10);
			ymap[str] = v;
		}
	}

	// 横一列に並べる
	map_t lines;
	for (size_t i = 0; i < n; ++i) {
		float const x = data->GetData(0, i);
		// 範囲外のデータは省く
		if (xMin > x || xMax < x) { continue; }

		float const y = data->GetRealData(1, i);
		if (yMin > y || yMax < y) { continue; }

		float const z = data->GetData(2, i);
		if (z != FLT_MAX) {
			TCHAR const* str = uniqueString ? data->GetStrData(1, i) : ::_itot(data->GetRealData(1, i), buf, 10);
			yz a = {ymap[str], z};
			lines[x].push_back(a);
		}
	}

	// 欠損部分を埋める
	BOOST_FOREACH(map_t::value_type& a, lines)
	{
		vec& v = a.second;
		std::sort(v.begin(), v.end(), sort_yz());

		std::vector<size_t> ins;
		std::vector<float> ume;
		for (size_t i = 0, n = 0; i < uniq.size() && n < v.size(); ++i) {
			if (uniq[i] == v[n].y) {
				++n;
			}
			else {
				ins.push_back(n);
				ume.push_back(uniq[i]);
			}
		}
		for (int i = (int)ins.size() - 1; i >= 0; --i) {
			yz a = {ume[i], FLT_MIN};
			v.insert(v.begin() + ins[i], a);
		}
		size_t diff = (uniq.size() > v.size()) ? uniq.size() - v.size() : 0;
		for (size_t i = 0; i < diff; ++i) {
			size_t idx = uniq.size() - diff + i;
			yz a = {uniq[idx], FLT_MIN};
			v.push_back(a);
		}
	}
	return lines;
}

map_t create_map_str_xy(IPlotData* data, bool const uniqueString)
{
	size_t const n = data->GetDataCount();

	float const xMax = data->GetMaximum(0);
	float const xMin = data->GetMinimum(0);
	float const yMax = data->GetMaximum(1);
	float const yMin = data->GetMinimum(1);

	TCHAR buf[20] = {};
	std::vector<float> uniq; {
		std::vector<std::wstring> tmp;
		tmp.reserve(n);
		for (size_t i = 0; i < n; ++i) {
			float const v = data->GetRealData(1, i);

			TCHAR const* str = uniqueString ? data->GetStrData(1, i) : ::_itot(v, buf, 10);
			if (yMin > v || yMax < v) { continue; }
			if (std::find(tmp.begin(), tmp.end(), str) == tmp.end()) {
				tmp.push_back(str);
				uniq.push_back(uniq.size());
			}
		}
	}

	std::map<std::wstring, size_t> ymap;
	{
		for (size_t i = 0; i < n; ++i) {
			float const v = data->GetRealData(1, i);
			if (yMin > v || yMax < v) { continue; }
			TCHAR const* str = uniqueString ? data->GetStrData(1, i) : ::_itot(v, buf, 10);
			ymap[str] = v;
		}
	}
	// 横一列に並べる
	map_t lines;
	{
		for (size_t i = 0; i < n; ++i) {
			float const v = data->GetRealData(0, i);
			if (xMin > v || xMax < v) { continue; }
			float const y = data->GetRealData(1, i);
			if (yMin > y || yMax < y) { continue; }

			TCHAR const* str = uniqueString ? data->GetStrData(0, i) : ::_itot(v, buf, 10);

			float const z = data->GetData(2, i);
			if (z != FLT_MAX) {
				TCHAR const* str = uniqueString ? data->GetStrData(1, i) : ::_itot(y, buf, 10);
				yz a = {ymap[str], z};
				lines[v].push_back(a);
			}
		}
	}
	// 欠損部分を埋める
	BOOST_FOREACH(map_t::value_type& a, lines)
	{
		vec& v = a.second;
		std::sort(v.begin(), v.end(), sort_yz());

		std::vector<size_t> ins;
		std::vector<float> ume;
		for (size_t i = 0, n = 0; i < uniq.size() && n < v.size(); ++i) {
			if (uniq[i] == v[n].y) {
				++n;
			}
			else {
				ins.push_back(n);
				ume.push_back(uniq[i]);
			}
		}
		for (int i = (int)ins.size() - 1; i >= 0; --i) {
			yz a = {ume[i], FLT_MIN};
			v.insert(v.begin() + ins[i], a);
		}
		size_t diff = (uniq.size() > v.size()) ? uniq.size() - v.size() : 0;
		for (size_t i = 0; i < diff; ++i) {
			size_t idx = uniq.size() - diff + i;
			yz a = {uniq[idx], FLT_MIN};
			v.push_back(a);
		}
	}
	return lines;
}


map_t create_map_f(IPlotData* data)
{
	size_t const n = data->GetDataCount();

	float const xMax = data->GetMaximum(0);
	float const xMin = data->GetMinimum(0);

	std::vector<float> uniq; {
		std::vector<float> tmp;
		tmp.reserve(n);
		for (size_t i = 0; i < n; ++i) {
			float const y = data->GetData(1, i);
			if (y != FLT_MAX) {
				tmp.push_back(y);
			}
		}
		std::sort(tmp.begin(), tmp.end());
		uniq.assign(tmp.begin(), std::unique(tmp.begin(), tmp.end()));
	}

	// 横一列に並べる
	map_t lines;
	for (size_t i = 0; i < n; ++i) {
		float const x = data->GetData(0, i);
		// 範囲外のデータは省く
		if (xMin > x || xMax < x) {
			continue;
		}

		float const y = data->GetData(1, i);
		float const z = data->GetData(2, i);
		if (y != FLT_MAX && z != FLT_MAX) {
			yz a = {y, z};
			lines[x].push_back(a);
		}
	}

	// 欠損部分を埋める
	BOOST_FOREACH(map_t::value_type& a, lines)
	{
		vec& v = a.second;
		std::sort(v.begin(), v.end(), sort_yz());

		std::vector<size_t> ins;
		std::vector<float> ume;
		for (size_t i = 0, n = 0; i < uniq.size() && n < v.size(); ++i) {
			if (uniq[i] == v[n].y) {
				++n;
			}
			else {
				ins.push_back(n);
				ume.push_back(uniq[i]);
			}
		}
		for (int i = (int)ins.size() - 1; i >= 0; --i) {
			yz a = {ume[i], FLT_MIN};
			v.insert(v.begin() + ins[i], a);
		}
		size_t diff = (uniq.size() > v.size()) ? uniq.size() - v.size() : 0;
		for (size_t i = 0; i < diff; ++i) {
			size_t idx = uniq.size() - diff + i;
			yz a = {uniq[idx], FLT_MIN};
			v.push_back(a);
		}
	}
	return lines;
}

map_t create_map(IPlotData* data, bool uniqueString)
{
	if (data->GetScaleType(0) == ST_CONSTANT && data->GetScaleType(1) == ST_CONSTANT) {
		return create_map_str_xy(data, uniqueString);
	}
	else if (data->GetScaleType(0) == ST_CONSTANT) {
		return create_map_str_x(data, uniqueString);
	}
	else if (data->GetScaleType(1) == ST_CONSTANT) {
		return create_map_str_y(data, uniqueString);
	}
	return create_map_f(data);
}


//! カラーマップを描画します
/*!
	ここでは、すでに作成済みのビットマップを貼り付けます
*/
void OCColorMap::Draw(ICanvas* g, OCRect const& rect, DrawArgs const& a) const
{
	if (!IsVisible()) { return; }
	if (!m_data->GetDataCount()) { return; }

    if (!m_pattern) { return; }

	if (m_image.IsNull()) {
		DrawArgs arg = a;
		double xMax = arg.xMax;
		double xMin = arg.xMin;
		double yMax = arg.yMax;
		double yMin = arg.yMin;

		double zMax = m_data->GetMaximum(2);
		double zMin = m_data->GetMinimum(2);

		m_pattern->SetMinMax(zMin, zMax);

		map_t list = create_map(m_data, !!m_plot->SendMessage(PI_MSG_GET_CONF_UNIQUE_STRING, 0, 0));
		if (list.empty()) { return; }

		std::vector<double> xarr;
		BOOST_FOREACH(map_t::value_type& line, list) {
			xarr.push_back(line.first);
		}
		if (xarr.size() <= 1) { return; }

		int width = static_cast<int>(MMToPx(rect.Width()) );//+ leftMargin + rightMargin);
		int height = static_cast<int>(MMToPx(rect.Height()) );//+ topMargin + bottomMargin);
		if (width <= 0 || height <= 0) {
			width = 10;
			height = 10;
		}
		m_image.Create(width, height, 24);

		BYTE* bits = reinterpret_cast<BYTE*>(m_image.GetBits());
		int const pitch = m_image.GetPitch();
		int const bpp = m_image.GetBPP() / 8;

		size_t i = 0;
		int xPrev = -INT_MAX;
		CPoint prevPoint(INT_MAX, INT_MAX);
		double prevZValue;
		BOOST_FOREACH(map_t::value_type& line, list) {
			int cur = static_cast<int>(MMToPx(rect.Width() * arg.XNormalize(xarr[i])));
			int next = (i == xarr.size() - 1)
				? static_cast<int>(MMToPx(rect.Width() * arg.XNormalize(xarr[i] + (xarr[i] - xarr[i - 1]))))
				: static_cast<int>(MMToPx(rect.Width() * arg.XNormalize(xarr[i + 1])));
			int const gridWidth = next - cur;
			int x1 = (cur - gridWidth / 2);
			int x2 = (cur + gridWidth / 2);

			if (i) {
				x1 = xPrev;
			}
			xPrev = x2;

			vec& v = line.second;

			bool first = true;
			int yPrev = INT_MAX;
			for (int j = 0; j < (int)v.size(); ++j) {
				int cur = static_cast<int>(MMToPx(rect.Height() - rect.Height() * arg.YNormalize(v[j].y)));
				int next = ((size_t)j == v.size() - 1)
					? static_cast<int>(MMToPx(rect.Height() - rect.Height() * arg.YNormalize(v[j].y + (v[j].y - v[j - 1].y))))
					: static_cast<int>(MMToPx(rect.Height() - rect.Height() * arg.YNormalize(v[j + 1].y)));
				if (cur > next) { std::swap(cur, next); }
				if (IsEqual(cur, next)) { continue; }

				int const gridHeight = next - cur;
				int y1 = (next - gridHeight / 2);
				int y2 = (next + gridHeight / 2);

				if (!first) {
					y2 = yPrev;
				}
				yPrev = y1;
				first = false;

				if (y1 > y2) { std::swap(y1, y2); }

				double const value = (prevPoint.x == INT_MAX || prevPoint != CPoint(x1, y1)) ? v[j].z : std::max<double>(v[j].z, prevZValue);
				COLORREF color = value != FLT_MAX ? m_pattern->GetColor(value) : m_pattern->GetColor(zMin);
				for (int xPos = x1; xPos < x2; ++xPos) {
					for (int yPos = y1; yPos < y2; ++yPos) {
						int const xp = std::min<int>(std::max<int>((xPos), 0), width - 1);
						int const yp = std::min<int>(std::max<int>((yPos), 0), height - 1);
						int addr = xp * bpp + yp * pitch;
						if (m_visibleLine) {
							// x1, y1にも線を描くと太くなるので右と下にだけ描いていく
							if (xPos == (x2 - 1) || yPos == (y2 - 1)) {
								bits[addr + 2] = GetRValue(m_lineColor);
								bits[addr + 1] = GetGValue(m_lineColor);
								bits[addr + 0] = GetBValue(m_lineColor);
								continue;
							}
						}
						bits[addr + 2] = GetRValue(color);
						bits[addr + 1] = GetGValue(color);
						bits[addr + 0] = GetBValue(color);
					}
				}
			}

			++i;
			if (i + 1 > xarr.size()) { break; }
		}
	}

	HDC hdc = g->GetHDC();
	double const ratio = g->GetZoomRatio();
	// 左座標、上座標、右座標、下座標ともに、mm単位をPixel単位に変換後四捨五入して整数値にする
	int pxLeft = static_cast<int>( MMToPx(rect.left * ratio) + 0.5 );
	int pxTop = static_cast<int>( MMToPx(rect.top * ratio) + 0.5 );
	int pxRight = static_cast<int>( MMToPx(rect.right * ratio) + 0.5 );
	int pxBottom = static_cast<int>( MMToPx(rect.bottom * ratio) + 0.5 );
	// Pixel単位、整数値になった各座標から、整数Pixel単位の幅と高さを算出する
	// 左上(1,1)右下(1,1)の領域でも、(1,1)の点は含むので、幅、高さともに1となる(Width, Heightで+1するの根拠)
	int pxWidth = pxRight - pxLeft + 1;
	int pxHeight = pxBottom - pxTop + 1;

	// 普通に縮小すると枠線が見えなくなることがあるのでモードを変える
	//!@todo 少しぼやけてしまうので、できれば画像のサイズとオブジェクトのサイズを同一としたい
	::SetStretchBltMode(hdc, HALFTONE);
	m_image.StretchBlt(hdc, pxLeft, pxTop, pxWidth, pxHeight, SRCCOPY);

	g->ReleaseHDC(hdc);

	// グリッドが見えるように、自分の上にグリッドを書く
	m_plot->SendMessage(PI_MSG_DRAW_GRID, 0, 0);
}

void OCColorMap::DrawSelected(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const
{
}

//! マップを作り直します
void OCColorMap::RecreateImage()
{
	if (!m_image.IsNull()) {
		m_image.Destroy();
	}
}

//! マップを更新します
void OCColorMap::Update(bool)
{
	RecreateImage();
}

//! シリアライズで複製されたデータでマップイメージを作ります
void OCColorMap::AfterSerializeUpdate()
{
	RecreateImage();
}

//! プロパティを追加します
void OCColorMap::AddProperty(IPropertyCreator* prop, ICanvas* g, bool /*plotMode*/)
{
	IPropertyGroup* grp = prop->CreateGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_GRID_GRAPH"), PI_COLORMAP_VISIBLE, m_visible);
	/*if (m_visible)*/ {
		grp->AddBoolProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_DRAW_FRAME"), PI_COLORMAP_VISIBLELINE, m_visibleLine, !m_visible);
		/*if (m_visibleLine)*/ {
			grp->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_FRAME"), PI_COLORMAP_LINECOLOR, m_lineColor, !m_visible || !m_visibleLine);
		}
	}
}

//! プロパティを追加します
void OCColorMap::AddTabProperty(IPropertyCreator* prop, ICanvas* g, bool /*plotMode*/)
{
	IPropertyGroup* grp = prop->CreateGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_GRID_GRAPH"));
	IPropertyGroup* parent = grp->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_GRID_GRAPH"), PI_COLORMAP_VISIBLE, m_visible);
	/*if (m_visible)*/ {
		parent->AddBoolProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_DRAW_FRAME"), PI_COLORMAP_VISIBLELINE, m_visibleLine, !m_visible);
		/*if (m_visibleLine)*/ {
			parent->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_FRAME"), PI_COLORMAP_LINECOLOR, m_lineColor, !m_visible || !m_visibleLine);
		}
	}
}



//! プロパティを更新します
bool OCColorMap::SetProperty(long id, LPARAM value, LPARAM* source)
{
	switch (id)
    {
	case PI_COLORMAP_VISIBLE:
		::SetPropertyHelper(m_visible, value, source);
		return true;
	case PI_COLORMAP_LINECOLOR:
		::SetPropertyHelper(m_lineColor, value, source);
		Update(false);
		return true;
	case PI_COLORMAP_VISIBLELINE:
		::SetPropertyHelper(m_visibleLine, value, source);
		Update(false);
		return true;
	case PI_COLOR_PATTERN:
        m_pattern = reinterpret_cast<OCColorPattern*>(value);
        return true;
    }
    return false;
}

void OCColorMap::CopyFrom(IPlotDrawer const* src)
{
	__super::CopyFrom(src);
	if (OCColorMap const* self = dynamic_cast<OCColorMap const*>(src)) {
		m_visibleLine = self->m_visibleLine;
	}
}

void OCColorMap::Serialize(ISerializer* ar)
{
	__super::Serialize(ar);

	ar->Serialize(_T("VisibleLine"), m_visibleLine, false);
}