#include "StdAfx.h"
#include <boost/foreach.hpp>
#include "PlotFactory.h"
//#include "BuildUpBarPlot.h"
#include "Plot2DFactory.h"
#include "Plot3DFactory.h"

OCPlotFactory::OCPlotFactory(HWND)
{
	m_factories[2] = new OCPlot2DFactory();
	m_factories[3] = new OCPlot3DFactory();
}

OCPlotFactory::~OCPlotFactory()
{
	BOOST_FOREACH(FactoryMap::value_type iter, m_factories) {
		delete iter.second;
	}
}

unsigned long OCPlotFactory::GetId() const
{
	return 1;
}

unsigned long OCPlotFactory::GetDimension() const
{
	return 2;
}

//! 生成可能なプロットの数を取得します
unsigned long OCPlotFactory::GetPlotCount(unsigned long dimension) const
{
	if (dimension == 4) { return 1; }
	return m_factories.find(dimension)->second->GetPlotCount();
}

#include "VectorPlot.h"
//! プロット描画クラスを生成します
IPlotDrawer* OCPlotFactory::Create(HWND owner, IMessageReceiver* plot, unsigned long dimension, unsigned long index) const
{
	if (dimension == 4) { return new OCVectorPlot(owner, plot); }
	return m_factories.find(dimension)->second->Create(owner, plot, index);
}

IPlotDrawer* OCPlotFactory::CreateBuildUpper() const
{
//	return new OCBuildUpBarPlot(0, 0);
	return 0;
}

//wchar_t const* OCPlotFactory::GetPlotName(unsigned long dimension, unsigned long index) const
//{
//	return m_factories.find(dimension)->second->GetPlotName(index);
//}
