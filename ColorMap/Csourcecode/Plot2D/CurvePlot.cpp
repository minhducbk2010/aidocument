/*!	@file
	@date	2008-03-04
	@author	wada

	@brief	曲線図の実装
*/
#include "StdAfx.h"
#include "ICanvas.h"
#include "IPlotData.h"
#include "IMessageReceiver.h"
#include "property.h"
#include "propertycreator.hpp"
#include "PropertyHelper.hpp"
#include "ISerializer.h"
#include "Message.h"
#include "PluginUtility/pluginutil.h"

#include "Interpolate/InterpolateFactory.h"
#include "Interpolate/Spline.hpp"
#include "Interpolate/BSpline.hpp"
#include "Interpolate/Hermite.hpp"
#include "Interpolate/Lagrange.hpp"
#include "Interpolate/RegressionCalc.hpp"
#include "Interpolate/MovingAverage.hpp"

#include "CurvePlot.h"
#include "RectClipping.h"

//! Constructor
OCCurvePlot::OCCurvePlot(HWND hWnd, IMessageReceiver* plot)
	: OCPlotImpl(hWnd, plot)
	, m_factory(new OCInterpolateFactory(plot))
	, m_interType(Interpolation::BSpline), m_order(2), m_extrapolation(0)
	, m_showFormula(false), m_regType(OCRegressionCalc::Regression)
	, m_range(2)
	, m_online(false)
	, m_dispMessage(true)
{
	SetInterpolationType(m_interType, false);
}

//! Destructor
OCCurvePlot::~OCCurvePlot()
{
}

//! 曲線図を描画します
void OCCurvePlot::Draw(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const
{
	DrawImpl(false, g, rect, arg);
}

void OCCurvePlot::DrawSelected(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const
{
	if (m_selected) {
		DrawImpl(true, g, rect, arg);
	}
}

void OCCurvePlot::DrawImpl(bool selected, ICanvas* g, OCRect const& rect, DrawArgs const& arg) const
{
	if (!IsVisible()) { return; }
	if (!m_data->GetDataCount()) { return; }

	g->SetPen(m_lineColor, m_lineWidth, m_lineStyle, m_lineRoundType);

	RectClipping clipper(g, m_clippingType, rect);

	std::vector<OCPoint> pts;
	GetDrawPoints(rect, arg, &pts);
	if (pts.size() > 1) {
		if (selected) {
			g->SetPen(RGB(0, 255, 0), 1, PS_DOT, 0);
			g->DrawRubberLines(&pts[0], pts.size());
		}
		else {
			g->DrawLines(&pts[0], pts.size());
		}
	}

	clipper.Reset();

	// 式を描画する
	if (m_showFormula){
		if(OCRegressionCalc* regress = dynamic_cast<OCRegressionCalc*>(m_inter.get())) {
			std::wstring formula = regress->GetFormula();
		m_plot->SendMessageW(PI_SET_FORMULA_STRINGS, reinterpret_cast<WPARAM>(&formula), 0);
		}
	}
}

void OCCurvePlot::GetDrawPoints(OCRect const& rect, DrawArgs const& arg, std::vector<OCPoint>* arr) const
{
	std::vector<double> xTmp, yTmp;
	if (!const_cast<OCCurvePlot*>(this)->InitInterpolate(m_inter.get(), xTmp, yTmp)) { return; }

	bool const isRegress = m_interType == Interpolation::Regress;
	if (xTmp.size() == 2 && !isRegress) {
		// 二点しかない時は直線にする
		for (size_t i = 0; i < yTmp.size(); ++i) {
			double xPos = rect.Width() * arg.XNormalize(xTmp[i]) + rect.left;
			double yPos = rect.Height() - rect.Height() * arg.YNormalize(yTmp[i]) + rect.top;
			arr->push_back(OCPoint(xPos, yPos));
		}
	}
	else {
		double xStart, xStop;
		GetDrawRange(arg, &xStart, &xStop, xTmp.front(), xTmp.back());

		int const count = 1000;
		double const interval = ((xStop - xStart) / count);
		int curentX = INT_MAX;
		for (int i = 0; i < count; ++i) {
			double x = interval * i + xStart;
			if (x > xStop) { break; }
			double y = m_inter->Calc(x);
			if (y == DBL_MAX) { continue; }

			double xPos = rect.Width() * arg.XNormalize(x) + rect.left;
			double yPos = rect.Height() - rect.Height() * arg.YNormalize(y) + rect.top;

			int const px = static_cast<int>(MMToPx(xPos));
			if (curentX != px) {
				arr->push_back(OCPoint(xPos, yPos));

				curentX = px;
			}
		}
		{
			double y = m_inter->Calc(xStop);
			if (y != DBL_MAX) {
				double xPos = rect.Width() * arg.XNormalize(xStop) + rect.left;
				double yPos = rect.Height() - rect.Height() * arg.YNormalize(y) + rect.top;
				arr->push_back(OCPoint(xPos, yPos));
			}
		}
	}
}

//! 描画対象のX区間を取得します
void OCCurvePlot::GetDrawRange(DrawArgs const& arg, double* start, double* stop, double sourceBegin, double sourceEnd) const
{
	/*	回帰式の場合:
			外挿補填する場合は軸の最大・最小値の範囲
		クリッピングされている場合:
			軸の最大・最小値の範囲とデータの最大・最小のうち、狭い方(外挿を求めないようにする)
		その他:
			データの最大・最小の範囲
	*/
	bool const isRegress = m_interType == Interpolation::Regress;
	bool const startExt = isRegress && m_extrapolation == 1 || m_extrapolation == 3;
	bool const stopExt = isRegress && m_extrapolation == 2 || m_extrapolation == 3;
	*start	= startExt ? arg.xMin : arg.xClipping ? std::max<double>(arg.xMin, sourceBegin) : sourceBegin;
	*stop	= stopExt ? arg.xMax : arg.xClipping ? std::min<double>(arg.xMax, sourceEnd) : sourceEnd;
}

void OCCurvePlot::EnableOnlineMode(bool enable)
{
	m_online = enable;
	//InitInterpolate(m_inter.get());
}

bool OCCurvePlot::GetNearPosition(OCPoint* p, OCPoint* value, OCRect const& rect, DrawArgs const& arg, bool next) const
{
	std::vector<double> xTmp, yTmp;
	const_cast<OCCurvePlot*>(this)->InitInterpolate(m_inter.get(), xTmp, yTmp);

	double const xMax = arg.xMax;
	double const xMin = arg.xMin;
	double const yMax = arg.yMax;
	double const yMin = arg.yMin;

	size_t minIndex, maxIndex;
	m_data->FindMinMaxIndex(0, &minIndex, &maxIndex); //!@todo パフォーマンスに難あり
	double const xRealMax = m_data->GetData(0, maxIndex);
	double const xRealMin = m_data->GetData(0, minIndex);

	while (true) {
		// X座標からX値を取得し、そこからYの値を得る
		//double const x = std::max<double>(std::min<double>((p->x - rect.left) / rect.Width() * (xMax - xMin) + xMin, m_max), m_min);
		double const x = (p->x - rect.left) / rect.Width() * (xMax - xMin) + xMin;
		double const y = m_inter->Calc(x);
		p->x = rect.Width() * (x - xMin) / (xMax - xMin) + rect.left;
		p->y = (rect.Height() - rect.Height() * ((y - yMin) / (yMax - yMin))) + rect.top;
		value->x = x;
		value->y = y;

		// Y方向にクリッピングする場合、Yの値がY軸の範囲外にある個所にはヒットさせない
		if (m_clippingType == plot::AllClip || m_clippingType == plot::YClip) {
			if (y > yMax || y < yMin) {
				double const offset = 0.05; // オフセット量(mm)
				if (next)	{ p->x += offset; }
				else		{ p->x -= offset; }

				if (x >= xRealMax || x <= xRealMin) {
					return false;
				}
				continue;
			}
		}
		break; // クリッピングしない場合はループしない
	}
	return true;
}

void OCCurvePlot::GetSearchPointFromValue(OCPoint* point, OCPoint* value, OCRect const& rect) const
{
	std::vector<double> xTmp, yTmp;
	const_cast<OCCurvePlot*>(this)->InitInterpolate(m_inter.get(), xTmp, yTmp);

	double xMax = m_data->GetMaximum(0);
	double xMin = m_data->GetMinimum(0);
	double yMax = m_data->GetMaximum(1);
	double yMin = m_data->GetMinimum(1);

	double const y = m_inter->Calc(value->x);
	point->x = rect.Width() * (value->x - xMin) / (xMax - xMin) + rect.left;
	point->y = (rect.Height() - rect.Height() * ((y - yMin) / (yMax - yMin))) + rect.top;
	value->y = y;
}

bool PtInLine(OCPoint begin, OCPoint end, double X, double Y, double range); // LinePlot.cpp
//! 曲線図の当たり判定
bool OCCurvePlot::HitTest(OCRect const& rect, DrawArgs const& arg, double x, double y)
{
	std::vector<OCPoint> pts;
	GetDrawPoints(rect, arg, &pts);

	for (int i = 0, n = static_cast<int>(pts.size() - 1); i < n; ++i) {
		if (PtInLine(pts[i], pts[i + 1], x, y, 2)) { return true; }
	}

	return false;
}

//! プロパティを設定します
void OCCurvePlot::AddProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode)
{
	{
		IPropertyGroup* group = prop->CreateGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_CURVE"), PI_CURVE_VISIBLE, m_visible, !plotMode);
		/*if (m_visible)*/
		{
			bool readOnly = !plotMode || !m_visible;

			IPropertyGroup* lineGr = group->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_LINE"));
			{
				MakeLineProperty(m_plot, lineGr, g, readOnly, m_lineStyle, PI_CURVE_LINESTYLE);
				lineGr->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_KANJI"), PI_CURVE_LINECOLOR, m_lineColor, readOnly || m_lineStyle == PS_NULL);
				lineGr->AddSelectorPropertyR(::GetResourceString(m_plot,"ID_SOURCE_TEXT_THICKNESS_PT"), PI_CURVE_LINEWIDTH, readOnly || m_lineStyle == PS_NULL, 
					m_lineWidth, 9, 0.25, 0.5, 0.75, 1.0, 1.5, 2.25, 3.0, 4.5, 6.0);

				std::wstring corner1 = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_ROUND");
				std::wstring corner2 = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_SQUARE");
				lineGr->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SHAPE_OF_CORNER"), PI_CURVE_LINEROUNDTYPE, m_lineRoundType, readOnly || m_lineStyle == PS_NULL,
						2, corner1.c_str(), corner2.c_str());
			}

			std::vector<std::wstring> names;
			for (int i = 0; i < m_factory->GetCount(); ++i) {
				names.push_back(m_factory->GetName(i));
			}

			IPropertyGroup* typeGr = group->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_TYPE_BETWEEN_ASSISTANT"));	// todo:ここをできればコンボボックスにしたい...
			{
				typeGr->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_TYPE_BETWEEN_ASSISTANT"), PI_CURVE_INTERPOLATE_TYPE, readOnly, m_interType, names);

				//!@todo 補間種別が異なる場合は表示したくない

				bool const regress = !readOnly && m_interType == Interpolation::Regress;
				bool const mave = !readOnly && m_interType == Interpolation::MovingAverage;

				// 回帰曲線
				std::wstring classProp1 = GetResourceString(m_plot, "ID_SOURCE_TEXT_MULTINOMIAL_EXPRESSION");
				std::wstring classProp2 = GetResourceString(m_plot, "ID_SOURCE_TEXT_LINEAR_KANJI");
				std::wstring classProp3 = GetResourceString(m_plot, "ID_SOURCE_TEXT_LOGARITHM");
				std::wstring classProp4 = GetResourceString(m_plot, "ID_SOURCE_TEXT_INDEX");
				std::wstring classProp5 = GetResourceString(m_plot, "ID_SOURCE_TEXT_INVOLUTION");
				typeGr->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_CLASSIFICATION"), PI_CURVE_APPROXIMATE, m_regType, !regress,
					5, classProp1.c_str(), classProp2.c_str(), classProp3.c_str(), classProp4.c_str(), classProp5.c_str());

				bool const mul = regress && m_regType == OCRegressionCalc::Regression;
				std::wstring zisu1 = ::GetResourceString(m_plot,"ID_RESOURCE_2");
				std::wstring zisu2 = ::GetResourceString(m_plot,"ID_RESOURCE_3");
				std::wstring zisu3 = ::GetResourceString(m_plot,"ID_RESOURCE_4");
				std::wstring zisu4 = ::GetResourceString(m_plot,"ID_RESOURCE_FIVE");
				typeGr->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_DEGREE_ZISU"), PI_CURVE_ORDER, m_order - 2, !mul,
					4, zisu1.c_str(), zisu2.c_str(), zisu3.c_str(), zisu4.c_str());

				std::wstring extraProp1 = GetResourceString(m_plot, "ID_SOURCE_TEXT_NONE");
				std::wstring extraProp2 = GetResourceString(m_plot, "ID_SOURCE_TEXT_FRONT");
				std::wstring extraProp3 = GetResourceString(m_plot, "ID_SOURCE_TEXT_REAR");
				std::wstring extraProp4 = GetResourceString(m_plot, "ID_SOURCE_TEXT_BOTH");
				typeGr->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_EXTRAPOLATION"), PI_CURVE_EXTRAPOLATION, m_extrapolation, !regress,
					4, extraProp1.c_str(), extraProp2.c_str(), extraProp3.c_str(), extraProp4.c_str());

				typeGr->AddBoolProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SHOW_EXPRESSION"), PI_CURVE_FORMULA_VISIBLE, m_showFormula, !regress);

				// 移動平均
				//!@todo 欠損値がある場合のスピンボタンの最大値
				int const count = static_cast<int>(m_data->GetDataCount() - 1);
				typeGr->AddSpinProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SECTION"), PI_CURVE_MA_RANGE, m_range, 2, count, !mave || 2 >= count);
			}
		}
	}
}

//! プロパティを設定します
void OCCurvePlot::AddTabProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode)
{
	{
		IPropertyGroup* parent;

		IPropertyGroup* group  = prop->CreateGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_CURVE"));
		parent=group->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_CURVE"), PI_CURVE_VISIBLE, m_visible, !plotMode);
		/*if (m_visible)*/
		{
			bool readOnly = !plotMode || !m_visible;

			IPropertyGroup* lineGr = parent->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_LINE"));
			{
				MakeLineProperty(m_plot, lineGr, g, readOnly, m_lineStyle, PI_CURVE_LINESTYLE);
				lineGr->AddColorProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_COLOR_KANJI"), PI_CURVE_LINECOLOR, m_lineColor, readOnly || m_lineStyle == PS_NULL);
				lineGr->AddSelectorPropertyR(::GetResourceString(m_plot,"ID_SOURCE_TEXT_THICKNESS_PT"), PI_CURVE_LINEWIDTH, readOnly || m_lineStyle == PS_NULL, 
					m_lineWidth, 9, 0.25, 0.5, 0.75, 1.0, 1.5, 2.25, 3.0, 4.5, 6.0);

				std::wstring corner1 = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_ROUND");
				std::wstring corner2 = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_SQUARE");
				lineGr->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SHAPE_OF_CORNER"), PI_CURVE_LINEROUNDTYPE, m_lineRoundType, readOnly || m_lineStyle == PS_NULL,
					2, corner1.c_str(), corner2.c_str());
			}

			std::vector<std::wstring> names;
			for (int i = 0; i < m_factory->GetCount(); ++i) {
				names.push_back(m_factory->GetName(i));
			}

			IPropertyGroup* typeGr = parent->AddGroup(::GetResourceString(m_plot,"ID_SOURCE_TEXT_TYPE_BETWEEN_ASSISTANT"));	// todo:ここをできればコンボボックスにしたい...
			{
				typeGr->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_TYPE_BETWEEN_ASSISTANT"), PI_CURVE_INTERPOLATE_TYPE, readOnly, m_interType, names);

				//!@todo 補間種別が異なる場合は表示したくない

				bool const regress = !readOnly && m_interType == Interpolation::Regress;
				bool const mave = !readOnly && m_interType == Interpolation::MovingAverage;

				// 回帰曲線
				std::wstring classProp1 = GetResourceString(m_plot, "ID_SOURCE_TEXT_MULTINOMIAL_EXPRESSION");
				std::wstring classProp2 = GetResourceString(m_plot, "ID_SOURCE_TEXT_LINEAR_KANJI");
				std::wstring classProp3 = GetResourceString(m_plot, "ID_SOURCE_TEXT_LOGARITHM");
				std::wstring classProp4 = GetResourceString(m_plot, "ID_SOURCE_TEXT_INDEX");
				std::wstring classProp5 = GetResourceString(m_plot, "ID_SOURCE_TEXT_INVOLUTION");
				typeGr->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_CLASSIFICATION"), PI_CURVE_APPROXIMATE, m_regType, !regress,
					5, classProp1.c_str(), classProp2.c_str(), classProp3.c_str(), classProp4.c_str(), classProp5.c_str());

				bool const mul = regress && m_regType == OCRegressionCalc::Regression;
				std::wstring zisu1 = ::GetResourceString(m_plot,"ID_RESOURCE_2");
				std::wstring zisu2 = ::GetResourceString(m_plot,"ID_RESOURCE_3");
				std::wstring zisu3 = ::GetResourceString(m_plot,"ID_RESOURCE_4");
				std::wstring zisu4 = ::GetResourceString(m_plot,"ID_RESOURCE_FIVE");
				typeGr->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_DEGREE_ZISU"), PI_CURVE_ORDER, m_order - 2, !mul,
					4, zisu1.c_str(), zisu2.c_str(), zisu3.c_str(), zisu4.c_str());

				std::wstring extraProp1 = GetResourceString(m_plot, "ID_SOURCE_TEXT_NONE");
				std::wstring extraProp2 = GetResourceString(m_plot, "ID_SOURCE_TEXT_FRONT");
				std::wstring extraProp3 = GetResourceString(m_plot, "ID_SOURCE_TEXT_REAR");
				std::wstring extraProp4 = GetResourceString(m_plot, "ID_SOURCE_TEXT_BOTH");
				typeGr->AddArrayProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_EXTRAPOLATION"), PI_CURVE_EXTRAPOLATION, m_extrapolation, !regress,
					4, extraProp1.c_str(), extraProp2.c_str(), extraProp3.c_str(), extraProp4.c_str());

				typeGr->AddBoolProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SHOW_EXPRESSION"), PI_CURVE_FORMULA_VISIBLE, m_showFormula, !regress);

				// 移動平均
				//!@todo 欠損値がある場合のスピンボタンの最大値
				int const count = static_cast<int>(m_data->GetDataCount() - 1);
				typeGr->AddSpinProperty(::GetResourceString(m_plot,"ID_SOURCE_TEXT_SECTION"), PI_CURVE_MA_RANGE, m_range, 2, count, !mave || 2 >= count);
			}
		}
	}
}

//! プロパティを設定します
bool OCCurvePlot::SetProperty(long id, LPARAM value, LPARAM* source)
{
	switch (id)
	{
	case PI_CURVE_VISIBLE:
		::SetPropertyHelper(m_visible, value, source);
		return true;
	case PI_CURVE_LINESTYLE:
		if (source) { *source = m_lineStyle; }
		m_lineStyle = int(value);
		return true;
	case PI_CURVE_LINECOLOR:
		if (source) { *source = m_lineColor; }
		m_lineColor = COLORREF(value);
		return true;
	case PI_CURVE_LINEROUNDTYPE:
		::SetPropertyHelper(m_lineRoundType, value, source);
		return true;
	case PI_CURVE_LINEWIDTH:
		if (!::SetPropertyHelper(m_lineWidth, value, source, 0.1f, 100.0f)) {
			std::wstring msg = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_INPUT_THICKNESS_LINE_RANGE_01_100");
			WarningMessage(m_plot, msg.c_str());
			return false;
		}
		break;
	case PI_CURVE_INTERPOLATE_TYPE:
		if (source) { *source = m_interType; }
		SetInterpolationType(static_cast<int>(value), true);

		//回帰式の表示
		{
			bool hidden = true;
			if(static_cast<int>(value) == 3){
				hidden = false;
			}else{
				hidden = true;
			}
			m_plot->SendMessageW(PI_SET_FORMULA_HIDDEN, reinterpret_cast<WPARAM>(&hidden), 0);
		}
		return true;
	case PI_CURVE_ORDER:
		::SetPropertyHelper(m_order, value + 2, source);
		if (OCRegressionCalc* regress = dynamic_cast<OCRegressionCalc*>(m_inter.get())) {
			regress->SetDegree(m_order);

			std::wstring formula = regress->GetFormula();
			m_plot->SendMessageW(PI_SET_FORMULA_STRINGS, reinterpret_cast<WPARAM>(&formula), 0);

			CheckRegress(regress);
		}
		return true;
	case PI_CURVE_EXTRAPOLATION:
		::SetPropertyHelper(m_extrapolation, value, source);
		return true;
	case PI_CURVE_FORMULA_VISIBLE:
		::SetPropertyHelper(m_showFormula, value, source);
		if((source == NULL) || (*source != 5))
			m_plot->SendMessageW(PI_SET_FORMULA_VISIBLE, reinterpret_cast<WPARAM>(&m_showFormula), 0);
		return true;
	case PI_CURVE_APPROXIMATE:
		::SetPropertyHelper(m_regType, value, source);
		if (OCRegressionCalc* regress = dynamic_cast<OCRegressionCalc*>(m_inter.get())) {
			regress->SetType(OCRegressionCalc::Type(m_regType));
			CheckRegress(regress);
		}
		return true;
	case PI_CURVE_MA_RANGE:
		{
			int current = m_range;
			::SetPropertyHelper(current, value, source);
			if (current < 2 || current >= static_cast<int>(m_data->GetDataCount())) {
				size_t const count = m_data->GetDataCount() - 1;
				TCHAR msg[100] = {};
				if (count <= 1) {
					::_stprintf_s(msg, ::GetResourceString(m_plot,"ID_SOURCE_TEXT_TWO_POINTS_DATA_ARE_FOLOWS_NOT_PERFORM_MOVING_AVERAGE"), count);
					m_range = 0;
				}
				else {
					::_stprintf_s(msg, ::GetResourceString(m_plot,"ID_SOURCE_TEXT_APPOINT_SECTION_MOVING_AVERAGE_BETWEEN_V_FROM_2"), count);
				}
				WarningMessage(m_plot, msg);
				return false;
			}
			
			m_range = current;
			if (OCMovingAverage* ma = dynamic_cast<OCMovingAverage*>(m_inter.get())) {
				ma->SetAverageCount(m_range);
			}
		}
		return true;
	}

	return __super::SetProperty(id, value, source);
}

//! プロパティを取得します
bool OCCurvePlot::GetProperty(long id, LPARAM* ret) const
{
	switch (id)
	{
	case PI_CURVE_INTERPOLATE_TYPE:
		*ret = m_interType;
		return true;
	case PI_CURVE_ORDER:
		*ret = m_order;
		return true;
	case PI_CURVE_APPROXIMATE:
		*ret = m_regType;
		return true;
	case PI_CURVE_MA_RANGE:
		*ret = m_range;
		return true;
	}
	return false;
}

bool OCCurvePlot::InitInterpolate(OCInterpolate* inter)
{
	m_xTemp.clear(); m_yTemp.clear();
	return InitInterpolate(inter, m_xTemp, m_yTemp);
}
// 玉虫色な解決
bool OCCurvePlot::InitInterpolate(OCInterpolate* inter, std::vector<double>& xTmp, std::vector<double>& yTmp)
{
	if (m_data && m_data->GetDataCount() <= 2) {
		m_regType = OCRegressionCalc::Linear;
	}

	// 補間クラス内でソートされるのでコピーを作っておく
	xTmp.reserve(m_data->GetDataCount());
	yTmp.reserve(m_data->GetDataCount());
	m_max = -DBL_MAX, m_min = DBL_MAX;
	double yValue = 0;
	for (size_t i = 0, size = m_data->GetDataCount(); i < size; ++i) {
		float x = m_data->GetData(0, (int)i);
		float y = m_data->GetData(1, (int)i);
		if (x != FLT_MAX && y != FLT_MAX) {
			xTmp.push_back(x);
			yValue = m_accumulative ? yValue + y : y;
			yTmp.push_back(yValue);
			m_max = std::max<double>(m_max, x);
			m_min = std::min<double>(m_min, x);
		}
	}

	if (xTmp.size() <= 1) { return false; }

	m_inter->Init((int)xTmp.size(), &xTmp[0], &yTmp[0]);
	if (OCMovingAverage* m = dynamic_cast<OCMovingAverage*>(m_inter.get())) {
		m_range = m->GetCount();
	}

	if (!m_inter->Prepare()) {
		if (m_interType == 2) {
			//std::wstring msg =  _T("エルミート補間に失敗しました。\n3次スプラインに切り替えます。");
			//::MessageBox(m_owner, msg.c_str(), _T("O-Chart"), MB_OK|MB_ICONINFORMATION);
		}
		else if (m_interType == 5) {
			//std::wstring msg =  _T("ラグランジュ補間に失敗しました。\n3次スプラインに切り替えます。");
			//::MessageBox(m_owner, msg.c_str(), _T("O-Chart"), MB_OK|MB_ICONINFORMATION);
		}
		const_cast<OCCurvePlot*>(this)->SetInterpolationType(Interpolation::BSpline, false);
		m_inter->Init((int)xTmp.size(), &xTmp[0], &yTmp[0]);
		m_inter->Prepare();
	}
	return true;
}

//! 補間タイプを設定します
void OCCurvePlot::SetInterpolationType(int type, bool validate)
{
	m_interType = type;
	m_inter.reset(m_factory->Create(type));

	if (OCRegressionCalc* regress = dynamic_cast<OCRegressionCalc*>(m_inter.get())) {
		regress->SetDegree(m_order);
		regress->SetType(OCRegressionCalc::Type(m_regType));

		std::wstring formula = regress->GetFormula();
		bool hidden = false;
		m_plot->SendMessageW(PI_SET_FORMULA_HIDDEN, reinterpret_cast<WPARAM>(&hidden), 0);
		m_plot->SendMessageW(PI_SET_FORMULA_VISIBLE, reinterpret_cast<WPARAM>(&m_showFormula), 0);
		m_plot->SendMessageW(PI_SET_FORMULA_STRINGS, reinterpret_cast<WPARAM>(&formula), 0);
	}
	else if (OCMovingAverage* ma = dynamic_cast<OCMovingAverage*>(m_inter.get())) {
		ma->SetAverageCount(m_range);
	}

	if (!m_data || m_data->GetDataCount() <= 1) { return; }
	if (!validate) { return; }

	// 指定した補間タイプが使用可能かどうかチェック
	//!@todo ちょっと効率が悪い

	std::vector<double> xTmp, yTmp;
	xTmp.reserve(m_data->GetDataCount());
	yTmp.reserve(m_data->GetDataCount());
	for (size_t i = 0, size = m_data->GetDataCount(); i < size; ++i) {
		float x = m_data->GetData(0, i);
		float y = m_data->GetData(1, i);
		if (x != FLT_MAX && y != FLT_MAX) {
			xTmp.push_back(x);
			yTmp.push_back(y);
		}
	}

	if (OCRegressionCalc* regress = dynamic_cast<OCRegressionCalc*>(m_inter.get())) {
        CheckRegress(regress, xTmp.size(), false);
	}

	// 有効なデータが足りていなければ何もしない
	if (xTmp.size() <= 2 || yTmp.size() <= 2) { return; }

	m_inter->Init((int)xTmp.size(), &xTmp[0], &yTmp[0]);
	if (!m_inter->Prepare()) {
		if (m_dispMessage && m_visible) {
			switch (m_interType)
			{
			case Interpolation::Hermite:
				{
					std::wstring msg = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_NOT_PERFORM_BETWEEN_ASSISTANT_LMEAT_CURRENT_DATA_CHANGE");
					WarningMessage(m_plot, msg.c_str());
				}
				break;
			case Interpolation::Spline:
				{
					std::wstring msg = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_NOT_PERFORM_BETWEEN_ASSISTANT_THIRD_SPLINE_CURRENT_DATA");
					WarningMessage(m_plot, msg.c_str());
				}
				break;
			}
		}
		// 失敗したら3次スプラインに変更
		SetInterpolationType(Interpolation::BSpline, false);
	}
}

void OCCurvePlot::CheckRegress(OCRegressionCalc* regress, bool dispMessage)
{
	std::vector<double> xTmp;
	for (size_t i = 0, size = m_data->GetDataCount(); i < size; ++i) {
		float x = m_data->GetData(0, i);
		float y = m_data->GetData(1, i);
		if (x != FLT_MAX && y != FLT_MAX) {
			xTmp.push_back(x);
			// 全部見る必要はないので途中で抜ける
			if (xTmp.size() > 10) { break; }
		}
	}
	CheckRegress(regress, xTmp.size(), dispMessage);
}

void OCCurvePlot::CheckRegress(OCRegressionCalc* regress, size_t dataCount, bool dispMessage)
{
	if (m_order == 1) { return; }

	if (m_regType == OCRegressionCalc::Regression && dataCount <= 3) {
        if (dispMessage) {
			std::wstring msg = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_FEW_DATA_MARKS_DEGREE_CHANGE_LINEAR_APPROXIMATON");
			WarningMessage(m_plot, msg.c_str());
        }

		regress->SetType(OCRegressionCalc::Linear);
		m_order = 2;
		m_regType = OCRegressionCalc::Linear;
	}
	else if (m_regType == OCRegressionCalc::Regression && dataCount <= (size_t)m_order + 1) {
		if (dispMessage) {
			std::wstring msg = ::GetResourceString(m_plot,"ID_SOURCE_TEXT_FEW_DATA_MARKS_DEGREE_CHANGE_DEGREE");
			WarningMessage(m_plot, msg.c_str());
        }
		m_order = (int)dataCount - 2;
		regress->SetDegree(m_order);
	}
	std::wstring formula = regress->GetFormula();
	m_plot->SendMessageW(PI_SET_FORMULA_STRINGS, reinterpret_cast<WPARAM>(&formula), 0);
}

void OCCurvePlot::CopyFrom(IPlotDrawer const* src)
{
	__super::CopyFrom(src);
	if (OCCurvePlot const* self = dynamic_cast<OCCurvePlot const*>(src)) {
		m_interType = self->m_interType;
		m_order = self->m_order;
		m_extrapolation = self->m_extrapolation;
		m_regType = self->m_regType;
		m_showFormula = self->m_showFormula;
		m_range = self->m_range;
		m_online = self->m_online;
		m_max = self->m_max;
		m_min = self->m_min;

		SetInterpolationType(m_interType, false);
	}
}

//! プロットの情報をXML形式で出力します
void OCCurvePlot::Serialize(ISerializer* ar)
{
	__super::Serialize(ar);

    // {Generated Code(by gen.rb)
    ar->Serialize(_T("InterType"), m_interType, int());
    ar->Serialize(_T("Order"), m_order, 1);
    ar->Serialize(_T("Extrapolation"), m_extrapolation, int());
    ar->Serialize(_T("RegType"), m_regType, int());
    ar->Serialize(_T("ShowFormula"), m_showFormula, bool());
	ar->Serialize(_T("MovingAverageRange"), m_range, m_range);
    // }

	m_order = std::max<int>(m_order, 1);

	if (dynamic_cast<IReadable*>(ar)) {
		m_dispMessage = false;
		SetInterpolationType(m_interType, false);
		m_dispMessage = true;
	}

	//3.02より前で作成したプロジェクト用
	if(m_interType == 3)
	{
		bool hidden = false;
		m_plot->SendMessageW(PI_SET_FORMULA_HIDDEN, reinterpret_cast<WPARAM>(&hidden), 0);
	}
	m_plot->SendMessageW(PI_SET_FORMULA_VISIBLE, reinterpret_cast<WPARAM>(&m_showFormula), 0);
}

//! サーチモードを開始します
void OCCurvePlot::BeginSearch(DrawArgs const& arg, bool interpolate, ExclusionScale exclude, OCRect const& rect)
{
	if (!interpolate) {
		__super::BeginSearch(arg, interpolate, exclude, rect);
		return;
	}

	m_searchArray.clear();

	std::vector<double> xTmp, yTmp;
	if (!InitInterpolate(m_inter.get(), xTmp, yTmp)) { return; }

	double const xMax = arg.xMax;
	double const xMin = arg.xMin;
	double const yMax = arg.yMax;
	double const yMin = arg.yMin;

	if (xTmp.size() == 2) {
		for (size_t i = 0; i < yTmp.size(); ++i) {
			double const xPos = rect.Width() * arg.XNormalize(xTmp[i]) + rect.left;
			double const yPos = rect.Height() - rect.Height() * arg.YNormalize(yTmp[i]) + rect.top;

			SearchValue sv = { OCPoint(xPos, yPos), OCPoint3D(xTmp[i], yTmp[i], -1), i };
			m_searchArray.push_back(sv);
		}
	}
	else {
		double xStart, xStop;
		GetDrawRange(arg, &xStart, &xStop, xTmp.front(), xTmp.back());

		int const count = 1000;
		double const interval = ((xStop - xStart) / count);
		for (int i = 0; i <= count; ++i) {
			double x = interval * i + xStart;
			if (x > xStop) { break; }
			double y = m_inter->Calc(x);
			if (y == DBL_MAX) { continue; }

			if (exclude & ExcludeX) {
				if (x > xMax || x < xMin) { continue; }
			}
			if (exclude & ExcludeY) {
				if (y > yMax || y < yMin) { continue; }
			}

			double const xPos = rect.Width() * arg.XNormalize(x) + rect.left;
			double const yPos = rect.Height() - rect.Height() * arg.YNormalize(y) + rect.top;

			SearchValue sv = { OCPoint(xPos, yPos), OCPoint3D(x, y, -1), m_searchArray.size() };
			m_searchArray.push_back(sv);
		}
	}
}

//!	X値を元に補間データのある座標と値を取得する
/*!
	@param[in]	rect	プロットの矩形座標
	@param[in]	target	補間対象の座標
	@param[out]	point	補間して得られた座標
	@param[out]	value	補間して得られた値

	折れ線図と違いひとつのX値に複数のYの値を持つことはないので、target.yは使用しない
*/
void OCCurvePlot::GetPointByValue(OCRect const& rect, OCPoint const& target, OCPoint* point, OCPoint3D* value)
{
	float const xMax = m_data->GetMaximum(0);
	float const xMin = m_data->GetMinimum(0);
	float const yMax = m_data->GetMaximum(1);
	float const yMin = m_data->GetMinimum(1);

	if (!InitInterpolate(m_inter.get())) { return; }

	double const yValue = m_inter->Calc(target.x);
	if (yValue == DBL_MAX) {
		return;
	}

	double const xPos = (target.x - xMin) / (xMax - xMin) * rect.Width() + rect.left;
	double const yPos = rect.Height() - rect.Height() * (yValue - yMin) / (yMax - yMin) + rect.top;

	point->SetPoint(xPos, yPos);
	value->x = target.x;
	value->y = yValue;
}