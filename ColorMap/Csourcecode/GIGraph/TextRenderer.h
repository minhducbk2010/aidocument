#pragma once

#include <boost/noncopyable.hpp>
#include "TexVertex.h"
#include "TextRenderAlign.h"
#include "StringUtil.hpp"
#include "SIUnitString.h"
#include "AxisEnum.h"

class TextInfo;

struct TextAlign
{
	enum type
	{
		Left,
		Center,
		Right,
	};
};

class TextRenderer : public boost::noncopyable
{
public:
	~TextRenderer();

	static TextRenderer* GetInstance();

	BOOL GetUseSIUnit();
	void SetUseSIUnit(BOOL value);

	void Enabled(bool enable) { m_enable = enable; }
	void Dispose();
	void SetFont(HDC device, const wchar_t *name, int height);
	void Render(Vertex pos, const wchar_t *text, const SIZE *size, TextRenderAlign align);
	void Render(Vertex pos, double value, const SIZE *size, TextRenderAlign align);
	TextInfo *CreateText(HDC device, const wchar_t *text);

	SIZE GetValueTextSize(HDC device, double value, LOGFONT const& lf) const;
	SIZE GetTextSize(HDC device, TCHAR const* text, LOGFONT const& lf) const;
	SIZE GetTextSize(TCHAR const* text) const;

	void AddText(std::wstring const& text, int x, int y, double angle, COLORREF color, TextAlign::type align);
	void AddText(std::wstring const& text, int x, int y, double angle, COLORREF color, TextAlign::type align, LOGFONT const& lf, bool vcenter = false);
	void AddText(double value, double x, double y, double angle, COLORREF color, TextAlign::type align);
	void AddText(double value, double x, double y, double angle, COLORREF color, TextAlign::type align, LOGFONT const& lf);
	void AddText(double value, double x, double y, double z, double angle, COLORREF color, TextAlign::type align, LOGFONT const& lf,
		StringFormat fmt, bool useComma);
	void AddTextFormat(double value, double x, double y, double z,
		COLORREF color, TextAlign::type align, LOGFONT const& lf,
		int figure, StringFormat fmt, bool useComma, 
		SIUnitLabel si, ScaleType scale, bool vcenter = true);
	void DrawGDIText(HDC dc);

	void LatestTextDoVerticalCenter();

private:
	TextRenderer();

	bool m_enable;
	HDC device;
	GLuint listBase;
	GLsizei listCount;
	LOGFONTW logFont;
	BOOL useSiUnit;

	struct DrawableText
	{
		std::wstring text;
		Gdiplus::REAL x, y;
		double angle;
		LOGFONT lf;
		COLORREF color;
		TextAlign::type align;
		bool verticalCenter;
		DrawableText(std::wstring const& text, Gdiplus::REAL x, Gdiplus::REAL y, double angle, LOGFONT lf, COLORREF color, TextAlign::type align)
			: text(text), x(x), y(y), angle(angle), lf(lf), color(color), align(align), verticalCenter(false)
		{}
	};
	std::vector<DrawableText> m_cache;
};

#define textRenderer TextRenderer::GetInstance()