#pragma once
#include "TexVertex.h"

class GLUquadric;

class GuideArrow
{
public:
	GuideArrow(GLdouble fromX, GLdouble fromY, GLdouble fromZ, GLdouble toX,
		GLdouble toY, GLdouble toZ, GLdouble r, GLdouble g, GLdouble b);
	virtual ~GuideArrow();

	void Modeling(GLdouble angle, GLdouble x, GLdouble y, GLdouble z);
	void Render(const wchar_t *name);

private:
	static GLUquadric *quad;
	static int reference;

	GLdouble fX, fY, fZ;
	GLdouble tX, tY, tZ;
	GLdouble r, g, b;
	GLdouble angle, rX, rY, rZ;
};
