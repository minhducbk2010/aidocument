// stdafx.h : 標準のシステム インクルード ファイルのインクルード ファイル、または
// 参照回数が多く、かつあまり変更されない、プロジェクト専用のインクルード ファイル
// を記述します。
//

#pragma once

#ifndef _WIN32_WINNT		// Windows XP 以降のバージョンに固有の機能の使用を許可します。                   
#define _WIN32_WINNT 0x0501	// これを Windows の他のバージョン向けに適切な値に変更してください。
#endif

// ATLを使うための定義
#undef __IStream_INTERFACE_DEFINED__
#define _ATL_NO_AUTOMATIC_NAMESPACE
#define _WTL_NO_AUTOMATIC_NAMESPACE

// CRT
#include <stdio.h>
#include <cassert>
#include <tchar.h>
#define _USE_MATH_DEFINES
#include <math.h>

// STL
#include <vector>
#include <deque>
#include <stack>
#include <map>
#include <string>
#include <algorithm>
#include <complex>

// Boost
#include <boost/scoped_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/format.hpp>
#include <boost/foreach.hpp>
#include <boost/range.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/numeric.hpp>
#include <boost/signals2.hpp>
#include <boost/noncopyable.hpp>
#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/ptr_container/ptr_deque.hpp>
#pragma warning(push)
#pragma warning(disable:4819)
#pragma warning(disable:4127)
#include <boost/lexical_cast.hpp>
#pragma warning(pop)

// windows
#include <windows.h>
#include <gdiplus.h>

// OpenGL
#include <gl.h>
#include <glu.h>

#include "utility.h"
#include "common.h"

//#undef min
//#undef max

//TODO: あとで統合
// メイン側のresource.h内の同名シンボルと同じ値であること
#define ID_PLOT_SCATTER                 62771
#define ID_PLOT_BAR                     62772
#define ID_PLOT_CONOTUR                 62773
#define ID_OPT_WIREFRAME                62774
#define ID_OPT_PAINT                    62775
#define ID_OPT_SLICE_LINE               62776
#define ID_CMD_SAVE                     62778
#define ID_CMD_COPY                     62779
#define ID_CMD_PRINT                    62780
#define ID_CMD_DIALOG                   62781
#define ID_CMD_RESET                    62782
#define ID_CMD_TURN                     62783
#define ID_CMD_RESIZE                   62784
#define ID_CMD_MOVE                     62785
#define ID_CMD_XY                       62786
#define ID_CMD_YZ                       62787
#define ID_CMD_ZX                       62788
#define ID_CMD_HELP                     62789
#define ID_CMD_COLORBAR                 62790
#define ID_CMD_HIDELINE                 62791
#define ID_CMD_COLOR                    62792
#define ID_CMD_MARKER                   62793
#define ID_CMD_LIGHT                    62794
#define ID_CMD_LINE                     62795
#define ID_CMD_CURVE                    62796
#define ID_PLOT_VECTOR                  62797
#define ID_CMD_PROJ_XY					62798
#define ID_CMD_PROJ_YZ					62799
#define ID_CMD_PROJ_ZX					62800
#define ID_CMD_CURSOR                   62801
#define ID_CMD_PROJ_COLOR               62802
#define ID_CMD_PROJ_PAINT               62803
#define ID_CMD_PROJ_SLICELINE           62804
#define ID_CMD_PLOT_CHANGE              63823

extern HWND g_applicationWindow;
extern BOOL g_isLibMode;