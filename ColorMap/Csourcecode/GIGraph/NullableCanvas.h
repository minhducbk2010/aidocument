/*!	@file
	@berief	何もしないキャンバスの実装
*/
#pragma once

#include "ICanvas.h"

#pragma warning(disable:4100)

class OCNullableCanvas : public ICanvas
{
public:
	OCNullableCanvas() {}
	virtual ~OCNullableCanvas() {}

	virtual void SetOffset(double mmx, double mmY) {}
	virtual double GetOffsetX() const {return 0;}
	virtual double GetOffsetY() const {return 0;}
	virtual void Attach(HDC hDC) {}
	virtual void Attach(Gdiplus::Metafile*) {}
	virtual void Attach(HBITMAP hBitmap) {}
	virtual void Detach() {}
	virtual HDC GetHDC() {return 0;}
	virtual void ReleaseHDC(HDC) {}
	virtual bool IsPrinting() const {return false;}
	virtual void SetPrinting(bool printing) {}
	virtual void SetTextAntialiasing(bool enable) {enable;}
	virtual void SetAntialiasing(bool enable) {enable;}
	virtual double GetZoomRatio() const {return 1;}
	virtual void SetZoomRatio(double ratio) {}
	virtual unsigned int Save() const {return 0;}
	virtual void Restore(unsigned int state) {}
	virtual void SetRoundMode(bool round) {}
	virtual void Rotate(int angle, double x, double y) {}
	virtual void Translate(double xOffset, double yOffset) {}
	virtual bool DrawRectangle(OCRect const& rect) {return true;}
	virtual bool DrawRectangle(double x, double y, double width, double height) {return true;}
	virtual bool FillRectangle(OCRect const& rect) {return true;}
	virtual bool DrawRoundRectangle(OCRect const& rect, float radius) {return true;}
	virtual bool DrawRectangleShadow(OCRect const& rect, double offset, double blur, short angle, COLORREF color) const {return true;}
	virtual bool DrawEllipse(OCRect const& rect) {return true;}
	virtual bool DrawEllipse(double x, double y, double width, double height) {return true;}
	virtual bool FillEllipse(OCRect const& rect) {return true;}
	virtual bool DrawRoundBalloon(const OCPoint* points, float radius, int type) const {return true;}
	virtual bool DrawEllipseBalloon(OCRect const& rect, OCPoint const& pitch) const {return true;}
	virtual bool GradientFill(OCRect const& rect, COLORREF begin, COLORREF end, GRADATION_KIND kind, GRADATION_TYPE direction) const {return true;}
	virtual bool DrawLine(OCPoint const& p1, OCPoint const& p2) const {return true;}
	virtual bool DrawLine(double x1, double y1, double x2, double y2) const {return true;}
	virtual bool DrawLines(OCPoint const* points, size_t count) const {return true;}
	virtual bool DrawPolygon(OCPoint const* points, size_t count) const {return true;}
	virtual bool FillPolygon(OCPoint const* points, size_t count) const {return true;}
	virtual bool DrawCurve(OCPoint const* points, size_t count) {return true;}
	virtual bool FillCurve(OCPoint const* points, size_t count) {return true;}
	virtual OCPoint MeasureString(wchar_t const* text, long length) {return OCPoint(1, 1);}
	virtual bool DrawString(wchar_t const* text, long length, OCPoint const& point) {return true;}
	virtual bool DrawString(wchar_t const* text, long length, OCRect const& rect, int flag = 0) {return true;}
	virtual bool DrawBitmap(HBITMAP hBitmap, double x, double y, double width, double height, short alpha) const {return true;}
	virtual bool DrawTransparentBitmap(HBITMAP hBitmap, double x, double y, double width, double height, short alpha, COLORREF color) const {return true;}
	virtual bool DrawBitmapFlip(HBITMAP hBitmap, double x, double y, double width, double height, short alpha, bool xFlip, bool yFlip) const {return true;}
	virtual bool DrawTransparentBitmap(HBITMAP hBitmap, double x, double y, COLORREF color) const {return true;}
	virtual bool DrawMetafile(HENHMETAFILE meta, double x, double y, double width, double height, short alpha, bool xFlip, bool yFlip) const {return true;}
	virtual bool FillRubberBand(OCRect const& rect) {return true;}
	virtual bool DrawRubberBand(OCRect const& rect) {return true;}
	virtual bool DrawRubberBandCurve(OCPoint const* points, size_t count) {return true;}
	virtual bool DrawRubberLine(OCPoint const& p1, OCPoint const& p2) const {return true;}
	virtual bool DrawRubberLines(OCPoint const* points, size_t count) const {return true;}
	virtual bool DrawRubberPolygon(OCPoint const* points, size_t count) {return true;}
	virtual bool DrawRubberEllipseBalloon(OCRect const& rect, OCPoint const& pitch) {return true;}
	virtual bool DrawResizeHandle(OCPoint const& position, bool locked) const {return true;}
	virtual bool DrawResizeBalloonHandle(OCPoint const& position, bool locked) const {return true;}

	// bFill : 中を塗りつぶすかどうか(◎対策)
	virtual bool DrawMarkerCircle(double x, double y, double size, bool bFill) {return true;}
	virtual bool DrawMarkerRectangle(double x, double y, double size) {return true;}
	// flag : 0 (Diamond), 1 (TriangleUp), 2 (TriangleDown)
	virtual bool DrawMarkerDiamondTriangle(double x, double y, double size, int flag) {return true;}
	// flag : 0 (Cross), 1 (Plus)
	virtual bool DrawMarkerCrossPlus(double x, double y, double size, int flag) {return true;}

	virtual void SetClipping(OCRect const& rect) {}
	virtual void SetClipping(OCPoint const* points, size_t count) {}
	virtual void AddClipping(OCRect const& rect) {}
	virtual void SetExcludeClippingEllipse(OCRect const& rect) {}
	virtual void SetExcludeClipping(OCPoint const* points, size_t count) {}
	virtual void ResetClipping() {}
	virtual void SetPen(COLORREF color, float width, int style, int roundType) {}
	virtual void SetPenColor(COLORREF color) {}
	virtual void SetPenWidth(float width) {}
	virtual void SetPenStyle(long style) {}
	virtual void ReloadCustomLines() {}
	virtual void SetCustomLineDirectory(TCHAR const*) {}
	virtual void SetCustomLineStyle(float*, unsigned long) {}
	virtual long GetCustomStyleCount() const {return 0;}
	virtual wchar_t const* GetCustomStyleName(long) const {return 0;}
	virtual long GetUserCustomStyleCount() const {return 0;}
	virtual bool SaveCustomLines(TCHAR const* directory) const {return false;}
	virtual void SetBackground(COLORREF color, FILLSTYLE style, short transparent) {}
	virtual void SetBackgroundColor(COLORREF color) {}
	virtual void SetBackgroundStyle(FILLSTYLE style) {}
	virtual void SetTransparent(short percent) {}
	virtual void SetPenTransparent(short percent) {}
	virtual void CreateGradationBrush(OCRect const& rect, double blur, COLORREF color) {}
	virtual void SetGradationStyle(GRADATION_TYPE direction, COLORREF begin, COLORREF end) {}
	virtual void SetFont(wchar_t const* font, long size, long angle) {}
	virtual void SetFont(LOGFONT const* lf) {}
	virtual void SetFont2(LOGFONT const* lf) {}
	virtual void SetFontColor(COLORREF color) {}
	virtual long GetFontSize() const {return 0;}
	virtual void CreatePath() {}
	virtual void ResetPath() {}
	virtual void CreateOffScreen(int width, int height) {}
	virtual bool HasOffScreen() const {return false;}
	virtual void ReleaseOffScreen() {}
	virtual void BitBlt(HDC hDC, int x, int y) {}
	virtual bool GetThumbnail(HDC dc, int width, int height) const{return false;}
	virtual bool LPtoDP(OCPoint* point, size_t count) {return true;}
	virtual bool LPtoDP(OCRect* rect) {return true;}
	virtual double GetDpi() const { return 1; }
	virtual void InitGraphics(Gdiplus::Graphics* g) const {g;}
};
