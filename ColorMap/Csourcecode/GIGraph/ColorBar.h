#pragma once

class Axis;
class GLBuffer;
struct TexVertex;
class ColorTexture;

class ColorBar
{
public:
	ColorBar(Axis* axis);
	virtual ~ColorBar();

	void AttachAxis(Axis* axis);
	void SetLabelVisible(bool visible) { m_labelVisible = visible; }
	void SetDimension(int dim) { m_dimension = dim; }
	void Render(ColorTexture* texture, std::wstring const& name, bool sub = false);

private:
	Axis* m_axis;
	bool m_labelVisible;
	int m_dimension;
};
