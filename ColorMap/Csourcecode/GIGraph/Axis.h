#pragma once

#include <string>
#include <boost/signals2.hpp>
#include "TexVertex.h"
#include "AxisEnum.h"
#include "TextRenderer.h"
#include "IAxis.h"
#include "IProperty.h"
#include "StringUtil.hpp"
#include "SIUnitString.h"

struct GridInfo
{
	double Value;
	double Coordinate;
	//SIZE Size;
	BOOL ShowValue;
};

class Graph3DBase;
class TextInfo;
class IPropertyGroup;
class ISerializer;

class Axis : public IAxis, public IPropertySetter
{
public:
	Axis(Graph3DBase *owner);
	virtual ~Axis();

	virtual int GetDimension() const = 0;

	bool IsVisible() const { return m_visible; }
	void SetVisible(bool visible) { m_visible = visible; }

	bool IsVisibleLabel() const { return m_labelVisible; }

	virtual TCHAR const* GetName() const;

	bool IsAutoLabel() const { return m_autoLabel; }
	virtual TCHAR const* GetLabel()	const { return m_label.c_str(); }
	virtual IRichText* GetAxisLabel() const { return nullptr; }
	void SetLabel(std::wstring const& value);
	void ResetLabel();

	void SetSIUnit(SIUnitLabel const& si);
	SIUnitLabel GetSIUnit() const { return m_SIUnit; }
	
	double GetLinearMaximum() const { return m_maximum; }
	double GetLinearMinimum() const { return m_minimum; }
	virtual double GetMaximum()	const;
	virtual double GetMinimum() const;
	virtual double GetInterval() const;
	virtual double GetSubInterval() const;
	virtual bool GetSecondUse() const { return m_useSecond; }
	virtual double GetSecondValue() const;
	virtual void SetMinMax(double min, double max);
	virtual void SetSecondUse(bool use);
	virtual int SetSecondValue(double second);
	virtual int SetInterval(double interval);
	virtual int SetSubInterval(double subInterval);
	void SetAutoScale(bool autoScale) { m_autoScale = autoScale; }

	virtual double GetDBStd() const { return m_dbStd; }
	virtual double GetDBExp() const { return m_dbExp; }

	virtual ScaleType GetScaleType() const { return m_scaleType; }
	StringFormat GetTickFormat() const { return m_scaleDispType; }
	int GetScaleFigure() const { return m_scaleFigure; }
	bool UseComma() const { return m_useComma; }

	double GetSize() const { return m_length; }
	virtual double GetEdge() const = 0;
	double GetFullSize() const { return m_length * MarginRate; }

	virtual bool GetGridVisible() const { return m_drawGrid; }
	virtual COLORREF GetGridColor() const { return m_gridColor; }
	virtual int GetGridStyle() const { return m_gridStyle; }
	size_t GetGridCount() const { return m_gridInfo.size(); }
	virtual std::vector<Vertex> GetGridPos(double coordinate, bool reverse) const = 0;
	GridInfo const& GetGridInfo(size_t index) const { return m_gridInfo[index]; }

	COLORREF GetSymbolColor() const;
	
	bool IsVisibleTickLabel() const { return m_drawTickLabel; }
	virtual bool IsSubTickVisible() const { return m_drawSubTick; }
	virtual COLORREF GetTickLabelColor() const { return m_tickLabelFontColor; }

	void CalcAutoScale(double minimum, double maximum);

	void SetScaleParam(double maximum, double minimum, ScaleType scale, const double *second = NULL);
	double ValueToCoordinate(double value) const;
	virtual double ValueToPosition(double value) const = 0;
	double CoordinateToValue(double coordinate);
	double GetNormalize(double value) const;
	double ValueToCoordinateRaw(double value) const;
	virtual double ValueToPositionRaw(double value) const = 0;
	double CoordinateToValueRaw(double coordinate) const;
	double Expand(double norm) const;
	double GetTextureValue(double value) const;
	void SetScaleVector(double *scale, int scaleLen);

	LOGFONT const& GetLabelFont() const { return m_labelFont; }
	COLORREF GetFontColor() const { return m_labelFontColor; }
	LOGFONT const& GetTickLabelFont() const { return m_tickLabelFont; }

	virtual void AutoScale() { AutoScaleChanged(this); }
	virtual void AutoLabel() { AutoLabelChanged(this); }

	void Modeling();
	void Render();
	virtual void RenderLabel() const = 0;

	Graph3DBase* GetOwner() const { return owner; }

// Override IAxis nullable...
	virtual OCRect const& GetRect() const;
	virtual bool IsBuildUp() const			{ return false; }
	virtual bool IsOffset() const			{ return false; }
	virtual bool IsReverse() const			{ return false; }
	virtual BOOL IsLabelEditing() const		{ return FALSE; }

	double ToLinear(double value) const;

#if 1
// O-Chart
	void AddProperty(IPropertyGroup*  parent, int mask, bool locked) const;
	virtual bool SetProperty(long id, LPARAM value, LPARAM* source);
	virtual bool GetProperty(long id, LPARAM* ret) const;
	void Serialize(ISerializer* ar);
#endif

	boost::signals2::signal<void(Axis*)> AutoScaleChanged;
	boost::signals2::signal<void(Axis*)> AutoLabelChanged;
	boost::signals2::signal<void(Axis*)> ScaleChanged;

private:
	void OnAutoScaleChanged();
	void OnAutoLabelChanged();
	void OnScaleChanged();

	void AdjustScaleInterval();

protected:
	virtual bool IsVisibleTicks() const { return false; }
	void RenderTickLabels();
	virtual void RenderBorderLine() const;
	virtual std::vector<Vertex> GetTickPos(double coordinate, double length) const = 0;
	virtual TextAlign::type GetTickLabelAlign() const = 0;
	virtual Vertex GetTickPosition(size_t index) const = 0;

	virtual Axis* GetBorderAxis() const = 0;
	double GetPropertyMax() const;
	double GetPropertyMin() const;
	double ConvertValue(ScaleType type, double value) const;
	double LinearToDecibel(double linear) const;
	double DecibelToLinear(double decibel) const;

	void GetLogScale();
	double GetScaleTick(double span) const;
	void CreateGrids();
	void CreateLinearGrids();
	void CreateLogGrids();
	void InsertSubTick(double current, double next, double subInterval);
	GridInfo AddGrid(double coordinate, double value, bool showValue);

	double GetSecondValueForUse() const;

protected:
	static const double MarginRate;

	std::vector<Vertex> m_grids;
	std::vector<Vertex> m_ticks;
	std::vector<Vertex> m_subTicks;

	Graph3DBase* owner;
	bool m_visible;
	ScaleType m_scaleType;
	bool m_autoScale;
	bool m_scaleFitting;
	double m_maximum;				//!< 最大値(リニア値)
	double m_minimum;				//!< 最小値(リニア値)
	bool m_useSecond;				//!< セカンド値を使用するか
	double m_second;				//!< セカンド値（スケール設定に応じた単位の値が入る）
	double m_interval;				//!< スケール間隔（セカンド値と同様）
	double m_subInterval;			//!< 補助目盛りの間隔（セカンド値と同様）
	double m_dbExp;
	double m_dbStd;
	bool m_drawTickLabel;
	bool m_drawSubTick;
	TickType m_tickType;
	double m_tickSize;
	StringFormat m_scaleDispType;
	int m_scaleFigure;
	bool m_useComma;

	bool m_drawBorder;
	bool m_drawGrid;
	int m_gridStyle;
	COLORREF m_gridColor;
	double m_gridWidth;
	double m_length;

	std::vector<GridInfo> m_gridInfo;
	std::wstring m_label;
	COLORREF m_symbolColor;
	bool m_labelVisible;
	bool m_autoLabel;
	LOGFONT m_labelFont;
	COLORREF m_labelFontColor;
	LOGFONT m_tickLabelFont;
	COLORREF m_tickLabelFontColor;
	SIUnitLabel m_SIUnit;

	// プロパティ用
	LOGFONT mutable m_labelFontProp;
	LOGFONT mutable m_tickLabelFontProp;
};

class XAxis : public Axis
{
public:
	XAxis(Graph3DBase* owner);

	virtual bool IsVisibleTicks() const;
	virtual double ValueToPosition(double value) const;
	virtual double ValueToPositionRaw(double value) const;

protected:
	virtual int GetDimension() const { return 0; }
	virtual double GetEdge() const;
	virtual std::vector<Vertex> GetGridPos(double coordinate, bool reverse) const;
	virtual std::vector<Vertex> GetTickPos(double coordinate, double length) const;
	virtual Axis* GetBorderAxis() const;
	virtual void RenderLabel() const;
	virtual TextAlign::type GetTickLabelAlign() const;
	virtual Vertex GetTickPosition(size_t index) const;
};

class YAxis : public Axis
{
public:
	YAxis(Graph3DBase* owner);

	virtual bool IsVisibleTicks() const;
	virtual double ValueToPosition(double value) const;
	virtual double ValueToPositionRaw(double value) const;

protected:
	virtual int GetDimension() const { return 1; }
	virtual double GetEdge() const;
	virtual std::vector<Vertex> GetGridPos(double coordinate, bool reverse) const;
	virtual std::vector<Vertex> GetTickPos(double coordinate, double length) const;
	virtual Axis* GetBorderAxis() const;
	virtual void RenderLabel() const;
	virtual TextAlign::type GetTickLabelAlign() const;
	virtual Vertex GetTickPosition(size_t index) const;
};

class ZAxis : public Axis
{
public:
	ZAxis(Graph3DBase* owner);

	virtual bool IsVisibleTicks() const;
	virtual double ValueToPosition(double value) const;
	virtual double ValueToPositionRaw(double value) const;

protected:
	virtual int GetDimension() const { return 2; }
	virtual double GetEdge() const;
	virtual std::vector<Vertex> GetGridPos(double coordinate, bool reverse) const;
	virtual std::vector<Vertex> GetTickPos(double coordinate, double length) const;
	virtual Axis* GetBorderAxis() const;
	virtual void RenderLabel() const;
	virtual TextAlign::type GetTickLabelAlign() const;
	virtual Vertex GetTickPosition(size_t index) const;
};

class TAxis : public ZAxis
{
public:
	TAxis(Graph3DBase* owner) : ZAxis(owner) { SetVisible(false); m_tickLabelFontColor = 0; }

protected:
	virtual int GetDimension() const { return 3; }
};

class VxAxis : public XAxis
{
public:
	VxAxis(Graph3DBase* owner) : XAxis(owner) { SetVisible(false); }

protected:
	virtual int GetDimension() const { return 3; }
};

class VyAxis : public YAxis
{
public:
	VyAxis(Graph3DBase* owner) : YAxis(owner) { SetVisible(false); }

protected:
	virtual int GetDimension() const { return 4; }
};

class VzAxis : public ZAxis
{
public:
	VzAxis(Graph3DBase* owner) : ZAxis(owner) { SetVisible(false); }

protected:
	virtual int GetDimension() const { return 5; }
};
