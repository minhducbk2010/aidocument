/*!	@file
	コンターマップの断面
*/
#pragma once

#include <wtl/atlapp.h>
#include <wtl/atlmisc.h>

struct OCRect;
class Axis;

class Section
{
public:
	Section();
	virtual ~Section();

	void AttachAxis(Axis* xAxis, Axis* yAxis);
	Axis* GetXAxis() const { return m_xAxis; }
	Axis* GetYAxis() const { return m_yAxis; }

	void SetExtractAxis(Axis* axis);
	void Clear();
	void SetData(std::vector<OCPoint> const& data, double value);
	void SetCursorValue(double value);
	void AddData(std::vector<OCPoint> const& data);

	void Draw(HDC dc, OCRect const& rect) const;

protected:
	virtual void AdjustRect(WTL::CRect* rect) const = 0;
	virtual void DrawBackground(WTL::CDCHandle& dc, WTL::CRect const& rect) const = 0;
	virtual void DrawPlot(WTL::CDCHandle& dc, WTL::CRect const& rect) const = 0;

	int GetXPosition(WTL::CRect const& rect, double value) const;
	int GetYPosition(WTL::CRect const& rect, double value) const;

	void GetAxisLinearMinMax(Axis const* axis, double* min, double* max) const;

protected:
	std::vector<std::vector<OCPoint>> m_lines;
	double m_value;				//!< カーソル値

	Axis* m_xAxis;				//!< X軸
	Axis* m_yAxis;				//!< Y軸
	Axis* m_exAxis;				//!<
};

//! Y方向の断面図
class OCVerticalSection : public Section
{
protected:
	virtual void AdjustRect(WTL::CRect* rect) const;
	virtual void DrawBackground(WTL::CDCHandle& dc, WTL::CRect const& rect) const;
	virtual void DrawPlot(WTL::CDCHandle& dc, WTL::CRect const& rect) const;
};

//! X方向の断面図
class OCHorizontalSection : public Section
{
protected:
	virtual void AdjustRect(WTL::CRect* rect) const;
	virtual void DrawBackground(WTL::CDCHandle& dc, WTL::CRect const& rect) const;
	virtual void DrawPlot(WTL::CDCHandle& dc, WTL::CRect const& rect) const;
};