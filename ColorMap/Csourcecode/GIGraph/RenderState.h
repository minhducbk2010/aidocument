/*!	@file
*/
#pragma once

struct RenderState
{
	RenderState(double pitch, double yaw, double roll, double h, double v, double scale, bool viewColorBar)
		: m_pitch(pitch), m_yaw(yaw), m_roll(roll), m_h(h), m_v(v), m_scale(scale)
		, m_viewColorBar(viewColorBar)
		, m_binding(false)
	{
	}

	void Bind()
	{
		::glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

		::glPushMatrix();

		::glTranslated(m_h, m_v, 0.0);
		::glScaled(m_scale, m_scale, m_scale);
		if (m_viewColorBar)
		{
			//::glTranslated(-2.5, 0.0, 0.0);
		}
		::glRotated(m_pitch,	1, 0, 0);
		::glRotated(m_yaw,		0, 1, 0);
		::glRotated(m_roll,		0, 0, 1);

		m_binding = true;
	}

	void Unbind()
	{
		::glPopMatrix();
		m_binding = false;
	}

	~RenderState()
	{
		if (m_binding) {
			Unbind();
		}
	}

private:
	double m_pitch, m_yaw, m_roll;
	double m_h, m_v;
	double m_scale;
	bool m_viewColorBar;

	bool m_binding;
};