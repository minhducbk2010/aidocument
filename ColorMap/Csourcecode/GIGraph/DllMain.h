/*!	@file
*/
#pragma once

typedef void* GlHandle;

class IShape;
class IMessageReceiver;

#define DLLEXPORT __declspec(dllexport)

enum GraphOperationMode
{
	Rotate,
	Zoom,
	Move,
	Cursor,
};

enum SectionKind
{
	Hidden		= 0,
	Vertical	= 0x01,
	Horizontal	= 0x02,
	Both		= Vertical | Horizontal,
};

extern "C"
{
	// コールバック関数(イベント)
	typedef void (__stdcall * OnMarkerClick)(long plotIndex, long dataIndex);
	typedef void (__stdcall * OnMarkerValueChange)(long plotIndex, long dataIndex, double value);
	typedef void (__stdcall * OnRotate)(float xAngle, float yAngle, float zAngle);

	// グラフに対する操作
	DLLEXPORT GlHandle WINAPI CreateWaterfall(HWND window);
	DLLEXPORT GlHandle WINAPI CreateSolidGraph(HWND window);
	DLLEXPORT GlHandle WINAPI CreateGraph(HWND window);
	DLLEXPORT BOOL WINAPI DeleteGraph(void* handle);
	DLLEXPORT BOOL WINAPI ClearPlots(void* handle);
	DLLEXPORT BOOL WINAPI UsePreviewImage(void* handle, BOOL use);
	DLLEXPORT BOOL WINAPI SetSize(void* handle, long width, long height);
	DLLEXPORT BOOL WINAPI Activate(void* handle);
	DLLEXPORT BOOL WINAPI Deactivate(void* handle);
	DLLEXPORT BOOL WINAPI CopyToImage(void* handle);
	DLLEXPORT BOOL WINAPI SetBoundaryData(void* handle, double* x, double* y, int length);
	DLLEXPORT BOOL WINAPI IsBoundaryEnabled(void* handle, BOOL* enable);
	DLLEXPORT BOOL WINAPI SetDimension(void* handle, long dimension);
	DLLEXPORT BOOL WINAPI VisibleMarker(void* handle, BOOL visible);
	DLLEXPORT BOOL WINAPI SetOperationMode(void* handle, GraphOperationMode mode);
	DLLEXPORT BOOL WINAPI SetColorBarVisible(void* handle, BOOL visible);
	DLLEXPORT BOOL WINAPI GetColorBarVisible(void* handle, BOOL* visible);
	DLLEXPORT BOOL WINAPI SetAxisGuideVisible(void* handle, BOOL visible);
	DLLEXPORT BOOL WINAPI GetAxisGuideVisible(void* handle, BOOL* visible);
	//DLLEXPORT BOOL WINAPI SetZoomRatio(void* handle, double ratio); // unused
	DLLEXPORT BOOL WINAPI SetMarkerColor(void* handle, COLORREF color);
	DLLEXPORT BOOL WINAPI GetMarkerColor(void* handle, COLORREF* color);
	DLLEXPORT BOOL WINAPI SetSectionMode(void* handle, SectionKind mode);
	DLLEXPORT BOOL WINAPI VisibleSectionButton(void* handle, BOOL visible);
	DLLEXPORT BOOL WINAPI DoCommand(void* handle, ULONG id);
	DLLEXPORT BOOL WINAPI GetState(void* handle, ULONG id, BOOL* enable, BOOL* checked);
	DLLEXPORT BOOL WINAPI EnableFillBackground(void* handle, BOOL enable);
	DLLEXPORT BOOL WINAPI SetCursorPosition(void* handle, long cursorX, long cursorY);
	DLLEXPORT BOOL WINAPI GetCursorPosition(void* handle, long* cursorX, long* cursorY);
	DLLEXPORT BOOL WINAPI SetAngle(void* handle, float x, float y, float z);
	DLLEXPORT BOOL WINAPI GetAngle(void* handle, float* x, float* y, float* z);
	DLLEXPORT BOOL WINAPI Translate(void* handle, double x, double y);
	DLLEXPORT BOOL WINAPI GetPosition(void* handle, double* x, double* y);
	DLLEXPORT BOOL WINAPI Scaling(void* handle, double scale);
	DLLEXPORT BOOL WINAPI GetScale(void* handle, double* scale);
	DLLEXPORT BOOL WINAPI EnableLeftClickSeleft(void* handle, BOOL enable);

	DLLEXPORT BOOL WINAPI Capture(void* handle, HDC dc, int width, int height);

	DLLEXPORT BOOL WINAPI ClearLockPoints(void* handle);
	DLLEXPORT BOOL WINAPI AddLockPoints(void* handle, long index);

	// プロットの操作
#ifdef GLGRAPHLIB
	DLLEXPORT BOOL WINAPI SetData(void* handle, long index, long dataCount, double const* xData, double const* yData, double const* zData);
#else
	DLLEXPORT BOOL WINAPI SetData(void* handle, long index, long dataCount, float const* xData, float const* yData, float const* zData);
#endif
	DLLEXPORT BOOL WINAPI SetData4D(void* handle, long index, long dataCount, float const* xData, float const* yData, float const* zData, float const* tData);
	DLLEXPORT BOOL WINAPI SetZValue(void* handle, long plotIndex, long dataIndex, double value);
	DLLEXPORT BOOL WINAPI SetUserData(void* handle, long index, long xLength, long yLength, double const* xData, double const* yData, double const* zData);
	DLLEXPORT BOOL WINAPI SetEffectiveData(void* handle, long index, BOOL* effective, long count);
	DLLEXPORT BOOL WINAPI SetPinBottom(void* handle, long index, float const* x, float const* y, float const* z, long length);
	DLLEXPORT BOOL WINAPI SetDataName(void* handle, long index, TCHAR const* xName, TCHAR const* yName, TCHAR const* zName);
	DLLEXPORT BOOL WINAPI SetDataUnit(void* handle, long index, TCHAR const* xName, TCHAR const* yName, TCHAR const* zName);
	DLLEXPORT BOOL WINAPI Refresh(void* handle, long index);
	DLLEXPORT BOOL WINAPI AddPlot(void* handle, long kind, long* index);
	DLLEXPORT BOOL WINAPI RemovePlot(void* handle, long index);
	DLLEXPORT BOOL WINAPI SetPlotKind(void* handle, long index, long kind);
	DLLEXPORT BOOL WINAPI IsPlotColoring(void* handle, long index, BOOL* enable);
	DLLEXPORT BOOL WINAPI EnablePlotColor(void* handle, long index, BOOL enable);
	DLLEXPORT BOOL WINAPI SetPlotColor(void* handle, long index, COLORREF color);
	DLLEXPORT BOOL WINAPI IsPlotVisible(void* handle, long index);
	DLLEXPORT BOOL WINAPI SetPlotVisible(void* handle, long index, long kind, BOOL visible);
	DLLEXPORT BOOL WINAPI EnableHideLine(void* handle, long index, BOOL enable);
	DLLEXPORT BOOL WINAPI GetHideLine(void* handle, long index, BOOL* hideLine);
	DLLEXPORT BOOL WINAPI EnablePlotPaint(void* handle, long index, BOOL enable);
	DLLEXPORT BOOL WINAPI GetPlotPaint(void* handle, long index, BOOL* enable);
	DLLEXPORT BOOL WINAPI EnableWireFrame(void* handle, long index, BOOL enable);
	DLLEXPORT BOOL WINAPI GetWireFrame(void* handle, long index, BOOL* enable);
	DLLEXPORT BOOL WINAPI EnableSliceLine(void* handle, long index, BOOL enable);
	DLLEXPORT BOOL WINAPI GetSliceLine(void* handle, long index, BOOL* enable);
	DLLEXPORT BOOL WINAPI EnableClipping(void* handle, long index, BOOL enable);
	DLLEXPORT BOOL WINAPI GetClipping(void* handle, long index, BOOL* enable);
	DLLEXPORT BOOL WINAPI GetPinned(void* handle, long index, BOOL* enable);
	DLLEXPORT BOOL WINAPI EnablePinned(void* handle, long index, BOOL enable);
    DLLEXPORT BOOL WINAPI SetUnselectableMarker(void* handle, long index, BOOL enable);
    DLLEXPORT BOOL WINAPI GetUnselectableMarker(void* handle, long index, BOOL* enable);
    DLLEXPORT BOOL WINAPI SetMarkerSelectEnabled(void* handle, long index, BOOL enable);
    DLLEXPORT BOOL WINAPI GetMarkerSelectEnabled(void* handle, long index, BOOL* enable);
    DLLEXPORT BOOL WINAPI SetSelectedMarkerIndex(void* handle, long plotIndex, long markerIndex);
    DLLEXPORT BOOL WINAPI GetSelectedMarkerIndex(void* handle, long plotIndex, long* markerIndex);
	DLLEXPORT BOOL WINAPI SetSelectedMarkerColor(void* handle, long index, COLORREF color);
    DLLEXPORT BOOL WINAPI GetSelectedMarkerColor(void* handle, long index, COLORREF* color);
	DLLEXPORT BOOL WINAPI SetMarkerSize(void* handle, long index, double size);
	DLLEXPORT BOOL WINAPI GetMarkerSize(void* handle, long index, double* size);
	DLLEXPORT BOOL WINAPI EnableMarkerDrag(void* handle, long index, BOOL enable);
	DLLEXPORT BOOL WINAPI SetContourType(void* handle, long index, long type);

	// 軸の操作
	DLLEXPORT BOOL WINAPI GetAxis(void* handle, int dimension, int* axis);
	DLLEXPORT BOOL WINAPI IsAutoScale(void* axisHandle, BOOL* autoScale);
	DLLEXPORT BOOL WINAPI SetAutoScale(void* axisHandle, BOOL autoScale);
	DLLEXPORT BOOL WINAPI GetMinimum(void* axisHandle, double* minimum);
	DLLEXPORT BOOL WINAPI GetMaximum(void* axisHandle, double* maximum);
	DLLEXPORT BOOL WINAPI GetScaleInterval(void* axisHandle, double* scaleInterval);
	DLLEXPORT BOOL WINAPI GetSecondValue(void* axisHandle, double* secondValue);
	DLLEXPORT BOOL WINAPI SetMinimum(void* axisHandle, double minimum);
	DLLEXPORT BOOL WINAPI SetMaximum(void* axisHandle, double maximum);
	DLLEXPORT BOOL WINAPI SetScaleInterval(void* axisHandle, double scaleInterval);
	DLLEXPORT BOOL WINAPI SetSecondValue(void* axisHandle, double secondValue);
	DLLEXPORT BOOL WINAPI SetMinMax(void* axisHandle, double min, double max);
	DLLEXPORT BOOL WINAPI GetAxisLabel(void* axisHandle, wchar_t* name, long length);
	DLLEXPORT BOOL WINAPI SetAxisLabel(void* axisHandle, wchar_t const* name);

	// イベントハンドラの設定
	DLLEXPORT BOOL WINAPI SetMarkerClickEvent(void* handle, OnMarkerClick click);
	DLLEXPORT BOOL WINAPI SetMarkerValueChangeEvent(void* handle, OnMarkerValueChange callback);
	DLLEXPORT BOOL WINAPI SetRotateEvent(void* handle, OnRotate callback);

#if 1
	// O-Chart用API

	DLLEXPORT IShape* WINAPI CreateSolidGraphInstance(HWND wnd, IMessageReceiver* owner);
	DLLEXPORT IShape* WINAPI CreateWaterfallInstance(HWND wnd, IMessageReceiver* owner);
	DLLEXPORT void WINAPI ReleaseInstance(IShape* ptr);

#endif
}
