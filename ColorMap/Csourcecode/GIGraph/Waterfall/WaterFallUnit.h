#pragma once

#include "IGraphUnit.h"

class GLBuffer;
class ColorTexture;
struct TexVertexF;

class WaterFallUnit : public IWaterFallUnit
{
public:
	WaterFallUnit();
	virtual ~WaterFallUnit();

	void InitializeData(int xCount, int zLines, Graph3DBase *base);
	virtual void ResetLines() {}
	BOOL PushLine(float z, float* x, float* y);
	void PushLines(float *z, float** x, float** y, int lineCount);
	void ModelingCursorLines(int index, int line);
	void ResetCursor();
	void Render() const;
	void RenderWireframeXLines();
	void RenderWireframeZLines();
	void RenderCursorLines();
	virtual void GetIndexLine(double x, double z, int* index, int* line);
	virtual void GetCursorLine(int* index, int* line) const;
	void SetWireframeColor(COLORREF color);
	virtual void SetCompressInterval(int compressInterval);
	virtual int GetCompressInterval() const { return m_compressInterval; }
	virtual int GetLineCount() const { return zLines; }

	virtual int GetXLineCount() const { return xCount; }
	virtual int GetZLineCount() const { return zLines; }

	virtual void Serialize(ISerializer* ar);

protected:
	virtual void Initialize(int xCount, int zLines) {xCount, zLines;}
	virtual void DoCursorChanged()const{}
	virtual void GetCursorValues(std::vector<OCPoint>* xLines, std::vector<OCPoint>* yLines) const;

	void DrawXLine(int y, bool cursor = false) const;
	void DrawYLine(int index) const;
	virtual void PushLineImpl(float z, float *x, float *y);

	Graph3DBase *base;
	int xCount;
	int zLines;
	int zMaxLines;

	int xLineCursor;
	int zLineCursor;

	COLORREF m_wireFrameColor;

	int m_compressInterval;

	std::vector<TexVertexF> m_vertexes;
};
