#include "stdafx.h"
#include "WalledWaterFallUnit.h"
#include "GLExtension.h"
#include "GLBuffer.h"
#include "TexVertex.h"
#include "Graph3DBase.h"
#include "Axis.h"
#include "TexVertex.h"
#include "ColorTexture.h"

WalledWaterFallUnit::WalledWaterFallUnit()
{
}
WalledWaterFallUnit::~WalledWaterFallUnit()
{
}

void WalledWaterFallUnit::Render() const
{
	if (zLines > 0)
	{
		glEnable(GL_POLYGON_OFFSET_FILL);
		glPolygonOffset(1.0F, 1.0F);

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		Draw(m_left);
		Draw(m_right);
		Draw(m_front);
		Draw(m_back);
		Draw(m_top);

		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);

		glDisable(GL_POLYGON_OFFSET_FILL);
	}
}
void WalledWaterFallUnit::Draw(std::vector<TexVertexF> const& v) const
{
	if (!v.empty()) {
		glPushClientAttrib(GL_CLIENT_VERTEX_ARRAY_BIT);
		glInterleavedArrays(GL_T2F_V3F, 0, &v[0]);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, static_cast<GLsizei>(v.size()));
		glPopClientAttrib();
	}
}

void WalledWaterFallUnit::ResetLines()
{
	m_top.clear();
	m_left.clear();
	m_right.clear();
	m_front.clear();
	m_back.clear();
}

void WalledWaterFallUnit::PushLineImpl(float z, float *x, float *y)
{
	if (zLines < zMaxLines)
	{
		++zLines;

		size_t const frontIndex = m_vertexes.size();
		double minYY = (double)base->GetZAxis()->GetLinearMinimum();
		{
			double xx = x[0], zz = z;
			base->ConvertValueToPosition(xx, zz, minYY);
		}
		for (int i = 0; i < xCount; ++i) {
			double xx = x[i], yy = y[i], zz = z;
			base->ConvertValueToPosition(xx, zz, yy);
			double const s = base->GetZAxis()->GetNormalize(y[i]);
			m_vertexes.push_back(TexVertexF(s, 0, xx, yy, zz));
			m_vertexes.push_back(TexVertexF(0, 0, xx, minYY, zz));
		}
		m_vertexes.push_back(m_vertexes[frontIndex]);

		if (zLines == zMaxLines) {
			MakeVertexes();
		}
	}
}

void WalledWaterFallUnit::MakeVertexes()
{
	ResetLines();

	if (m_vertexes.empty()) { return; }

	double minXX = static_cast<double>(base->GetXAxis()->GetLinearMinimum());
	double maxXX = static_cast<double>(base->GetXAxis()->GetLinearMaximum());
	double minYY = static_cast<double>(base->GetZAxis()->GetLinearMinimum());
	double maxYY = static_cast<double>(base->GetZAxis()->GetLinearMaximum());
	{
		double zz = 0;
		base->ConvertValueToPosition(minXX, zz, minYY);
		base->ConvertValueToPosition(maxXX, zz, maxYY);
	}

	int const lineCount = static_cast<int>(m_vertexes.size() / zLines);
	bool flip = true;
	for (int y = 0; /*endless*/; y += m_compressInterval) {
		y = std::min<int>(y, zLines - 1);
		int const index = y * lineCount;
		AddVertex(index, lineCount, minYY);
		if (y >= zLines - 1) { break; }
	}

	for (int y = 0; /*endless*/; y += m_compressInterval) {
		y = std::min<int>(y, zLines - 1);
		int const nextY = std::min<int>(y + m_compressInterval, zLines - 1);
		int x = flip ? 0 : xCount * 2 - 2;
		for (int i = 0; i < xCount; ++i) {
			int const currentIndex = y * lineCount + x;
			int const nextIndex = nextY * lineCount + x;

			TexVertexF const& current = m_vertexes[currentIndex];
			TexVertexF const& next = m_vertexes[nextIndex];

			m_top.push_back(current);
			m_top.push_back(next);

			if (flip)	{ x += 2; }
			else		{ x -= 2; }
		}
		if (nextY >= zLines - 1) { break; }
		flip = !flip;
	}

	// 最初と最後のラインは必ず描くので、前面と背面の壁の座標は常に同じ
	std::copy(m_vertexes.begin(), m_vertexes.begin() + xCount * 2, std::back_inserter(m_front));
	std::copy(m_vertexes.end() - xCount * 2, m_vertexes.end() - 1, std::back_inserter(m_back));
}

void WalledWaterFallUnit::AddVertex(int const index, int const lineCount, double const minYY)
{
	TexVertexF const& front = m_vertexes[index];
	TexVertexF const& back = m_vertexes[index + lineCount - 3];

	m_left.push_back(TexVertexF(0, front.T, front.X, minYY, front.Z));
	m_left.push_back(front);

	m_right.push_back(TexVertexF(0, back.T, back.X, minYY, back.Z));
	m_right.push_back(back);
}

void WalledWaterFallUnit::GetIndexLine(double x, double z, int *index, int *line)
{
	*line = -1;
	*index = -1;
	if (zLines <= 0) { return; }

	int const skipCount = static_cast<int>(m_vertexes.size() / zLines);
	size_t realCount = (m_vertexes.size() - zLines) / 2;
	int const lineCount = static_cast<int>(realCount / zLines);

	/**line = lineCount - 1;
	*index = zLines - 1;*/
	*line = zLines - 1;
	*index = lineCount - 1;

	for (int i = 0, n = 0; i <= lineCount; ++i, n += 2) {
		int const idx = n + skipCount * 0;
		TexVertexF const& v = m_vertexes[idx];
		if (x > v.X) { continue; }

		if (i == 0) {
			*index = 0;
		}
		else {
			double hi = m_vertexes[idx].X;
			double low = m_vertexes[(n - 2) + skipCount * 0].X;

			*index = std::abs(hi - x) <= std::abs(x - low) ? i : i - 1;
			//*index = i;
		}
		break;
	}

	for (int i = 0; i < zLines; ++i) {
		int const idx = i * skipCount + (0 * 2);
		TexVertexF const& v = m_vertexes[idx];
		if (z < v.Z) { continue; }

		if (i == 0) {
			*line = 0;
		}
		else {
			double hi = v.Z;
			double low = m_vertexes[(i - 1) * skipCount + (0 * 2)].Z;

			*line = std::abs(hi - z) <= std::abs(z - low) ? i : i - 1;
		}
		break;
	}
}

void WalledWaterFallUnit::SetCompressInterval(int compressInterval)
{
	m_compressInterval = compressInterval;
	MakeVertexes();
}

void WalledWaterFallUnit::Initialize(int /*xCount*/, int /*zLines*/)
{
	m_vertexes.clear();
	m_top.clear();
	m_left.clear();
	m_right.clear();
	m_front.clear();
	m_back.clear();

	m_x.clear();
	m_y.clear();
	m_z.clear();
}

void WalledWaterFallUnit::DoCursorChanged() const
{
	if (m_vertexes.empty() || !zLines) { return; }
	if (xLineCursor < 0 || zLineCursor < 0) { return; }

	int const skipCount = static_cast<int>(m_vertexes.size() / zLines);
	size_t realCount = (m_vertexes.size() - zLines) / 2;

	// カーソル値
	{
		int const idx = xLineCursor * skipCount + (zLineCursor * 2);
		OCPoint3D p(m_vertexes[idx].X, m_vertexes[idx].Z, m_vertexes[idx].Y);
		CursorValueChanged(p);
	}

	// 断面
	std::vector<OCPoint> xLines, yLines;
	int const lineCount = static_cast<int>(realCount / zLines);
	for (int i = 0, n = 0; i < lineCount; ++i, n += 2) {
		int const idx = n + skipCount * xLineCursor;
		yLines.push_back(OCPoint(m_vertexes[idx].Y, m_vertexes[idx].X));
	}
	for (int i = 0; i < zLines; ++i) {
		int const idx = i * skipCount + (zLineCursor * 2);
		xLines.push_back(OCPoint(-m_vertexes[idx].Z, m_vertexes[idx].Y));
	}
	CursorChanged(xLines, yLines);
}
