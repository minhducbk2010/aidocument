#include "stdafx.h"
#include "TrackingBarGraphUnit.h"
#include "GLExtension.h"
#include "GLBuffer.h"
#include "TexVertex.h"
#include "Graph3DBase.h"
#include "ColorTexture.h"
#include "Axis.h"

TrackingBarGraphUnit::TrackingBarGraphUnit()
{
}

TrackingBarGraphUnit::~TrackingBarGraphUnit()
{
}

void TrackingBarGraphUnit::Initialize(int xCount, int zLines)
{
	GLuint ord[13];
	ord[0] = 3;
	ord[1] = 2;
	ord[2] = 0;
	ord[3] = 1;
	ord[4] = xCount * 3 + 1;
	ord[5] = xCount * 3 + 2;
	ord[6] = xCount * 3 + 4;
	ord[7] = xCount * 3 + 3;
	ord[8] = 3;
	ord[9] = 2;
	ord[10] = 1;
	ord[11] = xCount * 3 + 3;
	ord[12] = xCount * 3 + 2;

	for (int z = 0; z < zLines; ++z)
	{
		for (int x = 0; x < xCount; ++x)
		{
			GLuint voffset = (xCount * 3 + 1) * (z * 2) + x * 3;

			for (int j = 0; j < 13; ++j)
			{
				m_barIndex.push_back(voffset + ord[j]);
			}
		}
	}
}

void TrackingBarGraphUnit::Render() const
{
	if (zLines <= 0 || m_barIndex.empty()) { return; }

	glPushClientAttrib(GL_CLIENT_VERTEX_ARRAY_BIT);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glInterleavedArrays(GL_T2F_V3F, 0, &m_vertexes[0]);

	for (int z = 0; /*...*/; z += m_compressInterval) {
		z = std::min<int>(z, zLines - 1);
		for (int x = 0; x < xCount; ++x) {
			int const index = z * xCount + x;
			glDrawElements(GL_TRIANGLE_STRIP, 13, GL_UNSIGNED_INT, &m_barIndex[index * 13]);
		}
		if (z >= zLines - 1) { break; }
	}
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
	glPopClientAttrib();
}

void TrackingBarGraphUnit::PushLineImpl(float z, float *x, float *y)
{
	if (zLines >= zMaxLines) { return; }

	double l, r, rr, t, b, zz, s, ss;
	double dx, dy, dz;
	dx = 0.0F;
	b = (double)base->GetYAxis()->GetMinimum();
	zz= z;
	base->ConvertValueToPosition(dx, b, zz);

	int offset = (xCount * 3 + 1) * zLines * 2;

	l = (double)base->GetXAxis()->GetMinimum();
	dy = dz = 0.0F;
	base->ConvertValueToPosition(l, dy, dz);
	r = l;

	if (zLines > 0)
	{
		offset -= (xCount * 3 + 1);
		int preOffset = offset - (xCount * 3 + 1);
		for (int i = 0; i < xCount * 3 + 1; ++i)
		{
			TexVertexF& pre = m_vertexes[preOffset + i];
			m_vertexes.push_back(TexVertexF(pre.S, pre.T, pre.X, pre.Y, zz));
		}

		offset += (xCount * 3 + 1);
	}

	for (int i = 0; i < xCount - 1; ++i)
	{
		l = r;
		r = x[i];
		t = y[i];
		dz = 0.0F;
		base->ConvertValueToPosition(r, t, dz);
		rr = x[i + 1];
		dy = dz = 0.0F;
		base->ConvertValueToPosition(rr, dy, dz);
		r += (rr - r) / 2.0F;
		s = (double)base->GetYAxis()->GetNormalize(y[i]);
		ss = 0.0F;
		m_vertexes.push_back(TexVertexF(ss, 0.0F, l, b, zz)); ++offset;
		m_vertexes.push_back(TexVertexF(s, 0.0F, l, t, zz)); ++offset;
		m_vertexes.push_back(TexVertexF(s, 0.0F, r, t, zz)); ++offset;
	}

	l = r;
	r = (double)base->GetXAxis()->GetMaximum();
	t = y[xCount - 1];
	dz = 0.0F;
	base->ConvertValueToPosition(r, t, dz);
	s = (double)base->GetYAxis()->GetNormalize(y[xCount - 1]);
	ss = 0.0F;
	m_vertexes.push_back(TexVertexF(ss, 0.0F, l, b, zz)); ++offset;
	m_vertexes.push_back(TexVertexF(s, 0.0F, l, t, zz)); ++offset;
	m_vertexes.push_back(TexVertexF(s, 0.0F, r, t, zz)); ++offset;
	m_vertexes.push_back(TexVertexF(ss, 0.0F, r, b, zz)); ++offset;

	if (zLines == zMaxLines - 1)
	{
		double zzz = (double)base->GetZAxis()->GetMaximum();
		dx = dy = 0.0F;
		base->ConvertValueToPosition(dx, dy, zzz);

		int preOffset = offset - (xCount * 3 + 1);
		for (int i = 0; i < xCount * 3 + 1; ++i)
		{
			TexVertexF& pre = m_vertexes[preOffset + i];
			m_vertexes.push_back(TexVertexF(pre.S, pre.T, pre.X, pre.Y, zz));
		}
	}

	++zLines;
}

