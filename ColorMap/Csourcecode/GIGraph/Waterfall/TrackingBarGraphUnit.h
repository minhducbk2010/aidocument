#pragma once

#include "WaterFallUnit.h"

class GLBuffer;
class ColorTexture;
struct TexVertex;

class TrackingBarGraphUnit : public WaterFallUnit
{
public:
	TrackingBarGraphUnit();
	virtual ~TrackingBarGraphUnit();

	void Render() const;

private:
	virtual void Initialize(int xCount, int zLines);
	void PushLineImpl(float z, float *x, float *y);

private:
	std::vector<GLuint> m_barIndex;
};
