#pragma once

#include <boost/scoped_ptr.hpp>
#include "Graph3DBase.h"
#include "ColorTexture.h"

class DeviceContext;
class Axis;
class Renderer;
class OptionCursorLine;
class IWaterFallUnit;
class IFile;

const int OPTION_CURSOR_LINES = 8;

class WaterFall : public Graph3DBase
{
public:
	WaterFall(HWND window, IMessageReceiver* owner);
	virtual ~WaterFall();

	virtual int GetTypeId() const { return 28; }
	void InitializeGraph(int type);
	virtual void InitializeData(int xCount, int zLines);
	using Graph3DBase::SetData;
	virtual void SetData(size_t index, IPlotData* data);
	virtual void SetPlotKind(int kind);
	virtual void SetPlotVisible(int index, int kind, bool visible);

	BOOL PushDataLine(float z, float* x, float* y);
	void PushDataLines(float* z, float** x, float** y, int lineCount);

	void GetCursorPoint(int x, int y, int *index, int *line);
	void GetCursorPointValue(int x, int y, float *vX, float *vY, float *vZ);
	void GetCursorPointOrder(int x, int y, float *order);
	void Select(int index, int line);

	void SetOptionCursorLineColor(int index, double r, double g, double b);
	void DrawOptionCursorLine(int index, double *x, double *y, double *z, int count);
	void SetXScaleVector(double* scale, int scaleLen);

	virtual void EnableHiddenLine(bool hidden);
	virtual bool IsHiddenLine() const;
	virtual void VisibleCursor(bool visible);

	void SetCompressInterval(int interval);
	int GetCompressInterval() const;

	virtual void ResetCursor();

	virtual size_t GetFileCount() const;
	virtual IGraphFile* GetFile(size_t index) const;
	virtual IGraphFile* GetEmptyFile() const;

	virtual IShape* AddData(IPlotData* data);
	
	virtual bool CanAddFile() const;
	virtual bool CheckUseFile(size_t fileId) const;
	virtual void UpdatePlotData();

// Controller
	virtual bool EnableCursor() const;
	virtual void SetCursor(int x, int y);
	virtual void SelectIndex(int index, int line);
	virtual void GetCursorIndex(int* index, int* line) const;

protected:
// Override
	virtual void RenderPlots(HDC dc) const;
	virtual void SetDataAfter();

#if 1
	virtual void AddPropertyImpl(OCProperty* prop, bool locked) const;
	virtual void AddTabPropertyImpl(OCProperty* prop, bool locked) const;
	virtual bool SetPropertyImpl(long id, LPARAM value, LPARAM* source);
	virtual bool GetPropertyImpl(long id, LPARAM* ret) const;
	virtual void SerializeImpl(ISerializer* ar);
#endif

	virtual Graph3DBase* Create() const { return new WaterFall(m_wnd, m_owner); }

	virtual void ResetPosition();

	using Graph3DBase::GetFile;

	virtual bool CheckUseColorPattern(TCHAR const* path);
	virtual bool UpdateColorPattern(TCHAR const* path);

// MessageHandler
	virtual void EnabledColorBar(BOOL* enabled, BOOL* checked) const;

private:
	virtual BOOL GetViewWireframe() const;
	virtual void SetViewWireframe(bool value);
	virtual void EnableFilling(bool enable);
	virtual BOOL IsFilling() const;
	virtual void EnableColoring(bool enable);
	virtual BOOL IsColoring() const;

	virtual IPlotData* GetData(size_t index) const;
	virtual size_t GetDataCount() const;

	virtual void RenderColorBar() const;

private:
	boost::shared_ptr<IPlotData> m_data;
	boost::scoped_ptr<IWaterFallUnit> m_drawer;
	boost::shared_ptr<ColorBar> m_colorBar;
	boost::shared_ptr<ColorTexture> m_colorPattern;

	bool m_viewWireFrame;
	bool m_coloring;
	bool m_filling;
	bool m_hiddenLine;
	OptionCursorLine* optionLine[OPTION_CURSOR_LINES];

	int m_importFileType;
	std::wstring m_importSettingFile;
};
