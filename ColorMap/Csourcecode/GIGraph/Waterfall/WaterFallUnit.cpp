#include "stdafx.h"
#include "WaterFallUnit.h"
#include "GLExtension.h"
#include "GLBuffer.h"
#include "Graph3DBase.h"
#include "Axis.h"
#include "ColorTexture.h"
#include "TexVertex.h"

WaterFallUnit::WaterFallUnit()
	: m_compressInterval(1)
	, m_wireFrameColor(RGB(192, 192, 192))
{
	xCount = 0;
	zLines = 0;
	zMaxLines = 0;

	xLineCursor = -1;
	zLineCursor = -1;
}
WaterFallUnit::~WaterFallUnit()
{
}

void WaterFallUnit::InitializeData(int xCount, int zLines, Graph3DBase *base)
{
	this->base = base;
	this->xCount = xCount;
	this->zLines = 0;
	this->zMaxLines = zLines;

	Initialize(xCount, zLines);
}

BOOL WaterFallUnit::PushLine(float z, float* x, float* y)
{
	PushLineImpl(z, x, y);
	return TRUE;
}
void WaterFallUnit::PushLines(float *z, float** x, float** y, int lineCount)
{
	for (int i = 0; i < lineCount; ++i) {
		PushLineImpl(z[i], x[i], y[i]);
	}
}
void WaterFallUnit::ModelingCursorLines(int index, int line)
{
	xLineCursor = std::min<int>(std::max<int>(line, 0), zMaxLines - 1);
	zLineCursor = std::min<int>(std::max<int>(index, 0), xCount - 1);

	DoCursorChanged();
}

void WaterFallUnit::ResetCursor()
{
	xLineCursor = zLineCursor = -1;
}

void WaterFallUnit::GetCursorValues(std::vector<OCPoint>* xLines, std::vector<OCPoint>* yLines) const
{
	//TODO: implement(テクレビ後)
	xLines, yLines;
}

void WaterFallUnit::Render() const
{
	if (zLines > 0)
	{
		glPushClientAttrib(GL_CLIENT_VERTEX_ARRAY_BIT);
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		int const lineCount = static_cast<int>(m_vertexes.size() / zLines);
		for (int i = 0; /*endless*/; i += m_compressInterval) {
			int const y = std::min<int>(i, zLines - 1);
			int const index = y * lineCount;
			glInterleavedArrays(GL_T2F_V3F, 0, &m_vertexes[index]);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, lineCount);

			if (y >= zLines - 1) { break; }
		}

		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);
		glPopClientAttrib();
	}
}

void WaterFallUnit::RenderWireframeXLines()
{
	if (zLines > 0)
	{
		glColor3ub(GetRValue(m_wireFrameColor), GetGValue(m_wireFrameColor), GetBValue(m_wireFrameColor));

		glEnableClientState(GL_VERTEX_ARRAY);

		for (int y = 0; /*endless*/; y += m_compressInterval) {
			y = std::min<int>(y, zLines - 1);
			DrawXLine(y);

			if (y >= zLines - 1) { break; }
		}

		::glDisableClientState(GL_VERTEX_ARRAY);
	}
}

void WaterFallUnit::DrawXLine(int const y, bool cursor/* = false*/) const
{
	int const lineCount = static_cast<int>(m_vertexes.size() / zLines);
	int const index = y * lineCount;

	std::vector<TexVertexF> lines;
	TexVertexF const& front = m_vertexes[index];
	lines.push_back(TexVertexF(front.S, front.T, front.X, (GLfloat)(-base->GetZAxis()->GetSize()), front.Z));
	for (int x = 0, n = 0; x < xCount; ++x, n += 2) {
		TexVertexF const& tv = m_vertexes[index + n];
		lines.push_back(TexVertexF(tv.S, tv.T, tv.X, tv.Y, tv.Z));
	}
	TexVertexF const& back = lines.back();
	lines.push_back(TexVertexF(back.S, back.T, back.X, (GLfloat)(-base->GetZAxis()->GetSize()), back.Z));

	size_t const offset = cursor ? 1 : 0;
	glInterleavedArrays(GL_T2F_V3F, 0, &lines[offset]);
	glDrawArrays(GL_LINE_STRIP, 0, static_cast<GLsizei>(lines.size() - offset * 2));
}

void WaterFallUnit::RenderWireframeZLines()
{
	if (zLines > 0)
	{
		glColor3ub(GetRValue(m_wireFrameColor), GetGValue(m_wireFrameColor), GetBValue(m_wireFrameColor));

		glEnableClientState(GL_VERTEX_ARRAY);

		for (int i = 0; i < xCount; ++i)
		{
			DrawYLine(i);
		}
	}
}

void WaterFallUnit::DrawYLine(int index) const
{
	std::vector<TexVertexF> lines;
	int const lineCount = static_cast<int>(m_vertexes.size() / zLines);
	for (int i = 0; /*...*/; i += m_compressInterval) {
		i = std::min<int>(i, zLines - 1);

		int const idx = (i * lineCount) + (index * 2);
		TexVertexF const& tv = m_vertexes[idx];
		lines.push_back(TexVertexF(tv.S, tv.T, tv.X, tv.Y, tv.Z));
		
		if (i >= zLines - 1) { break; }
	}

	glInterleavedArrays(GL_T2F_V3F, 0, &lines[0]);
	glDrawArrays(GL_LINE_STRIP, 0, static_cast<int>(lines.size()));
}

void WaterFallUnit::RenderCursorLines()
{
	if (zLines > 0)
	{
		GLint current;
		::glGetIntegerv(GL_DEPTH_FUNC, &current);
		::glLineWidth(2);

		glDepthFunc(GL_ALWAYS);
		glEnableClientState(GL_VERTEX_ARRAY);

		if (xLineCursor >= 0) {
			::glColor3ub(255, 0, 0);
			DrawXLine(xLineCursor, true);
		}
		if (zLineCursor >= 0) {
			::glColor3ub(0, 0, 255);
			DrawYLine(zLineCursor);
		}

		::glDepthFunc(current);
		::glLineWidth(1);
	}
}
void WaterFallUnit::GetIndexLine(double x, double z, int *index, int *line)
{
	if (zLines > 0)
	{
		int maxZ = xCount * (zLines - 1) + 1;
		*line = zMaxLines - 1;

		for (int i = 0, l = 0; i < maxZ; i += xCount, ++l)
		{
			if (z > m_vertexes[i].Z)
			{
				if (i == 0)
				{
					*line = 0;
				}
				else
				{
					double hi = m_vertexes[i].Z;
					double low = m_vertexes[i - xCount].Z;

					*line = std::abs(hi - z) <= std::abs(z - low) ? l : l - 1;
				}
				break;
			}
		}

		int maxX = xCount * *line + 1 + xCount;
		int idx = 0;

		*index = xCount - 1;

		for (int i = xCount * *line + 1; i < maxX; ++i, ++idx)
		{
			if (x < m_vertexes[i].X)
			{
				if (i == xCount * *line + 1)
				{
					*index = 0;
				}
				else
				{
					double hi = m_vertexes[i].X;
					double low = m_vertexes[i - 1].X;

					*index = std::abs(hi - x) <= std::abs(x - low) ? idx : idx - 1;
				}
				break;
			}
		}
	}
	else
	{
		*line = -1;
		*index = -1;
	}
}

void WaterFallUnit::GetCursorLine(int* index, int* line) const
{
	*index = zLineCursor;
	*line = xLineCursor;
}

void WaterFallUnit::PushLineImpl(float z, float *x, float *y)
{
	if (zLines < zMaxLines)
	{
		++zLines;

		double minYY = (double)base->GetYAxis()->GetMinimum();
		{
			double xx = x[0], zz = z;
			base->ConvertValueToPosition(xx, minYY, zz);
		}
		for (int i = 0; i < xCount; ++i) {
			double xx = x[i], yy = y[i], zz = z;
			base->ConvertValueToPosition(xx, yy, zz);
			double const s = static_cast<double>(base->GetYAxis()->GetNormalize(y[i]));
			m_vertexes.push_back(TexVertexF((GLfloat)s, 0, (GLfloat)xx, (GLfloat)yy, (GLfloat)zz));
			m_vertexes.push_back(TexVertexF(0, 0, (GLfloat)xx, (GLfloat)minYY, (GLfloat)zz));
		}
	}
}

void WaterFallUnit::SetWireframeColor(COLORREF color)
{
	m_wireFrameColor = color;
}
void WaterFallUnit::SetCompressInterval(int compressInterval)
{
	m_compressInterval = compressInterval;
}

#if 1

#include "ISerializer.h"
void WaterFallUnit::Serialize(ISerializer* ar)
{
	ar->Serialize(_T("XLineCursor"), xLineCursor, -1);
	ar->Serialize(_T("ZLineCursor"), zLineCursor, -1);
	ar->Serialize(_T("CompressInterval"), m_compressInterval, m_compressInterval);
	DoCursorChanged();
}

#endif