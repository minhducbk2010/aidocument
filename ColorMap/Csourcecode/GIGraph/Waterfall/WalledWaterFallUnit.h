#pragma once

#include "WaterFallUnit.h"
#include "TexVertex.h"

class GLBuffer;
class ColorTexture;
struct TexVertex;

class WalledWaterFallUnit : public WaterFallUnit
{
public:
	WalledWaterFallUnit();
	virtual ~WalledWaterFallUnit();

	virtual void ResetLines();
	void Render() const;
	virtual void GetIndexLine(double x, double z, int* index, int* line);
	void SetCompressInterval(int compressInterval);

protected:
	virtual void Initialize(int xCount, int zLines);
	virtual void DoCursorChanged() const;

private:
	void PushLineImpl(float z, float *x, float *y);
	void Draw(std::vector<TexVertexF> const& v) const;
	void MakeVertexes();
	void AddVertex(int index, int lineCount, double const minYY);

	// 壁の座標(間引き処理実行後)
	std::vector<TexVertexF> m_top;
	std::vector<TexVertexF> m_left;
	std::vector<TexVertexF> m_right;
	std::vector<TexVertexF> m_front;
	std::vector<TexVertexF> m_back;

	std::vector<double> m_x;
	std::vector<double> m_y;
	std::vector<double> m_z;
};
