#include "stdafx.h"
#include <cassert>
#include <map>
#include <boost/bind.hpp>
#include "WaterFall.h"
#include "DeviceContext.h"
#include "GLExtension.h"
#include "Axis.h"
#include "ColorBar.h"
#include "TextRenderer.h"
#include "WaterFallUnit.h"
#include "WalledWaterFallUnit.h"
#include "TrackingBarGraphUnit.h"
#include "OptionCursorLine.h"
#include "Section.h" // TODO: tmp
#include "IPlotData.h"
#include "IMessageReceiver.h" //TODO: tmp CursorValueChangedが消えたらこれも消す
#include "Message.h"
#include "Clipper.h"
#include "IFilter.h"
#include "IGraphFile.h"
#include "IFile.h"

class WaterfallFile : public IGraphFile
{
public:
	virtual IPlot* AddPlot(unsigned long /*type*/, unsigned long /*xItem*/, unsigned long /*yItem*/, boost::shared_ptr<IFilter> const& /*filter*/) {return 0;}
	virtual IPlot* Add3DPlot(unsigned long /*type*/, unsigned long /*xItem*/, unsigned long /*yItem*/, unsigned long /*zItem*/, boost::shared_ptr<IFilter> const& /*filter*/) {return 0;}
	virtual IPlot* Add4DPlot(unsigned long /*type*/, unsigned long /*xItem*/, unsigned long /*yItem*/, unsigned long /*zItem*/, unsigned long /*tItem*/,
		boost::shared_ptr<IFilter> const& /*filter*/) {return 0;}

	virtual bool IsEmpty() const { return m_empty; }
	virtual long GetFileId() const { return m_fileId; }
	virtual void SetFileId(long id) { m_fileId = id; }

	virtual TCHAR const* GetImportSettingFile() const { return m_importSettingFile.c_str(); }
	virtual int GetFileType() const { return m_fileType; }

	bool m_empty;
	long m_fileId;
	std::wstring m_importSettingFile;
	int m_fileType;
};
static WaterfallFile g_wfFile;

WaterFall::WaterFall(HWND window, IMessageReceiver* owner)
	: Graph3DBase(window, owner)
	, m_viewWireFrame(false)
	, m_coloring(true)
	, m_filling(true)
	, m_hiddenLine(true)
{
	std::fill(optionLine, optionLine + _countof(optionLine), nullptr);

	std::wstring projectDir = reinterpret_cast<TCHAR const*>(m_owner->SendMessage(PI_MSG_GET_PROJECTDIR, 0, 0));
	m_colorPattern.reset(new ColorTexture(projectDir + _T("\\ColorPattern\\"), m_owner));

	m_xAxis->ScaleChanged.connect(boost::bind(&WaterFall::SetDataAfter, this));
	m_yAxis->ScaleChanged.connect(boost::bind(&WaterFall::SetDataAfter, this));
	m_zAxis->ScaleChanged.connect(boost::bind(&WaterFall::SetDataAfter, this));
	m_colorBar.reset(new ColorBar(m_zAxis.get()));
	m_colorBar->SetLabelVisible(false);

	ResetPosition();
}
WaterFall::~WaterFall()
{
	std::for_each(optionLine, optionLine + _countof(optionLine), boost::checked_delete<OptionCursorLine>);
}

void WaterFall::InitializeGraph(int type)
{
	if (m_drawer) { return; } //TODO: <- テクレビ後に修正

	switch (type)
	{
	case 2:
		m_drawer.reset(new TrackingBarGraphUnit());
		//bDrawEdge = FALSE;
		break;
	case 3:
		m_drawer.reset(new WalledWaterFallUnit());
		break;
	default:
		m_drawer.reset(new WaterFallUnit());
		type = 0;
		break;
	}

	m_drawer->SetWireframeColor(m_wireframeColor);
	m_drawer->CursorChanged.connect(boost::bind(&WaterFall::CursorChanged, this, _1, _2));
	m_drawer->CursorValueChanged.connect(boost::bind(&WaterFall::CursorValueChanged, this, _1));

	for (int i = 0; i < OPTION_CURSOR_LINES; ++i)
	{
		delete optionLine[i];
		optionLine[i] = new OptionCursorLine(this);
	}
}

void WaterFall::InitializeData(int xCount, int zLines)
{
	if (m_drawer) {
		m_drawer->InitializeData(xCount, zLines, this);
	}
}

void WaterFall::SetData(size_t /*index*/, IPlotData* data)
{
	if (m_data.get() != data) {
		m_data.reset(data, boost::bind(&IPlotData::Destroy, _1));
	}

	InitializeGraph(3/*Wall*/);
	if (data->GetDataCount()) {
		AutoScaleAxes();
		AutoLabelAxes();
	}
	SetDataAfter();
}

void WaterFall::SetPlotKind(int kind)
{
	InitializeGraph(kind);
}

void WaterFall::SetPlotVisible(int /*index*/, int kind, bool /*visible*/)
{
	InitializeGraph(kind);
}

#if 1

#include "PropertyCreator.hpp"

void AddColorPattenProperty(IMessageReceiver* owner, IPropertyComposite* parent, bool const locked, ColorTexture* texture); //TODO: 移動

void WaterFall::AddPropertyImpl(OCProperty* prop, bool locked) const
{
	AddColorPattenProperty(m_owner, prop, locked, m_colorPattern.get());

	AddDataProperty(prop, locked);

	IPropertyGroup* grp = prop->CreateGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_PLOT"));
	grp->AddBoolProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_CLIPPING"), PI_DATA_CLIPPING, m_clipMaxX, locked); // Setは基底にある
	grp->AddSpinProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_THINNING_INT"), PI_WF_COMPRESS_INTERVAL, GetCompressInterval(), 1, m_drawer->GetLineCount(), locked);
}

void WaterFall::AddTabPropertyImpl(OCProperty* prop, bool locked) const
{
	AddColorPattenProperty(m_owner, prop->AddGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_FORMATTING")), locked, m_colorPattern.get());

	IPropertyGroup* grp = prop->AddGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_PLOT"))->AddGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_PLOT"));
	AddDataProperty(grp, locked);
	grp->AddBoolProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_CLIPPING"), PI_DATA_CLIPPING, m_clipMaxX, locked); // Setは基底にある
	grp->AddSpinProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_THINNING_INT"), PI_WF_COMPRESS_INTERVAL, GetCompressInterval(), 1, m_drawer->GetLineCount(), locked);
}

bool WaterFall::SetPropertyImpl(long id, LPARAM value, LPARAM* source)
{
	switch (id)
	{
	case PI_WF_COMPRESS_INTERVAL:
		{
			int cur = GetCompressInterval();
			int const max = m_drawer->GetZLineCount() - 1;
			if (::SetPropertyHelper(cur, value, source, 1, max)) {
				SetCompressInterval(cur);
			}
			else {
				TCHAR msg[256] = {};
				::_sntprintf_s(msg, _countof(msg) - 1, ::GetResourceString(m_owner,"ID_SOURCE_TEXT_THINNING_INT_MSG"), max);
				::MessageBox(m_wnd, msg, ::GetResourceString(m_owner,"ID_SOURCE_TEXT_O_CHART"), MB_OK|MB_ICONWARNING);
			}
		}
		return true;
	case PI_COLOR_PATTERN:
        {
			std::wstring pattern = ToWideString(m_colorPattern->GetCurrentPatternFileName());
			::SetPropertyHelper(pattern, value, source);
			m_colorPattern->SetPatternFileName(pattern.c_str());
        }
        return true;
	}
	return false;
}

bool WaterFall::GetPropertyImpl(long id, LPARAM* ret) const
{
	switch (id)
	{
	case PI_COLOR_PATTERN:
		{
			static std::wstring str;

			str = ToWideString(m_colorPattern->GetCurrentPatternFileName());
			*reinterpret_cast<wchar_t const**>(ret) = str.c_str();
		}
		return true;
	}
	return false;
}

#include "ISerializer.h"
void SerializeColorPattern(ISerializer* ar, ColorTexture* colorPattern, IMessageReceiver* owner); //TODO: 移動

void WaterFall::SerializeImpl(ISerializer* ar)
{
	IPlotData* data = GetData(0);
	ar->Serialize(_T("Data"), data);
	SetData(data);

	ISerializer* filterNode = ar->ChildNode(_T("Filter"));
	if (IReadable* reader = dynamic_cast<IReadable*>(filterNode)) {
		IFilter* ptr = reinterpret_cast<IFilter*>(m_owner->SendMessage(PI_GET_MIXED_FILTER, reinterpret_cast<WPARAM>(GetFile()), 0));
		boost::shared_ptr<IFilter> filter(ptr);
		filter->Serialize(filterNode);
		GetData(0)->SetFilter(filter);
	}
	else if (IFilter* filter = GetData(0)->GetFilter()) {
		filter->Serialize(filterNode);
	}

	ar->Serialize(_T("WireFrame"), m_viewWireFrame);
	ar->Serialize(_T("Paint"), m_filling, true);
	ar->Serialize(_T("Coloring"), m_coloring, true);

	m_drawer->Serialize(ar);

	if (IReadable* reader = dynamic_cast<IReadable*>(ar)) {
		if (reader->GetMajorVersion() == 3 && reader->GetMinorVersion() < 2) {
			// 3.1以前のOCPの復元
			EnableClipping(true);
		}
	}

	if (!IsReadable(ar)) {
		if (IFile* file = GetFile()) {
			std::wstring fileName = GetDirNameFromDirPath(file->GetDataFileDirectory());
			
			fileName += _T("\\ImportSetting.xml");

			m_importSettingFile = fileName;
			m_importFileType = file->GetImporterId();
		}
	}
	SerializeString(ar, _T("ImportSettingFile"), m_importSettingFile);
	ar->Serialize(_T("ImportFileType"), m_importFileType);

	SerializeColorPattern(ar->ChildNode(_T("ColorPattern")), m_colorPattern.get(), m_owner);
}

#endif

void WaterFall::RenderPlots(HDC /*dc*/) const
{
	if (!m_drawer) { return; }

	Clipper clipper(m_xAxis->GetSize(), m_yAxis->GetSize(), m_zAxis->GetSize());
	if (IsClipping()) {
		clipper.Clipping();
	}

	if (m_coloring) {
		m_colorPattern->SelectPattern(m_colorPattern->GetCurrentPattern());
	}
	else {
		m_colorPattern->SetMonocrhomePattern();
	}
	m_colorPattern->Bind();

	if (m_filling) {
		m_drawer->Render();
		m_colorPattern->Unbind();
	}

	if (m_viewWireFrame) 	{
		if (!IsHiddenLine()) {
			::glDisable(GL_DEPTH_TEST);
		}
		m_drawer->RenderWireframeXLines();
		::glEnable(GL_DEPTH_TEST);
	}
	//if (m_viewWireFrame) 	{
	//	if (!IsHiddenLine()) {
	//		::glDisable(GL_DEPTH_TEST);
	//	}
	//	m_drawer->RenderWireframeZLines();
	//	::glEnable(GL_DEPTH_TEST);
	//}

	m_colorPattern->Unbind();

	if (GetCursorLineVisible()) {
		m_drawer->RenderCursorLines();
	}

	for (int i = 0; i < OPTION_CURSOR_LINES; ++i)
	{
		if (optionLine[i] != NULL)
		{
			optionLine[i]->Render();
		}
	}
}

void WaterFall::RenderColorBar() const
{
	m_colorBar->Render(m_colorPattern.get(), m_zAxis->GetLabel());
}

template<class Range>
void unique(Range& r)
{
	std::sort(r.begin(), r.end());
	r.erase(std::unique(r.begin(), r.end()), r.end());
}

void WaterFall::SetDataAfter()
{
	if (m_lock) { return; }

	typedef std::map<float, float> inner_map_t;
	typedef std::map<float, inner_map_t> map_t;
	map_t buf; // 一時バッファ

	std::vector<float> xData, yData, zData;
	xData.reserve(m_data->GetDataCount());
	yData.reserve(m_data->GetDataCount());
	zData.reserve(m_data->GetDataCount());

	float invalid_value = FLT_MAX;
	for (size_t i = 0, n = m_data->GetDataCount(); i < n; ++i) {
		float const x = m_data->GetData(0, i);
		float const y = m_data->GetData(1, i);
		float const z = m_data->GetData(2, i);
		if (x != FLT_MAX && y != FLT_MAX && z != FLT_MAX) {
			buf[y][x] = FLT_MAX;

			xData.push_back(x);
			yData.push_back(y);
			zData.push_back(z);

			invalid_value = std::min<float>(invalid_value, z);
		}
	}

	if (xData.empty()) {
		if (m_drawer) {
			m_drawer->ResetCursor();
			m_drawer->ResetLines();
		}
		return;
	}

	// (X, Y, Z)のデータをグルーピング
	for (size_t i = 0; i < zData.size(); ++i) {
		float const x = xData[i];
		float const y = yData[i];
		float const z = zData[i];
		map_t::iterator it = buf.find(y);
		if (it != buf.end()) {
			inner_map_t::iterator it2 = it->second.find(x);
			if (it2 != it->second.end()) {
				it2->second = z;
			}
		}
	}

	unique(xData);
	unique(yData);

	size_t const MESH_LIMIT = 500000; //!< メッシュ数の上限(X*Y)
	if (xData.size() * yData.size() > MESH_LIMIT) {
		if (!m_serializing) {
			std::wstring msg = GetResourceString(m_owner, "ID_SOURCE_TEXT_TOO_MUCH_NUMBER_DATA_NOT_DRAW_GRAPH");
			::MessageBox(m_wnd, msg.c_str(), GetResourceString(m_owner, "ID_SOURCE_TEXT_O_CHART"), MB_OK|MB_ICONERROR);
		}
		return;
	}

	std::map<float, std::vector<float>> wave_data;
	for (map_t::iterator it = buf.begin(); it != buf.end(); ++it) {
		std::vector<float>& z = wave_data[it->first]; // ここで要素を作成
		z.reserve(it->second.size());

		std::map<float, float> const& xy = it->second;
		for (size_t i = 0; i < xData.size(); ++i) {
			inner_map_t::const_iterator hit = xy.find(xData[i]);
			if (hit == xy.end()) {
				z.push_back(invalid_value);
			}
			else {
				z.push_back(hit->second);
			}
		}
	}

	InitializeData(static_cast<int>(xData.size()), static_cast<int>(yData.size()));

	typedef std::map<float, std::vector<float>>::iterator data_iter;
	for (data_iter i = wave_data.begin(), e = wave_data.end(); i != e; ++i) {
		float const y = i->first;
		std::vector<float>& z = i->second;
		if (z.size() == xData.size()) {
			PushDataLine(y, &xData[0], &z[0]);
		}
	}
}

BOOL WaterFall::PushDataLine(float z, float *x, float *y)
{
	return m_drawer->PushLine(z, x, y);
}
void WaterFall::PushDataLines(float *z, float **x, float **y, int lineCount)
{
	m_drawer->PushLines(z, x, y, lineCount);
}

void WaterFall::GetCursorPoint(int x, int y, int *index, int *line)
{
	double dX, dY, dZ = 0;
	WindowToWorld(x, y, &dX, &dY, &dZ);
	m_drawer->GetIndexLine((double)dX, (double)dZ, index, line);
}
void WaterFall::GetCursorPointValue(int x, int y, float *vX, float *vY, float *vZ)
{
	double dX, dY, dZ;
	WindowToWorld(x, y, &dX, &dY, &dZ);
	GetValue((float)dX, (float)dY, (float)dZ, vX, vY, vZ);
}
void WaterFall::GetCursorPointOrder(int x, int y, float *order)
{
	double dX, dY, dZ;
	WindowToWorld(x, y, &dX, &dY, &dZ);
	GetOrder((float)dX, (float)dZ, order);
}
void WaterFall::Select(int index, int line)
{
	m_drawer->ModelingCursorLines(index, line);
}
void WaterFall::SetOptionCursorLineColor(int index, double r, double g, double b)
{
	optionLine[index]->SetLineColor(r, g, b);
}
void WaterFall::DrawOptionCursorLine(int index, double *x, double *y, double *z, int count)
{
	optionLine[index]->Modeling(x, y, z, count);
}

void WaterFall::SetXScaleVector(double *scale, int scaleLen)
{
	if(scale != NULL)
	{
		m_xAxis->SetScaleVector(scale, scaleLen);
	}
}

void WaterFall::EnableHiddenLine(bool hidden)
{
	m_hiddenLine = hidden;
}
bool WaterFall::IsHiddenLine() const
{
	return m_hiddenLine;
}

void WaterFall::VisibleCursor(bool visible)
{
	m_cursorLineVisible = visible;
}

void WaterFall::SetCompressInterval(int interval)
{
	m_drawer->SetCompressInterval(interval);
}

int WaterFall::GetCompressInterval() const
{
	return m_drawer->GetCompressInterval();
}

size_t WaterFall::GetFileCount() const
{
	return 1;
}

IGraphFile* WaterFall::GetFile(size_t index) const
{
	assert(index == 0);

	g_wfFile.m_empty = !CanAddFile();
	g_wfFile.m_fileId = -1;
	g_wfFile.m_fileType = m_importFileType;
	g_wfFile.m_importSettingFile = m_importSettingFile;
	return &g_wfFile;
}

IGraphFile* WaterFall::GetEmptyFile() const
{
	return nullptr;
}

IShape* WaterFall::AddData(IPlotData* data)
{
	if (!data) { return nullptr; }

	if (m_data.get() != data) {
		m_data.reset(data, boost::bind(&IPlotData::Destroy, _1));
	}
	m_data->LinkDataSet();

	return nullptr;
}

bool WaterFall::CanAddFile() const
{
	return m_data->GetFileId(0) < 0;
}

bool WaterFall::CheckUseFile(size_t fileId) const
{
	return (size_t)m_data->GetFileId(0) == fileId;
}

void WaterFall::UpdatePlotData()
{
	m_data->ClearItems();
	m_data->ChangeFile(m_data->GetFileId(0));
	m_data->LinkDataSet();
	SetData(0, m_data.get());
}

void WaterFall::ResetCursor()
{
	m_drawer->ResetCursor();
	m_xSection->SetData(std::vector<OCPoint>(), 0);
	m_ySection->SetData(std::vector<OCPoint>(), 0);
}

bool WaterFall::EnableCursor() const
{
	return EnableSection() && m_data->GetDataCount() > 0;
}

#include "RenderState.h"
void WaterFall::SetCursor(int x, int y)
{
	DeviceContextPtr context = DeviceContext::CreateInstance(m_viewer);
	context->MakeCurrentContext();

	::glClearColor(1, 1, 1, 0);
    ::glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	::glEnable(GL_DEPTH_TEST);

	int const width = EnableVerticalSection() ? m_width - 110 : m_width;
	int const height = EnableHorizontalSection() ? m_height - 110 : m_height;

	::glPushMatrix();
	{
		::glViewport(0, 0, width, height);
		::glMatrixMode(GL_PROJECTION);
		::glLoadIdentity();
		::glOrtho(-14, 14, -14, 14, -1, 1);

		::glMatrixMode(GL_MODELVIEW);
		::glLoadIdentity();

		RenderState state(m_pitch, m_yaw, m_roll, m_h, m_v, m_scale, m_viewColorBar);
		state.Bind();

		int xIndex, yIndex;
		GetCursorPoint(x, y, &xIndex, &yIndex);
		Select(xIndex, yIndex);
	}
	::glPopMatrix();
}

void WaterFall::SelectIndex(int index, int line)
{
	Select(index, line);
}

void WaterFall::GetCursorIndex(int* index, int* line) const
{
	m_drawer->GetCursorLine(index, line);
}

void WaterFall::ResetPosition()
{
	Rotate(20, 45, 0);
	Scaling(1);
	Translate(0, 0);
}

void WaterFall::EnabledColorBar(BOOL* enabled, BOOL* checked) const
{
	*enabled = TRUE;
	*checked = GetViewColorBar();
}

BOOL WaterFall::GetViewWireframe() const
{
	return m_viewWireFrame;
}

void WaterFall::SetViewWireframe(bool value)
{
	m_viewWireFrame = value;
}

void WaterFall::EnableFilling(bool enable)
{
	m_filling = enable;
}
BOOL WaterFall::IsFilling() const
{
	return m_filling;
}

void WaterFall::EnableColoring(bool enable)
{
	m_coloring = enable;
}
BOOL WaterFall::IsColoring() const
{
	return m_coloring;
}

IPlotData* WaterFall::GetData(size_t const index) const
{
	assert(index == 0);
	return m_data.get();
}

size_t WaterFall::GetDataCount() const
{
	return 1;
}

bool WaterFall::CheckUseColorPattern(TCHAR const* path)
{
	std::wstring const a = ::PathFindFileName(ToWideString(m_colorPattern->GetCurrentPatternFileName()).c_str());
	std::wstring const b = ::PathFindFileName(path);
	return a == b;
}

bool WaterFall::UpdateColorPattern(TCHAR const* path)
{
	if (CheckUseColorPattern(path)) {
		m_colorPattern->ReloadPatterns();
		return true;
	}
	return false;
}
