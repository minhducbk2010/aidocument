/*!	@file
*/
#include "stdafx.h"
#include <boost/range/algorithm/find.hpp>
#include <atlimage.h>
#include "IMessageReceiver.h"
#include "ICanvas.h"
#include "IGroup.h"
#include "IPlotData.h"
#include "ISerializer.h"
#include "GraphWrapper.h"
#include "Property.h"
#include "PropertyHelper.hpp"
#include "Message.h"
#include "SolidGraph/SolidGraph.h"
#include "Controller.hpp"
#include "algorithmex.h"
#include "Axis.h" // ?
#include "IFile.h"
#include "IFilter.h"
#include "AddPlotSetting.h"
#include "ShapeWrapper.h"

OCGraphWrapper::OCGraphWrapper(HWND wnd, IMessageReceiver* owner, factory_t factory)
	: OCRectShape(wnd, owner)
	, m_impl(factory())
	, m_mode(CControllerImpl::Rotate)
	, m_nameInitialized(false)
	, m_serializing(false)
{
	m_lineStyle = PS_NULL;
	m_fillStyle = FS_SOLID;

	m_impl->SetParent(this);
	m_impl->Resized.connect(boost::bind(&OCGraphWrapper::Resized, this, _1, _2));
	m_impl->PropertyUpdated.connect(boost::bind(&OCGraphWrapper::CreatePreviewImage, this, _1));
	m_impl->GetZoomRatio.connect(boost::bind(&OCGraphWrapper::GetZoomRatio, this));
	//m_impl->GetRect.connect(boost::bind(&OCGraphWrapper::GetRect, this, _1, _2, _3, _4));


	m_viewer.reset(new CControllerImpl(m_impl.get()));
	m_viewer->Create(m_parent);
	m_impl->AttachViewer(m_viewer->m_hWnd);

	m_viewer->GetZoomRatio.connect(boost::bind(&OCGraphWrapper::GetZoomRatio, this));
	m_viewer->GetRect.connect(boost::bind(&OCGraphWrapper::GetRect, this, _1, _2, _3, _4));

	m_messageHandler[ID_CMD_TURN]	= boost::bind(&OCGraphWrapper::SetMode, this, CControllerImpl::Rotate);
	m_messageHandler[ID_CMD_RESIZE]	= boost::bind(&OCGraphWrapper::SetMode, this, CControllerImpl::Zoom);
	m_messageHandler[ID_CMD_MOVE]	= boost::bind(&OCGraphWrapper::SetMode, this, CControllerImpl::Move);
	m_messageHandler[ID_CMD_CURSOR]	= boost::bind(&OCGraphWrapper::SetMode, this, CControllerImpl::Cursor);
	m_messageHandler[ID_CMD_SAVE]	= boost::bind(&OCGraphWrapper::SaveImage, this);
	m_messageHandler[ID_CMD_COPY]	= boost::bind(&OCGraphWrapper::CopyImage, this);
	m_messageHandler[ID_CMD_PRINT]	= boost::bind(&OCGraphWrapper::PrintImage, this);

	m_stateGetter[ID_CMD_TURN]		= boost::bind(&OCGraphWrapper::GetMouseCursorState, this, CControllerImpl::Rotate, _1, _2);
	m_stateGetter[ID_CMD_RESIZE]	= boost::bind(&OCGraphWrapper::GetMouseCursorState, this, CControllerImpl::Zoom, _1, _2);
	m_stateGetter[ID_CMD_MOVE]		= boost::bind(&OCGraphWrapper::GetMouseCursorState, this, CControllerImpl::Move, _1, _2);
	m_stateGetter[ID_CMD_CURSOR]	= boost::bind(&OCGraphWrapper::GetMouseCursorState, this, CControllerImpl::Cursor, _1, _2);
#ifdef NEW_TRIAL_EDITION
	m_stateGetter[ID_CMD_SAVE]		= boost::bind(&OCGraphWrapper::UnenabledAlways, this, _1, _2);
	m_stateGetter[ID_CMD_COPY]		= boost::bind(&OCGraphWrapper::UnenabledAlways, this, _1, _2);
	m_stateGetter[ID_CMD_PRINT]		= boost::bind(&OCGraphWrapper::UnenabledAlways, this, _1, _2);
#else
	m_stateGetter[ID_CMD_SAVE]		= boost::bind(&OCGraphWrapper::EnabledAlways, this, _1, _2);
	m_stateGetter[ID_CMD_COPY]		= boost::bind(&OCGraphWrapper::EnabledAlways, this, _1, _2);
	m_stateGetter[ID_CMD_PRINT]		= boost::bind(&OCGraphWrapper::EnabledAlways, this, _1, _2);
#endif
}

OCGraphWrapper::~OCGraphWrapper()
{
	TextRenderer::GetInstance()->Dispose();
	if (m_viewer.get() && m_viewer->m_hWnd != 0) {
		m_viewer->DestroyWindow();
	}
}

unsigned long OCGraphWrapper::GetTypeId() const
{
	return m_impl->GetTypeId();
}

IShape* OCGraphWrapper::Clone(IMessageReceiver* owner)
{
	OCGraphWrapper* clone = new OCGraphWrapper(this->m_parent, owner, boost::bind(&Graph3DBase::Clone, m_impl.get()));

	// シリアライズ機能を使って複製を作成
	boost::scoped_ptr<ISerializer> ar(reinterpret_cast<ISerializer*>(this->m_owner->SendMessage(PI_GET_SERIALIZE_WRITER, 0, 0)));
	this->Serialize(ar.get());

	{
		boost::scoped_ptr<ISerializer> reader(reinterpret_cast<ISerializer*>(this->m_owner->SendMessage(PI_GET_SERIALIZE_READER, (WPARAM)(ar.get()), 0)));
		clone->Serialize(reader.get());
	}
	clone->SetCanvas(this->m_canvas);

	return clone;
}

void OCGraphWrapper::SetRect(double left, double top, double right, double bottom)
{
	__super::SetRect(left, top, right, bottom);

	if (m_fitType != FitType::None) {
		OCPoint p(m_rect.left, m_rect.top);
		m_owner->SendMessage(PI_MSG_FIT, 0, reinterpret_cast<LPARAM>(&p));
		m_rect.left = p.x;
		m_rect.top = p.y;
	}
	if (m_fitType == FitType::All) {
		OCPoint p(m_rect.right, m_rect.bottom);
		m_owner->SendMessage(PI_MSG_FIT, 0, reinterpret_cast<LPARAM>(&p));
		m_rect.right = p.x;
		m_rect.bottom = p.y;
	}

	CreatePreviewImage();
}

long OCGraphWrapper::EndDrag(double x, double y)
{
	long const ret = __super::EndDrag(x, y);
	CreatePreviewImage();
	//編集中ならばビューアも更新する
	if(IsEditing())
	{
		if (m_selected) {
			// ツールパレットを表示(Excel上でもリサイズされたら表示しなおす)
			IGraph3D* g = this;
			m_owner->SendMessage(PI_MSG_SHOW_3D_TOOLBAR, TRUE, reinterpret_cast<LPARAM>(g));
			ShowViewer();
		}
	}
	return ret;
}

void OCGraphWrapper::SetSelected(bool select)
{
	__super::SetSelected(select);
	m_impl->SetSelected(select);

	if (!select) {
		CloseViewer();

		//boost::for_each(m_child, boost::bind(&IShape::SetSelected, _1, select));
	}
}

void OCGraphWrapper::DoubleClick(double x, double /*y*/)
{
	if (m_lock) { return; }

	if (m_selected) {
		// ツールパレットを表示
		if (!IsZero(x - 9999)) { // Excel上でダブルクリックされたらここでは表示しない
			IGraph3D* g = this;
			m_owner->SendMessage(PI_MSG_SHOW_3D_TOOLBAR, TRUE, reinterpret_cast<LPARAM>(g));
		}

		ShowViewer();
	}
}


//! ビューアを表示します
void OCGraphWrapper::ShowViewer()
{
		m_owner->SendMessage(PI_MSG_BEGIN_EDIT, reinterpret_cast<WPARAM>(this), 0);

		m_impl->Editing(true);

		double const ratio = m_canvas->GetZoomRatio();
		OCPoint p(MMToPx(m_rect.left * ratio), MMToPx(m_rect.top * ratio));
		m_canvas->LPtoDP(&p, 1);

		int const width = static_cast<int>(MMToPx(m_rect.Width() * ratio));
		int const height = static_cast<int>(MMToPx(m_rect.Height() * ratio));
		OCPoint br(p.x + width, p.y + height);

		HWND view = reinterpret_cast<HWND>(m_owner->SendMessage(PI_MSG_GET_VIEW_HANDLE, 0, 0));
		m_viewer->SetParent(view);

		CRect editRect((int)p.x, (int)p.y, (int)br.x, (int)br.y);
		editRect.OffsetRect(1, 1);
		m_viewer->SetWindowPos(HWND_BOTTOM, editRect.left, editRect.top, editRect.Width(), editRect.Height(), SWP_NOACTIVATE);

		ClippingWindow();
		m_viewer->ShowWindow(SW_SHOW);

		m_viewer->SetMode(static_cast<CControllerImpl::Mode>(m_mode));
		m_viewer->SetFocus();
}

//! ビューアを閉じます
void OCGraphWrapper::CloseViewer()
{
	if (m_viewer->m_hWnd == NULL) { return; }

	m_impl->Editing(false);

	HCURSOR cur = ::LoadCursor(NULL, IDC_ARROW);
	::SetClassLongPtr(*(m_viewer.get()), GCL_HCURSOR, (__int3264)(LONG_PTR)(cur));
	::SetCursor(cur);

	// 編集中であれば
	if (IsEditing()) {
		// ツールバーを閉じて
		IGraph3D* g = this;
		m_owner->SendMessage(PI_MSG_SHOW_3D_TOOLBAR, FALSE, reinterpret_cast<LPARAM>(g));

		m_viewer->ShowWindow(SW_HIDE);

		// グラフイメージを更新
		CreatePreviewImage();
		m_owner->SendMessage(PI_MSG_INVALIDATE, 0, 0);
		// プロット種別が変わっていることがあるのでアイコンを更新させる
		m_owner->SendMessage(PI_MSG_UPDATE_OBJECTPALETTE, 0, 0);
	}
}


bool OCGraphWrapper::IsEditing() const
{
	return !!m_viewer->IsWindowVisible();
}

//! グラフイメージをビットマップに保存
void OCGraphWrapper::SaveImage()
{
#ifndef NEW_TRIAL_EDITION
	WTL::CFileDialog dlg(FALSE, _T("bmp"), 0, OFN_OVERWRITEPROMPT | OFN_HIDEREADONLY, ::GetResourceString(m_owner,"ID_SOURCE_TEXT_PICTURE_FILE_BMP"));
	if (dlg.DoModal() == IDOK) {
		WTL::CBitmap img;
		GetImage(img);

		ATL::CImage saver;
		saver.Attach(img);
		saver.Save(dlg.m_szFileName);
		saver.Detach();
	}
#endif
}

void OCGraphWrapper::Capture(HDC dc, int width, int height)
{
	WTL::CBitmap img;
	{
		m_impl->Editing(false);
		WTL::CDC memDC;
		memDC.CreateCompatibleDC();

		BITMAPINFOHEADER bmpInfo = {sizeof(BITMAPINFOHEADER)};
		bmpInfo.biWidth = width;
		bmpInfo.biHeight = height;
		bmpInfo.biPlanes = 1;
		bmpInfo.biBitCount = 24;
		bmpInfo.biCompression = BI_RGB;

		void* data = 0;
		HBITMAP bmp = img.CreateDIBSection(memDC.m_hDC, reinterpret_cast<BITMAPINFO*>(&bmpInfo), DIB_RGB_COLORS, &data, 0, 0);
		if (bmp) {
			HBITMAP oldBmp = memDC.SelectBitmap(bmp);
			m_impl->Capture(memDC.m_hDC, bmpInfo.biWidth, bmpInfo.biHeight);
			memDC.SelectBitmap(oldBmp);
		}
		m_impl->Editing(true);
	}

	WTL::CDC offDC;
	offDC.CreateCompatibleDC();
	offDC.SelectBitmap(img);

	::BitBlt(dc, 0, 0, width, height, offDC.m_hDC, 0, 0, SRCCOPY);
}

//! グラフイメージをクリップボードにコピーします
void OCGraphWrapper::CopyImage()
{
#ifndef NEW_TRIAL_EDITION
	if (::OpenClipboard(m_parent)) {
		WTL::CBitmap bmp;
		m_impl->Editing(false);
		GetImage(bmp);
		m_impl->Editing(true);

		WTL::CDC offDC;
		offDC.CreateCompatibleDC();
		HBITMAP oldBmp = offDC.SelectBitmap(bmp);

		WTL::CSize size;
		bmp.GetSize(size);
		HDC srcDC = ::CreateDC(_T("DISPLAY"), NULL, NULL, NULL);
		HDC memDC = ::CreateCompatibleDC(srcDC);

		HBITMAP memBmp = ::CreateCompatibleBitmap(srcDC, size.cx, size.cy);
		::SelectObject(memDC, memBmp);

		::BitBlt(memDC, 0, 0, size.cx, size.cy, offDC.m_hDC, 0, 0, SRCCOPY);

		offDC.SelectBitmap(oldBmp);
		::SelectObject(memDC, 0);
		::DeleteDC(memDC);
		::DeleteDC(srcDC);

		::EmptyClipboard();
		::SetClipboardData(CF_BITMAP, memBmp);
		::CloseClipboard();

		::DeleteObject(memBmp);
	}
#endif
}

/*! グラフイメージを印刷します
	縦横比を維持したまま用紙サイズいっぱいに拡大し、用紙の中央に印刷します
*/
void OCGraphWrapper::PrintImage()
{
#ifndef NEW_TRIAL_EDITION
	WTL::CPrintDialog dlg;
	if (dlg.DoModal() == IDOK) {
		WTL::CDCHandle dc(dlg.GetPrinterDC());

		WTL::CBitmap bmp;
		m_impl->Editing(false); // 断面ON/OFFボタンを描かせない
		GetImage(bmp);
		m_impl->Editing(true);

		ATL::CImage img;
		img.Attach(bmp);

		WTL::CSize paperSize(dc.GetDeviceCaps(PHYSICALWIDTH), dc.GetDeviceCaps(PHYSICALHEIGHT));
		double const xRate = paperSize.cx / static_cast<double>(img.GetWidth());
		double const yRate = paperSize.cy / static_cast<double>(img.GetHeight());
		double const rate = std::min<double>(xRate, yRate);

		dc.SetMapMode(MM_ANISOTROPIC);
		dc.SetWindowExt(WTL::CSize(img.GetWidth(), img.GetHeight()));
		dc.SetViewportExt(WTL::CSize(static_cast<int>(img.GetWidth() * rate), static_cast<int>(img.GetHeight() * rate)));

		// イメージを用紙中央に配置する
		if (xRate > yRate) {
			int const paperWidth = static_cast<int>(img.GetWidth() * rate);
			int const left = paperSize.cx / 2 - paperWidth / 2;
			dc.SetViewportOrg(left, 0, nullptr);
		}
		else {
			int const paperHeight = static_cast<int>(img.GetHeight() * rate);
			int const top = paperSize.cy / 2 - paperHeight / 2;
			dc.SetViewportOrg(0, top, nullptr);
		}

		DOCINFO docInfo = {sizeof(DOCINFO)};
		dc.StartDoc(&docInfo);
		dc.StartPage();
		dc.BitBlt(0, 0, img.GetWidth(), img.GetHeight(), img.GetDC(), 0, 0, SRCCOPY);
		dc.EndPage();
		dc.EndDoc();

		img.ReleaseDC();
		img.Detach();

		::DeleteDC(dc);
	}
#endif
}

void OCGraphWrapper::ClearPlots()
{
	m_impl->ClearPlots();
}

void OCGraphWrapper::AddShowProperty(IPropertyComposite* parent)
{
	bool visibleChangable = true;
	for (IShape* s = dynamic_cast<IShape*>(m_parentGroup); s; s = dynamic_cast<IShape*>(s->GetParent())) {
		visibleChangable = s->IsVisible();
		if (!visibleChangable) { break; }
	}

	IPropertyGroup* group = parent->AddGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_SHOW"), PI_VISIBLE, m_visible, m_lock || !visibleChangable);
	/*if (m_visible)*/ {
		group->AddProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_NAME"), PI_NAME, m_name.c_str(), m_lock);

		std::wstring fitProp1 = GetResourceString(m_owner, "ID_SOURCE_TEXT_INVALIDITY");
		std::wstring fitProp2 = GetResourceString(m_owner, "ID_SOURCE_TEXT_UPPER_LEFT");
		std::wstring fitProp3 = GetResourceString(m_owner, "ID_SOURCE_TEXT_FOUR_CORNERS");
		group->AddArrayProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_FIT"), PI_FIT, m_fitType, m_lock, 3, 
			fitProp1.c_str(), fitProp2.c_str(), fitProp3.c_str());

		group->AddBoolProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_LOCK"), PI_POS_LOCK, m_posLock);

		m_impl->AddShowPropertyOption(group, m_lock);
	}
}

void OCGraphWrapper::AddPlacementProperty(IPropertyComposite* parent)
{
	IPropertyGroup* group = parent->AddGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_PLACEMENT"));
	group->AddProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_X_COORDINATE_MM"), PI_ALIGN_X, m_rect.left - PAPER_MARGIN_MM, m_lock);
	group->AddProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_Y_COORDINATE_MM"), PI_ALIGN_Y, m_rect.top - PAPER_MARGIN_MM, m_lock);
	group->AddProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_WIDTH_MM"), PI_ALIGN_WIDTH, m_rect.Width(), m_lock);
	group->AddProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_HEIGHT_MM"), PI_ALIGN_HEIGHT, m_rect.Height(), m_lock);
}

void OCGraphWrapper::AddLineProperty(IPropertyComposite* parent)
{
	CreateLineProperty(m_owner, parent, m_lock, m_lineStyle, m_lineColor, m_lineWidth, m_lineRoundType, m_canvas);
}

void OCGraphWrapper::AddShadowProperty(IPropertyComposite* parent)
{
	bool notDraw = m_lineStyle == PS_NULL && m_fillStyle == FS_NONE;
	IPropertyGroup* group = parent->AddGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_SHADOW"), PI_SHADOW, m_shadow, m_lock || notDraw);
	/*if (m_shadow)*/ {
		group->AddSpinProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_OFFSET"), PI_SHADOW_OFFSET, m_shadowOffset, -50, 50, m_lock || notDraw || !m_shadow);
		group->AddColorProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_COLOR_KANJI"), PI_SHADOW_COLOR, m_shadowColor, m_lock || notDraw || !m_shadow);
		group->AddTransparentProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_TRANSPARENCY_SHADOW"), PI_SHADOW_TRANSPARENT, m_shadowTransparent, m_lock || notDraw || !m_shadow);
	}
}

PropertyNode* OCGraphWrapper::CreateProperty()
{
	OCProperty prop;

	// 共通プロパティ
	AddShowProperty(&prop);
	AddPlacementProperty(&prop);
	m_impl->AddShowProperty(&prop, m_lock);

	AddLineProperty(&prop);
	AddShadowProperty(&prop);

	m_impl->AddProperty(&prop, m_lock);

	return prop.GetRawPtr();
}

PropertyNode* OCGraphWrapper::CreateTabProperty()
{
	OCProperty prop;
	if (IPropertyGroup* group = prop.CreateGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_SHOW"))) {
		AddShowProperty(group);
		AddPlacementProperty(group);
		m_impl->AddShowProperty(group, m_lock);
	}

	if (IPropertyGroup* group = prop.CreateGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_FORMATTING"))) {
		AddLineProperty(group);
		AddShadowProperty(group);
	}

	m_impl->AddTabProperty(&prop, m_lock);

	return prop.GetRawPtr();
}

void OCGraphWrapper::ReleaseProperty(PropertyNode* prop)
{
	delete prop;
}

bool OCGraphWrapper::SetProperty(long id, LPARAM value, LPARAM* source)
{
	if (SetRectProperty(id, value, source) || m_impl->SetProperty(id, value, source)) {
		CreatePreviewImage();
		return true;
	}
	return false;
}

bool OCGraphWrapper::GetProperty(long id, LPARAM* ret) const
{
	return m_impl->GetProperty(id, ret);
}

void OCGraphWrapper::SetDimension(int const dim)
{
	m_impl->SetDimension(dim);

	if (!m_nameInitialized) {
		m_name = m_impl->GetTypeId() == 28
			? ::GetResourceString(m_owner,"ID_SOURCE_TEXT_WATERFALL")
			: dim == 6
			? ::GetResourceString(m_owner,"ID_SOURCE_TEXT_VECTOR_DIAGRAM")
			: dim == 4
			? ::GetResourceString(m_owner,"ID_SOURCE_TEXT_4D_GRAPH")
			: ::GetResourceString(m_owner,"ID_SOURCE_TEXT_3D_GRAPH");
		m_nameInitialized = true;
	}
}

long OCGraphWrapper::GetDimension() const
{
	return m_impl->GetDimension();
}

void OCGraphWrapper::SetData(IPlotData* data)
{
	m_impl->SetData(data);
}

void OCGraphWrapper::AddData(IPlotData* data)
{
	m_impl->AddData(data);
	UpdateGraphImage();
}

// IGraph Implement

void OCGraphWrapper::InitRect(OCRect const& rect)
{
	m_rect = rect;
}

bool OCGraphWrapper::CanAddFile() const
{
	return m_impl->CanAddFile();
}

bool OCGraphWrapper::CanAddPlot() const
{
	// 3Dのみプロット追加可能。4D/ベクトル図はfalse
	return m_impl->CanAddFile() && m_impl->GetTypeId() == 26 && m_impl->GetDimension() == 3;
}

bool OCGraphWrapper::CheckUseFile(size_t fileId) const
{
	return m_impl->CheckUseFile(fileId);
	for (size_t i = 0; i < m_impl->GetDataCount(); ++i) {
		IPlotData* data = m_impl->GetData(i);
		if ((size_t)data->GetFileId(0) == fileId) { return true; }
	}
	return false;
}

IGraphFile* OCGraphWrapper::AddFile(size_t fileId, bool /*copyPlot*//* =/ true*/)
{
	IGraphFile* file = m_impl->AddFile(fileId);
	UpdateGraphImage();
	return file;
}

IGraphFile* OCGraphWrapper::AddFileNoClone(size_t fileId)
{
	bool showMsg = false;
	return AddFile(fileId, showMsg);
}

size_t OCGraphWrapper::GetFileCount() const
{
	return m_impl->GetFileCount();
}

IGraphFile* OCGraphWrapper::GetFile(size_t index) const
{
	return m_impl->GetFile(index);
}

IGraphFile* OCGraphWrapper::GetEmptyFile() const
{
	return m_impl->GetEmptyFile();
}

IAxis* OCGraphWrapper::GetXAxis(size_t /*index*/) const
{
	return m_impl->GetXAxis();
}

IAxis* OCGraphWrapper::GetYAxis(size_t /*index*/) const
{
	return m_impl->GetYAxis();
}

IAxis* OCGraphWrapper::GetZAxis(size_t /*index*/) const
{
	return m_impl->GetZAxis();
}

void OCGraphWrapper::AddPlot(AddPlotSetting3D const& setting, IPlotContainer* result)
{
	if (IPlotData* data = reinterpret_cast<IPlotData*>(m_owner->SendMessage(PI_MSG_CREATE_PLOTDATA, setting.fileId, 0))) {
		data->SetLink(0, (long)setting.fileId, (long)setting.xItem.itemId);
		data->SetLink(1, (long)setting.fileId, (long)setting.yItem.itemId);
		data->SetLink(2, (long)setting.fileId, (long)setting.zItems[0].itemId);
		data->SetLink(3, (long)setting.fileId, (long)setting.zItems[0].itemId);
		data->SetFilter(boost::shared_ptr<IFilter>(setting.filter));
		data->LinkDataSet();
		result->AddPlot(m_impl->AddPlot(data, setting.plotKind));

		UpdateGraphImage();
	}
}

bool OCGraphWrapper::UseDigitalmap() const
{
	return m_impl->UseDigitalmap();
}

void OCGraphWrapper::ShowDigitalmap()
{
	m_impl->ShowDigitalmap();
	CreatePreviewImage();
	m_owner->SendMessage(PI_MSG_INVALIDATE, 0, 0);
}

bool OCGraphWrapper::CheckUseColorPattern(TCHAR const* path)
{
	return m_impl->CheckUseColorPattern(path);
}

void OCGraphWrapper::UpdateColorPattern(TCHAR const* path)
{
	if (m_impl->UpdateColorPattern(path)) {
		CreatePreviewImage();
	}
}

void OCGraphWrapper::UpdateLock()
{
	m_impl->UpdateLock();
}

void OCGraphWrapper::UpdateUnlock()
{
	m_impl->UpdateUnlock();
	UpdateGraphImage();
}

// IGraph3D Implement

void OCGraphWrapper::OnCommand(UINT id)
{
	message_handler_t::iterator it = m_messageHandler.find(id);
	if (it != m_messageHandler.end()) {
		it->second();
	}
	else {
		m_impl->DoCommand(id);
	}

	m_viewer->Invalidate();
}

void OCGraphWrapper::GetState(UINT id, BOOL* enable, BOOL* checked)
{
	state_getter_handler_t::const_iterator it = m_stateGetter.find(id);
	if (it != m_stateGetter.end()) {
		(it->second)(enable, checked);
	}
	else {
		m_impl->GetCommandState(id, enable, checked);
	}
}

int OCGraphWrapper::GetActivePlotIndex() const
{
	return m_impl->GetActivePlotIndex();
}

COLORREF OCGraphWrapper::GetActivePlotColor() const
{
	return m_impl->GetActivePlotColor();
}

#include "SolidGraph.h"
void OCGraphWrapper::SetPlotKind(unsigned long kind)
{
	m_impl->SetPlotKind(kind);
}

void OCGraphWrapper::AddPlot(unsigned long type, IPlotData* data)
{
	SetPlotKind(type);
	SetData(data);
}

void OCGraphWrapper::SetData(int index, IPlotData* data)
{
	m_impl->SetData(index, data);
}

void OCGraphWrapper::SetPlotVisible(int index, int kind, bool visible)
{
	m_impl->SetPlotVisible(index, kind, visible);
}

void OCGraphWrapper::SetPlotKind(int index, int kind)
{
	if (SolidGraph* g = dynamic_cast<SolidGraph*>(m_impl.get())) {
		g->SetPlotKind(index, kind);
	}
}

//! マーカーの操作(選択/D&Dによる値変更)を有効にします
void OCGraphWrapper::EnableMarkerControl()
{
	m_viewer->EnableLeftClickSelect(true);
	m_viewer->EnableDragging(true);
}

void OCGraphWrapper::AddPlotForLib(int kind, IPlotData* data)
{
	m_impl->AddPlot(data, kind);
}

void OCGraphWrapper::RemovePlot(int index)
{
	m_impl->RemovePlot(index);
}

/*!	@brief	グラフを更新します
	データウィンドウなどで値を編集すると呼ばれます
*/
void OCGraphWrapper::UpdateGraphImage()
{
	// データ更新
	m_impl->UpdatePlotData();

	CreatePreviewImage(true);
}

void OCGraphWrapper::RestoreState(int state)
{
	__super::RestoreState(state);
	CreatePreviewImage();
}

int OCGraphWrapper::SavePreEditState()
{
	return m_impl->SavePreEditState();
}
void OCGraphWrapper::RestoreEditState(int state)
{
	m_impl->RestoreEditState(state);
	CreatePreviewImage();
}

void OCGraphWrapper::Serialize(ISerializer* ar)
{
	m_serializing = true;

	__super::Serialize(ar);
	m_impl->Serialize(ar);

	ar->Serialize(_T("ViewMode"), m_mode);
	SetMode(m_mode);

	CreatePreviewImage();

	m_serializing = false;
}

///// IFilter

IFile* OCGraphWrapper::GetFile() const
{
	return m_impl->GetFile();
}
IFilter* OCGraphWrapper::GetFilter() const
{
	return m_impl->GetFilter();
}

///// IGroup

void OCGraphWrapper::AddChild(IShape* /*shape*/)
{
}

void OCGraphWrapper::Insert(IShape* shape, size_t /*index*/)
{
	m_impl->InsertChild(shape);
}

size_t OCGraphWrapper::Remove(IShape* shape)
{
	m_impl->RemoveChild(shape);
	return 0;
}

size_t OCGraphWrapper::GetChildCount() const
{
	return m_impl->GetChildCount();
}
IShape* OCGraphWrapper::GetChild(size_t index) const
{
	return m_impl->GetChild(index);
}

bool OCGraphWrapper::Include(IShape* shape) const
{
	return GetIndex(shape) != -1;
}

void OCGraphWrapper::Select(IShape* shape, bool recursive/* = false*/)
{
	shape, recursive;
}
size_t OCGraphWrapper::GetIndex(IShape* /*shape*/) const
{
	//std::vector<ShapeWrapper*>::const_iterator it = boost::find_if(m_child, shape);
	//if (it != m_child.end()) {
	//	return std::distance(m_child.begin(), it);
	//}
	return size_t(-1);
}

///// -------------------------------------------------------------------------

void OCGraphWrapper::DrawImpl() const
{
	// 断面図が両方出ている時、右下に空白ができるので、そこはクリッピングして描かないようにする
	if (m_impl->EnableVerticalSection() && m_impl->EnableHorizontalSection()) {
		double const size = PxToMM(SECTION_SIZE);
		OCPoint pts[] = {
			OCPoint(m_rect.left,			m_rect.top),
			OCPoint(m_rect.right,			m_rect.top),
			OCPoint(m_rect.right,			m_rect.bottom - size),
			OCPoint(m_rect.right - size,	m_rect.bottom - size),
			OCPoint(m_rect.right - size,	m_rect.bottom),
			OCPoint(m_rect.left,			m_rect.bottom),
		};
		m_canvas->SetClipping(pts, _countof(pts));
	}

#if _DEBUG
	if (m_canvas->IsPrinting()) {
		// 印刷時はビットマップをそのまま描くと荒くなるので、直接描く
		int const width = static_cast<int>(MMToPx(m_rect.Width() * m_canvas->GetZoomRatio()));
		int const height = static_cast<int>(MMToPx(m_rect.Height() * m_canvas->GetZoomRatio()));
		HDC dc = m_canvas->GetHDC();
		m_impl->Capture(dc, width, height);
		m_canvas->ReleaseHDC(dc);
	}
	else {
		m_canvas->DrawBitmap(m_img.m_hBitmap, m_rect.left, m_rect.top, -1, -1, 0);
	}
#else
	m_canvas->DrawBitmap(m_img.m_hBitmap, m_rect.left, m_rect.top, -1, -1, 0);
#endif
	m_canvas->DrawRectangle(m_rect);
	m_canvas->ResetClipping();
}

void OCGraphWrapper::CreatePreviewImage(bool all_update)
{
	if (all_update) {
		m_impl->AutoLabelAxes();
		m_impl->AutoScaleAxes();
	}

	m_impl->SetLocation(m_rect.left, m_rect.top);

	if (m_img) {
		m_img.DeleteObject();
	}
	GetImage(m_img);
}

void OCGraphWrapper::GetImage(WTL::CBitmap& img, bool bitmap/* = true*/) const
{
	WTL::CDC memDC;
	memDC.CreateCompatibleDC();

	BITMAPINFOHEADER bmpInfo = {sizeof(BITMAPINFOHEADER)};
	bmpInfo.biWidth = static_cast<int>(MMToPx(m_rect.Width()));
	bmpInfo.biHeight = static_cast<int>(MMToPx(m_rect.Height()));
	bmpInfo.biPlanes = 1;
	bmpInfo.biBitCount = 24;
	bmpInfo.biCompression = BI_RGB;

	void* data = 0;
	HBITMAP bmp = img.CreateDIBSection(memDC.m_hDC, reinterpret_cast<BITMAPINFO*>(&bmpInfo), DIB_RGB_COLORS, &data, 0, 0);
	if (bmp) {
		HBITMAP oldBmp = memDC.SelectBitmap(bmp);
		m_impl->Capture(memDC.m_hDC, bmpInfo.biWidth, bmpInfo.biHeight, bitmap);
		memDC.SelectBitmap(oldBmp);
	}
}

void OCGraphWrapper::UpdateDataFile(long fileId)
{
	if (IPlotData* data = m_impl->GetData(0)) {
		for (size_t i = 0; i < 6; ++i) {
			data->SetLink((long)i, fileId); // 項目はそのまま
		}

		SetData(data);
	}
}

void OCGraphWrapper::ClippingWindow()
{
	// 断面図が両方表示されているときは、右下の空白部分はクリッピングする
	if (m_impl->EnableVerticalSection() && m_impl->EnableHorizontalSection()) {
		double const ratio = m_canvas->GetZoomRatio();
		int const width = static_cast<int>(MMToPx(m_rect.Width() * ratio));
		int const height = static_cast<int>(MMToPx(m_rect.Height() * ratio));
		int const size = static_cast<int>(SECTION_SIZE * ratio);
		WTL::CRect rect(0, 0, width, height);
		POINT pts[] = {
			WTL::CPoint(rect.left,			rect.top),
			WTL::CPoint(rect.right,			rect.top),
			WTL::CPoint(rect.right,			rect.bottom - size),
			WTL::CPoint(rect.right - size,	rect.bottom - size),
			WTL::CPoint(rect.right - size,	rect.bottom),
			WTL::CPoint(rect.left,			rect.bottom),
		};
		HRGN rgn = ::CreatePolygonRgn(pts, _countof(pts), WINDING);
		m_viewer->SetWindowRgn(rgn, FALSE);
	}
	else {
		m_viewer->SetWindowRgn(NULL, FALSE);
	}
}

void OCGraphWrapper::Resized(int width, int height)
{
	m_rect.right = m_rect.left + PxToMM(width /*/ m_canvas->GetZoomRatio()*/);
	m_rect.bottom = m_rect.top + PxToMM(height /*/ m_canvas->GetZoomRatio()*/);
	m_viewer->SetWindowPos(HWND_BOTTOM, 0, 0,  static_cast<int>(width * m_canvas->GetZoomRatio()), static_cast<int>(height * m_canvas->GetZoomRatio()), SWP_NOMOVE);

	CreatePreviewImage();
	ClippingWindow();

	// ウィンドウサイズが変わったらページを再描画させる
	m_owner->SendMessage(PI_MSG_INVALIDATE, 0, 0);
}

void OCGraphWrapper::SetMode(int mode)
{
	if (!m_serializing) {
		if (m_mode == mode && mode == CControllerImpl::Cursor) {
			m_impl->ResetCursor();
		}
	}

	// カーソル機能が使えないのに、カーソルモードにしようとしたら回転に切り替える
	// (Proで作成したOCPをStdで開いたときなどに起こりうる)
	if (mode == CControllerImpl::Cursor && !m_impl->EnableCursor()) {
		mode = CControllerImpl::Rotate;
	}

	m_mode = mode;
	m_viewer->SetMode(static_cast<CControllerImpl::Mode>(mode));
}

void OCGraphWrapper::GetMouseCursorState(int targetMode, BOOL* enable, BOOL* checked) const
{
	if (targetMode == CControllerImpl::Cursor) {
		*enable = m_impl->EnableCursor();
	}
	else {
		*enable = TRUE;
	}
	*checked = targetMode == m_viewer->GetMode();
}

//! 影を描画します
void OCGraphWrapper::DrawShadow() const
{
	OCRect r = m_rect;
	r.Offset(m_shadowOffsetRotated.x, m_shadowOffsetRotated.y);

	//TODO: remove cast
	m_canvas->SetPenTransparent((short)m_shadowTransparent);
	m_canvas->SetTransparent((short)m_shadowTransparent);
	m_canvas->CreateGradationBrush(r, m_shadowBlur, m_shadowColor);
	m_canvas->SetPenColor(m_shadowColor);

	if (m_fillStyle == FS_NONE) {
		m_canvas->DrawRectangle(r);
	}
	else {
		m_canvas->FillRectangle(r);
	}

	// 元に戻す
	m_canvas->SetPenTransparent(0);
	m_canvas->SetTransparent((short)m_transparency);
	m_canvas->SetBackground(m_fillColor, (FILLSTYLE)m_fillStyle, (short)m_transparency);
	m_canvas->SetPenColor(m_lineColor);
}

void OCGraphWrapper::DrawSelected()
{
	__super::DrawSelected();
}

int OCGraphWrapper::HitTestImpl(double x, double y) const
{
	if (!m_visible) { return HIT_NONE; }

	if (IsEditing()) {
		// 編集中はクリッピングを考慮する
		OCPoint const mm(x - (m_rect.left * m_canvas->GetZoomRatio()), y - (m_rect.top * m_canvas->GetZoomRatio()));
		OCPoint const px = MMToPx(mm);
		HRGN rgn = ::CreateRectRgn(0, 0, 0, 0);
		if (m_viewer->GetWindowRgn(rgn) != ERROR) {
			BOOL const ret = ::PtInRegion(rgn, static_cast<int>(px.x), static_cast<int>(px.y));
			::DeleteObject(rgn);
			return ret;
		}
	}

	if (m_rect.PtInRect(OCPoint(x, y))) {
		return HIT_MOVE;
	}
	return FALSE;
}

