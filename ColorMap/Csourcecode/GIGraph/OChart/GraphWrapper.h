#pragma once

#include <boost/shared_ptr.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/signals2.hpp>
#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/ptr_container/ptr_deque.hpp>
#include <wtl/atlapp.h>
#include <wtl/atlgdi.h>
#include "IGraph3D.h"
#include "IPlot.h"
#include "IGraphFile.h"
#include "PluginUtility/RectShape.h"
#include "MessageHandler.h"
#include "IGroup.h"

class Graph3DBase;
class OCGraphWrapper;
template<class Graph> class CController;
typedef CController<Graph3DBase> CControllerImpl;

class ShapeWrapper;

typedef boost::function<Graph3DBase*()> factory_t;

class OCGraphWrapper : public OCRectShape, public MessageHandler, public IGroup, public IGraph3D, public IFilterOwner, public IDigitalmapHolder
{
public:
	OCGraphWrapper(HWND wnd, IMessageReceiver* owner, factory_t factory);
	virtual ~OCGraphWrapper();

// Override Shapes
	virtual void Destroy() { delete this; }
	virtual unsigned long GetTypeId() const;
	virtual IShape* Clone(IMessageReceiver* owner);

	virtual void SetRect(double left, double top, double right, double bottom);
	virtual long EndDrag(double x, double y);
	virtual void SetSelected(bool select);

	virtual void DoubleClick(double x, double y);

	virtual PropertyNode* CreateProperty();
	virtual PropertyNode* CreateTabProperty();
	virtual void ReleaseProperty(PropertyNode* prop);
	virtual bool SetProperty(long id, LPARAM value, LPARAM* source);
	virtual bool GetProperty(long id, LPARAM* ret) const;

// Overrides IGraph
	virtual void InitRect(OCRect const& rect);

	virtual bool CanAddFile() const;
	virtual bool CanAddPlot() const;
	virtual bool CheckUseFile(size_t fileId) const;
	
	virtual IGraphFile* AddFile(size_t fileId, bool copyPlot = true);
	virtual IGraphFile* AddFileNoClone(size_t fileId);

	virtual size_t GetFileCount() const;
	virtual IGraphFile* GetFile(size_t index) const;
	virtual IGraphFile* GetEmptyFile() const;

	virtual size_t GetXAxisCount() const { return 1; }
	virtual IAxis* GetXAxis(size_t /*index*/) const;

	virtual size_t GetYAxisCount() const { return 1; }
	virtual IAxis* GetYAxis(size_t /*index*/) const;

	virtual size_t GetZAxisCount() const { return 1; }
	virtual IAxis* GetZAxis(size_t /*index*/) const;

	virtual void AllSelectAxis() {}
	virtual void AllSelectPlot() {}

	virtual void AddPlot(AddPlotSetting2D const&, IPlotContainer*) {}
	virtual void AddPlot(AddPlotSetting3D const& setting, IPlotContainer* result);
	virtual void AddPlot(AddPlotSettingVector const&, IPlotContainer*) {}

	virtual bool IsUseDigitalArray() const { return false; }
	virtual void ShowDigitalArray() {}
	virtual bool Is3D() const { return true; }

	virtual bool CheckUseColorPattern(TCHAR const* path);
	virtual void UpdateColorPattern(TCHAR const* path);

	virtual void UpdateLock();
	virtual void UpdateUnlock();

// IDigitalmapHolder
	virtual bool UseDigitalmap() const;
	virtual void ShowDigitalmap();

// Overrides IGraph3D
	virtual void SetDimension(int dim);
	long GetDimension() const;
	virtual void SetData(IPlotData* data);
	virtual void AddData(IPlotData* data);
	virtual void SetPlotKind(unsigned long kind);

	virtual void OnCommand(UINT id);
	virtual void GetState(UINT id, BOOL* enable, BOOL* checked);

	virtual int GetActivePlotIndex() const;
	virtual COLORREF GetActivePlotColor() const;

	virtual void AddPlot(unsigned long type, IPlotData* data);

	virtual void UpdateGraphImage();

	virtual void RestoreState(int state);
	virtual int SavePreEditState();
	virtual void RestoreEditState(int state);

	virtual void Serialize(ISerializer* ar);

// IFilterOwner
	virtual IFile* GetFile() const;
	virtual IFilter* GetFilter() const;

// IGroup
	virtual void Add(IShape* shape) {shape;}
	virtual void Insert(IShape* shape, size_t index);
	virtual size_t Remove(IShape* shape);
	virtual size_t GetChildCount() const;
	virtual IShape* GetChild(size_t index) const;
	virtual void Clear() {}

	virtual bool Include(IShape* shape) const;

	virtual void Select(IShape* shape, bool recursive = false);
    virtual size_t GetIndex(IShape* shape) const;

	virtual void BringToFront(IShape* child) {child;}
	virtual void SendToBack(IShape* child) {child;}
	virtual void ChangeZOrder(IShape* shape, size_t index) {shape, index;}

	void AddChild(IShape* child);

// Operation
	Graph3DBase* GetDrawer() const { return m_impl.get(); }

	void Capture(HDC dc, int width, int height);

	void ShowViewer();
	void CloseViewer();
	bool IsEditing() const;
	void SaveImage();
	void CopyImage();
	void PrintImage();
	void SetMode(int mode);

	void ClearPlots();

// GlGraphLib
	void SetData(int index, IPlotData* data);
	void SetPlotVisible(int index, int kind, bool visible);
	void SetPlotKind(int index, int kind);
	void EnableMarkerControl();
	void AddPlotForLib(int kind, IPlotData* data);
	void RemovePlot(int index);

protected:

// Override Shapes
	virtual void DrawImpl() const;
	virtual void DrawShadow() const;
	virtual void DrawSelected();
	virtual int HitTestImpl(double x, double y) const;

private:
	void CreatePreviewImage(bool all_update = false);
	void GetImage(WTL::CBitmap& img, bool bitmap = true) const;
	void UpdateDataFile(long fileId);
	void ClippingWindow();

// Event Handler
	void Resized(int width, int height);
	double GetZoomRatio() { return m_canvas->GetZoomRatio(); }

// Commands
	void GetMouseCursorState(int targetMode, BOOL* enable, BOOL* checked) const;

// Property
	void AddShowProperty(IPropertyComposite* parent);
	void AddPlacementProperty(IPropertyComposite* parent);
	void AddLineProperty(IPropertyComposite* parent);
	void AddShadowProperty(IPropertyComposite* parent);

private:
	boost::shared_ptr<Graph3DBase> m_impl;
	boost::scoped_ptr<CControllerImpl> m_viewer;
	int m_mode; // CControllerImpl::Mode

	bool m_nameInitialized;
	bool m_serializing;

	WTL::CBitmap m_img;
};

