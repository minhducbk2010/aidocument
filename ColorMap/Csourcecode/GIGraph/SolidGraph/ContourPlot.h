/*!	@file
*/
#pragma once

#include <boost/shared_ptr.hpp>
#include <boost/signals2.hpp>
#include "SolidUnitBase.hpp"
#include "ContourDecor.h"
#include "TexVertex.h"
#include "IMessageReceiver.h"
#include "Message.h"

class OCSurface;
class IPlotData;
class Axis;
struct RenderState;
class CrossCursor;

class ContourPlot : public SolidUnitBase
{
public:
	enum {
		Gridding,
		Triangle,
	};

	ContourPlot(CrossCursor* cursor, IMessageReceiver* owner);
	~ContourPlot();

	virtual void SetContourAlgorithm(long type);

	bool EnableCursor() const;
	void SetCursor(RenderState* state, int x, int y);
	void SelectCursor(int xIndex, int yIndex);
	void GetCursor(int* x, int* y) const;
	void GetIndexLine(double x, double z, int *index, int *line) const;
	void ResetCursor();

	void GetSectionData(std::vector<OCPoint>* x, std::vector<OCPoint>* y) const;

	void RenderCursor() const;
	void RenderCursor(double x, double y) const;
	TexVertex GetCursorPos(int xIndex, int yIndex) const;
	double InterpolateValue(double x, double y) const;

	void ShowDigitalmap(IPlotData* data);

	virtual void ClearModel();
	virtual void Modeling(TexVertex* vertexes, size_t length);
	void SetValue(int index, double z);
	void SetGridData(std::vector<double> const& x, std::vector<double> const& y, std::vector<double> const& z, size_t xLen, size_t yLen);
	void ClearGridData();

// Override
	virtual bool IsLighting() const	{ return false; }

protected:
// O-Chart
#if 1
	virtual void AddProperty(IPropertyComposite* prop, bool locked) const;
	virtual bool SetProperty(long id, LPARAM value, LPARAM* source);

	virtual void Serialize(ISerializer* ar);
#endif

private:
	int GetMeshValue() const;
	double GetAlphaValue() const;

	std::vector<Vertex> InterpolateXLines(double y) const;
	std::vector<Vertex> InterpolateYLines(double x) const;

private:
	virtual void RenderImpl(ColorTexture* texture, TexVertex const* vertexes, size_t length, bool clipping) const;
	virtual void RenderWireFrame(ColorTexture* texture, TexVertex const* vertexes, size_t length, bool clipping) const;

	boost::scoped_ptr<OCSurface> m_surface;
	int m_mesh;
	int m_alpha;
	IMessageReceiver* m_owner;

	CrossCursor* m_cursor;

	std::vector<double> m_manualX;
	std::vector<double> m_manualY;
	std::vector<double> m_manualZ;
	// m_manualX.size() * m_manualY.size() == m_manualZ.size()

	mutable CalcData m_data;
	mutable ContourDecor m_decor;
};
