#pragma once

#include "SolidUnitBase.hpp"

class ScatterPlot : public SolidUnitBase
{
public:
	ScatterPlot();
	virtual ~ScatterPlot();

	void SetMarkerSize(double size) { m_markerSize = size; }
	double GetMarkerSize() const { return m_markerSize; }
	void SelectMarker(int index) { m_selectedIndex = index; }

	void SelectOnlyMarkerVisible() { m_visibleSelectOnly = true; }
	virtual void PreparePickup() { m_pickup = true; }
	virtual void UnpreparePickup() { m_pickup = false; }

private:
	virtual void RenderImpl(ColorTexture* texture, TexVertex const* vertexes, size_t length, bool clipping) const;
	virtual void RenderWireFrame(ColorTexture* texture, TexVertex const* vertexes, size_t length, bool clipping) const;
	void RenderQuadaric(TexVertex const* vertexes, size_t length, ColorTexture* texture, bool wireframe, bool clipping) const;

private:
	double m_markerSize;
	int m_selectedIndex;
	bool m_visibleSelectOnly;
	bool m_pickup;
};
