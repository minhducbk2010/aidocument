/*!	@SolidFile
*/
#include "stdafx.h"
#include "SolidFile.h"
#include "IMessageReceiver.h"
#include "Message.h"
#include "IFile.h"
#include "ISerializer.h"
#include "PropertyCreator.hpp"
#include "ShapeWrapper.h"
#include "PlotGroup.h"
#include "Legend.h"
#include "SolidGraph.h"

template<class T, class U>
bool operator == (boost::shared_ptr<T>& a, U* b) { return a.get() == b; }

///////////////////////////////////////////////////////////////////////////////

//! Constructor
SolidFile::SolidFile(IMessageReceiver* owner, SolidGraph* graph)
	: m_owner(owner)
	, m_graph(graph)
	, m_name(::GetResourceString(m_owner,"ID_SOURCE_TEXT_FILE"))
	, m_autoname(true)
	, m_visible(true)
	, m_color(RGB(0, 0, 0))
	, m_selected(false)
	, m_fileType(-1)
{
}

void SolidFile::AddPlot(PlotGroup* plot)
{
	if (boost::find(m_plots, plot) == m_plots.end()) {
		plot->SetParent(this);
		m_plots.push_back(boost::shared_ptr<PlotGroup>(plot));

		AutoNaming();
	}
}

boost::shared_ptr<PlotGroup> SolidFile::GetPlot(size_t index) const
{
	return m_plots[index];
}

std::vector<boost::shared_ptr<PlotGroup>> SolidFile::GetPlots() const
{
	return m_plots;
}

void SolidFile::SetDimension(int dim)
{
	boost::for_each(m_plots, boost::bind(&PlotGroup::SetDimension, _1, dim));
}

void SolidFile::Modeling()
{
	boost::for_each(m_plots, boost::bind(&PlotGroup::Modeling, _1));
}

void SolidFile::Render(bool clipping) const
{
	if (!m_visible) { return; }

	boost::for_each(m_plots, boost::bind(&PlotGroup::Render, _1, clipping));
}

// IShape

void SolidFile::GetName(wchar_t* name, size_t length) const
{
	std::wstring const s = GetName();
	::_tcsncpy_s(name, length, s.c_str(), _TRUNCATE);
}

int SolidFile::HitTest(double x, double y) const
{
	return m_legend->Contains(this, x, y) ? HIT_MOVE : HIT_NONE;
}

void SolidFile::SetSelected(bool select)
{
	if (!select) {
		boost::for_each(m_plots, boost::bind(&PlotGroup::SetSelected, _1, select));
	}

	if (m_selected != select) {
		m_selected = select;
		OnPropertyUpdated();
		m_owner->SendMessage(PI_MSG_INVALIDATE, 0, 0);
	}
}
bool SolidFile::GetSelected() const
{
	return m_selected;
}

std::vector<std::wstring> GetFiles(IMessageReceiver* owner, IFile* file, long* current);//TODO: 3.3

void SolidFile::AddShowProperty(IPropertyComposite* prop)
{
	if (IFile* file = GetSourceFile()) {
		IPropertyGroup* group = prop->AddGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_SHOW"), PI_VISIBLE, m_visible, IsLock());

		group->AddProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_NAME"), PI_NAME, m_name.c_str(), IsLock());
		group->AddBoolProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_AUTO_NAMING"), PI_AUTO_NAME, m_autoname, IsLock());
		group->AddBoolProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_LOCK"), PI_POS_LOCK, m_posLock, IsLock());

		long index = 0;
		std::vector<std::wstring> files = GetFiles(m_owner, file, &index);
		group->AddArrayProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_USE_FILE"), PI_FILE_INDEX, IsLock(), index, files);

		group->AddColorProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_COLOR_KANJI"), PI_LINE_COLOR, m_color, IsLock() || m_graph->GetGraphType() != GraphType::File);
	}
}

PropertyNode* SolidFile::CreateProperty()
{
	OCProperty prop;
	AddShowProperty(&prop);
	return prop.GetRawPtr();
}
PropertyNode* SolidFile::CreateTabProperty()
{
	OCProperty prop;
	AddShowProperty(prop.CreateGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_SHOW")));
	return prop.GetRawPtr();
}

bool SolidFile::SetProperty(long id, LPARAM value, LPARAM* source)
{
	switch (id)
	{
	case PI_VISIBLE:
		::SetPropertyHelper(m_visible, value, source);
		OnPropertyUpdated();
		return true;
	case PI_NAME:
		::SetPropertyHelper(m_name, value, source);
		m_autoname = false; //TODO: command
		OnPropertyUpdated();
		return true;
	case PI_AUTO_NAME:
		::SetPropertyHelper(m_autoname, value, source);
		if (m_autoname) {
			AutoNaming();
		}
		return true;
	case PI_FILE_INDEX:
		if (source) { *source = (LPARAM)GetFileId(); }
		// 渡されるのはファイルのインデックスなので、IDに変換する
		if (IFile* file = reinterpret_cast<IFile*>(m_owner->SendMessage(PI_MSG_GET_FILE, value, 0))) {
			SetFileId(file->GetId());
		}
		AutoNaming();
		return true;
	case PI_LINE_COLOR:
		::SetPropertyHelper(m_color, value, source);
		boost::for_each(m_plots, boost::bind(&PlotGroup::SetProperty, _1, PI_3D_WIREFRAME_COLOR, value, nullptr));
		return true;
	case PI_LOCK:
		::SetPropertyHelper(m_lock, value, source);
		return true;
	case PI_POS_LOCK:
		::SetPropertyHelper(m_posLock, value, source);
		return true;
	}

	return false;
}
bool SolidFile::GetProperty(long /*id*/, LPARAM* /*ret*/) const
{
	return false;
}

void SolidFile::Serialize(ISerializer* ar)
{
	ar->Serialize(_T("Visible"), m_visible);
	SerializeString(ar, _T("Name"), m_name);
	ar->Serialize(_T("AutoName"), m_autoname);
	m_lock = false;
	ar->Serialize(_T("Lock"), m_posLock);
	ar->Serialize(_T("Color"), m_color);

	if (!IsReadable(ar)) {
		// 変数の更新(わかりにくい)
		GetImportSettingFile();
		GetFileType();
	}
	SerializeString(ar, _T("ImportSettingFile"), m_settingFile);
	ar->Serialize(_T("ImportFileType"), m_fileType);

	if (IsReadable(ar)) {
		AutoNaming();
	}
}

void SolidFile::OnPropertyUpdated(bool all_update)
{
	PropertyUpdated(all_update);
}

void SolidFile::AutoNaming()
{
	if (!m_autoname) { return; }

	IFile* file = GetSourceFile();
	if (file) {
		m_name = file->GetFileName();
		OnPropertyUpdated();
	}
}

// IGroup

void SolidFile::Add(IShape* shape)
{
	Insert(shape, 0);
}

void SolidFile::Insert(IShape* shape, size_t /*index*/)
{
	std::vector<boost::shared_ptr<PlotGroup>>::iterator it = boost::find(m_deletedPlots, shape);
	if (it != m_deletedPlots.end()) {
		m_plots.push_back(*it);
		m_deletedPlots.erase(it);

		OnPropertyUpdated(true);
	}
}

size_t SolidFile::Remove(IShape* shape)
{
	std::vector<boost::shared_ptr<PlotGroup>>::iterator it = boost::find(m_plots, shape);
	if (it != m_plots.end()) {
		m_deletedPlots.push_back(*it);
		m_plots.erase(it);

		OnPropertyUpdated(true);
	}

	return 0;
}

size_t SolidFile::GetChildCount() const
{
	return m_plots.size();
}

IShape* SolidFile::GetChild(size_t index) const
{
	return m_plots[index].get();
}

void SolidFile::Clear()
{
}

bool SolidFile::Include(IShape* /*shape*/) const
{
	return false;
}


void SolidFile::Select(IShape* /*shape*/, bool /*recursive*/ /* = false*/)
{
}

size_t SolidFile::GetIndex(IShape* /*shape*/) const
{
	return 0;
}


void SolidFile::BringToFront(IShape* /*child*/)
{
}

void SolidFile::SendToBack(IShape* /*child*/)
{
}

void SolidFile::ChangeZOrder(IShape* /*shape*/, size_t /*index*/)
{
}

// IGraphFile

bool SolidFile::IsEmpty() const
{
	BOOST_FOREACH(boost::shared_ptr<PlotGroup> const& plot, m_plots) {
		if (!plot->IsEmpty()) { return false; }
	}
	return true;
}

long SolidFile::GetFileId() const
{
	if (m_plots.empty()) { return -1; }
	return (long)m_plots[0]->GetFileId();
}

void SolidFile::SetFileId(long id)
{
	boost::for_each(m_plots, boost::bind(&PlotGroup::SetFileId, _1, id));

	AutoNaming();
	OnPropertyUpdated(true);
}

TCHAR const* SolidFile::GetImportSettingFile() const
{
	if (IFile* file = GetSourceFile()) {
		std::wstring fileName = GetDirNameFromDirPath(file->GetDataFileDirectory());
	
		fileName += _T("\\ImportSetting.xml");

		m_settingFile = fileName;
	}
	return m_settingFile.c_str();
}
int SolidFile::GetFileType() const
{
	if (IFile* file = GetSourceFile()) {
		m_fileType = file->GetImporterId();
	}
	return m_fileType;
}

// private

IPlotData* SolidFile::GetData() const
{
	return m_plots.empty() ? nullptr : m_plots[0]->GetData();
}

IFile* SolidFile::GetSourceFile() const
{
	return reinterpret_cast<IFile*>(m_owner->SendMessage(PI_MSG_GET_FILE_FROMID, GetFileId(), 0));
}
