/*!	@file
*/
#pragma once

#include "IGraphUnit.h"
#include "Property.h"

//TOOD: cpp作って↓のincludeをcppに移す
#include "ColorTexture.h"

class OCProperty;
struct IPropertyComposite;
class ISerializer;
class Axis;

//TODO: type-safe-enum
enum ContourAlgorithm
{
	Gridding, Triangle, Manual,
};

class SolidUnitBase : public ISolidUnit
{
public:
	SolidUnitBase()
		: m_dimension(3), m_visible(false), m_lighting(false), m_wireFrameVisible(false), m_sliceLineVisible(false)
		, m_coloring(true), m_filling(true), m_hiddenLine(true), m_wireFrameColor(RGB(0, 0, 0))
		, m_xyProjection(false), m_yzProjection(false), m_zxProjection(false)
		, m_coloringProjection(true), m_fillingProjection(true), m_sliceLineProjection(false)
		, m_contourAlgorithm(0), m_visibleCursor(true)
	{}

	void SetDimension(int dimension)	{ m_dimension = dimension; }
	int GetDimension() const			{ return m_dimension; }

	void Visible(bool visible)			{ m_visible = visible; }
	bool IsVisible() const				{ return m_visible; }

	void EnableLighting(bool enable)	{ m_lighting = enable; }
	virtual bool IsLighting() const		{ return m_lighting; }

	void EnableWireFrame(bool enable)	{ m_wireFrameVisible = enable; }
	bool IsWireFrameVisible() const		{ return m_wireFrameVisible; }

	void VisibleSliceLine(bool visible)	{ m_sliceLineVisible = visible; }
	bool IsSliceLineVisible() const		{ return m_sliceLineVisible; }

	void EnableColoring(bool enable)	{ m_coloring = enable; }
	bool IsColoring() const				{ return m_coloring; }

	void EnableFilling(bool enable)		{ m_filling = enable; }
	bool IsFilling() const				{ return m_filling; }

	void SetXYProjection(bool state)	{ m_xyProjection = state; }
	bool GetXYProjection() const		{ return m_xyProjection; }
	void SetYZProjection(bool state)	{ m_yzProjection = state; }
	bool GetYZProjection() const		{ return m_yzProjection; }
	void SetZXProjection(bool state)	{ m_zxProjection = state; }
	bool GetZXProjection() const		{ return m_zxProjection; }
	void SetColoringProjection(bool state)	{ m_coloringProjection = state; }
	bool GetColoringProjection() const		{ return m_coloringProjection; }
	void SetFillingProjection(bool state)	{ m_fillingProjection = state; }
	bool GetFillingProjection() const		{ return m_fillingProjection; }
	void SetSliceLineProjection(bool state)	{ m_sliceLineProjection = state; }
	bool GetSliceLineProjection() const		{ return m_sliceLineProjection; }
	void EnableHiddenLine(bool hidden)		{ m_hiddenLine = hidden; }
	bool IsHiddenLine() const				{ return m_hiddenLine; }
	void SetWireFrameColor(COLORREF color)	{ m_wireFrameColor = color; }
	COLORREF GetWireFrameColor() const		{ return m_wireFrameColor; }

	void VisibleCursor(bool visible)		{ m_visibleCursor = visible; }

	//!@todo Contourmap専用なので分割する
	virtual void SetContourAlgorithm(long type)		{ m_contourAlgorithm = type; }
	virtual long GetContourAlgorithm() const		{ return m_contourAlgorithm; }

	virtual void Modeling(TexVertex* /*vertexes*/, size_t /*length*/) {};
	virtual void ClearModel(){}

	virtual void Render(ColorTexture* texture, TexVertex const* vertexes, size_t length, bool clipping) const
	{
		if (m_visible) {
			InitLighting();

			RenderImpl(texture, vertexes, length, clipping);

			::glDisable(GL_NORMALIZE);
			::glDisable(GL_LIGHT0);
			::glDisable(GL_LIGHTING);

			if (IsWireFrameVisible()) {
				if (!m_hiddenLine) {
					::glDisable(GL_DEPTH_TEST);
				}
				if (!IsFilling() && IsColoring()) {
					texture->Bind();
				}
				RenderWireFrame(texture, vertexes, length, clipping);
				texture->Unbind();
				::glEnable(GL_DEPTH_TEST);
			}
		}
	}

	void AttachAxes(Axis* xAxis, Axis* yAxis, Axis* zAxis, Axis* tAxis)
	{
		m_xAxis = xAxis;
		m_yAxis = yAxis;
		m_zAxis = zAxis;
		m_tAxis = tAxis;
	}

	virtual void PreparePickup() {}
	virtual void UnpreparePickup() {}

// O-Chart
	virtual void AddProperty(IPropertyComposite* prop, bool locked) const {prop, locked;}
	virtual bool SetProperty(long id, LPARAM value, LPARAM* source)
	{
		switch (id)
		{
		case PI_3D_WIREFRAME_VISIBLE:
			if (source) { *source = IsWireFrameVisible(); }
			EnableWireFrame(!!value);
			return true;
		case PI_3D_FILLING:
			if (source) { *source = IsFilling(); }
			EnableFilling(!!value);
			return true;
		}
		return false;
	}
	virtual bool GetProperty(long id, LPARAM* ret) const
	{
		switch (id)
		{
		case PI_3D_WIREFRAME_VISIBLE:
			if (ret) { *ret = IsWireFrameVisible(); }
			return true;
		case PI_3D_FILLING:
			if (ret) { *ret = IsFilling(); }
			return true;
		}
		return false;
	}

	virtual void Serialize(ISerializer* ar) {ar;}

protected:
	void InitLighting() const
	{
		if (!IsLighting()) { return; }

		::glEnable(GL_LIGHTING);
		::glEnable(GL_LIGHT0);
		::glEnable(GL_NORMALIZE);
		::glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, 1);

		GLfloat ambient[] = { 0.5f, 0.5f, 0.5f, 1.0f }; 
		::glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);

		GLfloat diffuse[] = { 0.5f, 0.5f, 0.5f, 1.0f }; 
		::glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);

		GLfloat specular[] = { 0.5f, 0.5f, 0.5f, 1.0f }; 
		::glLightfv(GL_LIGHT0, GL_SPECULAR, specular);

		// 光源の位置は初期バージョンでは固定
		GLfloat pos[] = { -10.0f, 10.0f, 10.0f, 0.0f };
		::glLightfv(GL_LIGHT0, GL_POSITION, pos);

#ifdef _DEBUG
		SetMaterial(RGB(255, 255, 0));
		::RenderPoint(-5, 5, 5, 2); // 光源の位置
#endif
	}

	void SetMaterial(COLORREF color) const
	{
		GLfloat all[] = {GetRValue(color) / 255.0f, GetGValue(color) / 255.0f, GetBValue(color) / 255.0f, 1.0f};
		::glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, all);
	}

private:
	virtual void RenderImpl(ColorTexture* texture, TexVertex const* vertexes, size_t length, bool clipping) const = 0;
	virtual void RenderWireFrame(ColorTexture* /*texture*/, TexVertex const* /*vertexes*/, size_t /*length*/, bool /*clipping*/) const {} // = 0

protected:
	int m_dimension;
	bool m_visible;
	bool m_lighting;
	bool m_wireFrameVisible;
	bool m_sliceLineVisible; // TODO: contourmap
	bool m_coloring;
	bool m_filling;
	bool m_hiddenLine;
	COLORREF m_wireFrameColor;

	// TODO: contourmap
	bool m_xyProjection;
	bool m_yzProjection;
	bool m_zxProjection;
	bool m_coloringProjection;
	bool m_fillingProjection;
	bool m_sliceLineProjection;

	long m_contourAlgorithm;
	bool m_visibleCursor;

	//TODO: できればこれはなくしたい
	Axis* m_xAxis;
	Axis* m_yAxis;
	Axis* m_zAxis;
	Axis* m_tAxis;
};