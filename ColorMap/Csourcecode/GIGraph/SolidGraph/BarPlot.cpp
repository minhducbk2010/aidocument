/*!	@file
*/
#include "stdafx.h"
#include "BarPlot.h"
#include "TexVertex.h"
#include "ColorTexture.h"
#include "Axis.h"

BarPlot::BarPlot()
{
}

BarPlot::~BarPlot()
{
}

void BarPlot::RenderImpl(ColorTexture* texture, TexVertex const* vertexes, size_t length, bool clipping) const
{
	if (!IsLighting()) {
		texture->Bind();
	}

	if (IsFilling()) {
		for (size_t i = 0; i < length; ++i) {
			TexVertex const& v = vertexes[i];

			if (IsLighting()) {
				COLORREF const c = texture->GetColor(v.T);
				SetMaterial(c);
			}
			else {
				::glTexCoord1d(v.T);
			}

			RenderBar(GL_QUADS, texture, v, clipping);
		}
		texture->Unbind();
	}

	texture->Unbind();
}

void BarPlot::RenderWireFrame(ColorTexture* texture, TexVertex const* vertexes, size_t length, bool clipping) const
{
	::glPushMatrix();
	::glColor3ub(GetRValue(m_wireFrameColor), GetGValue(m_wireFrameColor), GetBValue(m_wireFrameColor));
	::glEnable(GL_POLYGON_OFFSET_LINE);
	::glPolygonOffset(-1.1f, -4);
	::glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	for (size_t i = 0; i < length; ++i) {
		TexVertex const& v = vertexes[i];
		::glTexCoord1d(v.T);

		RenderBar(GL_QUADS, texture, v, clipping);
	}
	::glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	::glDisable(GL_POLYGON_OFFSET_LINE);
	::glPopMatrix();
}

void BarPlot::RenderBar(GLenum mode, ColorTexture* /*texture*/, TexVertex const& vv, bool clipping) const
{
	if (clipping &&
		(std::abs(vv.X) > m_xAxis->GetSize() || std::abs(vv.Y) > m_yAxis->GetSize()))
	{
		return;
	}

	TexVertex v = vv;
	if (clipping) {
		// Z���̍ő�l�𒴂���l�́A�V��܂łŎ~�߂�
		v.Z = std::min<GLfloat>(v.Z, static_cast<GLfloat>(m_zAxis->GetSize()));
	}

	double const width = 0.5;
	double const bottom = -m_zAxis->GetSize();

	// ���͂̕�
	::glBegin(mode); {
		::glNormal3f(-10.0f, 10.0f, 10.0f);
		::glVertex3d(v.X - width, v.Z,		v.Y + width);
		::glVertex3d(v.X - width, v.Z,		v.Y - width);
		::glVertex3d(v.X - width, bottom,	v.Y - width);
		::glVertex3d(v.X - width, bottom,	v.Y + width);
	} ::glEnd();

	::glBegin(mode); {
		::glNormal3f(-10.0f, 10.0f, 10.0f);
		::glVertex3d(v.X + width, v.Z,		v.Y + width);
		::glVertex3d(v.X + width, v.Z,		v.Y - width);
		::glVertex3d(v.X + width, bottom,	v.Y - width);
		::glVertex3d(v.X + width, bottom,	v.Y + width);
	} ::glEnd();

	::glBegin(mode); {
		::glNormal3f(-10.0f, 10.0f, 10.0f);
		::glVertex3d(v.X + width, v.Z,		v.Y - width);
		::glVertex3d(v.X - width, v.Z,		v.Y - width);
		::glVertex3d(v.X - width, bottom,	v.Y - width);
		::glVertex3d(v.X + width, bottom,	v.Y - width);
	} ::glEnd();

	::glBegin(mode); {
		::glNormal3f(-10.0f, 10.0f, 10.0f);
		::glVertex3d(v.X + width, v.Z,		v.Y + width);
		::glVertex3d(v.X - width, v.Z,		v.Y + width);
		::glVertex3d(v.X - width, bottom,	v.Y + width);
		::glVertex3d(v.X + width, bottom,	v.Y + width);
	} ::glEnd();

	// �V��
	::glBegin(mode); {
		::glNormal3f(-10.0f, 10.0f, 10.0f);
		::glVertex3d(v.X + width, v.Z,		v.Y + width);
		::glVertex3d(v.X + width, v.Z,		v.Y - width);
		::glVertex3d(v.X - width, v.Z,		v.Y - width);
		::glVertex3d(v.X - width, v.Z,		v.Y + width);
	} ::glEnd();

	// ��
	::glBegin(mode); {
		::glNormal3f(-10.0f, 10.0f, 10.0f);
		::glVertex3d(v.X + width, bottom,	v.Y + width);
		::glVertex3d(v.X + width, bottom,	v.Y - width);
		::glVertex3d(v.X - width, bottom,	v.Y - width);
		::glVertex3d(v.X - width, bottom,	v.Y + width);
	} ::glEnd();
}