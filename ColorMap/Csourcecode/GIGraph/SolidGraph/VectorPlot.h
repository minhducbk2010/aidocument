/*!	@file
	@brief	�x�N�g���}
*/
#pragma once

struct IPropertyComposite;
class ISerializer;
class ColorTexture;
class Axis;
class IMessageReceiver;

struct Vector
{
	double x;
	double y;
	double z;
	double vx;
	double vy;
	double vz;
};

class VectorPlot
{
public:
	VectorPlot();
	~VectorPlot();

	void Render(ColorTexture* texture, Vector* vectors, size_t length, bool clipping) const;

	void AttachOwner(IMessageReceiver* owner) { m_owner = owner; }
	void AttachAxes(Axis* xAxis, Axis* yAxis, Axis* zAxis, Axis* tAxis)
	{
		m_xAxis = xAxis;
		m_yAxis = yAxis;
		m_zAxis = zAxis;
		m_tAxis = tAxis;
	}

#if 1
	virtual void AddProperty(IPropertyComposite* prop, bool locked) const;
	virtual bool SetProperty(long id, LPARAM value, LPARAM* source);

	virtual void Serialize(ISerializer* ar);
#endif

private:
	double m_norm;
	double m_arrowSize;
	bool m_centering;
	double m_threshold;

	IMessageReceiver* m_owner;
	Axis* m_xAxis;
	Axis* m_yAxis;
	Axis* m_zAxis;
	Axis* m_tAxis;
};
