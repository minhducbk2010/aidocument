#pragma once

#include <boost/noncopyable.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/signals2.hpp>
#include "IGraph.h"
#include "NullableShape.h"

struct TexVertex;
class SolidUnitBase;
class ScatterPlot;
class IPlotData;
class Axis;
class ColorBar;
class Legend;
// O-Chart
class OCProperty;
struct PropertyNode;
struct IPropertyComposite;
class ISerializer;
class ColorTexture;
struct RenderState;
class CrossCursor;
class IMessageReceiver;
class SolidGraph;

class PlotGroup : public NullableShape, public IDigitalmapHolder, public boost::noncopyable
{
public:
	PlotGroup(IMessageReceiver* owner, SolidGraph* graph, CrossCursor* cursor);
	~PlotGroup();

	virtual void Destroy() { delete this; }

	void SetDimension(int dimension);
	int GetDimension() const;
	void SimpleVisiblePlot(size_t index, bool visible);
	void VisiblePlot(size_t index, bool visible);
	void VisibleSubPlot(size_t index);
	void ToggleVisibleSubPlot(size_t index);
	void EnableMarker(bool enable);
	void SelectOnlyMarkerVisible();
	bool IsVisibleMarker() const;
	size_t GetVisiblePlotIndex() const;
	size_t GetVisibleSubPlotIndex() const;
	COLORREF GetPlotColor() const { return m_color; }

	void EnableLighting(bool enable);
	bool IsLighting() const;
	void EnableWireFrame(bool enable);
	bool IsWireFrameVisible() const;
	void EnableColoring(bool enable);
	bool IsColoring() const;
	void EnableFilling(bool enable);
	bool IsFilling() const;
	void SetVisibleSliceLine(bool visible);
	bool GetVisibleSliceLine() const;
	void EnableHiddenLine(bool hidden);
	bool IsHiddenLine() const;
	double GetMarkerSize() const;
	void SetMarkerSize(double size);

	void SetXYProjection(bool state);
	bool GetXYProjection() const;
	void SetYZProjection(bool state);
	bool GetYZProjection() const;
	void SetZXProjection(bool state);
	bool GetZXProjection() const;
	void SetColoringProjection(bool state);
	bool GetColoringProjection() const;
	void SetFillingProjection(bool state);
	bool GetFillingProjection() const;
	void SetSliceLineProjection(bool state);
	bool GetSliceLineProjection() const;
	void SetContourAlgorithm(long type);
	long GetContourAlgorithm() const;
	void VisibleCursor(bool visible);

	ColorTexture* GetColorPattern() const { return m_colorPattern.get(); }

	bool UseDigitalmap() const;
	void ShowDigitalmap();

	void SelectCursor(int xIndex, int yIndex);
	void GetCursorIndex(int* xIndex, int* yIndex) const;

	int HitTest(RenderState* state, int x, int y);
	int PickUpData(RenderState* state, int x, int y);

	size_t GetFileId() const;
	void SetFileId(size_t id);
	IPlotData* GetData() const { return m_data.get(); }
	void SetData(IPlotData* data);
	void ChangeZValue(int index, double z);
	void Modeling();

	SolidUnitBase* GetPlot(size_t index) { return &m_units[index]; }
	SolidUnitBase const* GetPlot(size_t index) const { return &m_units[index]; }

	void Render(bool clipping) const;
	void RenderColorBar(bool subPlot, bool labelVisible) const;

	void AttachAxes(Axis* xAxis, Axis* yAxis, Axis* zAxis, Axis* tAxis);
	void AttachLegend(Legend* legend) { m_legend = legend; }

	void Select(bool select);
	bool IsSelected() const { return m_selected; }

	bool IsEmpty() const;

	void CopyFrom(PlotGroup* src);

	bool CheckUseColorPattern(TCHAR const* path);
	bool UpdateColorPattern(TCHAR const* path);

	void SelectMarker(int index);

#if 1
	// Shape
	void GetName(wchar_t* name, size_t length) const;
	std::wstring GetName() const { return m_name; }
	unsigned long GetTypeId() const;

	bool IsVisible() const { return m_visible; }

	bool IsDeletable() const { return true; }

	int HitTest(double x, double y) const;

	PropertyNode* CreateProperty();
	PropertyNode* CreateTabProperty();

	void AddProperty(IPropertyComposite* prop, bool locked) const;
	void AddShowProperty(IPropertyComposite* prop, bool locked) const;
	void AddPlotProperty(IPropertyComposite* prop, bool locked) const;
	bool SetProperty(long id, LPARAM value, LPARAM* source);
	bool SetPropertyForAllUnit(long id, LPARAM value, LPARAM* source);
	bool GetProperty(long id, LPARAM* ret) const;

	//
	void SetDataProperty(int current, int dim, LPARAM value, LPARAM* source);

	void Serialize(ISerializer* ar);

#endif

	// Events
	boost::signals2::signal<void(bool)> PropertyUpdated;
	boost::signals2::signal<void(std::wstring const&)> ColorPatternChanged;
	boost::signals2::signal<void(Axis*)> DataChanged;

private:
	void InitColorBar() const;
	void GetVertexes(std::vector<TexVertex>& vertexes) const;
	void OnPropertyUpdated(bool all_update = false);

private:
	IMessageReceiver* m_owner;
	SolidGraph* m_graph;

	boost::shared_ptr<IPlotData> m_data;
	boost::ptr_vector<SolidUnitBase> m_units;
	boost::scoped_ptr<SolidUnitBase> m_subUnit;
	boost::scoped_ptr<ScatterPlot> m_marker;
	boost::shared_ptr<ColorTexture> m_colorPattern;
	boost::shared_ptr<ColorBar> m_colorBar;
	Legend* m_legend;

	std::vector<TexVertex> m_vertexes;

	Axis* m_xAxis;
	Axis* m_yAxis;
	Axis* m_zAxis;
	Axis* m_tAxis;

	// Property
	bool m_visible;
	std::wstring m_name;
	COLORREF m_color;
	bool m_clipping;

	bool m_selected;
	bool m_empty;

	bool m_nameInitialized; // 名前設定したかどうか(最初だけ自動設定する)
};
