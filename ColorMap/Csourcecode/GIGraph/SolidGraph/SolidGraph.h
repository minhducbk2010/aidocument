/*!	@file
*/
#pragma once

#include <vector>
#include <deque>
#include <boost/shared_ptr.hpp>
#include <boost/ptr_container/ptr_vector.hpp>
#include "Graph3DBase.h"
#include "type_safe_enum.h"

struct GraphType_def {
  enum type {
	  File,
	  Item,
  };
};

typedef safe_enum<GraphType_def, int> GraphType;

typedef std::vector<boost::shared_ptr<PlotGroup>> PlotList;

class SolidFile;
class PlotGroup;
class SolidUnitBase;
class VectorPlot;
class CrossCursor;
class Legend;

class SolidGraph : public Graph3DBase
{
public:
	SolidGraph(HWND window, IMessageReceiver* owner);
	virtual ~SolidGraph();

	virtual void SetParent(IGroup* parent);
	virtual void SetSelected(bool select);

	virtual int GetTypeId() const { return 26; }
	virtual void SetData(size_t index ,IPlotData* data);
	virtual IGraphFile* AddFile(size_t fileId);
	virtual IShape* AddData(IPlotData* data);
	virtual IShape* AddPlot(IPlotData* data, size_t kind);
	virtual void RemovePlot(size_t index);
	virtual void ClearPlots();
	virtual void SetPlotKind(int kind);
	void SetPlotKind(int index, int kind);
	virtual void SetPlotVisible(int index, int kind, bool visible);

	virtual size_t GetFileCount() const;
	virtual IGraphFile* GetFile(size_t index) const;
	virtual IGraphFile* GetEmptyFile() const;

	void SimpleVisiblePlot(size_t type);
	void VisiblePlot(size_t type, bool visible);
	void VisibleSubPlot(UINT type);
	void ToggleVisibleSubPlot(UINT type);
	void EnableMarker(bool enable);
	bool IsVisibleMarker() const;
	virtual void EnableLighting(bool enable);
	virtual bool IsLighting() const;
	void SetVisibleSliceLine(bool visible);
	bool GetVisibleSliceLine() const;
	virtual void EnableHiddenLine(bool hidden);
	virtual bool IsHiddenLine() const;
	virtual void VisibleCursor(bool visible);

	void SetXYProjection(bool state);
	bool GetXYProjection() const;
	void SetYZProjection(bool state);
	bool GetYZProjection() const;
	void SetZXProjection(bool state);
	bool GetZXProjection() const;
	void SetColoringProjection(bool state);
	bool GetColoringProjection() const;
	void SetFillingProjection(bool state);
	bool GetFillingProjection() const;
	void SetSliceLineProjection(bool state);
	bool GetSliceLineProjection() const;
	void SetContourAlgorithm(long type);
	long GetContourAlgorithm() const;

	GraphType GetGraphType() const { return m_graphType; }

	virtual BOOL GetViewColorBar() const;

	virtual bool UseDigitalmap() const;
	virtual void ShowDigitalmap();
	
	virtual void ResetCursor();
	
	void Modeling();

	virtual bool CheckUseColorPattern(TCHAR const* path);
	virtual bool UpdateColorPattern(TCHAR const* path);

// Group
	virtual size_t GetChildCount() const;
	virtual IShape* GetChild(size_t index);

	virtual void RemoveChild(IShape* obj);
	virtual void InsertChild(IShape* obj);

// Controller
	virtual int KeyDownCursorOffset() const { return -1; }

	virtual bool EnableCursor() const;
	virtual void SetCursor(int x, int y);
	virtual void SelectIndex(int index, int line);
	virtual void GetCursorIndex(int* index, int* line) const;
	
	virtual bool PickUpData(HDC dc, int x, int y);
	virtual bool HitTestMarker(HDC dc, int x, int y);
	virtual bool DragMarker(OCPoint const& offset);
	virtual void EndMarkerDrag();

	IShape* GetLegend() const;

	virtual bool CanAddFile() const;
	virtual bool CheckUseFile(size_t fileId) const;
	virtual void UpdatePlotData();

	virtual int GetActivePlotIndex() const;
	virtual COLORREF GetActivePlotColor() const;

// GraphLib
	void SetUseSection(bool use) { m_useSection = use; }
	void SelectMarker(int index) { m_selectedIndex = index; }

	boost::signals2::signal<void(long, long)> MarkerClicked;
	boost::signals2::signal<void(long, long, double)> MarkerValueChanged;

protected:
	virtual void RenderPlots(HDC dc) const;
	virtual IPlotData* GetData(size_t index) const;
	virtual size_t GetDataCount() const;
	virtual bool EnableSection() const;

#if 1
	virtual void SetDimension(int dim);
	virtual void AddPropertyImpl(OCProperty* prop, bool locked) const;
	virtual void AddTabPropertyImpl(OCProperty* prop, bool locked) const;
	virtual bool SetPropertyImpl(long id, LPARAM value, LPARAM* source);
	virtual bool GetPropertyImpl(long id, LPARAM* ret) const;
	virtual void SerializeImpl(ISerializer* ar);
#endif

	virtual Graph3DBase* Create() const { return new SolidGraph(m_wnd, m_owner); }

public:
// Override
	virtual void RenderColorBar() const;

	virtual BOOL GetViewWireframe() const;
	virtual void SetViewWireframe(bool value);
	virtual void EnableFilling(bool enable);
	virtual BOOL IsFilling() const;
	virtual void EnableColoring(bool enable);
	virtual BOOL IsColoring() const;

	virtual void AfterAutoScaleAxes();

// Operation
	void AddPlotGroup(PlotGroup* group, bool serialize = false);
	void AddPlotGroupToFile(PlotGroup* group);
	void AddFile(SolidFile* file, bool update = false);
	void UpdateData(IPlotData* data);

	void RefreshSection();

	virtual void SetLocation(double left, double top);
	PlotList GetPlots() const;
	PlotGroup* GetPlot(size_t index) const;
	size_t GetPlotCount() const;
	PlotGroup* GetActivePlot() const;

	PlotGroup* GetEmptyPlot() const;

// Commands
	void ToggleSliceLineVisible();
	void ToggleLighting();
	void ToggleVisibleMarker();
	void ToggleXYProjection();
	void ToggleYZProjection();
	void ToggleZXProjection();
	void ToggleColorProjection();
	void ToggleFillProjection();
	void ToggleSliceLineProjection();
	void ToggleSelectPlot();
	virtual void ResetPosition();

	void HideSection();
	
// MessageHandler
	void GetPlotKind(size_t targetKind, BOOL* enabled, BOOL* checked) const;
	void GetSubPlotKind(size_t targetKind, BOOL* enabled, BOOL* checked) const;
	void EnabledContourCommand(boost::function<BOOL()> f, BOOL* enabled, BOOL* checked) const;
	virtual void EnabledColorBar(BOOL* enabled, BOOL* checked) const;
	void EnableMarkerCommand(BOOL* enabled, BOOL* checked) const;
	void EnableLightingCommand(BOOL* enabled, BOOL* checked) const;
	void GetSelectPlot(BOOL* enabled, BOOL* checked) const;

// Property
	virtual void AddShowPropertyOption(IPropertyGroup* group, bool locked);
	template<class T>
	void AddPlotProperty(T* parent, bool locked) const;

	void CrossCursorChanged() const;
	void ColorPatternChanged(std::wstring const& pattern);

	void SetGraphType(GraphType type);

	OCRect GetParentRect() const;

private:
	boost::shared_ptr<CrossCursor> m_cursor;				//!< カーソル
	std::deque<boost::shared_ptr<SolidFile>> m_files;		//!< ファイル
	std::deque<boost::shared_ptr<SolidFile>> m_deletedFiles;//!< 削除済みのファイル(Undo用)

	boost::shared_ptr<VectorPlot> m_vectorPlot;				//!< ベクトル図
	boost::shared_ptr<Legend> m_legend;						//!< 凡例
	std::deque<IShape*> m_child;							//!< 子要素すべて(実体は↑)
	std::deque<IShape*> m_deletedChild;						//!< 削除済意味の子要素(Undo用)

	GraphType m_graphType;									//!< グラフタイプ
	size_t m_activePlot;									//!< アクティブプロット

	boost::shared_ptr<Axis> m_vxAxis;
	boost::shared_ptr<Axis> m_vyAxis;
	boost::shared_ptr<Axis> m_vzAxis;

	int m_selectedIndex;									//!< 選択しているデータ

	boost::shared_ptr<PlotGroup> m_dummyPlot;

	// GraphLib向け
	bool m_useSection;

	bool m_dragMarker;
	double m_dragValue;
};
