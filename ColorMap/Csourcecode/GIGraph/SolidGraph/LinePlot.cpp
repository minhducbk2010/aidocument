#include "stdafx.h"
#include "LinePlot.h"
#include "TexVertex.h"
#include "Clipper.h"
#include "Axis.h"

LinePlot::LinePlot()
{
}

LinePlot::~LinePlot()
{
}

void LinePlot::RenderImpl(ColorTexture* /*texture*/, TexVertex const* vertexes, size_t length, bool clipping) const
{
	Clipper clipper(m_xAxis->GetSize(), m_yAxis->GetSize(), m_zAxis->GetSize());
	if (clipping) {
		clipper.Clipping();
	}

	::glColor3ub(0, 0, 0);

	::glBegin(GL_LINE_STRIP);
	for (size_t i = 0; i < length; ++i) {
		TexVertex const& v = vertexes[i];
		::glVertex3d(v.X, v.Z, v.Y);
	}
	::glEnd();
}