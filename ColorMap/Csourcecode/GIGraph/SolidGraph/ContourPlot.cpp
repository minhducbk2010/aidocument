/*!	@file
*/
#include "stdafx.h"
#include <boost/foreach.hpp>
#include <boost/scoped_ptr.hpp>
#include "ContourPlot.h"
#include "ColorTexture.h"
#include "Surface/GriddingSurface.hpp"
#include "Surface/TriangleSurface.hpp"
#include "Selector.h"
#include "RenderState.h"
#include "CrossCursor.h"
//digitalmap
#include "IMessageReceiver.h"
#include "IPlotData.h"
#include "Axis.h"
#include "Clipper.h"
#include "File/File.h"
#include "OToolboxFunction.hpp"

int const MESH_NUM[] = {5, 10, 20, 30, 50};
double const ALPHA_NUM[] = {2, 8, 32, 128, 1024,};

//! スケールからスライスレベルの値を計算します
std::vector<double> CalcSliceLevel(double dMax, double dMin, double dDiv)
{
    std::vector<double> arr;
	if (IsZero(dDiv)) { return arr; }

    double dLevel = dMin;
    for(int i = 0; dLevel < dMax; ++i)
    {
        dLevel = dMin + (dDiv * i);
        if(dLevel > dMax) { break; }

        arr.push_back(dLevel);
    }
    return arr;
}

ContourPlot::ContourPlot(CrossCursor* cursor, IMessageReceiver* owner)
	: m_surface(new OCGriddingSurface())
	, m_mesh(2), m_alpha(2)
	, m_cursor(cursor), m_owner(owner)
{
	m_cursor->Add(this);
}

ContourPlot::~ContourPlot()
{
	m_cursor->Erase(this);
}

void ContourPlot::SetContourAlgorithm(long type)
{
	__super::SetContourAlgorithm(type);

	switch (type)
	{
	case Gridding:
	case Manual:
		m_surface.reset(new OCGriddingSurface());
		break;
	case Triangle:
		m_surface.reset(new OCTriangleSurface());
		break;
	}
	if (type != Manual) {
		m_manualX.clear();
		m_manualY.clear();
	}
}

bool ContourPlot::EnableCursor() const
{
	if (OCGriddingSurface* contour = dynamic_cast<OCGriddingSurface*>(m_surface.get())) {
		return !m_data.x.empty();
	}
	return false;
}

void ContourPlot::SetCursor(RenderState* state, int x, int y)
{
	OCGriddingSurface* gridding = dynamic_cast<OCGriddingSurface*>(m_surface.get());
	if (!gridding) { return; }

	int xIndex = -1, yIndex = -1;
	{
		Selector selector(x, y);
		state->Bind();
		gridding->DrawXLines();
		xIndex = selector.GetPoint();
		state->Unbind();
	}
	{
		Selector selector(x, y);
		state->Bind();
		gridding->DrawYLines();
		yIndex = selector.GetPoint();
		state->Unbind();
	}

	if (xIndex == -1 || yIndex == -1) { return; }

	SelectCursor(xIndex, yIndex);
}

void ContourPlot::SelectCursor(int xIndex, int yIndex)
{
	if (OCGriddingSurface* gridding = dynamic_cast<OCGriddingSurface*>(m_surface.get())) {
		int const len = gridding->GetMeshNumX();
		xIndex = std::min<int>(std::max<int>(xIndex, 0), len - 1);
		yIndex = std::min<int>(std::max<int>(yIndex, 0), len - 1);

		m_cursor->Select(xIndex, yIndex);
	}
}

void ContourPlot::GetSectionData(std::vector<OCPoint>* x, std::vector<OCPoint>* y) const
{
	OCGriddingSurface* gridding = dynamic_cast<OCGriddingSurface*>(m_surface.get());
	if (!gridding) { return; }

	if (!m_cursor->IsEffective()) { return; }

	if (m_cursor->IsActive(this)) {
		std::vector<double> const& xData = m_surface->GetGridX();
		std::vector<double> const& yData = m_surface->GetGridY();
		std::vector<double> const& zData = m_surface->GetGridZ();
		if (xData.empty()) { return; }

		int const len = gridding->GetMeshNumX();
		int const xIndex = m_cursor->GetXIndex();
		int const yIndex = m_cursor->GetYIndex();
		if (xIndex < 0 || yIndex < 0) { return; }

		for (int i = 0; i < len; ++i) {
			int const nx = i + len * xIndex;
			y->push_back(OCPoint(yData[nx], xData[nx]));

			int const ny = i * len + yIndex;
			x->push_back(OCPoint(-zData[ny], yData[ny]));
		}
	}
	else {
		TexVertex pos = m_cursor->GetCursorPos(0);
		BOOST_FOREACH(Vertex const& v, InterpolateXLines(pos.Y)) {
			y->push_back(OCPoint(v.Y, v.X));
		}

		BOOST_FOREACH(Vertex const& v, InterpolateYLines(pos.X)) {
			x->push_back(OCPoint(-v.Z, v.Y));
		}
	}
}

TexVertex ContourPlot::GetCursorPos(int xIndex, int yIndex) const
{
	if (OCGriddingSurface* gridding = dynamic_cast<OCGriddingSurface*>(m_surface.get())) {
		std::vector<double> const& xData = m_surface->GetGridX();
		std::vector<double> const& yData = m_surface->GetGridY();
		std::vector<double> const& zData = m_surface->GetGridZ();

		int const len = gridding->GetMeshNumX();
		int const index = yIndex + xIndex * len;
		if ((size_t)index < xData.size()) {
			return TexVertex(0, 0, xData[index], zData[index], yData[index]);
		}
	}
	return TexVertex();
}

//! (x, y)の位置にあるZ値を補間計算します
double ContourPlot::InterpolateValue(double x, double y) const
{
	if (OCGriddingSurface* gridding = dynamic_cast<OCGriddingSurface*>(m_surface.get())) {
		std::vector<double> xData = m_surface->GetGridX();
		std::vector<double> yData = m_surface->GetGridZ();
		std::vector<double> const& zData = m_surface->GetGridY();

		boost::sort(xData);
		boost::sort(yData);
		xData.erase(std::unique(xData.begin(), xData.end()), xData.end());
		yData.erase(std::unique(yData.begin(), yData.end()), yData.end());

		if (xData.empty() || yData.empty() ||
			x < xData[0] || x > xData.back() ||
			y < yData[0] || y > yData.back()) { return FLT_MAX; }
		if (xData.size() * yData.size() >= zData.size()) { return FLT_MAX; }

		std::vector<double> line;
		OCSpline spline;
		for (size_t i = 0; i < yData.size(); ++i) {
			size_t const idx = (yData.size() - i - 1) * xData.size();
			spline.Init((int)xData.size(), (double*)&xData[0], (double*)&zData[idx]);
			spline.Prepare();
			line.push_back(spline.Calc(x));
		}

		spline.Init((int)yData.size(), (double*)&yData[0], (double*)&line[0]);
		spline.Prepare();
		return spline.Calc(y);
	}
	return FLT_MAX;
}

void ContourPlot::GetCursor(int* x, int* y) const
{
	*x = m_cursor->GetXIndex();
	*y = m_cursor->GetYIndex();
}

void ContourPlot::GetIndexLine(double x, double z, int *index, int *line) const
{
	x, z, index, line;
}

void ContourPlot::ResetCursor()
{
	m_cursor->Reset();
}

void ContourPlot::ShowDigitalmap(IPlotData* data)
{
	ActivateToolbox();

	const int CHAR_COUNT = 256;	// OdfReaderWriter内で決まっている
	const int ITEM_COUNT = 3;
	// ODFファイルに必要なデータを集める
	// 項目名を一列に格納
	const wchar_t* xItem = data->GetItemName(0);
	const wchar_t* yItem = data->GetItemName(1);
	const wchar_t* zItem = data->GetItemName(2);
	wchar_t itemName[ITEM_COUNT * CHAR_COUNT];
	for (int i = 0; i < wcslen(xItem) + 1; i++)
	{
		itemName[i] = xItem[i];
	}
	for (int i = 0; i < wcslen(yItem) + 1; i++)
	{
		itemName[i + CHAR_COUNT] = yItem[i];
	}
	for (int i = 0; i < wcslen(zItem) + 1; i++)
	{
		itemName[i + CHAR_COUNT * 2] = zItem[i];
	}
	// 単位名
	const wchar_t* xUnit = data->GetUnitName(0);
	const wchar_t* yUnit = data->GetUnitName(1);
	const wchar_t* zUnit = data->GetUnitName(2);
	wchar_t unitName[ITEM_COUNT * CHAR_COUNT];
	for (int i = 0; i < wcslen(xUnit) + 1; i++)
	{
		unitName[i] = xUnit[i];
	}
	for (int i = 0; i < wcslen(yUnit) + 1; i++)
	{
		unitName[i + CHAR_COUNT] = yUnit[i];
	}
	for (int i = 0; i < wcslen(zUnit) + 1; i++)
	{
		unitName[i + CHAR_COUNT * 2] = zUnit[i];
	}
	// データ
	int dataCount = data->GetDataCount();
	int totalCount = dataCount * ITEM_COUNT;
	float* xyz = new float[totalCount];
	for (size_t i = 0; i < data->GetDataCount(); ++i) {
		real_t const dx = data->GetData(0, i);
		real_t const dy = data->GetData(1, i);
		real_t const dz = data->GetData(2, i);
		//if (dx != FLT_MAX && dy != FLT_MAX && dz != FLT_MAX) {
			xyz[i] = dx;
			xyz[i + dataCount] = dy;
			xyz[i + dataCount * 2] = dz;
		//}
	}
	// ファイル名(Toolboxに表示する)
	long id = data->GetFileId(0);
	OCFile* file = reinterpret_cast<OCFile*>(m_owner->SendMessage(PI_MSG_GET_FILE_FROMID, id, 0));
	CreateOdfFile(itemName, unitName, xyz, dataCount, file->GetFileName(), m_owner);
	delete[] xyz;
	
	if(!SendMessageToToolbox())
	{
		HWND hWnd = reinterpret_cast<HWND>(m_owner->SendMessage(PI_MSG_GET_VIEW_HANDLE, 0, 0));
		std::wstring msg = ::GetResourceString(m_owner,"ID_SOURCE_TEXT_FAIL_CALL_DIGITAL_MAP");
		::MessageBox(hWnd, msg.c_str(), ::GetResourceString(m_owner,"ID_SOURCE_TEXT_O_CHART"), MB_OK|MB_ICONEXCLAMATION);
	}
}

void ContourPlot::SetValue(int index, double z)
{
	if (OCGriddingSurface* surface = dynamic_cast<OCGriddingSurface*>(m_surface.get())) {
		double const pz = z == FLT_MAX ? FLT_MAX : m_zAxis->ValueToPosition(z);
		surface->SetValue(index, pz);
	}
}

void ContourPlot::SetGridData(std::vector<double> const& x, std::vector<double> const& y, std::vector<double> const& z, size_t xLen, size_t yLen)
{
	if (x.empty() || y.empty() || z.empty()) {
		SetContourAlgorithm(Gridding);
		return;
	}

	SetContourAlgorithm(Manual);
	m_surface->SetDecor(&m_decor);
	m_surface->SetData(&m_data);
	if (OCGriddingSurface* surface = dynamic_cast<OCGriddingSurface*>(m_surface.get())) {
		// 中では値ではなく座標で持っているので変換しておく
		std::vector<double> xInput = x, yInput = y, zInput = z;
		for (size_t i = 0; i < x.size(); ++i) {
			xInput[i] = m_xAxis->ValueToPosition(xInput[i]);
			yInput[i] = m_yAxis->ValueToPosition(yInput[i]);
			zInput[i] = zInput[i] == FLT_MAX ? FLT_MAX : m_zAxis->ValueToPosition(zInput[i]);
		}

		surface->SetGrid(&xInput[0], &zInput[0], &yInput[0], (int)xLen, (int)yLen);
	}
}

void ContourPlot::ClearGridData()
{
	if (OCGriddingSurface* surface = dynamic_cast<OCGriddingSurface*>(m_surface.get())) {
		surface->Clear();
	}
}

#if 1

#include "PropertyCreator.hpp"
#include "ISerializer.h"

void ContourPlot::AddProperty(IPropertyComposite* prop, bool locked) const
{
	locked = locked || !IsVisible();

	IPropertyGroup* grp = prop->AddGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_MAP_OPERATION"));
	int const interType = GetContourAlgorithm() == Manual ? Gridding : GetContourAlgorithm();

	std::wstring assistProp1 = GetResourceString(m_owner, "ID_SOURCE_TEXT_CURVATURE_MIN_METHOD");
	std::wstring assistProp2 = GetResourceString(m_owner, "ID_SOURCE_TEXT_TRIANGLE_SPLIT_PLOT_DESIGN");
	grp->AddArrayProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_TYPE_BETWEEN_ASSISTANT"), PI_3D_CALCULATE_TYPE, interType, locked, 2, 
		assistProp1.c_str(), assistProp2.c_str());

	/*if (m_interType == Gridding)*/ {
		std::wstring latticeProp1 = GetResourceString(m_owner, "ID_SOURCE_TEXT_COARSE");
		std::wstring latticeProp2 = GetResourceString(m_owner, "ID_SOURCE_TEXT_SLIGHTLY_COARSE");
		std::wstring latticeProp3 = GetResourceString(m_owner, "ID_SOURCE_TEXT_NORMAL");
		std::wstring latticeProp4 = GetResourceString(m_owner, "ID_SOURCE_TEXT_SLIGHTLY_PARTICULAR");
		std::wstring latticeProp5 = GetResourceString(m_owner, "ID_SOURCE_TEXT_PARTICULAR");
		grp->AddArrayProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_LATTICE"), PI_CONTOUR_PM, m_mesh, locked || interType != Gridding,
			5, latticeProp1.c_str(),latticeProp2.c_str(), latticeProp3.c_str(), latticeProp4.c_str(), latticeProp5.c_str()); 

		std::wstring surfaceProp1 = GetResourceString(m_owner, "ID_SOURCE_TEXT_SMOOTH");
		std::wstring surfaceProp2 = GetResourceString(m_owner, "ID_SOURCE_TEXT_SLIGHTLY_SMOOTH");
		std::wstring surfaceProp3 = GetResourceString(m_owner, "ID_SOURCE_TEXT_NORMAL");
		std::wstring surfaceProp4 = GetResourceString(m_owner, "ID_SOURCE_TEXT_SLIGHTLY_SHARP");
		std::wstring surfaceProp5 = GetResourceString(m_owner, "ID_SOURCE_TEXT_SHARP");
		grp->AddArrayProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_CURVED_SURFACE"), PI_CONTOUR_ALPHA, m_alpha, locked || interType != Gridding,
			5, surfaceProp1.c_str(), surfaceProp2.c_str(), surfaceProp3.c_str(), surfaceProp4.c_str(), surfaceProp5.c_str());
	}
}

bool ContourPlot::SetProperty(long id, LPARAM value, LPARAM* source)
{
	switch (id)
	{
	case PI_3D_CALCULATE_TYPE:
		if (source) { *source = GetContourAlgorithm(); }
		SetContourAlgorithm(static_cast<long>(value));
		return true;
	case PI_CONTOUR_PM:
		::SetPropertyHelper(m_mesh, value, source);
		return true;
	case PI_CONTOUR_ALPHA:
		::SetPropertyHelper(m_alpha, value, source);
		return true;
	case PI_3D_SLICELINE_VISIBLE:
		{
			BOOL cur = IsSliceLineVisible();
			::SetPropertyHelper(cur, value, source);
			VisibleSliceLine(!!cur);
		}
		return true;
	}
	return __super::SetProperty(id, value, source);
}

void ContourPlot::Serialize(ISerializer* ar)
{
	__super::Serialize(ar);
	ar->Serialize(_T("SliceLine"), m_sliceLineVisible);
	ar->Serialize(_T("XYProjection"), m_xyProjection);
	ar->Serialize(_T("YZProjection"), m_yzProjection);
	ar->Serialize(_T("ZXProjection"), m_zxProjection);
	ar->Serialize(_T("SliceLineProjection"), m_sliceLineProjection);
	ar->Serialize(_T("PaintProjection"), m_fillingProjection);
	ar->Serialize(_T("ColorProjection"), m_coloringProjection);
	ar->Serialize(_T("Mesh"), m_mesh, 2);
	ar->Serialize(_T("Alpha"), m_alpha, 2);

	if (IReadable* reader = dynamic_cast<IReadable*>(ar)) {
		if (reader->GetMajorVersion() == 2) {
			double meshNum;
			double alphaNum;
			ar->Serialize(_T("MeshNum"), meshNum);
			ar->Serialize(_T("AlphaNum"), alphaNum);

			m_mesh = m_alpha = 2;
			int n = 0;
			BOOST_FOREACH(double c, MESH_NUM) {
				if (c == meshNum) {
					m_mesh = n;
					break;
				}
				++n;
			}
			n = 0;
			BOOST_FOREACH(double c, ALPHA_NUM) {
				if (c == alphaNum) {
					m_alpha = n;
					break;
				}
				++n;
			}
		}

		long type = -1;
		if (ISerializer* child = ar->ChildNode(_T("Plot"))) { // 旧ver
			child->Serialize(_T("InterType"), type, -1);

			// 3.2以前のカーソル値
			if (reader->GetMajorVersion() == 3 && reader->GetMinorVersion() <= 2) {
				OCPoint cursor;
				child->Serialize(_T("CursorPosition"), cursor);
				m_cursor->Select((int)cursor.x, (int)cursor.y);
			}
		}

		if (type == -1) {
			ar->Serialize(_T("Algorithm"), type, Gridding);
		}
		SetContourAlgorithm(type);

		// メッシュデータの読み込み(データがあれば)
		if (IReadable* child = dynamic_cast<IReadable*>(ar->ChildNode(_T("MeshX")))) {
			size_t const count = child->ChildCount();
			m_manualX.resize(count);
			for (size_t i = 0; i < count; ++i) {
				ISerializer* v = child->ChildNode(i);
				v->Serialize(_T("X"), m_manualX[i]);
			}
		}
		if (IReadable* child = dynamic_cast<IReadable*>(ar->ChildNode(_T("MeshY")))) {
			size_t const count = child->ChildCount();
			m_manualY.resize(count);
			for (size_t i = 0; i < count; ++i) {
				ISerializer* v = child->ChildNode(i);
				v->Serialize(_T("Y"), m_manualY[i]);
			}
		}

		if (IReadable* child = dynamic_cast<IReadable*>(ar->ChildNode(_T("MeshData")))) {
			size_t const count = child->ChildCount();
			std::vector<double> x(count), y(count), z(count);

			for (size_t i = 0; i < count; ++i) {
				ISerializer* v = child->ChildNode(i);
				v->Serialize(_T("X"), x[i]);
				v->Serialize(_T("Y"), y[i]);
				v->Serialize(_T("Z"), z[i]);
			}

			m_manualZ = z;
			if (!m_manualX.empty() || !m_manualY.empty()) {
				SetGridData(x, y, z, m_manualX.size(), m_manualY.size());
			}
		}
	}
	else {
		long type = GetContourAlgorithm();
		ar->Serialize(_T("Algorithm"), type);

		// デジタルマップでメッシュを編集していたらその値も保存する
		if (GetContourAlgorithm() == Manual) {
			{
				std::vector<double> x, y;
				for (size_t j = 0; j < m_manualY.size(); ++j) {
					for (size_t i = 0; i < m_manualX.size(); ++i) {				
						x.push_back(m_manualX[i]);
						y.push_back(m_manualY[j]);
					}
				}

				ISerializer* child = ar->ChildNode(_T("MeshData"));
				for (size_t i = 0; i < x.size(); ++i) {
					ISerializer* v = child->ChildNode(_T("Value"));
					v->Serialize(_T("X"), x[i]);
					v->Serialize(_T("Y"), y[i]);
					v->Serialize(_T("Z"), m_manualZ[i]);
				}
			}
			{
				ISerializer* child = ar->ChildNode(_T("MeshX"));
				for (size_t i = 0; i < m_manualX.size(); ++i) {
					ISerializer* v = child->ChildNode(_T("Value"));
					v->Serialize(_T("X"), m_manualX[i]);
				}
			}
			{
				ISerializer* child = ar->ChildNode(_T("MeshY"));
				for (size_t i = 0; i < m_manualY.size(); ++i) {
					ISerializer* v = child->ChildNode(_T("Value"));
					v->Serialize(_T("Y"), m_manualY[i]);
				}
			}
		}
	}
}

#endif

void ContourPlot::ClearModel()
{
	m_data.x.clear();
	m_data.y.clear();
	m_data.z.clear();
	m_data.t.clear();
	m_cursor->Reset();
}

void ContourPlot::Modeling(TexVertex* vertexes, size_t length)
{
	m_surface->SetDecor(&m_decor);

	ClearModel();

	m_data.ConvertX = boost::bind(&Axis::ValueToPositionRaw, m_xAxis, _1);
	m_data.ConvertY = boost::bind(&Axis::ValueToPositionRaw, m_yAxis, _1);
	m_data.ConvertZ = boost::bind(&Axis::ValueToPositionRaw, m_zAxis, _1);
	m_data.ConvertT = boost::bind(&Axis::GetTextureValue, m_tAxis, _1);

	// マップ計算は元データの値で行う(計算後Surface内で座標値に戻す)
	m_data.dimension = GetDimension();
	m_data.xMax = m_data.yMax = -DBL_MAX;
	m_data.xMin = m_data.yMin = DBL_MAX;
	double const size = m_zAxis->GetSize();
	for (size_t i = 0; i < length; ++i) {
		double const x = m_xAxis->CoordinateToValueRaw(vertexes[i].X);
		double const y = m_yAxis->CoordinateToValueRaw(-vertexes[i].Y);
		double const z = m_zAxis->CoordinateToValueRaw(vertexes[i].Z);
		double const t = m_tAxis->Expand(vertexes[i].T);

		m_data.x.push_back(x);
		m_data.y.push_back(y);
		m_data.z.push_back(z);
		m_data.t.push_back(t);

		m_data.xMax = std::max<double>(m_data.xMax, x);
		m_data.xMin = std::min<double>(m_data.xMin, x);
		m_data.yMax = std::max<double>(m_data.yMax, y);
		m_data.yMin = std::min<double>(m_data.yMin, y);
	}

	m_data.zMax = m_zAxis->GetSize();
	m_data.zMin = -m_zAxis->GetSize();

	// 格子補間
	if (GetContourAlgorithm() != Manual) {
		m_surface->Calculate(&m_data, GetMeshValue(), GetAlphaValue());
	}
	else {
		m_surface->SetData(&m_data);
	}

	// 等高線作成
	std::vector<LevelLine> lines;
	double const interval = (m_data.zMax - m_data.zMin) / 20;
	std::vector<double> levels = CalcSliceLevel(m_data.zMax, m_data.zMin, interval);
	BOOST_FOREACH(double level, levels) {
		LevelLine line;
		line.level = level;
		lines.push_back(line);
	}
	m_surface->SetSliceLine(lines);

	// 断面図更新
	if (m_cursor->IsEffective()) {
		SelectCursor(m_cursor->GetXIndex(), m_cursor->GetYIndex());
	}
}

int ContourPlot::GetMeshValue() const
{
	return MESH_NUM[m_mesh];
}
double ContourPlot::GetAlphaValue() const
{
	return ALPHA_NUM[m_alpha];
}

void ContourPlot::RenderImpl(ColorTexture* texture, TexVertex const* /*vertexes*/, size_t /*length*/, bool clipping) const
{
	::glDisable(GL_LIGHT0);

	Clipper clipper(m_xAxis->GetSize(), m_yAxis->GetSize(), m_zAxis->GetSize());
	if (clipping) {
		clipper.Clipping();
	}

	texture->Bind();

	m_decor.coloring = IsColoring();
	m_decor.filling = IsFilling();
	m_decor.enableProjection = true;
	m_decor.coloringProjection = GetColoringProjection();
	m_decor.fillingProjection = GetFillingProjection();
	m_decor.xyProjection = GetXYProjection();
	m_decor.yzProjection = GetYZProjection();
	m_decor.zxProjection = GetZXProjection();
	m_decor.renderSliceLine = IsSliceLineVisible();
	m_decor.renderSliceLineProjection = GetSliceLineProjection();
	m_decor.texture = texture;

	m_data.xSideValue = m_xAxis->GetEdge() > 0 ? -m_xAxis->GetSize() : m_xAxis->GetSize();
	m_data.zSideValue = m_yAxis->GetEdge() > 0 ? -m_zAxis->GetSize() : m_zAxis->GetSize();

	::glEnable(GL_POLYGON_OFFSET_FILL);
	::glEnable(GL_POLYGON_OFFSET_LINE);

	if (IsFilling()) {
		m_surface->DrawSurface();
	}

	if (!IsHiddenLine()) {
		::glDisable(GL_DEPTH_TEST);
	}

	if (IsFilling()) {
		texture->Unbind();
	}
	m_surface->DrawSliceLine();
	::glEnable(GL_DEPTH_TEST);

	texture->Unbind();

    // クリッピングを解除してから投影図を描く
    clipper.DsiableClipping();
	if (GetFillingProjection()) {
		::glColor3ub(192, 192, 192);
		if (GetColoringProjection()) {
			texture->Bind();
		}
		m_surface->Projection();
		texture->Unbind();
	}

	if (!GetFillingProjection() && GetColoringProjection()) {
		texture->Bind();
	}
	m_surface->DrawSliceLineProjection();
	texture->Unbind();
    
	::glDisable(GL_POLYGON_OFFSET_LINE);
	::glDisable(GL_POLYGON_OFFSET_FILL);
}

void ContourPlot::RenderCursor() const
{
	if (!m_visibleCursor) { return; }

	// クリッピングは常に有効
	Clipper clipper(m_xAxis->GetSize(), m_yAxis->GetSize(), m_zAxis->GetSize());
	clipper.Clipping();

	if (OCGriddingSurface* g = dynamic_cast<OCGriddingSurface*>(m_surface.get())) {
		::glLineStipple(1, 0xFFFF);
		if (m_cursor->GetXIndex() >= 0) {
			g->DrawXLine(m_cursor->GetXIndex());
		}
		if (m_cursor->GetYIndex() >= 0) {
			g->DrawYLine(m_cursor->GetYIndex());
		}
	}
}

void ContourPlot::RenderCursor(double x, double y) const
{
	if (!m_visibleCursor) { return; }

	// クリッピングは常に有効
	Clipper clipper(m_xAxis->GetSize(), m_yAxis->GetSize(), m_zAxis->GetSize());
	clipper.Clipping();

	// サブカーソルは点線で描く
	::glEnable(GL_LINE_STIPPLE);
	if (OCGriddingSurface* g = dynamic_cast<OCGriddingSurface*>(m_surface.get())) {
		g->DrawXLine(InterpolateXLines(y), true);
		g->DrawYLine(InterpolateYLines(x), true);
	}
}

std::vector<Vertex> ContourPlot::InterpolateXLines(double y) const
{
	std::vector<Vertex> arr;

	if (OCGriddingSurface* gridding = dynamic_cast<OCGriddingSurface*>(m_surface.get())) {
		std::vector<double> xData = m_surface->GetGridX();
		std::vector<double> yData = m_surface->GetGridZ();
		std::vector<double> const& zData = m_surface->GetGridY();
		if (xData.empty()) { return arr; }

		boost::sort(xData);
		boost::sort(yData);
		xData.erase(std::unique(xData.begin(), xData.end()), xData.end());
		yData.erase(std::unique(yData.begin(), yData.end()), yData.end());

		BOOST_FOREACH(double x, xData) {
			std::vector<double> line;
			OCSpline spline;
			for (size_t i = 0; i < xData.size(); ++i) {
				size_t const idx = (xData.size() - i - 1) * xData.size();
				spline.Init((int)xData.size(), (double*)&xData[0], (double*)&zData[idx]);
				spline.Prepare();
				line.push_back(spline.Calc(x));
			}

			spline.Init((int)yData.size(), (double*)&yData[0], (double*)&line[0]);
			spline.Prepare();

			Vertex v(x, spline.Calc(y), y);
			arr.push_back(v);
		}
	}
	return arr;
}

std::vector<Vertex> ContourPlot::InterpolateYLines(double x) const
{
	std::vector<Vertex> arr;

	if (OCGriddingSurface* gridding = dynamic_cast<OCGriddingSurface*>(m_surface.get())) {
		std::vector<double> xData = m_surface->GetGridX();
		std::vector<double> yData = m_surface->GetGridZ();
		std::vector<double> const& zDataSrc = m_surface->GetGridY();
		if (xData.empty()) { return arr; }

		boost::sort(xData);
		boost::sort(yData);
		xData.erase(std::unique(xData.begin(), xData.end()), xData.end());
		yData.erase(std::unique(yData.begin(), yData.end()), yData.end());

		std::vector<double> zData;
		for (size_t i = 0; i < xData.size(); ++i) {
			for (int j = (int)yData.size() - 1; j >= 0; --j) {
				size_t idx = j * xData.size() + i;
				zData.push_back(zDataSrc[idx]);
			}
		}

		BOOST_FOREACH(double y, yData) {
			std::vector<double> line;
			OCSpline spline;
			for (size_t i = 0; i < yData.size(); ++i) {
				spline.Init((int)yData.size(), (double*)&yData[0], (double*)&zData[i * yData.size()]);
				if (spline.Prepare()) {
					line.push_back(spline.Calc(y));
				}
			}

			if (!line.empty()) {
				spline.Init((int)xData.size(), (double*)&xData[0], (double*)&line[0]);
				if (spline.Prepare()) {
					Vertex v(x, spline.Calc(x), y);
					arr.push_back(v);
				}
			}
		}
	}
	return arr;
}

void ContourPlot::RenderWireFrame(ColorTexture* /*texture*/, TexVertex const* /*vertexes*/, size_t /*length*/, bool clipping) const
{
	Clipper clipper(m_xAxis->GetSize(), m_yAxis->GetSize(), m_zAxis->GetSize());
	if (clipping) {
		clipper.Clipping();
	}

	m_surface->DrawWireFrame(m_wireFrameColor);
}