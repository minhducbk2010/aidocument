/*!	@file
*/
#pragma once

#include "SolidUnitBase.hpp"

class BarPlot : public SolidUnitBase
{
public:
	BarPlot();
	~BarPlot();

private:
	virtual void RenderImpl(ColorTexture* texture, TexVertex const* vertexes, size_t length, bool clipping) const;
	virtual void RenderWireFrame(ColorTexture* texture, TexVertex const* vertexes, size_t length, bool clipping) const;

	void RenderBar(GLenum mode, ColorTexture* texture, TexVertex const& v, bool clipping) const;
};
