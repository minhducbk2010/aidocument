/*!	@file
*/
#include "stdafx.h"
#include <boost/foreach.hpp>
#include <boost/range/algorithm/for_each.hpp>
#include "PlotGroup.h"
#include "Axis.h"
#include "ColorTexture.h"
#include "SolidGraph.h"
#include "ScatterPlot.h"
#include "BarPlot.h"
#include "ContourPlot.h"
#include "LinePlot.h"
#include "CurvePlot.h"
#include "Legend.h"
#include "IPlotData.h"
#include "Selector.h"
#include "RenderState.h"
#include "ColorBar.h"
#include "IFilter.h"
#include "IMessageReceiver.h"
#include "Message.h"
#include "DllData.h"

PlotGroup::PlotGroup(IMessageReceiver* owner, SolidGraph* graph, CrossCursor* cursor)
	: m_owner(owner)
	, m_graph(graph)
	, m_xAxis(0), m_yAxis(0), m_zAxis(0)
	, m_marker(new ScatterPlot())
	, m_visible(true)
	, m_color(RGB(0, 0, 0))
	, m_clipping(true)
	, m_selected(false)
	, m_empty(false)
	, m_nameInitialized(false)
{
	TCHAR const* dir = reinterpret_cast<TCHAR const*>(m_owner->SendMessage(PI_MSG_GET_PROJECTDIR, 0, 0));
	std::wstring projectDir = dir ? dir : _T("");
	m_colorPattern.reset(new ColorTexture(projectDir + _T("\\ColorPattern\\"), m_owner));

	m_units.push_back(new ScatterPlot());
	m_units.push_back(new BarPlot());
	m_units.push_back(new ContourPlot(cursor, m_owner));

	m_marker->EnableColoring(false);
}

PlotGroup::~PlotGroup()
{
}

void PlotGroup::SetDimension(int const dimension)
{
	m_colorBar->SetDimension(dimension);
	// 4D立体 or ベクトル図(立体)のときは、T軸が有効になる
	m_colorBar->SetLabelVisible(dimension == 4 || dimension == 6);
	if (dimension == 4 || dimension == 6) {
		m_colorBar->AttachAxis(m_tAxis);
	}

	std::for_each(m_units.begin(), m_units.end(), boost::bind(&SolidUnitBase::SetDimension, _1, dimension));
}

int PlotGroup::GetDimension() const
{
	return m_units[0].GetDimension();
}

void PlotGroup::SimpleVisiblePlot(size_t index, bool visible)
{
	std::for_each(m_units.begin(), m_units.end(), boost::bind(&SolidUnitBase::Visible, _1, false));
	VisiblePlot(index, visible);
}

void PlotGroup::VisiblePlot(size_t index, bool visible)
{
	if (index < m_units.size()) {
		m_units[index].Visible(visible);
	}
}

void PlotGroup::VisibleSubPlot(size_t index)
{
	m_subUnit.reset();
	switch (index)
	{
	case 1:
		m_subUnit.reset(new LinePlot());
		break;
	case 2:
		m_subUnit.reset(new CurvePlot());
		break;
	}

	if (m_subUnit) {
		m_subUnit->Visible(true);
		m_subUnit->AttachAxes(m_xAxis, m_yAxis, m_zAxis, m_tAxis);
	}
}

void PlotGroup::ToggleVisibleSubPlot(size_t index)
{
	switch (index)
	{
	case 1:
		if (dynamic_cast<LinePlot*>(m_subUnit.get())) {
			m_subUnit.reset();
			return;
		}
		break;
	case 2:
		if (dynamic_cast<CurvePlot*>(m_subUnit.get())) {
			m_subUnit.reset();
			return;
		}
		break;
	}
	VisibleSubPlot(index);
}

void PlotGroup::EnableMarker(bool enable)
{
	m_marker->Visible(enable);
}

void PlotGroup::SelectOnlyMarkerVisible()
{
	m_marker->Visible(true);
	m_marker->SelectOnlyMarkerVisible();
}

bool PlotGroup::IsVisibleMarker() const
{
	return m_marker->IsVisible();
}

size_t PlotGroup::GetVisiblePlotIndex() const
{
	for (size_t i = 0; i < m_units.size(); ++i) {
		if (m_units[i].IsVisible()) { return i; }
	}
	return static_cast<size_t>(-1);
}

size_t PlotGroup::GetVisibleSubPlotIndex() const
{
	if (dynamic_cast<LinePlot*>(m_subUnit.get())) {
		return 0;
	}
	if (dynamic_cast<CurvePlot*>(m_subUnit.get())) {
		return 1;
	}
	return static_cast<size_t>(-1);
}

void PlotGroup::EnableLighting(bool enable)
{
	std::for_each(m_units.begin(), m_units.end(), boost::bind(&SolidUnitBase::EnableLighting, _1, enable));
}
bool PlotGroup::IsLighting() const
{
	return m_units[0].IsLighting();
}

void PlotGroup::EnableWireFrame(bool enable)
{
	std::for_each(m_units.begin(), m_units.end(), boost::bind(&SolidUnitBase::EnableWireFrame, _1, enable));
}

bool PlotGroup::IsWireFrameVisible() const
{
	return m_units[0].IsWireFrameVisible();
}

void PlotGroup::EnableColoring(bool enable)
{
	std::for_each(m_units.begin(), m_units.end(), boost::bind(&SolidUnitBase::EnableColoring, _1, enable));
}

bool PlotGroup::IsColoring() const
{
	return m_units[0].IsColoring();
}

void PlotGroup::EnableFilling(bool enable)
{
	std::for_each(m_units.begin(), m_units.end(), boost::bind(&SolidUnitBase::EnableFilling, _1, enable));
}

bool PlotGroup::IsFilling() const
{
	return m_units[0].IsFilling();
}

void PlotGroup::SetVisibleSliceLine(bool visible)
{
	std::for_each(m_units.begin(), m_units.end(), boost::bind(&SolidUnitBase::VisibleSliceLine, _1, visible));
}

bool PlotGroup::GetVisibleSliceLine() const
{
	return m_units[0].IsSliceLineVisible();
}

void PlotGroup::EnableHiddenLine(bool hidden)
{
	std::for_each(m_units.begin(), m_units.end(), boost::bind(&SolidUnitBase::EnableHiddenLine, _1, hidden));
}
bool PlotGroup::IsHiddenLine() const
{
	return m_units[0].IsHiddenLine();
}

double PlotGroup::GetMarkerSize() const
{
	return m_marker->GetMarkerSize();
}
void PlotGroup::SetMarkerSize(double size)
{
	m_marker->SetMarkerSize(size);
	if (ScatterPlot* s = dynamic_cast<ScatterPlot*>(&m_units[0])) {
		s->SetMarkerSize(size);
	}
}

void PlotGroup::SetXYProjection(bool state)
{
	std::for_each(m_units.begin(), m_units.end(), boost::bind(&SolidUnitBase::SetXYProjection, _1, state));
}
bool PlotGroup::GetXYProjection() const
{
	return m_units[2].GetXYProjection();
}

void PlotGroup::SetYZProjection(bool state)
{
	std::for_each(m_units.begin(), m_units.end(), boost::bind(&SolidUnitBase::SetYZProjection, _1, state));
}
bool PlotGroup::GetYZProjection() const
{
	return m_units[2].GetYZProjection();
}

void PlotGroup::SetZXProjection(bool state)
{
	std::for_each(m_units.begin(), m_units.end(), boost::bind(&SolidUnitBase::SetZXProjection, _1, state));
}
bool PlotGroup::GetZXProjection() const
{
	return m_units[2].GetZXProjection();
}

void PlotGroup::SetColoringProjection(bool state)
{
	std::for_each(m_units.begin(), m_units.end(), boost::bind(&SolidUnitBase::SetColoringProjection, _1, state));
}
bool PlotGroup::GetColoringProjection() const
{
	return m_units[2].GetColoringProjection();
}

void PlotGroup::SetFillingProjection(bool state)
{
	std::for_each(m_units.begin(), m_units.end(), boost::bind(&SolidUnitBase::SetFillingProjection, _1, state));
}
bool PlotGroup::GetFillingProjection() const
{
	return m_units[2].GetFillingProjection();
}

void PlotGroup::SetSliceLineProjection(bool state)
{
	std::for_each(m_units.begin(), m_units.end(), boost::bind(&SolidUnitBase::SetSliceLineProjection, _1, state));
}
bool PlotGroup::GetSliceLineProjection() const
{
	return m_units[2].GetSliceLineProjection();
}

void PlotGroup::SetContourAlgorithm(long type)
{
	std::for_each(m_units.begin(), m_units.end(), boost::bind(&SolidUnitBase::SetContourAlgorithm, _1, type));
}
long PlotGroup::GetContourAlgorithm() const
{
	return m_units[2].GetContourAlgorithm();
}

void PlotGroup::VisibleCursor(bool visible)
{
	std::for_each(m_units.begin(), m_units.end(), boost::bind(&SolidUnitBase::VisibleCursor, _1, visible));
}

bool PlotGroup::UseDigitalmap() const
{
	if (m_data->GetDataCount() == 0) { return false; }
	// 3Dコンターのみ
	if (GetDimension() != 3) { return false; }

	if (ContourPlot const* c = dynamic_cast<ContourPlot const*>(&m_units[2])) {
		return c->IsVisible();
	}
	return false;
}

void PlotGroup::ShowDigitalmap()
{
	if (UseDigitalmap()) {
		if (ContourPlot* c = dynamic_cast<ContourPlot*>(&m_units[2])) {
			c->ShowDigitalmap(GetData());
		}
	}
}

void PlotGroup::SelectCursor(int xIndex, int yIndex)
{
	if (ContourPlot* c = dynamic_cast<ContourPlot*>(&m_units[2])) {
		c->SelectCursor(xIndex, yIndex);
	}
}

void PlotGroup::GetCursorIndex(int* xIndex, int* yIndex) const
{
	if (ContourPlot const* c = dynamic_cast<ContourPlot const*>(&m_units[2])) {
		c->GetCursor(xIndex, yIndex);
	}
}

int PlotGroup::HitTest(RenderState* state, int x, int y)
{
	if (m_vertexes.empty()) { return -1; }

	ColorTexture texture(L"", m_owner);

	Selector selector(x, y);
	state->Bind();

	bool const visible = m_marker->IsVisible();
	m_marker->PreparePickup();
	m_marker->Visible(true);
	m_marker->Render(&texture, &m_vertexes[0], m_vertexes.size(), false);
	m_marker->Visible(visible);
	m_marker->UnpreparePickup();

	state->Unbind();

	int const hit = selector.GetPoint();
	return hit;
}

int PlotGroup::PickUpData(RenderState* state, int x, int y)
{
	int const hit = HitTest(state, x, y);
	SelectMarker(hit);
	return hit;
}

void PlotGroup::SelectMarker(int index)
{
	if (ScatterPlot* s = dynamic_cast<ScatterPlot*>(&m_units[0])) {
		s->SelectMarker(index);
	}
	m_marker->SelectMarker(index);
}

size_t PlotGroup::GetFileId() const
{
	if (!m_data) { return size_t(-1); }
	return m_data->GetFileId(0);
}

void PlotGroup::SetFileId(size_t id)
{
	m_data->ChangeFile(id);
	m_data->LinkDataSet();
}

void PlotGroup::SetData(IPlotData* data)
{
	if (m_data.get() != data) {
		m_data.reset(data, boost::bind(&IPlotData::Destroy, _1));
	}

	if (m_data->GetDataCount() > 0) {
		if (!m_nameInitialized) {
			m_name = m_data->GetItemName(2);
			std::wstring const unit = m_data->GetUnitName(2);
			if (!unit.empty()) {
				m_name += (boost::wformat(_T(" [%1%]")) % unit).str();
			}
			m_nameInitialized = true;
		}

		m_empty = false;
	}
}

void PlotGroup::ChangeZValue(int index, double z)
{
	if (m_vertexes.size() > static_cast<size_t>(index)) {
		m_vertexes[index].Z = z == FLT_MAX ? FLT_MAX : m_zAxis->ValueToPosition(z);
		m_vertexes[index].T = m_tAxis->GetNormalize(z);
	}

	if (!m_vertexes.empty()) {
		std::for_each(m_units.begin(), m_units.end(), boost::bind(&ISolidUnit::Modeling, _1, &m_vertexes[0], m_vertexes.size()));
	}
	else {
		std::for_each(m_units.begin(), m_units.end(), boost::bind(&ISolidUnit::ClearModel, _1));
	}

	// GraphLib向けに作成されたインスタンスの場合のみ以下の処理を行う
	if (DllData* data = dynamic_cast<DllData*>(m_data.get())) {
		if (ContourPlot* contour = dynamic_cast<ContourPlot*>(GetPlot(2))) {
			contour->SetValue(index, z);
		}
	}
}

void PlotGroup::Modeling()
{
	if (IsEmpty()) { return; }

	m_vertexes.clear();
	GetVertexes(m_vertexes);
	if (!m_vertexes.empty()) {
		std::for_each(m_units.begin(), m_units.end(), boost::bind(&ISolidUnit::Modeling, _1, &m_vertexes[0], m_vertexes.size()));
	}
	else {
		std::for_each(m_units.begin(), m_units.end(), boost::bind(&ISolidUnit::ClearModel, _1));
	}

	// GraphLib向けに作成されたインスタンスの場合のみ以下の処理を行う
	if (DllData* data = dynamic_cast<DllData*>(m_data.get())) {
		if (ContourPlot* contour = dynamic_cast<ContourPlot*>(GetPlot(2))) {
			size_t UniqueCount(real_t const* data, long dataCount);
			long const dataCount = data->GetDataCount();
			std::vector<double> x(data->GetXData(), data->GetXData() + dataCount);
			std::vector<double> y(data->GetYData(), data->GetYData() + dataCount);
			std::vector<double> z(data->GetZData(), data->GetZData() + dataCount);
			size_t const xCount = UniqueCount(data->GetXData(), dataCount);
			size_t const yCount = UniqueCount(data->GetYData(), dataCount);
			if (xCount * yCount == z.size()) {
				contour->SetGridData(x, y, z, xCount, yCount);
			}
			else
			{
				// グリッドが不正の場合はグリッドを空にしておく
				contour->ClearGridData();
			}
		}
	}
}

void PlotGroup::Render(bool /*clipping*/) const
{
	if (!m_visible) { return; }
	if (!m_data) { return; }
	if (m_vertexes.empty()) { return; }
	if (IsEmpty()) { return; }

	InitColorBar();

	if (m_subUnit) {
		m_subUnit->Render(m_colorPattern.get(), &m_vertexes[0], m_vertexes.size(), m_clipping);
	}
	if (m_marker && !m_units[0].IsVisible()) {
		// メインプロットの塗りつぶしがOFFの場合、マーカーに色をつける
		if (!m_units[0].IsFilling()) {
			m_marker->EnableColoring(true);
		}
		m_marker->Render(m_colorPattern.get(), &m_vertexes[0], m_vertexes.size(), m_clipping);
		m_marker->EnableColoring(false); // restore
	}

	std::for_each(m_units.begin(), m_units.end(), boost::bind(&ISolidUnit::Render, _1, m_colorPattern.get(), &m_vertexes[0], m_vertexes.size(), m_clipping));
}

void PlotGroup::RenderColorBar(bool subPlot, bool labelVisible) const
{
	InitColorBar();
	m_colorBar->SetLabelVisible(labelVisible);
	std::wstring const& label = GetDimension() == 3 ? m_name : m_tAxis->GetLabel();
	m_colorBar->Render(m_colorPattern.get(), label, subPlot);
}

void PlotGroup::InitColorBar() const
{
	m_colorPattern->SetMinMax(0, 1);
	if (IsColoring()) {
		m_colorPattern->SelectPattern(m_colorPattern->GetCurrentPattern());
	}
	else {
		m_colorPattern->SetMonocrhomePattern();
	}
}

void PlotGroup::GetVertexes(std::vector<TexVertex>& vertexes) const
{
	if (!m_data) { return; }

	for (size_t i = 0, n = m_data->GetDataCount(); i < n; ++i) {
		double const x = m_data->GetData(0, i);
		double const y = m_data->GetData(1, i);
		double const z = m_data->GetData(2, i);
		double const t = m_data->GetData(3, i);
		if (x == FLT_MAX || y == FLT_MAX || z == FLT_MAX || t == FLT_MAX) {
#ifdef GLGRAPHLIB
			TexVertex v(
				0,
				m_tAxis->GetNormalize(t),
				m_xAxis->ValueToPosition(x),
				m_yAxis->ValueToPosition(y),
				FLT_MAX);
			vertexes.push_back(v);
#endif
			continue;
		}

		TexVertex v(
			0,
			m_tAxis->GetNormalize(t),
			m_xAxis->ValueToPosition(x),
			m_yAxis->ValueToPosition(y),
			m_zAxis->ValueToPosition(z));
		vertexes.push_back(v);
	}
}

void PlotGroup::OnPropertyUpdated(bool all_update)
{
	PropertyUpdated(all_update);
}

void PlotGroup::AttachAxes(Axis* xAxis, Axis* yAxis, Axis* zAxis, Axis* tAxis)
{
	m_xAxis = xAxis;
	m_yAxis = yAxis;
	m_zAxis = zAxis;
	m_tAxis = tAxis;

	std::for_each(m_units.begin(), m_units.end(), boost::bind(&SolidUnitBase::AttachAxes, _1, xAxis, yAxis, zAxis, tAxis));
	m_marker->AttachAxes(xAxis, yAxis, zAxis, tAxis);

	m_colorBar.reset(new ColorBar(m_zAxis));
}

void PlotGroup::Select(bool select)
{
	if (m_selected != select) {
		m_selected = select;
		OnPropertyUpdated();
		m_owner->SendMessage(PI_MSG_INVALIDATE, 0, 0);
	}
}

bool PlotGroup::IsEmpty() const
{
	return m_empty;
}

void PlotGroup::CopyFrom(PlotGroup* src)
{
	SimpleVisiblePlot(src->GetVisiblePlotIndex(), true);
	VisibleSubPlot(src->GetVisibleSubPlotIndex());
	EnableHiddenLine(src->IsHiddenLine());
	EnableLighting(src->IsLighting());
	EnableWireFrame(src->IsWireFrameVisible());
	EnableColoring(src->IsColoring());
	EnableFilling(src->IsFilling());
	SetVisibleSliceLine(src->GetVisibleSliceLine());
	SetMarkerSize(src->GetMarkerSize());
	SetXYProjection(src->GetXYProjection());
	SetYZProjection(src->GetYZProjection());
	SetZXProjection(src->GetZXProjection());
	SetColoringProjection(src->GetColoringProjection());
	SetFillingProjection(src->GetFillingProjection());
	SetSliceLineProjection(src->GetSliceLineProjection());
	SetContourAlgorithm(src->GetContourAlgorithm());
}

bool PlotGroup::CheckUseColorPattern(TCHAR const* path)
{
	std::wstring const a = ::PathFindFileName(ToWideString(m_colorPattern->GetCurrentPatternFileName()).c_str());
	std::wstring const b = ::PathFindFileName(path);
	return a == b;
}

bool PlotGroup::UpdateColorPattern(TCHAR const* path)
{
	if (CheckUseColorPattern(path)) {
		m_colorPattern->ReloadPatterns();
		return true;
	}
	return false;
}

#if 1

void PlotGroup::GetName(wchar_t* name, size_t length) const
{
	::_tcsncpy_s(name, length - 1, m_name.c_str(), _TRUNCATE);
}

unsigned long PlotGroup::GetTypeId() const
{
	unsigned long const typeId[] = { 20, 23, 24 };
	for (size_t i = 0; i < m_units.size(); ++i) {
		if (m_units[i].IsVisible()) {
			return typeId[i];
		}
	}

	return 24; // 平面コンター
}

int PlotGroup::HitTest(double x, double y) const
{
	return m_legend->Contains(this, x, y) ? HIT_MOVE : HIT_NONE;
}

#include "property.h"
#include "PropertyCreator.hpp"
#include "ISerializer.h"
#include "IMessageReceiver.h"

void AddDataProperty(IPropertyComposite* parent, IMessageReceiver* owner, IPlotData* data, int dim, bool const locked, bool use_file); //TODO: 場所変える(3.3)
void AddColorPattenProperty(IMessageReceiver* owner, IPropertyComposite* parent, bool const locked, ColorTexture* texture); //TODO: 移動(3.3)

PropertyNode* PlotGroup::CreateProperty()
{
	OCProperty prop;

	AddShowProperty(&prop, IsLock());
	AddColorPattenProperty(m_owner, &prop, IsLock(), m_colorPattern.get());
	AddDataProperty(&prop, m_owner, m_data.get(), GetDimension(), IsLock(), false);
	AddPlotProperty(&prop, IsLock());
	AddProperty(&prop, IsLock());

	return prop.GetRawPtr();
}

PropertyNode* PlotGroup::CreateTabProperty()
{
	OCProperty prop;

	AddShowProperty(prop.CreateGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_SHOW")), IsLock());
	AddColorPattenProperty(m_owner, prop.CreateGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_SHOW")), IsLock(), m_colorPattern.get());
	AddDataProperty(prop.CreateGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_PLOT")), m_owner, m_data.get(), GetDimension(), IsLock(), false);
	AddPlotProperty(prop.CreateGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_PLOT")), IsLock());
	AddProperty(prop.CreateGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_MAP_OPERATION")), IsLock());

	return prop.GetRawPtr();
}

void PlotGroup::AddProperty(IPropertyComposite* prop, bool locked) const
{
	std::for_each(m_units.begin(), m_units.end(), boost::bind(&SolidUnitBase::AddProperty, _1, prop, locked));
}

void PlotGroup::AddShowProperty(IPropertyComposite* prop, bool locked) const
{
	IPropertyGroup* grp = prop->AddGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_SHOW"), PI_VISIBLE, m_visible, locked);
	grp->AddProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_NAME_NAMAE"), PI_NAME, m_name.c_str(), locked);
	grp->AddBoolProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_LOCK"), PI_POS_LOCK, m_posLock, false);
	grp->AddColorProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_COLOR_KANJI"), PI_LINE_COLOR, m_color, locked || m_graph->GetGraphType() != GraphType::Item);
}

void PlotGroup::AddPlotProperty(IPropertyComposite* prop, bool locked) const
{
	IPropertyGroup* grp = prop->AddGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_PLOT"));

	size_t const plotIndex = GetVisiblePlotIndex();
	bool const useMarker = !IsVisibleMarker() && plotIndex != 0;

	std::wstring classification1 = ::GetResourceString(m_owner,"ID_SOURCE_TEXT_SCATTER_DIAGRAM");
	std::wstring classification2 = ::GetResourceString(m_owner,"ID_SOURCE_TEXT_BAR_GRAPH");
	std::wstring classification3 = ::GetResourceString(m_owner,"ID_SOURCE_TEXT_CONTOUR_MAP");
	grp->AddArrayProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_CLASSIFICATION"), PI_3D_PLOT_KIND, (int)plotIndex, locked, 3, 
		classification1.c_str(), classification2.c_str(), classification3.c_str());

	grp->AddSelectorPropertyR(::GetResourceString(m_owner,"ID_SOURCE_TEXT_MARKER_SIZE"), PI_SCATTER_SIZE, locked || useMarker,
		GetMarkerSize(), 9, 0.1, 0.5, 1.0, 1.5, 2.0, 3.0, 4.0, 5.0, 10.0);
	grp->AddBoolProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_CLIPPING"), PI_DATA_CLIPPING, m_clipping, locked);
}

bool PlotGroup::SetProperty(long id, LPARAM value, LPARAM* source)
{
	switch (id)
	{
	case PI_VISIBLE:
		::SetPropertyHelper(m_visible, value, source);
		OnPropertyUpdated();
		break;
	case PI_NAME:
		::SetPropertyHelper(m_name, value, source);
		OnPropertyUpdated();
		break;
	case PI_LOCK:
		::SetPropertyHelper(m_lock, value, source);
		break;
	case PI_POS_LOCK:
		::SetPropertyHelper(m_posLock, value, source);
		break;
	case PI_LINE_COLOR:
		::SetPropertyHelper(m_color, value, source);
		if (m_graph->GetGraphType() == GraphType::Item) {
			boost::for_each(m_units, boost::bind(&SolidUnitBase::SetWireFrameColor, _1, m_color));
		}
		OnPropertyUpdated();
		break;
	case PI_3D_WIREFRAME_COLOR:
		boost::for_each(m_units, boost::bind(&SolidUnitBase::SetWireFrameColor, _1, value));
		OnPropertyUpdated();
		return true;
	case PI_COLOR_PATTERN:
        {
			std::wstring pattern = ToWideString(m_colorPattern->GetCurrentPatternFileName());
			::SetPropertyHelper(pattern, value, source);
			m_colorPattern->SetPatternFileName(pattern.c_str());
			if (source) {
				ColorPatternChanged(pattern);
			}
			OnPropertyUpdated();
        }
        return true;
	case PI_SCATTER_SIZE:
		{
			double cur = GetMarkerSize();
			::SetPropertyHelper(cur, value, source);
			SetMarkerSize(cur);
			OnPropertyUpdated();
		}
		return true;
	case PI_3D_PLOT_KIND:
		SimpleVisiblePlot(static_cast<UINT>(value), true);
		OnPropertyUpdated();
		return true;
	case PI_XDATA_INDEX:
		SetDataProperty(m_data->GetItemId(0), 0, value, source);
		OnPropertyUpdated(true);
		return true;
	case PI_YDATA_INDEX:
		SetDataProperty(m_data->GetItemId(1), 1, value, source);
		OnPropertyUpdated(true);
		return true;
	case PI_ZDATA_INDEX:
		SetDataProperty(m_data->GetItemId(2), 2, value, source);
		OnPropertyUpdated(true);
		return true;
	case PI_TDATA_INDEX:
		SetDataProperty(m_data->GetItemId(3), 3, value, source);
		OnPropertyUpdated(true);
		return true;
	case PI_DATA_CLIPPING:
		::SetPropertyHelper(m_clipping, value, source);
		OnPropertyUpdated(true);
		return true;
	}

	BOOST_FOREACH(SolidUnitBase& unit, m_units) {
		if (unit.SetProperty(id, value, source)) {
			Modeling();
			OnPropertyUpdated();
			return true;
		}
	}

	return false;
}

bool PlotGroup::SetPropertyForAllUnit(long id, LPARAM value, LPARAM* source)
{
	bool success = false;
	BOOST_FOREACH(SolidUnitBase& unit, m_units) {
		success |= unit.SetProperty(id, value, source);
	}

	if (success) {
		Modeling();
		OnPropertyUpdated();
	}

	return success;
}

bool PlotGroup::GetProperty(long id, LPARAM* ret) const
{
	switch (id)
	{
	case PI_COLOR_PATTERN:
		{
			static std::wstring str;

			str = ToWideString(m_colorPattern->GetCurrentPatternFileName());
			*reinterpret_cast<wchar_t const**>(ret) = str.c_str();
		}
		return true;
	case PI_DATA_CLIPPING:
		*ret = m_clipping ? TRUE : FALSE;
		return true;
	}

	BOOST_FOREACH(SolidUnitBase const& unit, m_units) {
		if (unit.GetProperty(id, ret)) {
			return true;
		}
	}

	return false;
}

void PlotGroup::SetDataProperty(int current, int dim, LPARAM value, LPARAM* source)
{
	::SetPropertyHelper(current, value, source);
	m_data->SetLink(dim, m_data->GetFileId(0), current);
	m_data->LinkDataSet();

	Axis* axes[] = { m_xAxis, m_yAxis, m_zAxis, m_tAxis };
	DataChanged(axes[dim]);

	Modeling();
}

void SerializeColorPattern(ISerializer* ar, ColorTexture* colorPattern, IMessageReceiver* owner); //TODO: 移動

void PlotGroup::Serialize(ISerializer* ar)
{
	IPlotData* data = m_data.get();
	ar->Serialize(_T("Data"), data);
	SetData(data);

	ISerializer* filterNode = ar->ChildNode(_T("Filter"));
	if (IReadable* reader = dynamic_cast<IReadable*>(filterNode)) {
		IFile* file = reinterpret_cast<IFile*>(m_owner->SendMessage(PI_MSG_GET_FILE_FROMID, GetFileId(), 0));
		IFilter* ptr = reinterpret_cast<IFilter*>(m_owner->SendMessage(PI_GET_MIXED_FILTER, reinterpret_cast<WPARAM>(file), 0));
		boost::shared_ptr<IFilter> filter(ptr);
		filter->Serialize(filterNode);
		m_data->SetFilter(filter);
	}
	else if (IFilter* filter = data->GetFilter()) {
		filter->Serialize(filterNode);
	}

	SerializeString(ar, _T("Name"), m_name);
	ar->Serialize(_T("PlotColor"), m_color);
	ar->Serialize(_T("Clipping"), m_clipping);
	ar->Serialize(_T("Lock"), m_posLock);

	boost::for_each(m_units, boost::bind(&SolidUnitBase::Serialize, _1, ar));

	size_t kind = GetVisiblePlotIndex();
	ar->Serialize(_T("Kind"), kind);
	if (IsReadable(ar)) {
		VisiblePlot(kind, true);
	}

	int lineType = m_subUnit.get() ? dynamic_cast<LinePlot*>(m_subUnit.get()) ? 1 : 2 : 0;
	ar->Serialize(_T("LineType"), lineType);

	if (IsReadable(ar)) {
		VisibleSubPlot(lineType);

#define READ_SET(type, name, setter) { type b; ar->Serialize(name, b); setter(b); }
#define READ_SETD(type, name, setter, def) { type b; ar->Serialize(name, b, def); setter(b); }
		READ_SET(double, _T("MarkerSize"), SetMarkerSize);
		READ_SET(bool, _T("MarkerUsed"), EnableMarker);
		READ_SETD(bool, _T("Lighting"), EnableLighting, true);
		READ_SETD(bool, _T("WireFrame"), EnableWireFrame, false);
		READ_SETD(bool, _T("Coloring"), EnableColoring, true);
		READ_SETD(bool, _T("Filling"), EnableFilling, true);
		READ_SETD(bool, _T("VisibleSliceLine"), SetVisibleSliceLine, false);
		READ_SETD(bool, _T("HiddenLine"), EnableHiddenLine, true);
	}
	else {
#define WRITE_SET(type, name, getter) { type b = getter(); ar->Serialize(name, b); }
		WRITE_SET(double, _T("MarkerSize"), GetMarkerSize);
		WRITE_SET(bool, _T("MarkerUsed"), IsVisibleMarker);
		WRITE_SET(bool, _T("Lighting"), IsLighting);
		WRITE_SET(bool, _T("WireFrame"), IsWireFrameVisible);
		WRITE_SET(bool, _T("Coloring"), IsColoring);
		WRITE_SET(bool, _T("Filling"), IsFilling);
		WRITE_SET(bool, _T("VisibleSliceLine"), GetVisibleSliceLine);
		WRITE_SET(bool, _T("HiddenLine"), IsHiddenLine);
	}

	SerializeColorPattern(ar->ChildNode(_T("ColorPattern")), m_colorPattern.get(), m_owner);

	IFile* file = reinterpret_cast<IFile*>(m_owner->SendMessage(PI_MSG_GET_FILE_FROMID, m_data->GetFileId(0), 0));
	m_empty = file == nullptr;
	// テンプレートの場合は後で名前を付けるので、名前を付けない
	// ファイル追加時のSetDataで付けられるため
	if (m_empty) 
	{ 
		m_name = _T("");
		m_nameInitialized = false;
	}
	else
	{
		m_nameInitialized = true;
	}
}

#endif