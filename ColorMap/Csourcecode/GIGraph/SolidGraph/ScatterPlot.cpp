/*!	@file
*/
#include "stdafx.h"
#include "ScatterPlot.h"
#include "Graph3DBase.h"
#include "TexVertex.h"
#include "IPlotData.h"
#include "ColorTexture.h"
#include "Axis.h"

//! GraphLibから呼ばれたときのマーカーの色
static const COLORREF MARKER_COLOR = RGB(0, 0, 0);

ScatterPlot::ScatterPlot()
	: m_markerSize(2), m_selectedIndex(-1)
	, m_visibleSelectOnly(false), m_pickup(false)
{
}

ScatterPlot::~ScatterPlot()
{
}

void ScatterPlot::RenderImpl(ColorTexture* texture, TexVertex const* vertexes, size_t length, bool clipping) const
{
	::glColor3ub(192, 192, 192);
	if (IsColoring() && !IsLighting()) {
		texture->Bind();
	}

	if (IsFilling()) {
		RenderQuadaric(vertexes, length, texture, false, clipping);
	}

	texture->Unbind();
}

void ScatterPlot::RenderWireFrame(ColorTexture* texture, TexVertex const* vertexes, size_t length, bool clipping) const
{
	RenderQuadaric(vertexes, length, texture, true, clipping);
}

void ScatterPlot::RenderQuadaric(TexVertex const* vertexes, size_t length, ColorTexture* texture, bool wireframe, bool clipping) const
{
	boost::shared_ptr<GLUquadric> quad(::gluNewQuadric(), ::gluDeleteQuadric);

	GLdouble const size = m_markerSize * (wireframe ? 0.23 : 0.2);
	GLint const stacks = wireframe ? 5 : 20;
	if (wireframe) {
		::gluQuadricDrawStyle(quad.get(), GLU_LINE);
	}

	for (UINT i = 0; i < length; ++i) {
		::glLoadName(i);

		if (!m_pickup && m_visibleSelectOnly) {
			if ((UINT)m_selectedIndex != i) {
				continue;
			}
		}

		TexVertex const& v = vertexes[i];
		if (v.Z == FLT_MAX) { continue; }

		::glPushMatrix(); {
			if (!clipping ||
				(std::abs(v.X) <= m_xAxis->GetSize() &&
				std::abs(v.Y) <= m_yAxis->GetSize() &&
				std::abs(v.Z) <= m_zAxis->GetSize()))
			{
				double const t = (UINT)m_selectedIndex == i ? 1 : v.T;
				::glTexCoord1d(t);
				if (IsLighting()) {
					COLORREF const c = texture->GetColor(t);
					SetMaterial(c);
				}

				bool const enableTexture = texture->IsEnabled();
				if ((UINT)m_selectedIndex == i) {
					texture->Unbind();
					::glColor3ub(255, 0, 0);
				}
				else {
					COLORREF const color = g_isLibMode ? MARKER_COLOR : m_wireFrameColor;
					::glColor3ub(GetRValue(color), GetGValue(color), GetBValue(color));
				}

				::glTranslated(v.X, v.Z, v.Y);
				::glNormal3f(-1, -1, -1);
				::gluSphere(quad.get(), size, stacks, stacks);

				if (enableTexture && (UINT)m_selectedIndex == i) {
					texture->Bind();
				}
			}
		} ::glPopMatrix();
	}
}
