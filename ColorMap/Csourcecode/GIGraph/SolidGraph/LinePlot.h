#pragma once

#include "SolidUnitBase.hpp"

class LinePlot : public SolidUnitBase
{
public:
	LinePlot();
	virtual ~LinePlot();

private:
	virtual void RenderImpl(ColorTexture* texture, TexVertex const* vertexes, size_t length, bool clipping) const;
};
