/*!	@file
*/
#include "stdafx.h"
#include <complex>
#include <glut.h>
#pragma comment(lib, "glut32.lib")
#include "VectorPlot.h"
#include "ColorTexture.h"
#include "Common.h"
#include "Axis.h"
#include "Message.h"

/*!	ラジアンを角度(degree)に変換します
	@param	radian	ラジアン角
*/
double RadToDeg(double radian)
{
	return radian * (180.0f / M_PI);
}

//! 2点間の角度を求めます
double CalcAngle(OCPoint const& p1, OCPoint const& p2)
{
	std::complex<double> a(p1.x, p1.y), b(p2.x, p2.y);
	double rad = std::arg(b - a);
	return RadToDeg(rad);
}

VectorPlot::VectorPlot()
	: m_norm(1), m_arrowSize(3), m_centering(false)
	, m_threshold(0.2), m_owner()
{
}

VectorPlot::~VectorPlot()
{
}

void VectorPlot::Render(ColorTexture* texture, Vector* vectors, size_t const length, bool clipping) const
{
	// vectorsに入っている成分はx,y,zはOpenGL座標系での座標、vx,vy,vzはそのままのベクトル成分が入っている
	// ただしO-Chart座標系のx,y,z軸はOpenGL座標系ではそれぞれx,z,y軸に対応しており、
	// vectorsのy,zの組、vy,vzの組は実際のO-Chartでの値と入れ替わって入っている。
	// また、O-Chart座標系のy軸の方向とOpenGL座標系のz軸の方向は逆なので、vzはマイナスを付けて計算すること。
	// (zはすでにOpenGL座標系に変換されているので考慮する必要はない)

	// ベクトルの最大/最小を取得
	double minDistance = DBL_MAX;
	double maxDistance = -DBL_MAX;
	for (size_t i = 0; i < length; ++i) {
		Vector& v = vectors[i];

		double const distance = std::sqrt((v.vx * v.vx) + (v.vy * v.vy) + (v.vz * v.vz));
		maxDistance = std::max<double>(distance, maxDistance);
		minDistance = std::min<double>(distance, minDistance);
	}

	::glLineWidth(4);

	double const arrowSize = m_arrowSize / m_norm;

	// テクスチャーの最大最小値はカラーバーの最大最小値に合わせる
	// minDistance, maxDistanceに合わせると、カラーバーの最大最小値が変えられるようになったときおかしくなる
	// (Ver.3.7.3.0時点ではできないけど・・・)
	texture->SetMinMax(m_tAxis->GetMinimum(), m_tAxis->GetMaximum());
	// texture->SetMinMax(minDistance, maxDistance);

#if _DEBUG
	// 基準の矢じりを書いてみる
	//::glPushMatrix();
	//		COLORREF const color = RGB(0, 0, 0);
	//		::glColor3ub(GetRValue(color), GetGValue(color), GetBValue(color));

	//		::glTranslated(0, 0, 0);
	//		::glScalef(0.5, 0.5, 0.5);
	//		::glutSolidCone(1, 2, 5, 5);
	//::glPopMatrix();
#endif

	for (size_t i = 0; i < length; ++i) {
		Vector const& v = vectors[i];
		// ベクトルの長さを求めて、しきい値判定と色の取得
		double const distance = std::sqrt((v.vx * v.vx) + (v.vy * v.vy) + (v.vz * v.vz));
		// しきい値未満の値は描画しない(0-1に正規化して判定)
		double const dm = (distance - minDistance) / (maxDistance - minDistance);
		if (dm < m_threshold) { continue; }

		COLORREF const c = texture->GetColor(distance);
		::glColor3ub(GetRValue(c), GetGValue(c), GetBValue(c));

		// 実際に描画する座標の計算(OpenGL座標系における座標、ベクトルの成分、大きさ)
		double x, y, z, vx, vy, vz, dx, dy, dz;
		x = v.x;
		y = v.y;
		z = v.z;
		vx = v.vx / (m_xAxis->GetMaximum() - m_xAxis->GetMinimum());
		vy = v.vy / (m_zAxis->GetMaximum() - m_zAxis->GetMinimum());
		vz = v.vz / (m_yAxis->GetMaximum() - m_yAxis->GetMinimum());
		double const glDistanse = std::sqrt((vx * vx) + (vy * vy) + (vz * vz));
		dx = v.x + vx * arrowSize;
		dy = v.y + vy * arrowSize;
		dz = v.z - (vz * arrowSize);
		if (m_centering) {
			double ox = (dx - x) / 2;
			double oy = (dy - y) / 2;
			double oz = (dz - z) / 2;
			x -= ox; dx -= ox;
			y -= oy; dy -= oy;
			z -= oz; dz -= oz;
		}

		// クリッピング
		if (clipping &&
			(std::abs(x) > m_xAxis->GetSize() ||
			std::abs(y) > m_zAxis->GetSize() ||
			std::abs(z) > m_yAxis->GetSize()))
		{
			continue;
		}

		// 線を描く
		::glBegin(GL_LINE_STRIP);
			::glVertex3d(x, y, z);
			::glVertex3d(dx, dy, dz);
		::glEnd();

		// やじりを描く
        ::glPushMatrix();
			::glTranslated(dx, dy, dz);
			::glScalef(0.5, 0.5, 0.5);

			// ベクトルの方向とZ軸の方向が同じになるように回転させる
			double ang = acos(-vz / glDistanse) / M_PI * 180.0;
			// 回転軸はxy平面上にあるベクトル(vx,vy)の法線ベクトル
			::glRotated( ang, -vy, vx, 0.0);

			//double const xAngle = CalcAngle(OCPoint(0, 0), OCPoint(-v.vz, v.vy));
			//double const yAngle = CalcAngle(OCPoint(0, 0), OCPoint(-v.vz, v.vx));

			//::glRotatef(xAngle, 1, 0, 0);
			//::glRotatef(yAngle, 0, 1, 0);

            ::glutSolidCone(1, 2, 5, 5);
        ::glPopMatrix();
	}

	::glLineWidth(1);
}

#if 1

#include "PropertyCreator.hpp"
#include "ISerializer.h"

void VectorPlot::AddProperty(IPropertyComposite* prop, bool locked) const
{
	{
		IPropertyGroup* grp = prop->AddGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_ARROW"));
		grp->AddProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_STANDARD_VECTOR"), PI_VECTOR_STD_VECTOR, m_norm, locked);
		grp->AddProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_LENGTH_ARROW"), PI_VECTOR_ARROW_LENGTH, m_arrowSize, locked); //!@todo 単位
		grp->AddBoolProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_CENTERING"), PI_VECTOR_CENTERING, m_centering, locked);
	}
	{
		IPropertyGroup* grp = prop->AddGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_THRESHOLD"));
		grp->AddProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_THRESHOLD"), PI_VECTOR_THRESHOLD, m_threshold, locked);
	}
}

bool VectorPlot::SetProperty(long id, LPARAM value, LPARAM* source)
{
	switch (id)
	{
	case PI_VECTOR_STD_VECTOR:
		//if (!::SetPropertyHelper(m_norm, value, source, 0.001, 10.0)) {
		//	std::wstring msg = ::GetResourceString(m_owner,"ID_SOURCE_TEXT_ENTER_STD_VECTOR");
		//	::WarningMessage(m_owner, msg.c_str());
		//	return false;
		//}
		::SetPropertyHelper(m_norm, value, source);
		return true;
	case PI_VECTOR_ARROW_LENGTH:
		{
			double real = *reinterpret_cast<double*>(value);
			if (real > 0.0 && real < 1000.0) {
				if (source) { *(double*)source = m_arrowSize; }
				m_arrowSize = real;
				return true;
			}
			std::wstring msg = ::GetResourceString(m_owner,"ID_SOURCE_TEXT_INPUT_MORE_0_LESS_1000_INTO_ARROW_LENGTH");
			::WarningMessage(m_owner, msg.c_str());
		}
		return true;
	case PI_VECTOR_CENTERING:
		::SetPropertyHelper(m_centering, value, source);
		return true;
	//case PI_VECTOR_ARROW_ANGLE:
	//	::SetPropertyHelper(m_arrowAngle, value, source);
	//	return true;
	case PI_VECTOR_THRESHOLD:
		if (!::SetPropertyHelper(m_threshold, value, source, 0.0, 1.0)) {
			::WarningMessage(m_owner, ::GetResourceString(m_owner,"ID_SOURCE_TEXT_INPUT_RANGE_0_1_INTO_THRESHOLD"));
			return false;
		}
		return true;
	}
	return false;
}

void VectorPlot::Serialize(ISerializer* ar)
{
	ar->Serialize(_T("NormalizeValue"), m_norm, m_norm);
	ar->Serialize(_T("ArrowSize"), m_arrowSize, m_arrowSize);
	ar->Serialize(_T("PointCenter"), m_centering, m_centering);
	//ar->Serialize(_T("ArrowAngle"), m_arrowAngle, m_arrowAngle);
	ar->Serialize(_T("Threshold"), m_threshold, m_threshold);

	if (IReadable* reader = dynamic_cast<IReadable*>(ar)) {
		if (reader->GetMajorVersion() == 3 && reader->GetMinorVersion() < 2) {
			m_arrowSize /= 5;
		}
	}
}

#endif
