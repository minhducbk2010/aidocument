#include "stdafx.h"
#include "CurvePlot.h"
#include "TexVertex.h"
#include "Clipper.h"
#include "Axis.h"

CurvePlot::CurvePlot()
{
	m_nurbs = ::gluNewNurbsRenderer();
}

CurvePlot::~CurvePlot()
{
	::gluDeleteNurbsRenderer(m_nurbs);
}

void CurvePlot::RenderImpl(ColorTexture* /*texture*/, TexVertex const* vertexes, size_t length, bool clipping) const
{
	Clipper clipper(m_xAxis->GetSize(), m_yAxis->GetSize(), m_zAxis->GetSize());
	if (clipping) {
		clipper.Clipping();
	}

	::glColor3ub(0, 0, 0);

	int const order = 3 + 1; // 次数 + 1
	int const knotCount = static_cast<int>(length + order);
	std::vector<GLfloat> knots(knotCount);
	for (int i = 0; i < knotCount; ++i) {
		knots[i] = i + 1.0f; // 1始まり
	}

	std::vector<GLfloat> points;
	for (size_t i = 0; i < length; ++i) {
		points.push_back(static_cast<GLfloat>(vertexes[i].X));
		points.push_back(static_cast<GLfloat>(vertexes[i].Z));
		points.push_back(static_cast<GLfloat>(vertexes[i].Y));
	}

    ::gluNurbsProperty(m_nurbs, GLU_SAMPLING_TOLERANCE, 15.0);
	::gluNurbsProperty(m_nurbs, GLU_DISPLAY_MODE, GLU_OUTLINE_PATCH);

	::gluBeginCurve(m_nurbs);
        ::gluNurbsCurve(m_nurbs, knotCount, &knots[0], 3, &points[0], order, GL_MAP1_VERTEX_3);
	::gluEndCurve(m_nurbs);
}