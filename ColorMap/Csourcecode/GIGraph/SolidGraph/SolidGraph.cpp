/*!	@file
*/
#include "stdafx.h"
#include <boost/range/algorithm.hpp>
#include "SolidGraph.h"
#include "DeviceContext.h"
#include "Axis.h"
#include "Section.h"
#include "SolidFile.h"
#include "PlotGroup.h"
#include "IPlotData.h"
#include "property.h"
#include "SolidGraph/ContourPlot.h" //TODO: tmp
#include "SolidGraph/VectorPlot.h"
#include "IMessageReceiver.h" //TODO: tmp CursorValueChangedが消えたらこれも消す
#include "Message.h"
#include "common.h"
#include "RenderState.h"
#include "CrossCursor.h"
#include "ShapeWrapper.h"
#include "Legend.h"
#include "ICanvas.h"
#include "Version.h"

SolidGraph::SolidGraph(HWND window, IMessageReceiver* owner)
	: Graph3DBase(window, owner)
	, m_vectorPlot(new VectorPlot())
	, m_legend(new Legend(owner))
	, m_cursor(new CrossCursor())
	, m_graphType(GraphType::Item)
	, m_activePlot(0)
	, m_vxAxis(new VxAxis(this)), m_vyAxis(new VyAxis(this)), m_vzAxis(new VzAxis(this))
	, m_selectedIndex(-1)
	, m_dummyPlot(new PlotGroup(owner, this, m_cursor.get()))
	, m_useSection(true)
	, m_dragMarker(false)
{
	AddFile(new SolidFile(m_owner, this));
	AddPlotGroup(new PlotGroup(m_owner, this, m_cursor.get()));

	m_vectorPlot->AttachOwner(owner);
	m_vectorPlot->AttachAxes(m_xAxis.get(), m_yAxis.get(), m_zAxis.get(), m_tAxis.get());
	m_vectorPlot->AttachOwner(owner);

	m_axes.push_back(m_vxAxis.get());
	m_axes.push_back(m_vyAxis.get());
	m_axes.push_back(m_vzAxis.get());

	m_xAxis->ScaleChanged.connect(boost::bind(&SolidGraph::Modeling, this));
	m_yAxis->ScaleChanged.connect(boost::bind(&SolidGraph::Modeling, this));
	m_zAxis->ScaleChanged.connect(boost::bind(&SolidGraph::Modeling, this));
	m_tAxis->ScaleChanged.connect(boost::bind(&SolidGraph::Modeling, this));

	m_cursor->CursorChanged.connect(boost::bind(&SolidGraph::CrossCursorChanged, this));

	m_legend->PropertyUpdated.connect(boost::bind(&Graph3DBase::OnPropertyUpdated, this, _1));

	// Register Commands
	m_messageHandler[ID_PLOT_SCATTER]		= boost::bind(&SolidGraph::SimpleVisiblePlot, this, 0);
	m_messageHandler[ID_PLOT_BAR]			= boost::bind(&SolidGraph::SimpleVisiblePlot, this, 1);
	m_messageHandler[ID_PLOT_CONOTUR]		= boost::bind(&SolidGraph::SimpleVisiblePlot, this, 2);
	m_messageHandler[ID_OPT_SLICE_LINE]		= boost::bind(&SolidGraph::ToggleSliceLineVisible, this);
	m_messageHandler[ID_CMD_LIGHT]			= boost::bind(&SolidGraph::ToggleLighting, this);
	m_messageHandler[ID_CMD_MARKER]			= boost::bind(&SolidGraph::ToggleVisibleMarker, this);
	m_messageHandler[ID_CMD_PROJ_XY]		= boost::bind(&SolidGraph::ToggleXYProjection, this);
	m_messageHandler[ID_CMD_PROJ_YZ]		= boost::bind(&SolidGraph::ToggleYZProjection, this);
	m_messageHandler[ID_CMD_PROJ_ZX]		= boost::bind(&SolidGraph::ToggleZXProjection, this);
	m_messageHandler[ID_CMD_PROJ_COLOR]		= boost::bind(&SolidGraph::ToggleColorProjection, this);
	m_messageHandler[ID_CMD_PROJ_PAINT]		= boost::bind(&SolidGraph::ToggleFillProjection, this);
	m_messageHandler[ID_CMD_PROJ_SLICELINE]	= boost::bind(&SolidGraph::ToggleSliceLineProjection, this); 
    m_messageHandler[ID_CMD_LINE]			= boost::bind(&SolidGraph::ToggleVisibleSubPlot, this, 1);
	m_messageHandler[ID_CMD_CURVE]			= boost::bind(&SolidGraph::ToggleVisibleSubPlot, this, 2);
	m_messageHandler[ID_CMD_PLOT_CHANGE]	= boost::bind(&SolidGraph::ToggleSelectPlot, this);

#define F(x) (boost::function<BOOL()>)boost::bind(&SolidGraph::x, this)
#define MAKE_STATE_GETTER(method, param) boost::bind(&SolidGraph::method, this, param, _1, _2)

	m_stateGetter[ID_PLOT_SCATTER]			= MAKE_STATE_GETTER(GetPlotKind, 0);
	m_stateGetter[ID_PLOT_BAR]				= MAKE_STATE_GETTER(GetPlotKind, 1);
	m_stateGetter[ID_PLOT_CONOTUR]			= MAKE_STATE_GETTER(GetPlotKind, 2);
	m_stateGetter[ID_CMD_LIGHT]				= boost::bind(&SolidGraph::EnableLightingCommand, this, _1, _2);
	m_stateGetter[ID_CMD_MARKER]			= boost::bind(&SolidGraph::EnableMarkerCommand, this, _1, _2);
	m_stateGetter[ID_OPT_SLICE_LINE]		= MAKE_STATE_GETTER(EnabledContourCommand, F(GetVisibleSliceLine));
	m_stateGetter[ID_CMD_PROJ_XY]			= MAKE_STATE_GETTER(EnabledContourCommand,  F(GetXYProjection));
	m_stateGetter[ID_CMD_PROJ_YZ]			= MAKE_STATE_GETTER(EnabledContourCommand,  F(GetYZProjection));
	m_stateGetter[ID_CMD_PROJ_ZX]			= MAKE_STATE_GETTER(EnabledContourCommand,  F(GetZXProjection));
	m_stateGetter[ID_CMD_PROJ_COLOR]		= MAKE_STATE_GETTER(EnabledContourCommand,  F(GetColoringProjection));
	m_stateGetter[ID_CMD_PROJ_PAINT]		= MAKE_STATE_GETTER(EnabledContourCommand,  F(GetFillingProjection));
	m_stateGetter[ID_CMD_PROJ_SLICELINE]	= MAKE_STATE_GETTER(EnabledContourCommand,  F(GetSliceLineProjection));
	m_stateGetter[ID_CMD_LINE]				= MAKE_STATE_GETTER(GetSubPlotKind, 0);
	m_stateGetter[ID_CMD_CURVE]				= MAKE_STATE_GETTER(GetSubPlotKind, 1);
	m_stateGetter[ID_CMD_PLOT_CHANGE]		= boost::bind(&SolidGraph::GetSelectPlot, this, _1, _2);

	m_child.push_back(m_legend.get());

	ResetPosition();
}

SolidGraph::~SolidGraph()
{
}

void SolidGraph::AddPlotGroup(PlotGroup* group, bool serialize/* = false*/)
{
	BOOST_FOREACH(boost::shared_ptr<PlotGroup> const& plot, GetPlots()) {
		if (plot.get() == group) { return; }
	}

	group->PropertyUpdated.connect(boost::bind(&Graph3DBase::OnPropertyUpdated, this, _1));
	group->ColorPatternChanged.connect(boost::bind(&SolidGraph::ColorPatternChanged, this, _1));
	group->DataChanged.connect(boost::bind(&SolidGraph::AutoLabel, this, _1));

	group->AttachAxes(m_xAxis.get(), m_yAxis.get(), m_zAxis.get(), m_tAxis.get());
	group->AttachLegend(m_legend.get());

	if (serialize) { return; }

	bool hit = false;
	BOOST_FOREACH(boost::shared_ptr<SolidFile> const& file, m_files) {
		if (file->GetChildCount() == 0 || (size_t)file->GetFileId() == group->GetFileId()) {
			file->AddPlot(group);
			hit = true;
			break;
		}
	}

	if (!hit) {
		SolidFile* file = new SolidFile(m_owner, this);
		file->AddPlot(group);
		AddFile(file);
	}
}

void SolidGraph::AddPlotGroupToFile(PlotGroup* group)
{
	bool hit = false;
	BOOST_FOREACH(boost::shared_ptr<SolidFile> const& file, m_files) {
		if (file->GetChildCount() == 0 || (size_t)file->GetFileId() == group->GetFileId()) {
			file->AddPlot(group);
			hit = true;
			break;
		}
	}

	if (!hit) {
		SolidFile* file = new SolidFile(m_owner, this);
		file->AddPlot(group);
		AddFile(file);
	}
}

void SolidGraph::AddFile(SolidFile* file, bool update/* = false*/)
{
	// 追加できない場合は何もしない
	if (!CanAddFile()) { return; }

	if (!m_files.empty() && !update) {
		// 2つ目のファイルは青色にする
		file->SetProperty(PI_LINE_COLOR, RGB(0, 0, 255), nullptr);
	}

	file->SetParent(m_parent);
	file->AttachLegend(m_legend.get());
	file->PropertyUpdated.connect(boost::bind(&Graph3DBase::OnPropertyUpdated, this, _1));
	m_files.push_back(boost::shared_ptr<SolidFile>(file));
	m_child.push_front(file);	
}

IShape* SolidGraph::GetLegend() const
{
	return m_legend.get();
}

bool SolidGraph::CanAddFile() const
{
	if (m_dimension == 3) {
		size_t plotCount = 0;
		BOOST_FOREACH(boost::shared_ptr<SolidFile> const& f, m_files) {
			if (f->IsEmpty()) { continue; }
			plotCount += f->GetChildCount();
		}

		return plotCount < 2;
	}
	else if (m_dimension == 4) {
		size_t plotCount = 0;
		BOOST_FOREACH(boost::shared_ptr<SolidFile> const& f, m_files) {
			if (f->IsEmpty()) { continue; }
			plotCount += f->GetChildCount();
		}

		return plotCount < 1;
	}
	else if (m_dimension == 6) {
		// ベクトル図
		size_t plotCount = 0;
		BOOST_FOREACH(boost::shared_ptr<SolidFile> const& f, m_files) {
			if (f->IsEmpty()) { continue; }
			plotCount += f->GetChildCount();
		}

		return plotCount < 1;
	}
	return false;
}

bool SolidGraph::CheckUseFile(size_t fileId) const
{
	BOOST_FOREACH(boost::shared_ptr<SolidFile> const& f, m_files) {
		if (f->IsEmpty()) { continue; }

		return (size_t)f->GetFileId() == fileId;
	}
	return false;
}

void SolidGraph::UpdatePlotData()
{
	BOOST_FOREACH(boost::shared_ptr<PlotGroup> const& p, GetPlots()) {
		if (p->IsEmpty()) { continue; }

		IPlotData* data = p->GetData();
		data->ClearItems();
		data->ChangeFile(data->GetFileId(0));
		data->LinkDataSet();
	}	
	// 全プロットデータを更新してからグラフの更新を行う
	//∵重ね合わせしたグラフの更新では、全プロットデータの情報を使うため
	BOOST_FOREACH(boost::shared_ptr<PlotGroup> const& p, GetPlots()) {
		if (p->IsEmpty()) { continue; }

		UpdateData(p->GetData());
	}
}

int SolidGraph::GetActivePlotIndex() const
{
	if (m_activePlot >= GetPlots().size()) {
		const_cast<SolidGraph*>(this)->m_activePlot = 0;
	}

	return (int)m_activePlot;
}

COLORREF SolidGraph::GetActivePlotColor() const
{
	if (m_graphType == GraphType::File) {
		if (SolidFile* file = dynamic_cast<SolidFile*>(GetActivePlot()->GetParent())) {
			return file->GetPlotColor();
		}
	}
	return GetActivePlot()->GetPlotColor();
}

PlotGroup* SolidGraph::GetEmptyPlot() const
{
	BOOST_FOREACH(boost::shared_ptr<PlotGroup> const& g, GetPlots()) {
		if (g->IsEmpty()) {
			return g.get();
		}
	}
	return nullptr;
}

void SolidGraph::UpdateData(IPlotData* data)
{
	if (!data) { return; }

	for (size_t i = 0; i < data->GetDataCount(); ++i) {
		bool fail = false;
		for (int j = 0; j < m_dimension; ++j) {
			if (data->GetData(j, i) == FLT_MAX) {
				fail = true;
				break;
			}
		}
		if (fail) { continue; }
	}

	if (data->GetDataCount()) {
		AutoScaleAxes();
		AutoLabelAxes();

		boost::for_each(m_files, boost::bind(&SolidFile::AutoNaming, _1));
	}
	Modeling();
	RefreshSection();
}

void SolidGraph::SetLocation(double left, double top)
{
	m_legend->Move(left, top);
}

PlotList SolidGraph::GetPlots() const
{
	PlotList list;
	BOOST_FOREACH(boost::shared_ptr<SolidFile> const& file, m_files) {
		for (size_t i = 0; i < file->GetChildCount(); ++i) {
			list.push_back(file->GetPlot(i));
		}
	}
	return list;
}

PlotGroup* SolidGraph::GetPlot(size_t index) const
{
	PlotList plots = GetPlots();
	if (plots.empty()) {
		return m_dummyPlot.get();
	}
	return plots.size() <= index ? nullptr : plots[index].get();
}

size_t SolidGraph::GetPlotCount() const
{
	return GetPlots().size();
}

PlotGroup* SolidGraph::GetActivePlot() const
{
	return GetPlot(GetActivePlotIndex());
}

void SolidGraph::SetParent(IGroup* parent)
{
	__super::SetParent(parent);
	boost::for_each(m_files, boost::bind(&SolidFile::SetParent, _1, parent));
	m_legend->SetParent(parent);
}

void SolidGraph::SetSelected(bool select)
{
	if (!select) {
		boost::for_each(m_child, boost::bind(&IShape::SetSelected, _1, false));
	}
}

void SolidGraph::SetData(size_t index, IPlotData* data)
{
	if (!data) { return; }

	PlotList plots = GetPlots();
	if (plots.size() > index) {
		plots[index]->SetData(data);
		UpdateData(data);
	}
}

IShape* SolidGraph::AddPlot(IPlotData* data, size_t kind)
{
	PlotGroup* grp = GetEmptyPlot();
	if (!grp) {
		grp = new PlotGroup(m_owner, this, m_cursor.get());
	}
	grp->SetData(data);
	AddPlotGroup(grp);

	grp->SimpleVisiblePlot(kind, true);
	grp->SetDimension(m_dimension);
	grp->SetProperty(PI_LINE_COLOR, RGB(0, 0, 255), nullptr);

	if (!g_isLibMode) {
		if (GetPlotCount() >= 2) {
			// 重ね描きした時点で、ワイヤーフレームのみを表示するよう状態を切り替える
			//TODO: Undo(3.3)
			SetProperty(PI_3D_WIREFRAME_VISIBLE, TRUE, nullptr);
			SetProperty(PI_3D_FILLING, FALSE, nullptr);
			SetProperty(PI_3D_COLORING, FALSE, nullptr);
			SetProperty(PI_3D_COLORBAR_VISIBLE, FALSE, nullptr);
			m_legend->SetProperty(PI_VISIBLE, TRUE, nullptr);
		}
	}

	UpdateData(data);
	return grp;
}

void SolidGraph::RemovePlot(size_t index)
{
	PlotList plots = GetPlots();
	// プロットは常に1つだけは残す
	if (plots.size() == 1 || plots.size() <= index) { return; }

	// 1プロット = 1ファイルという前提なので、ファイルを消す
	boost::shared_ptr<SolidFile> deletedFile;
	for (size_t i = 0; i < m_files.size(); ++i) {
		BOOST_FOREACH(boost::shared_ptr<PlotGroup> const& plot, m_files[i]->GetPlots()) {
			if (plot == plots[index]) {
				deletedFile = m_files[index];
				m_files.erase(m_files.begin() + i);
				goto LOOP_END;
			}
		}
	}
LOOP_END:

	for (size_t i = 0; i < m_child.size(); ++i) {
		if (m_child[i] == deletedFile.get()) {
			m_child.erase(m_child.begin() + i);
			break;
		}
	}
}

void SolidGraph::ClearPlots()
{
	//TODO: impl
}

IGraphFile* SolidGraph::AddFile(size_t fileId)
{
	// 追加できない場合は何もしない
	if (!CanAddFile()) { return NULL; }

	bool first = true;


	if (SolidFile* file = dynamic_cast<SolidFile*>(GetEmptyFile())) {
		BOOST_FOREACH(boost::shared_ptr<PlotGroup> const& plot, file->GetPlots()) {
			plot->GetData()->CopyFrom(fileId, plot->GetData());
			bool const success = plot->GetData()->LinkDataSet();
			if(!success)
			{
				if (first) {
					ShowItemMismatchError(m_owner);
					first = false;	
				}
			}
			plot->SetData(plot->GetData());
		}

		return file;
	}
	return __super::AddFile(fileId);
}

IShape* SolidGraph::AddData(IPlotData* data)
{
	PlotGroup* ret = 0;
	if (PlotGroup* empty = GetEmptyPlot()) {
		empty->SetData(data);
		ret = empty;
	}
	else {
		PlotList plots = GetPlots();
		PlotGroup* grp = new PlotGroup(m_owner, this, m_cursor.get());
		grp->SetData(data);
		AddPlotGroup(grp);

		grp->SimpleVisiblePlot(2, true);
		grp->SetDimension(m_dimension);
		grp->SetProperty(PI_LINE_COLOR, RGB(0, 0, 255), nullptr);

		if (!plots.empty()) {
			grp->CopyFrom(plots[0].get());
		}
		ret = grp; 
	}

	if (GetPlotCount() >= 2) {
		// 重ね描きした時点で、ワイヤーフレームのみを表示するよう状態を切り替える
		//TODO: Undo(3.3)
		SetProperty(PI_3D_WIREFRAME_VISIBLE, TRUE, nullptr);
		SetProperty(PI_3D_FILLING, FALSE, nullptr);
		SetProperty(PI_3D_COLORING, FALSE, nullptr);
		SetProperty(PI_3D_COLORBAR_VISIBLE, FALSE, nullptr);
		m_legend->SetProperty(PI_VISIBLE, TRUE, nullptr);
	}

	UpdateData(data);
	return ret;
}

void SolidGraph::SetPlotKind(int kind)
{
	SimpleVisiblePlot(kind);
}

void SolidGraph::SetPlotKind(int index, int kind)
{
	if (PlotGroup* plot = GetPlot(index)) {
		plot->SimpleVisiblePlot(kind, true);
	}
}

void SolidGraph::SetPlotVisible(int index, int kind, bool visible)
{
	if (PlotGroup* plot = GetPlot(index)) {
		plot->SimpleVisiblePlot(kind, visible);
	}
}

size_t SolidGraph::GetFileCount() const
{
	return m_files.size();
}
IGraphFile* SolidGraph::GetFile(size_t index) const
{
	return m_files[index].get();
}
IGraphFile* SolidGraph::GetEmptyFile() const
{
	BOOST_FOREACH(boost::shared_ptr<SolidFile> const& file, m_files) {
		if (file->IsEmpty()) {
			return file.get();
		}
	}
	return nullptr;
}

void SolidGraph::SimpleVisiblePlot(size_t type)
{
	GetActivePlot()->SimpleVisiblePlot(type, true);

	// コンターマップ以外に変更したら、断面を隠す
	if (type != 2 && (m_verticalSection || m_horizontanlSection)) {
		HideSection();
	}
}

void SolidGraph::VisiblePlot(size_t type, bool visible)
{
	GetActivePlot()->VisiblePlot(type, visible);
}

void SolidGraph::VisibleSubPlot(UINT type)
{
	GetActivePlot()->VisibleSubPlot(type);
}

void SolidGraph::ToggleVisibleSubPlot(UINT type)
{
	GetActivePlot()->ToggleVisibleSubPlot(type);
}

void SolidGraph::EnableMarker(bool enable)
{
	GetActivePlot()->EnableMarker(enable);
}

bool SolidGraph::IsVisibleMarker() const
{
	return GetActivePlot()->IsVisibleMarker();
}

void SolidGraph::EnableMarkerCommand(BOOL* enabled, BOOL* checked) const
{
	*enabled = GetActivePlot()->GetVisiblePlotIndex() != 0;//TODO: const
	*checked = IsVisibleMarker();
}

void SolidGraph::EnableLighting(bool enable)
{
	GetActivePlot()->EnableLighting(enable);
}
bool SolidGraph::IsLighting() const
{
	return GetActivePlot()->IsLighting();
}

void SolidGraph::SetVisibleSliceLine(bool visible)
{
	GetActivePlot()->SetVisibleSliceLine(visible);
}

bool SolidGraph::GetVisibleSliceLine() const
{
	return GetActivePlot()->GetVisibleSliceLine();
}

void SolidGraph::EnableHiddenLine(bool hidden)
{
	GetActivePlot()->EnableHiddenLine(hidden);
}
bool SolidGraph::IsHiddenLine() const
{
	return GetActivePlot()->IsHiddenLine();
}

void SolidGraph::VisibleCursor(bool visible)
{
	GetActivePlot()->VisibleCursor(visible);
}

void SolidGraph::SetXYProjection(bool state)
{
	GetActivePlot()->SetXYProjection(state);
}
bool SolidGraph::GetXYProjection() const
{
	return GetActivePlot()->GetXYProjection();
}

void SolidGraph::SetYZProjection(bool state)
{
	GetActivePlot()->SetYZProjection(state);
}
bool SolidGraph::GetYZProjection() const
{
	return GetActivePlot()->GetYZProjection();
}

void SolidGraph::SetZXProjection(bool state)
{
	GetActivePlot()->SetZXProjection(state);
}
bool SolidGraph::GetZXProjection() const
{
	return GetActivePlot()->GetZXProjection();
}

void SolidGraph::SetColoringProjection(bool state)
{
	GetActivePlot()->SetColoringProjection(state);
}
bool SolidGraph::GetColoringProjection() const
{
	return GetActivePlot()->GetColoringProjection();
}

void SolidGraph::SetFillingProjection(bool state)
{
	GetActivePlot()->SetFillingProjection(state);
}
bool SolidGraph::GetFillingProjection() const
{
	return GetActivePlot()->GetFillingProjection();
}

void SolidGraph::SetSliceLineProjection(bool state)
{
	GetActivePlot()->SetSliceLineProjection(state);
}
bool SolidGraph::GetSliceLineProjection() const
{
	return GetActivePlot()->GetSliceLineProjection();
}

void SolidGraph::SetContourAlgorithm(long type)
{
	GetActivePlot()->SetContourAlgorithm(type);
}
long SolidGraph::GetContourAlgorithm() const
{
	return GetActivePlot()->GetContourAlgorithm();
}

BOOL SolidGraph::GetViewColorBar() const
{
	return __super::GetViewColorBar() && GetPlotCount() <= 1;
}

bool SolidGraph::UseDigitalmap() const
{
	return m_dimension == 3 && GetPlotCount() == 1 && GetActivePlot()->UseDigitalmap();
}

void SolidGraph::ShowDigitalmap()
{
	GetActivePlot()->ShowDigitalmap();
}

void SolidGraph::ResetCursor()
{
	if (ContourPlot* c = dynamic_cast<ContourPlot*>(GetActivePlot()->GetPlot(2))) {
		c->ResetCursor();

		m_xSection->Clear();
		m_ySection->Clear();
	}
}

void SolidGraph::Modeling()
{
	boost::for_each(m_files, boost::bind(&SolidFile::Modeling, _1));
}

bool SolidGraph::CheckUseColorPattern(TCHAR const* path)
{
	BOOST_FOREACH(boost::shared_ptr<SolidFile> const& file, m_files) {
		BOOST_FOREACH(boost::shared_ptr<PlotGroup> const& plot, file->GetPlots()) {
			if (plot->CheckUseColorPattern(path)) {
				return true;
			}
		}
	}

	return false;
}

bool SolidGraph::UpdateColorPattern(TCHAR const* path)
{
	bool changed = false;
	BOOST_FOREACH(boost::shared_ptr<SolidFile> const& file, m_files) {
		BOOST_FOREACH(boost::shared_ptr<PlotGroup> const& plot, file->GetPlots()) {
			plot->UpdateColorPattern(path);
			changed = true;
		}
	}
	return changed;
}

size_t SolidGraph::GetChildCount() const
{
	return m_child.size();
}
IShape* SolidGraph::GetChild(size_t index)
{
	return m_child[index];
}

template<class T, class U>
bool operator == (boost::shared_ptr<T>& a, U* b) { return a.get() == b; }

template<class Container>
bool move(Container& src, Container& dst, IShape* shape)
{
	for (Container::iterator it = src.begin(); it != src.end(); ++it) {
		if (*it == shape) {
			dst.push_front(*it);
			src.erase(it);
			return true;
		}
	}
	return true;
}

void SolidGraph::RemoveChild(IShape* obj)
{
	if (move(m_files, m_deletedFiles, obj)) {
		move(m_child, m_deletedChild, obj);
		m_activePlot = 0; // プロットの数が減るので

		OnPropertyUpdated(true);
	}
}
void SolidGraph::InsertChild(IShape* obj)
{
	if (move(m_deletedFiles, m_files, obj)) {
		move(m_deletedChild, m_child, obj);
		OnPropertyUpdated(true);
	}
}

bool SolidGraph::EnableCursor() const
{
	return EnableSection();
}

void SolidGraph::SetCursor(int x, int y)
{
	DeviceContextPtr context = DeviceContext::CreateInstance(m_wnd);
	context->MakeCurrentContext();

	int const width = EnableVerticalSection() ? m_width - 110 : m_width;
	int const height = EnableHorizontalSection() ? m_height - 110 : m_height;
	Reshape(width, height, 1);

	::glPushMatrix();
	{
		RenderState state(m_pitch, m_yaw, m_roll, m_h, m_v, m_scale, m_viewColorBar);

		if (ContourPlot* c = dynamic_cast<ContourPlot*>(GetActivePlot()->GetPlot(2))) {
			c->SetCursor(&state, x, y);
		}
	}
	::glPopMatrix();

	RefreshSection();
}

void SolidGraph::SelectIndex(int index, int line)
{
	GetActivePlot()->SelectCursor(line, index);
	RefreshSection();
}

void SolidGraph::GetCursorIndex(int* index, int* line) const
{
	GetActivePlot()->GetCursorIndex(line, index);
}

//! カーソル位置の断面を更新
void SolidGraph::RefreshSection()
{
	m_xSection->Clear();
	m_ySection->Clear();
	BOOST_FOREACH(ContourPlot* map, m_cursor->GetPlots()) {
		std::vector<OCPoint> xLine, yLine;
		map->GetSectionData(&xLine, &yLine);

		std::vector<OCPoint> xData, yData;
		for (size_t i = 0; i < xLine.size(); ++i) {
			xData.push_back(OCPoint(
				m_xSection->GetXAxis()->CoordinateToValue(xLine[i].x),
				m_xSection->GetYAxis()->CoordinateToValue(xLine[i].y)));
		}
		for (size_t i = 0; i < yLine.size(); ++i) {
			yData.push_back(OCPoint(
				m_ySection->GetXAxis()->CoordinateToValue(yLine[i].x),
				m_ySection->GetYAxis()->CoordinateToValue(yLine[i].y)));
		}

		m_xSection->AddData(xData);
		m_ySection->AddData(yData);
	}
	m_xSection->SetCursorValue(m_cursorValue[0]);
	m_ySection->SetCursorValue(m_cursorValue[1]);
}

bool SolidGraph::PickUpData(HDC dc, int x, int y)
{
	DeviceContextPtr context = DeviceContext::CreateInstance(dc);
	context->MakeCurrentContext();

	int const width = EnableVerticalSection() ? m_width - 110 : m_width;
	int const height = EnableHorizontalSection() ? m_height - 110 : m_height;
	Reshape(width, height, 1);

	PlotGroup* plot = GetActivePlot();
	RenderState state(m_pitch, m_yaw, m_roll, m_h, m_v, m_scale, m_viewColorBar);
	m_selectedIndex = plot->PickUpData(&state, x, y);

	if (m_selectedIndex != -1) {
		MarkerClicked(m_activePlot, m_selectedIndex);
	}

	return m_selectedIndex != -1;
}

bool SolidGraph::HitTestMarker(HDC dc, int x, int y)
{
	DeviceContextPtr context = DeviceContext::CreateInstance(dc);
	context->MakeCurrentContext();

	int const width = EnableVerticalSection() ? m_width - 110 : m_width;
	int const height = EnableHorizontalSection() ? m_height - 110 : m_height;
	Reshape(width, height, 1);

	RenderState state(m_pitch, m_yaw, m_roll, m_h, m_v, m_scale, m_viewColorBar);
	return GetActivePlot()->HitTest(&state, x, y) != -1;
}

bool SolidGraph::DragMarker(OCPoint const& offset)
{
	PlotGroup* plot = GetActivePlot();
	IPlotData* data = plot->GetData();
	if (m_selectedIndex < 0 || m_selectedIndex >= data->GetDataCount()) { return false; }

	// マウスの位置から新しいZ値を求める
	double const offsetValue = offset.y / GetParentRect().Height();
	double const range = m_zAxis->GetMaximum() - m_zAxis->GetMinimum();
	double const value = data->GetRawData(2, m_selectedIndex) + offsetValue * range;
	data->SetRawData(2, m_selectedIndex, value);

	if (m_dragValue != value) {
		// 値が変わっていたらクライアントに通知
		MarkerValueChanged(m_activePlot, m_selectedIndex, m_dragValue);
	}

	m_dragValue = value;
	m_dragMarker = true;

	plot->ChangeZValue(m_selectedIndex, value);

	return true;
}

void SolidGraph::EndMarkerDrag()
{
	if (m_dragMarker) {
		// クライアントに通知
		MarkerValueChanged(m_activePlot, m_selectedIndex, m_dragValue);
	}
	m_dragMarker = false;
}

//! 選択されているマーカーの値を表示する
void RenderSelectValue(IPlotData* data, int dim, size_t index, int maxWidth)
{
	if (!data || data->GetDataCount() <= index) { return; }

	TextRenderer* text = TextRenderer::GetInstance();
	int const y = 16 * dim;
	int const width = text->GetTextSize(data->GetItemName(dim)).cx;

	text->AddText(data->GetItemName(dim), maxWidth - width, y, 0, RGB(0, 0, 0), TextAlign::Left);

	TCHAR buf[64] = {};
	real_t const value = data->GetData(dim, index);
	if (value == FLT_MAX) {
		::_sntprintf_s(buf, _countof(buf) - 1, _T(" : -"));
	}
	else {
		::_sntprintf_s(buf, _countof(buf) - 1, _T(" : %g"), value);
	}

	text->AddText(buf, maxWidth, y, 0, RGB(0, 0, 0), TextAlign::Left);
}

OCRect SolidGraph::GetParentRect() const
{
	OCRect r;
	IShape* parent = dynamic_cast<IShape*>(m_parent);
	parent->GetRect(&r.left, &r.top, &r.right, &r.bottom);
	r = MMToPx(r);
	return r;
}

void SolidGraph::RenderPlots(HDC dc) const
{
	if (GetActivePlot()->IsVisible()) {
		m_cursor->Render(m_zAxis->GetSize());
	}
	std::for_each(m_files.begin(), m_files.end(), boost::bind(&SolidFile::Render, _1, IsClipping()));

	if (m_dimension == 6) {
		std::vector<Vector> v;
		if (IPlotData* data = GetData(0)) {
			for (size_t i = 0, n = data->GetDataCount(); i < n; ++i) {
				double const x = data->GetData(0, i);
				double const y = data->GetData(1, i);
				double const z = data->GetData(2, i);
				double const vx = data->GetData(3, i);
				double const vy = data->GetData(4, i);
				double const vz = data->GetData(5, i);
				if (x == FLT_MAX || y == FLT_MAX || z == FLT_MAX ||
					vx == FLT_MAX || vy == FLT_MAX || vz == FLT_MAX) { continue; }

				Vector tmp = {
					m_xAxis->ValueToPosition(x),
					m_zAxis->ValueToPosition(z),
					m_yAxis->ValueToPosition(y),
					vx, vz, vy,
				};
				v.push_back(tmp);
			}
			if (!v.empty()) {
				m_vectorPlot->Render(GetPlots()[0]->GetColorPattern(), &v[0], v.size(), IsClipping());
			}
		}
	}

	// マーカーが選択されていたらその値を表示
	if (m_selectedIndex >= 0) {
		IPlotData* data = GetData(0);

		int maxWidth = 0;
		TextRenderer* text = TextRenderer::GetInstance();
		for (int i = 0; i < m_dimension; ++i) {
			maxWidth = std::max<int>(text->GetTextSize(data->GetItemName(i)).cx, maxWidth);
		}

		for (int i = 0; i < m_dimension; ++i) {
			RenderSelectValue(data, i, m_selectedIndex, maxWidth);
		}
	}

	if (m_dimension != 6) {
		OCRect r = GetParentRect();
		IShape* parent = dynamic_cast<IShape*>(m_parent);
		double const zoomRatio = m_capturing ? 1 : parent->GetCanvas()->GetZoomRatio();
		int width = static_cast<int>(r.Width() * zoomRatio);
		if (EnableVerticalSection()) {
			width -= 110;
		}

		if (m_graphType == GraphType::Item) {
			m_legend->Draw<PlotGroup>(dc, GetPlots(), width);
		}
		else {
			m_legend->Draw<SolidFile>(dc, m_files, width);
		}
	}
}

void SolidGraph::RenderColorBar() const
{
	PlotList plots = GetPlots();
	for (size_t i = 0; i < plots.size(); ++i) {
		plots[i]->RenderColorBar(i != 0, plots.size() > 1 || m_dimension == 4 || m_dimension == 6);

		// 4Dの時は二本目以降は描かない
		if (m_dimension == 4 || m_dimension == 6) { break; }
	}
}

BOOL SolidGraph::GetViewWireframe() const
{
	return GetActivePlot()->IsWireFrameVisible();
}

void SolidGraph::SetViewWireframe(bool value)
{
	GetActivePlot()->EnableWireFrame(value);
}

void SolidGraph::EnableFilling(bool enable)
{
	GetActivePlot()->EnableFilling(enable);
}
BOOL SolidGraph::IsFilling() const
{
	return GetActivePlot()->IsFilling();
}

void SolidGraph::EnableColoring(bool enable)
{
	GetActivePlot()->EnableColoring(enable);
}
BOOL SolidGraph::IsColoring() const
{
	return GetActivePlot()->IsColoring();
}

void SolidGraph::AfterAutoScaleAxes()
{
	Modeling();
}

IPlotData* SolidGraph::GetData(size_t const index) const
{
	size_t count = 0;
	BOOST_FOREACH(boost::shared_ptr<SolidFile> const& file, m_files) {
		for (size_t i = 0; i < file->GetChildCount(); ++i) {
			if (count == index) {
				return file->GetPlot(i)->GetData();
			}
			++count;
		}
	}
	return nullptr;
}

size_t SolidGraph::GetDataCount() const
{
	size_t count = 0;
	BOOST_FOREACH(boost::shared_ptr<SolidFile> const& file, m_files) {
		count += file->GetChildCount();
	}
	return count;
}

bool SolidGraph::EnableSection() const
{
	if (!m_useSection) { return false; }

	if (ContourPlot const* c = dynamic_cast<ContourPlot const*>(GetActivePlot()->GetPlot(2))) {
		if (c->IsVisible() && c->EnableCursor()) {
			return __super::EnableSection() && m_dimension == 3;
		}
	}

	return false;
}

void SolidGraph::ToggleSliceLineVisible()
{
	SetVisibleSliceLine(!GetVisibleSliceLine());
}
void SolidGraph::ToggleLighting()
{
	EnableLighting(!IsLighting());
}
void SolidGraph::ToggleVisibleMarker()
{
	EnableMarker(!IsVisibleMarker());
}

void SolidGraph::ToggleXYProjection()
{
	SetXYProjection(!GetXYProjection());
}
void SolidGraph::ToggleYZProjection()
{
	SetYZProjection(!GetYZProjection());
}
void SolidGraph::ToggleZXProjection()
{
	SetZXProjection(!GetZXProjection());
}
void SolidGraph::ToggleColorProjection()
{
	SetColoringProjection(!GetColoringProjection());
}
void SolidGraph::ToggleFillProjection()
{
	SetFillingProjection(!GetFillingProjection());
}
void SolidGraph::ToggleSliceLineProjection()
{
	SetSliceLineProjection(!GetSliceLineProjection());
}

void SolidGraph::ToggleSelectPlot()
{
	// 0 <-> 1
	m_activePlot = m_activePlot == 0 ? 1 : 0;
	m_cursor->Activate(m_activePlot);
	// プロットが重ね描きされている場合、アクティブプロットとそうでないプロットで
	// 線種が異なっているので、ここで更新しておく
	RefreshSection();
}

void SolidGraph::ResetPosition()
{
	Rotate(20, 45, 0);
	Scaling(1);
	Translate(0, 0);
}

//! 断面を隠す
void SolidGraph::HideSection()
{
	if (!m_verticalSection && !m_horizontanlSection) { return; }

	int const width = m_verticalSection ? m_width - SECTION_SIZE : m_width;
	int const height = m_horizontanlSection ? m_height - SECTION_SIZE : m_height;
	Resized(width, height);

	m_verticalSection = m_horizontanlSection = false;
}

void SolidGraph::GetPlotKind(size_t targetKind, BOOL* enabled, BOOL* checked) const
{
	*enabled = m_dimension != 6;
	*checked = targetKind == GetActivePlot()->GetVisiblePlotIndex();
}

void SolidGraph::GetSubPlotKind(size_t targetKind, BOOL* enabled, BOOL* checked) const
{
	*enabled = TRUE;
	*checked = targetKind == GetActivePlot()->GetVisibleSubPlotIndex();
}

void SolidGraph::EnabledContourCommand(boost::function<BOOL()> f, BOOL* enabled, BOOL* checked) const
{
	*enabled = GetActivePlot()->GetVisiblePlotIndex() == 2;//TODO: const
	*checked = f();
}

void SolidGraph::EnabledColorBar(BOOL* enabled, BOOL* checked) const
{
	// カラーバーはプロットが1本以下の時のみ表示可能(今後のバージョンアップで表示可能にする)
	*enabled = GetPlotCount() <= 1;
	*checked = GetViewColorBar();
}

void SolidGraph::EnableLightingCommand(BOOL* enabled, BOOL* checked) const
{
	// ベクトル図は未対応
	*enabled = m_dimension != 6 && GetActivePlot()->GetVisiblePlotIndex() != 2;//TODO: const
	*checked = IsLighting();
}

void SolidGraph::GetSelectPlot(BOOL* enabled, BOOL* checked) const
{
	*enabled = m_dimension != 6 && GetPlotCount() > 1;
	*checked = m_activePlot == 0;
}

#if 1

#include "PropertyCreator.hpp"

void SolidGraph::SetDimension(int const dim)
{
	__super::SetDimension(dim);
	boost::for_each(m_files, boost::bind(&SolidFile::SetDimension, _1, dim));

	if (dim == 6) {
		AutoScaleAxis(m_vxAxis.get());
		AutoScaleAxis(m_vyAxis.get());
		AutoScaleAxis(m_vzAxis.get());

		SimpleVisiblePlot(size_t(-1));
	}
}

void SolidGraph::AddShowPropertyOption(IPropertyGroup* group, bool const locked)
{
	group->AddBoolProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_EXPLANATORY_NOTES"), PI_LEGEND_VISIBLE, m_legend->IsVisible(), locked);

	std::wstring textType1 = ::GetResourceString(m_owner,"ID_SOURCE_TEXT_FILE_DISTINCTION");
	std::wstring textType2 = ::GetResourceString(m_owner,"ID_SOURCE_TEXT_ITEM_DISTINCTION");
	group->AddArrayProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_TYPE"), PI_GRAPH_TYPE, m_graphType.underlying(), locked, 2, 
		textType1.c_str(), textType2.c_str());
}

template<class T>
void SolidGraph::AddPlotProperty(T* parent, bool const locked) const
{
	IPropertyGroup* grp = parent->AddGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_PLOT"));

	if (m_dimension != 6) {
		size_t const plotIndex = GetActivePlot().GetVisiblePlotIndex();
		bool const useMarker = !GetActivePlot().IsVisibleMarker() && plotIndex != 0;

		std::wstring classification1 = ::GetResourceString(m_owner,"ID_SOURCE_TEXT_SCATTER_DIAGRAM");
		std::wstring classification2 = ::GetResourceString(m_owner,"ID_SOURCE_TEXT_BAR_GRAPH");
		std::wstring classification3 = ::GetResourceString(m_owner,"ID_SOURCE_TEXT_CONTOUR_MAP");
		grp->AddArrayProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_CLASSIFICATION"), PI_3D_PLOT_KIND, (int)plotIndex, locked, 3, 
			classification1.c_str(), classification2.c_str(), classification3.c_str());
		//TODO: マーカーと散布図のプロパティは共用で構わないか?
		grp->AddSelectorPropertyR(::GetResourceString(m_owner,"ID_SOURCE_TEXT_MARKER_SIZE"), PI_SCATTER_SIZE, locked || useMarker,
			GetActivePlot().GetMarkerSize(), 9, 0.1, 0.5, 1.0, 1.5, 2.0, 3.0, 4.0, 5.0, 10.0);
	}
	else {
		bool const useMarker = !GetActivePlot().IsVisibleMarker();
		grp->AddSelectorPropertyR(::GetResourceString(m_owner,"ID_SOURCE_TEXT_MARKER_SIZE"), PI_SCATTER_SIZE, locked || useMarker,
			GetActivePlot().GetMarkerSize(), 9, 0.1, 0.5, 1.0, 1.5, 2.0, 3.0, 4.0, 5.0, 10.0);
	}
	grp->AddBoolProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_CLIPPING"), PI_DATA_CLIPPING, m_clipMaxX, locked); //TODO: ???
}

void SolidGraph::AddPropertyImpl(OCProperty* prop, bool locked) const
{
	//AddPlotProperty(prop, locked);

	if (m_dimension == 6) {
		m_vectorPlot->AddProperty(prop, locked);
	}
	else {
		//GetActivePlot().AddProperty(prop, locked);
	}
}

void SolidGraph::AddTabPropertyImpl(OCProperty* prop, bool locked) const
{
	//AddPlotProperty(prop->AddGroup(_T("プロット")), locked);

	if (m_dimension == 6) {
		m_vectorPlot->AddProperty(prop->AddGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_ARROW")), locked);
	}
	else {
		//GetActivePlot().AddProperty(prop->AddGroup(_T("マップ演算")), locked);
	}
}

bool SolidGraph::SetPropertyImpl(long id, LPARAM value, LPARAM* source)
{
	switch(id)
	{
	case PI_LEGEND_VISIBLE:
		m_legend->SetProperty(PI_VISIBLE, value, source);
		return true;
	case PI_GRAPH_TYPE:
		{
			int n = m_graphType.underlying();
			::SetPropertyHelper(n, value, source);
			m_graphType = GraphType(static_cast<GraphType_def::type>(n));
			SetGraphType(m_graphType);
		}
		return true;
	case PI_3D_FILLING:
		boost::for_each(GetPlots(), boost::bind(&PlotGroup::EnableFilling, _1, !!value));
		return true;
	case PI_3D_WIREFRAME_VISIBLE:
		boost::for_each(GetPlots(), boost::bind(&PlotGroup::EnableWireFrame, _1, !!value));
		return true;
	case PI_3D_COLORING:
		boost::for_each(GetPlots(), boost::bind(&PlotGroup::EnableColoring, _1, !!value));
		return true;
	case PI_3D_PLOT_VECOTR:
		return true;
	case PI_VXDATA_INDEX:
		SetDataProperty(GetData(0)->GetItemId(3), 3, value, source);
		return true;
	case PI_VYDATA_INDEX:
		SetDataProperty(GetData(0)->GetItemId(4), 4, value, source);
		return true;
	case PI_VZDATA_INDEX:
		SetDataProperty(GetData(0)->GetItemId(5), 5, value, source);
		return true;
	case PI_3D_CALCULATE_TYPE:
		if (value != ContourPlot::Gridding) {
			HideSection();
		}
		// returnはしない
	}

	return 
		m_vectorPlot->SetProperty(id, value, source) ||
		GetActivePlot()->SetProperty(id, value, source);
}

void SolidGraph::SetGraphType(GraphType type)
{
	BOOST_FOREACH(boost::shared_ptr<PlotGroup> const& plot, GetPlots()) {
		SolidFile* file = dynamic_cast<SolidFile*>(plot->GetParent());
		assert(file);

		COLORREF color = type == GraphType::File ? file->GetPlotColor() : plot->GetPlotColor();
		plot->SetProperty(PI_3D_WIREFRAME_COLOR, color, nullptr);
	}
}

bool SolidGraph::GetPropertyImpl(long id, LPARAM* ret) const
{
	return GetActivePlot()->GetProperty(id, ret);
}

#include "ISerializer.h"
void SolidGraph::SerializeImpl(ISerializer* ar)
{
	ar->Serialize(_T("Dimension"), m_dimension);
	{
		int n = m_graphType.underlying();
		ar->Serialize(_T("GraphType"), n, GraphType::Item);
		m_graphType = static_cast<GraphType_def::type>(n);
	}

	m_cursor->Serialize(ar->ChildNode(_T("CrossCursor")));

	PlotList plots = GetPlots();
	if (IReadable* r = dynamic_cast<IReadable*>(ar)) {
		Version const ver(r->GetMajorVersion(), r->GetMinorVersion(), r->GetReleaseVersion(), r->GetBuildVersion());
		if (ver <= Version(3, 3, 0, 0)) {
			GetActivePlot()->Serialize(ar);
		}
		else {
			if (IReadable* files = dynamic_cast<IReadable*>(ar->ChildNode(_T("Files")))) {
				for (size_t i = 0; i < files->ChildCount(); ++i) {
					if (m_files.size() > i) {
						m_files[i]->Serialize(files->ChildNode(i));
					}
					else {
						SolidFile* file = new SolidFile(m_owner, this);
						file->Serialize(files->ChildNode(i));
						AddFile(file, true);
					}
				}
			}

			for (size_t i = 0; ; ++i) {
				std::wstring name = (boost::wformat(_T("Plot_%1%")) % (i + 1)).str();
				if (IReadable* child = dynamic_cast<IReadable*>(ar->ChildNode(name.c_str()))) {
					if (plots.size() <= i) {
						PlotGroup* g = new PlotGroup(m_owner, this, m_cursor.get());
						AddPlotGroup(g, true);
						g->Serialize(child);
						AddPlotGroupToFile(g);
					}
					else {
						plots[i]->Serialize(child);
					}
				}
				else { break; }
			}
		}
		SetGraphType(m_graphType);
	}
	else { // 書き込み
		ISerializer* files = ar->ChildNode(_T("Files"));
		BOOST_FOREACH(boost::shared_ptr<SolidFile> const& file, m_files) {
			file->Serialize(files->ChildNode(_T("File")));
		}

		for (size_t i = 0; i < plots.size(); ++i) {
			std::wstring name = (boost::wformat(_T("Plot_%1%")) % (i + 1)).str();
			plots[i]->Serialize(ar->ChildNode(name.c_str()));
		}
	}
	SetDimension(m_dimension);

	if (m_vectorPlot) {
		m_vectorPlot->Serialize(ar);
	}

	if (m_dimension == 6)
	{
		// ベクトル図の場合はすべての軸はリニアになる
		if (m_xAxis->GetScaleType() != ST_LINEAR) { m_xAxis->SetProperty(PI_SCALE_TYPE, (int)ST_LINEAR, nullptr); }
		if (m_yAxis->GetScaleType() != ST_LINEAR) { m_yAxis->SetProperty(PI_SCALE_TYPE, (int)ST_LINEAR, nullptr); }
		if (m_zAxis->GetScaleType() != ST_LINEAR) { m_zAxis->SetProperty(PI_SCALE_TYPE, (int)ST_LINEAR, nullptr); }
		if (m_tAxis->GetScaleType() != ST_LINEAR) { m_tAxis->SetProperty(PI_SCALE_TYPE, (int)ST_LINEAR, nullptr); }
	}

	m_legend->Serialize(ar->ChildNode(_T("Legend")));
}

#endif

std::wstring to_s(double const value)
{
	if (value == FLT_MAX) { return _T("-"); }

	// Boost.FormatはLocaleに応じて1000が"1,000"になったりするので、printfを使う
	TCHAR s[100] = {};
	::_sntprintf_s(s, _countof(s) - 1, _T("%g"), value);
	return s;
}

void SolidGraph::CrossCursorChanged() const
{
	#define MAKE_ITEM(a, b, c) std::make_pair((boost::wformat(L"%1% %2%") % (a) % (b)).str(), to_s(c).c_str())

	TexVertex const v1 = m_cursor->GetCursorPos(0);
	double const x = m_xAxis->CoordinateToValue(v1.X);
	double const y = m_yAxis->CoordinateToValue(-v1.Y);
	double const z1 = m_zAxis->CoordinateToValue(v1.Z);

	size_t const idx1 = m_activePlot, idx2 = m_activePlot == 0 ? 1 : 0;
	IPlotData* data = GetData(idx1);
	if (!data) { return; }

	std::vector<std::pair<std::wstring, std::wstring>> prop;
	prop.push_back(std::make_pair(::GetResourceString(m_owner,"ID_SOURCE_TEXT_SEARCH_POINT"), _T("")));
	prop.push_back(MAKE_ITEM(::GetResourceString(m_owner,"ID_SOURCE_TEXT_X_COLON"), data->GetItemName(0), x));
	prop.push_back(MAKE_ITEM(::GetResourceString(m_owner,"ID_SOURCE_TEXT_Y_COLON"), data->GetItemName(1), y));

	m_cursorValue[0] = x;
	m_cursorValue[1] = y;
	m_cursorValue[2] = z1;

	if (GetDataCount() != 2) {
		prop.push_back(MAKE_ITEM(::GetResourceString(m_owner,"ID_SOURCE_TEXT_Z_COLON"),	data->GetItemName(2), z1));
	}
	else {
		TexVertex const v2 = m_cursor->GetCursorPos(1);
		IPlotData* data2 = GetData(idx2);
		double const z2 = m_zAxis->CoordinateToValue(v2.Z);

		prop.push_back(MAKE_ITEM(::GetResourceString(m_owner,"ID_SOURCE_TEXT_Z1_COLON"),	data->GetItemName(2), z1));
		prop.push_back(MAKE_ITEM(::GetResourceString(m_owner,"ID_SOURCE_TEXT_Z2_COLON"),	data2->GetItemName(2), z2));
		prop.push_back(MAKE_ITEM(::GetResourceString(m_owner,"ID_SOURCE_TEXT_DELTA_COLON"), ::GetResourceString(m_owner,"ID_SOURCE_TEXT_Z2_Z1"), z2 - z1));
	}
	m_owner->SendMessage(PI_MSG_SET_PROP_ITEMS, reinterpret_cast<WPARAM>(&prop[0]), prop.size());
}

void SolidGraph::ColorPatternChanged(std::wstring const& pattern)
{
	PlotList plots = GetPlots();
	if (m_dimension == 4 && plots.size() > 1) {
		size_t const idx = m_activePlot == 0 ? 1 : 0;
		// 4Dの場合はすべてのプロットでカラーパターンは共通
		LPARAM param = reinterpret_cast<LPARAM>(pattern.c_str());
		plots[idx]->SetProperty(PI_COLOR_PATTERN, param, nullptr);
	}
}
