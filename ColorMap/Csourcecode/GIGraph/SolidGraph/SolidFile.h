/*!	@file
*/
#pragma once

#include <boost/shared_ptr.hpp>
#include <boost/noncopyable.hpp>
#include <boost/signals2.hpp>
#include "NullableShape.h"
#include "IGroup.h"
#include "IGraphFile.h"

class PlotGroup;
class ShapeWrapper;
class Legend;
class IMessageReceiver;
class IPlotData;
class IFile;
struct IPropertyComposite;
class SolidGraph;

class SolidFile : public NullableShape, public IGroup, public IGraphFile, public boost::noncopyable
{
public:
	SolidFile(IMessageReceiver* owner, SolidGraph* graph);

	void AddPlot(PlotGroup* plot);
	boost::shared_ptr<PlotGroup> GetPlot(size_t index) const;
	std::vector<boost::shared_ptr<PlotGroup>> GetPlots() const;

	void SetDimension(int dim);
	void Modeling();
	void Render(bool clipping) const;

	void AutoNaming();

	void AttachLegend(Legend* legend) { m_legend = legend; }

// Shape
	void Destroy() { delete this; }

	void GetName(wchar_t* name, size_t length) const;
	std::wstring GetName() const { return m_name; }
	unsigned long GetTypeId() const { return 29; }
	bool IsVisible() const { return m_visible; }

	virtual bool IsEffective() const  { return !IsEmpty(); }
	bool IsDeletable() const { return true; }

	int HitTest(double x, double y) const;

	void SetSelected(bool select);
	bool GetSelected() const;
	bool IsSelected() const { return GetSelected(); }

	PropertyNode* CreateProperty();
	PropertyNode* CreateTabProperty();
	bool SetProperty(long id, LPARAM value, LPARAM* source);
	bool GetProperty(long id, LPARAM* ret) const;

	void Serialize(ISerializer* ar);

// IGroup
	virtual void Add(IShape* shape);
	virtual void Insert(IShape* shape, size_t index);
	virtual size_t Remove(IShape* shape);
	virtual size_t GetChildCount() const;
	virtual IShape* GetChild(size_t index) const;
	virtual void Clear();

	//! 自身の中にshapeがあるかどうかを返します
	virtual bool Include(IShape* shape) const;

	virtual void Select(IShape* shape, bool recursive = false);
    virtual size_t GetIndex(IShape* shape) const;

	virtual void BringToFront(IShape* child);
	virtual void SendToBack(IShape* child);
	virtual void ChangeZOrder(IShape* shape, size_t index);

// IGraphFile
	virtual IPlot* AddPlot(unsigned long /*type*/, unsigned long /*xItem*/, unsigned long /*yItem*/, boost::shared_ptr<IFilter> const& /*filter*/) {return 0;}
	virtual IPlot* Add3DPlot(unsigned long /*type*/, unsigned long /*xItem*/, unsigned long /*yItem*/, unsigned long /*zItem*/, boost::shared_ptr<IFilter> const& /*filter*/) {return 0;}
	virtual IPlot* Add4DPlot(unsigned long /*type*/, unsigned long /*xItem*/, unsigned long /*yItem*/, unsigned long /*zItem*/, unsigned long /*tItem*/,
		boost::shared_ptr<IFilter> const& /*filter*/) {return 0;}

	virtual bool IsEmpty() const;
	virtual long GetFileId() const;
	virtual void SetFileId(long id);

	virtual TCHAR const* GetImportSettingFile() const;
	virtual int GetFileType() const;

// Legend required
	COLORREF GetPlotColor() const { return m_color; }

// events
	boost::signals2::signal<void(bool)> PropertyUpdated;

private:
	IPlotData* GetData() const;
	IFile* GetSourceFile() const;
	void OnPropertyUpdated(bool all_update = false);

	void AddShowProperty(IPropertyComposite* prop);

private:
	IMessageReceiver* m_owner;
	SolidGraph* m_graph;

	std::vector<boost::shared_ptr<PlotGroup>> m_plots;
	std::vector<boost::shared_ptr<PlotGroup>> m_deletedPlots;

	std::wstring m_name;
	bool m_autoname;
	bool m_visible;
	COLORREF m_color;
	bool m_selected;

	std::wstring mutable m_settingFile;
	int mutable m_fileType;

	Legend* m_legend;
};