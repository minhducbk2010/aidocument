/*!	@file
*/
#pragma once

#include <boost/signals2.hpp>
#include <boost/noncopyable.hpp>
#include <boost/range/algorithm/find.hpp>
#include <vector>
#include "ISerializer.h"

class ContourPlot;

class CrossCursor : public boost::noncopyable
{
public:
	CrossCursor()
		: m_xIndex(-1), m_yIndex(-1)
		, m_activeIndex(0)
		, m_initialize(false)
	{
	}

	~CrossCursor()
	{
	}

	void Select(int x, int y)
	{
		m_xIndex = x;
		m_yIndex = y;
		OnCursorChanged();
	}

	void Reset() { Select(-1, -1); }

	bool IsEffective() const { return m_xIndex >= 0 && m_yIndex >= 0; }

	int GetXIndex() const { return m_xIndex; }
	int GetYIndex() const { return m_yIndex; }

	void Add(ContourPlot* plot)
	{
		// 初回はダミークラスが渡されるので無視する(BK)
		if (!m_initialize) { m_initialize = true; return; }
		m_plots.push_back(plot);
	}
	void Erase(ContourPlot* plot)
	{
		std::vector<ContourPlot*>::iterator it = boost::range::find(m_plots, plot);
		if (it != m_plots.end()) {
			m_plots.erase(it);
		}
	}
	void Activate(size_t index) { m_activeIndex = index; }
	bool IsActive(ContourPlot const* c) const { return c == GetActivePlot(); }

	std::vector<ContourPlot*> GetPlots() const {
		std::vector<ContourPlot*> plots;
		plots.push_back(m_plots[m_activeIndex]);
		if (m_plots.size() > 1) {
			plots.push_back(m_plots[m_activeIndex == 0 ? 1 : 0]);
		}
		return plots;
	}

	void Render(double const size)
	{
		if (!m_plots[m_activeIndex]->IsVisible()) { return; }

		m_plots[m_activeIndex]->RenderCursor();

		if (m_plots.size() != 2) { return; }
		if (m_xIndex < 0 || m_yIndex < 0) { return; }

		// カーソル位置に縦線を引く
		TexVertex const pts[] = { GetCursorPos(0), GetCursorPos(1) };

		if (pts[1].Z == FLT_MAX) { return; }

		size_t const idx = m_activeIndex == 0 ? 1 : 0;
		m_plots[idx]->RenderCursor(pts[0].X, pts[1].Y);

		double const min = std::min<double>(pts[0].Z, pts[1].Z);
		double const max = std::max<double>(pts[0].Z, pts[1].Z);

		::glLineWidth(2);
		::glColor3ub(0, 0, 0);
		::glBegin(GL_LINE_STRIP);
		::glVertex3d(pts[0].X, -size, pts[0].Y);
		::glVertex3d(pts[1].X, min, pts[1].Y);
		::glEnd();

		::glBegin(GL_LINE_STRIP);
		::glVertex3d(pts[0].X, size, pts[0].Y);
		::glVertex3d(pts[1].X, max, pts[1].Y);
		::glEnd();

		::glColor3ub(0, 255, 0);
		::glBegin(GL_LINE_STRIP);
		for (size_t i = 0; i < _countof(pts); ++i) {
			::glVertex3d(pts[i].X, pts[i].Z, pts[i].Y);
		}
		::glEnd();

		::glLineWidth(1);
		::glColor3ub(0, 0, 0);
	}

	TexVertex GetCursorPos(size_t const index)
	{
		if (index == 0) {
			return m_plots[m_activeIndex]->GetCursorPos(m_xIndex, m_yIndex);
		}

		size_t const idx = m_activeIndex == 0 ? 1 : 0;
		TexVertex const v1 = m_plots[m_activeIndex]->GetCursorPos(m_xIndex, m_yIndex);
		double const z = m_plots[idx]->InterpolateValue(v1.X, v1.Y);
		return TexVertex(v1.S, v1.T, v1.X, v1.Y, z);
	}
	
	void Serialize(ISerializer* ar)
	{
		ar->Serialize(_T("XIndex"), m_xIndex);
		ar->Serialize(_T("YIndex"), m_yIndex);
		if (IsReadable(ar)) {
			OnCursorChanged();
		}
	}

// Events
	boost::signals2::signal<void()> CursorChanged;

private:
	void OnCursorChanged()
	{
		CursorChanged();
	}

	ContourPlot* GetActivePlot() const { return m_plots[m_activeIndex]; }

private:
	int m_xIndex;	//!< Cursor-X
	int m_yIndex;	//!< Cursor-Y

	std::vector<ContourPlot*> m_plots;
	size_t m_activeIndex;

	bool m_initialize;
};