#pragma once

#include "SolidUnitBase.hpp"

class CurvePlot : public SolidUnitBase
{
public:
	CurvePlot();
	virtual ~CurvePlot();

private:
	virtual void RenderImpl(ColorTexture* texture, TexVertex const* vertexes, size_t length, bool clipping) const;

private:
	GLUnurbs* m_nurbs;
};
