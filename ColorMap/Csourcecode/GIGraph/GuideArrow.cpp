#include "stdafx.h"
#include "GuideArrow.h"
#include "TextRenderer.h"

GLUquadric *GuideArrow::quad = NULL;
int GuideArrow::reference = 0;

GuideArrow::GuideArrow(double fromX, double fromY, double fromZ, double toX,
	double toY, double toZ, double r, double g, double b)
{
	if (quad == NULL)
	{
		quad = gluNewQuadric();

		gluQuadricDrawStyle(quad, GLU_FILL);
		gluQuadricNormals(quad, GLU_FLAT);
	}

	++reference;

	this->fX = fromX;
	this->fY = fromY;
	this->fZ = fromZ;
	this->tX = toX;
	this->tY = toY;
	this->tZ = toZ;
	this->r = r;
	this->g = g;
	this->b = b;
}
GuideArrow::~GuideArrow()
{
	--reference;

	if (reference == 0)
	{
		gluDeleteQuadric(quad);
		quad = 0;
	}
}

void GuideArrow::Modeling(GLdouble angle, GLdouble x, GLdouble y, GLdouble z)
{
	this->angle = angle;
	this->rX = x;
	this->rY = y;
	this->rZ = z;
}
void GuideArrow::Render(const wchar_t* name)
{
	::glColor3d(r, g, b);
	::glBegin(GL_LINES);
	{
		::glVertex3d(fX, fY, fZ);
		::glVertex3d(tX, tY, tZ);
	}
	::glEnd();

	::glPushMatrix();
	{
		::glTranslated(tX, tY, tZ);
		::glRotated(angle, rX, rY, rZ);

		GLdouble const radius = 1.8;
		::gluCylinder(quad, radius, 0.0, 5, 5, 5);
		::glEnable(GL_CULL_FACE);
		::glCullFace(GL_FRONT);
		::gluDisk(quad, 0.0, radius, 5, 1);
		::glCullFace(GL_BACK);

		double wx, wy;
		::WorldToWindow(0, 0, 7, &wx, &wy);
		wx -= 2;
		wy -= 5;
		if (::wcscmp(name, _T("Z")) == 0) {
			wy -= 4;
		}

		LOGFONT lf = {};
		lf.lfHeight = ::PointToDeviceUnit(8);
		::_tcscpy_s(lf.lfFaceName, _T("Arial"));

		COLORREF const color = RGB(r * 255, g * 255, b * 255);
		TextRenderer::GetInstance()->AddText(name, (int)wx, (int)wy, 0, color, TextAlign::Center, lf);

		::glDisable(GL_CULL_FACE);
	}
	::glPopMatrix();
}
