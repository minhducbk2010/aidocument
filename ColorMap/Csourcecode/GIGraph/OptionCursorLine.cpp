#include "stdafx.h"
#include "OptionCursorLine.h"
#include "Graph3DBase.h"
#include "GLExtension.h"
#include "GLBuffer.h"
#include "TexVertex.h"

OptionCursorLine::OptionCursorLine(Graph3DBase *graphBase)
{
	this->graphBase = graphBase;

	vertex = NULL;
	vertexCount = 0;
	view = false;
	r = 0.6F;
	g = 0.0F;
	b = 0.0F;
}
OptionCursorLine::~OptionCursorLine()
{
	delete vertex;
}

void OptionCursorLine::SetLineColor(double r, double g, double b)
{
	this->r = r;
	this->g = g;
	this->b = b;
}
void OptionCursorLine::Modeling(double *x, double *y, double *z, int count)
{
	if (count > 0)
	{
		if (vertexCount != count)
		{
			delete vertex;

			vertexCount = count;
			Vertex *vertexes = new Vertex[vertexCount];

			double fx, fy, fz;

			for (int i = 0; i < vertexCount; ++i)
			{
				fx = x[i];
				fy = y[i];
				fz = z[i];

				graphBase->ConvertValueToPosition(fx, fy, fz);

				vertexes[i] = Vertex(fx, fy, fz);
			}

			vertex = new GLBuffer(GL_ARRAY_BUFFER_ARB, sizeof(Vertex) * vertexCount,
				(const GLvoid*)vertexes, GL_DYNAMIC_DRAW_ARB);

			delete []vertexes;
		}
		else
		{
			Vertex *vertexes = (Vertex*)vertex->Map(GL_WRITE_ONLY_ARB);

			double fx, fy, fz;

			for (int i = 0; i < vertexCount; ++i)
			{
				fx = x[i];
				fy = y[i];
				fz = z[i];

				graphBase->ConvertValueToPosition(fx, fy, fz);

				vertexes[i] = Vertex(fx, fy, fz);
			}

			vertex->Unmap();
		}

		view = true;
	}
	else
	{
		view = false;
	}
}
void OptionCursorLine::Render()
{
	if (view && vertex != NULL)
	{
		glDepthFunc(GL_ALWAYS);

		glColor3f(0.6F, 0.0F, 0.0F);

		glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer(3, GL_FLOAT, 0, (const GLvoid*)vertex->Bind());
		glDrawArrays(GL_LINE_STRIP, 0, vertexCount);

		glDepthFunc(GL_LESS);
	}
}
