/*!	@file
*/
#pragma once

#include "RenderState.h"

struct PreparedObject
{
	PreparedObject(Graph3DBase const* graphBase)
		: m_graphBase(graphBase)
		, m_state(graphBase->m_pitch, graphBase->m_yaw, graphBase->m_roll, graphBase->m_h, graphBase->m_v, graphBase->m_scale, graphBase->m_viewColorBar)
	{
		gluLookAt(0.0, 0.0, 0.0, 0.0, 0.0, -100.0, 0.0, 1.0, 0.0);

		m_state.Bind();
	}

	~PreparedObject()
	{
		m_state.Unbind();
	}

private:
	Graph3DBase const* m_graphBase;
	RenderState m_state;
};

boost::shared_ptr<PreparedObject> Graph3DBase::CreatePrepateObject() const
{
	return boost::shared_ptr<PreparedObject>(new PreparedObject(this));
}
