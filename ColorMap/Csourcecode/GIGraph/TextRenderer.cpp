#include "stdafx.h"
#include <boost/lexical_cast.hpp>
#include <boost/foreach.hpp>
#include "StringUtil.hpp"
#include "TextRenderer.h"
#include "TextInfo.h"
#include "SIUnit.h"

const GLsizei CharCount = 128;

TextRenderer::TextRenderer()
	: m_enable(true)
{
	listCount = 0;
	useSiUnit = FALSE;
}
TextRenderer::~TextRenderer()
{
}

TextRenderer* TextRenderer::GetInstance()
{
	static TextRenderer instance;
	return &instance;
}

void TextRenderer::Dispose()
{
	if (listCount != 0)
	{
		glDeleteLists(listBase, listCount);
		listCount = 0;
	}
}

BOOL TextRenderer::GetUseSIUnit()
{
	return useSiUnit;
}
void TextRenderer::SetUseSIUnit(BOOL value)
{
	useSiUnit = value;
}

void TextRenderer::SetFont(HDC device, const wchar_t *name, int height)
{
	this->Dispose();

	this->device = device;

	memset(&logFont, 0, sizeof(LOGFONTW));
	logFont.lfHeight = height;
	wcscpy_s(logFont.lfFaceName, _countof(logFont.lfFaceName), name);

	HFONT font = CreateFontIndirect(&logFont);
	HFONT oldFont = (HFONT)SelectObject(device, font);

	listCount = CharCount;
	listBase = glGenLists(listCount);
	wglUseFontBitmaps(device, 0, listCount, listBase);

	SelectObject(device, oldFont);
	DeleteObject(font);
}

void TextRenderer::Render(Vertex pos, const wchar_t *text, const SIZE *size,
						  TextRenderAlign align)
{
	SIZE sz;

	if (size == NULL) {
		int count = (int)wcslen(text);
		GetTextExtentPoint32W(device, text, count, &sz);
	}
	else {
		sz = *size;
	}

	glRasterPos3d(pos.X, pos.Y, pos.Z);

	switch(align)
	{
	case TraRight:
		glBitmap(0, 0, 0.0F, 0.0F, (GLfloat)-sz.cx, 0.0F, NULL);
		break;
	case TraCenter:
		glBitmap(0, 0, 0.0F, 0.0F, (GLfloat)(-sz.cx / 2), 0.0F, NULL);
		break;
	}

	glListBase(listBase);
	glCallLists((GLsizei)wcslen(text), GL_UNSIGNED_SHORT, text);
}
void TextRenderer::Render(Vertex pos, double value, const SIZE *size,
						  TextRenderAlign align)
{
	wchar_t buf[64] = {};

	if (useSiUnit) {
		SIUnit::Convert(value, buf, _countof(buf));
	}
	else {
		_snwprintf_s(buf, _countof(buf), _TRUNCATE, L"%g", value);
	}
	
	this->Render(pos, buf, size, align);
}
TextInfo *TextRenderer::CreateText(HDC device, const wchar_t *text)
{
	HFONT font = CreateFontIndirect(&logFont);
	HFONT oldFont = (HFONT)SelectObject(device, font);

	GLsizei count = (GLsizei)wcslen(text);
	GLuint base = glGenLists(count);
	for (int i = 0; i < count; ++i)
	{
		wglUseFontBitmapsW(device, text[i], 1, base + i);
	}

	SIZE size;
	GetTextExtentPoint32(device, text, count, &size);

	SelectObject(device, oldFont);
	DeleteObject(font);

	return new TextInfo(base, count, size);
}
SIZE TextRenderer::GetValueTextSize(HDC device, double value, LOGFONT const& lf) const
{
	wchar_t buf[64] = {};
	if (useSiUnit) {
		SIUnit::Convert(value, buf, _countof(buf));
	}
	else {
		_snwprintf_s(buf, _countof(buf), _TRUNCATE, L"%g", value);
	}
	return GetTextSize(device, buf, lf);
}

SIZE TextRenderer::GetTextSize(TCHAR const* text) const
{
	LOGFONT lf = {};
	lf.lfHeight = ::PointToDeviceUnit(9);
	::_tcscpy_s(lf.lfFaceName, _T("�l�r �o�S�V�b�N"));
	return GetTextSize(device ,text, lf);
}

SIZE TextRenderer::GetTextSize(HDC device, TCHAR const* name, LOGFONT const& lf) const
{
	HFONT font = ::CreateFontIndirect(&lf);
	HFONT oldFont = (HFONT)::SelectObject(device, font);

	size_t count = wcslen(name);

	SIZE size;
	::GetTextExtentPoint32(device, name, (int)count, &size);

	::SelectObject(device, oldFont);
	::DeleteObject(font);

	return size;
}

void TextRenderer::AddText(std::wstring const& text, int x, int y, double angle, COLORREF color, TextAlign::type align)
{
	LOGFONT lf = {};
	lf.lfHeight = ::PointToDeviceUnit(9);
	::_tcscpy_s(lf.lfFaceName, _T("�l�r �o�S�V�b�N"));
	AddText(text, x, y, angle, color, align, lf);
}

void TextRenderer::AddText(std::wstring const& text, int x, int y, double angle, COLORREF color, TextAlign::type align, LOGFONT const& lf, bool vcenter)
{
	if (m_enable && !text.empty()) {
		m_cache.push_back(DrawableText(text, Gdiplus::REAL(x), Gdiplus::REAL(y), angle, lf, color, align));
		m_cache.back().verticalCenter = vcenter;
	}
}

void TextRenderer::AddText(double value, double x, double y, double angle, COLORREF color, TextAlign::type align)
{
	LOGFONT lf = {};
	lf.lfHeight = ::PointToDeviceUnit(8);
	::_tcscpy_s(lf.lfFaceName, _T("Arial"));
	AddText(value, x, y, angle, color, align, lf);
}

void TextRenderer::AddText(double value, double x, double y, double angle, COLORREF color, TextAlign::type align, LOGFONT const& lf)
{
	AddText(value, x, y, 0, angle, color, align, lf, StringFormat::Digit, false);
}

void TextRenderer::AddText(double value, double x, double y, double z, double angle,
						   COLORREF color, TextAlign::type align, LOGFONT const& lf,
						   StringFormat fmt, bool useComma)
{
	std::wstring const buf = DblToStr(value, fmt, -1);
	std::wstring text = useComma && fmt == StringFormat::Digit ? SetComma(buf) : buf;

	double wx, wy;
	WorldToWindow(x, y, z, &wx, &wy);
	AddText(text.c_str(), (int)wx, (int)wy, angle, color, align, lf);
	m_cache.back().verticalCenter = true;
}

void TextRenderer::AddTextFormat(double value, double x, double y, double z,
								 COLORREF color, TextAlign::type align, LOGFONT const& lf,
								 int figure, StringFormat fmt, bool useComma,
								 SIUnitLabel si, ScaleType scale, bool vcenter)
{
	if (figure < 0) { figure = -1; }

	std::wstring buf;
	if(si != SIUnitLabel::OFF && scale == ST_LINEAR)
	{
		buf = ConvertSIUnitString(value, si, fmt, figure);
	}
	else
	{
		buf = DblToStr(value, fmt, figure);
	}
	std::wstring const text = useComma && fmt == StringFormat::Digit ? SetComma(buf) : buf;

	double wx, wy;
	WorldToWindow(x, y, z, &wx, &wy);
	AddText(text.c_str(), (int)wx, (int)wy, 0/*angle*/, color, align, lf);
	m_cache.back().verticalCenter = vcenter;
}

void TextRenderer::LatestTextDoVerticalCenter()
{
	m_cache.back().verticalCenter = true;
}

void TextRenderer::DrawGDIText(HDC dc)
{
	Gdiplus::Graphics g(dc);

	BOOST_FOREACH(DrawableText& c, m_cache) {
		Gdiplus::SolidBrush br(Gdiplus::Color(GetRValue(c.color), GetGValue(c.color), GetBValue(c.color)));
		Gdiplus::Font font(dc, &c.lf);

		g.SetTextRenderingHint(std::abs(c.lf.lfHeight) > 9  ? Gdiplus::TextRenderingHintAntiAlias : Gdiplus::TextRenderingHintSystemDefault);

		Gdiplus::REAL x = c.x;
		Gdiplus::REAL y = c.y;

		Gdiplus::RectF r;
		Gdiplus::Region rgn;
		Gdiplus::StringFormat fmt(Gdiplus::StringFormatFlagsNoClip | Gdiplus::StringFormatFlagsNoFitBlackBox | Gdiplus::StringFormatFlagsMeasureTrailingSpaces);
		{
			Gdiplus::CharacterRange range(0, static_cast<int>(c.text.size()));
			fmt.SetMeasurableCharacterRanges(1, &range);
		}
		g.MeasureCharacterRanges(c.text.c_str(), static_cast<int>(c.text.size()), &font, Gdiplus::RectF(0, 0, FLT_MAX, FLT_MAX), &fmt, 1, &rgn);
		rgn.GetBounds(&r, &g);

		Gdiplus::SizeF size;
		r.GetSize(&size);

		if (c.angle) {
			Gdiplus::Matrix mx;
			g.GetTransform(&mx);
			mx.RotateAt((Gdiplus::REAL)c.angle, Gdiplus::PointF(c.x, c.y + size.Height / 2.0f));
			g.SetTransform(&mx);
		}

		if (c.align == TextAlign::Center) {
			x -= size.Width / 2;
		}
		else if (c.align == TextAlign::Right) {
			x -= size.Width;
		}

		if (c.verticalCenter) {
			y -= size.Height / 2;
		}

		g.DrawString(c.text.c_str(), static_cast<int>(c.text.length()), &font, Gdiplus::PointF(x, y), &fmt, &br);

		g.ResetTransform();
	}

	m_cache.clear();
}