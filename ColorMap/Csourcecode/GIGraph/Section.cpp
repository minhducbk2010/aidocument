/*!	@file
	コンターマップの断面
*/
#include "StdAfx.h"
#include "utility.h"
#include "Section.h"
#include "Axis.h"

int NormalizeXPosition(WTL::CRect const& rect, double value, double const xMax = 10, double const xMin = -10)
{
	double const xPer = (value - xMin) / (xMax - xMin);
	return static_cast<int>(rect.left + rect.Width() * xPer);
}

int NormalizeYPosition(WTL::CRect const& rect, double value, double const yMax = 10, double const yMin = -10)
{
	double const yPer = (value - yMin) / (yMax - yMin);
	return static_cast<int>(rect.Height() - rect.Height() * yPer + rect.top);
}

//! Constructor
Section::Section()
	: m_value(FLT_MAX)
	, m_xAxis(0), m_yAxis(0), m_exAxis(0)
{
}

//! Destructor
Section::~Section()
{
}

//! グラフで使用している軸を関連付けます
void Section::AttachAxis(Axis* xAxis, Axis* yAxis)
{
	m_xAxis = xAxis;
	m_yAxis = yAxis;
}

//! 断面の抽出に使用している軸を設定します
void Section::SetExtractAxis(Axis* axis)
{
	m_exAxis = axis;
}

void Section::Clear()
{
	m_lines.clear();
}

//! 描画する断面のデータを設定します
void Section::SetData(std::vector<OCPoint> const& data, double value)
{
	Clear();
	m_value = value;
	m_lines.push_back(data);
}

void Section::SetCursorValue(double value)
{
	m_value = value;
}

void Section::AddData(std::vector<OCPoint> const& data)
{
	m_lines.push_back(data);
}

int Section::GetXPosition(WTL::CRect const& rect, double value) const
{
	return NormalizeXPosition(rect, value, m_xAxis->GetMaximum(), m_xAxis->GetMinimum());
}

int Section::GetYPosition(WTL::CRect const& rect, double value) const
{
	return NormalizeYPosition(rect, value, m_yAxis->GetMaximum(), m_yAxis->GetMinimum());
}

//! 断面図を描画します
void Section::Draw(HDC handle, OCRect const& pxRect) const
{
	WTL::CRect rect(static_cast<int>(pxRect.left), static_cast<int>(pxRect.top), static_cast<int>(pxRect.right), static_cast<int>(pxRect.bottom));
	AdjustRect(&rect);

	WTL::CDCHandle dc;
	dc.Attach(handle);

	WTL::CFont font;
	LOGFONT lf = {};
	if (dynamic_cast<OCHorizontalSection const*>(this)) {
		lf = m_yAxis->GetTickLabelFont();
	}
	else {
		lf = m_xAxis->GetTickLabelFont();
	}
	font.CreateFontIndirect(&lf);
	HFONT oldFont = dc.SelectFont(font);

	{ // 背景と軸を描画
		WTL::CPen pen;
		pen.CreatePen(PS_SOLID, 1, RGB(192, 192, 192));
		HPEN oldPen = dc.SelectPen(pen);

		DrawBackground(dc, rect);

		dc.SelectPen(oldPen);
	}

	// プロットを描画
	DrawPlot(dc, rect);

	dc.SelectFont(oldFont);
	dc.Detach();
}

//! 断面図に表示する軸の最大最小値を取得します
void Section::GetAxisLinearMinMax(Axis const* axis, double* min, double* max) const
{
	// ログの時はスケール変換しない
	if(axis->GetScaleType() == ST_LOG)
	{
		*min = axis->GetLinearMinimum();
		*max = axis->GetLinearMaximum();
	}
	else
	{
		*min = axis->GetMinimum();
		*max = axis->GetMaximum();
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////

const int TICK_SIZE = 40;
const int AXIS_SIZE = TICK_SIZE + 15;

struct Fit {
	enum Type
	{
		None	= 0,
		Left	= 1 << 0,
		Right	= 1 << 1,
		XCenter	= 1 << 2,
		Top		= 1 << 3,
		Bottom	= 1 << 4,
		YCenter	= 1 << 5,
	};
};

//! テキストを描画します
void DrawText(WTL::CDCHandle& dc, TCHAR const* text, int x, int y, COLORREF color, int angle = 0)
{
	Gdiplus::Font font(dc.m_hDC);
	Gdiplus::SolidBrush brush(Gdiplus::Color(GetRValue(color), GetGValue(color), GetBValue(color)));

	Gdiplus::Graphics g(dc.m_hDC);
	g.SetTextRenderingHint(Gdiplus::TextRenderingHintAntiAlias);

	Gdiplus::GraphicsState state = g.Save();
	if (angle) {
		Gdiplus::PointF center((Gdiplus::REAL)x, (Gdiplus::REAL)y);

		Gdiplus::Matrix mx;
		g.GetTransform(&mx);
		mx.RotateAt((Gdiplus::REAL)angle, center);
		g.SetTransform(&mx);
	}

	Gdiplus::PointF point((Gdiplus::REAL)x, (Gdiplus::REAL)y);
	g.DrawString(text, -1, &font, point, &brush);
	
	g.Restore(state);
}

//! 実数を文字列に変換し、指定の座標に描画します
void DrawValue(WTL::CDCHandle& dc, int x, int y, double value, Axis* axis, Fit::Type type = Fit::None)
{
	TCHAR buf[64] = {};
	int scaleFig = axis->GetScaleFigure();
	if(axis->GetSIUnit() != OFF && axis->GetScaleType() == ST_LINEAR){
		std::wstring str 
			= ConvertSIUnitString(value, axis->GetSIUnit(), axis->GetTickFormat(), scaleFig);

		lstrcpy(buf, str.c_str());
	}
	else{
		if (scaleFig == -1) {
			::_stprintf_s(buf, _T("%g"), value);
		}
		else {
			TCHAR fmt[20] = {};
			::_stprintf_s(fmt, _T("%%.%df"), scaleFig);
			::_stprintf_s(buf, fmt, value);
		}
	}

	WTL::CSize size;
	if (dc.GetTextExtent(buf, -1, &size)) {
		if (type & Fit::Right) {
			x -= size.cx;
		}
		else if (type & Fit::XCenter) {
			x -= size.cx / 2;
		}

		if (type & Fit::Bottom) {
			y -= size.cy;
		}
		else if (type & Fit::YCenter) {
			y -= size.cy / 2;
		}

		DrawText(dc, buf, x, y, axis->GetSymbolColor());
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// OCVerticalSection

void OCVerticalSection::AdjustRect(WTL::CRect* rect) const
{
	rect->top += 20;
	rect->right -= AXIS_SIZE;
	rect->bottom -= 40;
}

void OCVerticalSection::DrawBackground(WTL::CDCHandle& dc, WTL::CRect const& rect) const
{
	double const xMax = m_xAxis->GetMaximum(), xMin = m_xAxis->GetMinimum();
	double const yMax = m_yAxis->GetMaximum(), yMin = m_yAxis->GetMinimum();

	double max, min;

	{
		dc.FillRect(rect, WHITE_BRUSH);
		dc.Rectangle(rect);
	}

	// 軸, グリッド
	{ // X
		{
			WTL::CFont font;
			LOGFONT lf = m_xAxis->GetLabelFont();

			font.CreateFontIndirect(&lf);
			HFONT oldFont = dc.SelectFont(font);
			dc.SetTextColor(m_xAxis->GetFontColor());

			std::wstring name = m_xAxis->GetLabel();
			SIZE size;
			if (dc.GetTextExtent(name.c_str(), -1, &size)) {
				WTL::CPoint pt(rect.left + rect.Width() / 2 - size.cx / 2, rect.bottom + size.cy + 2);
				//dc.TextOut(pt.x, pt.y, name);
				DrawText(dc, name.c_str(), pt.x, pt.y, m_xAxis->GetFontColor());
			}

			dc.SelectFont(oldFont);
		}

		COLORREF old = dc.SetTextColor(m_xAxis->GetSymbolColor());
		for (size_t i = 1; ; ++i) {
			double v = xMin + m_xAxis->GetInterval() * i;
			if (v >= xMax || IsZero(v - xMax)) { break; }

			int x = GetXPosition(rect, v);
			dc.MoveTo(x, rect.top);
			dc.LineTo(x, rect.bottom);
		}

		GetAxisLinearMinMax(GetXAxis(), &min, &max);
		DrawValue(dc, GetXPosition(rect, xMin), rect.bottom, min, m_xAxis, Fit::XCenter);
		DrawValue(dc, GetXPosition(rect, xMax), rect.bottom, max, m_xAxis, Fit::XCenter);
		dc.SetTextColor(old);
	}
	{ // Y
		{
			WTL::CFont font;
			LOGFONT lf = m_yAxis->GetLabelFont();
			lf.lfOrientation = -90 * 10;
			lf.lfEscapement = -90 * 10;

			font.CreateFontIndirect(&lf);
			HFONT oldFont = dc.SelectFont(font);
			dc.SetTextColor(m_yAxis->GetFontColor());

			std::wstring name = m_yAxis->GetLabel();
			SIZE size;
			if (dc.GetTextExtent(name.c_str(), -1, &size)) {
				WTL::CPoint pt(rect.right + TICK_SIZE, rect.top + rect.Height() / 2 - size.cx / 2);
				//dc.TextOut(pt.x, pt.y, name);
				DrawText(dc, name.c_str(), pt.x, pt.y, m_yAxis->GetFontColor(), 90);
			}

			dc.SelectFont(oldFont);
		}

		COLORREF old = dc.SetTextColor(m_yAxis->GetSymbolColor());
		int const tickLeft = rect.right + 1;
		for (size_t i = 1; ; ++i) {
			double v = yMin + m_yAxis->GetInterval() * i;
			if (v >= yMax || IsZero(v - yMax)) { break; }

			int y = GetYPosition(rect, v);
			dc.MoveTo(rect.left, y);
			dc.LineTo(rect.right, y);
		}

		GetAxisLinearMinMax(GetYAxis(), &min, &max);
		DrawValue(dc, tickLeft, GetYPosition(rect, yMin), min, m_yAxis, Fit::Bottom);
		DrawValue(dc, tickLeft, GetYPosition(rect, yMax), max, m_yAxis, Fit::Top);
		dc.SetTextColor(old);
	}
}

void OCVerticalSection::DrawPlot(WTL::CDCHandle& dc, WTL::CRect const& rect) const
{
	// プロット(断面)

	bool first = true;
	BOOST_FOREACH(std::vector<OCPoint> const& line, m_lines) {
		WTL::CPen pen;
		if (first) {
			pen.CreatePen(PS_SOLID, 1, RGB(255, 0, 0));
			first = false;
		}
		else {
			pen.CreatePen(PS_DOT, 1, RGB(255, 0, 0));
		}
		HPEN oldPen = dc.SelectPen(pen);

		std::vector<POINT> pts;
		for (size_t i = 0; i < line.size(); ++i) {
			int x = static_cast<int>(rect.left + rect.Width() * m_xAxis->GetNormalize(line[i].x));
			int y = static_cast<int>(rect.Height() - rect.Height() * m_yAxis->GetNormalize(line[i].y) + rect.top);

			POINT pt = {x, y};
			pts.push_back(pt);
		}

		if (pts.size() > 1) {
			WTL::CRgn region;
			region.CreateRectRgn(rect.left, rect.top, rect.right, rect.bottom);
			dc.SelectClipRgn(region);
			dc.Polyline(&pts[0], static_cast<int>(pts.size()));
			dc.SelectClipRgn(0);
		}

		dc.SelectPen(oldPen);
	}

	if (m_value != FLT_MAX) {
		TCHAR buf[64] = {};
		::_stprintf_s(buf, _T("%s : %g"), m_exAxis->GetName(), m_value);
		DrawText(dc, buf, rect.left, rect.top - 20, 0);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// OCHorizontalSection

void OCHorizontalSection::AdjustRect(WTL::CRect* rect) const
{
	rect->left += 40;
}

void OCHorizontalSection::DrawBackground(WTL::CDCHandle& dc, WTL::CRect const& all) const
{
	double const xMax = m_xAxis->GetMaximum(), xMin = m_xAxis->GetMinimum();
	double const yMax = m_yAxis->GetMaximum(), yMin = m_yAxis->GetMinimum();

	double max, min;

	{
		WTL::CRect rect = all;
		rect.bottom -= AXIS_SIZE;
		dc.FillRect(rect, WHITE_BRUSH);
		dc.Rectangle(rect);
	}

	// 軸, グリッド
	{ // X
		WTL::CRect rect = all;
		rect.bottom -= TICK_SIZE;

		{
			WTL::CFont font;
			LOGFONT lf = m_xAxis->GetLabelFont();

			font.CreateFontIndirect(&lf);
			HFONT oldFont = dc.SelectFont(font);
			dc.SetTextColor(m_xAxis->GetFontColor());

			std::wstring name = m_xAxis->GetLabel();
			SIZE size;
			if (dc.GetTextExtent(name.c_str(), -1, &size)) {
				DrawText(dc, name.c_str(), rect.left + rect.Width() / 2 - size.cx / 2, rect.bottom, m_xAxis->GetFontColor());
			}

			dc.SelectFont(oldFont);
		}

		rect.bottom = all.bottom - AXIS_SIZE;

		COLORREF old = dc.SetTextColor(m_xAxis->GetSymbolColor());
		for (size_t i = 1; ; ++i) {
			double v = xMin + m_xAxis->GetInterval() * i;
			if (v >= xMax || IsZero(v - xMax)) { break; }

			int x = GetXPosition(rect, v);
			dc.MoveTo(x, rect.top);
			dc.LineTo(x, rect.bottom);
		}

		GetAxisLinearMinMax(GetXAxis(), &min, &max);
		DrawValue(dc, GetXPosition(rect, xMin), rect.bottom, min, m_xAxis, Fit::Left);
		DrawValue(dc, GetXPosition(rect, xMax), rect.bottom, max, m_xAxis, Fit::Right);
		dc.SetTextColor(old);
	}
	{ // Y
		WTL::CRect rect = all;
		
		rect.bottom -= AXIS_SIZE;

		{
			WTL::CFont font;
			LOGFONT lf = m_yAxis->GetLabelFont();
			lf.lfOrientation = 90 * 10;
			lf.lfEscapement = 90 * 10;

			font.CreateFontIndirect(&lf);
			HFONT oldFont = dc.SelectFont(font);
			dc.SetTextColor(m_yAxis->GetFontColor());

			std::wstring name = m_yAxis->GetLabel();
			SIZE size;
			if (dc.GetTextExtent(name.c_str(), -1, &size)) {
				WTL::CPoint pt(rect.left - TICK_SIZE, rect.top + rect.Height() / 2 + size.cx / 2);
				DrawText(dc, name.c_str(), pt.x, pt.y, m_yAxis->GetFontColor(), -90);
			}

			dc.SelectFont(oldFont);
		}

		COLORREF old = dc.SetTextColor(m_yAxis->GetSymbolColor());
		for (size_t i = 1; ; ++i) {
			double v = yMin + m_yAxis->GetInterval() * i;
			if (v > yMax || IsZero(v - yMax)) { break; }

			int y = GetYPosition(rect, v);
			dc.MoveTo(rect.left, y);
			dc.LineTo(rect.right, y);
		}
		
		GetAxisLinearMinMax(GetYAxis(), &min, &max);
		DrawValue(dc, rect.left, GetYPosition(rect, yMin), min, m_yAxis, (Fit::Type)(Fit::Right | Fit::YCenter));
		DrawValue(dc, rect.left, GetYPosition(rect, yMax), max, m_yAxis, (Fit::Type)(Fit::Right | Fit::YCenter));
		dc.SetTextColor(old);
	}
}

void OCHorizontalSection::DrawPlot(WTL::CDCHandle& dc, WTL::CRect const& all) const
{
	WTL::CRect rect = all;
	rect.bottom -= AXIS_SIZE;

	// カーソル値
	if (m_value != FLT_MAX) {
		WTL::CRect cursor = rect;
		cursor.OffsetRect(0, -15);

		TCHAR buf[64] = {};
		::_stprintf_s(buf, _T("%s : %g"), m_exAxis->GetName(), m_value);

		WTL::CSize size;
		dc.GetTextExtent(buf, static_cast<int>(::_tcslen(buf)), &size);

		DrawText(dc, buf, cursor.right - size.cx, cursor.top, 0);
	}

	bool first = true;
	BOOST_FOREACH(std::vector<OCPoint> const& line, m_lines) {
		// プロット(断面)
		WTL::CPen pen;
		if (first) {
			pen.CreatePen(PS_SOLID, 1, RGB(0, 0, 255));
			first = false;
		}
		else {
			pen.CreatePen(PS_DOT, 1, RGB(0, 0, 255));
		}
		HPEN oldPen = dc.SelectPen(pen);

		std::vector<POINT> pts;
		for (size_t i = 0; i < line.size(); ++i) {
			int x = static_cast<int>(rect.left + rect.Width() * m_xAxis->GetNormalize(line[i].x));
			int y = static_cast<int>(rect.Height() - rect.Height() * m_yAxis->GetNormalize(line[i].y) + rect.top);

			POINT pt = {x, y};
			pts.push_back(pt);
		}

		if (pts.size() > 1) {
			WTL::CRgn region;
			region.CreateRectRgn(rect.left, rect.top, rect.right, rect.bottom);
			dc.SelectClipRgn(region);
			dc.Polyline(&pts[0], static_cast<int>(pts.size()));
			dc.SelectClipRgn(0);
		}

		dc.SelectPen(oldPen);
	}
}
