/*!	@file
*/
#pragma once

#include "stdafx.h"
#include "IMessageReceiver.h"
#include "Message.h"

class DummyReceiver : public IMessageReceiver
{
public:
	DummyReceiver(HWND owner) : m_owner(owner)
	{
	}

	virtual LRESULT SendMessage(unsigned int id, WPARAM wParam, LPARAM /*lParam*/)
	{
		switch (id)
		{
		case PI_MSG_GET_VIEW_HANDLE:
			return reinterpret_cast<LRESULT>(m_owner);
		case PI_MSG_IS_PROFESSIONAL:
			return TRUE;
		case PI_MSG_GET_RESOURCE_STRING:
			return ::SendMessage(m_owner, PI_MSG_GET_RESOURCE_STRING, wParam, 0);
		}
		return 0;
	}

private:
	HWND m_owner;
	HWND m_mainHwnd;
};

