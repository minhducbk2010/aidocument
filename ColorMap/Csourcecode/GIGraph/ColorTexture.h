#pragma once

//#include "ColorMarker.h"
#include "PlotLib/ColorPattern.h"

class ColorTexture : public OCColorPattern
{
public:
	ColorTexture(std::wstring const& userDirectory, IMessageReceiver* owner);
	virtual ~ColorTexture();

	virtual void SetMinMax(double min, double max);

	void Initialize();
	void Bind();
	void Unbind();
	void Reset();
	void Destroy();

	bool IsEnabled() const { return m_binding; }

protected:
	virtual void UpdatePattern();

private:
	ColorTexture(ColorTexture const&);

private:
	GLuint texture;
	bool m_binding;
};
