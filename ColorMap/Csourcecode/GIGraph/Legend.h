/*!	@file
	@brief	凡例
*/
#pragma once

#include <algorithm>
#include <map>
#include <boost/format.hpp>
#include <boost/foreach.hpp>
#include <boost/noncopyable.hpp>
#include "NullableShape.h"
#include "PropertyCreator.hpp"
#include "PropertyHelper.hpp"
#include "IMessageReceiver.h"
#include "Message.h"
#include "IGroup.h"
#include "ISerializer.h"

namespace {
void SetPenStyle(Gdiplus::Pen* pen, long style)
{
	switch(style)
	{
	case PS_SOLID:
		pen->SetDashStyle(static_cast<Gdiplus::DashStyle>((int)style));
		break;
	case PS_DASH:
		{
			pen->SetDashStyle(Gdiplus::DashStyleCustom);
			Gdiplus::REAL ptn[] = {17.0f, 7.0f};
			pen->SetDashPattern(ptn, 2);
		}
		break;
	case PS_DOT:
		{
			pen->SetDashStyle(Gdiplus::DashStyleCustom);
			Gdiplus::REAL ptn[] = {2.0f, 2.0f};
			pen->SetDashPattern(ptn, 2);
		}
		break;
	case PS_DASHDOT:
		{
			pen->SetDashStyle(Gdiplus::DashStyleCustom);
			Gdiplus::REAL ptn[] = {2.0f, 7.0f, 8.0f, 7.0f};
			pen->SetDashPattern(ptn, 4);
		}
		break;
	case PS_DASHDOTDOT:
		{
			pen->SetDashStyle(Gdiplus::DashStyleCustom);
			Gdiplus::REAL ptn[] = {2.0f, 4.0f, 2.0f, 4.0f, 8.0f, 4.0f};
			pen->SetDashPattern(ptn, 6);
		}
		break;
	}
}

void SetLineRoundType(Gdiplus::Pen* pen, int type)
{
	switch (type)
	{
	case RT_RECTANGLE:
		pen->SetLineJoin(Gdiplus::LineJoinMiter);
		pen->SetDashCap(Gdiplus::DashCapFlat);
		pen->SetStartCap(Gdiplus::LineCapFlat);
		pen->SetEndCap(Gdiplus::LineCapFlat);
		break;
	case RT_ELLIPSE:
		pen->SetLineJoin(Gdiplus::LineJoinRound);
		pen->SetDashCap(Gdiplus::DashCapRound);
		pen->SetStartCap(Gdiplus::LineCapRound);
		pen->SetEndCap(Gdiplus::LineCapRound);
		break;
	}
}

}

//! 凡例
class Legend : public NullableShape, public boost::noncopyable
{
public:
	Legend(IMessageReceiver* owner)
		: m_owner(owner), m_name(::GetResourceString(m_owner,"ID_SOURCE_TEXT_EXPLANATORY_NOTES"))
		, m_visible(false), m_selected(false)
		, m_lineStyle(PS_SOLID), m_lineColor(0), m_lineWidth(1), m_roundStyle(RT_RECTANGLE)
		, m_fillColor(0xffffff), m_font(), m_fontColor(0)
	{
		m_font.lfHeight = ::PointToDeviceUnit(8);
		::_tcscpy_s(m_font.lfFaceName, GetResourceString(m_owner, "ID_SOURCE_TEXT_MS_P_GOTHIC"));
	}

// IShape Interface
	void Destroy() { delete this; }

	void GetName(wchar_t* name, size_t length) const
	{
		::_tcsncpy_s(name, length - 1, m_name.c_str(), _TRUNCATE);
	}
	unsigned long GetTypeId() const
	{
		return 16;
	}

	bool IsVisible() const { return m_visible; }

	bool IsDeletable() const { return false; }

	void SetSelected(bool select)
	{
		if (m_selected != select) {
			m_selected = select;
			OnPropertyUpdated();
			m_owner->SendMessage(PI_MSG_INVALIDATE, 0, 0);
		}
	}
	bool GetSelected() const { return m_selected; }

	void Move(double left, double top)
	{
		m_location.SetPoint(left, top);
	}

	int HitTest(double x, double y) const
	{
		POINT pt = ::MMToPx(OCPoint(x, y) - m_location);
		return ::PtInRect(&m_rect, pt) ? HIT_MOVE : HIT_NONE;
	}

	PropertyNode* CreateProperty();
	PropertyNode* CreateTabProperty();
	void ReleaseProperty(PropertyNode* prop) { delete prop; }
	bool SetProperty(long id, LPARAM value, LPARAM* source);
	bool GetProperty(long id, LPARAM* ret) const;

	void Serialize(ISerializer* ar);

	Gdiplus::Color FromCOLORREF(COLORREF color) const
	{
		return Gdiplus::Color(GetRValue(color), GetGValue(color), GetBValue(color));
	}

	Gdiplus::RectF FromRECT(RECT const& r) const
	{
		return Gdiplus::RectF(static_cast<Gdiplus::REAL>(r.left),
			static_cast<Gdiplus::REAL>(r.top),
			static_cast<Gdiplus::REAL>(r.right - r.left),
			static_cast<Gdiplus::REAL>(r.bottom - r.top));
	}

// Operation
	/*!	@brief	凡例を描画します
		@param[in]	groups
	*/
	template<class T, class Container>
	void Draw(HDC dc, Container const& groups, int const width) const
	{
		if (!m_visible) { return; }
		if (groups.empty()) { return; }

		m_plotRect.clear();

		Gdiplus::Graphics g(dc);
		Gdiplus::Font font(dc, &m_font);
		LOGFONT lf2 = m_font;
		::_tcscpy_s(lf2.lfFaceName, _countof(lf2.lfFaceName), _T("Arial"));
		lf2.lfWeight = 1000;
		Gdiplus::Font font2(dc, &lf2);
		Gdiplus::Pen pen(m_selected ? Gdiplus::Color(0, 0, 255) : FromCOLORREF(m_lineColor));
		Gdiplus::SolidBrush brush(FromCOLORREF(m_fillColor));
		Gdiplus::SolidBrush textBrush(Gdiplus::Color(FromCOLORREF(m_fontColor)));
		Gdiplus::SolidBrush whiteBrush(Gdiplus::Color(255, 255, 255));

		if (m_selected) {
			pen.SetDashStyle(Gdiplus::DashStyleCustom);
			Gdiplus::REAL ptn[] = {2.0f, 2.0f};
			pen.SetDashPattern(ptn, 2);
		}
		else {
			pen.SetWidth((float)m_lineWidth);//::GetPenWidth(m_lineWidth));
			SetPenStyle(&pen, m_lineStyle);
		}
		SetLineRoundType(&pen, m_roundStyle);

		g.SetSmoothingMode(Gdiplus::SmoothingModeAntiAlias);
		g.SetInterpolationMode(Gdiplus::InterpolationModeHighQualityBicubic);
		g.SetTextRenderingHint(Gdiplus::TextRenderingHintAntiAlias);

		RECT r = GetFrameRect<T>(&g, &font, groups, width);
		// 枠
		g.FillRectangle(&brush, FromRECT(r));
		g.DrawRectangle(&pen, FromRECT(r));

		TCHAR const* symbol[] = {_T("A"), _T("B")};

		int i = 0;
		r.left += MarginX * 2;
		BOOST_FOREACH(boost::shared_ptr<T> const& plot, groups) {
			if (!plot->IsVisible()) { continue; }
			if (plot->IsEmpty()) { continue; }

			r.top += MarginY * 2;

			std::wstring const name = plot->GetName().empty() ? ::GetResourceString(m_owner,"ID_RESOURCE_HAIHUN") : plot->GetName();
			Gdiplus::StringFormat fmt(Gdiplus::StringFormatFlagsNoWrap | Gdiplus::StringFormatFlagsNoFitBlackBox | Gdiplus::StringFormatFlagsMeasureTrailingSpaces);
			fmt.SetTrimming(Gdiplus::StringTrimmingEllipsisCharacter);

			Gdiplus::RectF bounds;
			g.MeasureString(name.c_str(), (INT)name.length(), &font, FromRECT(r), &fmt, &bounds);
			int const height = static_cast<int>(bounds.Height);

			int const markerSize = std::max<int>(MarkerWidth, (LONG)bounds.Height);

			// プロット名
			RECT textRect = { r.left + markerSize + 5, r.top, r.right, r.bottom };
			g.DrawString(name.c_str(), (INT)name.length(), &font, FromRECT(textRect), &fmt, &textBrush);

			RECT const tr = { textRect.left, textRect.top, textRect.right, textRect.top + height };
			m_plotRect[plot.get()] = tr;

			if (plot->IsSelected()) {
				Gdiplus::Pen pen2(Gdiplus::Color(0, 0, 255));
				pen2.SetDashStyle(Gdiplus::DashStyleCustom);
				Gdiplus::REAL ptn[] = {2.0f, 2.0f};
				pen2.SetDashPattern(ptn, 2);

				RECT frame = { r.left - MarginX, r.top - MarginY, r.right - MarginX, r.top + height + MarginY };
				g.DrawRectangle(&pen2, FromRECT(frame));
			}

			// マーカー
			RECT markerRect = { r.left - 1, r.top - 1, r.left + markerSize, r.top + markerSize };
			Gdiplus::SolidBrush b(FromCOLORREF(plot->GetPlotColor()));
			g.FillEllipse(&b, FromRECT(markerRect));

			fmt.SetLineAlignment(Gdiplus::StringAlignmentCenter);
			fmt.SetAlignment(Gdiplus::StringAlignmentCenter);
			markerRect.left += 1;
			markerRect.top += 2;
			g.DrawString(symbol[i], 1, &font2, FromRECT(markerRect), &fmt, &whiteBrush);

			r.top += height + MarginY;

			++i;
		}
	}

	template<class T>
	bool Contains(T const* plot, double x, double y)
	{
		POINT const pt = ::MMToPx(OCPoint(x, y) - m_location);
		RECT const r = m_plotRect[plot];
		return !!::PtInRect(&r, pt);
	}

	boost::signals2::signal<void(bool)> PropertyUpdated;

private:
	static const int MarginX = 5;
	static const int MarginY = 5;
	static const int MarkerWidth = 12;	//!< マーカーの幅(高さはテキストから算出する)
	static const int Top = 10;			//!< 凡例のTop座標(px)

	template<class T, class Container>
	RECT GetFrameRect(Gdiplus::Graphics* g, Gdiplus::Font* font, Container const& groups, int const right) const
	{
		LONG visibleCount = 0;
		SIZE maxSize = {};
		BOOST_FOREACH(boost::shared_ptr<T> const& plot, groups) {
			if (!plot->IsVisible()) { continue; }
			if (plot->IsEmpty()) { continue; }

			std::wstring const name = plot->GetName().empty() ? ::GetResourceString(m_owner,"ID_RESOURCE_HAIHUN") : plot->GetName();
			Gdiplus::RectF bounds;
			g->MeasureString(name.c_str(), (INT)name.length(), font, Gdiplus::PointF(0, 0), &bounds);

			maxSize.cx = std::max<LONG>(static_cast<LONG>(bounds.Width), maxSize.cx);
			maxSize.cy += static_cast<LONG>(bounds.Height);
			++visibleCount;
		}

		int const width = std::min<int>(maxSize.cx + MarginX * 4 + MarkerWidth, right / 2 - 10);
		int const left = right - width - 25; // 断面図のボタン▼と重ならないよう左にずらす

		int const space = groups.size() % 2 == 0 ? 1 : 0;
		RECT r = {
			left, Top,
			left + width,
			Top + maxSize.cy + MarginY * (visibleCount * 4 - space) };
		return (m_rect = r);
	}

	void OnPropertyUpdated()
	{
		PropertyUpdated(false);
	}

	void AddShowProperty(IPropertyComposite* prop);
	void AddLineProperty(IPropertyComposite* prop);
	void AddPaintProperty(IPropertyComposite* prop);
	void AddTextProperty(IPropertyComposite* prop);

private:
	IMessageReceiver* m_owner;

	std::wstring m_name;
	bool m_visible;
	bool m_selected;

	int m_lineStyle;
	COLORREF m_lineColor;
	double m_lineWidth;
	int m_roundStyle;
	COLORREF m_fillColor;
	LOGFONT m_font;
	COLORREF m_fontColor;

	OCPoint m_location;
	RECT mutable m_rect;
	std::map<void const*, RECT> mutable m_plotRect;
};

///////////////////////////////////////////////////////////////////////////////

inline void Legend::AddShowProperty(IPropertyComposite* prop)
{
	IPropertyGroup* group = prop->AddGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_SHOW"), PI_VISIBLE, m_visible, IsLock());
	group->AddProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_NAME"), PI_NAME, m_name.c_str(), IsLock());
	//group->AddBoolProperty(_T("ロック"), PI_LOCK, m_lock);
	group->AddBoolProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_LOCK"), PI_POS_LOCK, m_posLock);

}

inline void Legend::AddLineProperty(IPropertyComposite* prop)
{
	IPropertyGroup* group = prop->AddGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_LINE"));
	std::wstring styleProp1 = ::GetResourceString(m_owner,"ID_SOURCE_TEXT_SOLID_LINE");
	std::wstring styleProp2 = ::GetResourceString(m_owner,"ID_SOURCE_TEXT_DASHED_LINE");
	std::wstring styleProp3 = ::GetResourceString(m_owner,"ID_SOURCE_TEXT_DOTTED_LINE");
	std::wstring styleProp4 = ::GetResourceString(m_owner,"ID_SOURCE_TEXT_ONE_P_DASHED_LINE");
	std::wstring styleProp5 = ::GetResourceString(m_owner,"ID_RESOURCE_TWO_POINTS_OF_DOT_DASH_LINES");
	std::wstring styleProp6 = ::GetResourceString(m_owner,"ID_SOURCE_TEXT_NONE_KANJI");
	group->AddArrayProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_STYLE"), PI_LINE_STYLE, m_lineStyle, IsLock(), 6, 
		styleProp1.c_str(), styleProp2.c_str(), styleProp3.c_str(), styleProp4.c_str(), styleProp5.c_str(), styleProp6.c_str());

	group->AddColorProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_COLOR_KANJI"), PI_LINE_COLOR, m_lineColor, IsLock());
	group->AddSelectorPropertyR(::GetResourceString(m_owner,"ID_SOURCE_TEXT_THICKNESS_PT"), PI_LINE_WIDTH, IsLock(), m_lineWidth, 9, 0.25, 0.5, 0.75, 1.0, 1.5, 2.25, 3.0, 4.5, 6.0);
	//TODO: 3.3 角の形

	std::wstring corner1 = ::GetResourceString(m_owner,"ID_SOURCE_TEXT_ROUND");
	std::wstring corner2 = ::GetResourceString(m_owner,"ID_SOURCE_TEXT_SQUARE");
	group->AddArrayProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_SHAPE_OF_CORNER"), PI_LINE_ROUND, m_roundStyle, IsLock() || m_lineStyle == PS_NULL, 2, 
		corner1.c_str(), corner2.c_str());
}

inline void Legend::AddPaintProperty(IPropertyComposite* prop)
{
	IPropertyGroup* group = prop->AddGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_PAINT"));
	group->AddColorProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_COLOR_KANJI"), PI_FILL_COLOR, m_fillColor, IsLock());
}

inline void Legend::AddTextProperty(IPropertyComposite* prop)
{
	IPropertyGroup* group = prop->AddGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_TEXT"));

	int const height = static_cast<int>(std::abs(m_font.lfHeight) / (96.0 / 72.0) + 0.5);

	static std::wstring s;
	s = (boost::wformat(::GetResourceString(m_owner,"ID_SOURCE_TEXT_1_2")) % m_font.lfFaceName % height).str();

	static LOGFONT fontProp;
	fontProp = m_font;
	fontProp.lfWidth = m_fontColor;
	group->AddFontProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_FONT"), PI_FONT_TYPE, fontProp, IsLock());
}

inline PropertyNode* Legend::CreateProperty()
{
	OCProperty prop;
	AddShowProperty(&prop);
	AddLineProperty(&prop);
	AddPaintProperty(&prop);
	AddTextProperty(&prop);

	return prop.GetRawPtr();
}
inline PropertyNode* Legend::CreateTabProperty()
{
	OCProperty prop;
	AddShowProperty(prop.CreateGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_SHOW")));
	AddLineProperty(prop.CreateGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_FORMATTING")));
	AddPaintProperty(prop.CreateGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_FORMATTING")));
	AddTextProperty(prop.CreateGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_FORMATTING")));

	return prop.GetRawPtr();
}

inline bool Legend::SetProperty(long id, LPARAM value, LPARAM* source)
{
	switch (id)
	{
	case PI_VISIBLE:
		::SetPropertyHelper(m_visible, value, source);
		OnPropertyUpdated();
		return true;
	case PI_NAME:
		::SetPropertyHelper(m_name, value, source);
		return true;
	case PI_LOCK:
		::SetPropertyHelper(m_lock, value, source);
		return true;
	case PI_POS_LOCK:
		::SetPropertyHelper(m_posLock, value, source);
		return true;
	case PI_LINE_STYLE:
		::SetPropertyHelper(m_lineStyle, value, source);
		OnPropertyUpdated();
		return true;
	case PI_LINE_COLOR:
		::SetPropertyHelper(m_lineColor, value, source);
		OnPropertyUpdated();
		return true;
	case PI_LINE_WIDTH:
		::SetPropertyHelper(m_lineWidth, value, source);
		OnPropertyUpdated();
		return true;
	case PI_LINE_ROUND:
		::SetPropertyHelper(m_roundStyle, value, source);
		OnPropertyUpdated();
		return true;

	case PI_FILL_COLOR:
		::SetPropertyHelper(m_fillColor, value, source);
		OnPropertyUpdated();
		return true;

	case PI_FONT_TYPE:
		if (source) { *(LOGFONT*)source = m_font; }
		m_font = *reinterpret_cast<LOGFONT*>(value);
		m_fontColor = m_font.lfWidth; // Widthに色が入っている...
		m_font.lfWidth = 0;
		OnPropertyUpdated();
		return true;
	}

	return false;
}
inline bool Legend::GetProperty(long /*id*/, LPARAM* /*ret*/) const
{
	return false;
}

inline void Legend::Serialize(ISerializer* ar)
{
	ar->Serialize(_T("Visible"), m_visible, false);
	SerializeString(ar, _T("Name"), m_name, m_name);
	m_lock = false;
	ar->Serialize(_T("Lock"), m_posLock);
	ar->Serialize(_T("LineStyle"), m_lineStyle);
	ar->Serialize(_T("LineWidth"), m_lineWidth);
	ar->Serialize(_T("LineRoundStyle"), m_roundStyle);
	ar->Serialize(_T("LineColor"), m_lineColor);
	ar->Serialize(_T("FillColor"), m_fillColor);
	ar->Serialize(_T("Font"), m_font);
	ar->Serialize(_T("FontColor"), m_fontColor);
}