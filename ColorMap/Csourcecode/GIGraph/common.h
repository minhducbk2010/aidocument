/*!	@file
*/
#pragma once

#include <boost/shared_ptr.hpp>

static const TCHAR DEFAULT_AXIS_FONT[] = _T("ＭＳ Ｐゴシック");
static const int DEFAULT_FONT_SIZE = 10;

namespace
{

void WindowToWorld(int x, int y, double *glX, double *glY, double *glZ)
{
	GLint viewPort[4];
	glGetIntegerv(GL_VIEWPORT, viewPort);
	GLint const yy = viewPort[3] - y;

	GLfloat zz;
	glReadPixels(x, yy, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &zz);

	GLdouble projectionMatrix[16];
	GLdouble modelViewMatrix[16];
	::glGetDoublev(GL_PROJECTION_MATRIX, projectionMatrix);
	::glGetDoublev(GL_MODELVIEW_MATRIX, modelViewMatrix);

	::gluUnProject((GLdouble)x, (GLdouble)yy, (GLdouble)zz, modelViewMatrix, projectionMatrix, viewPort, glX, glY, glZ);
}

OCPoint3D WindowToWorld(int x, int y)
{
	OCPoint3D p;
	WindowToWorld(x, y, &p.x, &p.y, &p.z);
	return p;
}

OCPoint GetWindowToWorldRatio()
{
	GLint viewPort[4];
	::glGetIntegerv(GL_VIEWPORT, viewPort);
	int const width = viewPort[2] - viewPort[0];
	int const height = viewPort[3] - viewPort[1];
	return OCPoint(1.0 / width, 1.0 / height); //TODO: 分子は可変になるかも
}

void WorldToWindow(double glX, double glY, double glZ, double* x, double* y, double* z)
{
    GLint viewport[4] = {};
    GLdouble mvMatrix[16] = {}, pMatrix[16] = {};
	double dx, dy, dz;

    ::glGetIntegerv(GL_VIEWPORT, viewport);
    ::glGetDoublev(GL_MODELVIEW_MATRIX, mvMatrix);
    ::glGetDoublev(GL_PROJECTION_MATRIX, pMatrix);
	::gluProject(glX, glY, glZ, mvMatrix, pMatrix, viewport, &dx, &dy, &dz);

	*x = std::min<double>(dx, viewport[2]);
	*y = (viewport[3] + viewport[1]) - dy;
	*z = dz;
}

void WorldToWindow(double glX, double glY, double glZ, double* x, double* y)
{
	double tmp;
	WorldToWindow(glX, glY, glZ, x, y, &tmp);
}

OCPoint3D WorldToWindow(double glX, double glY, double glZ)
{
	OCPoint3D p;
	WorldToWindow(glX, glY, glZ, &p.x, &p.y, &p.z);
	return p;
}

void RenderPoint(double x, double y, double z, double size = 0.5)
{
	boost::shared_ptr<GLUquadric> quad(::gluNewQuadric(), ::gluDeleteQuadric);
	::glPushMatrix();
		::glTranslated(x, y, z);
		::gluSphere(quad.get(), size, 20, 20);
	::glPopMatrix();
}

double const DEFAULT_X_ANGLE = 20;
double const DEFAULT_Y_ANGLE = -45;
double const DEFAULT_Z_ANGLE = 0;

int const BUTTON_OFFSET = 20;	//!< ボタンまでのオフセット
int const BUTTON_SIZE = 10;		//!< ボタンの大きさ

int const SECTION_SIZE = 110;

} // namespace