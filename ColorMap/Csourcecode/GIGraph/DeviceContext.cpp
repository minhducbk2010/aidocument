// DeviceContext.cpp
#include "stdafx.h"
#include "DeviceContext.h"
#include "ColorTexture.h"

DeviceContext::DeviceContext(HDC device, HGLRC context)
	: wnd(0)
{
	this->device = device;
	this->context = context;
}
DeviceContext::~DeviceContext()
{
	RelaseCurrentContext();
}

DeviceContextPtr DeviceContext::CreateInstance(HWND window)
{
	HDC device = ::GetDC(window);
	DeviceContextPtr self = CreateInstance(device);
	self->wnd = window;
	return self;
}
DeviceContextPtr DeviceContext::CreateInstance(HDC device)
{
	PIXELFORMATDESCRIPTOR fd = {};
	fd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	fd.nVersion = 1;
	//fd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	fd.dwFlags = PFD_DRAW_TO_BITMAP | PFD_SUPPORT_OPENGL | PFD_SUPPORT_GDI;
	fd.iPixelType = PFD_TYPE_RGBA;
	fd.cColorBits = 24;
	fd.cDepthBits = 16;
	//fd.cStencilBits = 1;
	fd.iLayerType = PFD_MAIN_PLANE;

	int pixelFormat = ChoosePixelFormat(device, &fd);
	if (pixelFormat == 0)
	{
		return DeviceContextPtr();
	}

	if (!SetPixelFormat(device, pixelFormat, &fd))
	{
		fd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
		fd.iPixelType = PFD_TYPE_RGBA;
		fd.cColorBits = 32;
		fd.cDepthBits = 16;
		fd.cStencilBits = 1;
		fd.iLayerType = PFD_MAIN_PLANE;
		pixelFormat = ChoosePixelFormat(device, &fd);
		if (!SetPixelFormat(device, pixelFormat, &fd)) {
			return DeviceContextPtr();
		}
	}

	HGLRC context = wglCreateContext(device);
	if (context == NULL)
	{
		return DeviceContextPtr();
	}

	return DeviceContextPtr(new DeviceContext(device, context));
}

BOOL DeviceContext::MakeCurrentContext()
{
	if (wglGetCurrentContext() != context) {
		return wglMakeCurrent(device, context);
	}
	return TRUE;
}
BOOL DeviceContext::RelaseCurrentContext()
{
	if (wglMakeCurrent(device, NULL)) {
		wglDeleteContext(context);

		if (wnd) {
			::ReleaseDC(wnd, device);
		}
		return TRUE;
	}
	return FALSE;
}
BOOL DeviceContext::SwapBuffers()
{
	return ::SwapBuffers(device);
}
