#include "stdafx.h"
#include "ColorTexture.h"

ColorTexture::ColorTexture(std::wstring const& userDirectory, IMessageReceiver* owner)
	: OCColorPattern(userDirectory, owner)
	, texture(0)
	, m_binding(false)
{
	SetMinMax(110.0, 170.0);
}

ColorTexture::~ColorTexture()
{
	Destroy();
}

void ColorTexture::SetMinMax(double min, double max)
{
	__super::SetMinMax(min, max);
	Reset();
}

void ColorTexture::Initialize()
{
	int const Resolution = 64; // �F��
	double const colorDiv = (m_max - m_min) / Resolution;

	GLubyte image[Resolution][3] = {0};
	for (BYTE i = 0U; i < Resolution; ++i)
	{
		double value = m_min + colorDiv * i;
		COLORREF color = GetColor(value);

		image[i][0] = GetRValue(color);
		image[i][1] = GetGValue(color);
		image[i][2] = GetBValue(color);
	}

	glBindTexture(GL_TEXTURE_1D, texture);

	glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB, Resolution, 0, GL_RGB, GL_UNSIGNED_BYTE, image);

    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glBindTexture(GL_TEXTURE_1D, 0);
}

void ColorTexture::Bind()
{
	assert(glIsTexture(texture));

	glBindTexture(GL_TEXTURE_1D, texture);
	m_binding = true;
}
void ColorTexture::Unbind()
{
	glBindTexture(GL_TEXTURE_1D, 0);
	m_binding = false;
}

void ColorTexture::Reset()
{
	Unbind();
	Destroy();

	glEnable(GL_TEXTURE_1D);
	glGenTextures(1, &texture);

	Initialize();
}

void ColorTexture::Destroy()
{
	if (glIsTexture(texture)) {
		glDeleteTextures(1, &texture);
	}
}

void ColorTexture::UpdatePattern()
{
	Reset();
}