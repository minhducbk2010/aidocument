#pragma once

#include <GL/glext.h>

class GLExtension
{
public:
	static bool GetVBOSupported() { return vboSupported; }

	static void CheckDeviceAbility();
	static bool GenBuffersARB(GLsizei n, GLuint *buffers);
	static bool BindBufferARB(GLenum target, GLuint buffer);
	static bool BufferDataARB(GLenum target, int size, const GLvoid *data, GLenum usage);
	static bool DeleteBuffersARB(GLsizei n, const GLuint *buffers);
	static GLvoid *MapBufferARB(GLenum target, GLenum access);
	static GLboolean UnmapBufferARB(GLenum target);

private:
	static bool IsExtensionSupported(const char *extension);

	static bool vboSupported;
	static PFNGLGENBUFFERSARBPROC glGenBuffersARB;
	static PFNGLBINDBUFFERARBPROC glBindBufferARB;
	static PFNGLBUFFERDATAARBPROC glBufferDataARB;
	static PFNGLDELETEBUFFERSARBPROC glDeleteBuffersARB;
	static PFNGLMAPBUFFERARBPROC glMapBufferARB;
	static PFNGLUNMAPBUFFERARBPROC glUnmapBufferARB;
};
