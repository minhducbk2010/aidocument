/*!	@file
*/
#include "stdafx.h"
#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/range/algorithm/find.hpp>
#include <boost/format.hpp>
#include "Graph3DBase.h"
#include "Axis.h"
#include "ColorBar.h"
#include "Section.h"
#include "AxisGuide.h"
#include "TexVertex.h"
#include "GLExtension.h"
#include "GLBuffer.h"
#include "ColorTexture.h"
#include "TextRenderer.h"
#include "TextInfo.h"
#include "DeviceContext.h"
#include "IGraphUnit.h"
#include "IPlotData.h"
#include "utility.h"
#include "PreparedObject.h"
#include "IMessageReceiver.h"
#include "Message.h"
#include "common.h"
#include "DummyReceiver.h"

//! Constructor
Graph3DBase::Graph3DBase(HWND window, IMessageReceiver* owner)
	: m_wnd(window), m_owner(owner), m_parent(0)
	, m_dimension(3)
	, m_pitch(DEFAULT_X_ANGLE), m_yaw(DEFAULT_Y_ANGLE), m_roll(DEFAULT_Z_ANGLE)
	, m_scale(1), m_h(0), m_v(0)
	, m_viewAxisGuide(true), m_viewColorBar(true)
	, m_clipMaxX(true), m_clipMinX(true), m_clipMaxZ(true)
	, m_clipMinZ(true), m_clipMaxY(true), m_clipMinY(true)
	, m_wireframeColor(RGB(128, 128, 128))
	, m_cursorLineVisible(true)
	, m_xSection(new OCHorizontalSection()), m_ySection(new OCVerticalSection())
	, m_horizontanlSection(false), m_verticalSection(false)
	, m_editing(false), m_dragging(false), m_lock(false)
	, m_serializing(false), m_capturing(false)
{
	GLExtension::CheckDeviceAbility();

	std::wstring font = GetResourceString(m_owner, "ID_SOURCE_TEXT_MS_P_GOTHIC");
	TextRenderer::GetInstance()->SetFont(0, font.c_str(), DEFAULT_FONT_SIZE);

	glClearColor(1.0, 1.0, 1.0, 0.0);
	glShadeModel(GL_FLAT);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LINE_STIPPLE);
	glClearStencil(0);

	m_xAxis.reset(new XAxis(this));
	m_yAxis.reset(new YAxis(this));
	m_zAxis.reset(new ZAxis(this));
	m_tAxis.reset(new TAxis(this));
	m_axisGuide.reset(new AxisGuide());

	m_axes.push_back(m_xAxis.get());
	m_axes.push_back(m_zAxis.get());
	m_axes.push_back(m_yAxis.get());
	m_axes.push_back(m_tAxis.get());

	m_xAxis->AutoScaleChanged.connect(boost::bind(&Graph3DBase::AutoScaleAxis, this, _1));
	m_yAxis->AutoScaleChanged.connect(boost::bind(&Graph3DBase::AutoScaleAxis, this, _1));
	m_zAxis->AutoScaleChanged.connect(boost::bind(&Graph3DBase::AutoScaleAxis, this, _1));
	m_tAxis->AutoScaleChanged.connect(boost::bind(&Graph3DBase::AutoScaleAxis, this, _1));

	m_xAxis->AutoLabelChanged.connect(boost::bind(&Graph3DBase::AutoLabel, this, _1));
	m_yAxis->AutoLabelChanged.connect(boost::bind(&Graph3DBase::AutoLabel, this, _1));
	m_zAxis->AutoLabelChanged.connect(boost::bind(&Graph3DBase::AutoLabel, this, _1));
	m_tAxis->AutoLabelChanged.connect(boost::bind(&Graph3DBase::AutoLabel, this, _1));

	m_xSection->AttachAxis(m_yAxis.get(), m_zAxis.get());
	m_xSection->SetExtractAxis(m_xAxis.get());
	m_ySection->AttachAxis(m_zAxis.get(), m_xAxis.get());
	m_ySection->SetExtractAxis(m_yAxis.get());

	// Register Commands
	m_messageHandler[ID_OPT_WIREFRAME]	= boost::bind(&Graph3DBase::ToggleWireFrameVisible, this);
	m_messageHandler[ID_CMD_HIDELINE]	= boost::bind(&Graph3DBase::ToggleHideLine, this);
	m_messageHandler[ID_CMD_COLOR]		= boost::bind(&Graph3DBase::ToggleColoring, this);
	m_messageHandler[ID_OPT_PAINT]		= boost::bind(&Graph3DBase::ToggleFilling, this);
	m_messageHandler[ID_CMD_COLORBAR]	= boost::bind(&Graph3DBase::ToggleColorBarVisible, this);
	m_messageHandler[ID_CMD_HELP]		= boost::bind(&Graph3DBase::ToggleGuideVisible, this);
	m_messageHandler[ID_CMD_RESET]		= boost::bind(&Graph3DBase::ResetPosition, this);
	m_messageHandler[ID_CMD_XY]			= boost::bind(&Graph3DBase::SetXYPosition, this);
	m_messageHandler[ID_CMD_YZ]			= boost::bind(&Graph3DBase::SetYZPosition, this);
	m_messageHandler[ID_CMD_ZX]			= boost::bind(&Graph3DBase::SetZXPosition, this);

#define F(x) (boost::function<BOOL()>)boost::bind(&Graph3DBase::x, this)

	m_stateGetter[ID_OPT_WIREFRAME]		= boost::bind(&Graph3DBase::EnabledAlways, this, F(GetViewWireframe), _1, _2);
	m_stateGetter[ID_CMD_HIDELINE]		= boost::bind(&Graph3DBase::EnabledAlways, this, F(IsHiddenLine), _1, _2);
	m_stateGetter[ID_CMD_COLOR]			= boost::bind(&Graph3DBase::EnabledAlways, this, F(IsColoring), _1, _2);
	m_stateGetter[ID_OPT_PAINT]			= boost::bind(&Graph3DBase::EnabledAlways, this, F(IsFilling), _1, _2);
	m_stateGetter[ID_CMD_COLORBAR]		= boost::bind(&Graph3DBase::EnabledColorBar, this, _1, _2);
	m_stateGetter[ID_CMD_HELP]			= boost::bind(&Graph3DBase::EnabledAlways, this, F(GetViewAxisGuide), _1, _2);
	m_stateGetter[ID_CMD_RESET]			= boost::bind(&Graph3DBase::EnabledAlways, this, _1, _2);
	m_stateGetter[ID_CMD_XY]			= boost::bind(&Graph3DBase::EnabledAlways, this, _1, _2);
	m_stateGetter[ID_CMD_YZ]			= boost::bind(&Graph3DBase::EnabledAlways, this, _1, _2);
	m_stateGetter[ID_CMD_ZX]			= boost::bind(&Graph3DBase::EnabledAlways, this, _1, _2);

//ID_CMD_DIALOG
//ID_PLOT_VECTOR
}

Graph3DBase::~Graph3DBase()
{
}

void Graph3DBase::SetData(IPlotData* data)
{
	SetData(0, data);
}

IGraphFile* Graph3DBase::AddFile(size_t fileId)
{
	// 追加できない場合は何もしない
	if (!CanAddFile()) { return NULL; }
	
	bool first = true;

	IPlotData* data = reinterpret_cast<IPlotData*>(m_owner->SendMessage(PI_MSG_CREATE_PLOTDATA, fileId, 0));
	data->CopyFrom(fileId, GetData(0));
	bool const success = data->LinkDataSet();
	if(!success)
	{
		if (first) {
			ShowItemMismatchError(m_owner);
			first = false;				
		}
		//data->Destroy();
		//plot->Destroy();		
	}

	AddData(data);
	return GetFile(GetFileCount() - 1);
}

void Graph3DBase::EnableClipping(bool clipping)
{
	m_clipMinX = m_clipMaxZ = m_clipMinZ = m_clipMaxY = m_clipMinY = m_clipMaxX = clipping;
}

bool Graph3DBase::IsClipping() const
{
	return m_clipMaxX;
}

void Graph3DBase::CursorValueChanged(OCPoint3D const& point) const
{
	OCSearchPoint p(3);
	p.xName = GetData(0)->GetItemName(0);
	p.xUnit = GetData(0)->GetUnitName(0);
	p.yName = GetData(0)->GetItemName(1);
	p.yUnit = GetData(0)->GetUnitName(1);
	p.zName = GetData(0)->GetItemName(2);
	p.zUnit = GetData(0)->GetUnitName(2);
	m_cursorValue[0] = m_xAxis->CoordinateToValue(point.x);
	m_cursorValue[1] = m_yAxis->CoordinateToValue(-point.y);
	m_cursorValue[2] = m_zAxis->CoordinateToValue(point.z);
	::_stprintf_s(p.xValue, _T("%g"), m_cursorValue[0]);
	::_stprintf_s(p.yValue, _T("%g"), m_cursorValue[1]);
	::_stprintf_s(p.zValue, _T("%g"), m_cursorValue[2]);

	m_owner->SendMessage(PI_MSG_SET_PROP_VALUE, reinterpret_cast<WPARAM>(&p), 0);
}

BOOL Graph3DBase::GetClipMaxX()
{
	return m_clipMaxX;
}
BOOL Graph3DBase::GetClipMinX()
{
	return m_clipMinX;
}
BOOL Graph3DBase::GetClipMaxZ()
{
	return m_clipMaxZ;
}
BOOL Graph3DBase::GetClipMinZ()
{
	return m_clipMinZ;
}
BOOL Graph3DBase::GetClipMaxY()
{
	return m_clipMaxY;
}
BOOL Graph3DBase::GetClipMinY()
{
	return m_clipMinY;
}
void Graph3DBase::SetClipMaxX(bool value)
{
	m_clipMaxX = value;
}
void Graph3DBase::SetClipMinX(bool value)
{
	m_clipMinX = value;
}
void Graph3DBase::SetClipMaxZ(bool value)
{
	m_clipMaxZ = value;
}
void Graph3DBase::SetClipMinZ(bool value)
{
	m_clipMinZ = value;
}
void Graph3DBase::SetClipMaxY(bool value)
{
	m_clipMaxY = value;
}
void Graph3DBase::SetClipMinY(bool value)
{
	m_clipMinY = value;
}
bool Graph3DBase::GetCursorLineVisible() const
{
	return m_cursorLineVisible;
}
void Graph3DBase::SetCursorLineVisible(bool value)
{
	m_cursorLineVisible = value;
}

COLORREF Graph3DBase::GetWireframeColor()
{
	return m_wireframeColor;
}
void Graph3DBase::SetWireframeColor(COLORREF color)
{
	m_wireframeColor = color;
}

void Graph3DBase::ConvertValueToPosition(double& x, double& y, double& z)
{
	x = (double)m_xAxis->ValueToPosition(x);
	y = (double)m_yAxis->ValueToPosition(y);
	z = (double)m_zAxis->ValueToPosition(z);
}

void Graph3DBase::RenderPlanes()
{
	std::for_each(m_axes.begin(), m_axes.end(), boost::bind(&Axis::Modeling, _1));
	std::for_each(m_axes.begin(), m_axes.end(), boost::bind(&Axis::Render, _1));
}

void Graph3DBase::GetValue(float x, float y, float z, float *vX, float *vY, float *vZ)
{
	*vX = (float)m_xAxis->CoordinateToValue(x);
	*vY = (float)m_yAxis->CoordinateToValue(y);
	*vZ = (float)m_zAxis->CoordinateToValue(z);
}
void Graph3DBase::GetOrder(float x, float z, float *order)
{
	double frq = m_xAxis->CoordinateToValue(x);
	double rev = m_zAxis->CoordinateToValue(z);

	if (rev > 0.0)
	{
		*order = (float)(frq * 60.0 / rev);
	}
	else
	{
		*order = 0.0F;
	}
}

double AdjustAngle(double angle)
{
	if (angle > 180.0F) {
		angle -= 360.0F;
	}
	else if (angle <= -180.0F) {
		angle += 360.F;
	}
	return static_cast<int>(angle);
}
void Graph3DBase::Rotate(double pitch, double yaw, double roll)
{
	m_pitch = AdjustAngle(pitch);
	m_yaw = AdjustAngle(yaw);
	m_roll = AdjustAngle(roll);

	Rotated(pitch, yaw, roll);
}
void Graph3DBase::Scaling(double scale)
{
	m_scale = std::max<double>(scale, 0.01);
}

void Graph3DBase::Translate(double h, double v)
{
	m_h = h;
	m_v = v;
}

void Graph3DBase::GetPosition(double* h, double* v) const
{
	*h = m_h;
	*v = m_v;
}

void Graph3DBase::OffsetRotate(double pitch, double yaw, double roll)
{
	Rotate(m_pitch + pitch, m_yaw + yaw, m_roll + roll);
}
void Graph3DBase::OffsetScale(double scale)
{
	Scaling(m_scale + scale);
}
void Graph3DBase::OffsetTranslate(double h, double v)
{
	m_h += h;
	m_v += v;
}

void Graph3DBase::Render(HDC dc)
{
	RenderImpl(dc, true);//RenderImpl(!m_dragging);
}
void Graph3DBase::RenderFrame(HDC dc)
{
	RenderImpl(dc, false);
}

void Graph3DBase::RenderImpl(HDC dc, bool plotRendering)
{
	if (boost::shared_ptr<PreparedObject> p = CreatePrepateObject()) {
		RenderPlanes();
		if (plotRendering) {
			RenderPlots(dc);
		}
#ifdef _DEBUG
		::glDisable(GL_DEPTH_TEST);
		double const w = 10;
		::glColor3ub(255, 0, 0); ::RenderPoint(w, -w, w);
		::glColor3ub(0, 255, 0); ::RenderPoint(w, w, -w);
		::glColor3ub(0, 0, 255); ::RenderPoint(-w, -w, -w);
		::glEnable(GL_DEPTH_TEST);
#endif
	}

	::glPushMatrix(); {
		::glTranslated(m_h, m_v, 0);
		::glScaled(m_scale, m_scale, m_scale);
		if (GetViewColorBar()) {
			RenderColorBar();
		}
		if (m_viewAxisGuide) {
			m_axisGuide->Modeling();
			m_axisGuide->Render(m_pitch, m_yaw, m_roll);
		}
	}
	::glPopMatrix();
	glFlush();
}
void Graph3DBase::Reshape(int width, int height, double aspect, bool render/* = false*/)
{
	int const top = (render && EnableHorizontalSection()) ? SECTION_SIZE : 0;

	::glViewport(0, top, width, height);
	TransformProjection(aspect);
	::glMatrixMode(GL_MODELVIEW);
	::glLoadIdentity();
}
void Graph3DBase::Display()
{
//	context->SwapBuffers();
//	TextRenderer::GetInstance()->DrawGDIText(context->GetGDIDevice());
}
void Graph3DBase::Capture(HDC deviceContext, int width, int height, bool bitmap/* = true*/)
{
	DeviceContextPtr context = DeviceContext::CreateInstance(deviceContext);
	context->MakeCurrentContext();

	std::wstring font = GetResourceString(m_owner, "ID_SOURCE_TEXT_MS_P_GOTHIC");
	TextRenderer::GetInstance()->SetFont(deviceContext, font.c_str(), DEFAULT_FONT_SIZE);

	m_width = width; // TODO: 場所がいまいち
	m_height = height;

	WTL::CRect r(0, 0, width, height);

	if (EnableHorizontalSection()) {
		height -= SECTION_SIZE;
	}
	if (EnableVerticalSection()) {
		width -= SECTION_SIZE;
	}

	::glClearColor(1, 1, 1, 0);
    ::glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	::glEnable(GL_DEPTH_TEST);

	Reshape(width, height, 1, true);

	m_capturing = bitmap;
	Render(deviceContext);
	m_capturing = false;

	//
	TextRenderer::GetInstance()->DrawGDIText(deviceContext);

	//
	DrawSection(deviceContext, r);

	context.reset();

#ifdef _DEBUG
	{ // 角度を表示(デバッグ用)
		TCHAR buf[200] = {};
		::_stprintf_s(buf, _T("%g, %g, %g (DEBUG)"), m_pitch, m_yaw, m_roll);
		WTL::CDCHandle handle(deviceContext);
		//handle.TextOut(0, 0, buf);
	}
#endif
}

void Graph3DBase::DrawSection(HDC deviceContext, WTL::CRect const& sectionRect)
{
	// 断面
	// 横
	if (EnableHorizontalSection()) {
		OCRect sr(0, sectionRect.bottom - SECTION_SIZE, sectionRect.right, sectionRect.bottom);
		if (EnableVerticalSection()) {
			sr.right -= SECTION_SIZE;
		}
		sr.right -= 20; // 少し隙間をあける
		m_xSection->Draw(deviceContext, sr);
	}
	// 縦
	if (EnableVerticalSection()) {
		OCRect sr(sectionRect.right - SECTION_SIZE, 0, sectionRect.right, sectionRect.bottom - 10);
		if (EnableHorizontalSection()) {
			sr.bottom -= SECTION_SIZE;
		}
		m_ySection->Draw(deviceContext, sr);
	}

	if (EnableSection() && m_editing) {
		DrawSectionToggleButton(deviceContext);
	}
}

void Graph3DBase::UpdateLock()
{
	m_lock = true;
}

void Graph3DBase::UpdateUnlock()
{
	m_lock = false;
}

int Graph3DBase::SavePreEditState()
{
	UndoBuffer buf = {
		m_pitch, m_yaw, m_roll,
		m_scale, m_h, m_v,
	};

	int n = static_cast<int>(m_undo.size());
	m_undo[n] = buf;
	return n;
}

void Graph3DBase::RestoreEditState(int state)
{
	std::map<int, UndoBuffer>::iterator it = m_undo.find(state);
	if (it == m_undo.end()) { return; }

	UndoBuffer& buf = it->second;
	std::swap(m_pitch, buf.xAngle);
	std::swap(m_yaw, buf.yAngle);
	std::swap(m_roll, buf.zAngle);
	std::swap(m_scale, buf.scale);
	std::swap(m_h, buf.xPos);
	std::swap(m_v, buf.yPos);
}

//! 断面図が表示できるかどうか取得します
bool Graph3DBase::EnableSection() const
{
	// Proでのみ使用可能
	return m_owner->SendMessage(PI_MSG_IS_PROFESSIONAL, 0, 0) != 0;
}

//! 断面の表示切り替えボタンを描画します
void Graph3DBase::DrawSectionToggleButton(HDC dc) const
{
	WTL::CDCHandle handle(dc);
	WTL::CPen pen;
	pen.CreatePen(PS_SOLID, 1, RGB(192, 192, 192));
	HPEN oldPen = handle.SelectPen(pen);
	WTL::CBrush brush;
	brush.CreateSolidBrush(RGB(192, 192, 192));
	HBRUSH oldBrush = handle.SelectBrush(brush);
	{
		WTL::CRect const vr = GetVertShowButtonRect();
		if (EnableVerticalSection()) {
			WTL::CPoint pts [] = {
				WTL::CPoint(vr.right, vr.top),
				WTL::CPoint(vr.left, vr.top + vr.Height() / 2),
				WTL::CPoint(vr.right, vr.bottom)
			};
			handle.Polygon(&pts[0], _countof(pts));
		}
		else {
			WTL::CPoint pts [] = {
				WTL::CPoint(vr.left, vr.top),
				WTL::CPoint(vr.right, vr.top + vr.Height() / 2),
				WTL::CPoint(vr.left, vr.bottom)
			};
			handle.Polygon(&pts[0], _countof(pts));
		}
	}

	{
		WTL::CRect const hr = GetHorzShowButtonRect();
		if (EnableHorizontalSection()) {
			WTL::CPoint pts [] = {
				WTL::CPoint(hr.left, hr.bottom),
				WTL::CPoint(hr.left + hr.Width() / 2, hr.top),
				WTL::CPoint(hr.right, hr.bottom)
			};
			handle.Polygon(&pts[0], _countof(pts));
		}
		else {
			WTL::CPoint pts [] = {
				WTL::CPoint(hr.left, hr.top),
				WTL::CPoint(hr.left + hr.Width() / 2, hr.bottom),
				WTL::CPoint(hr.right, hr.top)
			};
			handle.Polygon(&pts[0], _countof(pts));
		}
	}
	handle.SelectPen(oldPen);
	handle.SelectBrush(oldBrush);
}

void Graph3DBase::SelectIndex(int index, int line)
{
	index, line;
}
void Graph3DBase::GetCursorIndex(int* index, int* line) const
{
	index, line;
}

void Graph3DBase::Click(int x, int y)
{
	// 断面のボタンが押されたか判定し、押されたら断面図の表示を切り替える
	WTL::CRect const vr = GetVertShowButtonRect();
	if (vr.PtInRect(WTL::CPoint(x, y))) {
		m_verticalSection = !m_verticalSection;

		if (m_verticalSection) {
			Resized(m_width + SECTION_SIZE, m_height);
		}
		else {
			Resized(m_width - SECTION_SIZE, m_height);
		}
	}

	WTL::CRect const hr = GetHorzShowButtonRect();
	if (hr.PtInRect(WTL::CPoint(x, y))) {
		m_horizontanlSection = !m_horizontanlSection;

		if (m_horizontanlSection) {
			Resized(m_width, m_height + SECTION_SIZE);
		}
		else {
			Resized(m_width, m_height - SECTION_SIZE);
		}
	}
}

WTL::CRect Graph3DBase::GetVertShowButtonRect() const
{
	int const right = EnableVerticalSection() ? m_width - SECTION_SIZE : m_width;
	return WTL::CRect(
		WTL::CPoint(right - BUTTON_OFFSET, BUTTON_OFFSET),
		WTL::CSize(BUTTON_SIZE, BUTTON_SIZE));
}

WTL::CRect Graph3DBase::GetHorzShowButtonRect() const
{
	int const bottom = EnableHorizontalSection() ? m_height - SECTION_SIZE : m_height;
	return WTL::CRect(
		WTL::CPoint(BUTTON_OFFSET, bottom - BUTTON_OFFSET),
		WTL::CSize(BUTTON_SIZE, BUTTON_SIZE));
}

void Graph3DBase::TransformProjection(double /*aspectRate*/) const
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	double const w = 22;
	double const h = 22;

	glOrtho(-w, w, -h, h, -300, 300);
}

void Graph3DBase::AutoScaleAxes()
{
	if (!GetDataCount()) { return; }

	int count = 0;
	for (size_t i = 0; i < m_axes.size(); ++i) {
		IPlotData* data = GetData(0);
		if (!data) { continue; }
		if (data->GetFileId((long)i) >= 0) {
			++count;
		}
	}
	for (int i = 0; i < count; ++i) {
		// ベクトル図のT軸(カラーバー)のオートスケールは特殊
		if (m_dimension == 6 && dynamic_cast<TAxis*>(m_axes[i]))
		{
			AutoScaleVectorColorBarAxis(m_axes[i]);
		}
		else
		{
			AutoScaleAxis(m_axes[i]);
		}
	}
	AfterAutoScaleAxes();
}

void Graph3DBase::AutoScaleAxis(Axis* axis)
{
	double max = -FLT_MAX, min = FLT_MAX;
	for (size_t i = 0; i < GetDataCount(); ++i) {
		IPlotData* data = GetData(i);
		if (!data) { continue; }
		int const dim = std::min<int>(m_dimension, axis->GetDimension());
		for (size_t i = 0, count = data->GetDataCount(); i < count; ++i) {
			double const v = data->GetData(dim, i);
			if (v == FLT_MAX) { continue; }
			max = std::max<double>(max, v);
			min = std::min<double>(min, v);
		}
	}
	axis->CalcAutoScale(min, max);
}

void Graph3DBase::AutoScaleVectorColorBarAxis(Axis* axis)
{
	// ベクトル図(立体)のカラーバーは通常の軸のオートスケールとは異なる
	double max = -FLT_MAX, min = FLT_MAX;
	for (size_t i = 0; i < GetDataCount(); ++i) {
		IPlotData* data = GetData(i);
		if (!data) { continue; }
		for (size_t i = 0, count = data->GetDataCount(); i < count; ++i) {
			double const vx = data->GetData(3, i);
			double const vy = data->GetData(4, i);
			double const vz = data->GetData(5, i);
			if (vx == FLT_MAX || vy == FLT_MAX || vz == FLT_MAX) { continue; }
			double const vectorLength = std::sqrt((vx * vx) + (vy * vy) + (vz * vz));
			max = std::max<double>(max, vectorLength);
			min = std::min<double>(min, vectorLength);
		}
	}
	axis->CalcAutoScale(min, max);
}

void Graph3DBase::AutoLabelAxes()
{
	for (size_t i = 0; i < m_axes.size(); ++i) {
		// ベクトル図のT軸(カラーバー)のオートラベルは特殊
		if (m_dimension == 6 && dynamic_cast<TAxis*>(m_axes[i]))
		{
			AutoVectorColorBarAxisLabel(m_axes[i]);
		}
		else
		{
			AutoLabel(m_axes[i]);
		}
	}
}

void Graph3DBase::AutoLabel(Axis* axis)
{
	if (!axis->IsAutoLabel()) { return; }

	MakeLabel(axis);
}

void Graph3DBase::AutoVectorColorBarAxisLabel(Axis* axis)
{
	if (!axis->IsAutoLabel()) { return; }

	// ベクトル図(立体)の場合は「ベクトルレベル」で固定
	std::wstring name = GetResourceString(m_owner,"ID_SOURCE_TEXT_VECTOR_LEVEL");
	axis->SetLabel(name);
}

void Graph3DBase::MakeLabel(Axis* axis)
{
	int dimension = axis->GetDimension();

	std::vector<std::wstring> nameCache;
	for (size_t i = 0; i < GetDataCount(); ++i) {
		IPlotData* data = GetData(i);
		if (!data) { continue; }
		std::wstring name = data->GetItemName(dimension);
		std::wstring unit = GetData(0)->GetUnitName(dimension);

		SIUnitLabel siLabel = axis->GetSIUnit();
		if(siLabel != SIUnitLabel::OFF && axis->GetScaleType() == ST_LINEAR){
			unit = GetSIUnitString(siLabel) + unit;
		}

		if (!unit.empty()) {
			name = boost::str(boost::wformat(_T("%1% [%2%]")) % name % unit);
		}

		if (boost::range::find(nameCache, name) == nameCache.end()) {
			nameCache.push_back(name);
		}
	}

	std::wstring name;
	for (size_t i = 0; i < nameCache.size(); ++i) {
		name += nameCache[i];
		if (i + 1 < nameCache.size()) {
			name += _T(", ");
		}
	}

	axis->SetLabel(name);
}

void Graph3DBase::ToggleWireFrameVisible()
{
	SetViewWireframe(!GetViewWireframe());
}

void Graph3DBase::ToggleHideLine()
{
	EnableHiddenLine(!IsHiddenLine());
}

void Graph3DBase::ToggleColoring()
{
	EnableColoring(!IsColoring());
}

void Graph3DBase::ToggleFilling()
{
	EnableFilling(!IsFilling());
}

void Graph3DBase::SetXYPosition()
{
	Rotate(90, 0, 0);
}
void Graph3DBase::SetYZPosition()
{
	Rotate(0, -90, 0);
}
void Graph3DBase::SetZXPosition()
{
	Rotate(0, 0, 0);
}

void Graph3DBase::ToggleColorBarVisible()
{
	m_viewColorBar = !m_viewColorBar;
}
void Graph3DBase::ToggleGuideVisible()
{
	m_viewAxisGuide = !m_viewAxisGuide;
}

void Graph3DBase::CursorChanged(std::vector<OCPoint> const& xLine, std::vector<OCPoint> const& yLine)
{
	std::vector<OCPoint> xData, yData;
	for (size_t i = 0; i < xLine.size(); ++i) {
		xData.push_back(OCPoint(
			m_xSection->GetXAxis()->CoordinateToValue(xLine[i].x),
			m_xSection->GetYAxis()->CoordinateToValue(xLine[i].y)));
	}
	for (size_t i = 0; i < yLine.size(); ++i) {
		yData.push_back(OCPoint(
			m_ySection->GetXAxis()->CoordinateToValue(yLine[i].x),
			m_ySection->GetYAxis()->CoordinateToValue(yLine[i].y)));
	}

	m_xSection->SetData(xData, m_cursorValue[0]);
	m_ySection->SetData(yData, m_cursorValue[1]);
}

void Graph3DBase::BeginDrag()
{
	m_dragging = true;
}
void Graph3DBase::EndDrag()
{
	m_dragging = false;
}

#if 1

#include "IMessageReceiver.h"
#include "Message.h"
#include "IFile.h"
#include "IFilter.h"
#include "property.h"
#include "PropertyHelper.hpp"

void Graph3DBase::SetDimension(int const dim)
{
	m_dimension = dim;

	// ベクトル図のT軸(カラーバー)のオートスケール、オートラベルは特殊なので設定し直す
	m_tAxis->AutoScaleChanged.disconnect_all_slots();
	m_tAxis->AutoLabelChanged.disconnect_all_slots();

	if (m_dimension == 6)
	{
		m_tAxis->AutoScaleChanged.connect(boost::bind(&Graph3DBase::AutoScaleVectorColorBarAxis, this, _1));
		m_tAxis->AutoLabelChanged.connect(boost::bind(&Graph3DBase::AutoVectorColorBarAxisLabel, this, _1));
	}
	else
	{
		m_tAxis->AutoScaleChanged.connect(boost::bind(&Graph3DBase::AutoScaleAxis, this, _1));
		m_tAxis->AutoLabelChanged.connect(boost::bind(&Graph3DBase::AutoLabel, this, _1));
	}
}

void AddColorPattenProperty(IMessageReceiver* owner, IPropertyComposite* parent, bool const locked, ColorTexture* texture)
{
	IPropertyGroup* group = parent->AddGroup(::GetResourceString(owner,"ID_SOURCE_TEXT_GRADATION"));
	group->AddCustomDialogProperty(::GetResourceString(owner,"ID_SOURCE_TEXT_COLOR_PATTERN"), PI_COLOR_PATTERN,
		ToWideString(texture->GetCurrentPatternName()).c_str(), 0, locked);
}

//! 使用可能なファイル名の一覧を取得します(プロパティでのみ使用)
std::vector<std::wstring> GetFiles(IMessageReceiver* owner, IFile* file, long* current)
{
	std::vector<std::wstring> files;
	long n = 0, index = 0;
	while (IFile* tmpFile = reinterpret_cast<IFile*>(owner->SendMessage(PI_MSG_GET_FILE, n++, 0))) {
		std::wstring const& fileName = tmpFile->GetFileName();
		wchar_t buff[_MAX_PATH * 2] = {};
		::_sntprintf_s(buff, _countof(buff), _T("%d:%s"), tmpFile->GetId(), fileName.c_str());

		files.push_back(buff);
		if (file == tmpFile) {
			index = n - 1;
		}
	}
	*current = index;
	return files;
}

void AddFileProperty(IPropertyGroup* group, IMessageReceiver* owner, IPlotData* data, bool const locked)
{
	IFile* file = reinterpret_cast<IFile*>(owner->SendMessage(PI_MSG_GET_FILE_FROMID, data->GetFileId(0), 0));
	if (!file) { return; }

	long index = 0;
	std::vector<std::wstring> files = GetFiles(owner, file, &index);
	group->AddArrayProperty(::GetResourceString(owner,"ID_SOURCE_TEXT_FILE"), PI_FILE_INDEX, locked, index, files);
}

void AddDataProperty(IPropertyComposite* parent, IMessageReceiver* owner, IPlotData* data, int dim, bool const locked, bool use_file)
{
	IFile* file = reinterpret_cast<IFile*>(owner->SendMessage(PI_MSG_GET_FILE_FROMID, data->GetFileId(0), 0));
	if (!file) { return; }

	IPropertyGroup* grp = parent->AddGroup(::GetResourceString(owner,"ID_SOURCE_TEXT_DATA"));

	if (use_file) {
		AddFileProperty(grp, owner, data, locked);
	}

	// データ項目
	std::vector<std::wstring> names;
	for (size_t i = 0; i < file->ItemCount(); ++i) {
		names.push_back(file->Item(i)->GetName());
	}

	grp->AddArrayProperty(::GetResourceString(owner,"ID_SOURCE_TEXT_X_DATA"), PI_XDATA_INDEX, locked, data->GetItemId(0), names);
	grp->AddArrayProperty(::GetResourceString(owner,"ID_SOURCE_TEXT_Y_DATA"), PI_YDATA_INDEX, locked, data->GetItemId(1), names);
	grp->AddArrayProperty(::GetResourceString(owner,"ID_SOURCE_TEXT_Z_DATA"), PI_ZDATA_INDEX, locked, data->GetItemId(2), names);
	if (dim == 4) {
		grp->AddArrayProperty(::GetResourceString(owner,"ID_SOURCE_TEXT_T_DATA"), PI_TDATA_INDEX, locked, data->GetItemId(3), names);
	}
	else if (dim == 6) {
		grp->AddArrayProperty(::GetResourceString(owner,"ID_SOURCE_TEXT_VX_DATA"), PI_VXDATA_INDEX, locked, data->GetItemId(3), names);
		grp->AddArrayProperty(::GetResourceString(owner,"ID_SOURCE_TEXT_VY_DATA"), PI_VYDATA_INDEX, locked, data->GetItemId(4), names);
		grp->AddArrayProperty(::GetResourceString(owner,"ID_SOURCE_TEXT_VZ_DATA"), PI_VZDATA_INDEX, locked, data->GetItemId(5), names);
	}

	size_t filterCount = data->GetFilter() ? data->GetFilter()->GetConditionCount() : 0;
	grp->AddCustomDialogProperty(::GetResourceString(owner,"ID_SOURCE_TEXT_FILTER_CONDITION"), PI_DATA_FILTER, boost::lexical_cast<std::wstring>((unsigned int)filterCount).c_str(), 0, locked);
}

void Graph3DBase::AddDataProperty(IPropertyComposite* parent, bool const locked) const
{
	::AddDataProperty(parent, m_owner, GetData(0), m_dimension, locked, true);
}

void Graph3DBase::AddShowProperty(IPropertyComposite* parent, bool const locked) const
{
	if (IPropertyGroup* g = parent->AddGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_SOLID_GRAPH"))) {
		g->AddProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_ANGLE_X"), PI_3D_ANGLE_PITCH, m_pitch, locked);
		g->AddProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_ANGLE_Z"), PI_3D_ANGLE_YAW, m_yaw, locked);
#ifdef _DEBUG
		g->AddProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_DEBUG_ANGLE_Y"), PI_3D_ANGLE_ROLL, m_roll, locked);
#endif
		g->AddProperty(::GetResourceString(m_owner,"ID_SOURCE_TEXT_ZOOM"), PI_3D_ANGLE_SCALE, m_scale * 100, locked);
	}
}

void Graph3DBase::AddProperty(OCProperty* prop, bool const locked) const
{
	AddPropertyImpl(prop, locked);

	m_xAxis->AddProperty(prop->CreateGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_X_AXIS")), 0, locked);
	m_yAxis->AddProperty(prop->CreateGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_Y_AXIS")), PI_PROPERTY_MAX, locked);
	m_zAxis->AddProperty(prop->CreateGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_Z_AXIS")), PI_PROPERTY_MAX * 2, locked);
	if (m_dimension == 4) {
		m_tAxis->AddProperty(prop->CreateGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_T_AXIS")), PI_PROPERTY_MAX * 3, locked);
	}
}

void Graph3DBase::AddTabProperty(OCProperty* prop, bool const locked) const
{
	AddTabPropertyImpl(prop, locked);

	m_xAxis->AddProperty(prop->CreateGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_X_AXIS"))->AddGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_X_AXIS")), 0, locked);
	m_yAxis->AddProperty(prop->CreateGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_Y_AXIS"))->AddGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_Y_AXIS")), PI_PROPERTY_MAX, locked);
	m_zAxis->AddProperty(prop->CreateGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_Z_AXIS"))->AddGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_Z_AXIS")), PI_PROPERTY_MAX * 2, locked);
	if (m_dimension == 4) {
		m_tAxis->AddProperty(prop->CreateGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_T_AXIS"))->AddGroup(::GetResourceString(m_owner,"ID_SOURCE_TEXT_T_AXIS")), PI_PROPERTY_MAX * 3, locked);
	}
}

bool Graph3DBase::SetProperty(long id, LPARAM value, LPARAM* source)
{
	switch (id)
	{
	case PI_FILE_INDEX:
		{
			if (IFile* file = GetFileByIndex((int)value)) {
				long index = 0;
				::SetPropertyHelper(index, value, source);
				GetData(0)->ChangeFile(file->GetId());
				GetData(0)->LinkDataSet();
				SetData(GetData(0)); // 軸スケールを更新
			}
		}
		return true;
	case PI_XDATA_INDEX:
		SetDataProperty(GetData(0)->GetItemId(0), 0, value, source);
		return true;
	case PI_YDATA_INDEX:
		SetDataProperty(GetData(0)->GetItemId(1), 1, value, source);
		return true;
	case PI_ZDATA_INDEX:
		SetDataProperty(GetData(0)->GetItemId(2), 2, value, source);
		return true;
	case PI_TDATA_INDEX:
		SetDataProperty(GetData(0)->GetItemId(3), 3, value, source);
		return true;
	case PI_DATA_FILTER:
		{
			IFilter* filter = reinterpret_cast<IFilter*>(value);
			GetData(0)->SetFilter(boost::shared_ptr<IFilter>(filter));
			SetData(GetData(0)); // 軸スケールを更新
		}
		return true;
	case PI_3D_COLORBAR_VISIBLE:
		::SetPropertyHelper(m_viewColorBar, value, source);
		break;
	case PI_3D_WIREFRAME_VISIBLE:
		{
			BOOL cur = GetViewWireframe();
			::SetPropertyHelper(cur, value, source);
			SetViewWireframe(!!cur);
		}
		break;
	case PI_DATA_CLIPPING:
		::SetPropertyHelper(m_clipMaxX, value, source);
		m_clipMinX = m_clipMaxZ = m_clipMinZ = m_clipMaxY = m_clipMinY = m_clipMaxX;
		return true;
	case PI_3D_ANGLE_PITCH:
		if (!::SetPropertyHelper(m_pitch, value, source, -180.0, 180.0)) {
			std::wstring msg = ::GetResourceString(m_owner,"ID_SOURCE_TEXT_ENTER_ANGLE_IN_RANGE");
			::MessageBox(m_wnd, msg.c_str(), ::GetResourceString(m_owner,"ID_SOURCE_TEXT_O_CHART"), MB_OK|MB_ICONWARNING);
			return false;
		}
		return true;
	case PI_3D_ANGLE_YAW:
		if (!::SetPropertyHelper(m_yaw, value, source, -180.0, 180.0)) {
			std::wstring msg = ::GetResourceString(m_owner,"ID_SOURCE_TEXT_ENTER_ANGLE_IN_RANGE");
			::MessageBox(m_wnd, msg.c_str(), ::GetResourceString(m_owner,"ID_SOURCE_TEXT_O_CHART"), MB_OK|MB_ICONWARNING);
			return false;
		}
		return true;
	case PI_3D_ANGLE_ROLL:
		if (!::SetPropertyHelper(m_roll, value, source, -180.0, 180.0)) {
			std::wstring msg = ::GetResourceString(m_owner,"ID_SOURCE_TEXT_ENTER_ANGLE_IN_RANGE");
			::MessageBox(m_wnd, msg.c_str(), ::GetResourceString(m_owner,"ID_SOURCE_TEXT_O_CHART"), MB_OK|MB_ICONWARNING);
			return false;
		}
		return true;
	case PI_3D_ANGLE_SCALE:
		if (!::SetPropertyHelper(m_scale, value, source, 10.0, 500.0)) {
			std::wstring msg = ::GetResourceString(m_owner,"ID_SOURCE_TEXT_ENTER_SCALE_F_RANGE");
			::MessageBox(m_wnd, msg.c_str(), ::GetResourceString(m_owner,"ID_SOURCE_TEXT_O_CHART"), MB_OK|MB_ICONWARNING);
			return false;
		}
		m_scale /= 100.0;
		return true;
	}

	if (m_xAxis->SetProperty(id, value, source)
		|| m_yAxis->SetProperty(id - PI_PROPERTY_MAX, value, source))
	{
		return true;
	}

	if (m_zAxis->SetProperty(id - PI_PROPERTY_MAX * 2, value, source)) {
		if (m_dimension == 3) {
			m_tAxis->SetProperty(id - PI_PROPERTY_MAX * 2, value, source);
		}
		return true;
	}

	if (m_tAxis->SetProperty(id - PI_PROPERTY_MAX * 3, value, source)){
		return true;
	}

	return SetPropertyImpl(id, value, source);
}

bool Graph3DBase::GetProperty(long id, LPARAM* ret) const
{
	if (m_xAxis->GetProperty(id, ret)
		|| m_yAxis->GetProperty(id - PI_PROPERTY_MAX, ret)
		|| m_zAxis->GetProperty(id - PI_PROPERTY_MAX * 2, ret)
		|| m_tAxis->GetProperty(id - PI_PROPERTY_MAX * 3, ret))
	{
		return true;
	}

	return GetPropertyImpl(id, ret);
}

void Graph3DBase::SetDataProperty(int current, int dim, LPARAM value, LPARAM* source)
{
	::SetPropertyHelper(current, value, source);
	GetData(0)->SetLink(dim, GetData(0)->GetFileId(0), current);
	GetData(0)->LinkDataSet();

	// Y軸とZ軸のデータが空の時に、Y->Zと項目を割り当てていくと、
	// Y軸にデータを割り当てた時点ではZ軸が空のためオートスケールがかからない
	// そういった場合のために、すべての軸に更新をかける
	SetData(GetData(0)); // 非効率なので状態変数を入れた方が良い
}

#include "ISerializer.h"

void SerializeColorPattern(ISerializer* ar, ColorTexture* colorPattern, IMessageReceiver* owner) // free function
{
	if (!ar) { return; }

	if (IsReadable(ar)) {
		// 旧バージョンのプロジェクトにはパターン名しかないので
		// その名前をもとに復元する
		std::string name;
		SerializeString(ar, _T("OldPatternName"), name);

		if (!name.empty()) {
			// 2.03 -> 3.00 になるときに名前が変わった
			if (name == ToMultiByteString(::GetResourceString(owner, "ID_SOURCE_TEXT_MAGENTA_VER_203"))) {
				name = ToMultiByteString(::GetResourceString(owner, "ID_SOURCE_TEXT_MAGENTA"));
			}
			else if (name ==ToMultiByteString( ::GetResourceString(owner, "ID_SOURCE_TEXT_RAINBOW_1_VER_203"))) {
				name = ToMultiByteString(::GetResourceString(owner, "ID_SOURCE_TEXT_RAINBOW_1"));
			}
			else if (name == ToMultiByteString(::GetResourceString(owner, "ID_SOURCE_TEXT_RAINBOW_2_VER_203"))) {
				name = ToMultiByteString(::GetResourceString(owner, "ID_SOURCE_TEXT_RAINBOW_2"));
			}

			colorPattern->SetOldPatternName(name);
		}
		else {
			std::string pattern;
			SerializeString(ar, _T("PatternFileName"), pattern);
			colorPattern->SetPatternFileName(ToWideString(pattern));
		}
	}
	else {
		std::string pattern = colorPattern->GetPatternFileName(colorPattern->GetCurrentPattern());
	    SerializeString(ar, _T("PatternFileName"), pattern);
	}
}

void Graph3DBase::Serialize(ISerializer* ar)
{
	m_serializing = true;

	ar->Serialize(_T("XAngle"), m_pitch);
	ar->Serialize(_T("YAngle"), m_roll);
	ar->Serialize(_T("ZAngle"), m_yaw);
	ar->Serialize(_T("Scale"), m_scale);
	ar->Serialize(_T("XPosition"), m_h);
	ar->Serialize(_T("YPosition"), m_v);

	ar->Serialize(_T("ColorBar"), m_viewColorBar);
	ar->Serialize(_T("Guide"), m_viewAxisGuide);

	if (IsReadable(ar)) {
#define READ_SET(name, setter) { bool b; ar->Serialize(name, b); setter(b); }
		READ_SET(_T("HideLine"), EnableHiddenLine);
		READ_SET(_T("Lighting"), EnableLighting);
		READ_SET(_T("Clipping"), EnableClipping);
	}
	else {
#define WRITE_SET(name, getter) { bool b = getter(); ar->Serialize(name, b); }
		WRITE_SET(_T("HideLine"), IsHiddenLine);
		WRITE_SET(_T("Lighting"), IsLighting);
		WRITE_SET(_T("Clipping"), IsClipping);
	}

	//MAKE_ENUM_SERIALIZE2(ar, _T("SectionMode"), m_sectionMode, SectionKind, Hidden);
	ar->Serialize(_T("VerticalSection"), m_verticalSection);
	ar->Serialize(_T("HorizontalSection"), m_horizontanlSection);

	m_xAxis->Serialize(ar->ChildNode(_T("XAxis")));
	m_yAxis->Serialize(ar->ChildNode(_T("YAxis")));
	m_zAxis->Serialize(ar->ChildNode(_T("ZAxis")));
	m_tAxis->Serialize(ar->ChildNode(_T("TAxis")));

	//if (IReadable* reader = dynamic_cast<IReadable*>(ar)) {
	//	bool plotVisible = true;
	//	reader->Serialize(_T("PlotVisible"), plotVisible, true);
	//	SetPlotKind(m_kind);
	//	m_plot->SetVisible(plotVisible);

	//	if (IReadable* vectorNode = dynamic_cast<IReadable*>(reader->ChildNode(_T("VectorMap")))) {
	//		CreateVectorMap();
	//		m_vector->Serialize(vectorNode);
	//	}
	//}
	//else {
	//	if (m_vector.get()) {
	//		bool b = m_plot->IsVisible();
	//		ar->Serialize(_T("PlotVisible"), b);
	//		
	//		m_vector->Serialize(ar->ChildNode(_T("VectorMap")));
	//	}
	//}

	//TODO: 選択されたマーカーの位置は復元出来る必要があるか?

	SerializeImpl(ar);

	if (IReadable* reader = dynamic_cast<IReadable*>(ar)) {
		if ((reader->GetMajorVersion() == 3 && reader->GetMinorVersion() < 2) ||
			reader->GetMajorVersion() == 2)
		{
			// 3.1以前のOCPの復元
			if (GetTypeId() == 28/*ウォーターフォール*/) {
				m_scale = 1;
			}
			else {
				m_pitch = AdjustAngle(m_pitch + 90);
			}
		}

		SetData(GetData(0));

		if (reader->IsTemplate()) {
			for (size_t i = 0; i < GetDataCount(); ++i) {
				for (long j = 0; j < 6; ++j) {
					GetData(i)->SetLink(j, -1);
				}
			}
		}
	}

	//if (IReadable* reader = dynamic_cast<IReadable*>(ar)) {
	//	// 互換性のための処理
	//	// 3.1.4.0以前は立体グラフのサイズに断面が含まれていなかった
	//	if (reader->GetMajorVersion() == 3 &&
	//		reader->GetMinorVersion() == 1 &&
	//		reader->GetReleaseVersion() < 4)
	//	{
	//		if (EnableVerticalSection()) {
	//			m_rect.right += PxToMM(GetSectionSize());
	//		}
	//		if (EnableHorizontalSection()) {
	//			m_rect.bottom += PxToMM(GetSectionSize());
	//		}
	//	}
	//}

	m_serializing = false;
}

Graph3DBase* Graph3DBase::Clone() const
{
	return Create();
}

void Graph3DBase::DoCommand(UINT const commandId)
{
	message_handler_t::iterator it = m_messageHandler.find(commandId);
	if (it != m_messageHandler.end()) {
		it->second();
	}
}

void Graph3DBase::GetCommandState(UINT id, BOOL* enable, BOOL* checked) const
{
	state_getter_handler_t::const_iterator it = m_stateGetter.find(id);
	if (it != m_stateGetter.end()) {
		(it->second)(enable, checked);
	}
}

bool Graph3DBase::HasCommand(UINT commandId)
{
	message_handler_t::iterator it = m_messageHandler.find(commandId);
	return it != m_messageHandler.end();
}

IFile* Graph3DBase::GetFile() const
{
	if (IPlotData* data = GetData(0)) {
		return reinterpret_cast<IFile*>(m_owner->SendMessage(PI_MSG_GET_FILE_FROMID, data->GetFileId(0), 0));
	}
	return nullptr;
}

IFile* Graph3DBase::GetFileByIndex(int index) const
{
	return reinterpret_cast<IFile*>(m_owner->SendMessage(PI_MSG_GET_FILE, index, 0));
}

IFilter* Graph3DBase::GetFilter() const
{
	return GetData(0)->GetFilter();
}

#endif

void Graph3DBase::OnPropertyUpdated(bool all_update)
{
	PropertyUpdated(all_update);
}
