/*!	@file
*/
#pragma once

#include <windows.h>
#include <wtl/atldlgs.h>
#include <wtl/atlmisc.h>
#include <wtl/atlcrack.h>
#include "resource.h"

extern HINSTANCE dllInstance;

//! 埋め込み式エディタ
template<class Graph>
class CController : public ATL::CDialogImpl<CController<Graph>>
{
public:
	boost::signals2::signal<double(void)> GetZoomRatio;
	boost::signals2::signal<void(double*, double*, double*, double*)> GetRect;


	enum { IDD = IDD_CONTRROLLER };

	enum Mode
	{
		Rotate,
		Zoom,
		Move,
		Cursor,
	};

	BEGIN_MSG_MAP_EX(CController)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		MSG_WM_PAINT(OnPaint)
		MSG_WM_ERASEBKGND(OnEraseBkgnd)
		MSG_WM_LBUTTONDOWN(OnLButtonDown)
		MSG_WM_MOUSEMOVE(OnMouseMove)
		MSG_WM_LBUTTONUP(OnLButtonUp)
		MSG_WM_RBUTTONDOWN(OnRButtonDown)
		MSG_WM_RBUTTONUP(OnRButtonUp)
		MSG_WM_KEYDOWN(OnKeyDown)
		MSG_WM_SHOWWINDOW(OnShowWindow)
		MSG_WM_SETCURSOR(OnSetCursor)
	END_MSG_MAP()

	explicit CController(Graph* graph)
		: m_graph(graph)
		, m_mouseDown(false), m_mode(Rotate)
		, m_draggable(false)
		, m_leftClickSelect(false)
	{
	}

	void EnableLeftClickSelect(bool enable)
	{
		m_leftClickSelect = enable;
	}

	void EnableDragging(bool enable)
	{
		m_draggable = enable;
	}

	void GetBitmap(WTL::CBitmap& img)
	{
		WTL::CDC memDC;
		memDC.CreateCompatibleDC();

		GetBitmap(memDC, img);

		memDC.DeleteDC();
	}

	void GetBitmap(WTL::CDC& memDC, WTL::CBitmap& img)
	{
		OCRect temp_r;
		//GetClientRect(&r);
		GetRect(&temp_r.left, &temp_r.top, &temp_r.right, &temp_r.bottom);
		OCRect r = MMToPx(temp_r);

		BITMAPINFOHEADER bmpInfo = {sizeof(BITMAPINFOHEADER)};
		bmpInfo.biWidth = static_cast<long>(r.Width());
		bmpInfo.biHeight = static_cast<long>(r.Height());
		bmpInfo.biPlanes = 1;
		bmpInfo.biBitCount = 24;
		bmpInfo.biCompression = BI_RGB;

		void* data = 0;
		HBITMAP bmp = img.CreateDIBSection(memDC.m_hDC, reinterpret_cast<BITMAPINFO*>(&bmpInfo), DIB_RGB_COLORS, &data, 0, 0);
		if (bmp) {
			HBITMAP oldBmp = memDC.SelectBitmap(bmp);
			m_graph->Capture(memDC.m_hDC,  static_cast<int>(r.Width()),  static_cast<int>(r.Height()));
			memDC.SelectBitmap(oldBmp);
		}
	}

	Mode GetMode() const { return m_mode; }
	void SetMode(Mode mode)
	{
		m_mode = mode;

#define LoadImageH(id) reinterpret_cast<HCURSOR>(::LoadImage(dllInstance, MAKEINTRESOURCE(id), IMAGE_CURSOR, 32, 32, LR_SHARED))

		switch (mode)
		{
		case Rotate:
			m_cursor = LoadImageH(IDC_CURSOR_ROTATE);
			break;
		case Move:
			m_cursor = LoadImageH(IDC_CURSOR_MOVE);
			break;
		case Zoom:
			m_cursor = LoadImageH(IDC_CURSOR_SCALING);
			break;
		case Cursor:
			m_cursor = LoadImageH(IDC_CURSOR_SCURSOR);
			break;
		}

#undef LoadImageH
	}

private:
	LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
	{
		return FALSE;
	}

	LRESULT OnShowWindow(BOOL visible, int /*lParam*/)
	{
		if (visible) {
			SetFocus();
			m_dc = GetDC();
		}
		else {
			ReleaseDC(m_dc);
			m_dc = 0;
		}
		return TRUE;
	}

	void OnPaint(HDC /*dc*/)
	{
		if (!m_dc) { return; }

		OCRect temp_r;
		//GetClientRect(&r);
		GetRect(&temp_r.left, &temp_r.top, &temp_r.right, &temp_r.bottom);
		OCRect r = MMToPx(temp_r);

		// グラフの後ろに半透明のウィンドウ(Layerd window)があると
		// テキストがちらつく問題があるため、一度ビットマップに描いてから転送する
		// パフォーマンスが気になっていたが、意外と遅くない?
		WTL::CDC memDC;
		memDC.CreateCompatibleDC();
		WTL::CBitmap bmp;
		GetBitmap(bmp);
		HBITMAP oldBmp = memDC.SelectBitmap(bmp.m_hBitmap);
		double const ratio = *GetZoomRatio();
		int const width = static_cast<int>( r.Width() * ratio );
		int const height = static_cast<int>( r.Height() * ratio );
		::StretchBlt(m_dc, -1, -1, width, height, memDC.m_hDC, 0, 0, static_cast<int>(r.Width()), static_cast<int>(r.Height()), SRCCOPY);
		memDC.SelectBitmap(oldBmp);
		memDC.DeleteDC();

		SetMsgHandled(FALSE); // これがないとツールチップが出なくなる
	}

	BOOL OnEraseBkgnd(HDC /*dc*/)
	{
		// 背景は描かない
		return TRUE;
	}

	void OnLButtonDown(UINT /*flags*/, WTL::CPoint point)
	{
		WTL::CPoint zoomPoint;
		zoomPoint.x = static_cast<long>(point.x / *GetZoomRatio());
		zoomPoint.y = static_cast<long>(point.y / *GetZoomRatio());
		m_click = zoomPoint;
		m_mouseDown = true;

		m_graph->BeginDrag();

		bool hitMarker = false;
		if (m_mode == Cursor) {
			m_graph->SetCursor(zoomPoint.x, zoomPoint.y);
		}
		else if (m_leftClickSelect) {
			hitMarker = PickUp(zoomPoint);
		}

		if (!hitMarker) {
			m_graph->Click(zoomPoint.x, zoomPoint.y);
		}

		SetFocus();
		SetCapture();
	}

	void OnMouseMove(UINT /*flags*/, WTL::CPoint point)
	{
		if (m_mouseDown) {
			CRect r;
			GetClientRect(&r);
			WTL::CPoint zoomPoint;
			zoomPoint.x = static_cast<long>(point.x / *GetZoomRatio());
			zoomPoint.y = static_cast<long>(point.y / *GetZoomRatio());

			if (m_draggable && ::GetAsyncKeyState(VK_SHIFT) &&
				m_graph->DragMarker(OCPoint(zoomPoint.x - m_click.x, m_click.y - zoomPoint.y))) {
			}
			else {
				switch (m_mode)
				{
				case Rotate:
					{
						double y = 360.0f * (static_cast<double>(zoomPoint.x - m_click.x) / r.Width());
						double x = 360.0f * (static_cast<double>(m_click.y - zoomPoint.y) / r.Height());
						m_graph->OffsetRotate(-x, y, 0);
					}
					break;
				case Zoom:
					{
						double offset = (zoomPoint.y > m_click.y) ? -0.08 : 0.08;
						m_graph->OffsetScale(offset);
					}
					break;
				case Move:
					{
						double xOffset = (double)(zoomPoint.x - m_click.x) / 10;
						double yOffset = -((double)(zoomPoint.y - m_click.y) / 10);
						m_graph->OffsetTranslate(xOffset, yOffset);
					}
					break;
				case Cursor:
					m_graph->SetCursor(zoomPoint.x, zoomPoint.y);
					break;
				}
			}
			m_click = zoomPoint;
			Invalidate(FALSE);
			UpdateWindow();
		}
		else {
			if (m_leftClickSelect) {
				HDC dc = GetDC();
				if (m_graph->HitTestMarker(dc, point.x, point.y)) {
					SetCursor(m_cursor = ::LoadCursor(NULL, IDC_HAND));
				}
				else {
					SetMode(m_mode);
				}
				ReleaseDC(dc);
			}
		}
	}
	void OnLButtonUp(UINT /*flags*/, WTL::CPoint /*point*/)
	{
		ReleaseCapture();

		m_graph->EndDrag();

		if (m_draggable) {
			m_graph->EndMarkerDrag();
		}

		Invalidate(FALSE);
		m_mouseDown = false;
	}

	void OnRButtonDown(UINT /*flags*/, WTL::CPoint point)
	{
		WTL::CPoint zoomPoint;
		zoomPoint.x = static_cast<long>(point.x / *GetZoomRatio());
		zoomPoint.y = static_cast<long>(point.y / *GetZoomRatio());

		if (!m_leftClickSelect) {
			PickUp(zoomPoint);
			Invalidate(FALSE);
			UpdateWindow();
		}
		else {
			CPoint pt = zoomPoint;
			ClientToScreen(&pt);
			WPARAM wParam = reinterpret_cast<WPARAM>(m_hWnd);
			LPARAM lParam = MAKELPARAM(pt.x, pt.y);
			GetParent().SendMessage(WM_CONTEXTMENU, wParam, lParam);
		}
	}

	bool PickUp(WTL::CPoint const& point)
	{
		HDC dc = GetDC();
		bool const ret = m_graph->PickUpData(dc, point.x, point.y);
		ReleaseDC(dc);

		return ret;
	}

	void OnRButtonUp(UINT /*flags*/, WTL::CPoint /*point*/)
	{
	}

	void OnKeyDown(TCHAR key, UINT /*repeats*/, UINT /*code*/)
	{
		switch (key)
		{
		case VK_LEFT:
		case VK_RIGHT:
		case VK_UP:
		case VK_DOWN:
			switch (m_mode)
			{
			case Rotate:
				{
					double y = 360.0f * (key == VK_UP || key == VK_DOWN) ? 0 : key == VK_LEFT ? -1 : 1;
					double x = 360.0f * (key == VK_LEFT || key == VK_RIGHT) ? 0 : key == VK_DOWN ? -1 : 1;
					m_graph->OffsetRotate(-x, y, 0);
					Invalidate(FALSE);
					UpdateWindow();
				}
				break;
			case Zoom:
				if (key == VK_UP || key == VK_DOWN)
				{
					double offset = (key == VK_UP) ? -0.08 : 0.08;
					m_graph->OffsetScale(offset);
					Invalidate(FALSE);
					UpdateWindow();
				}
				break;
			case Move:
				{
					double const offset = 0.5;
					double xOffset = (key == VK_UP || key == VK_DOWN) ? 0 : key == VK_LEFT ? -offset : offset;
					double yOffset = (key == VK_LEFT || key == VK_RIGHT) ? 0 : key == VK_DOWN ? -offset : offset;
					m_graph->OffsetTranslate(xOffset, yOffset);
					Invalidate(FALSE);
					UpdateWindow();
				}
				break;
			case Cursor:
				{
					int index, line;
					m_graph->GetCursorIndex(&index, &line);
					switch (key)
					{
					case VK_UP:
						++index;
						break;
					case VK_DOWN:
						--index;
						break;
					case VK_LEFT:
						line -= m_graph->KeyDownCursorOffset();
						break;
					case VK_RIGHT:
						line += m_graph->KeyDownCursorOffset();
						break;
					}
					m_graph->SelectIndex(index, line);
					Invalidate(FALSE);
					UpdateWindow();
				}
				break;
			}
			break;
		}
	}

	LRESULT OnSetCursor(HWND, UINT, UINT)
	{
		SetCursor(m_cursor);
		return 0;
	}

private:
	Graph* m_graph;
	Mode m_mode;

	HDC m_dc;
	HCURSOR m_cursor;

	bool m_draggable;
	bool m_mouseDown;
	WTL::CPoint m_click;

	// option
	bool m_leftClickSelect; //!< 左クリックでマーカーを選択するかどうか
};