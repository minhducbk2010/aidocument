#pragma once

#include <boost/noncopyable.hpp>

class Selector : public boost::noncopyable
{
public:
	Selector(int x, int y);
	~Selector();

	int GetPoint();

private:
	int SelectHits(GLuint hits);

private:
	static int const BUFSIZE = 512;
	GLuint m_selectBuffer[BUFSIZE];
};
