//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by GlGraph.rc
//
#define VS_VERSION_INFO                 1
#define IDD_CONTRROLLER                 11000
#define IDC_CURSOR_ROTATE               11001
#define IDC_CURSOR_MOVE                 11002
#define IDC_CURSOR_SCALING              11003
#define IDC_CURSOR_SCURSOR              11004

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
