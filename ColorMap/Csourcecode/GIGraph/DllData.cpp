/*!	@file
	@date	2008-01-16
*/
#include "StdAfx.h"
#include <float.h>
#include "DllData.h"

int DllData::s_fileId = 0;

DllData::DllData()
{
	m_fileId = s_fileId++;
}

DllData::~DllData()
{
}

void DllData::Destroy()
{
	delete this;
}

void DllData::SetData(long dataCount, real_t const* xData, real_t const* yData, real_t const* zData)
{
	m_tData.clear();
	m_xData.assign(xData, xData + dataCount);
	m_yData.assign(yData, yData + dataCount);
	m_zData.assign(zData, zData + dataCount);

	m_indexes.clear();
	for (long i = 0; i < dataCount; ++i) {
		m_indexes.push_back(i);
	}
}

void DllData::SetData(long dataCount, real_t const* xData, real_t const* yData, real_t const* zData, real_t const* tData)
{
	SetData(dataCount, xData, yData, zData);
	m_tData.assign(tData, tData + dataCount);
}

void DllData::SetDataEffective(long count, BOOL* effective)
{
	m_indexes.clear();
	for (long i = 0; i < count; ++i) {
		if (effective[i]) {
			m_indexes.push_back(i);
		}
	}
}

void DllData::Clear()
{
	m_xData.clear();
	m_yData.clear();
	m_zData.clear();
	m_tData.clear();
}

real_t DllData::GetMinimum(long dimension) const
{
	if (GetDataCount() <= 0) { return 0; }

	switch (dimension)
	{
	case 0: return *std::min_element(m_xData.begin(), m_xData.end());
	case 1: return *std::min_element(m_yData.begin(), m_yData.end());
	case 3:
		if (!m_tData.empty()) {
			return *std::min_element(m_tData.begin(), m_tData.end());
		} // tDataが空ならzDataの最小値を返す
	case 2: return *std::min_element(m_zData.begin(), m_zData.end());
	}
	assert(false);
	return FLT_MAX;
}

real_t DllData::GetMaximum(long dimension) const
{
	if (GetDataCount() <= 0) { return FLT_MAX; }

	switch (dimension)
	{
	case 0: return *std::max_element(m_xData.begin(), m_xData.end());
	case 1: return *std::max_element(m_yData.begin(), m_yData.end());
	case 3:
		if (!m_tData.empty()) {
			return *std::max_element(m_tData.begin(), m_tData.end());
		} // tDataが空ならzDataの最大値を返す
	case 2: return *std::max_element(m_zData.begin(), m_zData.end());
	}
	assert(false);
	return FLT_MAX;
}

real_t DllData::GetRealMinimum(long dimension) const
{
	return GetMaximum(dimension);
}

real_t DllData::GetRealMaximum(long dimension) const
{
	return GetMinimum(dimension);
}

real_t const* DllData::GetDataByDimension(int dimension) const
{
	switch (dimension)
	{
	case 0: return &m_xData[0];
	case 1: return &m_yData[0];
	case 2: return &m_zData[0];
	}
	return 0;
}

real_t DllData::GetData(int dimension, size_t index) const
{
	switch (dimension)
	{
	case 0: return m_xData[m_indexes[index]];
	case 1: return m_yData[m_indexes[index]];
	case 3:
		if (!m_tData.empty()) {
			return m_tData[m_indexes[index]];
		} // tDataが空ならzDataの値を返す
	case 2: return m_zData[m_indexes[index]];
	}
	assert(false);
	return FLT_MAX;
}

size_t DllData::FindMaximumIndex(long dimension, long /*exclusionScale*/, bool /*accumulative*/) const
{
	size_t min, max;
	FindMinMaxIndex(dimension, &min, &max);
	return max;
}

size_t DllData::FindMinimumIndex(long dimension, long exclusionScale, bool accumulative) const
{
	size_t min, max;
	FindMinMaxIndex(dimension, &min, &max, exclusionScale, accumulative);
	return min;
}

void DllData::FindMinMaxIndex(size_t dimension, size_t* min, size_t* max, long /*exclusionScale*/, bool /*accumulative*/) const
{
	if (GetDataCount() <= 0) { return; }

	std::vector<real_t> const& data = dimension == 0 ? m_xData : dimension == 1 ? m_yData : m_zData;

	*min = 0;
	*max = 0;
	float minValue = data[0], maxValue = data[0];
	for (size_t i = 1; i < data.size(); ++i) {
		if (minValue > data[i]) {
			minValue = data[i];
			*min = i;
		}
		if (maxValue < data[i]) {
			maxValue = data[i];
			*max = i;
		}
	}
}

void DllData::SetNames(TCHAR const* xName, TCHAR const* yName, TCHAR const* zName)
{
	m_names[0] = xName;
	m_names[1] = yName;
	m_names[2] = zName;
}

void DllData::SetUnits(TCHAR const* xName, TCHAR const* yName, TCHAR const* zName)
{
	m_units[0] = xName;
	m_units[1] = yName;
	m_units[2] = zName;
}

wchar_t const* DllData::GetItemName(long dimension) const
{
	return m_names[dimension].c_str();
}

wchar_t const* DllData::GetUnitName(long dimension) const
{
	return m_units[dimension].c_str();
}

void DllData::SetRawData(long dimension, size_t index, real_t const value)
{
	std::vector<real_t>* arr[] = {&m_xData, &m_yData, &m_zData, &m_tData};
	if (m_indexes.size() <= index) { return; }
	
	size_t const i = m_indexes[index];
	if (m_xData.size() <= i ) { return; }

	(*arr[dimension])[i] = value;
}
