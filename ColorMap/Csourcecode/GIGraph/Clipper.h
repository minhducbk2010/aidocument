/*!	@file
*/
#pragma once

#include <boost/noncopyable.hpp>

class Clipper : public boost::noncopyable
{
public:
	Clipper(double xSize, double ySize, double zSize)
		: m_xSize(xSize), m_ySize(ySize), m_zSize(zSize)
	{
	}

	~Clipper()
	{
		DsiableClipping();
	}

	void Clipping() const
	{
		CreateClipPlanes();

		glEnable(GL_CLIP_PLANE0);
		glEnable(GL_CLIP_PLANE1);
		glEnable(GL_CLIP_PLANE2);
		glEnable(GL_CLIP_PLANE3);
		glEnable(GL_CLIP_PLANE4);
		glEnable(GL_CLIP_PLANE5);
	}

	void DsiableClipping() const
	{
		glDisable(GL_CLIP_PLANE0);
		glDisable(GL_CLIP_PLANE1);
		glDisable(GL_CLIP_PLANE2);
		glDisable(GL_CLIP_PLANE3);
		glDisable(GL_CLIP_PLANE4);
		glDisable(GL_CLIP_PLANE5);
	}

private:
	void CreateClipPlanes() const
	{
		double const x = m_xSize + 0.1f;
		double const z = m_ySize + 0.1f;
		double const y = m_zSize + 0.1f;

		GLdouble clip0[] = { -1.01, 0.0, 0.0, x };
		GLdouble clip1[] = { 1.01, 0.0, 0.0, x };
		GLdouble clip2[] = { 0.0, 0.0, 1.01, z };
		GLdouble clip3[] = { 0.0, 0.0, -1.01, z };
		GLdouble clip4[] = { 0.0, -1.01, 0.0, y };
		GLdouble clip5[] = { 0.0, 1.01, 0.0, y };

		glClipPlane(GL_CLIP_PLANE0, clip0);
		glClipPlane(GL_CLIP_PLANE1, clip1);
		glClipPlane(GL_CLIP_PLANE2, clip2);
		glClipPlane(GL_CLIP_PLANE3, clip3);
		glClipPlane(GL_CLIP_PLANE4, clip4);
		glClipPlane(GL_CLIP_PLANE5, clip5);
	}

private:
	double m_xSize;
	double m_ySize;
	double m_zSize;
};