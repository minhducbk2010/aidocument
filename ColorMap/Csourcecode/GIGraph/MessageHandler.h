/*!	@file
*/
#pragma once

#include <map>
#include <boost/function.hpp>

class MessageHandler
{
protected:
// Commands
	// invoker
	typedef boost::function<void()> message_t;
	typedef std::map<unsigned int, message_t> message_handler_t;
	message_handler_t m_messageHandler;

	typedef boost::function<void(BOOL* enabled, BOOL* checked)> state_getter_t;
	typedef std::map<unsigned int, state_getter_t> state_getter_handler_t;
	state_getter_handler_t m_stateGetter;

	void EnabledAlways(BOOL* enabled, BOOL* /*checked*/) const
	{
		*enabled = TRUE;
	}

	void EnabledAlways(boost::function<BOOL()> f, BOOL* enabled, BOOL* checked) const
	{
		*enabled = TRUE;
		*checked = f();
	}

#ifdef NEW_TRIAL_EDITION
	void UnenabledAlways(BOOL* enabled, BOOL* /*checked*/) const
	{
		*enabled = FALSE;
	}

	void UnenabledAlways(boost::function<BOOL()> f, BOOL* enabled, BOOL* checked) const
	{
		*enabled = FALSE;
		*checked = f();
	}

#endif
};