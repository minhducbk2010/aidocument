//===========================================================================
/*!
	@file
	@author	小島
	@date	2005/02/24
*/
//===========================================================================
#pragma once
//---------------------------------------------------------------------------
struct DelaunayTriangle
{
	unsigned int p1;
	unsigned int p2;
	unsigned int p3;
};
//---------------------------------------------------------------------------
#include <vector>
#include <stack>
//---------------------------------------------------------------------------
#include "dDelaunayData.hpp"
//---------------------------------------------------------------------------
typedef BOOL (__stdcall * ProgressCallback)(short percent);
//---------------------------------------------------------------------------
class dDelaunay
{
public:
	dDelaunay() {}
	~dDelaunay() { Init(); }

	/*!	@brief データ一括入力
		@param x[io]	X座標値配列
		@param y[io]	Y座標値配列
		@param z[io]	Z座標値配列
		@param n[io]	点数 */
	void InputData(double* x, double* y, double* z, unsigned int n, int Individual = 0)
	{
		Normalize(x, y, n, Individual);

		dPoint* p;
		unsigned int i;
		for(i = 0; i < n; i++)
		{
    		p = new dPoint;
			p->x = x[i];
			p->y = y[i];
			p->z = z[i];
			p->n = (unsigned int)vP.size();
			vP.push_back(p);
		}
	}
	//!	デローニ三角分割実行
	bool Calculation(ProgressCallback progress)
	{
		if (progress) { progress(0); }
		double const interval = 100.0 / (vP.size() - 3);

		CreateInitTri();

		unsigned int n, m;
		int i;
		dPoint* p;
		dTri* t;
		dTri* t2, * t3, * t4;
		dTri* TrAd[3];
		int flg;
		double inv;
		int div;

		// 各点についてチェック
		// 最後の3点は仮想三角形の点なので無視
		for(n = 0; n < vP.size() - 3; n++)
		{
    		p = vP[n];

			for(m = 0; m < vT.size(); m++)
			{
        		t = vT[m];

				flg = 0;

				// 点pを内包し、且つ頂点としない三角形を探す
				if(t->SearchVertex(p) < 0)
				{
            		inv = t->IsInvolving(p);

					if(inv < 0.0)
					{
                		// 対象の三角形を点Pを頂点に分割する
						if(!DivTriangle(t, p, &t2, &t3))
						{
                    		vStack.push(t);
							vStack.push(t2);
							vStack.push(t3);
						}
						flg = 1;
					}
					else if(IsZero(inv))
					{
						div = DivDoubleTriangle(t, p, &t2, &t3, &t4);
						if(div == 0)
						{
							vStack.push(t);
							vStack.push(t2);
							vStack.push(t3);
							vStack.push(t4);
						}
						else if(div == 1)
						{
							vStack.push(t);
							vStack.push(t2);
						}
						flg = 1;
					}
				}

				// スタックが空になるまで繰り返し
				while(!vStack.empty())
				{
            		// スタックから三角形を取り出す
					t = vStack.top();
					vStack.pop();

					// 取り出された三角形の隣接三角形について繰り返す
					for(i = 0; i < 3; i++)
					{
                		TrAd[i] = t->Ad[i];
					}

					for(i = 0; i < 3; i++)
					{
						if(TrAd[i])
						{
                    		// 点pを頂点に持たない隣接三角形について処理
							if(TrAd[i]->SearchVertex(p) < 0)
							{
                        		// 対角線の長さを比較
								if(CompDiagonal(t, TrAd[i]) > 0)
								{
                            		// TとtmpT1の対角線を入れ替える

									if(SwapDiagonal(t, TrAd[i]) == 0)
									{
                                		vStack.push(t);
										vStack.push(TrAd[i]);
									}
								}
							}
						}
					}
				}

				if(flg == 1)
				{
            		break;
				}
			}
			if (progress && progress(static_cast<short>(n * interval)) == FALSE) {
				return false;
			}
		}

		if (progress) {
			progress(100); // 完了
		}
		return true;
	}

	//!	頂点数取得(仮想三角形除く)
	unsigned int GetPointCount() const
	{
		if (vP.size() < 3) { return 0; }
		return static_cast<unsigned int>(vP.size() - 3);
	}
	//!	三角形数取得(仮想三角形除く)
	unsigned int GetTrianlgeCount() const
	{
		unsigned int n;
		unsigned int cnt;
		int i, j;
		int flg;

		dPoint* p[3];
		for(i = 0; i < 3; i++)
		{
    		p[i] = vP[vP.size() - 3 + i];
		}

		cnt = 0;
		for(n = 0; n < vT.size(); n++)
		{
    		flg = 0;

			for(i = 0; i < 3; i++)
			{
        		for(j = 0; j < 3; j++)
				{
            		if(vT[n]->Pt[i] == p[j])
					{
                		flg = 1;
						break;
					}
				}

				if(flg == 1)
				{
            		break;
				}
			}

			if(flg == 0)
			{
        		cnt++;
			}
		}

		return cnt;
	}

	//!	結果取得
	int GetResult(DelaunayTriangle* t) const
	{
		dPoint* p[3];
		for(int i = 0; i < 3; i++)
		{
    		p[i] = vP[vP.size() - 3 + i];
		}

		unsigned int cnt = 0;
		for(unsigned int n = 0; n < vT.size(); n++)
		{
    		int flg = 0;

			for(int i = 0; i < 3; i++)
			{
        		for(int j = 0; j < 3; j++)
				{
            		if(vT[n]->Pt[i] == p[j])
					{
                		flg = 1;
						break;
					}
				}

				if(flg == 1)
				{
            		break;
				}
			}

			if(flg == 0)
			{
        		t[cnt].p1 = vT[n]->Pt[0]->n;
				t[cnt].p2 = vT[n]->Pt[1]->n;
				t[cnt].p3 = vT[n]->Pt[2]->n;
				cnt++;
			}
		}

		return cnt;
	}

private:
	std::vector<dPoint*> vP;
	std::vector<dTri*> vT;

	std::stack<dTri*> vStack;

	/*!	@brief 	三点の並び順を調べます.
		@param p1	1点目
		@param p2	2点目
		@param p3	3点目
		@retval 0以下	反時計回り
		@retval 0以上	時計回り
		@retval 0		直線 */
	double CheckCw(const dPoint* p1, const dPoint* p2, const dPoint* p3) const
	{
		return (p1->x - p3->x) * (p2->y - p3->y) - (p1->y - p3->y) * (p2->x - p3->x);
	}
	/*!	@brief 二点のX-Y座標上における距離を取得します.
		@param p1	1点目
		@param p2	2点目
		@return 二点間距離 */
	double GetDistance(const dPoint* p1, const dPoint* p2) const
	{
		double dx = (p1->x - p2->x) * (p1->x - p2->x);
		double dy = (p1->y - p2->y) * (p1->y - p2->y);
		return sqrt(dx + dy);
	}
	/*!	@brief 二つの三角形の共有点二点と非共有点二点を取得します.
			   非共有点1は三角形1側、非共有点2は三角形2側の頂点となります.
		@param t1	三角形1
		@param t2	三角形2
		@param sp1	共有点1アドレス
		@param sp2	共有点2アドレス
		@param ns1	非共有点1アドレス
		@param ns2	非共有点2アドレス
		@retval 0		取得成功
		@retval 0以下	共有点が二点ではない */
	int GetSharedVertex(const dTri* t1, const dTri* t2, dPoint** sp1,
    	dPoint** sp2, dPoint** ns1, dPoint** ns2)
	{
		int cnt = 0;

		for(int i = 0; i < 3; ++i)
		{
    		for(int j = 0; j < 3; ++j)
			{
        		if(t1->Pt[i] == t2->Pt[j])
				{
            		if(cnt == 0)
					{
                		*sp1 = t1->Pt[i];
					}
					else if(cnt == 1)
					{
                		*sp2 = t1->Pt[i];
					}
					cnt++;
				}
			}
		}

		if(cnt != 2)
		{
    		return -1;
		}

		for(int i = 0; i < 3; ++i)
		{
    		if(t1->Pt[i] != *sp1 && t1->Pt[i] != *sp2)
			{
        		*ns1 = t1->Pt[i];
			}
			if(t2->Pt[i] != *sp1 && t2->Pt[i] != *sp2)
			{
        		*ns2 = t2->Pt[i];
			}
		}

		return 0;
	}
	/*!	@brief 	一つの三角形を一点により三つの三角形に分割します.
		分割元の三角形は分割後の三角形のうちの一つになります.
		@param t1	分割対象三角形
		@param p	分割点
		@param t2	分割後三角形2アドレス
		@param t3	分割後三角形3アドレス
		@retval 0		成功
		@retval 0以下	失敗 */
	int DivTriangle(dTri* t1, dPoint* p, dTri** t2, dTri** t3)
	{
		dPoint* tmpP;
		dTri* tmpT1, * tmpT3;

		tmpP = t1->Pt[0];
		t1->Pt[0] = p;
		*t2 = new dTri(p, t1->Pt[2], tmpP);
		vT.push_back(*t2);
		*t3 = new dTri(p, tmpP, t1->Pt[1]);
		vT.push_back(*t3);

		tmpT1 = t1->Ad[0];
		if(tmpT1 != 0)
		{
    		for(int i = 0; i < 3; i++)
			{
        		if(tmpT1->Ad[i] == t1)
				{
            		tmpT1->Ad[i] = *t3;
				}
			}
		}

		tmpT3 = t1->Ad[2];
		if(tmpT3 != 0)
		{
    		for(int i = 0; i < 3; i++)
			{
        		if(tmpT3->Ad[i] == t1)
				{
            		tmpT3->Ad[i] = *t2;
				}
			}
		}

		t1->Ad[0] = *t3;
		t1->Ad[2] = *t2;
		(*t2)->Ad[0] = t1;
		(*t2)->Ad[1] = tmpT3;
		(*t2)->Ad[2] = *t3;
		(*t3)->Ad[0] = *t2;
		(*t3)->Ad[1] = tmpT1;
		(*t3)->Ad[2] = t1;

		return 0;
	}
	/*!	@brief 	二つの三角形を一点により四つの三角形に分割します.
		分割点は分割対象三角形の辺上にあることが前提です.
		分割元の三角形は分割後の三角形のうちの一つになります.
		@param t1	分割対象三角形
		@param p	分割点
		@param t2	分割後三角形2アドレス
		@retval 0		成功
		@retval 1		三角形を2つに分割
		@retval 0以下	失敗 */
	int DivDoubleTriangle(dTri* t1, dPoint* p, dTri** t2, dTri** t3, dTri** t4)
	{
		dPoint* tmpP;
		dTri* tmpT2, * tmpT4;

		int i, j, k;

		// 直線となっている3点を探す
		for(i = 0; i < 3; ++i)
		{
			if(IsZero(CheckCw(t1->Pt[i], p, t1->Pt[(i + 1) % 3])))
			{
				break;
			}
		}
		if(i >= 3)
		{
			return -1;
		}

		// 先ず三角形を2つに分割
		tmpP = t1->Pt[i];
		t1->Pt[i] = p;
		*t2 = new dTri(p, t1->Pt[(i + 2) % 3], tmpP);
		vT.push_back(*t2);

		*t3 = t1->Ad[i];

		if(*t3 == NULL)
		{
			tmpT2 = t1->Ad[(i + 2) % 3];
			t1->Ad[(i + 2) % 3] = *t2;
			(*t2)->Ad[0] = t1;
			(*t2)->Ad[1] = tmpT2;
			(*t2)->Ad[2] = NULL;

			return 1;
		}

		// 直線となっている3点を探す
		for(j = 0; j < 3; ++j)
		{
			if(IsZero(CheckCw((*t3)->Pt[j], p, (*t3)->Pt[(j + 1) % 3])))
			{
				break;
			}
		}
		if(j >= 3)
		{
			return -1;
		}

		tmpP = (*t3)->Pt[j];
		(*t3)->Pt[j] = p;
		*t4 = new dTri(p, (*t3)->Pt[(j + 2) % 3], tmpP);
		vT.push_back(*t4);

		tmpT2 = t1->Ad[(i + 2) % 3];
		if(tmpT2 != 0)
		{
			for(k = 0; k < 3; ++k)
			{
				if(tmpT2->Ad[k] == t1)
				{
					tmpT2->Ad[k] = *t2;
				}
			}
		}

		tmpT4 = (*t3)->Ad[(j + 2) % 3];
		if(tmpT4 != 0)
		{
			for(k = 0; k < 3; ++k)
			{
				if(tmpT4->Ad[k] == *t3)
				{
					tmpT4->Ad[k] = *t4;
				}
			}
		}

		t1->Ad[i] = *t4;
		t1->Ad[(i + 2) % 3] = *t2;
		(*t2)->Ad[0] = t1;
		(*t2)->Ad[1] = tmpT2;
		(*t2)->Ad[2] = *t3;
		(*t3)->Ad[j] = *t2;
		(*t3)->Ad[(j + 2) % 3] = *t4;
		(*t4)->Ad[0] = *t3;
		(*t4)->Ad[1] = tmpT4;
		(*t4)->Ad[2] = t1;

		return 0;
	}
	/*!	@brief 二つの三角形からなる四角形の対角線を比較します.
		二つの三角形に共有辺が無い場合は比較できません.
		@param t1	三角形1
		@param t2	三角形2
		@retval 0		三角形の非共有点同士でなる対角線の方が長い
		@retval 1		三角形の共有辺の方が長い
		@retval 0以下	共有辺が無い */
	int CompDiagonal(const dTri* t1, const dTri* t2)
	{
		dPoint* sp1;
		dPoint* sp2;
		dPoint* ns1;
		dPoint* ns2;
		double d[2];
		int r;

		r = GetSharedVertex(t1, t2, &sp1, &sp2, &ns1, &ns2);

		if(r < 0)
		{
    		return r;
		}

		d[0] = GetDistance(sp1, sp2);
		d[1] = GetDistance(ns1, ns2);

		if(d[0] > d[1])
		{
    		r = 1;
		}
		else
		{
    		r = 0;
		}

		return r;
	}
	/*! @brief 	二つの三角形からなる四角形の対角線を交換し、二つの三角形を更新します.
		@param t1	三角形1
		@param t2	三角形2
		@retval 0		成功
		@retval 0以下	失敗 */
	int SwapDiagonal(dTri* t1, dTri* t2)
	{
		dPoint* sp[2];	// 共有点
		dPoint* ns[2];	// 非共有点
		int spn[2];		// 非共有点の点番号
		int i;
		int ln = 0;

		dTri* oldt1Ad[3];
		dTri* oldt2Ad[3];

		// t1とt2の共有点と非共有点を探す
		if(GetSharedVertex(t1, t2, &sp[0], &sp[1], &ns[0], &ns[1]) < 0)
		{
    		return -1;
		}

		for(i = 0; i < 3; i++)
		{
    		if(t1->Pt[i] == ns[0])
			{
        		spn[0] = i;
			}
			if(t2->Pt[i] == ns[1])
			{
        		spn[1] = i;
			}
		}

		// ns[0], sp[0], ns[1]が反時計回りに並んでいるか調べる
		if(CheckCw(ns[0], sp[0], ns[1]) < 0)
		{
    		ln += 1;
		}
		// ns[0], sp[1], ns[1]が反時計回りに並んでいるか調べる
		if(CheckCw(ns[0], sp[1], ns[1]) < 0)
		{
    		ln += 2;
		}
		// 両方のフラグがあるか、または両方ともフラグがない場合は
		// 非共有点同士で出来る対角線が四角形の外にある
		if(ln < 1 || ln > 2)
		{
    		return -1;
		}
		ln--;

		// さらに以下が成り立たなければ
		// ns[0]-sp[ln]-ns[1]-sp[(ln + 1) % 2]は凸四角形？
		if(!(CheckCw(ns[1], sp[(ln + 1) % 2], ns[0]) < 0) ||
		   !(CheckCw(sp[(ln + 1) % 2], ns[0], sp[ln]) < 0) ||
		   !(CheckCw(sp[ln], ns[1], sp[(ln + 1) % 2]) < 0))
		{
    		return -1;
		}

		// 対角線交換前の隣接三角形を覚えておく
		for(i = 0; i < 3; i++)
		{
    		oldt1Ad[i] = t1->Ad[i];
			oldt2Ad[i] = t2->Ad[i];
		}

		t1->Pt[0] = ns[0];
		t1->Pt[1] = sp[ln];
		t1->Pt[2] = ns[1];
		t1->Ad[0] = oldt1Ad[spn[0]];
		t1->Ad[1] = oldt2Ad[(spn[1] + 2) % 3];
		t1->Ad[2] = t2;
		for(i = 0; i < 3; i++)
		{
    		if(t1->Ad[1] != 0)
			{
        		if(t1->Ad[1]->Ad[i] == t2)
				{
            		t1->Ad[1]->Ad[i] = t1;
				}
			}
		}

		t2->Pt[0] = ns[0];
		t2->Pt[1] = ns[1];
		t2->Pt[2] = sp[(ln + 1) % 2];
		t2->Ad[0] = t1;
		t2->Ad[1] = oldt2Ad[spn[1]];
		t2->Ad[2] = oldt1Ad[(spn[0] + 2) % 3];
		for(i = 0; i < 3; i++)
		{
    		if(t2->Ad[2] != 0)
			{
        		if(t2->Ad[2]->Ad[i] == t1)
				{
            		t2->Ad[2]->Ad[i] = t2;
				}
			}
		}

		return 0;
	}

	//! 初期化
	void Init()
	{
		unsigned int i;

		// スタック初期化
		while(!vStack.empty())
		{
    		vStack.pop();
		}

		// 三角形初期化
		for(i = 0; i < vT.size(); i++)
		{
    		if(vT[i] != 0)
			{
        		delete vT[i];
			}
		}
		vT.clear();

		// 点初期化
		for(i = 0; i < vP.size(); i++)
		{
       		delete vP[i];
		}
		vP.clear();
	}
	//!	仮想三角形生成
	void CreateInitTri()
	{
		unsigned int n;
		double minX, minY, maxX, maxY;

		minX = maxX = vP[0]->x;
		minY = maxY = vP[0]->y;

		for(n = 0; n < vP.size(); n++)
		{
    		if(vP[n]->x < minX)
			{
        		minX = vP[n]->x;
			}
			if(vP[n]->x > maxX)
			{
        		maxX = vP[n]->x;
			}
			if(vP[n]->y < minY)
			{
        		minY = vP[n]->y;
			}
			if(vP[n]->y > maxY)
			{
        		maxY = vP[n]->y;
			}
		}

		dPoint* p1;
		dPoint* p2;
		dPoint* p3;
		p1 = new dPoint;
		p1->x = 0;//minX + (maxX - minX) / 2.0;
		p1->y = 100;//minY - (maxY - minY) * 100.0;
		p1->z = 0;
		p1->n = (unsigned int)vP.size();
		vP.push_back(p1);
		p2 = new dPoint;
		p2->x = 100;//minX - (maxX - minX) * 100.0;
		p2->y = -100;//maxY + (maxY - minY) * 10.0;
		p2->z = 0;
		p2->n = (unsigned int)vP.size();
		vP.push_back(p2);
		p3 = new dPoint;
		p3->x = -100;//maxX + (maxX - minX) * 100.0;
		p3->y = -100;//p2->y;
		p3->z = 0;
		p3->n = (unsigned int)vP.size();
		vP.push_back(p3);

		dTri* t = new dTri(p1, p2, p3);
		vT.push_back(t);
	}

	void Normalize(double* x, double* y, unsigned int n, int Individual) const
	{
		double minX = x[0];
		double maxX = x[0];
		double minY = y[0];
		double maxY = y[0];
		double cntX, cntY;
		unsigned int i;

		for(i = 1; i < n; i++)
		{
    		if(x[i] < minX)
			{
        		minX = x[i];
			}
			if(x[i] > maxX)
			{
        		maxX = x[i];
			}
			if(y[i] < minY)
			{
        		minY = y[i];
			}
			if(y[i] > maxY)
			{
        		maxY = y[i];
			}
		}
		cntX = (maxX - minX) / 2.0 + minX;
		cntY = (maxY - minY) / 2.0 + minY;

		double wX, wY;
		double rateX, rateY;

		if(Individual != 0)
		{
    		wX = maxX - minX;
			wY = maxY - minY;

			rateX = (wX != 0.0) ? (2.0 / wX) : 1.0;
			rateY = (wY != 0.0) ? (2.0 / wY) : 1.0;
		}
		else
		{
    		if((maxX - minX) > (maxY - minY))
			{
        		wX = maxX - minX;
			}
			else
			{
        		wX = maxY - minY;
			}

			rateX = (wX != 0.0) ? (2.0 / wX) : 1.0;
			rateY = rateX;
		}

		for(i = 0; i < n; i++)
		{
    		x[i] = (double)((x[i] - cntX) * rateX);
			y[i] = (double)((y[i] - cntY) * rateY);
		}
	}
};
