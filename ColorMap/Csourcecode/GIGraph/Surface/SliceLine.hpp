#pragma once

struct LevelLine
{
	double level;
	int style;
	float width;
	COLORREF color;

	LevelLine() : level(), style(PS_SOLID), width(1), color(0) {}
};

// OpenGLでの線種
const unsigned short LinePattern[] =
{
	0xFFFF,	// 実線       1111 1111 1111 1111
	0xBBBB,	// 破線       1011 1011 1011 1011
	0xAAAA,	// 点線       1010 1010 1010 1010
	0xD75D,	// 一点鎖線   1101 0111 0101 1101
	0xDBDB,	// 二点鎖線   1101 1011 1101 1011
	0x0000,	// 無し
};


