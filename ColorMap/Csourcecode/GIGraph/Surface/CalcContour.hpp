/*! @file
*/
#pragma once
#include <windows.h>

//! 格子の最大値
#define	MAX_MATX	50
#define	MAX_MATY	50

class CContour
{
public:
	CContour() : PM1(0), PM2(1), ALPHA(32)
	{
	}

    void GriddingParam(double Pm1, double Pm2, double Alpha)
	{
		_ASSERT(Pm1 >= 0);
		_ASSERT(Pm2 >= 0);
		_ASSERT(Alpha >= 0);

		PM1 = Pm1;
		PM2 = Pm2;
		ALPHA = Alpha;
	}

	/*! @brief グリッディング計算実行(ｸﾞﾘｯﾃﾞｨﾝｸﾞした格子数の最大は31×31)

		@param  Nd			測定ﾃﾞｰﾀ数
		@param  *Xd,*Yd,*Zd	測定ﾃﾞｰﾀ､(X,Y)におけるZ値
		@param  Xmin,Xmax	ｸﾞﾘｯﾃﾞｨﾝｸﾞX領域
		@param  Ymin,Ymax	ｸﾞﾘｯﾃﾞｨﾝｸﾞY領域
		@param  Nx, Ny		格子数
		@param  Xg,Yg,f 	ｸﾞﾘｯﾃﾞｨﾝｸﾞ結果､(X,Y)におけるZ値

		@retval 0   正常終了
		@retval 1   ｸﾞﾘｯﾃﾞｨﾝｸﾞ格子数ｵｰﾊﾞｰ
		@retval 2   指定領域内の測定点数不足
		@retval 3   特異点行列による演算不能
		@retval 4   ｸﾞﾘｯﾃﾞｨﾝｸﾞ領域指定無効
		@retval 5   格子数指定無効
	*/
    long Gridding(long Nd,   double *Xd,  double *Yd, double *Zd,
		double Xmin, double Xmax, double Ymin,
		double Ymax, long Nx,   long Ny,  double FAR *f)
	{
		HGLOBAL	hA[MAX_MATX*MAX_MATY +1], hB = NULL, hXG = NULL, hYG = NULL;
		long	RetCode = 0;
		long	IGMT;

		if(Nd == 1) {           // 元データが1個しかない場合            // A.Murai
			double   fVal = *Zd;
			int     i, n = Nx * Ny;
			for(i=0 ; i<n ; i++) {
				*f++ = fVal;    // 全点、同じ値を返す
			}
			// RetCodeは??????
			return(RetCode);
		}

		ND = Nd;	    XD = Xd - 1;	YD = Yd - 1;	ZD = Zd - 1;
		XMIN = Xmin;	XMAX = Xmax;	YMIN = Ymin;	YMAX = Ymax;
		NX = Nx;	    NY = Ny;	    F = f - 1;

		for(I = 1; I <= MAX_MATX*MAX_MATY; ++I)
		{
			hA[I] = NULL;
			if ((hA[I] = GlobalAlloc(GPTR, (2*MAX_MATY+1 +1)*sizeof(double))) == NULL)	{	RetCode = -1;	goto ExitFunc;	}
		}

		if ((hB  = GlobalAlloc(GPTR, (MAX_MATX*MAX_MATY +1)*sizeof(double))) == NULL)	{	RetCode = -1;	goto ExitFunc;	}
		if ((hXG = GlobalAlloc(GPTR, (NX+1)*sizeof(double))) == NULL)	                {	RetCode = -1;	goto ExitFunc;	}
		if ((hYG = GlobalAlloc(GPTR, (NY+1)*sizeof(double))) == NULL)	                {	RetCode = -1;	goto ExitFunc;	}

		for(I = 1; I <= MAX_MATX*MAX_MATY; ++I)	A[I]= (double *)GlobalLock(hA[I]);
		B  = (double *)GlobalLock(hB);
		XG = (double *)GlobalLock(hXG);
		YG = (double *)GlobalLock(hYG);

		// MAIN ---------------------------------------------------------
		if (XMAX <= XMIN || YMAX <= YMIN)
		{
			RetCode = 4;
			goto ExitFunc;
		}

		if (NX < 2  || NY < 2 || NX > MAX_MATX || NY > MAX_MATY)
		{
    		RetCode = 5;
			goto ExitFunc;
		}

		DX = (XMAX - XMIN)/(NX - 1.);
		DY = (YMAX - YMIN)/(NY - 1.);

		for(I = 1; I <= NX; ++I)	XG[I] = XMIN + (I - 1.)*DX;
		for(J = 1; J <= NY; ++J)	YG[J] = YMIN + (J - 1.)*DY;

		// SURFACE.1 ------------------------------------------------------
		MA = NX * NY;
		MB = 2 * NY + 1;

		// SURFACE.1 ------------------------------------------------------
		NBAND = 2 * NY;

		if (MATRIX_1())
		{// ﾏﾄﾘｯｸｽにﾃﾞｰﾀ代入
    		RetCode = 2;
			goto ExitFunc;
		}

		if (CHOLESKI_BAND())
		{// ﾏﾄﾘｯｸｽから解を計算
			RetCode = 3;
			goto ExitFunc;
		}

		for(I = 1; I <= NX; ++I)
		{
			for (J = 1; J <= NY; ++J)
			{
				IGMT = NY*(I - 1) + J;
				F[NX*(J - 1) + I] = B[IGMT];
			}
		}

	ExitFunc:

		for(I = 1; I <= MAX_MATX * MAX_MATY; ++I)
		{
			if (hA[I] != NULL)
			{
				GlobalUnlock(hA[I]);
				GlobalFree(hA[I]);
			}
		}

		if (hB != NULL)
		{
			GlobalUnlock(hB);
			GlobalFree(hB);
		}

		if (hXG != NULL)
		{
			GlobalUnlock(hXG);
			GlobalFree(hXG);
		}

		if (hYG != NULL)
		{
			GlobalUnlock(hYG);
			GlobalFree(hYG);
		}

		return(RetCode);
	}

private:
    BOOL MATRIX_1()
	{
		BOOL Ret = FALSE;

		for (I = 1; I <= MA; ++I)
		{
			B[I] = 0.;
			for (J = 1; J <= NBAND+1; ++J)	A[I][J] = 0.;
		}

		if (PM1 > 0.)
		{
			JX_1();
			JY_1();
		}

		if (PM2 > 0.)
		{
			JXX_1();
			JYY_1();
			JXY_1();
			if (HMAT_1()) Ret = TRUE;
		}

		return(Ret);
	}
	void JX_1()
	{
		N1 = NY + 1;
		for (J = 1; J <= NY; ++J)
		{
			for (I = 1; I <= NX-1; ++I)
			{
				L1 = NY*(I - 1) + J;
				A[L1][1] = A[L1][1] + PM1;
				A[L1][N1] = A[L1][N1] - PM1;
				L2 = NY*I + J;
				A[L2][1] = A[L2][1] + PM1;
			}
		}
	}
	void JY_1()
	{
		for (I = 1; I <= NX; ++I)
		{
			for (J = 1; J <= NY-1; ++J)
			{
				L1 = NY*(I - 1) + J;
				A[L1][1] = A[L1][1] + PM1;
				A[L1][2] = A[L1][2] - PM1;
				L2 = L1 + 1;
				A[L2][1] = A[L2][1] + PM1;
			}
		}
	}
	void JXX_1()
	{
		N1 = NY + 1;
		N2 = 2*NY + 1;
		for (J = 1; J <= NY; ++J)
		{
			for (I = 2; I <= NX-1; ++I)
			{
				L1 = NY*(I - 2) + J;
				A[L1][1]  = A[L1][1]  + PM2;
				A[L1][N1] = A[L1][N1] - 2.0 * PM2;
				A[L1][N2] = A[L1][N2] + PM2;
				L2 = NY*(I - 1) + J;
				A[L2][1]  = A[L2][1]  + 4.0 * PM2;
				A[L2][N1] = A[L2][N1] - 2.0 * PM2;
				L3 = NY*I + J;
				A[L3][1]  = A[L3][1]  + PM2;
			}
		}
	}
	void JYY_1()
	{
		for (I = 1; I <= NX; ++I)
		{
			for (J = 2; J <= NY-1; ++J)
			{
				L1 = NY*(I - 1) + J - 1;
				A[L1][1] = A[L1][1] + PM2;
				A[L1][2] = A[L1][2] - 2.0 * PM2;
				A[L1][3] = A[L1][3] + PM2;
				L2 = L1 + 1;
				A[L2][1] = A[L2][1] + 4.0 * PM2;
				A[L2][2] = A[L2][2] - 2.0 * PM2;
				L3 = L2 + 1;
				A[L3][1] = A[L3][1] + PM2;
			}
		}
	}
	void JXY_1()
	{
		WT = 2.0 * PM2;
		C1 = 1.0;	C2 = -1.0;	C3 = -1.0;	C4 = 1.0;	V = 0.0;
		for(I = 1; I <= NX-1; ++I)
		{
			for(J = 1; J <= NY-1; ++J)
			{
				BMAT_1();
			}
		}
	}
	void BMAT_1()
	{
		L1 = NY*(I - 1) + J;
			A[L1][1]	= A[L1][1]		+ WT * C1 * C1;
			A[L1][2]	= A[L1][2]		+ WT * C1 * C2;
			A[L1][NY+1] = A[L1][NY+1]	+ WT * C1 * C3;
			A[L1][NY+2] = A[L1][NY+2]	+ WT * C1 * C4;
			B[L1]		= B[L1]			+ WT * C1 * V;
		L2 = L1 + 1;
			A[L2][1]	= A[L2][1]		+ WT * C2 * C2;
			A[L2][NY]	= A[L2][NY]		+ WT * C2 * C3;
			A[L2][NY+1]	= A[L2][NY+1]	+ WT * C2 * C4;
			B[L2]		= B[L2]			+ WT * C2 * V;
		L3 = NY*I + J;
			A[L3][1]	= A[L3][1]		+ WT * C3 * C3;
			A[L3][2]	= A[L3][2]		+ WT * C3 * C4;
			B[L3]		= B[L3]			+ WT * C3 * V;
		L4 = L3 + 1;
			A[L4][1]	= A[L4][1]		+ WT * C4 * C4;
			B[L4]		= B[L4]			+ WT * C4 * V;
	}
	BOOL HMAT_1()
	{
		long	NHDT = 0;
		double	CX, CY;

		for(K = 1; K <= ND; ++K)
		{
			if (XD[K] < XMIN - DX / 2.0) continue;
			if (XD[K] > XMAX + DX / 2.0) continue;

				I = (long)((XD[K] - XMIN)/DX + 0.000001) + 1;

			if (I < 1) I = 1;
			if (I > NX - 1) I = NX - 1;
			if (YD[K] < YMIN - DY / 2.0) continue;
			if (YD[K] > YMAX + DY / 2.0) continue;

				J = (long)((YD[K] - YMIN)/DY + 0.000001) + 1;

			if (J < 1) J = 1;
			if (J > NY - 1) J = NY - 1;
				CX = (XD[K] - XG[I]) / DX;
				CY = (YD[K] - YG[J]) / DY;
					WT = ALPHA;
					C1 = (1 - CX) * (1 - CY);
					C2 = (1 - CX) * CY;
					C3 = CX * (1 - CY);
					C4 = CX * CY;
					V = ZD[K];
					BMAT_1();
				NHDT = NHDT + 1;
		}

		if (NHDT < 3) return(TRUE);	// NG
		return(FALSE);	            // OK
	}
    BOOL CHOLESKI_BAND()
	{
		double const SMALL = 1E-10;

		BOOL	Ret = FALSE;
		long	NB1, MNB, KARA, MADE, IP1, IM1, JP1, KKARA;
		double	SA,  SB;

		NB1 = NBAND + 1;	MNB = MA - NBAND;
		A[1][1] = pow(A[1][1], 0.5);
		for(J = 2; J <= NB1; ++J)	A[1][J] = A[1][J] / A[1][1];
		B[1] = B[1]/A[1][1];

		for(I = 2; I <= MA; ++I)
		{
			if (I < NB1)	KARA = 1;	else	KARA = I - NBAND;
			if (I > MNB)	MADE = MA;	else	MADE = I + NBAND;
			IM1 = I - 1;	IP1 = I + 1;
			SA = A[I][1];	SB = B[I];

			for(K = KARA; K <= IM1; ++K)
			{
				SA -= pow(A[K][IP1 - K], 2.0);
				SB -= A[K][IP1 - K]*B[K];
			}

			if (SA <= SMALL)
			{
				Ret = TRUE;
				goto Line2640;
			}

			A[I][1] = pow(SA, .5);
			B[I] = SB/A[I][1];
			if (I == MA)	goto Line2530;
			for(J = IP1; J <= MADE; ++J)
			{
				SA = A[I][J - IM1];
				JP1 = J + 1;

				if (J < NB1) KKARA = 1;	else KKARA = J - NBAND;
				if (KKARA > IM1) goto Line2490;
				for(K = KKARA; K <= IM1; ++K)	SA -= A[K][IP1 - K] * A[K][JP1 - K];
	Line2490:
				A[I][J - IM1] = SA / A[I][1];
			}
		}
	Line2530:
		B[MA] = B[MA] / A[MA][1];
		for(I = MA - 1; I >= 1; --I)
		{
			if (I > MNB)
				MADE = MA;
			else
				MADE = I + NBAND;

			IP1 = I + 1;	IM1 = I - 1;	SB = B[I];
			for(K = IP1; K <= MADE; ++K)	SB -= A[I][K - IM1] * B[K];
			B[I] = SB / A[I][1];
		}

	Line2640:

		return(Ret);
	}

private:
    double	*A[MAX_MATX*MAX_MATY +1], *B;
    long	NBAND, N1, N2, L1, L2, L3, L4;
    long	MA, MB;
    double	PM1, PM2, ALPHA;
    long	ND, NX, NY;
    double	WT,	C1, C2, C3, C4, V;
    double	DX, DY;
    double  *XD, *YD, *ZD, XMIN, XMAX, YMIN, YMAX, *XG, *YG, *F;
    long	I, J, K;
};
