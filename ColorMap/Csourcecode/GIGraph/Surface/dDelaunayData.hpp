//===========================================================================
/*!	@file
	@author	小島
	@date	2005/02/24
*/
//===========================================================================
#pragma once
#ifndef dDelaunayDataH
#define dDelaunayDataH
//---------------------------------------------------------------------------
struct dPoint
{
	unsigned int n;
	double x;
	double y;
	double z;
};
//---------------------------------------------------------------------------
class dTri
{
public:
	dPoint* Pt[3];	//!< 頂点
	dTri* Ad[3];	//!< 隣接三角形

	/*! 引数の三点を頂点とします（省略不可）.
		三点は反時計周りに並び替えられます. */
	dTri(dPoint* p1, dPoint* p2, dPoint* p3)
	{
		Pt[0] = p1;
		Pt[1] = p2;
		Pt[2] = p3;

		for(int i = 0; i < 3; i++)
		{
    		Ad[i] = 0;
		}

		SortVertex();
	}

	/*!	@brief 指定の点が三角形の何番目の頂点か調べます.
		@param p	探索点
		@retval 0以上	探索点の頂点番号
		@retval 0以下	頂点ではない
	*/
	int SearchVertex(const dPoint* p)
	{
		for(int i = 0; i < 3; i++)
		{
    		if(Pt[i] == p)
			{
        		return i;
			}
		}
		return -1;
	}
	/*! @brief 指定の点が三角形内に含まれるかどうか調べます.
		@param p	判定点
		@retval 0以下	点は三角形内にある
		@retval 0以上	点は三角形外にある
		@retval 0		点は三角形辺上にある */
	double IsInvolving(const dPoint* p)
	{
		// 点が各辺の内側にあるかどうかを調べる
		// マイナスなら内側、0なら辺上、プラスなら外側
		double const a = CheckCw(Pt[0], Pt[1], p);
		double const b = CheckCw(Pt[1], Pt[2], p);
		double const c = CheckCw(Pt[2], Pt[0], p);

		// 点がどれか一つでも辺の外側なら三角形の外側
		if (a > 0.0 || b > 0.0 || c > 0.0) {
			return 1.0;
		}
		// 点がどれか一つでも辺上なら三角形の辺上
		else if (a == 0.0 || b == 0.0 || c == 0.0) {
			return 0.0;
		}

		return -1.0;
	}

private:
	/*!	@brief 三点の並び順を調べます.
		@param p1	1点目
		@param p2	2点目
		@param p3	3点目
		@retval 0以下	反時計回り
		@retval 0以上	時計回り
		@retval 0		直線
	*/
	double CheckCw(const dPoint* p1, const dPoint* p2, const dPoint* p3)
	{
		return (p1->x - p3->x) * (p2->y - p3->y) - (p1->y - p3->y) * (p2->x - p3->x);
	}
	/*!	頂点を反時計周りに位置するよう並び替えます.
		但し、1点目は変更されません. */
	void SortVertex()
	{
		if((Pt[0]->x - Pt[2]->x) * (Pt[1]->y - Pt[2]->y) -
		   (Pt[0]->y - Pt[2]->y) * (Pt[1]->x - Pt[2]->x) > 0)
		{
			std::swap(Pt[1], Pt[2]);
		}
	}
};
//---------------------------------------------------------------------------
#endif
