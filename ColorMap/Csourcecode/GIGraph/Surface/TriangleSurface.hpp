/*!	@file
	三角分割による曲面の描画を行います
*/
#pragma once

#include "Surface/Surface.hpp"
#include "Surface/dDelaunay.h"
#include "IPlotData.h"

#undef GetYValue

class OCTriangleSurface : public OCSurface
{
public:
	OCTriangleSurface() : m_triangles(0), m_triangleCount(0)
	{
	}

	virtual ~OCTriangleSurface()
	{
		Release();
	}

	void Release()
	{
		delete[] m_triangles; m_triangles = 0;
		m_triangleCount = 0;
	}

	bool Calculate(CalcData* data, int /*mesh*/, double /*alpha*/)
	{
		Release();

		m_data = data;

		for (size_t i = 0; i < m_data->x.size(); ++i) {
			m_data->x[i] = data->ConvertX(m_data->x[i]);
			m_data->y[i] = data->ConvertY(m_data->y[i]);
			m_data->z[i] = data->ConvertZ(m_data->z[i]);
			m_data->t[i] = data->ConvertT(m_data->t[i]);
		}
		m_data->xMax = data->ConvertX(m_data->xMax);
		m_data->xMin = data->ConvertX(m_data->xMin);
		m_data->yMax = data->ConvertY(m_data->yMax);
		m_data->yMin = data->ConvertY(m_data->yMin);

		m_gridX = data->x;
		m_gridY = data->y;
		m_gridZ = data->z;
		m_gridT = data->t;

//		WPARAM w = reinterpret_cast<WPARAM>(_T("三角分割を実行しています..."));
//		LPARAM l = TRUE;
//		ProgressCallback progress = 0;//reinterpret_cast<ProgressCallback>(m_graph->GetOwner()->SendMessage(PI_START_PROGRESSBAR, w, l));

		std::vector<double> xTemp = data->x;
		std::vector<double> yTemp = data->y;
		std::vector<double> zTemp = data->z;

		dDelaunay delaunay;
		delaunay.InputData(&xTemp[0], &yTemp[0], &zTemp[0], static_cast<unsigned int>(xTemp.size()), 1);
		if (delaunay.Calculation(/*progress*/0)) {
			if ((m_triangleCount = delaunay.GetTrianlgeCount()) > 0) {
				m_triangles = new DelaunayTriangle[m_triangleCount];
				delaunay.GetResult(m_triangles);     // 三角並び順構造体を取得
			}
		}
		//m_graph->GetOwner()->SendMessage(PI_EXIT_PROGRESSBAR, 0, 0);
		return m_triangleCount > 0;
	}

	//! 等高線を描画します
	virtual void DrawSliceLineImpl()
	{
		if (m_decor->renderSliceLine) {
			DrawSliceLineImpl(false);			
		}
	}

	virtual void DrawSliceLineProjectionImpl()
	{
		if (m_decor->renderSliceLineProjection) {
			DrawSliceLineImpl(true);
		}
	}

	void DrawSliceLineImpl(bool value)
	{
		double dX[4], dY[4], dH[4];
		for (int i = 0 ; i < m_triangleCount; ++i) {
			// １面の頂点座標を取得
			dX[0] = m_data->x[m_triangles[i].p1];
			dY[0] = m_data->y[m_triangles[i].p1];
			dH[0] = m_data->z[m_triangles[i].p1];
			dX[1] = m_data->x[m_triangles[i].p2];
			dY[1] = m_data->y[m_triangles[i].p2];
			dH[1] = m_data->z[m_triangles[i].p2];
			dX[2] = m_data->x[m_triangles[i].p3];
			dY[2] = m_data->y[m_triangles[i].p3];
			dH[2] = m_data->z[m_triangles[i].p3];

			PaintContour(dX, dY, dH, value);
		}
	}

	void DrawWireFrameImpl()
	{
		bool const useTexture = m_decor->coloring && !m_decor->filling;
		double const dZAxisMax = m_data->zMax;
		double const dZAxisMin = m_data->zMin;

		::glBegin(GL_TRIANGLES);
		for (int i = 0; i < m_triangleCount; ++i) {
			if (useTexture) {
				GLdouble const tex = (m_data->z[m_triangles[i].p1] - dZAxisMin) / (dZAxisMax - dZAxisMin);
				::glTexCoord1d(tex);
			}
			::glVertex3d(m_data->x[m_triangles[i].p1], m_data->z[m_triangles[i].p1], m_data->y[m_triangles[i].p1]);

			if (useTexture) {
				GLdouble const tex = (m_data->z[m_triangles[i].p2] - dZAxisMin) / (dZAxisMax - dZAxisMin);
				::glTexCoord1d(tex);
			}
			::glVertex3d( m_data->x[m_triangles[i].p2], m_data->z[m_triangles[i].p2], m_data->y[m_triangles[i].p2]);

			if (useTexture) {
				GLdouble const tex = (m_data->z[m_triangles[i].p3] - dZAxisMin) / (dZAxisMax - dZAxisMin);
				::glTexCoord1d(tex);
			}
			::glVertex3d(m_data->x[m_triangles[i].p3], m_data->z[m_triangles[i].p3], m_data->y[m_triangles[i].p3]);
		}
		::glEnd();
	}

	void DrawSurface()
	{
		DrawMap(false);
	}

	void Projection()
	{
		DrawMap(true);
	}

	void DrawMap(bool projection)
	{
		::glPushMatrix();
		::glPolygonOffset(5.0, 5.0);

		double const dZAxisMin = m_data->zMin, dZAxisMax = m_data->zMax;

		double dWari = 1.0;
		double dX[4], dY[4], dH[4];
		for (int i = 0 ; i < m_triangleCount; ++i) {
			// １面の頂点座標を取得
			dX[0] = m_data->x[m_triangles[i].p1];
			dY[0] = m_data->y[m_triangles[i].p1];
			dH[0] = m_data->z[m_triangles[i].p1];
			dX[1] = m_data->x[m_triangles[i].p2];
			dY[1] = m_data->y[m_triangles[i].p2];
			dH[1] = m_data->z[m_triangles[i].p2];
			dX[2] = m_data->x[m_triangles[i].p3];
			dY[2] = m_data->y[m_triangles[i].p3];
			dH[2] = m_data->z[m_triangles[i].p3];

			double dHWari[4];
			for (int k = 0 ; k < 3 ; k++) {
				dHWari[k] = dWari * ((dH[k] - dZAxisMin) / (dZAxisMax - dZAxisMin));
			}

			glColor3f(1.0, 1.0, 1.0);
			if (projection) {
				RenderProjection(GL_POLYGON, dX, dH, dY, dHWari, 3);
			} else {
				if (m_data->dimension == 4) {
					double v[] = {m_data->t[m_triangles[i].p1], m_data->t[m_triangles[i].p2], m_data->t[m_triangles[i].p3]};
					for (int k = 0 ; k < 3 ; k++) {
						dHWari[k] = dWari * ((v[k] - dZAxisMin) / (dZAxisMax - dZAxisMin));
					}
				}

				RenderPlane(GL_POLYGON, dX, dH, dY, dHWari, 3);
			}
		}

		::glPopMatrix();
	}


private:

	virtual int CalcInterpolate(double const* pdX, double const* pdY, double const* pdZ, double const dZ, double* pdCX, double* pdCY, double* pdCZ, bool bUpper)
	{
		int nRet = 0;
		double dCX[2], dCY[2], dCZ[2];

		// 0-1が高くて3-2が低い
   		if ((pdZ[1] <= dZ && pdZ[2] >= dZ && pdZ[0] <= dZ && pdZ[3] >= dZ) ||
			(pdZ[1] >= dZ && pdZ[2] <= dZ && pdZ[0] >= dZ && pdZ[3] <= dZ)){
   			if (pdZ[0] > pdZ[3]) {
				if (!IsZero(pdZ[0] - pdZ[3])) {
               		dCX[0] = pdX[3] + (dZ - pdZ[3]) * ((pdX[0] - pdX[3]) / (pdZ[0] - pdZ[3]));
               		dCY[0] = pdY[3] + (dZ - pdZ[3]) * ((pdY[0] - pdY[3]) / (pdZ[0] - pdZ[3]));
					dCZ[0] = dZ;
				}
				if (!IsZero(pdZ[1] - pdZ[2])) {
            		dCX[1] = pdX[2] + (dZ - pdZ[2]) * ((pdX[1] - pdX[2]) / (pdZ[1] - pdZ[2]));
   	       			dCY[1] = pdY[2] + (dZ - pdZ[2]) * ((pdY[1] - pdY[2]) / (pdZ[1] - pdZ[2]));
					dCZ[1] = dZ;
				}

				if (!IsZero(pdZ[0] - pdZ[3]) && !IsZero(pdZ[1] - pdZ[2])) {
					// 補間値より上の面を取得
					if (bUpper) {
						pdCX[0] = pdX[0];
						pdCY[0] = pdY[0];
						pdCZ[0] = pdZ[0];

						pdCX[1] = pdX[1];
						pdCY[1] = pdY[1];
						pdCZ[1] = pdZ[1];

						pdCX[2] = dCX[1];
						pdCY[2] = dCY[1];
						pdCZ[2] = dCZ[1];

						pdCX[3] = dCX[0];
						pdCY[3] = dCY[0];
						pdCZ[3] = dCZ[0];
					} else {
						pdCX[0] = dCX[0];
						pdCY[0] = dCY[0];
						pdCZ[0] = dCZ[0];

						pdCX[1] = dCX[1];
						pdCY[1] = dCY[1];
						pdCZ[1] = dCZ[1];

						pdCX[2] = pdX[2];
						pdCY[2] = pdY[2];
						pdCZ[2] = pdZ[2];

						pdCX[3] = pdX[3];
						pdCY[3] = pdY[3];
						pdCZ[3] = pdZ[3];
					}
					nRet = 4;
				}
   			} else {
				if (!IsZero(pdZ[3] - pdZ[0])) {
					dCX[1] = pdX[0] + (dZ - pdZ[0]) * ((pdX[3] - pdX[0]) / (pdZ[3] - pdZ[0]));
					dCY[1] = pdY[0] + (dZ - pdZ[0]) * ((pdY[3] - pdY[0]) / (pdZ[3] - pdZ[0]));
					dCZ[1] = dZ;
				}
				if (!IsZero(pdZ[2] - pdZ[1])) {
					dCX[0] = pdX[1] + (dZ - pdZ[1]) * ((pdX[2] - pdX[1]) / (pdZ[2] - pdZ[1]));
					dCY[0] = pdY[1] + (dZ - pdZ[1]) * ((pdY[2] - pdY[1]) / (pdZ[2] - pdZ[1]));
					dCZ[0] = dZ;
				}

				if (!IsZero(pdZ[3] - pdZ[0]) && !IsZero(pdZ[2] - pdZ[1])) {
					// 補間値より上の面を取得
					if (bUpper) {
						pdCX[0] = dCX[0];
						pdCY[0] = dCY[0];
						pdCZ[0] = dCZ[0];

						pdCX[1] = dCX[1];
						pdCY[1] = dCY[1];
						pdCZ[1] = dCZ[1];

						pdCX[2] = pdX[3];
						pdCY[2] = pdY[3];
						pdCZ[2] = pdZ[3];

						pdCX[3] = pdX[2];
						pdCY[3] = pdY[2];
						pdCZ[3] = pdZ[2];
					} else {
						pdCX[0] = pdX[0];
						pdCY[0] = pdY[0];
						pdCZ[0] = pdZ[0];

						pdCX[1] = pdX[1];
						pdCY[1] = pdY[1];
						pdCZ[1] = pdZ[1];

						pdCX[2] = dCX[0];
						pdCY[2] = dCY[0];
						pdCZ[2] = dCZ[0];

						pdCX[3] = dCX[1];
						pdCY[3] = dCY[1];
						pdCZ[3] = dCZ[1];
					}
					nRet = 4;
				}
   			}
   		}
		// 1-2が高くて0-3が低い
		else if ((pdZ[0] <= dZ && pdZ[1] >= dZ && pdZ[3] <= dZ && pdZ[2] >= dZ) ||
				 (pdZ[0] >= dZ && pdZ[1] <= dZ && pdZ[3] >= dZ && pdZ[2] <= dZ)) {
   			if (pdZ[0] > pdZ[1]) {
				if (!IsZero(pdZ[0] - pdZ[1])) {
               		dCX[1] = pdX[1] + (dZ - pdZ[1]) * ((pdX[0] - pdX[1]) / (pdZ[0] - pdZ[1]));
   					dCY[1] = pdY[1] + (dZ - pdZ[1]) * ((pdY[0] - pdY[1]) / (pdZ[0] - pdZ[1]));
					dCZ[1] = dZ;
				}
				if (!IsZero(pdZ[3] - pdZ[2])) {
               		dCX[0] = pdX[2] + (dZ - pdZ[2]) * ((pdX[3] - pdX[2]) / (pdZ[3] - pdZ[2]));
       	    		dCY[0] = pdY[2] + (dZ - pdZ[2]) * ((pdY[3] - pdY[2]) / (pdZ[3] - pdZ[2]));
					dCZ[0] = dZ;
				}

				if (!IsZero(pdZ[0] - pdZ[1]) && !IsZero(pdZ[3] - pdZ[2])) {
					// 補間値より上の面を取得
					if (bUpper) {
						pdCX[0] = pdX[0];
						pdCY[0] = pdY[0];
						pdCZ[0] = pdZ[0];

						pdCX[1] = dCX[1];
						pdCY[1] = dCY[1];
						pdCZ[1] = dCZ[1];

						pdCX[2] = dCX[0];
						pdCY[2] = dCY[0];
						pdCZ[2] = dCZ[0];

						pdCX[3] = pdX[3];
						pdCY[3] = pdY[3];
						pdCZ[3] = pdZ[3];
					} else {
						pdCX[0] = dCX[1];
						pdCY[0] = dCY[1];
						pdCZ[0] = dCZ[1];

						pdCX[1] = dCX[0];
						pdCY[1] = dCY[0];
						pdCZ[1] = dCZ[0];

						pdCX[2] = pdX[2];
						pdCY[2] = pdY[2];
						pdCZ[2] = pdZ[2];

						pdCX[3] = pdX[1];
						pdCY[3] = pdY[1];
						pdCZ[3] = pdZ[1];
					}
					nRet = 4;
				}
   			} else {
				if (!IsZero(pdZ[1] - pdZ[0])) {
					dCX[0] = pdX[0] + (dZ - pdZ[0]) * ((pdX[1] - pdX[0]) / (pdZ[1] - pdZ[0]));
					dCY[0] = pdY[0] + (dZ - pdZ[0]) * ((pdY[1] - pdY[0]) / (pdZ[1] - pdZ[0]));
					dCZ[0] = dZ;
				}
				if (!IsZero(pdZ[2] - pdZ[3])) {
					dCX[1] = pdX[3] + (dZ - pdZ[3]) * ((pdX[2] - pdX[3]) / (pdZ[2] - pdZ[3]));
   					dCY[1] = pdY[3] + (dZ - pdZ[3]) * ((pdY[2] - pdY[3]) / (pdZ[2] - pdZ[3]));
					dCZ[1] = dZ;
				}

				if (!IsZero(pdZ[1] - pdZ[0]) && !IsZero(pdZ[2] - pdZ[3])) {
					// 補間値より上の面を取得
					if (bUpper) {
						pdCX[0] = dCX[1];
						pdCY[0] = dCY[1];
						pdCZ[0] = dCZ[1];

						pdCX[1] = dCX[0];
						pdCY[1] = dCY[0];
						pdCZ[1] = dCZ[0];

						pdCX[2] = pdX[1];
						pdCY[2] = pdY[1];
						pdCZ[2] = pdZ[1];

						pdCX[3] = pdX[2];
						pdCY[3] = pdY[2];
						pdCZ[3] = pdZ[2];
					} else {
						pdCX[0] = pdX[0];
						pdCY[0] = pdY[0];
						pdCZ[0] = pdZ[0];

						pdCX[1] = dCX[0];
						pdCY[1] = dCY[0];
						pdCZ[1] = dCZ[0];

						pdCX[2] = dCX[1];
						pdCY[2] = dCY[1];
						pdCZ[2] = dCZ[1];

						pdCX[3] = pdX[3];
						pdCY[3] = pdY[3];
						pdCZ[3] = pdZ[3];
					}
					nRet = 4;
				}
   			}
		} else {
			double temp;
			for (int i = 0 ; i < 4 ; i++) {
				int nAfter  = (i + 1) % 4;
   				int nBefore = (i + 3) % 4;
				// １つだけ高い
   				if (pdZ[i] >= dZ && pdZ[nAfter] < dZ && pdZ[(i+2)%4] < dZ && pdZ[nBefore] < dZ) {
               		temp = (dZ - pdZ[nBefore]) / (pdZ[i] - pdZ[nBefore]);
					dCX[0] = pdX[nBefore] + (pdX[i] - pdX[nBefore]) * temp;
               		dCZ[0] = dZ;
		    		dCY[0] = pdY[nBefore] + (pdY[i] - pdY[nBefore]) * temp;
   					temp = (dZ - pdZ[nAfter]) / (pdZ[i] - pdZ[nAfter]);
               		dCX[1] = pdX[nAfter] + (pdX[i] - pdX[nAfter]) * temp;
	    			dCZ[1] = dZ;
               		dCY[1] = pdY[nAfter] + (pdY[i] - pdY[nAfter]) * temp;

					// 補間値より上の面を取得
					if (bUpper) {
						pdCX[0] = pdX[i];
						pdCY[0] = pdY[i];
						pdCZ[0] = pdZ[i];

						pdCX[1] = dCX[0];
						pdCY[1] = dCY[0];
						pdCZ[1] = dCZ[0];

						pdCX[2] = dCX[1];
						pdCY[2] = dCY[1];
						pdCZ[2] = dCZ[1];

						nRet = 3;
					} else {
						int k = i + 1;
						for (int j = 0 ; j < 3 ; j++) {
							if (k > 3) {k = 0;}
							pdCX[j] = pdX[k];
							pdCY[j] = pdY[k];
							pdCZ[j] = pdZ[k];
							k++;
						}
						pdCX[3] = dCX[0];
						pdCY[3] = dCY[0];
						pdCZ[3] = dCZ[0];

						pdCX[4] = dCX[1];
						pdCY[4] = dCY[1];
						pdCZ[4] = dCZ[1];

						nRet = 5;
					}
					break;
				}
   				// １つだけ低い
				if (pdZ[i] <= dZ && pdZ[nAfter] > dZ && pdZ[(i+2)%4] > dZ && pdZ[nBefore] > dZ) {
   					temp = (dZ - pdZ[i]) / (pdZ[nBefore] - pdZ[i]);
               		dCX[0] = pdX[i] + (pdX[nBefore] - pdX[i]) * temp;
   					dCZ[0] = dZ;
            		dCY[0] = pdY[i] + (pdY[nBefore] - pdY[i]) * temp;
   					temp = (dZ - pdZ[i]) / (pdZ[nAfter] - pdZ[i]);
               		dCX[1] = pdX[i] + (pdX[nAfter] - pdX[i]) * temp;
   					dCZ[1] = dZ;
               		dCY[1] = pdY[i] + (pdY[nAfter] - pdY[i]) * temp;

					// 補間値より上の面を取得
					if (bUpper) {
						int k = i + 1;
						for (int j = 0 ; j < 3 ; j++) {
							if (k > 3) {k = 0;}
							pdCX[j] = pdX[k];
							pdCY[j] = pdY[k];
							pdCZ[j] = pdZ[k];
							k++;
						}
						pdCX[3] = dCX[0];
						pdCY[3] = dCY[0];
						pdCZ[3] = dCZ[0];

						pdCX[4] = dCX[1];
						pdCY[4] = dCY[1];
						pdCZ[4] = dCZ[1];

						nRet = 5;
					} else {
						pdCX[0] = pdX[i];
						pdCY[0] = pdY[i];
						pdCZ[0] = pdZ[i];

						pdCX[1] = dCX[0];
						pdCY[1] = dCY[0];
						pdCZ[1] = dCZ[0];

						pdCX[2] = dCX[1];
						pdCY[2] = dCY[1];
						pdCZ[2] = dCZ[1];

						nRet = 3;
					}
					break;
   				}
   			}
   		}

		return nRet;
	}

	virtual void GetSliceLine(double const* pdX, double const* pdY, double const* pdZ, double const dContour, double* dCX, double* dCY, double* dCZ)
	{
		// 0-1が高いか低い
		if ((pdZ[1] <= dContour && pdZ[2] >= dContour && pdZ[0] <= dContour) ||
			(pdZ[1] >= dContour && pdZ[2] <= dContour && pdZ[0] >= dContour)){

			if (pdZ[0] > pdZ[2]) {
				if ((pdZ[0] - pdZ[2]) != 0.0) {
					dCZ[0] = dContour;
            		dCX[0] = pdX[2] + (dContour - pdZ[2]) * ((pdX[0] - pdX[2]) / (pdZ[0] - pdZ[2]));
            		dCY[0] = pdY[2] + (dContour - pdZ[2]) * ((pdY[0] - pdY[2]) / (pdZ[0] - pdZ[2]));
				}

			} else {
				if ((pdZ[2] - pdZ[0]) != 0.0) {
					dCZ[0] = dContour;
					dCX[0] = pdX[0] + (dContour - pdZ[0]) * ((pdX[2] - pdX[0]) / (pdZ[2] - pdZ[0]));
					dCY[0] = pdY[0] + (dContour - pdZ[0]) * ((pdY[2] - pdY[0]) / (pdZ[2] - pdZ[0]));
				}
			}

			if (pdZ[1] > pdZ[2]) {
				if ((pdZ[1] - pdZ[2]) != 0.0) {
					dCZ[1] = dContour;
    				dCX[1] = pdX[2] + (dContour - pdZ[2]) * ((pdX[1] - pdX[2]) / (pdZ[1] - pdZ[2]));
	    			dCY[1] = pdY[2] + (dContour - pdZ[2]) * ((pdY[1] - pdY[2]) / (pdZ[1] - pdZ[2]));
				}
			} else {
				if ((pdZ[2] - pdZ[1]) != 0.0) {
					dCZ[1] = dContour;
					dCX[1] = pdX[1] + (dContour - pdZ[1]) * ((pdX[2] - pdX[1]) / (pdZ[2] - pdZ[1]));
					dCY[1] = pdY[1] + (dContour - pdZ[1]) * ((pdY[2] - pdY[1]) / (pdZ[2] - pdZ[1]));
				}
			}
		}
		// 1-2が高いか低い
		else if ((pdZ[0] <= dContour && pdZ[1] >= dContour && pdZ[2] >= dContour) ||
   				 (pdZ[0] >= dContour && pdZ[1] <= dContour && pdZ[2] <= dContour)) {
			if (pdZ[0] > pdZ[1]) {
				if ((pdZ[0] - pdZ[1]) != 0.0) {
					dCZ[0] = dContour;
   					dCX[0] = pdX[1] + (dContour - pdZ[1]) * ((pdX[0] - pdX[1]) / (pdZ[0] - pdZ[1]));
    				dCY[0] = pdY[1] + (dContour - pdZ[1]) * ((pdY[0] - pdY[1]) / (pdZ[0] - pdZ[1]));
				}
			} else {
				if ((pdZ[1] - pdZ[0]) != 0.0) {
					dCZ[0] = dContour;
					dCX[0] = pdX[0] + (dContour - pdZ[0]) * ((pdX[1] - pdX[0]) / (pdZ[1] - pdZ[0]));
					dCY[0] = pdY[0] + (dContour - pdZ[0]) * ((pdY[1] - pdY[0]) / (pdZ[1] - pdZ[0]));
				}
			}

			if (pdZ[0] > pdZ[2]) {
				if ((pdZ[0] - pdZ[2]) != 0.0) {
					dCZ[1] = dContour;
					dCX[1] = pdX[2] + (dContour - pdZ[2]) * ((pdX[0] - pdX[2]) / (pdZ[0] - pdZ[2]));
    				dCY[1] = pdY[2] + (dContour - pdZ[2]) * ((pdY[0] - pdY[2]) / (pdZ[0] - pdZ[2]));
				}
			} else {
				if ((pdZ[2] - pdZ[0]) != 0.0) {
					dCZ[1] = dContour;
       				dCX[1] = pdX[0] + (dContour - pdZ[0]) * ((pdX[2] - pdX[0]) / (pdZ[2] - pdZ[0]));
      				dCY[1] = pdY[0] + (dContour - pdZ[0]) * ((pdY[2] - pdY[0]) / (pdZ[2] - pdZ[0]));
				}
			}
		}
		// 0-2が高いか低い
		else if ((pdZ[1] <= dContour && pdZ[0] >= dContour && pdZ[2] >= dContour) ||
   				 (pdZ[1] >= dContour && pdZ[0] <= dContour && pdZ[2] <= dContour)) {
			if (pdZ[1] > pdZ[0]) {
				if ((pdZ[1] - pdZ[0]) != 0.0) {
					dCZ[0] = dContour;
   					dCX[0] = pdX[0] + (dContour - pdZ[0]) * ((pdX[1] - pdX[0]) / (pdZ[1] - pdZ[0]));
    				dCY[0] = pdY[0] + (dContour - pdZ[0]) * ((pdY[1] - pdY[0]) / (pdZ[1] - pdZ[0]));
				}
			} else {
				if ((pdZ[0] - pdZ[1]) != 0.0) {
					dCZ[0] = dContour;
					dCX[0] = pdX[1] + (dContour - pdZ[1]) * ((pdX[0] - pdX[1]) / (pdZ[0] - pdZ[1]));
					dCY[0] = pdY[1] + (dContour - pdZ[1]) * ((pdY[0] - pdY[1]) / (pdZ[0] - pdZ[1]));
				}
			}

			if (pdZ[1] > pdZ[2]) {
				if ((pdZ[1] - pdZ[2]) != 0.0) {
					dCZ[1] = dContour;
					dCX[1] = pdX[2] + (dContour - pdZ[2]) * ((pdX[1] - pdX[2]) / (pdZ[1] - pdZ[2]));
    				dCY[1] = pdY[2] + (dContour - pdZ[2]) * ((pdY[1] - pdY[2]) / (pdZ[1] - pdZ[2]));
				}
			} else {
				if ((pdZ[2] - pdZ[1]) != 0.0) {
					dCZ[1] = dContour;
       				dCX[1] = pdX[1] + (dContour - pdZ[1]) * ((pdX[2] - pdX[1]) / (pdZ[2] - pdZ[1]));
      				dCY[1] = pdY[1] + (dContour - pdZ[1]) * ((pdY[2] - pdY[1]) / (pdZ[2] - pdZ[1]));
				}
			}
		}
	}

private:
	DelaunayTriangle* m_triangles;
	int m_triangleCount;
};