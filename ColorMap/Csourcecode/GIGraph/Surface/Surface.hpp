#pragma once

#include "Surface/SliceLine.hpp"
#include "Axis.h"

class Axis;
class IPlotData;

class OCSurface
{
public:
	OCSurface() : m_xMesh(1), m_yMesh(1)
	{
	}

	virtual ~OCSurface() {}

	int GetMeshNumX() const { return m_xMesh; }
	int GetMeshNumY() const { return m_yMesh; }

	void SetDecor(ContourDecor* decor) { m_decor = decor; }
	void SetData(CalcData* data) { m_data = data; }
	virtual bool Calculate(CalcData* data, int mesh, double alpha) = 0;

	void DrawWireFrame(COLORREF color)
	{
		::glEnable(GL_POLYGON_OFFSET_LINE);
		::glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		::glPolygonOffset(2, 2);
		::glColor3ub(GetRValue(color), GetGValue(color), GetBValue(color));
		::glTexCoord1d(0);
		::glLineWidth(1);
		::glLineStipple(1, LinePattern[0]);
		DrawWireFrameImpl();
		::glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		::glDisable(GL_POLYGON_OFFSET_LINE);
	}
	void DrawSliceLine()
	{
		::glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		::glColor3ub(0, 0, 0);
		::glTexCoord1d(0);
		DrawSliceLineImpl();
		::glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	void DrawSliceLineProjection()
	{
		::glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		::glColor3ub(0, 0, 0);
		::glTexCoord1d(0);
		DrawSliceLineProjectionImpl();
		::glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	virtual void DrawSurface() = 0;
	virtual void Projection() = 0;

	virtual bool GetVertexZ(double* , double* , double* , int , int ) {return false;}

	void SetSliceLine(std::vector<LevelLine> const& lines) { m_slices = lines; }

	double GetGridX(size_t index) const { return m_gridX.size() > index ? m_gridX[index] : 0; }
	double GetGridY(size_t index) const { return m_gridY.size() > index ? m_gridY[index] : 0; }
	double GetGridZ(size_t index) const { return m_gridZ.size() > index ? m_gridZ[index] : 0; }
	double GetGridT(size_t index) const { return m_gridT.size() > index ? m_gridT[index] : 0; }

	std::vector<double> /*const*/& GetGridX() /*const*/ { return m_gridX; }
	std::vector<double> /*const*/& GetGridY() /*const*/ { return m_gridY; }
	std::vector<double> /*const*/& GetGridZ() /*const*/ { return m_gridZ; }

protected:
	virtual void DrawWireFrameImpl() = 0;
	virtual void DrawSliceLineImpl() = 0;
	virtual void DrawSliceLineProjectionImpl() = 0;

	void RenderPlane(GLenum mode, double* x, double* y, double* z, double* tex, size_t length)
	{
		::glBegin(mode);
		for (size_t i = 0; i < length; ++i) {
			if (tex) { glTexCoord1d(tex[i]); }
			::glVertex3d(x[i], y[i], z[i]);
		}
		::glEnd();
	}

	void RenderProjection(GLenum mode, double* x, double* y, double* z, double* tex, size_t length)
	{
		::glEnable(GL_POLYGON_OFFSET_FILL);
		if (m_decor->yzProjection) {
			glBegin(mode);
			for (size_t i = 0; i < length; ++i) {
				if (tex) { glTexCoord1d(tex[i]); }
				glVertex3d(m_data->xSideValue, y[i], z[i]);
			}
			glEnd();
		}
		if (m_decor->xyProjection) {
			glBegin(mode);
			for (size_t i = 0; i < length; ++i) {
				if (tex) { glTexCoord1d(tex[i]); }
				glVertex3d(x[i], m_data->zMin, z[i]);
			}
			glEnd();
		}
		if (m_decor->zxProjection) {
			glBegin(mode);
			for (size_t i = 0; i < length; ++i) {
				if (tex) { glTexCoord1d(tex[i]); }
				glVertex3d(x[i], y[i], m_data->zSideValue);
			}
			glEnd();
		}
	}

	//! 等高線を描画します
	void PaintContour(double* pdX, double* pdY, double* pdZ, bool bProj)
	{
		for (size_t i = 0 ; i < m_slices.size(); ++i) {
			double dCX[2] = {}, dCY[2] = {}, dCZ[2] = {};
			double dContour = m_slices[i].level;

			GetSliceLine(pdX, pdY, pdZ, dContour, dCX, dCY, dCZ);

			if (dCX[0] == 0.0 && dCY[0] == 0.0 && dCZ[0] == 0.0 &&
				dCX[1] == 0.0 && dCY[1] == 0.0 && dCZ[1] == 0.0) { continue; }

			::glLineWidth(m_slices[i].width);
			::glLineStipple(1, LinePattern[m_slices[i].style]);

			double const t = (dContour + 5) / 10;
			double tex[] = { t, t };

			if (bProj) {
				RenderProjection(GL_LINES, dCX, dCZ, dCY, tex, 2);
			}
			else {
				RenderPlane(GL_LINES, dCX, dCZ, dCY, tex, 2);
			}
		}
	}

	virtual int CalcInterpolate(double const* pdX, double const* pdY, double const* pdZ, double const dZ, double* pdCX, double* pdCY, double* pdCZ, bool bUpper) = 0;
	virtual void GetSliceLine(double const* pdX, double const* pdY, double const* pdZ, double const dContour, double* dCX, double* dCY, double* dCZ) = 0;

protected:
	CalcData* m_data;
	ContourDecor* m_decor;
	int m_xMesh;
	int m_yMesh;

	std::vector<LevelLine> m_slices;

	std::vector<double> m_gridX;
	std::vector<double> m_gridY;
	std::vector<double> m_gridZ;
	std::vector<double> m_gridT;
};