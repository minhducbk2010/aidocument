#pragma once

#include "Surface/Surface.hpp"
#include "Surface/CalcContour.hpp"
#include "Interpolate/Spline.hpp"

class OCGriddingSurface : public OCSurface
{
public:
	OCGriddingSurface()
	{
	}

	virtual bool Calculate(CalcData* data, int mesh, double alpha)
	{
		m_data = data;

		const double Pm1 = 0, Pm2 = 1;
		m_xMesh = m_yMesh = mesh;

		m_gridZ.resize(m_xMesh * m_yMesh);
		CContour calculator;
		calculator.GriddingParam(Pm1, Pm2, alpha);
		calculator.Gridding(long(data->x.size()),
			&data->x[0], &data->y[0], &data->z[0],
			data->xMin, data->xMax, data->yMin, data->yMax,
			m_xMesh, m_yMesh, &m_gridZ[0]);

		for (size_t i = 0; i < m_gridZ.size(); ++i) {
			m_gridZ[i] = data->ConvertZ(m_gridZ[i]);
		}

		// 4Dの時はTも補間して求める
		if (m_data->dimension == 4) {
			m_gridT.resize(m_xMesh * m_yMesh);
			calculator.Gridding(long(data->x.size()),
				&data->x[0], &data->y[0], &data->t[0],
				data->xMin, data->xMax, data->yMin, data->yMax,
				m_xMesh, m_yMesh, &m_gridT[0]);

			for (size_t i = 0; i < m_gridT.size(); ++i) {
				m_gridT[i] = data->ConvertT(m_gridT[i]);
			}
		}
		else {
			// 3Dの時は z == t
			m_gridT = m_gridZ; // コピーのコストを考慮するべき?
		}

		// 格子
		m_gridX.resize(m_xMesh * m_yMesh);
		m_gridY.resize(m_xMesh * m_yMesh);
		double xGap = (data->xMax - data->xMin) / (m_xMesh - 1);
		double yGap = (data->yMax - data->yMin) / (m_yMesh - 1);
		for (int i = 0, n = 0; i < m_xMesh; ++i) {
			for (int j = 0; j < m_yMesh; ++j) {
				double const x = data->ConvertX(data->xMin + xGap * j);
				double const y = data->ConvertY(data->yMin + yGap * i);
				m_gridX[n] = x;
				m_gridY[n] = y;
				++n;
			}
		}
		std::swap(m_gridY, m_gridZ);

		m_xMin = data->ConvertX(data->xMin);
		m_xMax = data->ConvertX(data->xMax);
		m_yMin = data->ConvertY(data->yMax);
		m_yMax = data->ConvertY(data->yMin);
		return true;
	}

	double CalcZ(double x, double y,
		std::vector<double> /*const*/& xGrid, std::vector<double> /*const*/& yGrid, std::vector<double> /*const*/& zGrid)
	{
		OCSpline spline;

		std::vector<double> xInter(yGrid.size());
		for(size_t i = 0; i < yGrid.size(); ++i)
		{
			spline.Init((int)xGrid.size(), &xGrid[0], &zGrid[i * xGrid.size()]);
			spline.Prepare();
			xInter[i] = spline.Calc(x);
		}

		spline.Init((int)yGrid.size(), &yGrid[0], &xInter[0]);
		spline.Prepare();

		return spline.Calc(y);
	}

	void SetGrid(double const* x, double const* y, double const* z,
		int xMesh, int yMesh)
	{
		int const gridNum = xMesh * yMesh;
		m_gridX.assign(x, x + gridNum);
		m_gridY.assign(y, y + gridNum);
		m_gridZ.assign(z, z + gridNum);
		m_gridT = m_gridY;
		m_xMesh = xMesh;
		m_yMesh = yMesh;
	}

	void SetValue(int index, double z)
	{
		if (m_gridY.size() > static_cast<size_t>(index)) {
			m_gridY[index] = z;
			m_gridT[index] = z;
		}
	}

	void Clear()
	{
		m_gridX.clear();
		m_gridY.clear();
		m_gridZ.clear();
		m_xMesh = m_yMesh = 0;
	}

	//! ワイヤーフレームを描画します
	void DrawWireFrameImpl()
	{
		double dX[4], dY[4], dH[4], color[4];

		double const dZAxisMax = m_data->zMax;
		double const dZAxisMin = m_data->zMin;

		for (int i = 0 ; i < m_xMesh - 1 ; i++) {
			for (int j = 0 ; j < m_yMesh - 1 ; j++) {
				// １面の頂点座標を取得
				if (!GetVertexZ(dX, dY, dH, i, j)) { continue; }
				GetVertexT(color, i, j);

				::glBegin(GL_QUADS);
					for (int k = 0; k < 4; ++k) {
						GLdouble const tex = (color[k] - dZAxisMin) / (dZAxisMax - dZAxisMin);
						::glTexCoord1d(tex);
						::glVertex3d(dX[k], dY[k], dH[k]);
					}
				::glEnd();
			}
		}
	}

	//! 等高線を描画します
	virtual void DrawSliceLineImpl()
	{
		if (m_decor->renderSliceLine) {
			DrawSliceLineImpl(false);
		}
	}

	virtual void DrawSliceLineProjectionImpl()
	{
		if (m_decor->renderSliceLineProjection) {
			DrawSliceLineImpl(true);
		}
	}

	void DrawSliceLineImpl(bool value)
	{
		double dX[4], dY[4], dH[4];
		for (int i = 0 ; i < m_xMesh - 1 ; i++) {
			for (int j = 0 ; j < m_yMesh - 1 ; j++) {
				if (!GetVertexZ(dX, dY, dH, i, j)) { continue; }
				PaintContour(dX, dH, dY, value);
			}
		}
	}

	void DrawSurface()
	{
		DrawMap(m_decor->coloring, false);
	}

	void Projection()
	{
		DrawMap(m_decor->coloringProjection, true);
	}
	
	void DrawXLines()
	{
		double dX[4], dY[4], dH[4];
		for (int i = 0 ; i < m_yMesh - 1; ++i) {
			::glLoadName(i);
			for (int j = 0 ; j < m_xMesh - 1; ++j) {
				// １面の頂点座標を取得
				if (!GetVertexZ(dX, dY, dH, j, i)) { continue; }

				std::vector<double> x, y, z;
				for (int k = 0; k < 4; ++k) {
					x.push_back(dX[k]);
					y.push_back(dY[k]);
					z.push_back(dH[k]);
				}

				::glBegin(GL_QUADS);
				for (int k = 0; k < 4; ++k) {
					::glVertex3d(x[k], y[k], z[k]);
				}
				::glEnd();
			}
		}
	}

	void DrawXLine(int index)
	{
		std::vector<Vertex> arr;
		for (int k = 0; k < m_xMesh; ++k) {
			int const nx = k + m_xMesh * index;
			if (m_gridX.size() <= nx) { break; }
			Vertex v(m_gridX[nx], m_gridY[nx], m_gridZ[nx]);
			arr.push_back(v);
		}
		DrawXLine(arr);
	}

	void DrawXLine(std::vector<Vertex> const& v, bool test=false)
	{
		::glColor3ub(255, 0, 0);
		::glLineStipple(1, LinePattern[test ? 1 : 0]);
		::glLineWidth(2);
		::glBegin(GL_LINE_STRIP);
		for (size_t i = 0; i < v.size(); ++i) {
			::glVertex3d(v[i].X, v[i].Y, v[i].Z);
		}
		::glLineWidth(1);
		::glLineStipple(1, LinePattern[0]);
		::glEnd();
	}

	void DrawYLines()
	{
		double dX[4], dY[4], dH[4];
		for (int i = 0 ; i < m_xMesh - 1; ++i) {
			::glLoadName(i);
			for (int j = 0 ; j < m_yMesh - 1; ++j) {
				// １面の頂点座標を取得
				if (!GetVertexZ(dX, dY, dH, i, j)) { continue; }

				std::vector<double> x, y, z;
				for (int k = 0; k < 4; ++k) {
					x.push_back(dX[k]);
					y.push_back(dY[k]);
					z.push_back(dH[k]);
				}

				::glBegin(GL_QUADS);
				for (int k = 0; k < 4; ++k) {
					::glVertex3d(x[k], y[k], z[k]);
				}
				::glEnd();
			}
		}
	}

	void DrawYLine(int index)
	{
		std::vector<Vertex> arr;
		for (int k = 0; k < m_yMesh; ++k) {
			int const ny = k * m_yMesh + index;
			if (m_gridX.size() <= ny) { break; }
			Vertex v(m_gridX[ny], m_gridY[ny], m_gridZ[ny]);
			arr.push_back(v);
		}
		DrawYLine(arr);
	}

	void DrawYLine(std::vector<Vertex> const& v, bool test=false)
	{
		::glLineWidth(2);
		::glColor3ub(0, 0, 255);
		::glLineStipple(1, LinePattern[test ? 1 : 0]);
		::glBegin(GL_LINE_STRIP);
		for (size_t i = 0; i < v.size(); ++i) {
			::glVertex3d(v[i].X, v[i].Y, v[i].Z);
		}
		::glEnd();
		::glLineWidth(1);
		::glLineStipple(1, LinePattern[0]);
	}

private:
	//! マップを描画します
	void DrawMap(bool color, bool proj)
	{
		::glPushMatrix();
		::glPolygonOffset(5.0, 5.0);
		//::glPolygonOffset(-1, -1);
		DrawMapImpl(color, proj);
		::glPopMatrix();
	}

	void DrawMapImpl(bool color, bool proj)
	{
		double dX[4], dY[4], dH[4];

		for (int i = 0 ; i < m_xMesh - 1 ; i++) {
			for (int j = 0 ; j < m_yMesh - 1 ; j++) {
				// １面の頂点座標を取得
				if (!GetVertexZ(dX, dY, dH, i, j)) { continue; }

				double dT[5] = {};
				GetVertexT(dT, i, j);

				double dHWari[5] = {};
				for (int k = 0 ; k < 4 ; k++) {
					dHWari[k] = (dT[k] - m_data->zMin) / (m_data->zMax- m_data->zMin);
				}

				// 投影
				if (proj) {
					RenderProjection(GL_POLYGON, dX, dY, dH, dHWari, 4);
				}
				else {
					RenderPlane(GL_POLYGON, dX, dY, dH, dHWari, 4);
				}
			}
		}

		::glFinish();
	}

	virtual int CalcInterpolate(double const* pdX, double const* pdY, double const* pdZ, double const dZ, double* pdCX, double* pdCY, double* pdCZ, bool bUpper)
	{
		int nRet = 0;
		double dCX[2], dCY[2], dCZ[2];

		// 0-1が高くて3-2が低い
   		if ((pdZ[1] <= dZ && pdZ[2] >= dZ && pdZ[0] <= dZ && pdZ[3] >= dZ) ||
			(pdZ[1] >= dZ && pdZ[2] <= dZ && pdZ[0] >= dZ && pdZ[3] <= dZ)){
   			if (pdZ[0] > pdZ[3]) {
				if (!IsZero(pdZ[0] - pdZ[3])) {
               		dCX[0] = pdX[3] + (dZ - pdZ[3]) * ((pdX[0] - pdX[3]) / (pdZ[0] - pdZ[3]));
               		dCY[0] = pdY[3] + (dZ - pdZ[3]) * ((pdY[0] - pdY[3]) / (pdZ[0] - pdZ[3]));
					dCZ[0] = dZ;
				}
				if (!IsZero(pdZ[1] - pdZ[2])) {
            		dCX[1] = pdX[2] + (dZ - pdZ[2]) * ((pdX[1] - pdX[2]) / (pdZ[1] - pdZ[2]));
   	       			dCY[1] = pdY[2] + (dZ - pdZ[2]) * ((pdY[1] - pdY[2]) / (pdZ[1] - pdZ[2]));
					dCZ[1] = dZ;
				}

				if (!IsZero(pdZ[0] - pdZ[3]) && !IsZero(pdZ[1] - pdZ[2])) {
					// 補間値より上の面を取得
					if (bUpper) {
						pdCX[0] = pdX[0];
						pdCY[0] = pdY[0];
						pdCZ[0] = pdZ[0];

						pdCX[1] = pdX[1];
						pdCY[1] = pdY[1];
						pdCZ[1] = pdZ[1];

						pdCX[2] = dCX[1];
						pdCY[2] = dCY[1];
						pdCZ[2] = dCZ[1];

						pdCX[3] = dCX[0];
						pdCY[3] = dCY[0];
						pdCZ[3] = dCZ[0];
					} else {
						pdCX[0] = dCX[0];
						pdCY[0] = dCY[0];
						pdCZ[0] = dCZ[0];

						pdCX[1] = dCX[1];
						pdCY[1] = dCY[1];
						pdCZ[1] = dCZ[1];

						pdCX[2] = pdX[2];
						pdCY[2] = pdY[2];
						pdCZ[2] = pdZ[2];

						pdCX[3] = pdX[3];
						pdCY[3] = pdY[3];
						pdCZ[3] = pdZ[3];
					}
					nRet = 4;
				}
   			} else {
				if (!IsZero(pdZ[3] - pdZ[0])) {
					dCX[1] = pdX[0] + (dZ - pdZ[0]) * ((pdX[3] - pdX[0]) / (pdZ[3] - pdZ[0]));
					dCY[1] = pdY[0] + (dZ - pdZ[0]) * ((pdY[3] - pdY[0]) / (pdZ[3] - pdZ[0]));
					dCZ[1] = dZ;
				}
				if (!IsZero(pdZ[2] - pdZ[1])) {
					dCX[0] = pdX[1] + (dZ - pdZ[1]) * ((pdX[2] - pdX[1]) / (pdZ[2] - pdZ[1]));
					dCY[0] = pdY[1] + (dZ - pdZ[1]) * ((pdY[2] - pdY[1]) / (pdZ[2] - pdZ[1]));
					dCZ[0] = dZ;
				}

				if (!IsZero(pdZ[3] - pdZ[0]) && !IsZero(pdZ[2] - pdZ[1])) {
					// 補間値より上の面を取得
					if (bUpper) {
						pdCX[0] = dCX[0];
						pdCY[0] = dCY[0];
						pdCZ[0] = dCZ[0];

						pdCX[1] = dCX[1];
						pdCY[1] = dCY[1];
						pdCZ[1] = dCZ[1];

						pdCX[2] = pdX[3];
						pdCY[2] = pdY[3];
						pdCZ[2] = pdZ[3];

						pdCX[3] = pdX[2];
						pdCY[3] = pdY[2];
						pdCZ[3] = pdZ[2];
					} else {
						pdCX[0] = pdX[0];
						pdCY[0] = pdY[0];
						pdCZ[0] = pdZ[0];

						pdCX[1] = pdX[1];
						pdCY[1] = pdY[1];
						pdCZ[1] = pdZ[1];

						pdCX[2] = dCX[0];
						pdCY[2] = dCY[0];
						pdCZ[2] = dCZ[0];

						pdCX[3] = dCX[1];
						pdCY[3] = dCY[1];
						pdCZ[3] = dCZ[1];
					}
					nRet = 4;
				}
   			}
   		}
		// 1-2が高くて0-3が低い
		else if ((pdZ[0] <= dZ && pdZ[1] >= dZ && pdZ[3] <= dZ && pdZ[2] >= dZ) ||
				 (pdZ[0] >= dZ && pdZ[1] <= dZ && pdZ[3] >= dZ && pdZ[2] <= dZ)) {
   			if (pdZ[0] > pdZ[1]) {
				if (!IsZero(pdZ[0] - pdZ[1])) {
               		dCX[1] = pdX[1] + (dZ - pdZ[1]) * ((pdX[0] - pdX[1]) / (pdZ[0] - pdZ[1]));
   					dCY[1] = pdY[1] + (dZ - pdZ[1]) * ((pdY[0] - pdY[1]) / (pdZ[0] - pdZ[1]));
					dCZ[1] = dZ;
				}
				if (!IsZero(pdZ[3] - pdZ[2])) {
               		dCX[0] = pdX[2] + (dZ - pdZ[2]) * ((pdX[3] - pdX[2]) / (pdZ[3] - pdZ[2]));
       	    		dCY[0] = pdY[2] + (dZ - pdZ[2]) * ((pdY[3] - pdY[2]) / (pdZ[3] - pdZ[2]));
					dCZ[0] = dZ;
				}

				if (!IsZero(pdZ[0] - pdZ[1]) && !IsZero(pdZ[3] - pdZ[2])) {
					// 補間値より上の面を取得
					if (bUpper) {
						pdCX[0] = pdX[0];
						pdCY[0] = pdY[0];
						pdCZ[0] = pdZ[0];

						pdCX[1] = dCX[1];
						pdCY[1] = dCY[1];
						pdCZ[1] = dCZ[1];

						pdCX[2] = dCX[0];
						pdCY[2] = dCY[0];
						pdCZ[2] = dCZ[0];

						pdCX[3] = pdX[3];
						pdCY[3] = pdY[3];
						pdCZ[3] = pdZ[3];
					} else {
						pdCX[0] = dCX[1];
						pdCY[0] = dCY[1];
						pdCZ[0] = dCZ[1];

						pdCX[1] = dCX[0];
						pdCY[1] = dCY[0];
						pdCZ[1] = dCZ[0];

						pdCX[2] = pdX[2];
						pdCY[2] = pdY[2];
						pdCZ[2] = pdZ[2];

						pdCX[3] = pdX[1];
						pdCY[3] = pdY[1];
						pdCZ[3] = pdZ[1];
					}
					nRet = 4;
				}
   			} else {
				if (!IsZero(pdZ[1] - pdZ[0])) {
					dCX[0] = pdX[0] + (dZ - pdZ[0]) * ((pdX[1] - pdX[0]) / (pdZ[1] - pdZ[0]));
					dCY[0] = pdY[0] + (dZ - pdZ[0]) * ((pdY[1] - pdY[0]) / (pdZ[1] - pdZ[0]));
					dCZ[0] = dZ;
				}
				if (!IsZero(pdZ[2] - pdZ[3])) {
					dCX[1] = pdX[3] + (dZ - pdZ[3]) * ((pdX[2] - pdX[3]) / (pdZ[2] - pdZ[3]));
   					dCY[1] = pdY[3] + (dZ - pdZ[3]) * ((pdY[2] - pdY[3]) / (pdZ[2] - pdZ[3]));
					dCZ[1] = dZ;
				}

				if (!IsZero(pdZ[1] - pdZ[0]) && !IsZero(pdZ[2] - pdZ[3])) {
					// 補間値より上の面を取得
					if (bUpper) {
						pdCX[0] = dCX[1];
						pdCY[0] = dCY[1];
						pdCZ[0] = dCZ[1];

						pdCX[1] = dCX[0];
						pdCY[1] = dCY[0];
						pdCZ[1] = dCZ[0];

						pdCX[2] = pdX[1];
						pdCY[2] = pdY[1];
						pdCZ[2] = pdZ[1];

						pdCX[3] = pdX[2];
						pdCY[3] = pdY[2];
						pdCZ[3] = pdZ[2];
					} else {
						pdCX[0] = pdX[0];
						pdCY[0] = pdY[0];
						pdCZ[0] = pdZ[0];

						pdCX[1] = dCX[0];
						pdCY[1] = dCY[0];
						pdCZ[1] = dCZ[0];

						pdCX[2] = dCX[1];
						pdCY[2] = dCY[1];
						pdCZ[2] = dCZ[1];

						pdCX[3] = pdX[3];
						pdCY[3] = pdY[3];
						pdCZ[3] = pdZ[3];
					}
					nRet = 4;
				}
   			}
		} else {
			double temp;
			for (int i = 0 ; i < 4 ; i++) {
				int nAfter  = (i + 1) % 4;
   				int nBefore = (i + 3) % 4;
				// １つだけ高い
   				if (pdZ[i] >= dZ && pdZ[nAfter] < dZ && pdZ[(i+2)%4] < dZ && pdZ[nBefore] < dZ) {
               		temp = (dZ - pdZ[nBefore]) / (pdZ[i] - pdZ[nBefore]);
					dCX[0] = pdX[nBefore] + (pdX[i] - pdX[nBefore]) * temp;
               		dCZ[0] = dZ;
		    		dCY[0] = pdY[nBefore] + (pdY[i] - pdY[nBefore]) * temp;
   					temp = (dZ - pdZ[nAfter]) / (pdZ[i] - pdZ[nAfter]);
               		dCX[1] = pdX[nAfter] + (pdX[i] - pdX[nAfter]) * temp;
	    			dCZ[1] = dZ;
               		dCY[1] = pdY[nAfter] + (pdY[i] - pdY[nAfter]) * temp;

					// 補間値より上の面を取得
					if (bUpper) {
						pdCX[0] = pdX[i];
						pdCY[0] = pdY[i];
						pdCZ[0] = pdZ[i];

						pdCX[1] = dCX[0];
						pdCY[1] = dCY[0];
						pdCZ[1] = dCZ[0];

						pdCX[2] = dCX[1];
						pdCY[2] = dCY[1];
						pdCZ[2] = dCZ[1];

						nRet = 3;
					} else {
						int k = i + 1;
						for (int j = 0 ; j < 3 ; j++) {
							if (k > 3) {k = 0;}
							pdCX[j] = pdX[k];
							pdCY[j] = pdY[k];
							pdCZ[j] = pdZ[k];
							k++;
						}
						pdCX[3] = dCX[0];
						pdCY[3] = dCY[0];
						pdCZ[3] = dCZ[0];

						pdCX[4] = dCX[1];
						pdCY[4] = dCY[1];
						pdCZ[4] = dCZ[1];

						nRet = 5;
					}
					break;
				}
   				// １つだけ低い
				if (pdZ[i] <= dZ && pdZ[nAfter] > dZ && pdZ[(i+2)%4] > dZ && pdZ[nBefore] > dZ) {
   					temp = (dZ - pdZ[i]) / (pdZ[nBefore] - pdZ[i]);
               		dCX[0] = pdX[i] + (pdX[nBefore] - pdX[i]) * temp;
   					dCZ[0] = dZ;
            		dCY[0] = pdY[i] + (pdY[nBefore] - pdY[i]) * temp;
   					temp = (dZ - pdZ[i]) / (pdZ[nAfter] - pdZ[i]);
               		dCX[1] = pdX[i] + (pdX[nAfter] - pdX[i]) * temp;
   					dCZ[1] = dZ;
               		dCY[1] = pdY[i] + (pdY[nAfter] - pdY[i]) * temp;

					// 補間値より上の面を取得
					if (bUpper) {
						int k = i + 1;
						for (int j = 0 ; j < 3 ; j++) {
							if (k > 3) {k = 0;}
							pdCX[j] = pdX[k];
							pdCY[j] = pdY[k];
							pdCZ[j] = pdZ[k];
							k++;
						}
						pdCX[3] = dCX[0];
						pdCY[3] = dCY[0];
						pdCZ[3] = dCZ[0];

						pdCX[4] = dCX[1];
						pdCY[4] = dCY[1];
						pdCZ[4] = dCZ[1];

						nRet = 5;
					} else {
						pdCX[0] = pdX[i];
						pdCY[0] = pdY[i];
						pdCZ[0] = pdZ[i];

						pdCX[1] = dCX[0];
						pdCY[1] = dCY[0];
						pdCZ[1] = dCZ[0];

						pdCX[2] = dCX[1];
						pdCY[2] = dCY[1];
						pdCZ[2] = dCZ[1];

						nRet = 3;
					}
					break;
   				}
   			}
   		}
		return nRet;
	}

	virtual void GetSliceLine(double const* pdX, double const* pdY, double const* pdZ, double const dContour, double* dCX, double* dCY, double* dCZ)
	{
		// 0-1が高くて3-2が低い
		if ((pdZ[1] <= dContour && pdZ[2] >= dContour && pdZ[0] <= dContour && pdZ[3] >= dContour) ||
			(pdZ[1] >= dContour && pdZ[2] <= dContour && pdZ[0] >= dContour && pdZ[3] <= dContour)){
			if (pdZ[0] > pdZ[3]) {
				if ((pdZ[0] - pdZ[3]) != 0.0) {
					dCZ[0] = dContour;
            		dCX[0] = pdX[3] + (dContour - pdZ[3]) * ((pdX[0] - pdX[3]) / (pdZ[0] - pdZ[3]));
            		dCY[0] = pdY[3] + (dContour - pdZ[3]) * ((pdY[0] - pdY[3]) / (pdZ[0] - pdZ[3]));
				}
			} else {
				if ((pdZ[3] - pdZ[0]) != 0.0) {
					dCZ[0] = dContour;
					dCX[0] = pdX[0] + (dContour - pdZ[0]) * ((pdX[3] - pdX[0]) / (pdZ[3] - pdZ[0]));
					dCY[0] = pdY[0] + (dContour - pdZ[0]) * ((pdY[3] - pdY[0]) / (pdZ[3] - pdZ[0]));
				}
			}

			if (pdZ[1] > pdZ[2]) {
				if ((pdZ[1] - pdZ[2]) != 0.0) {
					dCZ[1] = dContour;
    				dCX[1] = pdX[2] + (dContour - pdZ[2]) * ((pdX[1] - pdX[2]) / (pdZ[1] - pdZ[2]));
	    			dCY[1] = pdY[2] + (dContour - pdZ[2]) * ((pdY[1] - pdY[2]) / (pdZ[1] - pdZ[2]));
				}
			} else {
				if ((pdZ[2] - pdZ[1]) != 0.0) {
					dCZ[1] = dContour;
					dCX[1] = pdX[1] + (dContour - pdZ[1]) * ((pdX[2] - pdX[1]) / (pdZ[2] - pdZ[1]));
					dCY[1] = pdY[1] + (dContour - pdZ[1]) * ((pdY[2] - pdY[1]) / (pdZ[2] - pdZ[1]));
				}
			}
		}
		// 1-2が高くて0-3が低い
		else if ((pdZ[0] <= dContour && pdZ[1] >= dContour && pdZ[3] <= dContour && pdZ[2] >= dContour) ||
   				 (pdZ[0] >= dContour && pdZ[1] <= dContour && pdZ[3] >= dContour && pdZ[2] <= dContour)) {
			if (pdZ[0] > pdZ[1]) {
				if ((pdZ[0] - pdZ[1]) != 0.0) {
					dCZ[0] = dContour;
    				dCX[0] = pdX[1] + (dContour - pdZ[1]) * ((pdX[0] - pdX[1]) / (pdZ[0] - pdZ[1]));
	    			dCY[0] = pdY[1] + (dContour - pdZ[1]) * ((pdY[0] - pdY[1]) / (pdZ[0] - pdZ[1]));
				}
			} else {
				if ((pdZ[1] - pdZ[0]) != 0.0) {
					dCZ[0] = dContour;
					dCX[0] = pdX[0] + (dContour - pdZ[0]) * ((pdX[1] - pdX[0]) / (pdZ[1] - pdZ[0]));
					dCY[0] = pdY[0] + (dContour - pdZ[0]) * ((pdY[1] - pdY[0]) / (pdZ[1] - pdZ[0]));
				}
			}

			if (pdZ[3] > pdZ[2]) {
				if ((pdZ[3] - pdZ[2]) != 0.0) {
					dCZ[1] = dContour;
    				dCX[1] = pdX[2] + (dContour - pdZ[2]) * ((pdX[3] - pdX[2]) / (pdZ[3] - pdZ[2]));
    				dCY[1] = pdY[2] + (dContour - pdZ[2]) * ((pdY[3] - pdY[2]) / (pdZ[3] - pdZ[2]));
				}
			} else {
				if ((pdZ[2] - pdZ[3]) != 0.0) {
    				dCZ[1] = dContour;
       				dCX[1] = pdX[3] + (dContour - pdZ[3]) * ((pdX[2] - pdX[3]) / (pdZ[2] - pdZ[3]));
   					dCY[1] = pdY[3] + (dContour - pdZ[3]) * ((pdY[2] - pdY[3]) / (pdZ[2] - pdZ[3]));
				}
			}
		} else {
			for (int i = 0 ; i < 4 ; i++) {
				int nAfter  = (i + 1) % 4;
				int nBefore = (i + 3) % 4;
				// １つだけ高い
				if (pdZ[i] >= dContour && pdZ[nAfter] < dContour &&
    				pdZ[(i+2)%4] < dContour && pdZ[nBefore] < dContour) {
					double temp = (dContour - pdZ[nBefore]) / (pdZ[i] - pdZ[nBefore]);
					dCX[0] = pdX[nBefore] + (pdX[i] - pdX[nBefore]) * temp;
					dCZ[0] = dContour;
					dCY[0] = pdY[nBefore] + (pdY[i] - pdY[nBefore]) * temp;

					temp = (dContour - pdZ[nAfter]) / (pdZ[i] - pdZ[nAfter]);
					dCX[1] = pdX[nAfter] + (pdX[i] - pdX[nAfter]) * temp;
					dCZ[1] = dContour;
					dCY[1] = pdY[nAfter] + (pdY[i] - pdY[nAfter]) * temp;
					break;
				}
				// １つだけ低い
				if (pdZ[i] <= dContour && pdZ[nAfter] > dContour &&
    				pdZ[(i+2)%4] > dContour && pdZ[nBefore] > dContour) {
					double temp = (dContour - pdZ[i]) / (pdZ[nBefore] - pdZ[i]);
					dCX[0] = pdX[i] + (pdX[nBefore] - pdX[i]) * temp;
					dCZ[0] = dContour;
					dCY[0] = pdY[i] + (pdY[nBefore] - pdY[i]) * temp;

					temp = (dContour - pdZ[i]) / (pdZ[nAfter] - pdZ[i]);
					dCX[1] = pdX[i] + (pdX[nAfter] - pdX[i]) * temp;
					dCZ[1] = dContour;
					dCY[1] = pdY[i] + (pdY[nAfter] - pdY[i]) * temp;
					break;
				}
			}
		}
	}

	//! 1面の頂点座標を取得します
	bool GetVertexZ(double* x, double* y, double* z, int xMesh, int yMesh)
	{
		x[0] = m_gridX[xMesh + m_xMesh * yMesh];
		y[0] = m_gridY[xMesh + m_xMesh * yMesh];
		z[0] = m_gridZ[xMesh + m_xMesh * yMesh];
		x[1] = m_gridX[m_xMesh + xMesh + m_xMesh * yMesh];
		y[1] = m_gridY[m_xMesh + xMesh + m_xMesh * yMesh];
		z[1] = m_gridZ[m_xMesh + xMesh + m_xMesh * yMesh];
		x[3] = m_gridX[xMesh + m_xMesh * yMesh + 1];
		y[3] = m_gridY[xMesh + m_xMesh * yMesh + 1];
		z[3] = m_gridZ[xMesh + m_xMesh * yMesh + 1];
		x[2] = m_gridX[m_xMesh + xMesh + m_xMesh * yMesh + 1];
		y[2] = m_gridY[m_xMesh + xMesh + m_xMesh * yMesh + 1];
		z[2] = m_gridZ[m_xMesh + xMesh + m_xMesh * yMesh + 1];

		for (int i = 0; i < 4; ++i) {
			if (x[i] == FLT_MAX || y[i] == FLT_MAX || z[i] == FLT_MAX) {
				return false;
			}
		}

		return true;
	}

	bool GetVertexT(double* t, int xMesh, int yMesh)
	{
		t[0] = m_gridT[xMesh + m_xMesh * yMesh];
		t[1] = m_gridT[m_xMesh + xMesh + m_xMesh * yMesh];
		t[3] = m_gridT[xMesh + m_xMesh * yMesh + 1];
		t[2] = m_gridT[m_xMesh + xMesh + m_xMesh * yMesh + 1];
		return true;
	}

private:
	double m_xMax, m_xMin;
	double m_yMax, m_yMin;
};