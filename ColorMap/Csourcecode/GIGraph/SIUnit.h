#pragma once

class SIUnit
{
public:
	static void Convert(double value, wchar_t *si, size_t count);

private:
	static void ConvertToString(double value, int digit, wchar_t *si, size_t count);
	static double TakeUp(double value, int *rank);
	static double SpreadOut(double value, int *rank);
	static double RoundUp(double value, int digit);

	static const wchar_t LargeUnit[];
	static const wchar_t SmallUnit[];
	static const int Digit;
};
