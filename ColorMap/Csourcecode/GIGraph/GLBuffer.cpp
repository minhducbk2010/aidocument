#include "stdafx.h"
#include "GLBuffer.h"
#include "GLExtension.h"

GLBuffer::GLBuffer(GLenum target, int size, const GLvoid *data, GLenum usage)
{
	this->target = target;

	if (GLExtension::GenBuffersARB(1, &id))
	{
		buffer = NULL;
		GLExtension::BindBufferARB(target, id);
		GLExtension::BufferDataARB(target, size, data, usage);
		GLExtension::BindBufferARB(target, 0);
	}
	else
	{
		buffer = new BYTE[size];
		memcpy(buffer, data, size);
	}
}
GLBuffer::~GLBuffer()
{
	if (buffer == NULL)
	{
		GLExtension::DeleteBuffersARB(1, &id);
	}

	delete []buffer;
}

void *GLBuffer::Bind()
{
	if (buffer != NULL)
	{
		return (void*)buffer;
	}
	else
	{
		GLExtension::BindBufferARB(target, id);
		return NULL;
	}
}
void GLBuffer::Unbind()
{
	if (buffer == NULL)
	{
		GLExtension::BindBufferARB(target, 0);
	}
}
GLvoid *GLBuffer::Map(GLenum access)
{
	if (buffer != NULL)
	{
		return (GLvoid*)buffer;
	}
	else
	{
		GLExtension::BindBufferARB(target, id);
		return GLExtension::MapBufferARB(target, access);
	}
}
GLboolean GLBuffer::Unmap()
{
	if (buffer != NULL)
	{
		return GL_TRUE;
	}
	else
	{
		return GLExtension::UnmapBufferARB(target);
	}
}
