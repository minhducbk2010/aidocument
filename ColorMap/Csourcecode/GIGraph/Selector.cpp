/*!	@file
*/
#include "StdAfx.h"
#include "Selector.h"

Selector::Selector(int x, int y)
{
	::glSelectBuffer(BUFSIZE, m_selectBuffer);
    ::glRenderMode(GL_SELECT);
    ::glInitNames();
    ::glPushName(static_cast<GLuint>(-1));

	//::glPushMatrix();
	::glMatrixMode(GL_PROJECTION);
	::glLoadIdentity();

    GLint viewport[4];
    ::glGetIntegerv(GL_VIEWPORT, viewport);
	GLdouble const th = 5;
	::gluPickMatrix(x, viewport[3] - y, th, th, viewport);

	double const w = 22;
	double const h = 22;
	glOrtho(-w, w, -h, h, -300, 300);
}

Selector::~Selector()
{
	::glPopName();
	//::glPopMatrix();

	::glMatrixMode(GL_PROJECTION);
	::glMatrixMode(GL_MODELVIEW);
}

int Selector::GetPoint()
{
	GLuint const hit = ::glRenderMode(GL_RENDER);
    return SelectHits(hit);
}

int Selector::SelectHits(GLuint hits)
{
    // ヒットしたデータがない場合
    if (hits <= 0) {
        return -1;
    }

    double fDepthMin = 10.0f;

    // 作業用ポインタを用意
    GLuint* pnTmpBuf = m_selectBuffer;

	GLuint nHitName = GLuint(-1);
    for (GLuint i = 0; i < hits; ++i) {
        ++pnTmpBuf;
        double const fDepth = double(*pnTmpBuf / 0x7fffffff);
        ++pnTmpBuf;
        ++pnTmpBuf;
        // 最小デプスの確認
        if (fDepthMin > fDepth) {
            fDepthMin = fDepth;
            nHitName = *pnTmpBuf;
        }
        pnTmpBuf++;
    }

    return nHitName;
}