/*!	@file
*/
#pragma once

#include <boost/function.hpp>

class ColorTexture;

struct ContourDecor
{
	ContourDecor()
		: filling(true), coloring(true)
		, enableProjection(false), xyProjection(false), yzProjection(false), zxProjection(false)
		, coloringProjection(false), fillingProjection(false)
	{}

	bool filling;
	bool coloring;

	bool enableProjection;
	bool xyProjection;
	bool yzProjection;
	bool zxProjection;
	bool coloringProjection;
	bool fillingProjection;

	bool renderSliceLine;
	bool renderSliceLineProjection;

	ColorTexture* texture;
};

struct CalcData
{
	CalcData()
		: dimension(3)
	{}

	int dimension;

	std::vector<double> x;
	std::vector<double> y;
	std::vector<double> z;
	std::vector<double> t;
	double xMin, xMax, yMin, yMax, zMin, zMax;

	double xSideValue;
	double zSideValue;

	boost::function<double(double)> ConvertX;
	boost::function<double(double)> ConvertY;
	boost::function<double(double)> ConvertZ;
	boost::function<double(double)> ConvertT;
};
