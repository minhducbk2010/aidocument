// DeviceContext.h
#pragma once

#include <boost/shared_ptr.hpp>

class DeviceContext;
typedef boost::shared_ptr<DeviceContext> DeviceContextPtr;

class DeviceContext
{
public:
	virtual ~DeviceContext();

	static DeviceContextPtr CreateInstance(HWND window);
	static DeviceContextPtr CreateInstance(HDC device);

	HDC GetGDIDevice() const { return device; }

	BOOL MakeCurrentContext();
	BOOL SwapBuffers();
	BOOL RelaseCurrentContext();

protected:
	DeviceContext(HDC device, HGLRC context);

private:
	HWND wnd;
	HDC device;
	HGLRC context;
};
