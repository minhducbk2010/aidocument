#include "stdafx.h"
#include "GLExtension.h"

bool GLExtension::vboSupported = false;
PFNGLGENBUFFERSARBPROC GLExtension::glGenBuffersARB = NULL;
PFNGLBINDBUFFERARBPROC GLExtension::glBindBufferARB = NULL;
PFNGLBUFFERDATAARBPROC GLExtension::glBufferDataARB = NULL;
PFNGLDELETEBUFFERSARBPROC GLExtension::glDeleteBuffersARB = NULL;
PFNGLMAPBUFFERARBPROC GLExtension::glMapBufferARB = NULL;
PFNGLUNMAPBUFFERARBPROC GLExtension::glUnmapBufferARB = NULL;

void GLExtension::CheckDeviceAbility()
{
	vboSupported = IsExtensionSupported("GL_ARB_vertex_buffer_object");
	if (vboSupported)
	{
		glGenBuffersARB = (PFNGLGENBUFFERSARBPROC)wglGetProcAddress("glGenBuffersARB");
		glBindBufferARB = (PFNGLBINDBUFFERARBPROC)wglGetProcAddress("glBindBufferARB");
		glBufferDataARB = (PFNGLBUFFERDATAARBPROC)wglGetProcAddress("glBufferDataARB");
		glDeleteBuffersARB = (PFNGLDELETEBUFFERSARBPROC)wglGetProcAddress("glDeleteBuffersARB");
		glMapBufferARB = (PFNGLMAPBUFFERARBPROC)wglGetProcAddress("glMapBufferARB");
		glUnmapBufferARB = (PFNGLUNMAPBUFFERARBPROC)wglGetProcAddress("glUnmapBufferARB");
	}
}
bool GLExtension::GenBuffersARB(GLsizei n, GLuint *buffers)
{
	if (glGenBuffersARB != NULL)
	{
		glGenBuffersARB(n, buffers);
		return true;
	}
	return false;
}
bool GLExtension::BindBufferARB(GLenum target, GLuint buffer)
{
	if (glBindBufferARB != NULL)
	{
		glBindBufferARB(target, buffer);
		return true;
	}
	return false;
}
bool GLExtension::BufferDataARB(GLenum target, int size, const GLvoid *data, GLenum usage)
{
	if (glBufferDataARB != NULL)
	{
		glBufferDataARB(target, size, data, usage);
		return true;
	}
	return false;
}
bool GLExtension::DeleteBuffersARB(GLsizei n, const GLuint *buffers)
{
	if (glDeleteBuffersARB != NULL)
	{
		glDeleteBuffersARB(n, buffers);
		return true;
	}
	return false;
}
GLvoid *GLExtension::MapBufferARB(GLenum target, GLenum access)
{
	if (glMapBufferARB != NULL)
	{
		return glMapBufferARB(target, access);
	}
	return NULL;
}
GLboolean GLExtension::UnmapBufferARB(GLenum target)
{
	if (glUnmapBufferARB != NULL)
	{
		return glUnmapBufferARB(target);
	}
	return GL_FALSE;
}

bool GLExtension::IsExtensionSupported(const char *extension)
{
	const unsigned char *extensions = NULL;
	const unsigned char *start;
	unsigned char *whereStr, *terminator;

	whereStr = (unsigned char*)strchr(extension, ' ');
	if (whereStr != NULL || extension[0] == '\0')
	{
		return false;
	}

	const unsigned char *ver;
	ver = glGetString(GL_VERSION);
	extensions = glGetString(GL_EXTENSIONS);
	if (extensions == NULL)
	{
		return false;
	}
	
	start = extensions;
	while (true)
	{
		whereStr = (unsigned char*)strstr((const char*)start, extension);
		if (whereStr == NULL)
		{
			break;
		}

		terminator = whereStr + strlen(extension);
		if (whereStr == start || *(whereStr - 1) == ' ')
		{
			if (*terminator == ' ' || *terminator == '\0')
			{
				return true;
			}
		}
		start = terminator;
	}

	return false;
}
