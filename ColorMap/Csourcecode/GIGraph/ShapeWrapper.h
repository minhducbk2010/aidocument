/*!	@file
*/
#pragma once

#include "NullableShape.h"
#include "Property.h"

class ShapeWrapper
{
public:
	template<class T>
	explicit ShapeWrapper(T* shape)
		: m_shape(new ShapeProxy<T>(shape))
		, m_impl(shape)
	{
	}

	~ShapeWrapper()
	{
	}

	IShape* GetBody() const { return m_shape.get(); }
	void* GetImpl() const { return m_impl; }

	void Move(double left, double top) { m_shape->Move(left, top); }
	void SetSelected(bool select) { m_shape->SetSelected(select); }

private:
	boost::shared_ptr<IShape> m_shape;
	void* m_impl;

private:
	template<class T>
	class ShapeProxy : public NullableShape
	{
	public:
		explicit ShapeProxy(T* impl)
			: m_impl(impl), m_parentGroup(0)
		{
		}
		virtual ~ShapeProxy()
		{
		}

		// IShape Interface
		virtual void Destroy() { delete this; }

		virtual void SetParent(IGroup* parent) { m_parentGroup = parent; }
		virtual IGroup* GetParent() const { return m_parentGroup; }

		virtual void GetName(wchar_t* name, size_t length) const { m_impl->GetName(name, length); }

		virtual unsigned long GetTypeId() const { return m_impl->GetTypeId(); }
		virtual bool IsDeletable() const { return m_impl->IsDeletable(); }

		virtual void Lock(bool lock) { m_impl->Lock(lock); }
		virtual bool IsLock() const { return m_impl->IsLock(); }
		virtual void PositionLock(bool lock) { m_impl->PositionLock(lock); }
		virtual bool IsPositionLock() const { return m_impl->IsPositionLock(); }

		virtual bool IsVisible() const { return m_impl->IsVisible(); }
		virtual bool IsEffective() const  { return true; }

		virtual void Move(double left, double top) { m_impl->Move(left, top); }

		virtual int HitTest(double x, double y) const { return m_impl->HitTest(x, y); }

		virtual void SetSelected(bool select) { m_impl->Select(select); }
		virtual bool GetSelected() const { return m_impl->IsSelected(); }

		// IProperty
		virtual PropertyNode* CreateProperty() { return m_impl->CreateProperty(); }
		virtual PropertyNode* CreateTabProperty() { return m_impl->CreateTabProperty(); }
		virtual void ReleaseProperty(PropertyNode* prop) { delete prop; }
		virtual bool SetProperty(long id, LPARAM value, LPARAM* source) { return m_impl->SetProperty(id, value, source); }
		virtual bool GetProperty(long id, LPARAM* ret) const { return m_impl->GetProperty(id, ret); }

	private:
		T* m_impl;
		IGroup* m_parentGroup;
	};
};