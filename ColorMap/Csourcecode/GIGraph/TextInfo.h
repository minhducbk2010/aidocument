#pragma once

#include "TexVertex.h"
#include "TextRenderAlign.h"

class TextInfo
{
public:
	TextInfo(GLuint base, GLsizei count, SIZE size)
	{
		listBase = base;
		listCount = count;
		this->size = size;
		yOffset = 0.0F;
	}
	~TextInfo()
	{
		//glDeleteLists(listBase, listCount);
	}

	void Render(Vertex /*pos*/, TextRenderAlign /*align*/)
	{
		//glRasterPos3f(pos.X, pos.Y, pos.Z);

		//switch (align)
		//{
		//case TraLeft:
		//	glBitmap(0, 0, 0.0F, 0.0F, 0.0F, yOffset, NULL);
		//	break;
		//case TraRight:
		//	glBitmap(0, 0, 0.0F, 0.0F, (GLdouble)-size.cx, yOffset, NULL);
		//	break;
		//case TraCenter:
		//	glBitmap(0, 0, 0.0F, 0.0F, (GLdouble)(-size.cx / 2), yOffset, NULL);
		//	break;
		//}

		//for (int i = 0; i < listCount; ++i)
		//{
		//	glCallList(listBase + i);
		//}
	}

	SIZE size;
	GLdouble yOffset;

private:
	GLuint listBase;
	GLsizei listCount;
};
