#pragma once

struct TexVertex
{
	TexVertex()
		: S(), T(), X(), Y(), Z() {}
	TexVertex(GLdouble s, GLdouble t, GLdouble x, GLdouble y, GLdouble z)
		: S(s), T(t), X(x), Y(y), Z(z) {}
	GLdouble S, T, X, Y, Z;
};

struct TexVertexF
{
	TexVertexF()
		: S(), T(), X(), Y(), Z() {}
	TexVertexF(GLfloat s, GLfloat t, GLfloat x, GLfloat y, GLfloat z)
		: S(s), T(t), X(x), Y(y), Z(z) {}
	GLfloat S, T, X, Y, Z;
};

struct Vertex
{
	Vertex() : X(), Y(), Z() {}
	Vertex(GLdouble x, GLdouble y, GLdouble z)
		: X(x), Y(y), Z(z) {}
	GLdouble X, Y, Z;
};

struct Texture
{
	Texture(GLdouble u, GLdouble v) : u(u), v(v) {}
	GLdouble u, v;
};
