/*!	@file
	@date	2008-01-16
	@brief	DLLに直接渡されたデータを管理するクラス
*/
#pragma once

#include "IPlotData.h"
#include "AxisEnum.h"

class DllData : public IPlotData
{
public:
	DllData();
	virtual ~DllData();

	void SetData(long dataCount, real_t const* xData, real_t const* yData, real_t const* zData);
	void SetData(long dataCount, real_t const* xData, real_t const* yData, real_t const* zData, real_t const* tData);
	void SetDataEffective(long count, BOOL* effective);
	void SetNames(TCHAR const* xName, TCHAR const* yName, TCHAR const* zName);
	void SetUnits(TCHAR const* xName, TCHAR const* yName, TCHAR const* zName);
	void Clear();

// override
	virtual void Destroy();
	virtual long GetFileId(long /*dimension*/) {return m_fileId;}
	virtual long GetItemId(long /*dimension*/) {return 0;}
	virtual void ChangeFile(long /*fileId*/) {}
	virtual void SetLink(long /*dimension*/, long /*fileId*/) {}
	virtual void SetLink(long /*dimension*/, long /*fileId*/, long /*itemId*/) {}
	virtual void CopyFrom(size_t /*fileId*/, IPlotData* /*src*/) {}
	virtual void UpdateItemIndex() {}
	virtual bool LinkDataSet() {return true;}
	virtual void ClearItems() {}

	virtual void SetFilter(boost::shared_ptr<IFilter> const& /*filter*/) {}
	virtual IFilter* GetFilter() const {return 0;}

	virtual void SetDataIndex(size_t const* /*index*/, size_t /*count*/) {}
	virtual size_t GetAbsoluteIndex(size_t /*relativeIndex*/) const {return 0;}
	virtual size_t GetRelativeIndex(size_t /*absoluteIndex*/) const {return 0;}

	virtual void SortForIndex() {}
	virtual void SortForX() {}

	virtual wchar_t const* GetItemName(long dimension) const;
	virtual wchar_t const* GetUnitName(long dimension) const;

	// 未使用
	virtual void AddReferenceData(IPlotData* /*data*/) {}
	virtual void ClearReferenceData() {}
	virtual bool UseReferenceData() const {return false;}
	virtual size_t GetReferenceDataCount() const {return 0;}
	virtual IPlotData* GetReferenceData(size_t /*index*/) const {return 0;}

	virtual int GetScaleType(long /*dimension*/) const {return ST_LINEAR;}
	virtual void SetScaleType(long /*dimension*/, long /*log*/) {}
	virtual bool UseString(long /*dimension*/) const {return false;}

	virtual void SetData(long /*dimension*/, float const* /*data*/, size_t /*dataCount*/, bool /*copy*/) {}
	virtual size_t GetRealDataCount() const {return GetDataCount();}
	real_t const* GetXData() const {return m_xData.empty() ? 0 : &m_xData[0];}
	real_t const* GetYData() const {return m_yData.empty() ? 0 : &m_yData[0];}
	real_t const* GetZData() const {return m_zData.empty() ? 0 : &m_zData[0];}
	virtual real_t GetMinimum(long dimension) const;
	virtual real_t GetMaximum(long dimension) const;
	virtual real_t GetRealMinimum(long dimension) const;
	virtual real_t GetRealMaximum(long dimension) const;

	virtual real_t const* GetDataByDimension(int dimension) const;

	virtual real_t GetRawData(int dimension, size_t index) const {return GetData(dimension, index);}
	virtual real_t GetRealData(int dimension, size_t index) const {return GetData(dimension, index);}
	virtual real_t GetData(int dimension, size_t index) const;
	virtual TCHAR const* GetStrData(int /*dimension*/, size_t /*index*/) const {return _T("");}
	virtual TCHAR const* GetStrDataFromAllData(int dimension, size_t index) const { return _T("");}
	virtual void SetRawData(long dimension, size_t index, real_t const value);
	virtual real_t ConvertValue(long, real_t) const { return 0; }

	virtual size_t GetDataCount() const { return m_indexes.size(); }
	virtual size_t FindMaximumIndex(long dimension, long exclusionScale = 0, bool accumulative = false) const;
	virtual size_t FindMinimumIndex(long dimension, long exclusionScale = 0, bool accumulative = false) const;
	virtual void FindMinMaxIndex(size_t dimension, size_t* min, size_t* max, long exclusionScale = 0, bool accumulative = false) const;

	virtual void Serialize(ISerializer* /*ar*/) {}

	virtual IPlotData* Clone() /*const*/ {return 0;}

	size_t GetRealIndex(size_t base) const { return m_indexes.size() <= base ? base : m_indexes[base]; }

private:
	int m_fileId;
	std::vector<real_t> m_xData, m_yData, m_zData, m_tData;
	std::vector<size_t> m_indexes;

	std::wstring m_names[6];
	std::wstring m_units[6];

	static int s_fileId;

	bool m_heap;
};