#include "stdafx.h"
#include "ColorBar.h"
#include "GLExtension.h"
#include "GLBuffer.h"
#include "TexVertex.h"
#include "TextRenderer.h"
#include "ColorTexture.h"
#include "Axis.h"
#include "Graph3DBase.h"

ColorBar::ColorBar(Axis* axis)
	: m_axis(axis), m_labelVisible(false), m_dimension(3)
{
}
ColorBar::~ColorBar()
{
}

void ColorBar::AttachAxis(Axis* axis)
{
	m_axis = axis;
}

void ColorBar::Render(ColorTexture* texture, std::wstring const& name, bool sub/* = false*/)
{
	::glPushMatrix();
	::glPushClientAttrib(GL_CLIENT_VERTEX_ARRAY_BIT);

	double const x = sub ? 20 : 17;		// カラーバーを置く位置(左端)
	double const len = 1;				// カラーバーの幅

	// カラーバーを描画
	{
		double const top = m_axis->GetSize(), bottom = -m_axis->GetSize();
		texture->Bind();
		::glBegin(GL_QUAD_STRIP);
			::glTexCoord1i(0); glVertex3d(x, bottom, 10);
			::glTexCoord1i(1); glVertex3d(x, top, 10);
			::glTexCoord1i(0); glVertex3d(x + len, bottom, 10);
			::glTexCoord1i(1); glVertex3d(x + len, top, 10);
		::glEnd();
		texture->Unbind();

		// バー周囲の枠を描く
		::glColor3f(0, 0, 0);
		::glBegin(GL_LINE_LOOP);
			::glVertex3d(x, bottom, 10.1);
			::glVertex3d(x, top, 10.1);
			::glVertex3d(x + len, top, 10.1);
			::glVertex3d(x + len, bottom, 10.1);
		::glEnd();
	}

	::glTranslated(x + len, 0.0, 0.0);

	if (!sub) {
		// 目盛りラベル
		if (m_axis->IsVisibleTickLabel()) {
			COLORREF const color = m_axis->GetTickLabelColor();
			for (UINT i = 0U; i < m_axis->GetGridCount(); ++i) {
				double const val = m_axis->GetGridInfo(i).Value;
				// 座標はリニア値で求める必要がある
				double const lin = m_axis->ToLinear(val);
				GLdouble const y = m_axis->GetNormalize(lin) * 20 - 10;

				textRenderer->AddTextFormat(val, 0.5, y, 0, color, TextAlign::Left,
					m_axis->GetTickLabelFont(), m_axis->GetScaleFigure(), m_axis->GetTickFormat(), m_axis->UseComma(),
					m_axis->GetSIUnit(), m_axis->GetScaleType(), true);
			}
		}
	}

	// 軸ラベル
	if (m_labelVisible && m_axis->IsVisibleLabel()) {
		HWND wnd = m_axis->GetOwner()->GetWnd();
		HDC dc = ::GetDC(wnd);
		SIZE size = textRenderer->GetTextSize(dc, name.c_str(), m_axis->GetLabelFont());
		::ReleaseDC(wnd, dc);

		if (m_dimension == 4 || m_dimension == 6) {
			TextAlign::type align = TextAlign::Center;

			double nx, ny, right;
			::WorldToWindow(0, 12, 0, &nx, &ny);
			// ラベルの右端がはみ出るか確認し、はみ出るようなら左に寄せる
			::WorldToWindow(999, 12, 0, &right, &ny);
			if ((nx + size.cx) > right + 5) {
				nx = right - size.cx - 5;
				align = TextAlign::Left;
			}

			textRenderer->AddText(name.c_str(), (int)nx, (int)ny, 0, 0, align, m_axis->GetLabelFont());
		}
		else {
			TextAlign::type const align = TextAlign::Left;

			double nx, ny;
			double const x = sub ? 1 : -1.7;
			::WorldToWindow(x, 0, 0, &nx, &ny);
			textRenderer->AddText(name.c_str(), (int)nx, (int)ny + size.cx / 2, -90, 0, align, m_axis->GetLabelFont());
		}
	}

	::glPopClientAttrib();
	::glPopMatrix();
}
