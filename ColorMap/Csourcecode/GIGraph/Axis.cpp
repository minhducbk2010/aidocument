#include "stdafx.h"
#include <complex>
#include <boost/tuple/tuple.hpp>
#include <boost/foreach.hpp>
#include "Axis.h"
#include "TextRenderer.h"
#include "TextInfo.h"
#include "Graph3DBase.h"
#include "DeviceContext.h"
#include "IPlotData.h"
#include "Message.h"

template<class T>
std::vector<T>& operator<<(std::vector<T>& lhs, std::vector<T> const& rhs)
{
	lhs.insert(lhs.end(), rhs.begin(), rhs.end());
	return lhs;
}

double ToLog(double linear)
{
	if (IsZero(linear) || linear <= 0) {
		return 0;
	}
	return std::log10(linear);
}

int GetDigits(double value)
{
	TCHAR buf[36] = {};
	::_sntprintf_s(buf, _TRUNCATE, _T("%g"), value);
	return static_cast<int>(::_tcslen(buf));
}

typedef boost::tuple<double, double, double> edge_t;

edge_t GetEdgePos(double const angle, double const zAngle)
{
	double const sign = 1.0;

	if (!IsZero(zAngle)) {
		if (angle <= -90) {
			return boost::make_tuple(sign, -sign, -sign);
		}
		else if (angle < 0) {
			return boost::make_tuple(sign, sign, -sign);
		}
		else if (angle < 90) {
			return boost::make_tuple(sign, sign, sign);
		}
		else if (angle <= 180) {
			return boost::make_tuple(sign, -sign, sign);
		}
		return boost::make_tuple(-sign, sign, sign);
	}
	else {
		if (angle < -90) {
			return boost::make_tuple(sign, -sign, sign);
		}
		else if (angle < 0) {
			return boost::make_tuple(sign, sign, sign);
		}
		else if (angle < 90) {
			return boost::make_tuple(-sign, sign, sign);
		}
		else if (angle < 180) {
			return boost::make_tuple(-sign, -sign, sign);
		}
	}
	return boost::make_tuple(sign, -sign, sign);
}

edge_t GetEdgePos(double const xAngle, double const yAngle, double const zAngle)
{
	edge_t edge = GetEdgePos(yAngle, zAngle);
	if (!IsZero(zAngle)) {
		if (xAngle < 0) {
			edge.get<0>() = -edge.get<0>();
		}
		if (xAngle > 90 || xAngle < -90) {
			edge.get<1>() = -edge.get<1>();
			edge.get<2>() = -edge.get<2>();
		}
	}
	else {
		if (xAngle < 0) {
			edge.get<2>() = -edge.get<2>();
		}
		if (xAngle > 90 || xAngle < -90) {
			edge.get<0>() = -edge.get<0>();
			edge.get<1>() = -edge.get<1>();
		}
	}
	return edge;
}

int GetAngleNumber(double const angle)
{
	double const a = angle + 180;
	if (a >= 360) { return 0; }
	return static_cast<int>(a / 90);
}

double GetZSign(Graph3DBase* owner)
{
	double const sign = 1;
	double const x = owner->GetPitch() + 180;
	int const angle = GetAngleNumber(owner->GetYaw());
	if (90 <= x && x < 270) {
		if (angle == 1 || angle == 3) { return -sign; }
	}
	else {
		if (angle == 0 || angle == 2) { return -sign; }
	}
	return sign;
}

double const Axis::MarginRate = 1.0F;

// begin-end間の角度を求める
double GetAngle(OCPoint begin, OCPoint end)
{
	// OpenGL内の座標をウィンドウ座標に変換する
	GLint viewport[4] = {};
	GLdouble mvMatrix[16] = {}, pMatrix[16] = {};
	double ax, ay, az;
	double bx, by, bz;

	::glGetIntegerv(GL_VIEWPORT, viewport);
	::glGetDoublev(GL_MODELVIEW_MATRIX, mvMatrix);
	::glGetDoublev(GL_PROJECTION_MATRIX, pMatrix);

	::gluProject(begin.x, -1, begin.y, mvMatrix, pMatrix, viewport, &ax, &ay, &az);
	::gluProject(end.x, -1, end.y, mvMatrix, pMatrix, viewport, &bx, &by, &bz);

	// 角度を求める
	std::complex<double> a(ax, ay), b(bx, by);
	double angle = std::arg(b - a);

	angle *= 180 / M_PI;
	angle = -angle;
	return static_cast<double>(angle);
}

Axis::Axis(Graph3DBase *owner)
	: m_visible(true), m_scaleType(ST_LINEAR), m_autoScale(true), m_scaleFitting(false)
	, m_maximum(1), m_minimum(-1), m_interval(1), m_subInterval(0.5)
	, m_useSecond(false), m_second(0)
	, m_dbExp(20), m_dbStd(1)
	, m_drawTickLabel(true), m_tickType(TT_OUTSIDE), m_tickSize(0.8), m_tickLabelFontColor(0)
	, m_scaleDispType(StringFormat::Digit)
	, m_scaleFigure(-1)
	, m_useComma(false)
	, m_drawSubTick(false)
	, m_drawBorder(true)
	, m_drawGrid(true), m_gridStyle(0), m_gridColor(RGB(192, 192, 192)), m_gridWidth(1)
	, m_labelVisible(true), m_autoLabel(true)
	, m_labelFontColor(0)
	, m_length(10)
	, m_SIUnit(SIUnitLabel::OFF)
{
	this->owner = owner;

	// フォント設定を環境設定から取得する
	TCHAR const* labelFontStr = reinterpret_cast<TCHAR const*>(owner->GetOwner()->SendMessage(PI_MSG_GET_CONF_AXISFONT, 0, 0));
	std::wstring labelFont = labelFontStr ? labelFontStr : _T("");
	long labelFontSize = static_cast<long>(owner->GetOwner()->SendMessage(PI_MSG_GET_CONF_AXISFONTSIZE, 0, 0));

	TCHAR const* tickFontStr = reinterpret_cast<TCHAR const*>(owner->GetOwner()->SendMessage(PI_MSG_GET_CONF_AXIS_TICKFONT, 0, 0));
	std::wstring tickFont = tickFontStr ? tickFontStr : _T("");
	long tickFontSize = static_cast<long>(owner->GetOwner()->SendMessage(PI_MSG_GET_CONF_AXIS_TICKFONTSIZE, 0, 0));

	// 設定が取得できなければデフォルト値を使用する
	if (labelFont.empty())	{ labelFont = GetResourceString(owner->GetOwner(), "ID_SOURCE_TEXT_MS_P_GOTHIC"); }
	if (labelFontSize == 0) { labelFontSize = ::PointToDeviceUnit(9); }
	if (tickFont.empty())	{ tickFont =  GetResourceString(owner->GetOwner(), "ID_SOURCE_TEXT_MS_P_GOTHIC"); }
	if (tickFontSize == 0)	{ tickFontSize = ::PointToDeviceUnit(8); }

	::memset(&m_labelFont, 0, sizeof(m_labelFont));
	::wcscpy_s(m_labelFont.lfFaceName, _countof(m_labelFont.lfFaceName), labelFont.c_str());
	m_labelFont.lfHeight = labelFontSize;

	::memset(&m_tickLabelFont, 0, sizeof(m_tickLabelFont));
	::wcscpy_s(m_tickLabelFont.lfFaceName, _countof(m_tickLabelFont.lfFaceName), tickFont.c_str());
	m_tickLabelFont.lfHeight = tickFontSize;
}

Axis::~Axis()
{
}

TCHAR const* Axis::GetName() const
{
	if (owner->GetDataCount()) {
		return owner->GetData(0)->GetItemName(GetDimension());
	}
	else {
		return ::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_HYPHEN_3");
	}
}

double Axis::GetMaximum() const
{
	return ConvertValue(m_scaleType, m_maximum);
}
double Axis::GetMinimum() const
{
	return ConvertValue(m_scaleType, m_minimum);
}
double Axis::GetInterval() const
{
	switch (m_scaleType)
	{
	case ST_LOG:
		return 1;
	case ST_DECIBEL:
	default:
		return m_interval;
	}
}

double Axis::GetSubInterval() const
{
	return m_scaleType == ST_LOG ? 1 : m_subInterval;
}

double Axis::GetSecondValue() const
{
	switch (m_scaleType)
	{
	case ST_LOG:
		return 1; //??? ログスケールの時はセカンド値は使用しない
	case ST_DECIBEL:
		{
			////TODO: セカンド値は可変にしたい(平面も固定)
			return GetMinimum() + m_interval;
		}
	case ST_LINEAR:
	default:
		return m_second;
	}
}

double Axis::GetSecondValueForUse() const
{
	if (GetSecondUse()) {
		return GetSecondValue();
	}

	if (m_scaleType == ST_LINEAR) {
		return m_minimum + m_interval;
	}
	// リニア以外のセカンド値は計算して求める(固定値なのでフラグは見ても見なくても変わらない)
	return GetSecondValue();
}

void Axis::SetMinMax(double min, double max)
{
	m_maximum = ToLinear(max);
	m_minimum = ToLinear(min);
	m_interval = GetScaleTick(max - min);
	m_subInterval = m_interval / 5;
	m_second = m_minimum + m_interval;
	OnScaleChanged();
}

//! セカンド値の使用を設定します
void Axis::SetSecondUse(bool use) { 
	m_useSecond = use; 
	OnScaleChanged();
}

//! セカンド値を設定します
int Axis::SetSecondValue(double second)
{
	if (second < m_minimum || second > m_maximum) { // 同じなのはOK
		// ID_SOURCE_TEXT_INPUT_BETWEEN_MIN_MAX_SECOND_VALUE
		return -1;
	}
	m_second = second;
	m_autoScale = false;
	OnScaleChanged();
	return 0;
}

//! 目盛間隔を設定します
int Axis::SetInterval(double interval)
{
	if (interval <= 0 || IsZero(interval)) {
		// 目盛間隔には0より大きい値を入力してください。
		return -1;
	}
	else if (interval >= GetMaximum() - GetMinimum()) {
		// 目盛間隔が大きすぎます。\n最大値-最小値より小さい値を入力してください。
		return -2;
	}
	m_interval = interval;
	m_autoScale = false;

	OnScaleChanged();
	return 0;
}

//! 補助目盛間隔を設定します
int Axis::SetSubInterval(double subInterval)
{
	if ((subInterval <= 0 || IsZero(subInterval)))
	{ 
		// 補助目盛間隔には0より大きい値を入力してください。
		return -1; 
	}
	else if (subInterval >= m_interval) {
		// 補助目盛間隔には目盛間隔より小さな値を入力してください。
		return -2;
	}
	m_subInterval = subInterval;
	m_autoScale = false;

	OnScaleChanged();
	return 0;
}

void Axis::SetLabel(std::wstring const& value)
{
	m_label = value;
}
void Axis::ResetLabel()
{
}
void Axis::SetScaleParam(double maximum, double minimum, ScaleType scale, const double *second)
{
	if (maximum <= minimum ||
		(scale == ST_LOG && (maximum <= 0.0 || minimum <= 0.0)))
	{
		return;
	}

	m_maximum = maximum;
	m_minimum = minimum;
	m_scaleType = scale;

	bool autoSecond = true;

	if (second != NULL)
	{
		if (*second > minimum && *second < maximum)
		{
			m_second = *second;
			autoSecond = false;
		}
	}

	if (autoSecond)
	{
		if (m_scaleType == ST_LOG)
		{
			double maxLog = log10(m_maximum);
			double minLog = log10(m_minimum);
			double div = this->GetScaleTick(maxLog - minLog);
			m_second = m_minimum + pow(10.0, div);
		}
		else
		{
			double div = this->GetScaleTick(m_maximum - m_minimum);
			m_second = ceil(m_minimum / div) * div;
		}
		CreateGrids();
	}
}

void Axis::CalcAutoScale(double minimum, double maximum)
{
	if (!m_autoScale) { return; }
	// プロットがなくて元になる最大最小値がない場合はとりあえずそのままにしておく
	if (minimum == FLT_MAX && maximum == -FLT_MAX) { return; }

	if (minimum >= maximum) {
		minimum *= 0.8;
		maximum *= 1.2;
		if (minimum >= maximum) {
			minimum -= 10;
			maximum += 10;
		}
	}

	if (m_scaleType == ST_LINEAR) {
		m_minimum = minimum;
		m_maximum = maximum;
		m_interval = GetScaleTick(m_maximum - m_minimum);
		if (!m_scaleFitting) {
			m_maximum = std::ceil(m_maximum / m_interval) * m_interval;
			m_minimum = std::floor(m_minimum / m_interval) * m_interval;
		}

		m_subInterval = m_interval / 5.0;
		m_second = m_minimum + m_interval;
	}
	else {
		double logMax, logMin;
		if (maximum <= 0) {
			logMax = 1;
			logMin = 0;
		}
		else if (minimum <= 0) {
			logMax = std::log10(maximum);
			logMin = logMax - 3.0;
		}
		else {
			logMax = std::log10(maximum);
			logMin = std::log10(minimum);
		}
		m_maximum = std::pow(10, std::ceil(logMax));
		m_minimum = std::pow(10, std::floor(logMin));
		m_interval = 1;
		if (m_scaleType == ST_DECIBEL) {
			double const decibelMax = LinearToDecibel(m_maximum);
			double const decibelMin = LinearToDecibel(m_minimum);
			double const decibelInterval = GetScaleTick(decibelMax - decibelMin);
			m_interval = decibelInterval;
			m_subInterval = m_interval / 5.0;
			m_second = decibelMin + decibelInterval;
		}
	}
}

COLORREF Axis::GetSymbolColor() const
{
	return m_symbolColor;
}

/*!	元データを頂点座標に変換します。
	@param[in]	value	元データ(リニア)
*/
double Axis::ValueToCoordinate(double value) const
{
	return GetNormalize(value) * (m_length * 2);
}
double Axis::CoordinateToValue(double coordinate)
{
	if (coordinate == FLT_MAX) { return FLT_MAX; }
	double const value = (GetMaximum() - GetMinimum()) * ((coordinate + m_length) / (m_length * 2)) + GetMinimum();
	if (m_scaleType == ST_LOG)
	{
		return std::pow(10, value);
	}
	return value;
}
double Axis::GetNormalize(double value) const
{
	double const min = GetMinimum(), max = GetMaximum();
	value = ConvertValue(m_scaleType, value);

	return (value - min) / (max - min);
}

/*!	元データを頂点座標に変換します。
	@param[in]	value	元データ(軸のスケールタイプと同じ型の値)
	@note		スケールタイプがログであればvalueもログ
*/
double Axis::ValueToCoordinateRaw(double value) const
{
	double const min = GetMinimum(), max = GetMaximum();
	double const norm = (value - min) / (max - min);
	return norm * (m_length * 2);
}
/*!	頂点座標を実数に変換します。
	@param[in]	coordinate	頂点座標
	@return		軸のスケールタイプに沿った実数
*/
double Axis::CoordinateToValueRaw(double coordinate) const
{
	if (coordinate == FLT_MAX) { return FLT_MAX; }
	double const value = (GetMaximum() - GetMinimum()) * ((coordinate + m_length) / (m_length * 2)) + GetMinimum();
	return value;
}

double Axis::Expand(double norm) const
{
	double const min = GetMinimum(), max = GetMaximum();
	return (max - min) * norm + min;
}

double Axis::GetTextureValue(double value) const
{
	return GetNormalize(value) * (GetSize() * 2) - GetSize();
}

double Axis::GetScaleTick(double span) const
{
    double dVal, dScaleMax, dTick;

    char s[80];
	::sprintf_s(s, sizeof(s), "%1.0le", span);   // s[]: "Xe+XX"
    ::sscanf_s(s, "%lf", &dScaleMax);
    switch (s[0])
    {
        case '0':
            dTick = 0.;
            break; // この条件が成立することはない
        case '1':
            dTick = 0.25;
            break;
        case '2':
            dTick = 0.5;
            break;
        case '3':
        case '4':
        case '5':
            dTick = 1.;
            break;
        default:
            dTick = 2.;
            break;
    }
    s[0] = '1';                         // s[]: "1e+XX"
    ::sscanf_s(s, "%lf", &dVal);
    dTick *= dVal;
    
    return dTick;
}
void Axis::CreateGrids()
{
	m_gridInfo.clear();
	m_subTicks.clear();

	if (m_scaleType == ST_LOG) {
		CreateLogGrids();
	}
	else {
		CreateLinearGrids();
	}
}

void Axis::CreateLinearGrids()
{
	double const min = GetMinimum(), max = GetMaximum();
	double const interval = GetInterval();
	double const subInterval = GetSubInterval();
	double const second = GetSecondValueForUse();

	AddGrid(ValueToCoordinate(ToLinear(min)), min, true);
	InsertSubTick(min, second, subInterval);

	for (int i = IsEqual(min, second) ? 1 : 0; ; ++i) {
		double const value = IsZero(second + i * interval) ? 0.0 : second + i * interval;
		if (value < max) {
			double const linear = ToLinear(value);
			AddGrid(ValueToCoordinate(linear), value, true/*showValue*/);
		}
		double const next = std::min<double>(max, value + interval);
		InsertSubTick(value, next, subInterval);

		if (value >= max) { break; }
	}
	AddGrid(ValueToCoordinate(ToLinear(max)), max, true);
}

void Axis::CreateLogGrids()
{
	double const maxLog = GetMaximum();
	double const minLog = GetMinimum();
	for (double i = minLog; i < maxLog; i += 1) {
		double const val = std::pow(10.0, i);
		AddGrid(ValueToCoordinate(val), val, true);

		double const next = std::min<double>(std::pow(10, GetMaximum()), std::pow(10.0, i + 1));
		InsertSubTick(val, next, val);
	}

	{
		double const val = std::pow(10, GetMaximum());
		AddGrid(ValueToCoordinate(val), val, true);
	}
}

//! 補助目盛りを作成、追加します
void Axis::InsertSubTick(double current, double next, double subInterval)
{
	if (subInterval <= 0) { return; }
	if (!m_drawSubTick) { return; }

	for (int j = 1; ; ++j) {
		double const sv = IsZero(current + subInterval * j) ? 0.0 : current + subInterval * j;
		if (sv >= next) { break; }

		double const pos = ValueToCoordinate(ToLinear(sv));
		m_subTicks << GetTickPos(pos, m_tickSize / 2);
		m_grids << GetGridPos(pos, j % 2 == 0);
	}
}

//-----------------------------------------------------
// オクターブトラッキング時のために追加 080728nakano
// 軸に配列を直で設定
//------------------------------------------------------
void Axis::SetScaleVector(double* scale, int scaleLen)
{
	for (int i = 0; i < scaleLen; ++i) {
		AddGrid(ValueToCoordinate(scale[i]), scale[i], true);
	}
}

GridInfo Axis::AddGrid(double coordinate, double value, bool showValue)
{
	GridInfo gridInfo;

	gridInfo.Coordinate = coordinate;
	gridInfo.Value = value;
	gridInfo.ShowValue = showValue;
	m_gridInfo.push_back(gridInfo);

	return gridInfo;
}

void Axis::Modeling()
{
	m_grids.clear();
	m_ticks.clear();

	CreateGrids();
	size_t const count = GetGridCount();

	for (size_t i = 0; i < count; ++i) {
		if (i > 0 && i < count - 1) {
			m_grids << GetGridPos(m_gridInfo[i].Coordinate, i % 2 == 0);
		}
		m_ticks << GetTickPos(m_gridInfo[i].Coordinate, m_tickSize);
	}
}

void Axis::Render()
{
	if (!m_visible) { return; }

	if (m_labelVisible && IsVisibleTicks()) {
		// 軸ラベル
		RenderLabel();
	}

	glEnableClientState(GL_VERTEX_ARRAY);

	// 目盛りと目盛ラベル
	if (!m_ticks.empty() && m_drawTickLabel && IsVisibleTicks()) {
		::glColor3ub(0, 0, 0);
		::glVertexPointer(3, GL_DOUBLE, 0, &m_ticks[0]);
		::glDrawArrays(GL_LINES, 0, static_cast<GLsizei>(m_ticks.size()));

		RenderTickLabels();
	}

	if (!m_subTicks.empty() && m_drawSubTick && IsVisibleTicks()) {
		::glColor3ub(0, 0, 0);
		::glVertexPointer(3, GL_DOUBLE, 0, &m_subTicks[0]);
		::glDrawArrays(GL_LINES, 0, static_cast<GLsizei>(m_subTicks.size()));
	}

	// グリッド
	if (!m_grids.empty() && m_drawGrid) {
		::glColor3ub(GetRValue(m_gridColor), GetGValue(m_gridColor), GetBValue(m_gridColor));
		::glVertexPointer(3, GL_DOUBLE, 0, &m_grids[0]);
		::glDrawArrays(GL_LINES, 0, static_cast<GLsizei>(m_grids.size()));
	}

	// 対称ライン
	if (m_drawBorder) {
		RenderBorderLine();
	}

	glDisableClientState(GL_VERTEX_ARRAY);
}

OCRect const& Axis::GetRect() const
{
	static OCRect r;
	return r;
}

void Axis::RenderBorderLine() const
{
	Axis* axis = GetBorderAxis();
	std::vector<Vertex> v;
	v	<< axis->GetGridPos(axis->m_gridInfo.front().Coordinate, false)
		<< axis->GetGridPos(axis->m_gridInfo.back().Coordinate, false);

	::glColor3ub(0, 0, 0);
	::glVertexPointer(3, GL_DOUBLE, 0, &v[0]);
	::glDrawArrays(GL_LINES, 0, static_cast<GLsizei>(v.size()));
}

void Axis::RenderTickLabels()
{
	if (!IsVisibleTickLabel()) { return; }

	bool bDrawXScaleEdge = true;

	TextAlign::type align = GetTickLabelAlign();

	if(bDrawXScaleEdge) {
//		TextRenderer::GetInstance()->AddText(GetMinimum(), -x, yPos, zPos, 0, GetSymbolColor(), align, GetTickLabelFont());
	}

	UINT iStart = bDrawXScaleEdge ? 0U : 1U;
	size_t iStop = bDrawXScaleEdge ? GetGridCount() : GetGridCount() - 1;
//	int const labelCount = static_cast<int>(iStop - iStart);
//	int const intervalIndex = std::max<int>(1, labelCount / 6);
	for (UINT i = iStart; i < iStop; ++i) {
		GridInfo const& info = GetGridInfo(i);
		if (info.ShowValue) {  // WFCheck
			/*if(i == iStart || i == iStop - 1 || (i - iStart) % (intervalIndex) == 0)*/ {
				Vertex const v = GetTickPosition(i);
				TextRenderer::GetInstance()->AddTextFormat(info.Value, v.X, v.Y, v.Z,
					m_tickLabelFontColor, align, GetTickLabelFont(), m_scaleFigure, m_scaleDispType, m_useComma, m_SIUnit, m_scaleType);

			}
		}
	}

	if(bDrawXScaleEdge) {
//		TextRenderer::GetInstance()->AddText(GetMaximum(), x, yPos, zPos, 0, GetSymbolColor(), align, GetTickLabelFont());
	}
}

double Axis::GetPropertyMax() const
{
	switch (m_scaleType)
	{
		// ログの時はすでにCalcAutoScaleで計算されている
	case ST_LOG:
		return m_maximum;
	case ST_DECIBEL:
		{
			return ConvertValue(m_scaleType, m_maximum);
		}
	default:
		return ConvertValue(m_scaleType, m_maximum);
	}
}

double Axis::GetPropertyMin() const
{
	switch (m_scaleType)
	{
		// ログの時はすでにCalcAutoScaleで計算されている
	case ST_LOG:
		return m_minimum;
	case ST_DECIBEL:
		{
			return ConvertValue(m_scaleType, m_minimum);
		}
	default:
		return ConvertValue(m_scaleType, m_minimum);
	}
}

double Axis::ToLinear(double value) const
{
	switch (m_scaleType)
	{
	case ST_DECIBEL:
		return DecibelToLinear(value);
	case ST_CONSTANT:
		return value + 1; //1 origin
	}
	return value;
}

double Axis::ConvertValue(ScaleType const type, double const value) const
{
	return type == ST_LINEAR
		? value
		: type == ST_LOG
		? ToLog(value)
		: LinearToDecibel(value);
}

//! 実数をデシベルに変換します
double Axis::LinearToDecibel(double linear) const
{
	return std::log10(linear / m_dbStd) * m_dbExp;
}

//! デシベルを実数に変換します
double Axis::DecibelToLinear(double decibel) const
{
	return std::pow(10, decibel / m_dbExp) * m_dbStd;
}

#if 1

#include "property.h"
#include "PropertyHelper.hpp"
#include "ISerializer.h"

void Axis::AddProperty(IPropertyGroup* parent, int mask, bool locked) const
{
	{
		IPropertyGroup* group = parent->AddGroup(::GetResourceString(owner->GetOwner(), "ID_SOURCE_TEXT_LABEL"), PI_AXIS_LABEL_VISIBLE + mask, m_labelVisible, locked);
		/*if (m_labelVisible)*/ {
			group->AddProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_NAME"), PI_AXIS_LABEL + mask, m_label.c_str(), locked || !m_labelVisible);
			group->AddBoolProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_AUTO_NAMING"), PI_AXIS_AUTOLABEL + mask, m_autoLabel, locked || !m_labelVisible);

			m_labelFontProp = m_labelFont;
			m_labelFontProp.lfWidth = m_labelFontColor;
			group->AddFontProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_FONT"), PI_AXIS_LABEL_FONT + mask, m_labelFontProp, locked || !m_labelVisible);
		}
	}
	{
		IPropertyGroup* group = parent->AddGroup(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_SCALE"));

		std::wstring typeProp1 = GetResourceString(owner->GetOwner(), "ID_SOURCE_TEXT_LINEAR");
		std::wstring typeProp2 = GetResourceString(owner->GetOwner(), "ID_SOURCE_TEXT_LOG");
		std::wstring typeProp3 = GetResourceString(owner->GetOwner(), "ID_SOURCE_TEXT_DECIBEL");
		std::wstring typeProp4 = GetResourceString(owner->GetOwner(), "ID_SOURCE_TEXT_CONSTANT");
		std::wstring typeProp5 = GetResourceString(owner->GetOwner(), "ID_SOURCE_TEXT_ANGLE_DO");
		bool isVectorPlot = (!!owner && owner->GetDimension() == 6); 
		group->AddArrayProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_TYPE"), PI_SCALE_TYPE + mask, m_scaleType, locked || isVectorPlot, isVectorPlot ? 1 : 3, // ベクトル図の時はリニアのみ、その他の時でもコンスタントと角度は外す
				typeProp1.c_str(), typeProp2.c_str(), typeProp3.c_str(), typeProp4.c_str(), typeProp5.c_str());

		group->AddBoolProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_AUTO_SCALE"), PI_SCALE_AUTO + mask, m_autoScale, locked);
		/*if (m_scaleType == ST_LINEAR)*/ {
			group->AddBoolProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_SCALE_FIT"), PI_SCALE_FIT + mask, m_scaleFitting, locked || m_scaleType != ST_LINEAR || !m_autoScale);
		}
		group->AddProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_MIN"), PI_SCALE_MIN + mask, GetPropertyMin(), locked);
		/*if (m_scaleType == ST_LINEAR)*/ {
			group->AddBoolProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_USE_SECOND_VALUE"), PI_SCALE_SECOND_USE + mask, m_useSecond, locked || m_scaleType != ST_LINEAR);
			group->AddProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_SECOND_VALUE"), PI_SCALE_SECOND + mask, m_second, locked || m_scaleType != ST_LINEAR || !m_useSecond);
		}
		group->AddProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_MAX"), PI_SCALE_MAX + mask, GetPropertyMax(), locked);
		/*if (m_scaleType == ST_DECIBEL)*/ {
			group->AddProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_INDEX"), PI_SCALE_EXP + mask, m_dbExp, locked || m_scaleType != ST_DECIBEL);
			group->AddProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_STANDARD_VALUE"), PI_SCALE_STD + mask, m_dbStd, locked || m_scaleType != ST_DECIBEL);
		}
	}
	{
		IPropertyGroup* group = parent->AddGroup(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_SCALE_KANJI"));
		group->AddProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_INTERVAL"),	PI_TICK_INTERVAL + mask, m_interval, locked || m_scaleType == ST_LOG || m_tickType == TT_NONE);
		if (!dynamic_cast<TAxis const*>(this))
		{
			group->AddProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_SIZE"), PI_TICK_SIZE + mask, m_tickSize, locked || m_tickType == TT_NONE);

			std::wstring typeProp1 = GetResourceString(owner->GetOwner(), "ID_SOURCE_TEXT_NONE_KANJI");
			std::wstring typeProp2 = GetResourceString(owner->GetOwner(), "ID_SOURCE_TEXT_INWARD");
			std::wstring typeProp3 = GetResourceString(owner->GetOwner(), "ID_SOURCE_TEXT_OUTWARD");
			std::wstring typeProp4 = GetResourceString(owner->GetOwner(), "ID_SOURCE_TEXT_INTERSECTION");
			group->AddArrayProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_TYPE"), PI_TICK_TYPE + mask, m_tickType, locked, 4, 
				typeProp1.c_str(), typeProp2.c_str(), typeProp3.c_str(), typeProp4.c_str());
		}
	}
	{
		m_tickLabelFontProp = m_tickLabelFont;
		m_tickLabelFontProp.lfWidth = m_tickLabelFontColor;

		IPropertyGroup* group = parent->AddGroup(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_SCALE_LABEL"), PI_TICK_LABELVISIBLE + mask, m_drawTickLabel, locked);

		std::wstring formProp1 = GetResourceString(owner->GetOwner(), "ID_SOURCE_TEXT_DECIMAL");
		std::wstring formProp2 = GetResourceString(owner->GetOwner(), "ID_SOURCE_TEXT_INDEX");
		group->AddArrayProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_SHOW_FORM"), PI_TICK_DISPLAY_TYPE + mask, m_scaleDispType.underlying(), locked, 2, 
			formProp1.c_str(), formProp2.c_str());

		std::wstring decimalProp1 = GetResourceString(owner->GetOwner(), "ID_SOURCE_TEXT_AUTO");
		std::wstring decimalProp2 = GetResourceString(owner->GetOwner(), "ID_RESOURCE_0");
		std::wstring decimalProp3 = GetResourceString(owner->GetOwner(), "ID_RESOURCE_1");
		std::wstring decimalProp4 = GetResourceString(owner->GetOwner(), "ID_RESOURCE_2");
		std::wstring decimalProp5 = GetResourceString(owner->GetOwner(), "ID_RESOURCE_3");
		std::wstring decimalProp6 = GetResourceString(owner->GetOwner(), "ID_RESOURCE_4");
		std::wstring decimalProp7 = GetResourceString(owner->GetOwner(), "ID_RESOURCE_FIVE");
		group->AddArrayProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_DECIMAL_DIGITS"), PI_TICK_FIGURE + mask, m_scaleFigure + 1, locked || !m_drawTickLabel, 7,
			decimalProp1.c_str(), decimalProp2.c_str(), decimalProp3.c_str(), decimalProp4.c_str(), decimalProp5.c_str(), decimalProp6.c_str(), decimalProp7.c_str());

		bool disableSIUnit = m_scaleType != ST_LINEAR;	// リニアのみSI文字を使用できる
		std::wstring si1 = GetResourceString(owner->GetOwner(), "ID_RESOURCE_OFF");
		std::wstring si2 = GetResourceString(owner->GetOwner(), "ID_RESOURCE_G");
		std::wstring si3 = GetResourceString(owner->GetOwner(), "ID_RESOURCE_M");
		std::wstring si4 = GetResourceString(owner->GetOwner(), "ID_RESOURCE_SMALL_K");
		std::wstring si5 = GetResourceString(owner->GetOwner(), "ID_RESOURCE_SMALL_M");
		std::wstring si6 = GetResourceString(owner->GetOwner(), "ID_RESOURCE_SMALL_U");
		std::wstring si7 = GetResourceString(owner->GetOwner(), "ID_RESOURCE_SMALL_N");
		std::wstring si8 = GetResourceString(owner->GetOwner(), "ID_RESOURCE_SMALL_P");
		group->AddArrayProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_SI_PREFIX"), PI_TICK_SIUNIT + mask, m_SIUnit, locked || disableSIUnit, 8,
			si1.c_str(), si2.c_str(), si3.c_str(),// ∵小文字"m"と区別するため(デジエ欠陥管理No.1参照)
			si4.c_str(), si5.c_str(), si6.c_str(), si7.c_str(), si8.c_str());

		group->AddBoolProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_USE_COMMA"), PI_LINKTEXT_USECOMMA + mask, m_useComma, locked);
		group->AddFontProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_FONT"), PI_TICK_FONT + mask, m_tickLabelFontProp, locked || !m_drawTickLabel);
	}
	//// T軸には表示しないプロパティ
	if (!dynamic_cast<TAxis const*>(this)) {
		{
			IPropertyGroup* group = parent->AddGroup(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_SUPPORTING_SCALE"), PI_AXIS_SUBTICK_VISIBLE + mask, m_drawSubTick, locked);
			group->AddProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_INTERVAL"), PI_SUBTICK_INTERVAL + mask, m_subInterval, locked || !m_drawSubTick || m_scaleType == ST_LOG);
		}
		{
			IPropertyGroup* group = parent->AddGroup(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_GRID"), PI_AXIS_GRID_VISIBLE + mask, m_drawGrid, locked);
			group->AddBoolProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_SYMMETRIC_LINE"), PI_AXIS_BORDER_LINE + mask, m_drawBorder, locked);
			/*if (m_drawGrid)*/ {
				group->AddColorProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_COLOR_KANJI"), PI_AXIS_GRID_LINECOLOR + mask, m_gridColor, locked || !m_drawGrid);
			}
		}
		{
			IPropertyGroup* grp = parent->AddGroup(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_PLACEMENT"));
			grp->AddProperty(::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_AXIAL_LENGTH"), PI_XAXIS_LENGTH + mask, m_length, locked);
		}
	}
}

bool Axis::SetProperty(long id, LPARAM value, LPARAM* source)
{
	//if (GetDimension() == 2 && m_graph->GetDimension() == 3) {
	//	// 3次元の場合はT軸も一緒に変更する
	//	m_graph->GetTAxis()->SetProperty(id, value, 0);
	//}

	switch (id)
	{
	case PI_SCALE_TYPE:
		m_autoScale = true;
		::SetPropertyHelper(m_scaleType, value, source);
		OnAutoScaleChanged();
		OnScaleChanged();
		OnAutoLabelChanged();
		return true;
	case PI_SCALE_AUTO:
		::SetPropertyHelper(m_autoScale, value, source);
		OnAutoScaleChanged();
		OnScaleChanged();
		return true;
	case PI_SCALE_FIT:
		::SetPropertyHelper(m_scaleFitting, value, source);
        OnAutoScaleChanged();
		OnScaleChanged();
		return true;
	case PI_SCALE_MIN:
		{
			double v = *reinterpret_cast<double*>(value);
			if (m_scaleType == ST_DECIBEL) {
				v = ToLinear(v);
			}
			else if (m_scaleType == ST_LOG) {
				// IsZeroだと、ログスケールで1e-12を設定したときとかに引っ掛かる
				if (v <= 0 /*|| IsZero(v)*/) {
					std::wstring msg = ::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_INPUT_VALUE_BIGGER_0");
					WarningMessage(owner->GetOwner(), msg.c_str());
					return false;
				}
			}
			if (v < m_maximum) {
				if (source) {
					if (m_scaleType == ST_DECIBEL) {
						*(double*)source = LinearToDecibel(m_minimum);
					}
					else {
						*(double*)source = m_minimum;
					}
				}
				m_minimum = v;
				m_second = std::max<double>(GetMinimum(), m_second);
				AdjustScaleInterval();
			}
		}	
		OnScaleChanged();
		return true;
	case PI_SCALE_MAX:
		{
			double v = *reinterpret_cast<double*>(value);
			bool update = true;
			if (m_scaleType == ST_DECIBEL) {
				v = ToLinear(v);
			}
			else if (m_scaleType == ST_LOG) {
				// IsZeroだと、ログスケールで1e-12を設定したときとかに引っ掛かる
				if (v <= 0 /*|| IsZero(v)*/) {
					std::wstring msg = ::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_INPUT_VALUE_BIGGER_0");
					WarningMessage(owner->GetOwner(), msg.c_str());
					return false;
				}
			}
			if (v > m_minimum) {
				if (source) {
					if (m_scaleType == ST_DECIBEL) {
						*(double*)source = LinearToDecibel(m_maximum);
					}
					else {
						*(double*)source = m_maximum;
					}
				}
				m_maximum = v;
				m_second = std::min<double>(GetMaximum(), m_second);
				AdjustScaleInterval();
			}
		}	
		OnScaleChanged();
		return true;
	case PI_TICK_INTERVAL:
		{
			if (m_scaleType == ST_LOG) { return false;}

			double v = m_interval;
			::SetPropertyHelper(v, value, source);
			if (v <= 0) {
				TCHAR const* msg = ::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_INPUT_BIGGER_0_INTO_SCALE_INTARVAL");
				WarningMessage(owner->GetOwner(), msg);
				return false;
			}
			else if(v >= ( GetMaximum() - GetMinimum())){
				TCHAR const* msg = ::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_SCALE_INTERVAL_TOO_BIG_INPUT_SMALLER_MAX_MINIUS_MIN");
				WarningMessage(owner->GetOwner(), msg);
				return false;
			}
			m_interval = v;
			// 目盛間隔が細かすぎてしまうことがあるので、ここで補正する
			AdjustScaleInterval();
		}
		OnScaleChanged();
		return true;
	case PI_SCALE_SECOND_USE:
		{
			// リニア以外は設定できない
			if (m_scaleType != ST_LINEAR) { return false;}
			::SetPropertyHelper(m_useSecond, value, source);
		}
		OnScaleChanged();
		return true;
	case PI_SCALE_SECOND:
		{
			// リニア以外は設定できない
			if (m_scaleType != ST_LINEAR) { return false;}

			double v = m_second;
			::SetPropertyHelper(v, value, source);

			if (v < m_minimum || v > m_maximum) { // 同じなのはOK
				std::wstring msg = ::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_INPUT_BETWEEN_MIN_MAX_SECOND_VALUE");
				::WarningMessage(owner->GetOwner(), msg.c_str());
				return false;
			}
			m_second = v;
		}
		OnScaleChanged();
		return true;
	case PI_SUBTICK_INTERVAL:
		{
			if (m_scaleType == ST_LOG) { return false;}

			double v = m_subInterval;
			::SetPropertyHelper(v, value, source);
			double linearCur = v;
			double linearInterval = m_interval;
			if (m_scaleType == ST_DECIBEL) {
				linearCur = ToLinear(linearCur);
				linearInterval = ToLinear(linearInterval);
			}
			if (v <= 0 || IsZero(v)) {
				TCHAR const* msg = ::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_INPUT_BIGGER_0_INTO_SUPPORT_SCALE_INTERVAL");
				WarningMessage(owner->GetOwner(), msg);
				return false;
			}
			else if (linearCur >= linearInterval) {
				TCHAR const* msg = ::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_INPUT_SMALLER_SCALE_INTERVAL_INTO_SUPPORT_SCALE_INTERVAL");
				WarningMessage(owner->GetOwner(), msg);
				return false;
			}
			m_subInterval = v;
		}
		OnScaleChanged();
		return true;
	case PI_AXIS_SUBTICK_VISIBLE:
		::SetPropertyHelper(m_drawSubTick, value, source);
		return true;
	case PI_TICK_SIZE:
		::SetPropertyHelper(m_tickSize, value, source);
		return true;
	case PI_TICK_TYPE:
		::SetPropertyHelper(m_tickType, value, source);
		return true;
	case PI_SCALE_EXP:
		{
			double v = m_dbExp;
			::SetPropertyHelper(v, value, source);
			if (v == 0) {
				std::wstring msg = ::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_INPUT_VALUE_EXCEPT_0_INTO_INDEX");
				WarningMessage(owner->GetOwner(), msg.c_str());
				return false;
			}
			m_dbExp = v;
		}
		return true;
	case PI_SCALE_STD:
		{
			double v = m_dbStd;
			::SetPropertyHelper(v, value, source);
			if (v <= 0 || v == 0) {
				std::wstring msg = ::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_INPUT_BIGGER_0_INTO_SATANDARD_VALUE");
				WarningMessage(owner->GetOwner(), msg.c_str());
				return false;
			}
			m_dbStd = v;
		}
		return true;
	case PI_TICK_LABELVISIBLE:
		::SetPropertyHelper(m_drawTickLabel, value, source);
		return true;
	case PI_TICK_DISPLAY_TYPE:
		::SetPropertyHelper(m_scaleDispType, value, source);
	case PI_TICK_FIGURE:
		{
			int src = m_scaleFigure + 1;
			::SetPropertyHelper(src, value, source);
			m_scaleFigure = src - 1;
		}
		return true;
	case PI_TICK_FONT:
		{
			CopyFont(source, m_tickLabelFont, m_tickLabelFontColor);
			m_tickLabelFont = *reinterpret_cast<LOGFONT*>(value);
			m_tickLabelFontColor = static_cast<COLORREF>(m_tickLabelFont.lfWidth);
			m_tickLabelFont.lfWidth = 0;
		}
		return true;
	case PI_TICK_SIUNIT:
		::SetPropertyHelper(m_SIUnit, value, source);
		OnAutoLabelChanged();
		return true;
	case PI_AXIS_BORDER_LINE:
		::SetPropertyHelper(m_drawBorder, value, source);
		return true;
	case PI_AXIS_GRID_VISIBLE:
		::SetPropertyHelper(m_drawGrid, value, source);
		return true;
	case PI_AXIS_GRID_LINESTYLE:
		::SetPropertyHelper(m_gridStyle, value, source);
		return true;
	case PI_AXIS_GRID_LINECOLOR:
		::SetPropertyHelper(m_gridColor, value, source);
		return true;
	case PI_AXIS_GRID_LINEWIDTH:
		::SetPropertyHelper(m_gridWidth, value, source);
		return true;
	case PI_AXIS_LABEL_VISIBLE:
		::SetPropertyHelper(m_labelVisible, value, source);
		return true;
	case PI_AXIS_LABEL:
		::SetPropertyHelper(m_label, value, source);
		m_autoLabel = false;
		return true;
	case PI_AXIS_AUTOLABEL:
		::SetPropertyHelper(m_autoLabel, value, source);
		OnAutoLabelChanged();
		return true;
	case PI_AXIS_LABEL_FONT:
		{
			CopyFont(source, m_labelFont, m_labelFontColor);
			m_labelFont = *reinterpret_cast<LOGFONT*>(value);
			m_labelFontColor = static_cast<COLORREF>(m_labelFont.lfWidth);
			m_labelFont.lfWidth = 0;
		}
		return true;
	case PI_XAXIS_LENGTH:
	case PI_YAXIS_LENGTH:
		if (::SetPropertyHelper(m_length, value, source, 5.0, 30.0)) {
			OnScaleChanged();
		}
		else {
			std::wstring msg = ::GetResourceString(owner->GetOwner(),"ID_SOURCE_TEXT_AXIS_LENGTH_MSG");
			::WarningMessage(owner->GetOwner(), msg.c_str());
		}
		return true;
	case PI_LINKTEXT_USECOMMA:
		::SetPropertyHelper(m_useComma, value, source);
		return true;
	}

	return false;
}

//! プロパティを取得します
bool Axis::GetProperty(long id, LPARAM* ret) const
{
	switch (id)
	{
	case PI_SCALE_MAX:
		*reinterpret_cast<double*>(ret) = GetPropertyMax();
		return true;
	case PI_SCALE_MIN:
		*reinterpret_cast<double*>(ret) = GetPropertyMin();
		return true;
	case PI_TICK_INTERVAL:
		*reinterpret_cast<double*>(ret) = GetInterval();
		return true;
	case PI_SUBTICK_INTERVAL:
		*reinterpret_cast<double*>(ret) = GetSubInterval();
		return true;
	case PI_SCALE_SECOND:
		*reinterpret_cast<double*>(ret) = GetSecondValue();
		return true;
	}

	return false;
}

void Axis::OnAutoScaleChanged()
{
	AutoScaleChanged(this);
}

void Axis::OnAutoLabelChanged()
{
	AutoLabelChanged(this);
}

void Axis::OnScaleChanged()
{
	ScaleChanged(this);
}

void Axis::AdjustScaleInterval()
{
	double const threshold = 100;

	if (m_scaleType == ST_LOG || m_scaleType == ST_DECIBEL)
	{
		bool const isLog = m_scaleType == ST_LOG;
		double div = isLog ? 1 : m_interval;
		double min = GetMinimum();
		double max = GetMaximum();

		if ((max - min) / m_interval > 100) {
			m_interval = div = (max - min) / 100;
			m_subInterval = m_interval / 5;
		}
		else if ((max - min) <= m_interval)
		{
			m_interval = div = (max - min) / 2;
			m_subInterval = m_interval / 5;
		}
	}
	else
	{
		if ((m_maximum - m_minimum) / m_interval > threshold)
		{
			m_interval = (m_maximum - m_minimum) / threshold;
			m_subInterval = m_interval / 5;
		}
		else if ((m_maximum - m_minimum) <= m_interval)
		{
			m_interval = (m_maximum - m_minimum) / 2;
			m_subInterval = m_interval / 5;
		}
	}

}

void Axis::Serialize(ISerializer* ar)
{
	ar->Serialize(_T("AutoScale"), m_autoScale);
	ar->Serialize(_T("ScaleFitting"), m_scaleFitting);
	ar->Serialize(_T("Min"), m_minimum, 0.0);
	ar->Serialize(_T("Max"), m_maximum, 1.0);
	ar->Serialize(_T("UseSecond"), m_useSecond, true);	// 3.5以前はセカンド値を使用のプロパティはなく、常にONだったのでない場合はtrue
	ar->Serialize(_T("Second"), m_second, 0.2);
	ar->Serialize(_T("Interval"), m_interval, 0.2);
	ar->Serialize(_T("SubInterval"), m_subInterval, m_interval / 5.0);
	MAKE_ENUM_SERIALIZE(ar, _T("ScaleType"), m_scaleType, ScaleType);
	ar->Serialize(_T("dBExp"), m_dbExp, 20);
	ar->Serialize(_T("dBStd"), m_dbStd, 1);
	ar->Serialize(_T("AutoLabel"), m_autoLabel);
	SerializeString(ar, _T("Label"), m_label);
	ar->Serialize(_T("LabelVisible"), m_labelVisible, true);
	ar->Serialize(_T("LabelFont"), m_labelFont);
	ar->Serialize(_T("LabelFontColor"), m_labelFontColor);
	ar->Serialize(_T("DrawTickLabel"), m_drawTickLabel, true);
	ar->Serialize(_T("TickSize"), m_tickSize, m_tickSize);
	MAKE_ENUM_SERIALIZE2(ar, _T("TickType"), m_tickType, TickType, TT_OUTSIDE);
	ar->Serialize(_T("TickFont"), m_tickLabelFont);
	ar->Serialize(_T("TickFontColor"), m_tickLabelFontColor, GetSymbolColor());
	ar->Serialize(_T("ScaleFigure"), m_scaleFigure);
	MAKE_SENUM_SERIALIZE(ar, _T("ScaleDisplayType"), m_scaleDispType, StringFormat);
	ar->Serialize(_T("UseComma"), m_useComma, m_useComma);
	MAKE_ENUM_SERIALIZE(ar, _T("SIUnit"), m_SIUnit, SIUnitLabel);
	ar->Serialize(_T("DrawSubTick"), m_drawSubTick, false);
	ar->Serialize(_T("DrawBorder"), m_drawBorder, true);
	ar->Serialize(_T("DrawGrid"), m_drawGrid, true);
	ar->Serialize(_T("GridStyle"), m_gridStyle);
	ar->Serialize(_T("GridColor"), m_gridColor, RGB(192, 192, 192));
	ar->Serialize(_T("GridWidth"), m_gridWidth, 1);
	ar->Serialize(_T("Length"), m_length, 10);

	if (IReadable* reader = dynamic_cast<IReadable*>(ar)) {
		if (reader->GetMajorVersion() == 3 && reader->GetMinorVersion() < 2) {
			// 3.1以前のOCPの復元
			if (owner->GetTypeId() == 28/*ウォーターフォール*/) {
				ar->Serialize(_T("Autoscale"), m_autoScale);
				ar->Serialize(_T("Autolabel"), m_autoLabel);

				// 3.01では、常にfalseで書き込まれているが、3.1では非表示にできないものであった
				m_drawTickLabel = true;

				// 以下の項目は3.2で新設したものなので、デフォルト値を設定しておく
				m_tickType = TT_OUTSIDE;
				m_tickSize = 0.4;
				m_interval = GetScaleTick(m_maximum - m_minimum);
				m_subInterval = m_interval / 5;
				m_second = m_minimum + m_interval;
			}
		}
	}

	CreateGrids();
}

#endif

///////////////////////////////////////////////////////////////////////////////////////////////////

XAxis::XAxis(Graph3DBase* owner)
	: Axis(owner)
{
	m_symbolColor = RGB(255, 0, 0);
	m_tickLabelFontColor = GetSymbolColor();
}

bool XAxis::IsVisibleTicks() const
{
	return (owner->GetPitch() != 0.0 && owner->GetPitch() != 180.0F) || (owner->GetYaw() != 90.0F && owner->GetYaw() != -90.0F);
}

double XAxis::ValueToPosition(double value) const
{
	double p = ValueToCoordinate(value);
	return p - GetSize();
}

double XAxis::ValueToPositionRaw(double value) const
{
	double p = ValueToCoordinateRaw(value);
	return p - GetSize();
}

double XAxis::GetEdge() const
{
	return GetSize() * GetEdgePos(owner->GetPitch(), owner->GetYaw(), owner->GetRoll()).get<0>();
}

std::vector<Vertex> XAxis::GetGridPos(double coordinate, bool reverse) const
{
	double const x = owner->GetXAxis()->GetSize();
	double const y = owner->GetZAxis()->GetEdge();
	double const z = owner->GetYAxis()->GetEdge();

	double const xg = coordinate - x;
	std::vector<Vertex> grids;
	if (reverse)
	{
		grids.push_back(Vertex(xg, -y, z));		// bottom
		grids.push_back(Vertex(xg, -y, -z));
		grids.push_back(Vertex(xg, -y, -z));	// wall
		grids.push_back(Vertex(xg, y, -z));
	}
	else
	{
		grids.push_back(Vertex(xg, y, -z));		// wall
		grids.push_back(Vertex(xg, -y, -z));
		grids.push_back(Vertex(xg, -y, -z));	// bottom
		grids.push_back(Vertex(xg, -y, z));
	}
	return grids;
}

std::vector<Vertex> XAxis::GetTickPos(double coordinate, double length) const
{
	double const x = owner->GetXAxis()->GetSize();
	double const y = owner->GetZAxis()->GetEdge();
	double const z = owner->GetYAxis()->GetEdge();
	double const xg = coordinate - x;

	if (owner->GetYAxis()->GetEdge() < 0) {
		length = -length;
	}

	std::vector<Vertex> ticks;
	switch (m_tickType)
	{
	case TT_OUTSIDE:
		ticks.push_back(Vertex(xg, -y, z));
		ticks.push_back(Vertex(xg, -y, z + length));
		break;
	case TT_CROSS:
		ticks.push_back(Vertex(xg, -y, z - length / 2));
		ticks.push_back(Vertex(xg, -y, z + length / 2));
		break;
	case TT_INSIDE:
		ticks.push_back(Vertex(xg, -y, z));
		ticks.push_back(Vertex(xg, -y, z - length));
		break;
	}
	return ticks;
}

Axis* XAxis::GetBorderAxis() const
{
	return owner->GetZAxis();
}

void XAxis::RenderLabel() const
{
	double const sign = (owner->GetYAxis()->GetEdge() < 0) ? -1 : 1;
	double const zSign = owner->GetZAxis()->GetEdge() < 0 ? -1 : 1;
	double const textLength = 3;

	double const x = 0;
	double const y = -(owner->GetZAxis()->GetSize() * 1.3) * zSign;
	double const z = owner->GetYAxis()->GetEdge() + (m_tickSize + textLength) * sign;
	double /*const */angle = ::GetAngle(OCPoint(-1, -1), OCPoint(1, -1));

	if (angle < -90 || angle > 90) {
		angle -= 180;
	}

	double wx, wy;
	::WorldToWindow(x, y, z, &wx, &wy);

	HDC device = ::GetDC(owner->GetWnd());
	SIZE const labelSize = textRenderer->GetTextSize(device, m_label.c_str(), m_labelFont);
	wy -= labelSize.cy / 2;
	::ReleaseDC(owner->GetWnd(), device);

	textRenderer->AddText(m_label, (int)wx, (int)wy, angle, m_labelFontColor, TextAlign::Center, m_labelFont);
}

TextAlign::type XAxis::GetTickLabelAlign() const
{
	return TextAlign::Center;
}

Vertex XAxis::GetTickPosition(size_t index) const
{
	double x = owner->GetXAxis()->GetSize();
	double y = owner->GetZAxis()->GetEdge();
	double z = owner->GetYAxis()->GetEdge();

	double const sign = (owner->GetYAxis()->GetEdge() < 0) ? -1 : 1;
	double const textLength = 1;

	double const yPos = -y * 1.13;
	double const zPos = z  + (m_tickSize + textLength) * sign;
	return Vertex(GetGridInfo(index).Coordinate - x, yPos, zPos);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
YAxis::YAxis(Graph3DBase* owner)
	: Axis(owner)
{
	m_symbolColor = RGB(0, 128, 0);
	m_tickLabelFontColor = GetSymbolColor();
}

bool YAxis::IsVisibleTicks() const
{
	return (owner->GetPitch() != 0.0F && owner->GetPitch() != 180.0F) || (owner->GetYaw() != 0.0F && owner->GetYaw() != 180.0F);
}

double YAxis::ValueToPosition(double value) const
{
	double p = ValueToCoordinate(value);
	return GetSize() - p;
}

double YAxis::ValueToPositionRaw(double value) const
{
	double p = ValueToCoordinateRaw(value);
	return GetSize() - p;
}

double YAxis::GetEdge() const
{
	return GetSize() * GetEdgePos(owner->GetPitch(), owner->GetYaw(), owner->GetRoll()).get<1>();
}

std::vector<Vertex> YAxis::GetGridPos(double coordinate, bool reverse) const
{
	double const x = owner->GetXAxis()->GetEdge();
	double const y = owner->GetZAxis()->GetEdge();
	double const z = owner->GetYAxis()->GetSize();

	std::vector<Vertex> grids;
	double zg = z - coordinate;
	if (reverse)
	{
		grids.push_back(Vertex(x, -y, zg));
		grids.push_back(Vertex(-x, -y, zg));
		grids.push_back(Vertex(-x, -y, zg));
		grids.push_back(Vertex(-x, y, zg));
	}
	else
	{
		grids.push_back(Vertex(-x, y, zg));
		grids.push_back(Vertex(-x, -y, zg));
		grids.push_back(Vertex(-x, -y, zg));
		grids.push_back(Vertex(x, -y, zg));
	}
	return grids;
}

std::vector<Vertex> YAxis::GetTickPos(double coordinate, double length) const
{
	double const x = owner->GetXAxis()->GetEdge();
	double const y = owner->GetZAxis()->GetEdge();
	double const z = owner->GetYAxis()->GetSize();
	double zg = z - coordinate;

	if (owner->GetXAxis()->GetEdge() < 0) {
		length = -length;
	}

	std::vector<Vertex> ticks;
	switch (m_tickType)
	{
	case TT_OUTSIDE:
		ticks.push_back(Vertex(x, -y, zg));
		ticks.push_back(Vertex(x + length, -y, zg));
		break;
	case TT_CROSS:
		ticks.push_back(Vertex(x - length / 2, -y, zg));
		ticks.push_back(Vertex(x + length / 2, -y, zg));
		break;
	case TT_INSIDE:
		ticks.push_back(Vertex(x, -y, zg));
		ticks.push_back(Vertex(x - length, -y, zg));
		break;
	}
	return ticks;
}

Axis* YAxis::GetBorderAxis() const
{
	return owner->GetXAxis();
}

void YAxis::RenderLabel() const
{
	double const x = owner->GetXAxis()->GetSize();
	double const y = -owner->GetZAxis()->GetEdge() * 1.3;
	double const z = 0;
	double /*const */angle = ::GetAngle(OCPoint(1, 1), OCPoint(1, -1));

	if (angle < -90 || angle > 90) {
		angle -= 180;
	}

	double const sign = (owner->GetXAxis()->GetEdge() < 0) ? -1 : 1;
	double const textLength = 3;
	double const xPos = (x + m_tickSize + textLength) * sign;

	double wx, wy;
	::WorldToWindow(xPos, y, z, &wx, &wy);

	HDC device = ::GetDC(owner->GetWnd());
	SIZE const labelSize = textRenderer->GetTextSize(device, m_label.c_str(), m_labelFont);
	wy -= labelSize.cy / 2;
	::ReleaseDC(owner->GetWnd(), device);
	
	TextRenderer::GetInstance()->AddText(m_label, (int)wx, (int)wy, angle, m_labelFontColor, TextAlign::Center, m_labelFont);
}

TextAlign::type YAxis::GetTickLabelAlign() const
{
	return TextAlign::Center;
}

Vertex YAxis::GetTickPosition(size_t index) const
{
	double x = owner->GetXAxis()->GetEdge();
	double y = owner->GetZAxis()->GetEdge();
	double z = owner->GetYAxis()->GetSize();

	double const sign = (owner->GetXAxis()->GetEdge() < 0) ? -1 : 1;
	double const textLength = 1;
	double const xPos = x + (m_tickSize + textLength) * sign;
	double const yPos = -y * 1.13;
	return Vertex(xPos, yPos, z - GetGridInfo(index).Coordinate);
}

///////////////////////////////////////////////////////////////////////////////////////////////////

ZAxis::ZAxis(Graph3DBase* owner)
	: Axis(owner)
{
	m_symbolColor = RGB(0, 0, 255);
	m_tickLabelFontColor = GetSymbolColor();
}

bool ZAxis::IsVisibleTicks() const
{
	return (owner->GetPitch() != 90.0F && owner->GetPitch() != -90.0F) || (owner->GetRoll() != 0.0 && owner->GetRoll() != 180.0F);
}

double ZAxis::ValueToPosition(double value) const
{
	double p = ValueToCoordinate(value);
	return p - GetSize();
}

double ZAxis::ValueToPositionRaw(double value) const
{
	double p = ValueToCoordinateRaw(value);
	return p - GetSize();
}

double ZAxis::GetEdge() const
{
	return GetSize() * GetEdgePos(owner->GetPitch(), owner->GetYaw(), owner->GetRoll()).get<2>();
}

std::vector<Vertex> ZAxis::GetGridPos(double coordinate, bool reverse) const
{
	double const x = owner->GetXAxis()->GetEdge();
	double const y = owner->GetZAxis()->GetSize();
	double const z = owner->GetYAxis()->GetEdge();

	std::vector<Vertex> grids;
	double yg = coordinate - y;
	if (reverse)
	{
		grids.push_back(Vertex(-x, yg, z));
		grids.push_back(Vertex(-x, yg, -z));
		grids.push_back(Vertex(-x, yg, -z));
		grids.push_back(Vertex(x, yg, -z));
	}
	else
	{
		grids.push_back(Vertex(x, yg, -z));
		grids.push_back(Vertex(-x, yg, -z));
		grids.push_back(Vertex(-x, yg, -z));
		grids.push_back(Vertex(-x, yg, z));
	}
	return grids;
}

std::vector<Vertex> ZAxis::GetTickPos(double coordinate, double length) const
{
	double const sign = GetZSign(owner);
	double const x = owner->GetXAxis()->GetEdge() * sign;
	double const y = owner->GetZAxis()->GetSize();
	double const z = owner->GetYAxis()->GetEdge() * sign;
	double yg = coordinate - y;

	//TODO: refactoring
	std::vector<Vertex> ticks;
	int /*const*/ angle = GetAngleNumber(owner->GetYaw());
	double const xAngle = owner->GetPitch();
	if (IsZero(owner->GetRoll())) {
		if (xAngle > 90 || xAngle < -90) {
			--angle;
			if (angle < 0) { angle=3; }
		}

		switch (angle)
		{
		case 0:
			length = -length;
			switch (m_tickType)
			{
			case TT_OUTSIDE:
				ticks.push_back(Vertex(x, yg, -z));
				ticks.push_back(Vertex(x, yg, -z - length));
				break;
			case TT_CROSS:
				ticks.push_back(Vertex(x, yg, -z - length / 2));
				ticks.push_back(Vertex(x, yg, -z + length / 2));
				break;
			case TT_INSIDE:
				ticks.push_back(Vertex(x, yg, -z));
				ticks.push_back(Vertex(x, yg, -z + length));
				break;
			}
			break;
		case 1:
			switch (m_tickType)
			{
			case TT_OUTSIDE:
				ticks.push_back(Vertex(x, yg, -z));
				ticks.push_back(Vertex(x - length, yg, -z));
				break;
			case TT_CROSS:
				ticks.push_back(Vertex(x - length / 2, yg, -z));
				ticks.push_back(Vertex(x + length / 2, yg, -z));
				break;
			case TT_INSIDE:
				ticks.push_back(Vertex(x, yg, -z));
				ticks.push_back(Vertex(x + length, yg, -z));
				break;
			}
			break;
		case 2:
			switch (m_tickType)
			{
			case TT_OUTSIDE:
				ticks.push_back(Vertex(x, yg, -z));
				ticks.push_back(Vertex(x, yg, -z - length));
				break;
			case TT_CROSS:
				ticks.push_back(Vertex(x, yg, -z - length / 2));
				ticks.push_back(Vertex(x, yg, -z + length / 2));
				break;
			case TT_INSIDE:
				ticks.push_back(Vertex(x, yg, -z));
				ticks.push_back(Vertex(x, yg, -z + length));
				break;
			}
			break;
		case 3:
			length = -length;
			switch (m_tickType)
			{
			case TT_OUTSIDE:
				ticks.push_back(Vertex(x, yg, -z));
				ticks.push_back(Vertex(x - length, yg, -z));
				break;
			case TT_CROSS:
				ticks.push_back(Vertex(x - length / 2, yg, -z));
				ticks.push_back(Vertex(x + length / 2, yg, -z));
				break;
			case TT_INSIDE:
				ticks.push_back(Vertex(x, yg, -z));
				ticks.push_back(Vertex(x + length, yg, -z));
				break;
			}
			break;
		}
	}
	else {
		switch (angle)
		{
		case 0:
			length = -length;
			switch (m_tickType)
			{
			case TT_OUTSIDE:
				ticks.push_back(Vertex(x, yg, -z));
				ticks.push_back(Vertex(x, yg, -z - length));
				break;
			case TT_CROSS:
				ticks.push_back(Vertex(x, yg, -z - length / 2));
				ticks.push_back(Vertex(x, yg, -z + length / 2));
				break;
			case TT_INSIDE:
				ticks.push_back(Vertex(x, yg, -z));
				ticks.push_back(Vertex(x, yg, -z + length));
				break;
			}
			break;
		case 1:
			if ((xAngle < 0 && xAngle > -90) || (xAngle > 0 && xAngle < 90)) {
				length *= -1;
			}
			switch (m_tickType)
			{
			case TT_OUTSIDE:
				ticks.push_back(Vertex(x, yg, -z));
				ticks.push_back(Vertex(x, yg, -z - length));
				break;
			case TT_CROSS:
				ticks.push_back(Vertex(x, yg, -z - length / 2));
				ticks.push_back(Vertex(x, yg, -z + length / 2));
				break;
			case TT_INSIDE:
				ticks.push_back(Vertex(x, yg, -z));
				ticks.push_back(Vertex(x, yg, -z + length));
				break;
			}
			break;
		case 2:
			switch (m_tickType)
			{
			case TT_OUTSIDE:
				ticks.push_back(Vertex(x, yg, -z));
				ticks.push_back(Vertex(x, yg, -z - length));
				break;
			case TT_CROSS:
				ticks.push_back(Vertex(x, yg, -z - length / 2));
				ticks.push_back(Vertex(x, yg, -z + length / 2));
				break;
			case TT_INSIDE:
				ticks.push_back(Vertex(x, yg, -z));
				ticks.push_back(Vertex(x, yg, -z + length));
				break;
			}
			break;
		case 3:
			switch (m_tickType)
			{
			case TT_OUTSIDE:
				ticks.push_back(Vertex(x, yg, -z));
				ticks.push_back(Vertex(x, yg, -z - length));
				break;
			case TT_CROSS:
				ticks.push_back(Vertex(x, yg, -z - length / 2));
				ticks.push_back(Vertex(x, yg, -z + length / 2));
				break;
			case TT_INSIDE:
				ticks.push_back(Vertex(x, yg, -z));
				ticks.push_back(Vertex(x, yg, -z + length));
				break;
			}
			break;
		}
	}
	return ticks;
}

Axis* ZAxis::GetBorderAxis() const
{
	return owner->GetYAxis();
}

double GetMaxLength(double a, double b)
{
	return GetDigits(a) > GetDigits(b) ? a : b;
}

void ZAxis::RenderLabel() const
{
	HDC device = ::GetDC(owner->GetWnd());
	SIZE const labelSize = textRenderer->GetTextSize(device, m_label.c_str(), m_labelFont);
	SIZE const tickSize = textRenderer->GetValueTextSize(device, GetMaxLength(m_minimum, m_maximum), m_tickLabelFont);

	double const sign = GetZSign(owner);
	double const x = owner->GetXAxis()->GetEdge() * sign;
	double const y = 0;
	double const z = -owner->GetYAxis()->GetEdge() * sign;

	double wx, wy;
	::WorldToWindow(x, y, z, &wx, &wy);

	if (owner->GetRoll() == 0) {
		wx -= (tickSize.cx + 15);
		if (GetTickLabelAlign() == TextAlign::Right) {
			wx -= labelSize.cy;
		}
		wy -= labelSize.cy;

		double const angle = -90;
		textRenderer->AddText(m_label, (int)wx, (int)wy, angle, m_labelFontColor, TextAlign::Center, m_labelFont);
	}
	else {
		// Z-X平面のときは、横書きにする
		wy -= (tickSize.cx + 15);
		if (GetTickLabelAlign() == TextAlign::Right) {
			wy -= labelSize.cy;
		}

		double const angle = 0;
		textRenderer->AddText(m_label, (int)wx, (int)wy, angle, m_labelFontColor, TextAlign::Center, m_labelFont);
	}
	::ReleaseDC(owner->GetWnd(), device);
}

TextAlign::type ZAxis::GetTickLabelAlign() const
{
	return TextAlign::Right;
}

Vertex ZAxis::GetTickPosition(size_t index) const
{
	double const sign = GetZSign(owner);
	double x = owner->GetXAxis()->GetEdge() * 1.08 * sign;
	double y = owner->GetZAxis()->GetSize();
	double z = owner->GetYAxis()->GetEdge() * -sign;

	double const zPos = z * 1.08;
	return Vertex(x, GetGridInfo(index).Coordinate - y, zPos);
}

