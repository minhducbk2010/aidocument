#pragma once

class GLBuffer
{
public:
	GLBuffer(GLenum target, int size, const GLvoid *data, GLenum usage);
	virtual ~GLBuffer();

	void *Bind();
	void Unbind();
	GLvoid *Map(GLenum target);
	GLboolean Unmap();

private:
	GLenum target;
	GLuint id;
	BYTE *buffer;
};
