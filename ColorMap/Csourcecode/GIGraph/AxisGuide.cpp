#include "stdafx.h"
#include "AxisGuide.h"
#include "GuideArrow.h"

AxisGuide::AxisGuide()
{
	xArrow = new GuideArrow(0.0F, 0.0F, 0.0F, 10.0F, 0.0F, 0.0F, 1.0F, 0.0F, 0.0F);
	yArrow = new GuideArrow(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -10.0F, 0.0F, 0.5F, 0.0F);
	zArrow = new GuideArrow(0.0F, 0.0F, 0.0F, 0.0F, 10.0F, 0.0F, 0.0F, 0.0F, 1.0F);
}
AxisGuide::~AxisGuide()
{
	delete zArrow;
	delete yArrow;
	delete xArrow;
}

void AxisGuide::Modeling()
{
	xArrow->Modeling(90.0, 0.0, 1.0, 0.0);
	yArrow->Modeling(180.0, 1.0, 0.0, 0.0);
	zArrow->Modeling(-90.0, 1.0, 0.0, 0.0);
}
void AxisGuide::Render(double pitch, double yaw, double roll)
{
	float const scale = 0.1f;

	::glPushMatrix();
	::glTranslated(-18.5, -17, 0);
	::glScalef(scale, scale, scale);
	::glRotated(pitch, 1, 0, 0);
	::glRotated(yaw, 0, 1, 0);
	::glRotated(roll, 0, 0, 1);
	xArrow->Render(L"X");
	yArrow->Render(L"Y");
	zArrow->Render(L"Z");
	::glPopMatrix();
}
