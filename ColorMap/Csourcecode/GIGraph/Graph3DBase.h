#pragma once

#include <map>
#include <boost/shared_ptr.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/function.hpp>
#include <boost/signals2.hpp>
#include <wtl/atlapp.h>
#include <wtl/atlmisc.h>
#include "utility.h"
#include "IPlot.h"
#include "MessageHandler.h"

class Axis;
class Section;
class GLBuffer;
class ColorBar;
class AxisGuide;
class DeviceContext;
class IGraphUnit;
class PlotGroup;
class IPlotData;
struct PreparedObject;
// O-Chart
class IShape;
class IGroup;
class ISerializer;
class IGraphFile;
class IMessageReceiver;
class OCProperty;
struct IPropertyComposite;
class IPropertyGroup;
class ColorTexture;
class ShapeWrapper;

class Graph3DBase : public IFilterOwner, public MessageHandler
{
	friend PreparedObject;
public:
	Graph3DBase(HWND window, IMessageReceiver* owner);
	virtual ~Graph3DBase();
	
	virtual void SetParent(IGroup* parent) { m_parent = parent; }
	virtual void SetSelected(bool select) { select; }

	HWND GetWnd() const { return m_wnd; }

	virtual int GetTypeId() const = 0;
	virtual void SetData(IPlotData* data);
	virtual void SetData(size_t index ,IPlotData* data) = 0;
	virtual IPlotData* GetData(size_t index) const = 0;
	virtual size_t GetDataCount() const = 0;
	virtual IGraphFile* AddFile(size_t fileId);
	virtual IShape* AddData(IPlotData* data) = 0;
	virtual IShape* AddPlot(IPlotData* data, size_t kind) {data, kind;return nullptr;}
	virtual void RemovePlot(size_t index) {index;}
	virtual void ClearPlots() {}
	virtual void SetPlotKind(int kind) = 0;
	virtual void SetPlotVisible(int index, int kind, bool visible) = 0;

	Axis* GetXAxis() const { return m_xAxis.get(); }
	Axis* GetYAxis() const { return m_yAxis.get(); }
	Axis* GetZAxis() const { return m_zAxis.get(); }
	Axis* GetTAxis() const { return m_tAxis.get(); }

	virtual size_t GetFileCount() const { return 0; }
	virtual IGraphFile* GetFile(size_t /*index*/) const { return nullptr; }
	virtual IGraphFile* GetEmptyFile() const { return nullptr; }

	virtual BOOL GetViewColorBar() const		{ return m_viewColorBar; }
	void SetViewColorBar(bool value)	{ m_viewColorBar = value; }
	BOOL GetViewAxisGuide() const		{ return m_viewAxisGuide; }
	void SetViewAxisGuide(bool value)	{ m_viewAxisGuide = value; }
	virtual BOOL GetViewWireframe() const = 0;
	virtual void SetViewWireframe(bool value) = 0;
	virtual void EnableColoring(bool enable) = 0;
	virtual BOOL IsColoring() const = 0;
	virtual void EnableFilling(bool enable) = 0;
	virtual BOOL IsFilling() const = 0;
	virtual void EnableHiddenLine(bool hidden) = 0;
	virtual bool IsHiddenLine() const = 0;
	//TODO: ウォーターフォールでも対応したい
	virtual void EnableLighting(bool enable) {enable;}
	virtual bool IsLighting() const {return false;}
	void EnableClipping(bool clipping);
	bool IsClipping() const;
	virtual void VisibleCursor(bool visible) = 0;
	virtual void ResetCursor() = 0;

	void CursorValueChanged(OCPoint3D const& point) const;

	BOOL GetClipMaxX();
	BOOL GetClipMinX();
	BOOL GetClipMaxZ();
	BOOL GetClipMinZ();
	BOOL GetClipMaxY();
	BOOL GetClipMinY();
	void SetClipMaxX(bool value);
	void SetClipMinX(bool value);
	void SetClipMaxZ(bool value);
	void SetClipMinZ(bool value);
	void SetClipMaxY(bool value);
	void SetClipMinY(bool value);
	bool GetCursorLineVisible() const;
	void SetCursorLineVisible(bool value);
	COLORREF GetWireframeColor();
	void SetWireframeColor(COLORREF color);

	void ConvertValueToPosition(double& x, double& y, double& z);
	void RenderPlanes();
	void GetValue(float x, float y, float z, float *vX, float *vY, float *vZ);
	void GetOrder(float x, float z, float *order);

	double GetPitch() const	{ return m_pitch; }
	double GetYaw() const	{ return m_yaw; }
	double GetRoll() const	{ return m_roll; }
	double GetScale() const	{ return m_scale; }

	void Rotate(double pitch, double yaw, double roll);
	void Scaling(double scale);
	void Translate(double h, double v);
	void GetPosition(double* h, double* v) const;

	void OffsetRotate(double pitch, double yaw, double roll);
	void OffsetScale(double scale);
	void OffsetTranslate(double h, double v);

	void Render(HDC dc);
	void RenderFrame(HDC dc);
	void Reshape(int width, int height, double aspect, bool render = false);
	void Display();
	void Capture(HDC deviceContext, int width, int height, bool bitmap = true);

	virtual void UpdateLock();
	virtual void UpdateUnlock();

	void AutoScaleAxes();
	void AutoLabelAxes();

	int SavePreEditState();
	void RestoreEditState(int state);

// Controller

	//TODO: 暫定
	virtual int KeyDownCursorOffset() const { return 1; }

	virtual bool EnableCursor() const = 0;
	virtual void SetCursor(int x, int y) = 0;
	virtual void SelectIndex(int index, int line);
	virtual void GetCursorIndex(int* index, int* line) const;

	virtual bool PickUpData(HDC dc, int x, int y){dc, x, y;return false;}
	virtual bool HitTestMarker(HDC dc, int x, int y) {dc;return false;}
	void Click(int x, int y);
	virtual bool DragMarker(OCPoint const& offset){offset; return false;}
	virtual void EndMarkerDrag(){}

	void BeginDrag();
	void EndDrag();

// O-Chart

	virtual int GetActivePlotIndex() const { return 0; }
	virtual COLORREF GetActivePlotColor() const { return 0; }

// Group
	virtual size_t GetChildCount() const { return 0; }
	virtual IShape* GetChild(size_t /*index*/) { return 0; }

	virtual void RemoveChild(IShape* /*obj*/) {}
	virtual void InsertChild(IShape* /*obj*/) {}

// Operation
	void Editing(bool editing) { m_editing = editing; }

	IMessageReceiver* GetOwner() const { return m_owner; }
	void AttachViewer(HWND viewer) { m_viewer = viewer; }
	virtual void SetDimension(int dim);
	long GetDimension() { return m_dimension; }

	virtual bool UseDigitalmap() const { return false; }
	virtual void ShowDigitalmap() {}

	virtual void AddShowPropertyOption(IPropertyGroup* /*group*/, bool /*locked*/) {}
	void AddShowProperty(IPropertyComposite* parent, bool locked) const;
	void AddProperty(OCProperty* prop, bool locked) const;
	void AddTabProperty(OCProperty* prop, bool locked) const;
	bool SetProperty(long id, LPARAM value, LPARAM* source);
	bool GetProperty(long id, LPARAM* ret) const;
	void Serialize(ISerializer* ar);

	virtual bool CheckUseColorPattern(TCHAR const* path) = 0;
	virtual bool UpdateColorPattern(TCHAR const* path) = 0;

	Graph3DBase* Clone() const;

	void DoCommand(UINT commandId);
	void GetCommandState(UINT id, BOOL* enable, BOOL* checked) const;
	bool HasCommand(UINT commandId);

	virtual bool EnableHorizontalSection() const { return m_horizontanlSection; }
	virtual bool EnableVerticalSection() const { return m_verticalSection; }

	virtual bool CanAddFile() const = 0;
	virtual bool CheckUseFile(size_t fileId) const = 0;
	virtual void UpdatePlotData() = 0;

	virtual void SetLocation(double left, double top) {left, top;}

// IFilterOwner
	virtual IFile* GetFile() const;
	virtual IFilter* GetFilter() const;

// event
	boost::signals2::signal<void(int width, int height)> Resized;
	boost::signals2::signal<void(IShape*)> AddChild;
	boost::signals2::signal<void(bool)> PropertyUpdated;
	boost::signals2::signal<double(void)> GetZoomRatio;
	boost::signals2::signal<void(double*, double*, double*, double*)> GetRect;
	boost::signals2::signal<void(float, float, float)> Rotated;

	void OnPropertyUpdated(bool all_update = false);

// MessageHandler
	virtual void EnabledColorBar(BOOL* enabled, BOOL* checked) const = 0;

protected:
	void RenderImpl(HDC dc, bool plotRendering);

	void AutoScaleAxis(Axis* axis);
	void AutoScaleVectorColorBarAxis(Axis* axis);
	void AutoLabel(Axis* axis);
	void AutoVectorColorBarAxisLabel(Axis* axis);
	virtual void AfterAutoScaleAxes() {}

	virtual bool EnableSection() const;
	void DrawSectionToggleButton(HDC dc) const;
	WTL::CRect GetVertShowButtonRect() const;
	WTL::CRect GetHorzShowButtonRect() const;

	virtual Graph3DBase* Create() const = 0;
	virtual void RenderPlots(HDC dc) const = 0;
	virtual void SetDataAfter() {}
	virtual void AddPropertyImpl(OCProperty* prop, bool locked) const = 0;
	virtual void AddTabPropertyImpl(OCProperty* prop, bool locked) const = 0;
	virtual bool SetPropertyImpl(long id, LPARAM value, LPARAM* source) = 0;
	virtual bool GetPropertyImpl(long id, LPARAM* ret) const = 0;
	virtual void SerializeImpl(ISerializer* ar) = 0;

	void SetDataProperty(int current, int dim, LPARAM value, LPARAM* source);

	void TransformProjection(double aspectRate) const;

	void DrawSection(HDC deviceContext, WTL::CRect const& sectionRect);

// O-Chart
// Command Method
	void ToggleWireFrameVisible();
	void ToggleHideLine();
	void ToggleColoring();
	void ToggleFilling();
	void ToggleColorBarVisible();
	void ToggleGuideVisible();
	virtual void ResetPosition() = 0;
	void SetXYPosition();
	void SetYZPosition();
	void SetZXPosition();

	void CursorChanged(std::vector<OCPoint> const& xLine, std::vector<OCPoint> const& yLine);

	boost::shared_ptr<PreparedObject> CreatePrepateObject() const;

	// Property
	void AddDataProperty(IPropertyComposite* parent, bool locked) const;

	IFile* GetFileByIndex(int index) const;

private:
	virtual void RenderColorBar() const = 0;
	void MakeLabel(Axis* axis);

protected:
	HWND m_wnd;
	HWND m_viewer;
	IMessageReceiver* m_owner;
	IGroup* m_parent;

	boost::shared_ptr<Axis> m_xAxis;
	boost::shared_ptr<Axis> m_yAxis;
	boost::shared_ptr<Axis> m_zAxis;
	boost::shared_ptr<Axis> m_tAxis;
	boost::shared_ptr<AxisGuide> m_axisGuide;
	boost::shared_ptr<Section> m_xSection;	//!< X断面
	boost::shared_ptr<Section> m_ySection;	//!< Y断面

	std::vector<Axis*> m_axes;

	int m_width;
	int m_height;
	bool m_horizontanlSection;
	bool m_verticalSection;
	
	int m_dimension;

	double m_pitch, m_yaw, m_roll;
	double m_scale;
	double m_h, m_v;

	bool m_viewColorBar;
	bool m_viewAxisGuide;

	bool m_clipMaxX;
	bool m_clipMinX;
	bool m_clipMaxZ;
	bool m_clipMinZ;
	bool m_clipMaxY;
	bool m_clipMinY;

	COLORREF m_wireframeColor;
	bool m_cursorLineVisible;

	mutable double m_cursorValue[3]; // x, y, z
	bool m_editing;
	bool m_dragging;
	bool m_lock;

	bool m_serializing;
	bool m_capturing;

	struct UndoBuffer {
		double xAngle, yAngle, zAngle;
		double scale;
		double xPos, yPos;
	};
	std::map<int, UndoBuffer> m_undo;
};
