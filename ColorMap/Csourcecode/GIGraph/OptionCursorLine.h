#pragma once

class Graph3DBase;
class GLBuffer;

class OptionCursorLine
{
public:
	OptionCursorLine(Graph3DBase *graphBase);
	virtual ~OptionCursorLine();

	void SetLineColor(double r, double g, double b);
	void Modeling(double *x, double *y, double *z, int count);
	void Render();

private:
	Graph3DBase *graphBase;
	GLBuffer *vertex;
	int vertexCount;
	bool view;
	double r, g, b;
};
