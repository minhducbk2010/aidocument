/*!	@file
*/
#include "stdafx.h"
#include <locale>
#include <boost/shared_ptr.hpp>
#include "DllMain.h"
#include "SolidGraph/SolidGraph.h"
#include "SolidGraph/PlotGroup.h"
#include "SolidGraph/ContourPlot.h"
#include "Waterfall/WaterFall.h"
#include "Axis.h"
#include "TextRenderer.h"
#include "DummyReceiver.h"
#include "NullableCanvas.h"
#include "DllData.h"
#include "Property.h"
#include "IShape.h"
#include "OChart/GraphWrapper.h"

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")
#pragma comment(lib, "gdiplus.lib")

HINSTANCE dllInstance = NULL;
HWND g_applicationWindow;
// GraphLibから呼ばれているか?
BOOL g_isLibMode = FALSE;

namespace {
ULONG_PTR gdiToken;
Gdiplus::GdiplusStartupInput gdiStartInput;
}

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID /*lpvReserved*/)
{
	switch (fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		dllInstance = hinstDLL;
		Gdiplus::GdiplusStartup(&gdiToken, &gdiStartInput, nullptr);
		setlocale(LC_ALL, "japanese");
		break;
	case DLL_PROCESS_DETACH:
		Gdiplus::GdiplusShutdown(gdiToken);
		dllInstance = NULL;
		break;
	}

	return TRUE;
}

// API ////////////////////////////////////////////////////////////////////////
// helper

#pragma warning(disable:4100) //@todo とりあえず(作業途中の警告除去。最後にこのpragmaは削除する)

Graph3DBase* CreateSolidGraph(HWND wnd, IMessageReceiver* owner)
{
	SolidGraph* g = new SolidGraph(wnd, owner);
	return g;
}
Graph3DBase* CreateWaterfall(HWND wnd, IMessageReceiver* owner) { return new WaterFall(wnd, owner); }
typedef Graph3DBase* ( * MakeFunc)(HWND, IMessageReceiver*);

class GlGraph
{
public:
	GlGraph(HWND wnd, MakeFunc f)
		: m_receiver(wnd), m_impl(new OCGraphWrapper(wnd, &m_receiver, boost::bind(f, wnd, &m_receiver)))
	{
		m_impl->SetCanvas(&m_canvas);
		if (SolidGraph* g = dynamic_cast<SolidGraph*>(m_impl->GetDrawer())) {
			g->SetUseSection(false);
		}
		m_data.push_back(new DllData());
	}

	OCGraphWrapper* GetImpl() const { return m_impl; }

	DllData* GetData(size_t index) { return m_data[index]; }
	DllData* CreateData()
	{
		m_data.push_back(new DllData());
		return m_data.back();
	}
	void DeleteData(size_t index)
	{
		m_data.erase(m_data.begin() + index);
	}
	size_t GetDataCount() const { return m_data.size(); }

private:
	DummyReceiver m_receiver;
	OCNullableCanvas m_canvas;
	OCGraphWrapper* m_impl;

	std::vector<DllData*> m_data;
};

#define GET_GLGRAPH_OR_RETURN(name) if (!handle) { return FALSE; } \
	GlGraph* name = reinterpret_cast<GlGraph*>(handle);

#define GET_GRAPH_OR_RETURN(name) if (!handle) { return FALSE; } \
	GlGraph* name##_ = reinterpret_cast<GlGraph*>(handle); \
	OCGraphWrapper* name = name##_->GetImpl();

#define GET_AXIS_OR_RETURN(name) if (!axisHandle) { return FALSE; } \
	IAxis* name##_ = reinterpret_cast<IAxis*>(axisHandle); \
	Axis* name = dynamic_cast<Axis*>(name##_);

GlGraph* CreateObject(HWND window, MakeFunc f)
{
	g_isLibMode = TRUE;
	return new GlGraph(window, f);
}

PlotGroup* GetPlotGroup(OCGraphWrapper* g, long plotIndex)
{
	if (SolidGraph* graph = dynamic_cast<SolidGraph*>(g->GetDrawer())) {
		if (PlotGroup* plot = graph->GetPlot(plotIndex)) {
			return plot;
		}
	}
	return nullptr;
}

PlotGroup* GetPlotGroup(GlGraph* g, long plotIndex)
{
	return GetPlotGroup(g->GetImpl(), plotIndex);
}

// グラフに対する操作

DLLEXPORT GlHandle WINAPI CreateWaterfall(HWND window)
{
	return CreateObject(window, CreateWaterfall);
}

DLLEXPORT GlHandle WINAPI CreateSolidGraph(HWND window)
{
	return CreateObject(window, CreateSolidGraph);
}

void SetDefaultScale(IAxis* axis)
{
	if (Axis* a = dynamic_cast<Axis*>(axis)) {
		a->SetAutoScale(false);
		a->SetMinMax(0, 1);
	}
}

DLLEXPORT GlHandle WINAPI CreateGraph(HWND window) // CreateSolidGraph
{
	GlGraph* g = CreateObject(window, CreateSolidGraph);
	OCGraphWrapper* wrapper = g->GetImpl();
	SetDefaultScale(wrapper->GetXAxis(0));
	SetDefaultScale(wrapper->GetYAxis(0));
	SetDefaultScale(wrapper->GetZAxis(0));
	wrapper->EnableMarkerControl();
	if (PlotGroup* plot = GetPlotGroup(g, 0)) {
		plot->SelectOnlyMarkerVisible();
	}
	return g;
}

DLLEXPORT BOOL WINAPI DeleteGraph(void* handle)
{
	GlGraph* g = reinterpret_cast<GlGraph*>(handle);
	delete g;

	return TRUE;
}

DLLEXPORT BOOL WINAPI ClearPlots(void* handle)
{
	GET_GRAPH_OR_RETURN(g);
	g->ClearPlots();
	return TRUE;
}
DLLEXPORT BOOL WINAPI UsePreviewImage(void* handle, BOOL use)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetSize(void* handle, long width, long height)
{
	GET_GRAPH_OR_RETURN(g);
	g->SetRect(0, 0, PxToMM(width), PxToMM(height));

	return TRUE;
}
DLLEXPORT BOOL WINAPI Activate(void* handle)
{
	GET_GRAPH_OR_RETURN(g);
	g->SetSelected(true);
	g->DoubleClick(0, 0);

	return TRUE;
}
DLLEXPORT BOOL WINAPI Deactivate(void* handle)
{
	GET_GRAPH_OR_RETURN(g);
	g->SetSelected(false);

	return TRUE;
}
DLLEXPORT BOOL WINAPI CopyToImage(void* handle)
{
	GET_GRAPH_OR_RETURN(g);
	g->CopyImage();

	return TRUE;
}
DLLEXPORT BOOL WINAPI SetBoundaryData(void* handle, double* x, double* y, int length)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI IsBoundaryEnabled(void* handle, BOOL* enable)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetDimension(void* handle, long dimension)
{
	GET_GRAPH_OR_RETURN(g);
	g->SetDimension(dimension);
	return TRUE;
}
DLLEXPORT BOOL WINAPI VisibleMarker(void* handle, BOOL visible)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetOperationMode(void* handle, GraphOperationMode mode)
{
	GET_GRAPH_OR_RETURN(g);
	g->SetMode(mode);
	return TRUE;
}

template<typename T>
BOOL SetProperty(void* handle, long id, T value)
{
	GET_GRAPH_OR_RETURN(g);
	return g->SetProperty(id, value, nullptr);
}
template<class T>
BOOL GetProperty(void* handle, long id, T* result)
{
	GET_GRAPH_OR_RETURN(g);
	LPARAM v;
	bool const ret = g->GetProperty(id, &v);
	*result = static_cast<T>(v);
	return ret;
}

#define DEFINE_PROPERTY_FUNCTION(name, id) \
	DLLEXPORT BOOL WINAPI Set##name(void* handle, BOOL value) \
	{\
		return SetProperty(handle, id, value);\
	}\
	DLLEXPORT BOOL WINAPI Get##name(void* handle, BOOL* value)\
	{\
		return GetProperty(handle, id, value);\
	}

DEFINE_PROPERTY_FUNCTION(ColorBarVisible, PI_3D_COLORBAR_VISIBLE)
DEFINE_PROPERTY_FUNCTION(AxisGuideVisible, PI_3D_AXISGUIDE_VISIBLE)
DEFINE_PROPERTY_FUNCTION(MarkerColor, PI_LINE_COLOR)

DLLEXPORT BOOL WINAPI SetSectionMode(void* handle, SectionKind mode)
{

	return TRUE;
}
DLLEXPORT BOOL WINAPI VisibleSectionButton(void* handle, BOOL visible)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI DoCommand(void* handle, ULONG id)
{
	GET_GRAPH_OR_RETURN(g);
	g->OnCommand(id);
	return TRUE;
}
DLLEXPORT BOOL WINAPI GetState(void* handle, ULONG id, BOOL* enable, BOOL* checked)
{
	GET_GRAPH_OR_RETURN(g);
	g->GetState(id, enable, checked);
	return TRUE;
}
DLLEXPORT BOOL WINAPI EnableFillBackground(void* handle, BOOL enable)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetCursorPosition(void* handle, long cursorX, long cursorY)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI GetCursorPosition(void* handle, long* cursorX, long* cursorY)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetAngle(void* handle, float x, float y, float z)
{
	GET_GRAPH_OR_RETURN(g);
	g->GetDrawer()->Rotate(x, y, z);
	return TRUE;
}
DLLEXPORT BOOL WINAPI GetAngle(void* handle, float* x, float* y, float* z)
{
	GET_GRAPH_OR_RETURN(g);
	*x = static_cast<float>(g->GetDrawer()->GetPitch());
	*y = static_cast<float>(g->GetDrawer()->GetYaw());
	*z = static_cast<float>(g->GetDrawer()->GetRoll());
	return TRUE;
}
DLLEXPORT BOOL WINAPI Translate(void* handle, double x, double y)
{
	GET_GRAPH_OR_RETURN(g);
	g->GetDrawer()->Translate(x, y);
	return TRUE;
}
DLLEXPORT BOOL WINAPI GetPosition(void* handle, double* x, double* y)
{
	GET_GRAPH_OR_RETURN(g);
	g->GetDrawer()->GetPosition(x, y);
	return TRUE;
}
DLLEXPORT BOOL WINAPI Scaling(void* handle, double scale)
{
	GET_GRAPH_OR_RETURN(g);
	g->GetDrawer()->Scaling(scale);
	return TRUE;
}
DLLEXPORT BOOL WINAPI GetScale(void* handle, double* scale)
{
	GET_GRAPH_OR_RETURN(g);
	*scale = g->GetDrawer()->GetScale();
	return TRUE;
}
DLLEXPORT BOOL WINAPI EnableLeftClickSeleft(void* handle, BOOL enable)
{
	return TRUE;
}

DLLEXPORT BOOL WINAPI Capture(void* handle, HDC dc, int width, int height)
{
	GET_GRAPH_OR_RETURN(g);

	g->Capture(dc, width, height);

	return TRUE;
}

DLLEXPORT BOOL WINAPI ClearLockPoints(void* handle)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI AddLockPoints(void* handle, long index)
{
	return TRUE;
}

size_t UniqueCount(real_t const* data, long dataCount)
{
	std::vector<real_t> v(data, data + dataCount);
	std::sort(v.begin(), v.end());
	return std::distance(v.begin(), std::unique(v.begin(), v.end()));
}

// プロットの操作
#ifdef GLGRAPHLIB
DLLEXPORT BOOL WINAPI SetData(void* handle, long index, long dataCount, double const* xData, double const* yData, double const* zData)
#else
DLLEXPORT BOOL WINAPI SetData(void* handle, long index, long dataCount, float const* xData, float const* yData, float const* zData)
#endif
{
	GET_GLGRAPH_OR_RETURN(g);

	DllData* data = g->GetData(index);
	data->SetData(dataCount, xData, yData, zData);
	g->GetImpl()->SetData(index, data);

	return TRUE;
}
DLLEXPORT BOOL WINAPI SetData4D(void* handle, long index, long dataCount, float const* xData, float const* yData, float const* zData, float const* tData)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetZValue(void* handle, long plotIndex, long dataIndex, double value)
{
	GET_GLGRAPH_OR_RETURN(g);

	DllData* data = g->GetData(plotIndex);
	data->SetRawData(2, dataIndex, value);
	if (PlotGroup* plot = GetPlotGroup(g, plotIndex)) {
		plot->ChangeZValue(dataIndex, value);
	}

	return TRUE;
}
DLLEXPORT BOOL WINAPI SetUserData(void* handle, long index, long xLength, long yLength, double const* xData, double const* yData, double const* zData)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetEffectiveData(void* handle, long index, BOOL* effective, long count)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetPinBottom(void* handle, long index, float const* x, float const* y, float const* z, long length)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetDataName(void* handle, long index, TCHAR const* xName, TCHAR const* yName, TCHAR const* zName)
{
	GET_GLGRAPH_OR_RETURN(g);

	DllData* data = g->GetData(index);
	data->SetNames(xName, yName, zName);
	g->GetImpl()->SetData(index, data);

	return TRUE;
}
DLLEXPORT BOOL WINAPI SetDataUnit(void* handle, long index, TCHAR const* xName, TCHAR const* yName, TCHAR const* zName)
{
	GET_GLGRAPH_OR_RETURN(g);

	DllData* data = g->GetData(index);
	data->SetUnits(xName, yName, zName);
	g->GetImpl()->SetData(index, data);

	return TRUE;
}
DLLEXPORT BOOL WINAPI Refresh(void* handle, long index)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI AddPlot(void* handle, long kind, long* index)
{
	GET_GLGRAPH_OR_RETURN(g);

	DllData* data = g->CreateData();
	g->GetImpl()->AddPlotForLib(kind, data);
	*index = g->GetDataCount() - 1;

	return TRUE;
}
DLLEXPORT BOOL WINAPI RemovePlot(void* handle, long index)
{
	GET_GLGRAPH_OR_RETURN(g);
	g->GetImpl()->RemovePlot(index);
	if (g->GetDataCount() != 1) {
		g->DeleteData(index);
	}
	return TRUE;
}

DLLEXPORT BOOL WINAPI SetPlotKind(void* handle, long index, long kind)
{
	GET_GRAPH_OR_RETURN(g);
	g->SetPlotKind(index, kind);
	return TRUE;
}
DLLEXPORT BOOL WINAPI IsPlotColoring(void* handle, long index, BOOL* enable)
{
	GET_GRAPH_OR_RETURN(g);
	if (PlotGroup* plot = GetPlotGroup(g, index)) {
		*enable = plot->IsColoring() ? TRUE : FALSE;
		return TRUE;
	}
	return FALSE;
}
DLLEXPORT BOOL WINAPI EnablePlotColor(void* handle, long index, BOOL enable)
{
	GET_GRAPH_OR_RETURN(g);
	if (PlotGroup* plot = GetPlotGroup(g, index)) {
		plot->EnableColoring(!!enable);
		return TRUE;
	}
	return FALSE;
}
DLLEXPORT BOOL WINAPI SetPlotColor(void* handle, long index, COLORREF color)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI IsPlotVisible(void* handle, long index)
{
	GET_GRAPH_OR_RETURN(g);
	if (PlotGroup* plot = GetPlotGroup(g, index)) {
		return plot->GetVisiblePlotIndex() != -1;
	}

	return TRUE;
}
DLLEXPORT BOOL WINAPI SetPlotVisible(void* handle, long index, long kind, BOOL visible)
{
	GET_GRAPH_OR_RETURN(g);

	g->SetPlotVisible(index, kind, visible);

	return TRUE;
}
DLLEXPORT BOOL WINAPI EnableHideLine(void* handle, long index, BOOL enable)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI GetHideLine(void* handle, long index, BOOL* hideLine)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI EnablePlotPaint(void* handle, long index, BOOL enable)
{
	GET_GRAPH_OR_RETURN(g);

	if (PlotGroup* plot = GetPlotGroup(g, index)) {
		plot->SetPropertyForAllUnit(PI_3D_FILLING, enable, nullptr);
	}

	return TRUE;
}
DLLEXPORT BOOL WINAPI GetPlotPaint(void* handle, long index, BOOL* enable)
{
	GET_GRAPH_OR_RETURN(g);

	if (PlotGroup* plot = GetPlotGroup(g, index)) {
		LPARAM v;
		plot->GetProperty(PI_3D_FILLING, &v);
		*enable = v;
	}

	return TRUE;
}
DLLEXPORT BOOL WINAPI EnableWireFrame(void* handle, long index, BOOL enable)
{
	GET_GRAPH_OR_RETURN(g);

	if (PlotGroup* plot = GetPlotGroup(g, index)) {
		plot->SetPropertyForAllUnit(PI_3D_WIREFRAME_VISIBLE, enable, nullptr);
	}

	return TRUE;
}
DLLEXPORT BOOL WINAPI GetWireFrame(void* handle, long index, BOOL* enable)
{
	GET_GRAPH_OR_RETURN(g);

	if (PlotGroup* plot = GetPlotGroup(g, index)) {
		LPARAM v = 0;
		plot->GetProperty(PI_3D_WIREFRAME_VISIBLE, &v);
		*enable = v;
	}

	return TRUE;
}
DLLEXPORT BOOL WINAPI EnableSliceLine(void* handle, long index, BOOL enable)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI GetSliceLine(void* handle, long index, BOOL* enable)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI EnableClipping(void* handle, long index, BOOL enable)
{
	GET_GRAPH_OR_RETURN(g);

	if (PlotGroup* plot = GetPlotGroup(g, index)) {
		plot->SetProperty(PI_DATA_CLIPPING, enable, nullptr);
	}

	return TRUE;
}
DLLEXPORT BOOL WINAPI GetClipping(void* handle, long index, BOOL* enable)
{
	GET_GRAPH_OR_RETURN(g);

	if (PlotGroup* plot = GetPlotGroup(g, index)) {
		LPARAM v;
		plot->GetProperty(PI_DATA_CLIPPING, &v);
		*enable = v;
	}

	return TRUE;
}
DLLEXPORT BOOL WINAPI GetPinned(void* handle, long index, BOOL* enable)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI EnablePinned(void* handle, long index, BOOL enable)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetUnselectableMarker(void* handle, long index, BOOL enable)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI GetUnselectableMarker(void* handle, long index, BOOL* enable)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetMarkerSelectEnabled(void* handle, long index, BOOL enable)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI GetMarkerSelectEnabled(void* handle, long index, BOOL* enable)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetSelectedMarkerIndex(void* handle, long plotIndex, long markerIndex)
{
	if (plotIndex != 0) { return FALSE; }

	GET_GRAPH_OR_RETURN(g);

	if (SolidGraph* graph = dynamic_cast<SolidGraph*>(g->GetDrawer())) {
		graph->SelectMarker(markerIndex);
	}

	if (PlotGroup* plot = GetPlotGroup(g, plotIndex)) {
		plot->SelectMarker(markerIndex);
	}

	return TRUE;
}
DLLEXPORT BOOL WINAPI GetSelectedMarkerIndex(void* handle, long plotIndex, long* markerIndex)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetSelectedMarkerColor(void* handle, long index, COLORREF color)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI GetSelectedMarkerColor(void* handle, long index, COLORREF* color)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetMarkerSize(void* handle, long index, double size)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI GetMarkerSize(void* handle, long index, double* size)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI EnableMarkerDrag(void* handle, long index, BOOL enable)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetContourType(void* handle, long index, long type)
{
	GET_GLGRAPH_OR_RETURN(g);

	if (SolidGraph* graph = dynamic_cast<SolidGraph*>(g->GetImpl()->GetDrawer())) {
		graph->SetContourAlgorithm(type);
		return TRUE;
	}

	return FALSE;
}

// 軸の操作
DLLEXPORT BOOL WINAPI GetAxis(void* handle, int dimension, int* axis)
{
	GET_GRAPH_OR_RETURN(g);

	switch (dimension)
	{
	case 0:
		*axis = reinterpret_cast<int>(g->GetXAxis(0));
		break;
	case 1:
		*axis = reinterpret_cast<int>(g->GetYAxis(0));
		break;
	case 2:
		*axis = reinterpret_cast<int>(g->GetZAxis(0));
		break;
	default:
		return FALSE;
	}

	return TRUE;
}
DLLEXPORT BOOL WINAPI IsAutoScale(void* axisHandle, BOOL* autoScale)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetAutoScale(void* axisHandle, BOOL autoScale)
{
	return TRUE;
}
DLLEXPORT BOOL WINAPI GetMinimum(void* axisHandle, double* minimum)
{
	GET_AXIS_OR_RETURN(axis);
	*minimum = axis->GetMinimum();
	return TRUE;
}
DLLEXPORT BOOL WINAPI GetMaximum(void* axisHandle, double* maximum)
{
	GET_AXIS_OR_RETURN(axis);
	*maximum = axis->GetMaximum();
	return TRUE;
}
DLLEXPORT BOOL WINAPI GetScaleInterval(void* axisHandle, double* scaleInterval)
{
	GET_AXIS_OR_RETURN(axis);
	*scaleInterval = axis->GetInterval();
	return TRUE;
}
DLLEXPORT BOOL WINAPI GetSecondValue(void* axisHandle, double* secondValue)
{
	GET_AXIS_OR_RETURN(axis);
	*secondValue = axis->GetSecondValue();
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetMinimum(void* axisHandle, double minimum)
{
	GET_AXIS_OR_RETURN(axis);
	LPARAM param = reinterpret_cast<LPARAM>(&minimum);
	axis->SetProperty(PI_SCALE_MIN, param, nullptr);
	if (dynamic_cast<ZAxis*>(axis)) {
		axis->GetOwner()->GetTAxis()->SetProperty(PI_SCALE_MIN, param, nullptr);
	}
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetMaximum(void* axisHandle, double maximum)
{
	GET_AXIS_OR_RETURN(axis);
	LPARAM param = reinterpret_cast<LPARAM>(&maximum);
	axis->SetProperty(PI_SCALE_MAX, param, nullptr);
	if (dynamic_cast<ZAxis*>(axis)) {
		axis->GetOwner()->GetTAxis()->SetProperty(PI_SCALE_MAX, param, nullptr);
	}
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetScaleInterval(void* axisHandle, double scaleInterval)
{
	GET_AXIS_OR_RETURN(axis);
	LPARAM param = reinterpret_cast<LPARAM>(&scaleInterval);
	axis->SetProperty(PI_TICK_INTERVAL, param, nullptr);
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetSecondValue(void* axisHandle, double secondValue)
{
	GET_AXIS_OR_RETURN(axis);
	LPARAM param = reinterpret_cast<LPARAM>(&secondValue);
	axis->SetProperty(PI_SCALE_SECOND, param, nullptr);
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetMinMax(void* axisHandle, double min, double max)
{
	GET_AXIS_OR_RETURN(axis);
	axis->SetMinMax(min, max);
	if (dynamic_cast<ZAxis*>(axis)) {
		axis->GetOwner()->GetTAxis()->SetAutoScale(false);
		axis->GetOwner()->GetTAxis()->SetMinMax(min, max);
	}
	return TRUE;
}
DLLEXPORT BOOL WINAPI GetAxisLabel(void* axisHandle, wchar_t* name, long length)
{
	GET_AXIS_OR_RETURN(axis);
	::wcsncpy_s(name, length, axis->GetLabel(), length - 1);
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetAxisLabel(void* axisHandle, wchar_t const* name)
{
	GET_AXIS_OR_RETURN(axis);
	axis->SetProperty(PI_AXIS_AUTOLABEL, FALSE, nullptr);
	axis->SetLabel(name ? name : L"");
	return TRUE;
}

// イベントハンドラの設定
DLLEXPORT BOOL WINAPI SetMarkerClickEvent(void* handle, OnMarkerClick click)
{
	GET_GRAPH_OR_RETURN(g);
	if (SolidGraph* graph = dynamic_cast<SolidGraph*>(g->GetDrawer())) {
		graph->MarkerClicked.connect(click);
	}
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetMarkerValueChangeEvent(void* handle, OnMarkerValueChange callback)
{
	GET_GRAPH_OR_RETURN(g);
	if (SolidGraph* graph = dynamic_cast<SolidGraph*>(g->GetDrawer())) {
		graph->MarkerValueChanged.connect(callback);
	}
	return TRUE;
}
DLLEXPORT BOOL WINAPI SetRotateEvent(void* handle, OnRotate callback)
{
	GET_GRAPH_OR_RETURN(g);
	g->GetDrawer()->Rotated.connect(callback);	
	return TRUE;
}

#if 1

// O-Chart 専用API ////////////////////////////////////////////////////////////

DLLEXPORT IShape* WINAPI CreateSolidGraphInstance(HWND wnd, IMessageReceiver* owner)
{
	return new OCGraphWrapper(wnd, owner, boost::bind(&CreateSolidGraph, wnd, owner));
}

DLLEXPORT IShape* WINAPI CreateWaterfallInstance(HWND wnd, IMessageReceiver* owner)
{
	return new OCGraphWrapper(wnd, owner, boost::bind(&CreateWaterfall, wnd, owner));
}

DLLEXPORT void WINAPI ReleaseInstance(IShape* ptr)
{
	ptr->Destroy();
}

#endif
