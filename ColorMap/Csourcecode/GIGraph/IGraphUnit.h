#pragma once

#include <boost/signals2.hpp>

struct TexVertex;
class Graph3DBase;
class ISerializer;
class ColorTexture;

class IGraphUnit
{
public:
	virtual ~IGraphUnit() = 0 {}
};

class ISolidUnit : public IGraphUnit
{
public:
	virtual void EnableWireFrame(bool enable) = 0;

	virtual void Modeling(TexVertex* vertexes, size_t length) = 0;
	virtual void ClearModel() = 0;
	virtual void Render(ColorTexture* texture, TexVertex const* vertexes, size_t length, bool clipping) const = 0;
};

class IWireframeDrawable
{
public:
	virtual void RenderWireframeXLines() = 0;
	virtual void RenderWireframeZLines() = 0;
	virtual void SetWireframeColor(COLORREF color) = 0;
};

class ICursorDrawable
{
public:
	virtual void GetIndexLine(double x, double z, int *index, int *line) = 0;
	virtual void ModelingCursorLines(int index, int line) = 0;
	virtual void RenderCursorLines() = 0;
};

class IWaterFallUnit : public IGraphUnit, public IWireframeDrawable, public ICursorDrawable
{
public:
	virtual void InitializeData(int xCount, int zLines, Graph3DBase *base) = 0;

	virtual int GetXLineCount() const = 0;
	virtual int GetZLineCount() const = 0;

	virtual void ResetLines() = 0;
	virtual BOOL PushLine(float z, float* x, float* y) = 0;
	virtual void PushLines(float *z, float** x, float** y, int lineCount) = 0;
	virtual void SetCompressInterval(int compressInterval) = 0;
	virtual int GetCompressInterval() const = 0;
	virtual void GetCursorLine(int* index, int* line) const = 0;
	virtual int GetLineCount() const = 0;
	virtual void ResetCursor() = 0;

	virtual void Render() const = 0;

#if 1
	virtual void Serialize(ISerializer* ar) = 0;
#endif

// event
	boost::signals2::signal<void(std::vector<OCPoint> const& xLine, std::vector<OCPoint> const& yLine)> CursorChanged;
	boost::signals2::signal<void(OCPoint3D const& point)> CursorValueChanged;
};
