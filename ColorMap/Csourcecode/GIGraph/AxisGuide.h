#pragma once

class GuideArrow;

class AxisGuide
{
public:
	AxisGuide();
	virtual ~AxisGuide();

	void Modeling();
	void Render(double pitch, double yaw, double roll);

private:
	GuideArrow *xArrow;
	GuideArrow *yArrow;
	GuideArrow *zArrow;
};
