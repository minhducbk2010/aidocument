#include "stdafx.h"
#include "SIUnit.h"
#include <float.h>

const wchar_t SIUnit::LargeUnit[] = { L'', L'k', L'M', L'G' };
const wchar_t SIUnit::SmallUnit[] = { L'', L'm', L'u', L'n', L'p' };
const int SIUnit::Digit = 3;

void SIUnit::Convert(double value, wchar_t *si, size_t count)
{
	int rank = 0;
	bool minus = value < 0.0F;
	double absv = std::abs(value);

	if (absv >= 1000.0F)
	{
		double tuv = TakeUp(absv, &rank);
		wchar_t s[12];
		ConvertToString(RoundUp(tuv, Digit), 3, s, _countof(s));
		if (minus)
		{
			_snwprintf_s(si, count, _TRUNCATE, L"-%s%c", s, LargeUnit[rank]);
		}
		else
		{
			_snwprintf_s(si, count, _TRUNCATE, L"%s%c", s, LargeUnit[rank]);
		}
	}
	else if (absv < 1.0F && value != 0.0F)
	{
		double tuv = SpreadOut(absv, &rank);
		wchar_t s[12];
		ConvertToString(RoundUp(tuv, Digit), 3, s, _countof(s));
		if (minus)
		{
			_snwprintf_s(si, count, _TRUNCATE, L"-%s%c", s, SmallUnit[rank]);
		}
		else
		{
			_snwprintf_s(si, count, _TRUNCATE, L"%s%c", s, SmallUnit[rank]);
		}
	}
	else
	{
		wchar_t s[12];
		ConvertToString(RoundUp(value, Digit), Digit, si, _countof(s));
	}
}

void SIUnit::ConvertToString(double value, int digit, wchar_t *si, size_t count)
{
	wchar_t fmt[8];
	_snwprintf_s(fmt, _countof(fmt), _TRUNCATE, L"%%.%df", digit);
	_snwprintf_s(si, count, _TRUNCATE, fmt, value);

	size_t len = wcsnlen(si, count);
	for (size_t i = len - 1; i > 0U; --i)
	{
		if (si[i] == L'.')
		{
			si[i] = L'\0';
			break;
		}
		else if (si[i] == L'0')
		{
			si[i] = L'\0';
		}
		else
		{
			break;
		}
	}
}
double SIUnit::TakeUp(double value, int *rank)
{
	*rank = 0;

	if (_finite((double)value) == 0)
	{
		return value;
	}

	do
	{
		value /= 1000.0F;
		++(*rank);
	}
	while (value >= 1000.0F);

	*rank = std::min<int>(*rank, 3);

	return value;
}
double SIUnit::SpreadOut(double value, int *rank)
{
	*rank = 0;

	do
	{
		value *= 1000.0F;
		++(*rank);
	}
	while (value < 1.0F);

	*rank = std::min<int>(*rank, 4);

	return value;
}
double SIUnit::RoundUp(double value, int digit)
{
	double coef = pow(10.0F, digit);
	return value > 0.0F ? ceil(value * coef) / coef :
		floor(value * coef) / coef;
}
