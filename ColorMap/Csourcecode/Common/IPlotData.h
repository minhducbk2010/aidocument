/*!	@file
	@date	2008-03-05
	@author	wada

	@brief	プロットが使用するデータを保持するクラスのインターフェイス
*/
#pragma once

#include <boost/shared_ptr.hpp>

#ifdef GLGRAPHLIB
typedef double real_t;
#else
typedef float real_t;
#endif

class IFilter;
class ISerializer;

enum ExclusionScale { // FindMaximum, FindMininumで使用する
	ExcludeNone = 0,
	ExcludeX = 1 << 1,
	ExcludeY = 1 << 2,
	ExcludeZ = 1 << 3,
};

class IPlotData
{
protected:
	// このインターフェイスを削除するには Destroy メンバ関数を使用します
	virtual ~IPlotData() = 0 {}

public:
	virtual void Destroy() = 0;

	virtual long GetFileId(long dimension) = 0;
	virtual long GetItemId(long dimension) = 0;
	virtual void ChangeFile(long fileId) = 0;
	virtual void SetLink(long dimension, long fileId) = 0;
	virtual void SetLink(long dimension, long fileId, long itemId) = 0;
	virtual void CopyFrom(size_t fileId, IPlotData* src) = 0;

	virtual void UpdateItemIndex() = 0;
	virtual bool LinkDataSet() = 0;
	virtual void ClearItems() = 0;

	virtual void SetFilter(boost::shared_ptr<IFilter> const& filter) = 0;
	virtual IFilter* GetFilter() const = 0;

	virtual void SetDataIndex(size_t const* index, size_t count) = 0;
	virtual size_t GetAbsoluteIndex(size_t relativeIndex) const = 0;
	virtual size_t GetRelativeIndex(size_t absoluteIndex) const = 0;
	//!@{データのソート
	virtual void SortForIndex() = 0;
	virtual void SortForX() = 0;
	//@}

	virtual wchar_t const* GetItemName(long dimension) const = 0;
	virtual wchar_t const* GetUnitName(long dimension) const = 0;

	// 名前変えた方がいいかも
	// いまのところ積み上げグラフに使うために存在している
	virtual void AddReferenceData(IPlotData* data) = 0;
	virtual void ClearReferenceData() = 0;
	virtual bool UseReferenceData() const = 0;
	virtual size_t GetReferenceDataCount() const = 0;
	virtual IPlotData* GetReferenceData(size_t index) const = 0;

	virtual int GetScaleType(long dimension) const = 0;
	virtual bool UseString(long dimension) const = 0;

	virtual size_t GetRealDataCount() const = 0;
	virtual real_t GetMinimum(long dimension) const = 0;
	virtual real_t GetMaximum(long dimension) const = 0;
	virtual real_t GetRealMinimum(long dimension) const = 0;
	virtual real_t GetRealMaximum(long dimension) const = 0;

	virtual real_t GetRawData(int dimension, size_t index) const = 0;
	virtual real_t GetRealData(int dimension, size_t index) const = 0;
	virtual real_t GetData(int dimension, size_t index) const = 0;
	virtual TCHAR const* GetStrData(int dimension, size_t index) const = 0;
	virtual TCHAR const* GetStrDataFromAllData(int dimension, size_t index) const = 0; //!< フィルタリングされていない元データから取得する
	virtual void SetRawData(long dimension, size_t index, real_t const value) = 0;
	virtual real_t ConvertValue(long dimension, real_t linear) const = 0;

	virtual size_t GetDataCount() const = 0;
	virtual size_t FindMaximumIndex(long dimension, long exclusionScale = 0, bool accumulative = false) const = 0;
	virtual size_t FindMinimumIndex(long dimension, long exclusionScale = 0, bool accumulative = false) const = 0;
	virtual void FindMinMaxIndex(size_t dimension, size_t* min, size_t* max, long exclusionScale = 0, bool accumulative = false) const = 0;

	virtual void Serialize(ISerializer* ar) = 0;

	virtual IPlotData* Clone() /*const*/ = 0;
};