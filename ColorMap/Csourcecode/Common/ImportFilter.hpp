#pragma once

#import <msxml3.dll>
#include <ctime>
#include <string>
#include "../Library/PluginUtility/pluginutil.h"
#include "ImporterIdDefinition.h"
#include "version.h"
#include "IMessageReceiver.h"
#pragma comment(lib, "version.lib")

class ImportFilter
{
public:
	ImportFilter()
		: m_version(1, 0, 0, 0)
	{
		std::time_t timer;
		std::time(&timer);

		TCHAR date[256] = {};
		std::tm t;
		localtime_s(&t, &timer);
		::_stprintf_s(date, _countof(date) - 1, _T("%d/%02d/%02d"), t.tm_year + 1900, t.tm_mon + 1, t.tm_mday);
		m_time = date;
	}
	virtual ~ImportFilter() {}

	bool Load(std::wstring const& fileName)
	{
		MSXML2::IXMLDOMDocumentPtr doc;
		doc.CreateInstance(__uuidof(MSXML2::DOMDocument30));

		if (!doc->load(fileName.c_str())) { return false; }

		return LoadInfo(doc);
	}

	/*! このフィルタを利用できるインポートプラグインの種別
		値はImporterIdDefinition.hに定義されている
	*/
	int GetImporterId() const { return m_importerId; }
	std::wstring GetCreateTime() const { return m_time; }
	Version GetCreateVersion() const { return m_version; }
	std::wstring GetImporterExtention(IMessageReceiver* owner) const 
	{
		switch (m_importerId) {
		case CSV_IMPORTER_ID:
			return ::GetResourceString(owner, "ID_SOURCE_TEXT_ALLCSVTXT");
		case EXCEL_IMPORTER_ID:
			return ::GetResourceString(owner, "ID_SOURCE_TEXT_ALLXLSX");
		case MESH_IMPORTER_ID:
			return ::GetResourceString(owner, "ID_SOURCE_TEXT_ALLCSVXLS");
		default:
			return ::GetResourceString(owner, "ID_SOURCE_TEXT_MISSING");
		}
	}

	std::wstring GetImporterString(IMessageReceiver* owner) const {
		switch (m_importerId) {
		case CSV_IMPORTER_ID:
			return ::GetResourceString(owner, "ID_SOURCE_TEXT_ASCII");
		case EXCEL_IMPORTER_ID:
			return ::GetResourceString(owner, "ID_SOURCE_TEXT_EXCEL");
		case MESH_IMPORTER_ID:
			return ::GetResourceString(owner, "ID_SOURCE_TEXT_MESHFILE");
		default:
			return ::GetResourceString(owner, "ID_SOURCE_TEXT_MISSING");
		}
	}

protected:
	void SaveInfo(MSXML2::IXMLDOMDocumentPtr doc) const
	{
		MSXML2::IXMLDOMNodePtr root = doc->lastChild;
		CXmlNode xml(doc, root);
		CXmlNode info(doc, xml.AddNode(_T("Information")));
		info.SetAttribute(_T("Type"), m_importerId);
		info.SetAttribute(_T("CreateDate"), m_time.c_str());

		CXmlNode ver(doc, xml.AddNode(_T("Version")));
		ver.SetAttribute(_T("Major"), m_version.major);
		ver.SetAttribute(_T("Minor"), m_version.minor);
		ver.SetAttribute(_T("Release"), m_version.release);
		ver.SetAttribute(_T("Build"), m_version.build);
	}

	bool LoadInfo(MSXML2::IXMLDOMDocumentPtr doc)
	{
		MSXML2::IXMLDOMNodePtr root = doc->lastChild;
		CXmlNode xml(doc, root);

		CXmlNode info(doc, xml.FindChild(_T("Information")));
		if (info.GetNode()) {
			m_importerId = info.GetAttribute(_T("Type"), -1);
			CString time = info.GetAttribute(_T("CreateDate"));
			m_time = time;
		}
		else { return false; }

		CXmlNode ver(doc, xml.FindChild(_T("Version")));
		if (ver.GetNode()) {
			m_version.major = ver.GetAttribute(_T("Major"));
			m_version.minor = ver.GetAttribute(_T("Minor"));
			m_version.release = ver.GetAttribute(_T("Release"));
			m_version.build = ver.GetAttribute(_T("Build"));
		}
		else { return false; }

		return true;
	}

protected:
	int m_importerId;
	Version m_version;
	std::wstring m_time;
};
