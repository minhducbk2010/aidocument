#pragma once

#ifdef _DEBUG
int const YAXIS_LIMIT = 100;
#else
int const YAXIS_LIMIT = 20; //!< プロット追加やグラフウィザードで指定できるY軸の数
#endif