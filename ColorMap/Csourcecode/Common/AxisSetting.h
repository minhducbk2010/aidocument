#pragma once

#include <algorithm>
#include <string>
#include <vector>

/*! 軸目盛りラベルの設定
	@invariant start >=0
	@invariant interval >=1
*/
struct AxisTickLabelSetting
{
	double minimum; //!< 最小値
	double maximum; //!< 最大値
	BOOL constant;	//!< コンスタント?

	//@{補助設定
	BOOL step;	//!< ステップ表示
	int start;	//!< 開始
	int interval; //!< 間隔
	// @}

	struct label_t {
		BOOL first;
		TCHAR second[128];
	};

	label_t labels[102];
	long labelCount;

	AxisTickLabelSetting& operator=(AxisTickLabelSetting const& src)
	{
		minimum = src.minimum;
		maximum = src.maximum;
		constant = src.constant;
		step = src.step;
		start = src.start;
		interval = src.interval;
		labelCount = src.labelCount;
		for (long i = 0; i < labelCount; ++i) {
			labels[i].first = src.labels[i].first;
			::_tcscpy_s(labels[i].second, src.labels[i].second);
		}
		return *this;
	}
};