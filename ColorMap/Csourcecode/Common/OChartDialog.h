#pragma once

#include <string>
#include <afxpriv.h>
#include "Message.h"
#include "ResourceHelper.h"

// 基底クラスはBCGの有無で切り替える
#ifdef _BCGCBPRO_VERSION_
#	define BASE_DIALOG CBCGPDialog
#else
#	define BASE_DIALOG CDialog
#endif

class COChartDialog : public BASE_DIALOG
{
public:
	COChartDialog() {}
	COChartDialog(UINT id) : BASE_DIALOG(id) {}
	COChartDialog(UINT id, CWnd* parent) : BASE_DIALOG(id, parent) {}

	int DialogToPixelX(int x) const
	{
		CRect r(x, 0, 0, 0);
		MapDialogRect(&r);
		return r.left;
	}

	int DialogToPixelY(int y) const
	{
		CRect r(0, y, 0, 0);
		MapDialogRect(&r);
		return r.top;
	}

	virtual INT_PTR DoModal()
	{
	#define DELETE_EXCEPTION(e) do { if(e) { e->Delete(); } } while(0)

		// can be constructed with a resource template or InitModalIndirect
		ASSERT(m_lpszTemplateName != NULL || m_hDialogTemplate != NULL ||
			m_lpDialogTemplate != NULL);

		// load resource as necessary
		LPCDLGTEMPLATE lpDialogTemplate = m_lpDialogTemplate;
		HGLOBAL hDialogTemplate = m_hDialogTemplate;
		HINSTANCE hInst = AfxGetResourceHandle();
		if (m_lpszTemplateName != NULL)
		{
			hInst = AfxFindResourceHandle(m_lpszTemplateName, RT_DIALOG);
			HRSRC hResource = ::FindResource(hInst, m_lpszTemplateName, RT_DIALOG);
			hDialogTemplate = LoadResource(hInst, hResource);
		}
		if (hDialogTemplate != NULL)
			lpDialogTemplate = (LPCDLGTEMPLATE)LockResource(hDialogTemplate);

		// return -1 in case of failure to load the dialog template resource
		if (lpDialogTemplate == NULL)
			return -1;

		// disable parent (before creating dialog)
		HWND hWndParent = PreModal();
		AfxUnhookWindowCreate();
		BOOL bEnableParent = FALSE;
	#ifndef _AFX_NO_OLE_SUPPORT
		CWnd* pMainWnd = NULL;
		BOOL bEnableMainWnd = FALSE;
	#endif
		if (hWndParent && hWndParent != ::GetDesktopWindow() && ::IsWindowEnabled(hWndParent))
		{
			::EnableWindow(hWndParent, FALSE);
			bEnableParent = TRUE;
	#ifndef _AFX_NO_OLE_SUPPORT
			pMainWnd = AfxGetMainWnd();
			if (pMainWnd && pMainWnd->IsFrameWnd() && pMainWnd->IsWindowEnabled())
			{
				//
				// We are hosted by non-MFC container
				// 
				pMainWnd->EnableWindow(FALSE);
				bEnableMainWnd = TRUE;
			}
	#endif
		}

		TRY
		{
	// O-Chart
			std::wstring font;
			int fontSize;
			GetFontFromResource(hWndParent, font, fontSize);

			CDialogTemplate temp(lpDialogTemplate);
			temp.SetFont(font.c_str(), fontSize);
			lpDialogTemplate = (LPCDLGTEMPLATE)temp.Detach();
	// -

			// create modeless dialog
			AfxHookWindowCreate(this);
			if (CreateDlgIndirect(lpDialogTemplate,
							CWnd::FromHandle(hWndParent), hInst))
			{
				if (m_nFlags & WF_CONTINUEMODAL)
				{
					// enter modal loop
					DWORD dwFlags = MLF_SHOWONIDLE;
					if (GetStyle() & DS_NOIDLEMSG)
						dwFlags |= MLF_NOIDLEMSG;
					VERIFY(RunModalLoop(dwFlags) == m_nModalResult);
				}

				// hide the window before enabling the parent, etc.
				if (m_hWnd != NULL)
					SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW|
						SWP_NOSIZE|SWP_NOMOVE|SWP_NOACTIVATE|SWP_NOZORDER);
			}
		}
		CATCH_ALL(e)
		{
			DELETE_EXCEPTION(e);
			m_nModalResult = -1;
		}
		END_CATCH_ALL

	#ifndef _AFX_NO_OLE_SUPPORT
		if (bEnableMainWnd)
			pMainWnd->EnableWindow(TRUE);
	#endif
		if (bEnableParent)
			::EnableWindow(hWndParent, TRUE);
		if (hWndParent != NULL && ::GetActiveWindow() == m_hWnd)
			::SetActiveWindow(hWndParent);

		// destroy modal window
		DestroyWindow();
		PostModal();

		// unlock/free resources as necessary
		if (m_lpszTemplateName != NULL || m_hDialogTemplate != NULL)
			UnlockResource(hDialogTemplate);
		if (m_lpszTemplateName != NULL)
			FreeResource(hDialogTemplate);

		return m_nModalResult;
	}

	virtual BOOL Create(LPCTSTR lpszTemplateName, CWnd* pParentWnd = NULL)
	{
		ASSERT(IS_INTRESOURCE(lpszTemplateName) ||
		AfxIsValidString(lpszTemplateName));

		m_lpszTemplateName = lpszTemplateName;  // used for help
		if (IS_INTRESOURCE(m_lpszTemplateName) && m_nIDHelp == 0)
			m_nIDHelp = LOWORD((DWORD_PTR)m_lpszTemplateName);

		//#ifdef _DEBUG
		// if (!_AfxCheckDialogTemplate(lpszTemplateName, FALSE))
		// {
		// ASSERT(FALSE);          // invalid dialog template name
		// PostNcDestroy();        // cleanup if Create fails too soon
		// return FALSE;
		// }
		//#endif //_DEBUG

		HINSTANCE hInst = AfxFindResourceHandle(lpszTemplateName, RT_DIALOG);
		std::wstring font;
		int fontSize;
		GetFontFromResource(pParentWnd->GetSafeHwnd(), font, fontSize);

		CDialogTemplate dlgTemp;
		dlgTemp.Load(lpszTemplateName);
		dlgTemp.SetFont(font.c_str(), fontSize);
		HGLOBAL hTemplate = dlgTemp.Detach();

		BOOL bResult = CreateIndirect(hTemplate, pParentWnd, hInst);
		FreeResource(hTemplate);

		return bResult; 
	}

	virtual BOOL Create(UINT nIDTemplate, CWnd* pParentWnd = NULL)
	{
		return Create(ATL_MAKEINTRESOURCE(nIDTemplate), pParentWnd);
	}

	virtual BOOL OnInitDialog()
	{
		BOOL ret = BASE_DIALOG::OnInitDialog();
#ifdef MAINPROJECT
		LocalizeDialogResource(m_hWnd);
#else
		LocalizeDialogResourcePlugin(m_hWnd, m_owner);
#endif
		return ret;
	}

protected:
	virtual void GetFontFromResource(HWND hWndParent, std::wstring& font, int& fontSize)
	{
#ifdef MAINPROJECT
		font = GetResourceString("ID_SOURCE_TEXT_MS_UI_GOTHIC");
		fontSize = ::_ttoi(GetResourceString("ID_SOURCE_TEXT_DLG_FONTSIZE"));
#else
		m_owner = reinterpret_cast<IMessageReceiver*>(::SendMessage(hWndParent, PI_MSG_GET_DOCUMENT_POINTER, 0, 0));
		while (!m_owner) {
			HWND wnd = ::GetParent(hWndParent);
			if (!wnd) { break; }
			m_owner = reinterpret_cast<IMessageReceiver*>(::SendMessage(wnd, PI_MSG_GET_DOCUMENT_POINTER, 0, 0));
			hWndParent = wnd;
		}

		font = GetResourceString(m_owner, "ID_SOURCE_TEXT_MS_UI_GOTHIC");
		fontSize = ::_ttoi(GetResourceString(m_owner, "ID_SOURCE_TEXT_DLG_FONTSIZE"));
#endif
	}

protected:
#ifndef MAINPROJECT
	IMessageReceiver* m_owner;
#endif
};

// 英語版で通常のフォントサイズではダイアログが大きくなりすぎて、
// 最小スペック解像度1024x768の120DPIで縦の長さが足りないダイアログのためのダイアログクラス
class COChartMiniSizeFontDialog : public COChartDialog
{
public:
	COChartMiniSizeFontDialog(UINT id) : COChartDialog(id) {}
	COChartMiniSizeFontDialog(UINT id, CWnd* parent) : COChartDialog(id, parent) {}

protected:
	virtual void GetFontFromResource(HWND hWndParent, std::wstring& font, int& fontSize)
	{
#ifdef MAINPROJECT
		font = GetResourceString("ID_SOURCE_TEXT_MS_UI_GOTHIC");
		fontSize = ::_ttoi(GetResourceString("ID_SOURCE_TEXT_DLG_MINI_FONTSIZE"));
#else
		m_owner = reinterpret_cast<IMessageReceiver*>(::SendMessage(hWndParent, PI_MSG_GET_DOCUMENT_POINTER, 0, 0));
		while (!m_owner) {
			HWND wnd = ::GetParent(hWndParent);
			if (!wnd) { break; }
			m_owner = reinterpret_cast<IMessageReceiver*>(::SendMessage(wnd, PI_MSG_GET_DOCUMENT_POINTER, 0, 0));
			hWndParent = wnd;
		}

		font = GetResourceString(m_owner, "ID_SOURCE_TEXT_MS_UI_GOTHIC");
		fontSize = ::_ttoi(GetResourceString(m_owner, "ID_SOURCE_TEXT_DLG_MINI_FONTSIZE"));
#endif
	}
};