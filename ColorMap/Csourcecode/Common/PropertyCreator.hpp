/*!	@file
	@author	wada
	@brief	プロパティ構築のためのクラス
*/
#pragma once

#include <vector>
#include "utility.h"
#include "property.h"
#include "type_safe_enum.h"

class IShape;

struct OwnerDraw
{
	typedef void (__stdcall * OnDrawComboValue)(HDC dc, int index, RECT const& rect, UINT state);
	OnDrawComboValue OnDraw;
	int count;
	int index;
};

class IPropertyGroup;
//! プロパティを作成するクラスのインターフェイス
class IPropertyCreator
{
public:
	virtual IPropertyGroup* CreateGroup(wchar_t const* name) = 0;
	virtual IPropertyGroup* CreateGroup(wchar_t const* name, int id, BOOL checked, bool readOnly = false) = 0;
};

struct IPropertyComposite
{
	virtual IPropertyGroup* AddGroup(wchar_t const* name, int id = -1, bool checked = false, bool readOnly = false) = 0;
};

//! プロパティグループ
class IPropertyGroup : public IPropertyComposite
{
public:
	virtual void AddBoolProperty(wchar_t const* name, int id, bool value, bool readOnly = false) = 0;
	virtual void AddProperty(wchar_t const* name, int id, wchar_t const* value, bool readOnly = false) = 0;
	virtual void AddProperty(wchar_t const* name, int id, int value, bool readOnly = false) = 0;
	virtual void AddProperty(wchar_t const* name, int id, double value, bool readOnly = false) = 0;
	virtual void AddSelectorProperty(wchar_t const* name, int id, double value, int count, ...) = 0;
	virtual void AddSelectorProperty(wchar_t const* name, int id, int value, int count, ...) = 0;
	virtual void AddSelectorPropertyR(wchar_t const* name, int id, bool readOnly, double value, int count, ...) = 0;
	virtual void AddSelectorPropertyR(wchar_t const* name, int id, bool readOnly, int value, int count, ...) = 0;
	virtual void AddTransparentProperty(wchar_t const* name, int id, int value, bool readOnly = false) = 0;
	virtual void AddSpinProperty(wchar_t const* name, int id, int value, int min, int max, bool readOnly = false) = 0;
	virtual void AddColorProperty(wchar_t const* name, int id, COLORREF color, bool readOnly = false) = 0;
	//virtual void AddDialogProperty(wchar_t const* name, int id, int value, bool readOnly = false) = 0;
	//virtual void AddDialogProperty(wchar_t const* name, int id, wchar_t const* value, bool readOnly = false) = 0;
	virtual void AddCustomDialogProperty(wchar_t const* name, int id, wchar_t const* value, LPARAM param, bool readOnly = false) = 0;
	virtual void AddFontProperty(wchar_t const* name, int id, wchar_t const* value, bool readOnly = false) = 0;
	virtual void AddFontProperty(wchar_t const* name, int id, LOGFONT const& value, bool readOnly = false) = 0;
	virtual void AddDrawDialogProperty(wchar_t const* name, int id, int stStyle, int edStyle, bool readOnly = false) = 0;
	virtual void AddArrowStyleProperty(wchar_t const* name, int id, int style, bool readOnly = false) = 0;
	virtual void AddDrawComboProperty(wchar_t const* name, int id, int value, int count, OwnerDraw::OnDrawComboValue func) = 0;
	virtual void AddArrayProperty(wchar_t const* name, int id, int index, int count, ...) = 0;
	virtual void AddArrayProperty(wchar_t const* name, int id, int index, bool readOnly, int count, ...) = 0;
	virtual void AddArrayProperty(wchar_t const* name, int id, int index, std::vector<std::wstring> const& elems) = 0;
	virtual void AddArrayProperty(wchar_t const* name, int id, bool readOnly, int index, std::vector<std::wstring> const& elems) = 0;
	virtual void AddArrayProperty(wchar_t const* name, int id, int index, ValueList* arr) = 0;
};

//=================================================================================================

//! プロパティグループ
class OCPropertyGroup : public IPropertyGroup
{
	friend class OCProperty;
public:
	/*!	@brief Constructor
		@param[in]	name	グループ名
	*/
	OCPropertyGroup(wchar_t const* name) : m_current(NULL)
	{
		m_node = MakeProperty(name, -1 ,-1);
	}

	wchar_t const* GetName() const { return m_node->name; }
	/*!	@brief ON/OFFプロパティを追加します
		@param[in]	name	プロパティ名称
		@param[in]	id		プロパティの固有ID
		@param[in]	value	プロパティの現在の状態
	*/
	void AddBoolProperty(wchar_t const* name, int id, bool value, bool readOnly = false)
	{
		if (m_current) {
			m_current = (m_current->next = MakeBooleanProperty(name, id, value, readOnly));
		}
		else {
			m_current = (m_node->child = MakeBooleanProperty(name, id, value, readOnly));
		}
	}

	void AddProperty(wchar_t const* name, int id, wchar_t const* value, bool readOnly = false)
	{
		if (m_current) {
			m_current = (m_current->next = MakeProperty(name, id, value, readOnly));
		}
		else {
			m_current = (m_node->child = MakeProperty(name, id, value, readOnly));
		}
	}

	void AddProperty(wchar_t const* name, int id, int value, bool readOnly = false)
	{
		if (m_current) {
			m_current = (m_current->next = MakeProperty(name, id, value, readOnly));
		}
		else {
			m_current = (m_node->child = MakeProperty(name, id, value, readOnly));
		}
	}

	void AddProperty(wchar_t const* name, int id, double value, bool readOnly = false)
	{
		if (m_current) {
			m_current = (m_current->next = MakeProperty(name, id, value, readOnly));
		}
		else {
			m_current = (m_node->child = MakeProperty(name, id, value, readOnly));
		}
	}

	void AddSelectorProperty(wchar_t const* name, int id, double value, int count, ...)
	{
		va_list marker;
		va_start(marker, count);
		ValueList* beg = 0, *arr = 0;
		short index = -1;
		for (int i = 0; i < count; ++i) {
			double v = va_arg(marker, double);
			wchar_t buf[36] = {};
			::_stprintf_s(buf, _countof(buf), _T("%g"), v);
			if (beg == 0) {
				beg = arr = new ValueList(buf);
			}
			else {
				arr = (arr->next = new ValueList(buf));
			}
			if (IsZero(value - v)) {
				index = (short)i;
			}
		}
		va_end(marker);

		if (m_current) {
			m_current = (m_current->next = CreateProperty(name, id));
		}
		else {
			m_current = (m_node->child = CreateProperty(name, id));
		}
		m_current->type = PROP_COMBO_DBL;
		m_current->aryIndex = index;
		m_current->u.aryValue = beg;
		m_current->reserve = value;
	}
	void AddSelectorProperty(wchar_t const* name, int id, int value, int count, ...)
	{
		va_list marker;
		va_start(marker, count);
		ValueList* beg = 0, *arr = 0;
		short index = -1;
		for (int i = 0; i < count; ++i) {
			int v = va_arg(marker, int);
			wchar_t buf[36] = {};
			::_stprintf_s(buf, _countof(buf), _T("%d"), v);
			if (beg == 0) {
				beg = arr = new ValueList(buf);
			}
			else {
				arr = (arr->next = new ValueList(buf));
			}
			if (!(value - v)) {
				index = (short)i;
			}
		}
		va_end(marker);

		if (m_current) {
			m_current = (m_current->next = CreateProperty(name, id));
		}
		else {
			m_current = (m_node->child = CreateProperty(name, id));
		}
		m_current->type = PROP_COMBO_INT;
		m_current->aryIndex = index;
		m_current->u.aryValue = beg;
		m_current->reserve = static_cast<double>(value);	// 手入力時の値を保存(できればint保存したい...)
	}

	void AddSelectorPropertyR(wchar_t const* name, int id, bool readOnly, double value, int count, ...)
	{
		va_list marker;
		va_start(marker, count);
		ValueList* beg = 0, *arr = 0;
		short index = -1;
		for (int i = 0; i < count; ++i) {
			double v = va_arg(marker, double);
			wchar_t buf[36] = {};
			::_stprintf_s(buf, _countof(buf), _T("%g"), v);
			if (beg == 0) {
				beg = arr = new ValueList(buf);
			}
			else {
				arr = (arr->next = new ValueList(buf));
			}
			if (IsZero(value - v)) {
				index = (short)i;
			}
		}
		va_end(marker);

		if (m_current) {
			m_current = (m_current->next = CreateProperty(name, id, readOnly));
		}
		else {
			m_current = (m_node->child = CreateProperty(name, id, readOnly));
		}
		m_current->type = PROP_COMBO_DBL;
		m_current->aryIndex = index;
		m_current->u.aryValue = beg;
		m_current->reserve = value;
	}
	void AddSelectorPropertyR(wchar_t const* name, int id, bool readOnly, int value, int count, ...)
	{
		va_list marker;
		va_start(marker, count);
		ValueList* beg = 0, *arr = 0;
		short index = -1;
		for (int i = 0; i < count; ++i) {
			int v = va_arg(marker, int);
			wchar_t buf[36] = {};
			::_stprintf_s(buf, _countof(buf), _T("%d"), v);
			if (beg == 0) {
				beg = arr = new ValueList(buf);
			}
			else {
				arr = (arr->next = new ValueList(buf));
			}
			if (!(value - v)) {
				index = (short)i;
			}
		}
		va_end(marker);

		if (m_current) {
			m_current = (m_current->next = CreateProperty(name, id, readOnly));
		}
		else {
			m_current = (m_node->child = CreateProperty(name, id, readOnly));
		}
		m_current->type = PROP_COMBO_INT;
		m_current->aryIndex = index;
		m_current->u.aryValue = beg;
		m_current->reserve = static_cast<double>(value);	// 手入力時の値を保存(できればint保存したい...)
	}

	//! 透明度
	void AddTransparentProperty(wchar_t const* name, int id, int value, bool readOnly = false)
	{
		if (m_current) {
			m_current = (m_current->next = CreateProperty(name, id, readOnly));
		}
		else {
			m_current = (m_node->child = CreateProperty(name, id, readOnly));
		}
		m_current->type = PROP_TRANSPARENT;
		m_current->u.nValue = value;
	}

	//! スピンボタン
	void AddSpinProperty(wchar_t const* name, int id, int value, int min, int max, bool readOnly = false)
	{
		if (m_current) {
			m_current = (m_current->next = CreateProperty(name, id));
		}
		else {
			m_current = (m_node->child = CreateProperty(name, id));
		}
		m_current->type = PROP_SPIN;
		m_current->u.nValue = value;
		m_current->begin = min;
		m_current->end = max;
		m_current->readOnly = readOnly;
	}

	void AddColorProperty(wchar_t const* name, int id, COLORREF color, bool readOnly = false)
	{
		if (m_current) {
			m_current = (m_current->next = MakeColorProperty(name, id, color, readOnly));
		}
		else {
			m_current = (m_node->child = MakeColorProperty(name, id, color, readOnly));
		}
	}

	///*! スライダーで値を変更するプロパティを作成します
	//	値は整数(PROP_INT)になります
	//	@param	begin	値の範囲(始点)
	//	@param	end		値の範囲(終点)
	//	スライダーは begin-end 間で値を変更することができます
	//*/
	//void AddSliderProperty(wchar_t const* name, int id, int value, int begin, int end)
	//{
	//	if (m_current) {
	//		m_current = (m_current->next = MakeSliderProperty(name, id, value, begin, end));
	//	}
	//	else {
	//		m_current = (m_node->child = MakeSliderProperty(name, id, value, begin, end));
	//	}
	//}

	/*!	カスタムプロパティを作成します
		プロパティリストにはボタンが表示され、ボタンをクリックするとダイアログが表示されます
		ダイアログはオブジェクト内部で表示してください
	*/
	void AddDialogProperty(wchar_t const* name, int id, int value, bool readOnly = false)
	{
		if (m_current) {
			m_current = (m_current->next = MakeDialogProperty(name, id, value));
		}
		else {
			m_current = (m_node->child = MakeDialogProperty(name, id, value));
		}
		m_current->readOnly = readOnly;
	}
	void AddDialogProperty(wchar_t const* name, int id, wchar_t const* value, bool readOnly = false)
	{
		if (m_current) {
			m_current = (m_current->next = MakeDialogProperty(name, id, value));
		}
		else {
			m_current = (m_node->child = MakeDialogProperty(name, id, value));
		}
		m_current->readOnly = readOnly;
	}

	virtual void AddCustomDialogProperty(wchar_t const* name, int id, wchar_t const* value, LPARAM param, bool readOnly = false)
	{
		if (m_current) {
			m_current = (m_current->next = MakeCustomDialogProperty(name, id, value, param));
		}
		else {
			m_current = (m_node->child = MakeCustomDialogProperty(name, id, value, param));
		}
		m_current->readOnly = readOnly;
	}

	/*! フォントプロパティを作成します
		フォントタイプの選択コンボボックスを表示します
	*/
	void AddFontProperty(wchar_t const* name, int id, wchar_t const* value, bool readOnly = false)
	{
		if (m_current) {
			m_current = (m_current->next = MakeFontProperty(name, id, value));
		}
		else {
			m_current = (m_node->child = MakeFontProperty(name, id, value));
		}
		m_current->readOnly = readOnly;
	}
	void AddFontProperty(wchar_t const* name, int id, LOGFONT const& value, bool readOnly = false)
	{
		if (m_current) {
			m_current = (m_current->next = MakeFontProperty(name, id, value));
		}
		else {
			m_current = (m_node->child = MakeFontProperty(name, id, value));
		}
		m_current->readOnly = readOnly;
	}

	/*! オーナードローするカスタムプロパティを作成します(ラインの端点形状専用)
		設定情報をオーナードローで描画する以外はカスタムプロパティと同じ仕様になります。
	*/
	void AddDrawDialogProperty(wchar_t const* name, int id, int stStyle, int edStyle, bool readOnly = false)
	{
		int style = MAKELONG(stStyle, edStyle);

		if (m_current) {
			m_current = (m_current->next = MakeDrawDlgProperty(name, id, style));
		}
		else {
			m_current = (m_node->child = MakeDrawDlgProperty(name, id, style));
		}
		m_current->readOnly = readOnly;
	}

	virtual void AddArrowStyleProperty(wchar_t const* name, int id, int style, bool readOnly = false)
	{
		ValueList* beg = 0, *arr = 0;
		beg = arr = new ValueList(_T("△"));
		arr = (arr->next = new ValueList(_T("△")));
		arr = (arr->next = new ValueList(_T("▲")));

		PropertyNode* node = MakeArrowStyleProperty(name, id, style);
		if (m_current) {
			m_current = (m_current->next = node);
		}
		else {
			m_current = (m_node->child = node);
		}
		m_current->u.aryValue = beg;
		m_current->readOnly = readOnly;
	}

	void AddDrawComboProperty(wchar_t const* name, int id, int value, int count, OwnerDraw::OnDrawComboValue func)
	{
		if (m_current) {
			m_current = (m_current->next = MakeDrawComboProperty(name, id, value, count, func));
		}
		else {
			m_current = (m_node->child = MakeDrawComboProperty(name, id, value, count, func));
		}
	}

	void AddArrayProperty(wchar_t const* name, int id, int index, int count, ...)
	{
		va_list marker;
		va_start(marker, count);
		ValueList* beg = 0, *arr = 0;
		while (count--) {
			if (beg == 0) { 
				beg = arr = new ValueList(va_arg(marker, wchar_t const*));
			}
			else { 
				arr = (arr->next = new ValueList(va_arg(marker, wchar_t const*)));
			}
		}
		va_end(marker);

		if (m_current) {
			m_current = (m_current->next = CreateProperty(name, id));
		}
		else {
			m_current = (m_node->child = CreateProperty(name, id));
		}
		m_current->type = PROP_ARRAY;
		m_current->aryIndex = static_cast<short>(index);
		m_current->u.aryValue = beg;
	}
	void AddArrayProperty(wchar_t const* name, int id, int index, bool readOnly, int count, ...)
	{
		va_list marker;
		va_start(marker, count);
		ValueList* beg = 0, *arr = 0;
		while (count--) {
			if (beg == 0) { 
				beg = arr = new ValueList(va_arg(marker, wchar_t const*));
			}
			else { 
				arr = (arr->next = new ValueList(va_arg(marker, wchar_t const*)));
			}
		}
		va_end(marker);

		if (m_current) {
			m_current = (m_current->next = CreateProperty(name, id, readOnly));
		}
		else {
			m_current = (m_node->child = CreateProperty(name, id, readOnly));
		}
		m_current->type = PROP_ARRAY;
		m_current->aryIndex = static_cast<short>(index);
		m_current->u.aryValue = beg;
	}

	void AddArrayProperty(wchar_t const* name, int id, int index, std::vector<std::wstring> const& elems)
	{
		ValueList* beg = 0, *arr = 0;
		for (size_t i = 0; i < elems.size(); ++i) {
			if (beg == 0) { 
				beg = arr = new ValueList(elems[i].c_str());
			}
			else { 
				arr = (arr->next = new ValueList(elems[i].c_str()));
			}
		}

		if (m_current) {
			m_current = (m_current->next = CreateProperty(name, id));
		}
		else {
			m_current = (m_node->child = CreateProperty(name, id));
		}
		m_current->type = PROP_ARRAY;
		m_current->aryIndex = static_cast<short>(index);
		m_current->u.aryValue = beg;
	}
	void AddArrayProperty(wchar_t const* name, int id, bool readOnly, int index, std::vector<std::wstring> const& elems)
	{
		ValueList* beg = 0, *arr = 0;
		for (size_t i = 0; i < elems.size(); ++i) {
			if (beg == 0) { 
				beg = arr = new ValueList(elems[i].c_str());
			}
			else { 
				arr = (arr->next = new ValueList(elems[i].c_str()));
			}
		}

		if (m_current) {
			m_current = (m_current->next = CreateProperty(name, id, readOnly));
		}
		else {
			m_current = (m_node->child = CreateProperty(name, id, readOnly));
		}
		m_current->type = PROP_ARRAY;
		m_current->aryIndex = static_cast<short>(index);
		m_current->u.aryValue = beg;
	}

	void AddArrayProperty(wchar_t const* name, int id, int index, ValueList* arr)
	{
		if (m_current) {
			m_current = (m_current->next = CreateProperty(name, id));
		}
		else {
			m_current = (m_node->child = CreateProperty(name, id));
		}
		m_current->type = PROP_ARRAY;
		m_current->aryIndex = static_cast<short>(index);
		m_current->u.aryValue = arr;		
	}

	/*!	@brief グループを追加します
		@param[in]	name	グループの名称
	*/
	IPropertyGroup* AddGroup(wchar_t const* name, int id = -1, bool checked = false, bool readOnly = false)
	{
		OCPropertyGroup group(name);
		m_child.push_back(group);

		OCPropertyGroup* ret = dynamic_cast<OCPropertyGroup*>(&m_child.back());
		ret->m_node->id = id;
		ret->m_node->type = PROP_BOOL;
		ret->m_node->u.nValue = !!checked;
		ret->m_node->useChild = 1;
		ret->m_node->readOnly = readOnly;
		if (m_current) {
			m_current = (m_current->next = ret->m_node);
		}
		else {
			m_current = (m_node->child = ret->m_node);
		}

		return ret;
	}

private:
	PropertyNode* CreateProperty(wchar_t const* name, int id, bool readOnly = false)
	{
		PropertyNode* prop = new PropertyNode();
		::wcsncpy_s(prop->name, BUFF_SIZE, name, BUFF_SIZE - 1);
		prop->id = id;
		prop->readOnly = readOnly;
		return prop;
	}

	PropertyNode* MakeColorProperty(wchar_t const* name, int id, COLORREF value, bool readOnly)
	{
		PropertyNode* prop = CreateProperty(name, id, readOnly);
		prop->type = PROP_COLOR;
		prop->u.clrValue = value;
		return prop;
	}
	PropertyNode* MakeBooleanProperty(wchar_t const* name, int id, BOOL value, bool readOnly = false)
	{
		PropertyNode* prop = CreateProperty(name, id, readOnly);
		prop->type = PROP_BOOL;
		prop->u.nValue = value;
		return prop;
	}
	PropertyNode* MakeProperty(wchar_t const* name, int id, int value, bool readOnly = false)
	{
		PropertyNode* prop = CreateProperty(name, id, readOnly);
		prop->type = PROP_INT;
		prop->u.nValue = value;
		return prop;
	}
	PropertyNode* MakeProperty(wchar_t const* name, int id, float value, bool readOnly = false)
	{
		PropertyNode* prop = CreateProperty(name, id, readOnly);
		prop->type = PROP_DBL;
		prop->u.dValue = value;
		return prop;
	}
	PropertyNode* MakeProperty(wchar_t const* name, int id, double value, bool readOnly = false)
	{
		PropertyNode* prop = CreateProperty(name, id, readOnly);
		prop->type = PROP_DBL;
		prop->u.dValue = value;
		return prop;
	}
	PropertyNode* MakeProperty(wchar_t const* name, int id, wchar_t const* value, bool readOnly = false)
	{
		PropertyNode* prop = CreateProperty(name, id, readOnly);
		prop->type = PROP_STR;
		::wcsncpy_s(prop->u.strValue, BUFF_SIZE, value, BUFF_SIZE - 1);
		return prop;
	}
	PropertyNode* MakeSliderProperty(wchar_t const* name, int id, int value, int begin, int end)
	{
		PropertyNode* prop = CreateProperty(name, id);
		prop->type = PROP_SLIDER;
		prop->u.nValue = value;
		prop->begin = begin;
		prop->end = end;
		return prop;
	}
	PropertyNode* MakeDialogProperty(wchar_t const* name, int id, int value)
	{
		PropertyNode* prop = CreateProperty(name, id);
		prop->type = PROP_CUSTOM_DLG;
		prop->u.nValue = value;
		return prop;
	}
	PropertyNode* MakeDialogProperty(wchar_t const* name, int id, wchar_t const* value)
	{
		PropertyNode* prop = CreateProperty(name, id);
		prop->type = PROP_CUSTOM_DLG;
		::wcsncpy_s(prop->u.strValue, BUFF_SIZE, value, BUFF_SIZE - 1);
		return prop;
	}
	PropertyNode* MakeCustomDialogProperty(wchar_t const* name, int id, wchar_t const* value, LPARAM param)
	{
		PropertyNode* prop = CreateProperty(name, id);
		prop->type = PROP_CUSTOM_DLG_EX;
		::wcsncpy_s(prop->u.strValue, BUFF_SIZE, value, BUFF_SIZE - 1);
		prop->aryIndex = static_cast<long>(param);
		return prop;
	}
	PropertyNode* MakeFontProperty(wchar_t const* name, int id, wchar_t const* value)
	{
		PropertyNode* prop = CreateProperty(name, id);
		prop->type = PROP_FONT;
		::wcsncpy_s(prop->u.strValue, BUFF_SIZE, value, BUFF_SIZE - 1);
		return prop;
	}
	PropertyNode* MakeFontProperty(wchar_t const* name, int id, LOGFONT const& value)
	{
		PropertyNode* prop = CreateProperty(name, id);
		prop->type = PROP_FONT;
		prop->u.ptr = &value;
		return prop;
	}
	PropertyNode* MakeDrawDlgProperty(wchar_t const* name, int id, int value)
	{
		PropertyNode* prop = CreateProperty(name, id);
		prop->type = PROP_DRAW_DLG;
		prop->u.nValue = value;
		return prop;
	}
	PropertyNode* MakeDrawComboProperty(wchar_t const* name, int id, int value, int count, OwnerDraw::OnDrawComboValue func)
	{
		PropertyNode* prop = CreateProperty(name, id);
		prop->type = PROP_COMBO_OWD;

		static OwnerDraw od;
		od.OnDraw = func;
		od.index = value;
		od.count = count;
		prop->u.ptr = &od;
		return prop;
	}
	PropertyNode* MakeArrowStyleProperty(wchar_t const* name, int id, int value)
	{
		PropertyNode* prop = CreateProperty(name, id);
		prop->type = PROP_ARRAY;
		prop->aryIndex = value;
		return prop;
	}

private:
	PropertyNode* m_node;
	PropertyNode* m_current;

	std::vector<OCPropertyGroup> m_child;
};

//! プロパティの作成を行うためのヘルパクラス
class OCProperty : public IPropertyCreator, public IPropertyComposite
{
public:
	/*!	@brief プロパティグループを作成します
		@param name	グループの名称
	*/
	IPropertyGroup* CreateGroup(wchar_t const* name)
	{
		for (size_t i = 0; i < m_group.size(); ++i) {
			OCPropertyGroup& grp = m_group[i];
			if (::_tcsicmp(name, grp.GetName()) == 0) {
				return &grp;
			}
		}

		OCPropertyGroup grp(name);
		if (!m_group.empty()) {
			m_group.back().m_node->next = grp.m_node;
		}
		m_group.push_back(grp);
		return &m_group.back();
	}

	/*! @berief	チェックボックスつきのプロパティグループを作成します
		@param	name	グループの名称
		@param	checked	チェック状態(ON/OFF)
	*/
	IPropertyGroup* CreateGroup(wchar_t const* name, int id, BOOL checked, bool readOnly = false)
	{
		OCPropertyGroup grp(name);
		if (!m_group.empty()) {
			m_group.back().m_node->next = grp.m_node;
		}
		grp.m_node->id = id;
		grp.m_node->type = PROP_BOOL;
		grp.m_node->u.nValue = checked;
		grp.m_node->useChild = 1;
		grp.m_node->readOnly = readOnly;
		m_group.push_back(grp);
		return &m_group.back();
	}

	//! 作成済みのプロパティ構造体のポインタを返します
	/*!	@attention	この関数が返すポインタは呼び出し元でdeleteすること!
	*/
	PropertyNode* GetRawPtr() const
	{
		return m_group.empty() ? 0 : m_group.front().m_node;
	}

	virtual IPropertyGroup* AddGroup(wchar_t const* name, int id = -1, bool checked = false, bool readOnly = false)
	{
		id, checked, readOnly;

		for (size_t i = 0; i < m_group.size(); ++i) {
			if (::_wcsicmp(m_group[i].m_node->name, name) == 0) {
				return &m_group[i];
			}
		}
		return CreateGroup(name, id, checked, readOnly);
	}

private:
	std::vector<OCPropertyGroup> m_group;
};

/******************************************************************************
	SetPropertyHelper
******************************************************************************/

template<class T>
inline void SetPropertyHelper(T& target, LPARAM value, LPARAM* source)
{
	if (source) { *source = static_cast<LPARAM>(target); }
	target = static_cast<T>(value);
}

template<class T>
inline void SetPropertyHelper(safe_enum<T>& target, LPARAM value, LPARAM* source)
{
	if (source) { *source = static_cast<LPARAM>(target.underlying()); }
	target = static_cast<T::type>(value);
}

template<>
inline void SetPropertyHelper<std::wstring>(std::wstring& target, LPARAM value, LPARAM* source)
{
	if (source) {
		wchar_t* buff = reinterpret_cast<wchar_t*>(source);
		::wcsncpy_s(buff, 512, target.c_str(), 512 - 1);
	}
	target = reinterpret_cast<wchar_t*>(value);
}

template<>
inline void SetPropertyHelper<float>(float& target, LPARAM value, LPARAM* source)
{
	double v = *reinterpret_cast<double*>(value);
	if (source) { *(double*)source = target; }
	target = static_cast<float>(v);
}

template<>
inline void SetPropertyHelper<double>(double& target, LPARAM value, LPARAM* source)
{
	double v = *reinterpret_cast<double*>(value);
	if (source) { *(double*)source = target; }
	target = v;
}

template<>
inline void SetPropertyHelper<bool>(bool& target, LPARAM value, LPARAM* source)
{
	if (source) { *source = target ? 1 : 0; }
	target = !!value;
}

// between
template<class T>
inline bool SetPropertyHelper(T& target, LPARAM value, LPARAM* source, T const& min, T const& max)
{
	if (value >= min && value <= max) {
		if (source) { *source = static_cast<LPARAM>(target); }
		target = static_cast<T>(value);
		return true;
	}
	return false;
}

template<>
inline bool SetPropertyHelper<double>(double& target, LPARAM value, LPARAM* source, double const& min, double const& max)
{
	double real = *reinterpret_cast<double*>(value);
	if (real >= min && real <= max) {
		if (source) { *(double*)source = target; }
		target = real;
		return true;
	}
	return false;
}

template<>
inline bool SetPropertyHelper<float>(float& target, LPARAM value, LPARAM* source, float const& min, float const& max)
{
	float real = static_cast<float>(*reinterpret_cast<double*>(value));
	if (real >= min && real <= max) {
		if (source) { *(double*)source = target; }
		target = static_cast<float>(real);
		return true;
	}
	return false;
}

// !=
template<class T>
inline bool SetPropertyHelper(T& target, LPARAM value, LPARAM* source, T const& notValue)
{
	if (target != notValue) {
		if (source) { *source = static_cast<LPARAM>(target); }
		target = static_cast<T>(value);
		return true;
	}
	return false;
}

template<>
inline bool SetPropertyHelper(double& target, LPARAM value, LPARAM* source, double const& notValue)
{
	double v = *reinterpret_cast<double*>(value);

	if (!IsZero(v - notValue)) {
		if (source) { *(double*)source = v; }
		target = static_cast<float>(v);
		return true;
	}
	return false;
}

inline void CopyFont(LPARAM* source, LOGFONT const& font, COLORREF color)
{
	if (source) {
		LOGFONT* lf = reinterpret_cast<LOGFONT*>(source);
		*lf = font;
		lf->lfWidth = color;
	}
}

//template<>
//inline bool SetPropertyHelper(float& target, LPARAM value, LPARAM* source, double const& notValue)
//{
//	double v = *reinterpret_cast<double*>(value);
//
//	if (!IsZero(v - notValue)) {
//		if (source) { *(double*)source = v; }
//		target = static_cast<float>(v);
//		return true;
//	}
//	return false;
//}
