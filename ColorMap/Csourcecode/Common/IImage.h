/*!	@file
	@brief	イメージを持つオブジェクトの基底クラス
	@author	wada
	@date	2007-12-19
*/
#pragma once

class IImage
{
public:
	virtual bool LoadFile(wchar_t const* fileName) = 0;
	virtual bool SetBitmap(HBITMAP bitmap) = 0;
	/*!	@brief	イメージを設定します
		@param[in]	type	イメージの種別
		@param[in]	handle	イメージハンドル

		@li CF_BITMAP
		@li CF_DIB
		@li CF_METAFILEPICT
	*/
	virtual bool SetImageHandle(int type, HANDLE handle) = 0;
};