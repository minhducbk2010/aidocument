/*!	@file
	@brief	線種設定に関する定義ファイル
*/
#pragma once

#include <vector>
#include <string>
#include "ICanvas.h"
#include "Message.h"

namespace {

//! 設定可能な線種の一覧を取得します
std::vector<std::wstring> GetPenStyles(ICanvas* canvas, IMessageReceiver* owner)
{
	std::vector<std::wstring> styles;
	styles.push_back(::GetResourceString(owner, "ID_SOURCE_TEXT_SOLID_LINE"));
	styles.push_back(::GetResourceString(owner, "ID_SOURCE_TEXT_DASHED_LINE"));
	styles.push_back(::GetResourceString(owner, "ID_SOURCE_TEXT_DOTTED_LINE"));
	styles.push_back(::GetResourceString(owner, "ID_SOURCE_TEXT_ONE_P_DASHED_LINE"));
	styles.push_back(::GetResourceString(owner, "ID_RESOURCE_TWO_POINTS_OF_DOT_DASH_LINES"));
	styles.push_back(::GetResourceString(owner, "ID_SOURCE_TEXT_NONE_KANJI")); //TODO: 仕様では「無し」は一番下に置かれる(PS_NULLとインデックスがずれるので、暫定的に上に置いている)
	for (long i = 0; i < canvas->GetCustomStyleCount(); ++i) {
		styles.push_back(canvas->GetCustomStyleName(i));
	}
	return styles;
}

}