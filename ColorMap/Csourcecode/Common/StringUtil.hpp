#pragma once

#include <boost/algorithm/string.hpp>
#include "type_safe_enum.h"

struct StringFormat_def {
	enum type
	{
		Digit,		//!< 小数
		Exponent,	//!< 指数
		NotExponent, //!< 小数、桁数指定なし(表示形式「小数」で、桁数「自動」にすると、指数になることがあるので)
	};
};

typedef safe_enum<StringFormat_def> StringFormat;

inline std::wstring DblToStr(double value, StringFormat format, int decimalPlace)
{
	wchar_t buf[36], fmt[12];
	if (format == StringFormat::Digit) {
		if (decimalPlace == -1) {
			::swprintf_s(fmt, _countof(fmt), _T("%%g"));
		}
		else {
			::swprintf_s(fmt, _countof(fmt), _T("%%.%df"), decimalPlace);
		}
	}
	else if (format == StringFormat::Exponent){
		if (decimalPlace == -1) {
			::swprintf_s(fmt, _countof(fmt), _T("%%e"));
		}
		else {
			::swprintf_s(fmt, _countof(fmt), _T("%%.%de"), decimalPlace);
		}
	}
	else {
		::swprintf_s(fmt, _countof(fmt), _T("%%f"));
	}
	::_snwprintf_s(buf, _countof(buf), _countof(buf) - 1, fmt, value);
	return buf;
}

/*!	@brief	数字文字列にカンマを付与します
	@param[in]	in	数字文字列
	@return	カンマを付与した文字列
	@note	1000 -> 1,000
*/
inline std::wstring SetComma(std::wstring const& in)
{
	std::wstring result;

	std::vector<std::wstring> v;
	boost::algorithm::split(v, in, boost::is_any_of("."));
	if (v.empty()) { return in; }

	std::wstring fixed = v[0];
	typedef std::wstring::reverse_iterator iter_t;
	size_t count = 0;
	for (iter_t it = fixed.rbegin(); it != fixed.rend(); ++it) {
		if (count++ == 3) {
			if (*it != '-') {
				result.insert(0, 1, ',');
				count = 1;
			}
		}
		result.insert(0, 1, *it);
	}

	return v.size() > 1 ? result + _T(".") + v[1] : result;
}
