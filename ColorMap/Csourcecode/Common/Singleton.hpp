#pragma once

template<class T>
class OCSingleton
{
public:
	//! ローダークラスのインスタンスを取得します
	static T& GetInstance()
	{
		// マルチスレッドは考慮しない
		static T instance;
		return instance;
	}

protected:
	//!@{Singleton
	OCSingleton() {}
	OCSingleton(OCPluginLoader const&);
	OCSingleton& operator=(OCPluginLoader const&);
	//@}
};