#pragma once

// 演算式(Arithmetic expression)の計算
#include "StringDefine.h"

// エラーコード
#define SUCCESS	0	// 演算成功
#define SYNTAX_ERROR	1	// 構文エラー(逆ポーランド記法に変換できなかった)
#define CALC_ERROR	2	// 逆ポーランド演算でエラー
#define DIV0_ERROR	3	// 演算の途中で0で割った
#define OVERFLOW_ERROR 4 // 演算の途中でdoubleの上下限を超えて計算できなくなった

// 定数
#define NUMBERS 10	// 数字文字列の数(0〜9)
#define OPERATORS 6	// 演算子の数( (  )  +  -  *  / )

// 演算子優先度
#define OPERANK_HIGH 2  // 演算子優先度高(*,/)
#define OPERANK_LOW	1	// 演算子優先度低(+,-)
#define OPERANK_BRACKET 0	// 演算子括弧


class CCalcAE
{
public:
	CCalcAE(void) {}
	virtual ~CCalcAE(void) {}

	int Calculate(std::wstring ae, double& result)
	{
		if (ae == STRING_NOTHING) { return SYNTAX_ERROR; }

		std::vector<std::wstring> revPolishAe;
		int ret = ToReversePolish(ae, revPolishAe);

		if (ret != 0) {
			return ret;
		}

		ret = CalcRevPolishExpression(revPolishAe, result);
		if (ret != 0) {
			return ret;
		}

		return SUCCESS;
	}


private:
	//! 演算式をトークンに分解(数字と+,-,*,/,(,)以外が入っていた場合はfalseを返す)
	bool ToTokens(std::wstring ae, std::vector<std::wstring>& tokens)
	{
		bool afterLeft = true; // 先頭、もしくは左括弧が出てから最初の文字列を検証
		bool numberFlag = false; // 数値をトークンにしている最中か
		bool afterDot = false; // 小数点の後か
		bool useDotToken = false; // 既に小数点を使っているトークン
		std::wstring token = STRING_NOTHING; // tokensに入れる文字列

		const std::wstring numbers[NUMBERS] = 
			{STRING_ZERO, STRING_ONE, STRING_TWO, 
			STRING_THREE, STRING_FOUR, STRING_FIVE, 
			STRING_SIX , STRING_SEVEN, STRING_EIGHT, STRING_NINE};
		const std::wstring operators[OPERATORS] = {STRING_LEFT, STRING_RIGHT, STRING_PLUS, STRING_MINUS, STRING_ASTERISK, STRING_SLASH};

		for (size_t i = 0; i < ae.size(); i++) {
			std::wstring temp;
			temp = ae.substr(i, 1);

			// 空白文字は削除し、今の文字列をトークンとして追加
			if (temp == STRING_SPACE || temp == STRING_DBSPACE || temp == STRING_TAB) {
				if (token != STRING_NOTHING) { 
					tokens.push_back(token); 
					token.clear();
				}
				continue;
			}

			bool hit = false;
			for (int n = 0; n < NUMBERS; n++) {
				// 0〜9のチェック
				if (temp == numbers[n]) {
					hit = true;
					break;
				}
			}

			// 1〜9の数字だった場合
			if (hit) { 
				token += temp;
				numberFlag = true; 
				afterLeft = false;
				afterDot = false;
				continue; 
			}
			
			// 小数点の後で、数字が見つからない時点でfalse
			if (afterDot) { return false; }

			// 「.」のチェック
			if (STRING_DOT == temp) {
				// 小数点が2回同じトークンで出た、またはトークンの先頭に小数点が来てしまった時はfalse
				if (useDotToken || token == STRING_NOTHING) { return false; }
				token += temp;
				afterDot = true;
				useDotToken = true;
				continue;
			}

			hit = false;
			for (int n = 0; n < OPERATORS; n++) {
				if (temp == operators[n]) {
					if (token != STRING_NOTHING) { 
						tokens.push_back(token); // 今までの数字をトークンとして確定
						token.clear();
					}
					
					if ((temp == STRING_PLUS || temp == STRING_MINUS) && afterLeft) { 
						tokens.push_back(STRING_ZERO); // 「-」が先頭の場合は0を追加する
						afterLeft = false;
					}
					else if (temp == STRING_LEFT) { afterLeft = true; }
					else { afterLeft = false; }
					tokens.push_back(temp); // 演算子をトークンとして登録
					hit = true;
					numberFlag = false;
					break;
				}
			}

			if (hit) { 
				afterDot = false;
				useDotToken = false;
				continue;
			}
			
			// 該当しなかった場合はfalse
			return false; 
		}

		if (token != STRING_NOTHING) { tokens.push_back(token); }

		return true;
	}

	//! 引数の演算子の優先度を返します
	//! (高い場合(*,/)はOPERANK_HIGH　= 2、低い場合(+,-)はOPERANK_LOW = 1, 括弧の場合はOPERANK_BRACKET = 0)
	int GetOpeTokenRank(std::wstring token)
	{
		if (token == STRING_ASTERISK || token == STRING_SLASH) { return OPERANK_HIGH; }
		if (token == STRING_LEFT || token == STRING_RIGHT) { return OPERANK_BRACKET; }
		else { return OPERANK_LOW; }
	}

	//! 演算式を逆ポーランド記法に変換
	//! Param[in]	ae		演算式
	//!	Param[out]	revPolishAe		逆ポーランド記法に変換されたトークンを入れるバッファ
	int ToReversePolish(std::wstring ae, std::vector<std::wstring>& revPolishAe)
	{
		std::vector<std::wstring> tokens;
		if (!ToTokens(ae, tokens) || tokens.empty()) { return SYNTAX_ERROR; }

		// 逆ポーランド記法に変換(ここに来る時点で、数字と+,-,*,/,(,)しかないことは保障されている)

		std::vector<std::wstring> ope; // 演算子用スタック +,-,*,/,(,)を入れる
		bool afterOpe = false; // 四則演算の演算子の後か
		int leftCount = 0;

		for (size_t i = 0; i < tokens.size(); i++) {
			// 数字かチェック
			TCHAR* end = STRING_NOTHING;
			::_tcstod(tokens[i].c_str(), &end);
			CString strEnd = end;
			if (strEnd == STRING_NOTHING) {
				revPolishAe.push_back(tokens[i]);
				afterOpe = false;
				continue;
			}
			
			// 演算子チェック
			if (tokens[i] == STRING_RIGHT) {
				while(1) {
					if (ope.size() < 1) { return SYNTAX_ERROR; }
					if (ope.back() == STRING_LEFT) {
						ope.pop_back();
						break;
					}
					revPolishAe.push_back(ope.back());
					ope.pop_back();
				}
				afterOpe = false;
				leftCount++;
			}
			else if (tokens[i] == STRING_LEFT) {
				ope.push_back(tokens[i]);
				afterOpe = false;
				leftCount--;
			}
			else {
				// 連続で四則演算の演算子が出てきたらfalse
				if (afterOpe) { return SYNTAX_ERROR; }
				// 優先順位は * / > + -
				while(ope.size() > 0) {
					// スタック最上段の演算子よりトークンの演算子の優先順位が低い、または同じとき
					if (GetOpeTokenRank(ope.back()) >= GetOpeTokenRank(tokens[i])) {
						revPolishAe.push_back(ope.back());
						ope.pop_back();
						continue;
					}
					break;
				}
				ope.push_back(tokens[i]);
				afterOpe = true;
			}
		}

		// 左かっこと右かっこの数が合わないときはエラー
		if (leftCount != 0 ) { return SYNTAX_ERROR; }

		// スタックが空になるまでバッファに格納
		while (ope.size() > 0) {
			revPolishAe.push_back(ope.back());
			ope.pop_back();
		}

		return SUCCESS;
	}

	// 逆ポーランド記法の演算式を計算する
	int CalcRevPolishExpression(std::vector<std::wstring> const& revPolishAe, double& result)
	{
		std::vector<double> stack;

		for (size_t i = 0; i < revPolishAe.size(); i++) {
			// 数字かチェック
			TCHAR* end = STRING_NOTHING;
			double value = ::_tcstod(revPolishAe[i].c_str(), &end);
			CString strEnd = end;
			if (strEnd == STRING_NOTHING) {
				stack.push_back(value);
				continue;
			}

			// 演算子の場合はスタックに2つ以上数字がなければならない
			if (stack.size() < 2) { return CALC_ERROR; }
			double value1 = stack.back();
			stack.pop_back();
			double value2 = stack.back();
			stack.pop_back();

			if (revPolishAe[i] == STRING_PLUS ) {
				stack.push_back(value2 + value1);
			}
			else if (revPolishAe[i] == STRING_MINUS) {
				stack.push_back(value2 - value1);
			}
			else if (revPolishAe[i] == STRING_ASTERISK) {
				stack.push_back(value2 * value1);
			}
			else if (revPolishAe[i] == STRING_SLASH) {
				// 0.0で割ってはいけない
				// 0.0か0.0でないかを判断するため、doubleの計算誤差に関してはここでは考慮しない
				if (value1 == 0.0) { return DIV0_ERROR; }
				stack.push_back(value2 / value1);
			}
			else { return CALC_ERROR; }

			// オーバーフローしている場合はエラー
			if (stack[stack.size() - 1] == HUGE_VAL || stack[stack.size() - 1] == - HUGE_VAL) { return OVERFLOW_ERROR; }
		
		}
		// スタックに要素がない、または2つ以上の要素がある場合は演算失敗
		if (stack.empty() ||  stack.size() > 1) { return CALC_ERROR; }
		
		result = stack[0];
		return SUCCESS;
	}
};
