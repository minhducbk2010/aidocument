/*!	@file
	@date	2007-12-11
	@author	wada

	@brief	図形オブジェクトの抽象基底クラス
	図形プラグインはすべてこのインターフェイスを使用します
	EXE,DLLで共有するインターフェイス
*/
#pragma once

#import <msxml3.dll>
#include "IProperty.h"

class ICanvas;
class IMessageReceiver;
class IGroup;
struct PropertyNode;
class ISerializer;

//! 図形クラス
class IShape : public IProperty
{
protected:
	// このインターフェイスを削除するには Destroy メンバ関数を使用します
	virtual ~IShape() = 0 {}
public:
	//! オブジェクトを解放します
	/*! 派生先で delete this を実行すること */
	virtual void Destroy() = 0;

	//! オブジェクトの固有IDを取得します
	virtual int GetId() const = 0;
	virtual void SetId(int id) = 0;

	virtual IMessageReceiver* GetOwner() const = 0;
	virtual void ChangeOwner(IMessageReceiver* owner) = 0;
	virtual HWND GetParentWindow() const = 0;

	virtual void SetParent(IGroup* parent) = 0;
	virtual IGroup* GetParent() const = 0;

	//! オブジェクトの名称を取得します
    virtual void GetName(wchar_t* name, size_t length) const = 0;

	//! クラスの固有IDを取得します
	virtual unsigned long GetTypeId() const = 0;
	//! 名称を編集できるかどうかを取得します
	virtual bool IsNameEditable() const = 0;
	//! ユーザが削除できるかどうかを取得します
	virtual bool IsDeletable() const = 0;

	virtual bool IsChainCreate() const = 0;
	virtual bool IsDraggingCreate() const = 0;

	virtual void SetCanvas(ICanvas* canvas) = 0;
	virtual ICanvas* GetCanvas() const = 0;

	virtual void SetAngle(int angle) = 0;
	virtual int GetAngle() const = 0;

	virtual void Lock(bool lock) = 0;
	virtual bool IsLock() const = 0;

	//! オブジェクトの表示状態を取得します
	/*!	@return	true = 表示, false = 非表示 */
	virtual bool IsVisible() const = 0;
	//! オブジェクトが有効であるかどうかを取得します
	/*!
		無効な(falseを返す)オブジェクトはオブジェクトウィンドウに表示されません
	*/
	virtual bool IsEffective() const = 0;

	//! オブジェクトの移動を許可するかどうかを設定します
	virtual void PositionLock(bool lock) = 0;
	virtual bool IsPositionLock() const = 0;

	//! オブジェクトを指定量分移動します
	virtual void Offset(double xOffset, double yOffset) = 0;
	//! オブジェクトを指定の座標に移動します
	virtual void Move(double left, double top) = 0;

	//! 描画
	virtual void Draw() = 0;
	//! 選択状態を描画
	virtual void DrawSelected() = 0;
	//! 点滅の点灯を描画
	virtual void DrawBlink() = 0;

	//! 当たり判定
	virtual int HitTest(double x, double y) const = 0;
	virtual bool Contains(double left, double top, double right, double bottom) const = 0;

	virtual void MouseDown(unsigned long flags, double x, double y) = 0;
	virtual void MouseMove(unsigned long flags, double x, double y) = 0;
	virtual void MouseUp(unsigned long flags, double x, double y) = 0;

	virtual bool KeyDown(unsigned long key, unsigned short repeat, unsigned long flags) = 0;

	//! ドラッグ開始
	virtual void BeginDrag(int type, double x, double y) = 0;
	//! 親がドラッグ開始
	virtual void ParentBeginDrag(int type, double x, double y) = 0;
	//! ドラッグ
	virtual void Drag(double x, double y) = 0;
	//! ドラッグ終了
	virtual long EndDrag(double x, double y) = 0;
	//! ダブルクリック
	virtual void DoubleClick(double x, double y) = 0;

	virtual int SaveState() = 0;
	virtual void RestoreState(int state) = 0;
	virtual void ClearState() = 0;

	//! オブジェクトの全体を含む矩形座標を取得します
	virtual void GetRect(double* x, double* y, double* right, double* bottom) const = 0;
	virtual void GetRotatedRect(double* x, double* y, double* right, double* bottom) const = 0;
	//! オブジェクトの座標を設定します
	virtual void SetRect(double left, double top, double right, double bottom) = 0;
	virtual void GetStartPoint(double* x, double* y) const = 0;
	virtual void GetStopPoint(double* x, double* y) const = 0;

	//! カーソルを取得
	virtual LPTSTR GetCursor(double x, double y) const = 0;
	virtual long GetToolHint(wchar_t* text, long length) const = 0;

	virtual void SetSelected(bool select) = 0;
	virtual bool GetSelected() const = 0;

	virtual void InvertLR() = 0;
	virtual void InvertTB() = 0;

	virtual void DeleteCustomLines(int const* deleteLineIndexes, int count) = 0;

	//! 条件付き書式を持っているオブジェクトかどうか
	virtual bool IsCondFormatEditable() const = 0;

	// Serialize
	virtual void Serialize(ISerializer* ar) = 0;

	/*! @brief 引数のオブジェクトの情報をコピーします
		書式などを引き継ぎます
	*/
	virtual void CopyFrom(IShape const* source) = 0;

	virtual IShape* Clone(IMessageReceiver* owner) /*const*/ = 0;
};