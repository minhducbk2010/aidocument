/*!	@file
	@date	2008-07-04
	@author	wada

	@brief	レイアウトグリッドの描画関数定義
*/
#pragma once
#include "ColorAdjustment.h"

COLORREF GetLightColor(COLORREF src);

inline double MMToPx(HDC dc, double const mm)
{
	int dpi = ::GetDeviceCaps(dc, LOGPIXELSX);

    double const INCH = 25.4059;
	double const PIXEL = dpi / INCH;
    return PIXEL * mm;
}

static float GetPenWidth(HDC dc, float width)
{
    double const mm = width * 0.3528f;
	double const pw = std::max<double>(MMToPx(dc, mm), 1);
    return static_cast<float>(pw);
}

/*!	レイアウトグリッドを描画します
	@param[in]	dc			描画先のデバイスコンテキスト
	@param[in]	interval	グリッド間隔(単位はミリメートル)
	@param[in]	divide		グリッドの分割数
	@param[in]	color		グリッドの色
	@param[in]	mmRect		グリッドの描画領域(単位はミリメートル)

	@pre	interval > 0
	@pre	divide > 0
*/
inline void DrawGrid(HDC hdc, double const interval, int const divide, COLORREF const color, RECT const& mmRect, bool const print)
{
	_ASSERT(interval > 0);
	_ASSERT(divide > 0);

	CDC dc; dc.Attach(hdc);

	int const penWidth = 1;//GetPenWidth(hdc, 0.25f);
	CPen pen;
	CPen subPen;

	if (penWidth > 1 && false) {
		int const style = (divide == 1 || !print) ? PS_DOT : PS_SOLID;
		LOGBRUSH brush = {};
		brush.lbStyle = PS_SOLID;
		brush.lbColor = color;
		pen.CreatePen(style | PS_GEOMETRIC | PS_ENDCAP_SQUARE, penWidth, &brush);

		brush.lbColor = GetLightColor(color);
		subPen.CreatePen(PS_SOLID | PS_GEOMETRIC | PS_ENDCAP_SQUARE, penWidth, color);
	}
	else {
		pen.CreatePen(divide == 1 ? PS_DOT : PS_SOLID, penWidth, color);
		subPen.CreatePen(PS_SOLID, penWidth, GetLightColor(color));
	}

	CPen* oldPen = dc.SelectObject(&pen);

	std::vector<CPoint> pts;

	double const pxInterval = MMToPx(interval);
	double div = MMToPx(interval / (double)divide);
	OCRect pxRect(MMToPx(OCRect(mmRect)));
	// 縦のライン
	for (int i = 0; ; ++i) {
		double nx = i * pxInterval + pxRect.left;
		if (nx >= pxRect.right) { break; }

		int const px = nx + 0.5;
		if (i % 2) {
			pts.push_back(CPoint(px, pxRect.top));
			pts.push_back(CPoint(px, pxRect.bottom + 2));
		}
		else {
			pts.push_back(CPoint(px, pxRect.bottom + 2));
			pts.push_back(CPoint(px, pxRect.top));
		}

		CPen* cache = dc.SelectObject(&subPen);
		for (int j = 1; j < divide; ++j) {
			double mm = (i * pxInterval) + (div * j);
			nx = mm + pxRect.left;
			dc.MoveTo(static_cast<int>(nx + 0.5), static_cast<int>(pxRect.top));
			dc.LineTo(static_cast<int>(nx + 0.5), static_cast<int>(pxRect.bottom + 2));
		}
		dc.SelectObject(cache);
	}

	if (!pts.empty()) {
		dc.Polyline(&pts[0], static_cast<int>(pts.size()));
		pts.clear();
	}

	// 横のライン
	for (int i = 0; ; ++i) {
		double ny = i * pxInterval + pxRect.top;
		if (ny >= pxRect.bottom) { break; }

		int const py = ny + 0.5;
		if (i % 2) {
			pts.push_back(CPoint(pxRect.left, py));
			pts.push_back(CPoint(pxRect.right + 2, py));
		}
		else {
			pts.push_back(CPoint(pxRect.right + 2, py));
			pts.push_back(CPoint(pxRect.left, py));
		}

		CPen* cache = dc.SelectObject(&subPen);
		for (int j = 1; j < divide; ++j) {
			double mm = (i * pxInterval) + (div * j);
			ny = mm + pxRect.top + 0.5;
			dc.MoveTo(static_cast<int>(pxRect.left), static_cast<int>(ny + 0.5));
			dc.LineTo(static_cast<int>(pxRect.right + 2), static_cast<int>(ny + 0.5));
		}
		dc.SelectObject(cache);
	}

	if (!pts.empty()) {
		dc.Polyline(&pts[0], static_cast<int>(pts.size()));
	}
	dc.SelectObject(oldPen);
	dc.Detach();
}

//! 指定の色より若干淡い色を作成します
inline COLORREF GetLightColor(COLORREF src)
{
    unsigned short h;               // H(Hue/色相)
    unsigned char s, v;             // S(Saturation/彩度 0:白色〜255:派手)、V(Value/明度 0:暗い〜255:明るい)
    RGBtoHSV(src, &h, &s, &v);

    if(s < 60) {
        unsigned char tmp = static_cast<unsigned char>((255 - v) / 2);
        v = v + tmp;
    }

    s /= static_cast<unsigned char>(2);

    HSVtoRGB(&src, h, s, v);
    
    return src;
}
