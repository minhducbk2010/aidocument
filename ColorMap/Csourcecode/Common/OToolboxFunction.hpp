/*!	
	@brief	Toolboxで使用する、関数を管理する
*/
#pragma once
#include "Shlobj.h"
#include "IMessageReceiver.h"

struct ImportInfo
{
	int ItemCount;
	int DataCount;

	int ItemRow;
	int UnitRow;
	int DataStartRow;
	int CommentRows;

	int SheetNo;

	wchar_t ImportFileName[512];
};

struct WindowInfo{
	HWND hWnd;
};

BOOL TitleCmp(const wchar_t* title, const wchar_t* toolboxTitle)
{
	// 取得したタイトルの一部を抜き出し
	wchar_t splitTitle[MAXCHAR] ={0};
	int len = wcslen(toolboxTitle);
	wcsncpy(splitTitle, title, len);

	wchar_t macthTitle[MAXCHAR] = {0};
	wcscpy(macthTitle, toolboxTitle);

	return wcscmp(splitTitle, macthTitle) == 0;
}

BOOL CALLBACK EnumWndProc( HWND hWnd, LPARAM lParam )
{
	// ウインドウタイトルを取得
	wchar_t title[MAXCHAR] = _T("");
	GetWindowText(hWnd, title, sizeof(title));

	// 名前が一致したら、ウィンドウハンドルを設定
	if( TitleCmp(title, _T("O-Chart Toolbox -")) || 
		TitleCmp(title, _T("O-Chart Toolbox 体験版 -")) ||
		TitleCmp(title, _T("OC-1300 Toolbox -")) ||
		TitleCmp(title, _T("OC-1300 Toolbox trial version -")) ){
		((WindowInfo*)lParam)->hWnd = hWnd;
	}

	return true;
}

/*!
*/
inline HWND GetWindowHandle()
{
	WindowInfo info;
	info.hWnd =NULL;

	EnumWindows( EnumWndProc, (LPARAM)&info);
	return info.hWnd;
}

/*! Toolboxを起動します
*/
inline bool ActivateToolbox()
{
	// 既に起動されていたら何もしない
	HWND hwnd = GetWindowHandle();
	if(hwnd != NULL) { return true; }

	// レジストリ情報から起動
	HKEY hKey;
    if (::RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\Onosokki\\Onosokki O-Chart Toolbox"), 0, KEY_READ, &hKey) != ERROR_SUCCESS) 
	{
		return false;
	}

    DWORD nSize;
	wchar_t szPath[MAX_PATH];
    ::RegQueryValueEx(hKey, _T("Folder"), 0, 0, 0, &nSize);
    if (::RegQueryValueEx(hKey, _T("Folder"), 0, 0, (LPBYTE)szPath, &nSize) != ERROR_SUCCESS)
	{
		return false;
	}

    // Toolboxを起動
	std::wstring exePath(szPath);
	exePath += _T("\\OToolbox.exe");
	if( _taccess(exePath.c_str(), 0) != 0 )
	{
		return false;
	}
	
	STARTUPINFO m_si;
	PROCESS_INFORMATION m_pi;
	::ZeroMemory(&m_si, sizeof(STARTUPINFO));
	::ZeroMemory(&m_pi, sizeof(PROCESS_INFORMATION));
    ::CreateProcess(exePath.c_str(), 0, 0, 0, TRUE, NORMAL_PRIORITY_CLASS, 0, 0, &m_si, &m_pi);

	// Toolboxの起動時間待ち
	// 20秒ほど待つ
	for (int i = 0; i < 200; i++)
	{
		hwnd = GetWindowHandle();
		if(hwnd != NULL) { break; }
		::Sleep(100);
	}
	if(hwnd == NULL) { return false; }

	return true;
}

inline bool CreateOdfFile(wchar_t* itemNames, wchar_t* unitNames, float* data, int dataCount, const wchar_t* fileName, IMessageReceiver* owner)
{
	// OdfReaderWriter.dll の読み込み
	HKEY hKey;
    if (::RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\Onosokki\\\Onosokki O-Chart Toolbox"), 0, KEY_READ, &hKey) != ERROR_SUCCESS) 
	{
		return false;
	}
	DWORD nSize;
	wchar_t szPath[MAX_PATH];
    ::RegQueryValueEx(hKey, _T("Folder"), 0, 0, 0, &nSize);
    if (::RegQueryValueEx(hKey, _T("Folder"), 0, 0, (LPBYTE)szPath, &nSize) != ERROR_SUCCESS)
	{
		return false;
	}

	std::wstring dllPath(szPath);
	dllPath += _T("\\plugin\\OdfReaderWriter.dll");
	HMODULE dll = ::LoadLibrary(dllPath.c_str());
	if (!dll) { return false; }

	// ODFを保存するフォルダの作成
	LPITEMIDLIST pidlist;
	wchar_t specialPath[MAX_PATH];
	SHGetSpecialFolderLocation(GetWindowHandle(), CSIDL_LOCAL_APPDATA, &pidlist );
	SHGetPathFromIDList( pidlist, specialPath );

	std::wstring odfPath(specialPath);
	odfPath += _T("\\Onosokki O-Chart Toolbox\\Project01\\Import\\tmp");
	CreateDirectory(odfPath.c_str(), NULL);
	
	// ODFファイルの作成
	// とりあえず ファイル名[O-Chart] とした
	std::wstring title(fileName);
	title += ::GetResourceString(owner, "ID_SOURCE_TEXT_OC");

	ImportInfo info = {3, dataCount, 0, 0, 0, 0, 0, _T("")};
	const int FILENAME_LENGTH = 512;	// 構造体で定義されているものと同じ
	wcsncpy(info.ImportFileName, title.c_str(), FILENAME_LENGTH);

	typedef int (WINAPI * CreateOdfF)(wchar_t const* odfDirectoryPath, wchar_t* itemNames, wchar_t* unitNames, float* data, ImportInfo* info);
	CreateOdfF create = reinterpret_cast<CreateOdfF>(::GetProcAddress(dll, "CreateOdfFile"));
	create(odfPath.c_str(), itemNames, unitNames, data, &info);

	::FreeLibrary(dll);
	return true;
}

/*!
*/
inline bool SendMessageToToolbox()
{
	HWND hWnd;
	// 20秒ほど待つ
	for (int i = 0; i < 200; i++)
	{
		hWnd = GetWindowHandle();
		if(hWnd != NULL) { break; }
		::Sleep(100);
	}
	if(hWnd == NULL) { return false; }

	// コピー構造体で受け渡す値の設定
	COPYDATASTRUCT copy;
	ZeroMemory(&copy, sizeof(COPYDATASTRUCT));
	std::wstring message = _T("DigitalMap");	// メッセージは適当に設定
	copy.dwData = 0;
	copy.lpData = (PVOID)message.c_str();
	copy.cbData = wcslen(message.c_str()) * 2;	// マルチバイトなので大きさを倍に

	return ::SendMessage(hWnd, WM_COPYDATA, NULL, (LPARAM)&copy);
}

