/*!	@file
	@brief	カスタム線種を管理するためのクラスの実装
*/
#pragma once

#include <vector>
#include <string>
#include <boost/noncopyable.hpp>
#include <boost/foreach.hpp>
#include <shlobj.h>
#include "find_file.hpp"
#include "algorithmex.h"

class OCPenStyleManager : public boost::noncopyable
{
	struct Pattern {
		std::wstring name;
		std::wstring fileName;
		std::vector<float> pattern;

		bool operator==(Pattern const& other) const {
			return pattern == other.pattern && name == other.name;
		}
	};

public:
	OCPenStyleManager() : m_presetCount(0)
	{
		Reload();
	}

	void Reload();
	void Save();
	void Save(TCHAR const* directory);
	void SavePattern(std::wstring const& fileName, Pattern const& p) const;

	int GetUserStyleCount() const { return m_userStyleCount; }
	int GetPresetCount() const { return m_presetCount; }

	long GetPatternCount() const {
		return static_cast<long>(m_styles.size());
	}

	wchar_t const* GetPatternName(long index) const {
		return m_styles[index].name.c_str();
	}

	float const* GetPattern(long index) const {
		return &m_styles[index].pattern[0];
	}

	void AddPattern(TCHAR const* name, float const* pattern, long patternLength) {
		Pattern p;
		p.name = name;
		p.fileName = GetNewStyleFileName();
		p.pattern.assign(pattern, pattern + patternLength);
		m_styles.push_back(p);
	}

	void Overwrite(long index, float const* pattern, long patternLength) {
		m_styles[index].pattern.assign(pattern, pattern + patternLength);
	}

	void Remove(long index) {
		if (m_styles.size() <= static_cast<size_t>(index)) { return; }
		m_styles.erase(m_styles.begin() + index);
	}

	void SetSettingDirectory(std::wstring const& subDir) {
		m_subSettingDirectory = subDir;
	}

private:
	std::wstring GetNewStyleFileName() const;
	std::wstring MakeFileName(int index) const;
	int LoadDirectory(std::wstring const& dir);
	Pattern LoadPattern(std::wstring const& fileName) const;
	std::wstring GetCommonDirectory() const;

private:
	std::vector<Pattern> m_styles;
	std::wstring m_subSettingDirectory;
	int m_userStyleCount;
	int m_presetCount;
};

// Implement

void OCPenStyleManager::Reload()
{
	m_styles.clear();

	m_userStyleCount = LoadDirectory(m_subSettingDirectory);
	m_presetCount = LoadDirectory(GetCommonDirectory());
}

int OCPenStyleManager::LoadDirectory(std::wstring const& dir)
{
	TCHAR cond[_MAX_PATH] = {};
	::_tcscpy_s(cond, dir.c_str());
	::_tcscat_s(cond, _T("*.ini"));

	int loadCount = 0;

	find_file::find_file_range glob(cond);
	BOOST_FOREACH(WIN32_FIND_DATA& file, glob) {
		std::wstring fullPath = dir;
		fullPath += file.cFileName;
		Pattern p = LoadPattern(fullPath);
		if (p.pattern.size() > 0) {
			if (!oc::include(m_styles, p)) {
				m_styles.push_back(p);
				++loadCount;
			}
		}
	}
	return loadCount;
}

void OCPenStyleManager::Save()
{
	std::wstring const dir = GetCommonDirectory();
	::DeleteFiles(dir.c_str());
	int const errorCode = ::SHCreateDirectoryEx(0, dir.c_str(), nullptr);
	if (errorCode != 0) {
		OC_Log("Error SHCreateDirectoryEx -> [%d]", errorCode);
		assert(false);
	}

	int n = 0;
	BOOST_FOREACH(Pattern& p, m_styles) {
		std::wstring fileName = MakeFileName(n++);
		SavePattern(fileName, p);
		p.fileName = fileName;
	}
}

void OCPenStyleManager::Save(TCHAR const* directory)
{
	BOOST_FOREACH(Pattern& p, m_styles) {
		std::wstring fileName = std::wstring(directory) + ::PathFindFileName(p.fileName.c_str());
		SavePattern(fileName, p);
		p.fileName = fileName;
	}
}

void OCPenStyleManager::SavePattern(std::wstring const& fileName, Pattern const& p) const
{
	::WritePrivateProfileString(_T("PenStyle"), _T("Name"), p.name.c_str(), fileName.c_str());
	for (size_t i = 0; i < p.pattern.size(); ++i) {
		TCHAR key[16] = {}, value[32] = {};
		::_stprintf_s(key, _T("Value%d"), i + 1);
		::_stprintf_s(value, _T("%d"), (int)p.pattern[i]);
		::WritePrivateProfileString(_T("PenStyle"), key, value, fileName.c_str());
	}
}

std::wstring OCPenStyleManager::GetNewStyleFileName() const
{
	std::wstring path = GetCommonDirectory();
	for (int i = 1; ; ++i) {
		std::wstring const fileName = MakeFileName(i);
		if (::_taccess_s(fileName.c_str(), 0) == 0) { continue; }
		return fileName;
	}
}

std::wstring OCPenStyleManager::MakeFileName(int index) const
{
	std::wstring path = GetCommonDirectory();
	TCHAR fileName[_MAX_PATH];
	::_stprintf_s(fileName, _T("%s\\PenStyle_%03d.ini"), path.c_str(), index + 1);
	return fileName;
}

std::wstring OCPenStyleManager::GetCommonDirectory() const
{
	TCHAR path[_MAX_PATH] = {};
	::SHGetSpecialFolderPath(0, path, CSIDL_APPDATA, FALSE);
	::_tcscat_s(path, _T("\\Onosokki Common\\OSeries\\LineStyle\\"));

	return path;
}

OCPenStyleManager::Pattern OCPenStyleManager::LoadPattern(std::wstring const& fileName) const
{
	Pattern p;
	p.fileName = fileName;

	TCHAR name[20] = {};
	::GetPrivateProfileString(_T("PenStyle"), _T("Name"), _T(""), name, _countof(name), fileName.c_str());
	p.name = name;

	for (int i = 1; ; ++i) {
		TCHAR key[16] = {};
		::_stprintf_s(key, _T("Value%d"), i);
		int const offset = ::GetPrivateProfileInt(_T("PenStyle"), key, -1, fileName.c_str());
		if (offset <= 0) { break; }

		p.pattern.push_back(static_cast<float>(offset));
	}
	return p;
}
