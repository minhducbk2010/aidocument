/*! @file
	@brief	平面コンターマップのクリップライン設定
	@data	2008-07-28
*/
#pragma once

#include <vector>
#include <utility>
#include <boost/shared_ptr.hpp>
#include "utility.h"

class IFilter;

struct ClippingPoint
{
	typedef std::pair<bool, OCPoint> point_t;
	typedef std::vector<point_t> point_array;

	ClippingPoint() : threshold(5) {}

	point_array extractPoints;	//!< 抽出ポイント
	double threshold;			//!< Xデータ許容値
};

struct ClippingFile
{
	// インデックスはそれぞれ 0 はじまり
	// -1以外であればコンボボックスやX/Y軸項目のリストには画面表示時に既に値が入っている
	int fileId;		//!< 使用しているファイルのId(未設定の場合は-1)
	int xItemNo;	//!< 使用しているX項目のインデックス(未設定の場合は-1)
	int yItemNo;	//!< 使用しているY項目のインデックス(未設定の場合は-1)

	boost::shared_ptr<IFilter> filter;
};

class IClipExtractor;
struct ClippingSetting
{
	enum Type {
		Point,	//!< 「自動抽出ポイントの適用」
		File,	//!< 「データポイントの適用」
	};

	// この値を元に画面表示時にどちらのラジオボタンにチェックが入るかを設定します
	Type type;	//!< どちらの設定がアクティブになっているか

	ClippingPoint points;
	ClippingFile file;

	IClipExtractor* extractor;
};

class IClipExtractor
{
public:
	virtual OCPoint* Extract(double threshold, unsigned long* size) = 0;
	virtual void Free(OCPoint* ptr) = 0;
};
