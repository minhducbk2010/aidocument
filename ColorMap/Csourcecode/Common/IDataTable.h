/*!	@file

	@todo 名前
*/
#pragma once

class IDataTable
{
public:
	//!@ 二次元データをセット
	//!@param data	表に挿入する二次元のデータ
	//!@param rows	行数
	//!@param cols	列数
	//! data[rows][cols]
	virtual void SetData(float const** data, long rows, long cols) = 0;

	virtual void AddData(float* data, unsigned long count) = 0;
};