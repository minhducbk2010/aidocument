/*!	@file
	@date	2008-06-06
	@author	wada

	@brief	VARIANTを操作するユーティリティ
*/
#pragma once

#include <atlsafe.h>
#include <vector>

//typedef std::vector<std::vector<float>> Dim2Array;
typedef std::vector<std::vector<_variant_t>> Dim2Array;

template<class T>
inline void GetFloatsImpl(VARIANT const& arr, Dim2Array& ret)
{
	CComSafeArray<T> safeArray;
	safeArray.Attach(arr.parray);

	long index[2] = {};
	for (long j = 0, cnt = safeArray.GetCount(0); j < cnt; ++j) {
		index[0] = j;

		std::vector<_variant_t> item(safeArray.GetCount(1));
		for (size_t i = 0; i < item.size(); ++i) {
			index[1] = i;

			T f; safeArray.MultiDimGetAt(index, f);
			item[i] = f;
		}
		ret.push_back(item);
	}

	safeArray.Detach();
}

//inline Dim2Array GetFloats(VARIANT& arr)
inline void GetVariantArray2D(VARIANT const& arr, Dim2Array& ret)
{
	VARTYPE vt = VARTYPE(arr.vt - VT_ARRAY);
	if (vt == VT_VARIANT) {
		GetFloatsImpl<VARIANT>(arr, ret);
	}
	else if (vt == VT_R8) {
		GetFloatsImpl<double>(arr, ret);
	}
	else if (vt == VT_R4) {
		GetFloatsImpl<float>(arr, ret);
	}
	else if (vt == VT_INT) {
		GetFloatsImpl<int>(arr, ret);
	}
	else if (vt == VT_BSTR) {
		GetFloatsImpl<BSTR>(arr, ret);
	}
	else { _ASSERT(false); }
}

template<class T>
inline std::vector<std::wstring> GetStringsImpl(VARIANT const& arr)
{
	std::vector<std::wstring> ret;

	CComSafeArray<T> safeArray;
	safeArray.Attach(arr.parray);

	for (ULONG i = 0; i < safeArray.GetCount(); ++i) {
		ret.push_back(static_cast<LPCTSTR>(_bstr_t(safeArray.GetAt(i))));
	}

	safeArray.Detach();

	return ret;
}

inline std::vector<std::wstring> GetStrings(VARIANT const& arr)
{
	VARTYPE vt = VARTYPE(arr.vt - VT_ARRAY);
	if (vt == VT_VARIANT) {
		return GetStringsImpl<VARIANT>(arr);
	}
	else if (vt == VT_BSTR) {
		return GetStringsImpl<BSTR>(arr);
	}
	else { _ASSERT(false); }

	return std::vector<std::wstring>();
}
