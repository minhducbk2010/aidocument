/*!	@file
	@date	2008-12-26
	@author	wada

	@brief	Range-based アルゴリズムの実装
*/
#pragma once

#include <boost/range.hpp>

namespace oc
{
	//! std::find の Range-interface
	template<typename Range, class T>
	inline typename boost::range_const_iterator<Range>::type
		find(Range const& r, T const& value)
	{
		typename boost::range_const_iterator<Range>::type begin = boost::begin(r), end = boost::end(r);
		return std::find(begin, end, value);
	}

	//! std::find_if の Range-interface
	template<typename Range, class Predicate>
	inline typename boost::range_const_iterator<Range>::type
		find_if(Range const& r, Predicate pred)
	{
		typename boost::range_const_iterator<Range>::type begin = boost::begin(r), end = boost::end(r);
		return std::find_if(begin, end, pred);
	}

	////! std::find_if の Range-interface
	//template<typename Range, class Predicate>
	//inline typename boost::range_const_iterator<Range>::type
	//	find_if_reversed(Range const& r, Predicate pred)
	//{
	//	typename boost::range_const_reverse_iterator<Range>::type begin = boost::rbegin(r), end = boost::rend(r);
	//	return std::find_if(begin, end, pred);
	//}

	//! std::for_each の Range-interface
	template<typename Range, class Function>
	inline void for_each(Range const& r, Function func)
	{
		/*typename boost::range_const_iterator<Range>::type begin = boost::begin(r), end = boost::end(r);
		return std::find_if(begin, end, pred);*/
		std::for_each(boost::begin(r), boost::end(r), func);
	}

	/*!	@brief	コンテナ(r)にtargetが含まれているか調べます
		@return	targetがあればtrue, そうでなければfalseを返します
	*/
	template<typename Range, class T>
	inline bool include(Range const& r, T const& target)
	{
		typename boost::range_const_iterator<Range>::type end = boost::end(r);
		return find(r, target) != end;
	}


	/*!	@brief
	*/
	template<typename Range, class Predicate>
	inline bool include_if(Range const& r, Predicate const& pred)
	{
		typename boost::range_const_iterator<Range>::type end = boost::end(r);
		return find_if(r, target, pred) != end;
	}

	/*! @brief	コンテナ(r)からtargetを取り除きます
		@return	削除に成功したらtrue, 要素が見つからない場合はfalseを返します
	*/
	template<typename Range, class T>
	inline bool erase(Range& r, T const& target)
	{
		typename boost::range_iterator<Range>::type begin = boost::begin(r), end = boost::end(r);
		typename boost::range_iterator<Range>::type iter = std::find(begin, end, target);
		if (iter != end) {
			r.erase(iter);
			return true;
		}
		return false;
	}

}

