/*!	@file
	@date	2008-09-30
	@author	wada
	
	@brief	O-Chartで使用するカラーコードの定義
*/
#pragma once

//! O-Chartで使用するカラーコードの一覧
static COLORREF DEFAULT_COLORS[] = {
    RGB(0, 0, 0),               // 黒            1
    RGB(0, 0, 255),             // 青            2
    RGB(255, 0, 0),             // 赤            3
    RGB(0, 128, 0),				// グリーン      4
    RGB(255,128,  0),			// オレンジ      5
    0x800000,                   // 紺            6
    0x800080,                   // 紫            7
    RGB(  0,255,  0),			// 黄緑          8
    0xffff00,                   // シアン        9
    RGB(255,128,192),			// ピンク        10
    0xffff,						// 黄            11
    0xff00ff,					// 赤紫          12
    0x808080,                   // グレー        13
    RGB(128, 20,  0),			// 茶色          14
    RGB(255, 255, 255),         // 白            15
    RGB(224,255,255),			// ﾗｲﾄｼｱﾝ        16
    RGB(144,238,144),			// ﾗｲﾄｸﾞﾘｰﾝ      17
    RGB(255,182,193),			// ﾗｲﾄﾋﾟﾝｸ       18
    RGB(255,255,224),			// ﾗｲﾄｲｴﾛｰ       19
    0xc0c0c0,                   // ﾗｲﾄｸﾞﾚｰ       20
    RGB(  0,139,139),			// ﾀﾞｰｸｼｱﾝ       21
    RGB(  0,100,  0),			// ﾀﾞｰｸｸﾞﾘｰﾝ     22
    RGB(165,  0,  0),			// ﾀﾞｰｸﾚｯﾄﾞ      23
    RGB(63, 70, 70),			// ﾀﾞｰｸｸﾞﾚｰ      24
    RGB(211, 216, 225),			// シルバー      25
    RGB(255,215,  0),			// ゴールド      26
    0x8080,						// オリーブ      27
    RGB(154,205, 50),			// ｲｴﾛｰｸﾞﾘｰﾝ     28
    RGB(240,230,140),			// カーキ        29
    RGB(238,130,238),			// スミレ        30
    RGB(255, 99, 71 )			// トマト        31
};

#if defined(_MFC_VER)
class COChartPalette : public CPalette
{
public:
	COChartPalette()
	{
		LOGPALETTE    lp;
		lp.palVersion = 0x300;
		lp.palNumEntries = 1;
		CreatePalette(&lp);

		size_t const colors = _countof(DEFAULT_COLORS);
		ResizePalette(colors);
		PALETTEENTRY  PalEntry[colors];
		for (size_t i = 0; i < colors; ++i) {
			PalEntry[i].peFlags = 0;
			PalEntry[i].peRed = GetRValue(DEFAULT_COLORS[i]);
			PalEntry[i].peGreen = GetGValue(DEFAULT_COLORS[i]);
			PalEntry[i].peBlue = GetBValue(DEFAULT_COLORS[i]);
		}
		SetPaletteEntries(0, colors, PalEntry);
	}
};
#endif