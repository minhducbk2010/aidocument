/*!	@file
	@brief	ツールチップが表示できるリストボックス
*/
#pragma once

namespace
{

class CToolTipListBox : public CBCGPListBox
{
	DECLARE_DYNAMIC(CToolTipListBox)
public:
	CToolTipListBox()
		: CBCGPListBox()
	{
	}

	void ToolTipsActivate(bool activate)
	{
		m_tooltip.Activate(activate);
	}

	void InitToolTips()
	{
		EnableToolTips(TRUE);
		m_tooltip.Create(this, TTS_ALWAYSTIP);
		m_tooltip.AddTool(this, _T("--"));
		ToolTipsActivate(TRUE);
	}

protected:
	virtual BOOL PreTranslateMessage(MSG* pMsg)
	{
		m_tooltip.RelayEvent(pMsg);
		return __super::PreTranslateMessage(pMsg);
	}

private:
	DECLARE_MESSAGE_MAP()
	void OnMouseMove(UINT nFlags, CPoint point)
	{
		BOOL out;
		int index = static_cast<int>(ItemFromPoint(point, out));

		CString str = _T("");
		if (!out) {
			GetText(index, str);
		}
		else {
			index = -1;
		}
		
		if (m_oldIndex != index) {
			m_tooltip.UpdateTipText(str, this);
		}
		m_oldIndex = index;
	}

private:
	CToolTipCtrl m_tooltip;
	int m_oldIndex;
};

IMPLEMENT_DYNAMIC(CToolTipListBox, CBCGPListBox)

BEGIN_MESSAGE_MAP(CToolTipListBox, CBCGPListBox)
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


}