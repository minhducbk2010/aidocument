#pragma once

// O-Chartプロジェクト外でのライセンスチェック用

enum License
{
	Basic,
	Standard,
	Professional,
	Viewer = -1,
};