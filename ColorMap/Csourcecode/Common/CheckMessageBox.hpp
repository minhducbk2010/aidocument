/*!	@file
	@date	2013-05-07
	@author	shimuray

	@brief	チェック付きメッセージボックス表示関数
*/

#pragma once

#include <tchar.h>

//! チェック付きメッセージを表示するときに使う(SendMessageで引数に使う)
struct CHECK_MESSAGE
{
	UINT* checkStatus; // チェック状態(0:OFF)
	TCHAR const* msg;
	TCHAR const* checkMsg;
};


