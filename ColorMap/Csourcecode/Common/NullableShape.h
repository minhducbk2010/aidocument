/*!	@file
	@brief	なにもしないオブジェクト
*/
#pragma once

#include "IShape.h"
#include "IGroup.h"
#include "property.h"

class NullableShape : public IShape
{
public:
	NullableShape() : m_canvas(0), m_parent(0), m_lock(false), m_posLock(false)
	{}

// IShape Interface
	virtual int GetId() const { return -1; }
	virtual void SetId(int id) { id; }

	virtual IMessageReceiver* GetOwner() const { return nullptr; }
	virtual void ChangeOwner(IMessageReceiver* owner) { owner; }
	virtual HWND GetParentWindow() const { return 0; }

	virtual void SetParent(IGroup* parent) { m_parent = parent; }
	virtual IGroup* GetParent() const { return m_parent; }

	virtual void GetName(wchar_t* name, size_t length) const {
		if (length) {
			name[0] = '\0';
		}
	}

	virtual unsigned long GetTypeId() const { return 0; }
	virtual bool IsNameEditable() const { return false; }
	virtual bool IsDeletable() const { return false; }

	virtual bool IsChainCreate() const { return false; }
	virtual bool IsDraggingCreate() const { return false; }

	virtual void SetCanvas(ICanvas* canvas) { m_canvas = canvas; }
	virtual ICanvas* GetCanvas() const { return m_canvas; }

	virtual void SetAngle(int angle) { angle; }
	virtual int GetAngle() const { return 0; }

	void Lock(bool lock) { m_lock = lock; }
	bool IsLock() const {
		if (IShape const* p = dynamic_cast<IShape const*>(m_parent)) {
			if (p->IsLock()) { return true; }
		}
		return m_lock;
	}

	virtual bool IsVisible() const { return false; }
	virtual bool IsEffective() const  { return true; }

	virtual void PositionLock(bool lock) { m_posLock = lock; }
	virtual bool IsPositionLock() const {
		if (IShape const* p = dynamic_cast<IShape const*>(m_parent)) {
			if (p->IsPositionLock()) { return true; }
		}
		return m_posLock;
	}

	virtual void Offset(double xOffset, double yOffset) { xOffset, yOffset; }
	virtual void Move(double /*left*/, double /*top*/) { }

	virtual void Draw() {}
	virtual void DrawSelected() {}
	virtual void DrawBlink() {}

	virtual int HitTest(double /*x*/, double /*y*/) const { return 0; }
	virtual bool Contains(double /*left*/, double /*top*/, double /*right*/, double /*bottom*/) const { return false; }

	virtual void MouseDown(unsigned long /*flags*/, double /*x*/, double /*y*/) { }
	virtual void MouseMove(unsigned long /*flags*/, double /*x*/, double /*y*/) { }
	virtual void MouseUp(unsigned long /*flags*/, double /*x*/, double /*y*/) { }

	virtual bool KeyDown(unsigned long key, unsigned short repeat, unsigned long flags) { key, repeat, flags; return false; }

	virtual void BeginDrag(int type, double x, double y) { type, x, y; }
	virtual void ParentBeginDrag(int type, double x, double y) { type, x, y; }
	virtual void Drag(double x, double y) { x, y; }
	virtual long EndDrag(double x, double y) { x, y; return 0; }
	virtual void DoubleClick(double x, double y) { x, y; }

	virtual int SaveState() { return -1; }
	virtual void RestoreState(int state) { state; }
	virtual void ClearState() {}

	virtual void GetRect(double* x, double* y, double* right, double* bottom) const {
		*x = *y = *right = *bottom = 0;
	}
	virtual void GetRotatedRect(double* x, double* y, double* right, double* bottom) const {
		*x = *y = *right = *bottom = 0;
	}
	virtual void SetRect(double left, double top, double right, double bottom) { left, top, right, bottom; }
	virtual void GetStartPoint(double* x, double* y) const { x, y; }
	virtual void GetStopPoint(double* x, double* y) const { x, y; }

	virtual LPTSTR GetCursor(double x, double y) const { x, y; return nullptr; }
	virtual long GetToolHint(wchar_t* text, long length) const { text, length; return -1; }

	virtual void SetSelected(bool select) { m_selected = select; }
	virtual bool GetSelected() const { return m_selected; }

	virtual void InvertLR() {}
	virtual void InvertTB() {}

	virtual void DeleteCustomLines(int const* deleteLineIndexes, int count) { deleteLineIndexes, count; }

	virtual bool IsCondFormatEditable() const { return false; }

	virtual void Serialize(ISerializer* ar) { ar; }

	virtual void CopyFrom(IShape const* source) { source; }
	virtual IShape* Clone(IMessageReceiver* owner) /*const*/ { owner; return nullptr; }

	// IProperty
	virtual PropertyNode* CreateProperty() { return 0; }
	virtual PropertyNode* CreateTabProperty() { return 0; }
	virtual void ReleaseProperty(PropertyNode* prop) { delete prop; }
	virtual bool SetProperty(long /*id*/, LPARAM /*value*/, LPARAM* /*source*/) { return false; }
	virtual bool GetProperty(long /*id*/, LPARAM* /*ret*/) const { return false; }

protected:
	ICanvas* m_canvas;
	IGroup* m_parent;
	bool m_lock;
	bool m_posLock;
	bool m_selected;
};