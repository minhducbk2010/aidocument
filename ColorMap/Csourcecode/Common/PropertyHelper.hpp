/*!	@file
	@date	2008-09-08
	@author	wada

	@brief	プロパティのテンプレート
*/
#include "PropertyCreator.hpp"
#include "PenStyles.h"

#include "IMessageReceiver.h"
#include "Message.h"

#pragma once

/////////// test //////////////////////////////////////////////////////////////

const int PAPER_MARGIN_MM = 0;

static void CreatePlacementProperty(IMessageReceiver* owner, OCProperty& prop, OCRect const& rect, int angle, bool readOnly, bool width_height_enable = true)
{
	IPropertyGroup*  group = prop.CreateGroup(::GetResourceString(owner, "ID_SOURCE_TEXT_PLACEMENT"));
	group->AddProperty(::GetResourceString(owner, "ID_SOURCE_TEXT_X_COORDINATE_MM"), PI_ALIGN_X, rect.left - PAPER_MARGIN_MM, readOnly);
	group->AddProperty(::GetResourceString(owner, "ID_SOURCE_TEXT_Y_COORDINATE_MM"), PI_ALIGN_Y, rect.top - PAPER_MARGIN_MM, readOnly);
	group->AddProperty(::GetResourceString(owner, "ID_SOURCE_TEXT_WIDTH_MM"), PI_ALIGN_WIDTH, rect.Width(), readOnly || !width_height_enable);
	group->AddProperty(::GetResourceString(owner, "ID_SOURCE_TEXT_HEIGHT_MM"), PI_ALIGN_HEIGHT, rect.Height(), readOnly || !width_height_enable);
	group->AddSpinProperty(::GetResourceString(owner, "ID_SOURCE_TEXT_ANGLE_DO"), PI_ANGLE, angle, -180, 180, readOnly);
}

static void CreatePlacementProperty(IMessageReceiver* owner, OCProperty& prop, OCRect const& rect, bool readOnly, bool width_height_enable = true)
{
	IPropertyGroup* group = prop.CreateGroup(::GetResourceString(owner, "ID_SOURCE_TEXT_PLACEMENT"));
	group->AddProperty(::GetResourceString(owner, "ID_SOURCE_TEXT_X_COORDINATE_MM"), PI_ALIGN_X, rect.left - PAPER_MARGIN_MM, readOnly);
	group->AddProperty(::GetResourceString(owner, "ID_SOURCE_TEXT_Y_COORDINATE_MM"), PI_ALIGN_Y, rect.top - PAPER_MARGIN_MM, readOnly);
	group->AddProperty(::GetResourceString(owner, "ID_SOURCE_TEXT_WIDTH_MM"), PI_ALIGN_WIDTH, rect.Width(), readOnly || !width_height_enable);
	group->AddProperty(::GetResourceString(owner, "ID_SOURCE_TEXT_HEIGHT_MM"), PI_ALIGN_HEIGHT, rect.Height(), readOnly || !width_height_enable);
}

//タブ版  配置項目作成(角度あり)
static void CreatePlacementTabProperty(IMessageReceiver* owner, IPropertyGroup* group, OCRect const& rect, int angle, bool readOnly, bool width_height_enable = true)
{
	IPropertyGroup* parent;
	
	parent=group->AddGroup(::GetResourceString(owner, "ID_SOURCE_TEXT_PLACEMENT"));
	parent->AddProperty(::GetResourceString(owner, "ID_SOURCE_TEXT_X_COORDINATE_MM"), PI_ALIGN_X, rect.left - PAPER_MARGIN_MM, readOnly);
	parent->AddProperty(::GetResourceString(owner, "ID_SOURCE_TEXT_Y_COORDINATE_MM"), PI_ALIGN_Y, rect.top - PAPER_MARGIN_MM, readOnly);
	parent->AddProperty(::GetResourceString(owner, "ID_SOURCE_TEXT_WIDTH_MM"), PI_ALIGN_WIDTH, rect.Width(), readOnly || !width_height_enable);
	parent->AddProperty(::GetResourceString(owner, "ID_SOURCE_TEXT_HEIGHT_MM"), PI_ALIGN_HEIGHT, rect.Height(), readOnly || !width_height_enable);
	parent->AddSpinProperty(::GetResourceString(owner, "ID_SOURCE_TEXT_ANGLE_DO"), PI_ANGLE, angle, -180, 180, readOnly);
}

//タブ版　配置項目作成
static void CreatePlacementTabProperty(IMessageReceiver* owner, IPropertyGroup* group, OCRect const& rect, bool readOnly, bool width_height_enable = true)
{
	IPropertyGroup* parent;

	parent=group->AddGroup(::GetResourceString(owner, "ID_SOURCE_TEXT_PLACEMENT"));
	parent->AddProperty(::GetResourceString(owner, "ID_SOURCE_TEXT_X_COORDINATE_MM"), PI_ALIGN_X, rect.left - PAPER_MARGIN_MM, readOnly);
	parent->AddProperty(::GetResourceString(owner, "ID_SOURCE_TEXT_Y_COORDINATE_MM"), PI_ALIGN_Y, rect.top - PAPER_MARGIN_MM, readOnly);
	parent->AddProperty(::GetResourceString(owner, "ID_SOURCE_TEXT_WIDTH_MM"), PI_ALIGN_WIDTH, rect.Width(), readOnly || !width_height_enable);
	parent->AddProperty(::GetResourceString(owner, "ID_SOURCE_TEXT_HEIGHT_MM"), PI_ALIGN_HEIGHT, rect.Height(), readOnly || !width_height_enable);
}

template<class T>
static IPropertyGroup* CreateLineProperty(IMessageReceiver* owner, T* parent, bool lock, int lineStyle, COLORREF lineColor, double lineWidth, int roundStyle, ICanvas* canvas)
{
	IPropertyGroup* group = parent->AddGroup(::GetResourceString(owner, "ID_SOURCE_TEXT_LINE"));
	MakeLineProperty(owner, group, canvas, lock, lineStyle);
	/*if (m_lineStyle != PS_NULL)*/ {
		group->AddColorProperty(::GetResourceString(owner, "ID_SOURCE_TEXT_COLOR_KANJI"), PI_LINE_COLOR, lineColor, lock || lineStyle == PS_NULL);
		group->AddSelectorPropertyR(::GetResourceString(owner, "ID_SOURCE_TEXT_THICKNESS_PT"), PI_LINE_WIDTH, lock || lineStyle == PS_NULL,
			lineWidth, 9, 0.25, 0.5, 0.75, 1.0, 1.5, 2.25, 3.0, 4.5, 6.0);

		std::wstring corner1 = ::GetResourceString(owner,"ID_SOURCE_TEXT_ROUND");
		std::wstring corner2 = ::GetResourceString(owner,"ID_SOURCE_TEXT_SQUARE");
		group->AddArrayProperty(::GetResourceString(owner, "ID_SOURCE_TEXT_SHAPE_OF_CORNER"), PI_LINE_ROUND, roundStyle, lock || lineStyle == PS_NULL,
			2, corner1.c_str(), corner2.c_str());
	}
	return group;
}

// PI_ALIGN_X, PI_ALIGN_Y
inline bool SetPlacePropertyHelper(double& target, LPARAM value, LPARAM* source)
{
	double v = *reinterpret_cast<double*>(value);
	if (v < -1000 || v > 1000) { return false; }

	if (source) { *(double*)source = target; }
	target = v + PAPER_MARGIN_MM;;
	return true;
}

template<class Canvas>
void MakeLineProperty(IMessageReceiver* owner, IPropertyGroup* group, Canvas* canvas, bool locked, int currentStyle, int id = PI_LINE_STYLE)
{
	std::vector<std::wstring> const styles = GetPenStyles(canvas, owner);
	// 線種設定画面で線種が消されて無くなることがあるので、
	// その場合は「なし」を選んでいることにする
	if ((long)styles.size() <= currentStyle) {
		currentStyle = PS_NULL;
	}
	group->AddArrayProperty(::GetResourceString(owner, "ID_SOURCE_TEXT_STYLE"), id, locked, currentStyle, styles);
}
