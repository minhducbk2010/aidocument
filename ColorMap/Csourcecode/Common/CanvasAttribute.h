/*!	@file
	@date	2008-01-21
	@author	wada
	
	@brief	描画設定
	プラグインで使用する
*/
#pragma once

class ICanvas;

#include "utility.h" //!@todo OCPointは別ファイルに移行する
#include "Property.h"
#include "PropertyCreator.hpp"

namespace FitType {
	enum FitType
	{
		None,		//!< 無効
		LeftTop,	//!< 左上
		All			//!< 四隅
	};
}

/*!	全オブジェクトが持つ設定の定義
	このクラスの型は全てプリミティブな型にすること
*/
struct OCCanvasAttribute
{
	OCCanvasAttribute()
		: m_canvas(0), m_dragging(false), m_selected(false)
		, m_lineColor(RGB(0, 0, 0)), m_fillColor(RGB(255, 255, 255))
		, m_lineRoundType(0/*RT_ELLIPSE*/)
		, m_visible(true), m_lineStyle(Gdiplus::DashStyleSolid)
		, m_fillStyle(1/*FS_SOLID*/), m_lineWidth(1), m_angle(0)
		, m_gradation(false), m_gradColor(0xff0000), m_gradDirection(0)
		, m_transparency(0)
		, m_lock(false), m_posLock(false), m_fitType(FitType::All)
		, m_shadow(false), m_shadowOffset(2), m_shadowColor(RGB(128, 128, 128)), m_shadowBlur(0/*2*/), m_shadowTransparent(50)
		, m_shadowOffsetRotated(2, 2)
	{
	}

protected:
	ICanvas* m_canvas;

	OCPoint m_clickPoint;
	int m_hitType;
	bool m_dragging;
	bool m_selected;

	std::wstring m_name;
	bool m_visible;

	int m_angle;
	bool m_lock;
	bool m_posLock;
	FitType::FitType m_fitType;

	int m_lineStyle;
	float m_lineWidth;
	COLORREF m_lineColor;
	int m_lineRoundType;

	int m_fillStyle;
	COLORREF m_fillColor;
	bool m_gradation;
	COLORREF m_gradColor;
	int m_gradDirection;
	int m_transparency;

	bool m_shadow;
	int/*double*/ m_shadowBlur;
	int/*double*/ m_shadowOffset;
	OCPoint m_shadowOffsetRotated;
	COLORREF m_shadowColor;
	int m_shadowTransparent;

	friend class OCSerializer;
};