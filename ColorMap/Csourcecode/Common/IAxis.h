/*!	@file
	@date	2008-08-26
	@author	wada
*/
#pragma once

#include "AxisEnum.h"

struct OCRect;
class IRichText;

class IAxis
{
public:
	virtual ~IAxis() = 0 {}

	//virtual int GetId() const = 0;

	virtual TCHAR const* GetName() const = 0;
	virtual TCHAR const* GetLabel() const = 0;
	virtual IRichText* GetAxisLabel() const = 0;

	virtual OCRect const& GetRect() const = 0;

	virtual double GetMaximum() const = 0;
	virtual double GetMinimum() const = 0;
	virtual bool GetSecondUse() const = 0;
	virtual double GetSecondValue() const = 0;
	virtual double GetInterval() const = 0;
	virtual double GetSubInterval() const = 0;

	virtual bool IsSubTickVisible() const = 0;
	virtual COLORREF GetTickLabelColor() const = 0;

    virtual void SetMinMax(double min, double max) = 0;
	virtual void SetSecondUse(bool use) = 0;
	virtual int SetSecondValue(double second) = 0;
	virtual int SetInterval(double interval) = 0;
	virtual int SetSubInterval(double subInterval) = 0;

	virtual double GetDBStd() const = 0;
	virtual double GetDBExp() const = 0;

	virtual void AutoScale() = 0;
	virtual void AutoLabel() = 0;

	virtual ScaleType GetScaleType() const = 0;

	virtual bool GetGridVisible() const = 0;
	virtual COLORREF GetGridColor() const = 0;
	virtual int GetGridStyle() const = 0;

	virtual bool IsBuildUp() const = 0;
	virtual bool IsOffset() const = 0;
	virtual bool IsReverse() const = 0;
	virtual BOOL IsLabelEditing() const = 0;
};