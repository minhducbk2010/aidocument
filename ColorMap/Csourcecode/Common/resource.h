//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by EdgePropertyDialog.rc
//
#define IDD_INPLACE_EDITOR              2001
#define IDC_INPLACE_RICHEDIT            2003
#define IDR_INPLACE_EDIT_POPUP_MENU     2013
#define IDC_INPLACE_RICHEDIT_INVISIBLE  2014
#define IDD_SETTINGDIALOG               2500
#define IDC_START_SHAPE                 2501
#define IDC_START_SIZE                  2502
#define IDC_START_ANGLE                 2503
#define IDC_END_SHAPE                   2504
#define IDC_END_SIZE                    2505
#define IDC_END_ANGLE                   2506
#define IDC_APPLICATION                 2507
#define IDC_PREVIEW_PICT                2509

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
