/*!	@file
	@date	2008-07-18
	@brief	セルの設定
*/

/*	仕様
	* 罫線設定ダイアログ
		罫線設定ダイアログは RuleSetting の SetterとGetter を持つ。
		アプリケーションは現在の設定をRuleSettingに格納し、ダイアログのSetメソッドに渡す。
		ダイアログが閉じた後、OKボタンが押されていたらGetメソッドを使用し、最新の情報を取得する。

		また外部仕様にある「セルが複数選択されているかどうか」は、Setterの第二引数に渡す。
		例えば:
			罫線設定ダイアログ::SetSetting(RuleSetting const&, CellSelectState select)
		とし、第二引数の値に応じて中央のボタンのEnableを決定する。

		プレビューボタンが押されたら GetParent()->SendMessage(PI_MSG_APPLY_RULE) を呼び出す。

	* 書式設定ダイアログ
		罫線設定と同様に FormatSetting の Setter と Getter を持つ。
		プレビューボタンが押されたら GetParent()->SendMessage(PI_MSG_APPLY_FORMAT) を呼び出す。
*/
#pragma once
#include <string>

// 仮)
enum {
	PI_MSG_APPLY_RULE = 1,
	PI_MSG_APPLY_FORMAT = 2
};

//! セルの選択状態
/*
    cssSingle:
        セルがひとつだけ選ばれている。
        m_vertMiddle,m_horzMiddleに対応するボタンは選択できない。プリセットの「内枠」も無効。
        また、斜めの線も表示しない。
    cssHorizontal:
        セルが横方向に複数選ばれている。m_vertMiddleに対応するボタンは設定できない。
        斜めの線は添付画像の cssHorizontal.bmp のようなイメージで出る。
        プリセットの「内枠」は有効。
    cssVertical:
        セルが縦方向に複数選ばれている。m_horzMiddleに対応するボタンは選択できない。
        斜めの線は添付画像の cssVertical.bmp のようなイメージで出る。
        プリセットの「内枠」は有効。
    cssMulti:
        縦横に複数選択されている。全てのボタンが有効で、斜めの線は cssMulti.bmp のようなイメージで出る。
*/
enum CellSelectState
{
    cssSingle,      //!< 単一選択
    cssHorizontal,  //!< 横に複数選択
    cssVertical,    //!< 縦に複数選択
    cssMulti,       //!< 縦横に複数選択
};

//! 罫線の設定
struct RuleSetting
{
	//! 線の設定
	struct Line
	{
		BOOL diff;		//!< 領域選択時この罫線は異なった設定になっている
		int style;		//!< 線種
		COLORREF color;	//!< 線の色
		float width;	//!< 線の太さ(pt)

		Line() : diff(FALSE), style(PS_NULL), color(0), width(1.0f) {}
		Line(int s, COLORREF c, float w) : diff(FALSE), style(s), color(c), width(w) {}
		void ApplyValue(const Line &value)
		{
			if( !value.diff ){
				*this = value;
			}
		}
	};

	Line m_top;			//!< 上の線
	Line m_vertMiddle;	//!< 縦方向の中央線
	Line m_bottom;		//!< 下の線
	Line m_left;		//!< 左の線
	Line m_right;		//!< 右の線
	Line m_horzMiddle;	//!< 横方向の中央線
	Line m_skewL;		//!< 左斜め
	Line m_skewR;		//!< 右斜め

	void ApplyValue(const RuleSetting &value)
	{
		// 個々のセルでは、縦方向の中央線、横方向の中央線は適用しない
		m_top.ApplyValue(value.m_top);
		m_bottom.ApplyValue(value.m_bottom);
		m_left.ApplyValue(value.m_left);
		m_right.ApplyValue(value.m_right);
		m_skewL.ApplyValue(value.m_skewL);
		m_skewR.ApplyValue(value.m_skewR);
	}
};

//! 書式設定
struct FormatSetting
{
	//LOGFONT
	std::wstring font;	//!< フォント名
	int fontSize;		//!< フォントサイズ
	COLORREF color;		//!< フォントの色

	int horzPos;		//!< 横位置(0:左揃え, 1:中央揃え, 2:右揃え)
	int vertPos;		//!< 縦位置(0:上揃え, 1:中央揃え, 2:下揃え)
	int lineSpace;		//!< 行間

	int topMargin;		//!< 余白(上)
	int bottomMargin;	//!< 余白(下)
	int leftMargin;		//!< 余白(左)
	int rightMargin;	//!< 余白(右)
};