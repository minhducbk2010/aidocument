/*!	@file
	@date	2008-09-09
	@author	wada

	@brief	シリアライズを行うインターフェイスの宣言
*/
#pragma once

#include <string>
#include <comutil.h>
#include <boost/preprocessor/seq/for_each.hpp>
#include "ICanvas.h" // FILLSTYLE

class IPlotData;

//! シリアル化に対応している型の一覧
/*!
	このリストにはPOD型のみを登録すること
	std::stringなどを使用するとEXE側で取得したメモリをDLLで解放するなどリークの原因となる
	文字列(string)のシリアライズには SerializeString 関数を使用する
*/
#define TYPE_SEQ (bool)(int)(short)(unsigned short)(unsigned char)(size_t)(COLORREF)(long)(double)(float)(FILLSTYLE)

//!@class シリアル化を行うクラス
class ISerializer
{
public:
	virtual ~ISerializer() = 0 {}

	//! ノード名称を取得します
	virtual TCHAR const* NodeName() const = 0;

	//! 子ノードを取得します
	virtual ISerializer* ChildNode(TCHAR const* name) = 0;

	/*
		以下のメソッドを自動生成している
		virtual void Serialize(TCHAR const* name, type& value, type const& defaultValue) = 0;
		typeにはTYPE_SEQで定義している型が入る
	*/

	#define MAKE_SERIALIZER_VIRTUAL_METHOD(r, data, elem) \
	virtual void Serialize(TCHAR const* name, elem& value, elem const& defaultValue = elem()) = 0;

	BOOST_PP_SEQ_FOR_EACH(MAKE_SERIALIZER_VIRTUAL_METHOD, _, TYPE_SEQ);

	virtual void Serialize(TCHAR const* name, IPlotData*& data) = 0;
	virtual void SerializeEmptyPlotData(TCHAR const* name, IPlotData*& data) = 0;
	virtual void Serialize(TCHAR const* name, GUID& guid) = 0;
	virtual void Serialize(TCHAR const* name, OCRect& rect) = 0;
	virtual void Serialize(TCHAR const* name, OCPoint& rect) = 0;
	virtual void Serialize(TCHAR const* name, LOGFONT& lf) = 0;

	virtual bool IsTemplate() const = 0;

protected:
	friend void SerializeString(ISerializer* ar, TCHAR const* name, std::string& value, std::string const& def);
	friend void SerializeString(ISerializer* ar, TCHAR const* name, std::wstring& value, std::wstring const& def);

	// write only
	virtual void Serialize(TCHAR const* name, std::string& value) = 0;
	virtual void Serialize(TCHAR const* name, std::wstring& value) = 0;
};

//! 読み込み時に使用するストリーム
class IReadable : public ISerializer
{
public:
	typedef void* ( * Allocator)(size_t size);
	typedef void ( * Deallocator)(void* ptr);

public:
	enum State
	{
		ProjectRead,
		Copy,
	};

	//!@{プロジェクトファイル内のバージョンを取得します
	//! メジャーバージョンを取得します
	virtual int GetMajorVersion() const = 0;
	//! マイナーバージョンを取得します
	virtual int GetMinorVersion() const = 0;
	//! リリースバージョンを取得します
	virtual int GetReleaseVersion() const = 0;
	//! ビルドバージョンを取得します
	virtual int GetBuildVersion() const = 0;
	//! 現在の状態を取得します
	virtual State GetState() const = 0;
	//@}

	virtual _variant_t GetValue() const = 0;

	virtual size_t ChildCount() const = 0;
	virtual ISerializer* ChildNode(size_t index) = 0;
	using ISerializer::ChildNode;

	virtual char* SerializeStringA(TCHAR const* name, Allocator alloc) = 0;
	virtual wchar_t* SerializeStringW(TCHAR const* name, Allocator alloc) = 0;
};

// ユーティリティマクロ

//! enum型のシリアライズを行う際に使用します
#define MAKE_ENUM_SERIALIZE(ar, name, value, type) \
{\
	int tmp = value; \
	ar->Serialize(name, tmp, tmp); \
	value = static_cast<type>(tmp); \
}

//! type safe enum型のシリアライズを行う際に使用します
#define MAKE_SENUM_SERIALIZE(ar, name, value, enumtype) \
{\
	int tmp = value.underlying(); \
	ar->Serialize(name, tmp, tmp); \
	value = (enumtype::type)tmp; \
}

//! enum型のシリアライズを行う際に使用します
/*!
	デフォルト値を設定したい場合はこちらを使用します
*/
#define MAKE_ENUM_SERIALIZE2(ar, name, value, type, def) \
{\
	int tmp = value; \
	ar->Serialize(name, tmp, def); \
	value = static_cast<type>(tmp); \
}

//! 文字列のシリアライズを行います
/*!
	@param[in]	ar		シリアライズを実行するオブジェクト
	@param[in]	name	要素の名称
	@param[in]	value	要素の値
	@param[in]	def		要素のデフォルト値(読み取り時のみ使用します)
*/
inline void SerializeString(ISerializer* ar, TCHAR const* name, std::string& value, std::string const& def = std::string())
{
	if (IReadable* reader = dynamic_cast<IReadable*>(ar)) {
		char* buf = reader->SerializeStringA(name, malloc);
		value = buf == 0 ? def : buf;
		free(buf);
	}
	else {
		ar->Serialize(name, value);
	}
}

//! 文字列のシリアライズを行います
/*!
	@param[in]	ar		シリアライズを実行するオブジェクト
	@param[in]	name	要素の名称
	@param[in]	value	要素の値
	@param[in]	def		要素のデフォルト値(読み取り時のみ使用します)
*/
inline void SerializeString(ISerializer* ar, TCHAR const* name, std::wstring& value, std::wstring const& def = std::wstring())
{
	if (IReadable* reader = dynamic_cast<IReadable*>(ar)) {
		wchar_t* buf = reader->SerializeStringW(name, malloc);
		value = buf == 0 ? def : buf;
		free(buf);
	}
	else {
		ar->Serialize(name, value);
	}
}

inline bool IsReadable(ISerializer* ar)
{
	return dynamic_cast<IReadable*>(ar) != 0;
}
