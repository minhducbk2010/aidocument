/*!	@file
	@date	2008-03-11
	@author	wada

	@brief	平面プロットの描画インターフェイス
*/
#pragma once

#import <msxml3.dll>
#include <boost/scoped_array.hpp>
#include "utility.h"

class IPlotData;
class IPropertyCreator;
class OCProperty;
class ICanvas;
class IMessageReceiver;
class ISerializer;
class IPropertyGroup;



#pragma pack(push, 1)
struct DrawArgs
{
	double xMax, xMin;
	double yMax, yMin;
	BOOL xReverse;
	BOOL yReverse;
	BOOL xClipping;
	BOOL yClipping;
	DrawArgs() : xReverse(FALSE), yReverse(FALSE) {}

	double XNormalize(double value) const
	{
		double v = (value - xMin) / (xMax - xMin);
		return xReverse ? 1 - v : v;
	}

	double YNormalize(double value) const
	{
		double v = (value - yMin) / (yMax - yMin);
		return yReverse ? 1 - v : v;
	}
};
#pragma pack(pop)

//!@todo pure virtual function
class IPlotDrawer
{
public:
	virtual void Destroy() = 0;

	virtual BOOL IsVisible() const = 0;
	virtual void Visible(BOOL visible) = 0;

	virtual void SetSelected(BOOL selected) = 0;

	virtual bool HitTest(OCRect const& rect, DrawArgs const& arg, double x, double y) = 0;

	virtual void SetData(IPlotData* data) = 0;
	virtual IPlotData* GetData() const = 0;

	virtual void Update(bool silent) = 0;
	virtual void AfterSerializeUpdate() = 0;

	virtual void Draw(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const = 0;
	virtual void DrawSelected(ICanvas* g, OCRect const& rect, DrawArgs const& arg) const = 0;
	virtual void DrawLegend(ICanvas* g, OCRect const& rect, int ratio) const = 0;
	virtual void DrawBlink(ICanvas* g, OCRect const& rect, DrawArgs const& arg) = 0;

	virtual void EnableOnlineMode(bool enable) = 0;
	virtual double GetYPosition(double x, OCRect const& rect, DrawArgs const& arg) const = 0;

	virtual void GetPosition(size_t index, double* x, double* y, OCRect const& rect, DrawArgs const& arg) const = 0;

	virtual void GetXMinMax(float* min, float* max) const = 0;
	virtual void GetYMinMax(float* min, float* max) const = 0;
	virtual void GetZMinMax(float* min, float* max) const = 0;
	virtual void AddProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode) = 0;
	virtual void AddTabProperty(IPropertyCreator* prop, ICanvas* g, bool plotMode) = 0;
	virtual bool SetProperty(long id, LPARAM value, LPARAM* source) = 0;
	virtual bool GetProperty(long id, LPARAM* ret) const = 0;

	virtual void Serialize(ISerializer* ar) = 0;
	virtual void AfterSerialize() = 0;

	virtual void CopyFrom(IPlotDrawer const* src) = 0;
};

class IBuildUpPlot
{
public:
	virtual void AddPlot(IPlotDrawer* plot) = 0;
};
