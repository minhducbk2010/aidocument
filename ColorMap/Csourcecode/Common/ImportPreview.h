/*!	@file
	@author	wada
	@date	2008-02-29

	@brief	インポート時のプレビューに使用する構造体の定義
*/
#pragma once

struct OCPreviewData
{
	//OCPreviewData(wchar_t const* aName, wchar_t const* aValue) 
	//	: next(0)
	//{
	//	::wcscpy_s(name, 128, aName);
	//	::wcscpy_s(value, 128, aValue);
	//}
//	~OCPreviewData() { delete next; }

	wchar_t name[128];
	wchar_t value[128];

	OCPreviewData* next;
};

struct OCImportParam
{
	long structSize;
	long itemRow;	//! 項目のある行 0始まり(ファイルの1行目が0)。ただし、-1は項目名無しを意味する。
	long unitRow;	//! 単位のある行 0始まり(ファイルの1行目が0)。ただし、-1は単位名無しを意味する。
	long dataRow;	//! データ開始行 0始まり(ファイルの1行目が0)。ただし、-1はデータ無しを意味する。

	//!@todo これは別の場所に移す
	bool colormap;

	// 区切り文字、引用符文字
	wchar_t separator;
	wchar_t quote;

	// ステップ設定
	bool useStep;
	double stepStart;
	double stepInterval;

	// ImportSetting.xmlファイルへのフルパスファイル名
	const wchar_t *pwszImportSettingFilename;

    // Excelのみ。シート番号(0〜)
    short sheetNo;
};