/*!	@file
	@brief	線分のインターフェイス
*/
#pragma once

class ILine
{
public:
	virtual void GetEdgeOffset(double* x, double* y) const = 0;
	virtual void GetRectEdgeInclude(double* left, double* top, double* right, double* bottom) const = 0;
};