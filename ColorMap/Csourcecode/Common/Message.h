/*!	@file
	@breif	メッセージIDの定義
*/
#pragma once

#include <string>
#include "CheckMessageBox.hpp"

enum {
	//! 外部アプリから送られる可能性のあるメッセージは、
	//! WM_APP + 0x050以降で固定し、設定後は絶対に動かしてはいけない
	PI_MSG_IS_STARTING = WM_APP + 0x050, // 起動処理中か
	PI_MSG_CAN_API_CONNECT, // APIサーバが起動しているか
	PI_MSG_IS_MAINWINDOW, // メインウィンドウかどうか
	PI_MSG_GET_DOCUMENT_POINTER, // docへのポインタ要求(Toolbox C++コードのリソース取得に使用しているので固定)
	PI_MSG_GET_RESOURCE_STRING, // リソース文字列取得(Toolbox C++コードのリソース取得に使用しているので固定)
	PI_MSG_GET_MAINWINDOW, // メインウィンドウのハンドル要求(Toolbox C++コードのリソース取得に使用しているので固定)

	//! 固定メッセージここまで

	PI_MSG_CREATE_GRAPH = WM_APP + 0x100,
	PI_MSG_ADJUST_GRAPH,
	PI_MSG_UPDATE_GRAPH,
    PI_MSG_UPDATE_GRAPH_FROM_PLOT,
	PI_MSG_UPDATE_DATABASE,
    PI_MSG_GET_FILEDATA,
	PI_MSG_GET_FILEDATA_STR,
    PI_MSG_GET_FILE,
	PI_MSG_GET_FILE_FROMID,
	PI_MSG_CREATE_PLOTDATA,
    PI_MSG_FIT,
	PI_MSG_GET_GRID_GAP,
	PI_MSG_SHOW_TEXT_TOOLBAR,
	PI_MSG_TEXT_TOOLBAR_UPDATE,
	PI_MSG_SHOW_3D_TOOLBAR,
	PI_MSG_SET_PROP_VALUE,
	PI_MSG_SET_PROP_ITEMS,
	PI_MSG_UPDATE_FILTER,
	PI_MSG_GET_COMMENT_VISIBLE,
	PI_MSG_CLOSE_DATAWINDOW,

    PI_MSG_GET_THUMBNAIL,
	PI_MSG_GET_BIGTHUMBNAIL,
	PI_MSG_FORCECREATE_THUMBNAIL,
    PI_MSG_SET_ZOOMRATIO,
	PI_MSG_SET_ZOOMRATIO_NOMOVE,
	PI_MSG_GET_ZOOMRATIO,
	PI_MSG_GET_REPORT_TOPLEFT,
    PI_MSG_SET_REPORT_TOPLEFT,
	PI_MSG_GET_NAVI_RATIO,
	PI_MSG_GET_PROJECTDIR,
	PI_MSG_ATTACH_CANVAS,
	PI_MSG_GET_CANVAS,

	PI_MSG_GET_CONF_GRAPHHEIGHT,
	PI_MSG_GET_CONF_GRAPHWIDTH,
	PI_MSG_GET_CONF_GRAPHLEGENDDISP,
	PI_MSG_GET_CONF_GRAPHTITLEDISP,
	PI_MSG_GET_CONF_GRAPHCOLORPATTERN,
	PI_MSG_GET_CONF_AXISFONT,
	PI_MSG_GET_CONF_AXISFONTSIZE,
	PI_MSG_GET_CONF_AXIS_TICKFONT,
	PI_MSG_GET_CONF_AXIS_TICKFONTSIZE,
	PI_MSG_GET_CONF_CONTOUR_MESH,
	PI_MSG_GET_CONF_CONTOUR_ALPHA,
	PI_MSG_GET_CONF_CONTOUR_ACCEPTVALUE,
	PI_MSG_GET_CONF_CONTOUR_CLIPLINESTYLE,
	PI_MSG_GET_CONF_CONTOUR_CLIPLINECOLOR,
	PI_MSG_GET_CONF_CONTOUR_CLIPLINEWIDTH,
	PI_MSG_GET_CONF_EXTRACT_NUMBER,
	PI_MSG_GET_CONF_UNIQUE_STRING,
	PI_MSG_GET_CONF_SHOW_FILEADD_ERROR,
	PI_MSG_GET_CONF_SHOW_OLDVER_PROJECT_OPEN_ERROR,
	PI_MSG_SET_CONF_DONT_SHOW_FILEADD_ERROR,
	PI_MSG_SET_CONF_DONT_SHOW_OLDVER_PROJECT_OPEN_ERROR,
	PI_MSG_GET_CONF_ADJUST_FONTSIZE_FOR_VER22_PROJECT,
	PI_MSG_GET_CONF__ADJUST_FONTSIZE_THRESHOLD_MM,

	PI_MSG_UPDATE_TASKPALETTE,
	PI_MSG_UPDATE_NAVIPALETTE,
	PI_MSG_UPDATE_OBJECTPALETTE,
	PI_MSG_UPDATE_PROPERTY,
	PI_MSG_UPDATE_PAGELIST,

	PI_MSG_OBJECTPALETTE_UPDATE_SELECT,

	PI_MSG_GET_VIEW_HANDLE,
	PI_MSG_SHOW_SIMPLEEDITOR,
	PI_MSG_SHOW_SLICELEVELSETTING,
	PI_MSG_SHOW_AXISTICKSETTING,
	PI_MSG_SHOW_LEGENDSETTING,
	PI_MSG_SHOW_TEXTSETTINGS,
	PI_MSG_SHOW_CLIPLINESETTINGS,
    PI_MSG_SHOW_COLORSELECT,

	PI_MSG_EDITMODE,
	PI_MSG_GET_EDITMODE,

	PI_MSG_BEGIN_EDIT,
	PI_MSG_END_EDIT,
	PI_MSG_UPDATE_STATUSBAR,
	PI_MSG_SHOW_TEXTEDITOR,

	PI_SET_CONTOUR_FILLING,
	PI_GET_ZAXIS_GRADATION,
	PI_SET_ZAXIS_TRANSPARENT,
	PI_SET_MARKER_COLOR,
	PI_GET_MARKER_VISIBLE,
	PI_GET_BAR_VISIBLE,
	PI_GET_MARKER_SIZE,
	PI_GET_PLOT_SORT,
	PI_GET_YAXIS_INTERVAL,
	PI_GET_ZAXIS_INTERVAL,
	PI_ZAXIS_AUTOVISIBLECHANGE,
	PI_AUTOSCALE_X,
	PI_AUTOSCALE_Y,
	PI_AUTOSCALE_Z,

	PI_SET_FORMULA_VISIBLE,
	PI_SET_FORMULA_HIDDEN,
	PI_SET_FORMULA_STRINGS,

	PI_GET_IMAGE_DIRECTORY,
	PI_SET_IMAGE_DIRECTORY,
	PI_GET_IMAGE_FILTER_INDEX,
	PI_SET_IMAGE_FILTER_INDEX,

	PI_GET_SERIALIZE_READER,
	PI_GET_SERIALIZE_WRITER,
	PI_GET_MIXED_FILTER,
	
	PI_GET_LINKTEXT,
	PI_GET_PRINT_LINKTEXT,
	PI_GET_FILEID,
	PI_GET_TEXTID,

	PI_START_PROGRESSBAR,
	PI_EXIT_PROGRESSBAR,

	PI_CHECK_LICENSE,
	PI_GET_EDITION,
	PI_GET_DIGITALMAP_LICENSE,
	PI_GET_KEY_MODE,
	PI_SHOW_VIEWER_MSG,
	PI_CONNECT_SERVER,
	PI_DISCONNECT_SERVER,
	PI_MSG_INVALIDATE,
	PI_MSG_IS_PROFESSIONAL,

	PI_MSG_GET_PAPERSIZE,
	PI_MSG_GET_MARGINRECT,

	PI_MSG_GET_GRAPH_FILENUM,

	PI_MSG_ADD_CHILDOBJECT,
	PI_MSG_REMOVE_CHILDOBJECT,

	PI_MSG_ADD_SELECTED_OBJECT,
	PI_MSG_REMOVE_SELECTED_OBJECT,
	PI_MSG_CLEAR_SELECTED_OBJECT,
	PI_MSG_GET_SELOBJECT_COUNT,
	///
	PI_MSG_MERGE_COMMAND,
	PI_MSG_UNMERGE_COMMAND,
	PI_MSG_OBJECT_DELETE,
	PI_MSG_EDIT_CUT,
	PI_MSG_EDIT_OLECOPY,
	PI_MSG_EDIT_COPY,
	PI_MSG_EDIT_PASTE,
	PI_MSG_TEXT_EDITING,
	PI_MSG_GROUPING,
	PI_MSG_UNGROUP,
	PI_MSG_TEXT_CLEAR,
	PI_MSG_PROPERTY_OPENONLY,
	PI_MSG_RULESETTING_COMMAND,
	PI_MSG_BEGIN_PASTE_TEXTS,
	PI_MSG_PASTE_OBJECTS,
	PI_MSG_OBJECTIMAGE_SAVE,
	PI_MSG_PASTE_CONDFORMAT,
	PI_MSG_CAN_PASTE_CONDFORMAT,

	PI_MSG_ERROR_MESSAGE,
	PI_MSG_WARNING_MESSAGE,
	PI_MSG_INFORMATION_MESSAGE,
	PI_MSG_ERROR_CHECK_MESSAGE,
	PI_MSG_WARNING_CHECK_MESSAGE,
	PI_MSG_INFORMATION_CHECK_MESSAGE,

	PI_MSG_DRAW_GRID,

	PI_MSG_GET_POSSIBLE_GROUPING,
	PI_MSG_GET_POSSIBLE_UNGROUPING,
};

///////////////////////////////////////////////////////////////////////////////

#include "IShape.h"
#include "IMessageReceiver.h"
#include "utility.h"
#include <vector>

inline TCHAR const* GetResourceString(IMessageReceiver* owner, char const* const resourceId)
{
	if(!owner) { return _T(""); }

	WPARAM id = reinterpret_cast<WPARAM>(resourceId);
#if OTOOLBOX
	// Toolboxの場合 owner はHWND型になり、メッセージで処理する
	HWND wnd = reinterpret_cast<HWND>(owner);
	return reinterpret_cast<TCHAR const*>(::SendMessage(wnd, PI_MSG_GET_RESOURCE_STRING, id, 0));
#else
	return reinterpret_cast<TCHAR const*>(owner->SendMessage(PI_MSG_GET_RESOURCE_STRING, id, 0));
#endif
}

inline std::wstring GetLinkTextImpl(IMessageReceiver* owner, int fileId, int linkId)
{
	wchar_t const* str = reinterpret_cast<wchar_t const*>(owner->SendMessage(PI_GET_LINKTEXT, fileId,linkId));
	return str ? std::wstring(str) : std::wstring();
}

//! 指定IDの印刷用のリンクテキストをアプリケーションから取得します
/*! メッセージの戻り値は一時的なポインタなのでコピーして返します	*/
inline std::wstring GetPrintLinkTextImpl(IMessageReceiver* owner, int fileId, int linkId)  
{
	wchar_t const* str = reinterpret_cast<wchar_t const*>(owner->SendMessage(PI_GET_PRINT_LINKTEXT, fileId,linkId));
	return str ? std::wstring(str) : std::wstring();
}

namespace message_wrapper
{
	//! 指定リンクテキストIDよりファイルIDをアプリケーションから取得します
	inline int GetFileId(IShape const* shape, int linkTextId)
	{
		int fileId = static_cast<int>(shape->GetOwner()->SendMessage(PI_GET_FILEID, linkTextId, 0));
		return fileId;
	}

#define GET_FILEID(id) message_wrapper::GetFileId(this, (id))

	inline int GetTextId(IShape* shape, int linkTextId)
	{
		int textId = static_cast<int>(shape->GetOwner()->SendMessage(PI_GET_TEXTID, linkTextId, 0));
		return textId;
	}

#define GET_TEXTID(id) message_wrapper::GetTextId(this, (id))
}	

/*!	@brief	オブジェクトの矩形座標をステータスバーに表示します
	上下左右の座標を表示します
*/
static inline void SetStatusBarRect(IMessageReceiver* owner, OCRect const& rect)
{
	if (rect.left == DBL_MAX || rect.top == DBL_MAX || rect.right == -DBL_MAX || rect.bottom == -DBL_MAX) {
		owner->SendMessage(PI_MSG_UPDATE_STATUSBAR, 0, 0);
	}
	else {
		std::vector<TCHAR*> ptr;
		TCHAR a[256] = {}, b[256] = {}, c[256] = {}, d[256] = {};
		::_sntprintf_s(a, _countof(a) - 1, GetResourceString(owner, "ID_SOURCE_VALUE_LEFT_MM"), rect.left);
		::_sntprintf_s(b, _countof(b) - 1, GetResourceString(owner, "ID_SOURCE_VALUE_RIGHT_MM"), rect.right);
		::_sntprintf_s(c, _countof(c) - 1, GetResourceString(owner, "ID_SOURCE_VALUE_TOP_MM"), rect.top);
		::_sntprintf_s(d, _countof(d) - 1, GetResourceString(owner, "ID_SOURCE_VALUE_BOTTOM_MM"), rect.bottom);
		ptr.push_back(a);
		ptr.push_back(b);
		ptr.push_back(c);
		ptr.push_back(d);
		owner->SendMessage(PI_MSG_UPDATE_STATUSBAR, reinterpret_cast<WPARAM>(&ptr[0]), static_cast<LPARAM>(ptr.size()));
	}
}

/*!	@brief	オブジェクトのサイズをステータスバーに表示します
	幅と高さを表示します
*/
inline void SetStatusBarSize(IMessageReceiver* owner, OCRect const& rect)
{
	std::vector<TCHAR*> ptr;
	TCHAR a[256] = {}, b[256] = {};
	::_stprintf_s(a, GetResourceString(owner, "ID_SOURCE_VALUE_WIDTH_MM"), rect.Width());
	::_stprintf_s(b, GetResourceString(owner, "ID_SOURCE_VALUE_HEIGHT_MM"), rect.Height());

	ptr.push_back(a);
	ptr.push_back(b);
	owner->SendMessage(PI_MSG_UPDATE_STATUSBAR, reinterpret_cast<WPARAM>(&ptr[0]), static_cast<LPARAM>(ptr.size()));
}

inline void ErrorMessage(IMessageReceiver* owner, TCHAR const* msg)
{
	owner->SendMessage(PI_MSG_ERROR_MESSAGE, reinterpret_cast<WPARAM>(msg), 0);
}

inline void WarningMessage(IMessageReceiver* owner, TCHAR const* msg)
{
	owner->SendMessage(PI_MSG_WARNING_MESSAGE, reinterpret_cast<WPARAM>(msg), 0);
}

inline void InformationMessage(IMessageReceiver* owner, TCHAR const* msg)
{
	owner->SendMessage(PI_MSG_INFORMATION_MESSAGE, reinterpret_cast<WPARAM>(msg), 0);
}

//! チェックボタン付きのエラーメッセージボックスを表示します
//! 引数のcheckStatusにチェックのON/OFF状態を格納します
inline void ErrorCheckMessage(IMessageReceiver* owner, TCHAR const* msg, TCHAR const* checkMsg, bool& checkStatus)
{
	CHECK_MESSAGE cm;
	cm.msg = msg;
	cm.checkMsg = checkMsg;
	UINT idAutoRespond = 0;
	cm.checkStatus = &idAutoRespond;
	owner->SendMessage(PI_MSG_ERROR_CHECK_MESSAGE, reinterpret_cast<WPARAM>(&cm), 0);
	checkStatus = *cm.checkStatus != 0;
}

//! チェックボタン付きの警告メッセージボックスを表示します
//! 引数のcheckStatusにチェックのON/OFF状態を格納します
inline void WarningCheckMessage(IMessageReceiver* owner, TCHAR const* msg, TCHAR const* checkMsg, bool& checkStatus)
{
	CHECK_MESSAGE cm;
	cm.msg = msg;
	cm.checkMsg = checkMsg;
	UINT idAutoRespond = 0;
	cm.checkStatus = &idAutoRespond;
	owner->SendMessage(PI_MSG_WARNING_CHECK_MESSAGE, reinterpret_cast<WPARAM>(&cm), 0);	
	checkStatus = *cm.checkStatus != 0;
}

//! ファイル追加時に項目が見つからなかったときのエラーメッセージを表示する
//! 環境設定でメッセージが表示されない設定の場合は表示しない
inline void ShowItemMismatchError(IMessageReceiver* owner)
{
	bool showError = !!owner->SendMessage(PI_MSG_GET_CONF_SHOW_FILEADD_ERROR, 0, 0);
	if (!showError) { return; }

	bool check = false;
	std::wstring checkMsg = GetResourceString(owner, "ID_SOURCE_TEXT_MSGNOTSHOW");
	if(owner->SendMessage(PI_MSG_GET_CONF_EXTRACT_NUMBER, 0, 0)) {
		std::wstring msg = GetResourceString(owner, "ID_SOURCE_TEXT_ITEM_NUM_NOT_MATCH_CANNOT_ADD_PLOT");
		WarningCheckMessage(owner, msg.c_str(), checkMsg.c_str(), check);
	}
	else {
		std::wstring msg = GetResourceString(owner, "ID_SOURCE_TEXT_PLOTNOSHOW");
		WarningCheckMessage(owner, msg.c_str(), checkMsg.c_str(), check);
	}
	if (check) { owner->SendMessage(PI_MSG_SET_CONF_DONT_SHOW_FILEADD_ERROR, 0, 0); }
}

//! Ver.2プロジェクト展開時の警告メッセージを表示する
//! 環境設定でメッセージが表示されない設定の場合は表示しない
inline void ShowVer2ProjectOpenError(IMessageReceiver* owner)
{
	bool showError = !!owner->SendMessage(PI_MSG_GET_CONF_SHOW_OLDVER_PROJECT_OPEN_ERROR, 0, 0);
	if (!showError) { return; }

	bool check = false;
	std::wstring checkMsg = GetResourceString(owner, "ID_SOURCE_TEXT_MSGNOTSHOW");
	std::wstring msg = GetResourceString(owner, "ID_SOURCE_TEXT_CANNOT_RESTORE_All_REPORTS_OLD_VERSION");

	WarningCheckMessage(owner, msg.c_str(), checkMsg.c_str(), check);

	if (check) { owner->SendMessage(PI_MSG_SET_CONF_DONT_SHOW_OLDVER_PROJECT_OPEN_ERROR, 0, 0); }
}

//! チェックボタン付きの情報メッセージボックスを表示します
//! 引数のcheckStatusにチェックのON/OFF状態を格納します
inline void InfomationCheckMessage(IMessageReceiver* owner, TCHAR const* msg, TCHAR const* checkMsg, bool& checkStatus)
{
	CHECK_MESSAGE cm;
	cm.msg = msg;
	cm.checkMsg = checkMsg;
	UINT idAutoRespond = 0;
	cm.checkStatus = &idAutoRespond;
	owner->SendMessage(PI_MSG_INFORMATION_CHECK_MESSAGE, reinterpret_cast<WPARAM>(&cm), 0);
	checkStatus = *cm.checkStatus != 0;
}

///////////////////////////////////////////////////////////////////////////////

// PI_MSG_CREATEGRAPH, PI_MSG_UPDATE_GRAPH
struct CGPARAM
{
	~CGPARAM()
	{
		for (unsigned long i = 0; i < dataCount; ++i) {
			delete[] data[i];
		}
		delete[] data;
	}

	unsigned long dataCount;
	unsigned long itemCount;
	float** data;
};

// PI_MSG_GET_FILEDATA
struct FileData
{
	wchar_t const* name;
	wchar_t const* unit;
    float const* data;
    unsigned long size;
};

struct FileDataStr
{
	wchar_t const* name;
	wchar_t const* unit;
	std::wstring const* data;
    unsigned long size;
};

typedef BOOL (__stdcall * ProgressCallback)(short percent);