/*!	@file
	@date	2008-09-25
	@author	wada

	立体グラフの共通処理の実装(立体グラフ/ウォーターフォール)
*/
#pragma once

#include <atlimage.h>

class OCSolidGraphBase
{
public:
	//! クリップボードへ画像を転送します
	void DoCopy()
	{
		if (::OpenClipboard(GetHandle())) {
			HBITMAP memBmp = GetGraphImage();

			::EmptyClipboard();
			::SetClipboardData(CF_BITMAP, memBmp);
			::CloseClipboard();

			::DeleteObject(memBmp);
		}
	}

	//! グラフイメージをファイルに保存します
	void DoSave()
	{
		WTL::CFileDialog dlg(FALSE, _T("bmp"), 0, OFN_OVERWRITEPROMPT | OFN_HIDEREADONLY,  ::GetResourceString(GetMessageReceiver(), "ID_SOURCE_TEXT_PICTURE_FILE_BMP"));
		if (dlg.DoModal() == IDOK) {
			HBITMAP memBmp = GetGraphImage();

			ATL::CImage img;
			img.Attach(memBmp);
			img.Save(dlg.m_szFileName);
			img.Detach();

			::DeleteObject(memBmp);
		}
	}

	//! グラフイメージを印刷します
	void DoPrint()
	{
		WTL::CPrintDialog dlg;
		if (dlg.DoModal() == IDOK) {
			WTL::CDCHandle dc(dlg.GetPrinterDC());

			HBITMAP memBmp = GetGraphImage();
			ATL::CImage img;
			img.Attach(memBmp);

			// とりあえずフルサイズで印刷
			// 絵が荒くなるので何かしらの対策は必要?
			dc.SetMapMode(MM_ANISOTROPIC);
			dc.SetWindowExt(WTL::CSize(img.GetWidth(), img.GetHeight()));
			dc.SetViewportExt(dc.GetDeviceCaps(PHYSICALWIDTH), dc.GetDeviceCaps(PHYSICALHEIGHT));

			DOCINFO docInfo = {sizeof(DOCINFO)};
			dc.StartDoc(&docInfo);
			dc.StartPage();
			dc.BitBlt(0, 0, img.GetWidth(), img.GetHeight(), img.GetDC(), 0, 0, SRCCOPY);
			dc.EndPage();
			dc.EndDoc();

			img.ReleaseDC();
			img.Detach();

			::DeleteDC(dc);
		}
	}

protected:
	virtual HWND GetHandle() const = 0;
	virtual HBITMAP GetGraphImage() const = 0;
	virtual IMessageReceiver* GetMessageReceiver() const = 0;
};