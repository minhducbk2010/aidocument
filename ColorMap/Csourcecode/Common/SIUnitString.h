#pragma once

#include <stdio.h>
#include <iostream>
#include <StringUtil.hpp>


	enum SIUnitLabel
	{
		OFF = 0,
		G, M, k,
		m, u, n, p
	};

	inline std::wstring GetSIUnitString(SIUnitLabel si)
	{
		std::wstring unitString;
		switch(si)
		{
			case G:
				unitString = _T("G");
				break;
			case M:
				unitString = _T("M");
				break;
			case k:
				unitString = _T("k");
				break;
			case m:
				unitString = _T("m");
				break;
			case u:
				unitString = _T("u");
				break;
			case n:
				unitString = _T("n");
				break;
			case p:
				unitString = _T("p");
				break;
			default:
				unitString = _T("");
				break;
		}

		return unitString;
	}	
	
	inline double GetScale(SIUnitLabel si)
	{
		double scale;
		switch(si)
		{
			case G:
				scale = 1e-9;
				break;
			case M:
				scale = 1e-6;
				break;
			case k:
				scale = 1e-3;
				break;
			case m:
				scale = 1e3;
				break;
			case u:
				scale = 1e6;
				break;
			case n:
				scale = 1e9;
				break;
			case p:
				scale = 1e12;
				break;
			default:	//OFFとそれ以外
				scale = 1;
				break;
		}

		return scale;
	}

	//! 数値をSI単位系に応じて調整した文字列で返す
	inline std::wstring ConvertSIUnitString(double value, SIUnitLabel si, StringFormat format, int decimalPlace)
	{
		value *= GetScale(si);	//SI単位に合わせて桁を調整

		return DblToStr(value, format, decimalPlace);
	}
