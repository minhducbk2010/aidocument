/*!	@file
	@date	2008-09-05
	@author	wada

	@brief	プロパティインターフェイス
*/
#pragma once
struct PropertyNode;


class IPropertySetter
{
public:
	//! プロパティを設定(更新)します
	/*!	ID は CreateProperty で作成された PropertyNode 構造体が保持しています
		@param[in]	id		プロパティのID
		@param[in]	value	プロパティの値
		@param[out]	source	プロパティの現在(変更前)の値(Undoで使用します)
	*/
	virtual bool SetProperty(long id, LPARAM value, LPARAM* source) = 0;
};

//!@class プロパティを持っているオブジェクトの抽象基底クラス
class IProperty : public IPropertySetter
{
public:
	//! プロパティを作成します
	virtual PropertyNode* CreateProperty() = 0;
	//! タブ用プロパティを作成します
	virtual PropertyNode* CreateTabProperty() =0;
	//! プロパティを削除(delete)します
	/*!	@param[in]	prop	削除対象のプロパティ(CreatePropertyの戻り値) */
	virtual void ReleaseProperty(PropertyNode* prop) = 0;

	//! 指定IDのプロパティ(値)を取得します
	/*!
		@param[in]	id	プロパティのID
		@param[out]	ret	戻り値
		@return		指定IDのプロパティがあればtrue、存在しなければfalseを返します
		@todo; 要純粋仮想関数化
	*/
	virtual bool GetProperty(long /*id*/, LPARAM* /*ret*/) const = 0;
};