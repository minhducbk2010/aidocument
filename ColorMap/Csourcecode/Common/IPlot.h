/*!	@file
	@brief	プロットのインターフェイス
	@date	2007-12-18
	@author	wada
*/
#pragma once

class IAxis;
class IProperty;

class IPlot
{
public:
	virtual ~IPlot() = 0 {}

	virtual void AttachXAxis(IAxis* axis) = 0;
	virtual void AttachYAxis(IAxis* axis) = 0;
	virtual void AttachZAxis(IAxis* axis) = 0;

	virtual IAxis* GetXAxis() const = 0;
	virtual IAxis* GetYAxis() const = 0;
	virtual IAxis* GetZAxis() const = 0;

	//virtual IProperty* GetImpl(size_t index) const {return 0;}
};

class ISolidPlot : public IPlot
{
public:
	virtual void SetEnableColor(bool enable) = 0;
	virtual void SetFillColor(COLORREF color) = 0;
	virtual void SetMarkerSize(double size) = 0;
	virtual double GetMarkerSize() const = 0;
};

class IFilter;
class IFile;
class IFilterOwner
{
public:
	virtual IFile* GetFile() const = 0;
	virtual IFilter* GetFilter() const = 0;
};
