#pragma once

//===========================================================================
//! SI単位変換
/*!
	文字列を数値(実数)に変換します。
	文字列中にSI接頭辞が含まれる場合、数値を接頭辞に合わせて単位変換します。

	@param[in]	pstr	変換対象の文字列
	@param[out]	fval	変換後の値

	@return	変換に成功したらtrue、文字列にSI接頭辞が含まれない場合はfalseを返します。
*/
//===========================================================================
bool ConvertSIUnit(char const* pstr, float& fVal)
{
	char *endstr = '\0';
	double dVal = ::strtod(pstr, &endstr);
	std::string strSI = endstr;
	if(*endstr == '\0')
	{
		fVal = static_cast<double>(dVal);
		return false;
	}

	struct si {
		char unit[3];
		double exp;
	} units[] = {
		{"P", 1e15},
		{"T", 1e12},
		{"G", 1e9},
		{"M", 1e6},
		{"k", 1e3},
		{"h", 1e2},
		{"da", 1e1},
		{"d", 1e-1},
		{"c", 1e-2},
		{"m", 1e-3},
		{"u", 1e-6},
		{"μ", 1e-6},
		{"n", 1e-9},
		{"p", 1e-12},
	};

	if(dVal != 0)
	{
		double dnew = 0;
		bool bCvt = false;
		for (size_t i = 0; i < _countof(units); ++i) {
			if (units[i].unit == strSI) {
				dnew = dVal * units[i].exp;
				bCvt = true;
			}
		}

		if(bCvt) {
			fVal = static_cast<float>(dnew);
			return true;
		}
	}
	fVal = static_cast<double>(dVal);
	return false;
}
