/*!	@file
	@date	2008-06-02
	@author	kikuchi

	@brief	Odfファイルの間引き・ステップの実装
*/
#pragma once
#include "../Library/File/OdfHeader.h"
#include <float.h>
#include "IMessageReceiver.h"
#include "Message.h"

class ThinningOdf
{
public:
	//! Constructor
	ThinningOdf(wchar_t const* file)
		: m_fileName(file)
	{
		std::ifstream is(file, std::ios::binary);
		is.read(reinterpret_cast<char*>(&m_header), sizeof(m_header));
	}

	//! Destructor
	~ThinningOdf()
	{
	}

	//! 間引き
	BOOL ThinningOut(int type, int interval)
	{
		// ルートを取得
		wchar_t drive[_MAX_DRIVE] = {}, dir[_MAX_DIR] = {}, root[_MAX_DRIVE + _MAX_DIR] = {};
		::_wsplitpath_s(m_fileName.c_str(), drive, _MAX_DRIVE, dir, _MAX_DIR, 0, 0, 0, 0);
		::_wmakepath_s(root, sizeof(root) / 2, drive, dir, 0, 0);

		size_t dataCount = 0;

		for (unsigned long count = 0; count < m_header.itemCount; ++count) {
			wchar_t readItem[512] = {};
			::swprintf_s(readItem, 512, _T("Item%04d.odf"), count + 1);
			std::wstring readPath = root;
			readPath += readItem;

			// ファイルオープン
			HANDLE read = ::CreateFile(readPath.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
			if (read == INVALID_HANDLE_VALUE) {
				return FALSE;
			}

			DWORD size;

			wchar_t item[256] = {}, unit[256] = {};
			char kind;
			::ReadFile(read, item, sizeof(item), &size, NULL);
			::ReadFile(read, unit, sizeof(unit), &size, NULL);
			::ReadFile(read, &kind, sizeof(kind), &size, NULL);
			
			std::vector<std::wstring> comments;
			for (unsigned long i = 0; i < m_header.commentRows; ++i) {
				wchar_t comment[256] = {};
				::ReadFile(read, comment, sizeof(comment), &size, NULL);
				comments.push_back(comment);
			}

			// ここから書き込み
			wchar_t writeItem[512] = {};
			::swprintf_s(writeItem, 512, _T("Item%04d.odf"), m_header.itemCount + count + 1);
			std::wstring writePath = root;
			writePath += writeItem;

			HANDLE write = ::CreateFile(writePath.c_str(), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
			if (write == INVALID_HANDLE_VALUE) {
				::CloseHandle(read);
				return FALSE;
			}

			::WriteFile(write, item, sizeof(item), &size, NULL);
			::WriteFile(write, unit, sizeof(unit), &size, NULL);
			::WriteFile(write, &kind, sizeof(kind), &size, NULL);
			for (size_t i = 0; i < comments.size(); ++i) {
				wchar_t comment[256] = {};
				::wcscpy_s(comment, comments[i].c_str());
				::WriteFile(write, comment, sizeof(comment), &size, NULL);
			}

			if (m_header.dataCount > 0) {
				if (static_cast<short>(kind) == 1) {
					dataCount = WriteStringData(read, write, interval);
				} else {
					// TODO: 演算CHは考慮しなくて良い？
					dataCount = WriteFloatData(read, write, type, interval);
				}
			}

			::CloseHandle(read);
			::CloseHandle(write);
		}

		// index書き直し
		m_header.dataCount = dataCount;
		
		std::ofstream os(m_fileName.c_str(), std::ios::binary);
		os.write(reinterpret_cast<char*>(&m_header), sizeof(m_header));

		for (unsigned long i = 0; i < m_header.itemCount; ++i) {
			wchar_t item[512] = {};
			::swprintf_s(item, 512, _T("Item%04d.odf"), i + 1);
			os.write(reinterpret_cast<char*>(item), sizeof(item));
		}

		DeleteSourceFile(root, m_header.itemCount);

		return TRUE;
	}

	//! 数値ＣＨ
	size_t WriteFloatData(HANDLE read, HANDLE write, int type, int interval)
	{
		interval = std::min<int>(interval, m_header.dataCount);

		std::vector<FloatData> datas;
		std::vector<FloatData> buff;
		DWORD size;
		for (unsigned long i = 0; i < m_header.dataCount; i += interval) {
			for(int j = 0; j < interval; ++j) {
				FloatData data;
				::ReadFile(read, &data, sizeof(data), &size, NULL);
				if (size == sizeof(data)) {
					buff.push_back(data);
				}
			}

			if (type == 5) {	//最大最小の場合は個別に調べる
				datas.push_back(ThinningData(3, buff));
				datas.push_back(ThinningData(4, buff));
			} else {
				datas.push_back(ThinningData(type, buff));
			}
			buff.clear();
		}

		::WriteFile(write, &datas[0], sizeof(FloatData) * datas.size(), &size, NULL);
		return datas.size();
	}

	//! 文字列CH
	size_t WriteStringData(HANDLE read, HANDLE write, int interval)
	{
		std::vector<StringData> headers;
		std::vector<std::wstring> datas;
		DWORD size;
		size_t count = 0;

		for (unsigned long i = 0; i < m_header.dataCount; i += interval) {
			for (int j = 0; j < interval; ++j) {
				StringData head;
				wchar_t* str;

				::ReadFile(read, &head, sizeof(head), &size, NULL);
				if (size == sizeof(head)) {
					str = new wchar_t[head.size];
					::ReadFile(read, str, head.size * 2, &size, NULL);

					if ((unsigned short)size == head.size * 2) {
						headers.push_back(head);
						datas.push_back(str);
					}

					delete[] str;
				}
			}
			if (headers.size() > 0 && datas.size() > 0) {
				::wcscpy_s(headers[0].data, datas[0].c_str());
				::WriteFile(write, &headers[0], sizeof(StringData), &size, NULL);
			}
			headers.clear();
			datas.clear();

			++count;
		}
		return count;
	}

	//! ステップ
	BOOL SetStep(double start, double interval, IMessageReceiver* owner)
	{
		std::vector<FloatData> step;
		double count = start;
		for (unsigned long i =0; i < m_header.dataCount; ++i, count += interval) {
			FloatData buf;
			buf.data = static_cast<float>(count);
			buf.edited = buf.reserve = 0;
			buf.used = 1;

			step.push_back(buf);
		}

		// step.odfを作成
		wchar_t drive[_MAX_DRIVE] = {}, dir[_MAX_DIR] = {}, root[_MAX_DRIVE + _MAX_DIR] = {};
		::_wsplitpath_s(m_fileName.c_str(), drive, _MAX_DRIVE, dir, _MAX_DIR, 0, 0, 0, 0);
		::_wmakepath_s(root, sizeof(root) / 2, drive, dir, 0, 0);
		std::wstring path = root;
		path += _T("Step.odf");

		wchar_t item[256] = {}, unit[256] = {}, comment[256] = {};
		char flag = (char)0;
		::wcscpy_s(item, 256, ::GetResourceString(owner, "ID_SOURCE_TEXT_STEP"));

		HANDLE writer = ::CreateFile(path.c_str(), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		if (writer == INVALID_HANDLE_VALUE) {
			::CloseHandle(writer);
			return FALSE;
		}

		DWORD size;
		::WriteFile(writer, item, sizeof(item), &size, NULL);
		::WriteFile(writer, unit, sizeof(unit), &size, NULL);
		::WriteFile(writer, &flag, sizeof(flag), &size, NULL);
		for (unsigned long i = 0; i < m_header.commentRows; ++i) {
			::WriteFile(writer, comment, sizeof(comment), &size, NULL);	// コメントはダミー
		}
		if (step.size() > 0) {
			::WriteFile(writer, &step[0], sizeof(FloatData) * step.size(), &size, NULL);
		}
		::CloseHandle(writer);

		// index書き直し
		++m_header.itemCount;

		std::ofstream os(m_fileName.c_str(), std::ios::binary);
		os.write(reinterpret_cast<char*>(&m_header), sizeof(m_header));
		
		wchar_t stepFile[512] = {};
		::wcscpy_s(stepFile, 512, _T("Step.odf"));
		os.write(reinterpret_cast<char*>(stepFile), sizeof(stepFile));

		for (unsigned long i = 0; i < m_header.itemCount; ++i) {
			wchar_t file[512] = {};
			::swprintf_s(file, 512, _T("Item%04d.odf"), i + 1);
			os.write(reinterpret_cast<char*>(file), sizeof(file));
		}

		DeleteSourceFile(root, m_header.itemCount);

		return TRUE;
	}

	/*! 間引き前のファイルを削除する
		@param	root		間引きファイルのあるディレクトリ名
		@param	itemCount	インポートファイルの項目数

		memo 間引き後のファイルを間引き前のファイル名でリネーム→上書き保存　な感じ
	*/
	BOOL DeleteSourceFile(wchar_t const* root, unsigned long itemCount)
	{
		for (unsigned long i = 0; i < itemCount; ++i) {
			wchar_t source[512] = {}, dest[512] = {};
			::swprintf_s(source, 512, _T("Item%04d.odf"), itemCount + i + 1);
			::swprintf_s(dest, 512, _T("Item%04d.odf"), i + 1);

			std::wstring sourcePath = root, destPath = root;
			sourcePath += source;
			destPath += dest;

			BOOL ret = ::MoveFileEx(sourcePath.c_str(), destPath.c_str(), MOVEFILE_REPLACE_EXISTING);
			if (!ret) { return FALSE; }
		}
		return TRUE;
	}

private:
	//! 間引きの分岐点
	FloatData ThinningData(int type, std::vector<FloatData> const& data)
	{
		FloatData ret;
		ret.edited = 0; ret.reserve = 0; ret.used = 1; ret.data = 0.0f;

		switch(type)
		{
		case 0:	// 始点
			if (data.size() == 0) {
				ret.data = FLT_MAX;
			} else { 
				ret = data[0];
			}
			break;
		case 1:	// 平均
			{
				double sum = 0.0;
				size_t nanCount = 0;
				for (size_t i = 0; i < data.size(); ++i) {
					if (data[i].data == FLT_MAX) {
						++nanCount;
					} else {
						sum += data[i].data;
					}
				}
				ret.data = (nanCount == data.size()) ? FLT_MAX : static_cast<float>(sum / (data.size() - nanCount));
			}
			break;
		case 2:	//中間
			{
				float max = GetMax(data);
				float min = GetMin(data);
				ret.data = (max == FLT_MAX || min == FLT_MAX) ? FLT_MAX : (min + max) / 2;
			}
			break;
		case 3:	//最大
			ret.data = GetMax(data);
			break;
		case 4:	//最小
			ret.data = GetMin(data);
			break;
		}
		return ret;
	}

	//! 最大値算出
	float GetMax(std::vector<FloatData> const& data)
	{
		float maximum = data[0].data;
		size_t nanCount = 0;
		for (size_t i = 0; i < data.size(); ++i) {
			if (data[i].data == FLT_MAX) {
				++nanCount;
			} else {
				maximum = max(maximum, data[i].data);
			}
		}
		return (nanCount == data.size()) ? FLT_MAX : maximum;
	}
	//!最小値算出
	float GetMin(std::vector<FloatData> const& data)
	{
		float minimum = data[0].data;
		size_t nanCount = 0;
		for (size_t i = 0; i < data.size(); ++i) {
			if (data[i].data == FLT_MAX) {
				++nanCount;
			} else {
				minimum = min(minimum, data[i].data);
			}
		}
		return (nanCount == data.size()) ? FLT_MAX : minimum;
	}

	Index m_header;
	std::wstring m_fileName;
};