#pragma once

#include <vector>

class IRtfHolder;

/*! 凡例の表示設定
*/
struct LegendSetting
{
     int ratio;              //!< 表示倍率
	 bool wrapPlotStr;	//<! プロットの文字列を折り返すか(Ver.2.2以前の表示対応)
     int returnPos;          //!< 折り返し位置
     bool displayFileName;   //!< ファイル名をプロットの後ろに表示する

     struct PlotSetting
     {
         unsigned int id;   //!< プロットのID
         bool visible;      //!< 表示状態(ON/OFF)
         TCHAR name[128];	//!< プロット名称
		 IRtfHolder* rtf;	//!< テキスト情報
     };
     PlotSetting plots[100];
	 int plotCount;
};
