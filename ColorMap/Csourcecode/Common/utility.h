#pragma once

#ifndef OC_COMMON_UTILITY_H_
#define OC_COMMON_UTILITY_H_

#include <string>
#include <limits>
#include <algorithm>
#include <ctime>
#include <AtlConv.h>
#include <tchar.h>
#include <boost/operators.hpp>

// 定数(別のヘッダに移動する
static int const HIT_NONE = 0;
static int const HIT_MOVE = 1;
static int const HIT_CREATE = 2;

//! 引数の値が0(に限りなく近い値)であれば真を返します
template<class T>
inline bool IsZero(T d)
{
	return std::abs(d) <= 0.000001;//std::numeric_limits<T>::epsilon();
}

//! 引数の等値判定
template<class T>
inline bool IsEqual(T a, T b)
{
	return IsZero(std::abs(a - b));
}

/*!	@brief	NULLポインタの定義
	@see http://ja.wikibooks.org/wiki/More_C%2B%2B_Idioms/nullptr
*/
const class {
public:
    template <class T>
    operator T*() const
    {
        return 0;
    }

    template <class C, class T>
    operator T C::*() const
    {
        return 0;
    }

private:
    void operator&() const;

} nullptr = {};

//! 点(座標)
struct OCPoint
{
	double x, y;

	OCPoint() : x(0), y(0) {}
	OCPoint(double x, double y) : x(x), y(y) {}
#if defined(_MFC_VER)
	OCPoint(CPoint pt) : x(pt.x), y(pt.y) {}
#endif

	void SetPoint(double x, double y)
	{
		this->x = x;
		this->y = y;
	}

	void Offset(double x, double y)
	{
		this->x += x;
		this->y += y;
	}

	//! otherとの距離を取得する
	double Distance(OCPoint other) const
	{
		double dx = x - other.x;
		double dy = y - other.y;
		return std::sqrt(dx * dx + dy * dy);
	}

#if defined(_MFC_VER)
	//! 点をCPointに変換する
	//! 小数点以下は切り捨てられる
	operator CPoint() const
	{
		return CPoint((int)x, (int)y);
	}
#else
	operator POINT() const
	{
		POINT p;
		p.x = (int)x;
		p.y = (int)y;
		return p;
	}
#endif

#if defined(_GDIPLUS_H)
	operator Gdiplus::PointF() const
	{
		return Gdiplus::PointF	((float)x, (float)y);
	}
#endif

	bool operator!=(double p) const
	{
		return x != p || y != p;
	}
	bool operator!=(OCPoint const& p) const
	{
		return x != p.x || y != p.y;
	}

	OCPoint operator+(double d) const
	{
		return OCPoint(x + d, y + d);
	}
	OCPoint operator+(OCPoint const& pt) const
	{
		return OCPoint(x + pt.x, y + pt.y);
	}
	OCPoint& operator+=(OCPoint const& pt)
	{
		x += pt.x;
		y += pt.y;
		return *this;
	}

	OCPoint operator-(OCPoint const& pt) const
	{
		return OCPoint(x - pt.x, y - pt.y);
	}
	OCPoint operator-(double d) const
	{
		return OCPoint(x - d, y - d);
	}
	OCPoint& operator-=(OCPoint const& pt)
	{
		x -= pt.x;
		y -= pt.y;
		return *this;
	}

	OCPoint operator*(OCPoint const& pt) const
	{
		return OCPoint(x * pt.x, y * pt.y);
	}
	OCPoint operator*(double d) const
	{
		return OCPoint(x * d, y * d);
	}
	OCPoint& operator*=(OCPoint const& pt)
	{
		x *= pt.x;
		y *= pt.y;
		return *this;
	}

	OCPoint operator/(OCPoint const& pt) const
	{
		return OCPoint(x / pt.x, y / pt.y);
	}
	OCPoint operator/(double d) const
	{
		return OCPoint(x / d, y / d);
	}
	OCPoint& operator/=(OCPoint const& pt)
	{
		x /= pt.x;
		y /= pt.y;
		return *this;
	}
};

struct OCPoint3D
{
public:
	double x;
	double y;
	double z;

	OCPoint3D() : x(0), y(0), z(0)
	{
	}

	OCPoint3D(float ax, float ay, float az)
		: x(ax), y(ay), z(az)
	{
	}
};

//! サーチポイントをプロパティダイアログに表示する際に使う構造体
struct OCSearchPoint
{
	OCSearchPoint(int dim) : dimension(dim)
		, xName(0), xUnit(0)
		, yName(0), yUnit(0)
		, zName(0), zUnit(0)
	{}

	long dimension;
	TCHAR const* xName;
	TCHAR const* xUnit;
	TCHAR xValue[64];
	TCHAR const* yName;
	TCHAR const* yUnit;
	TCHAR yValue[64];
	TCHAR const* zName;
	TCHAR const* zUnit;
	TCHAR zValue[64];
};

//! デバッガにメッセージを送信します(デバッグ時のみ)
inline void OC_Log(const char* msg, ...)
{
#ifdef DEBUG
    char buf[512] = {0};

    va_list argp;
    va_start(argp, msg);
    {
		::vsnprintf_s(buf, _countof(buf) - 1, _TRUNCATE, msg, argp);
    }
    va_end(argp);

	std::string message = buf;
	message += "\n";
    ::OutputDebugStringA(message.c_str());
#endif
}

/*! @brief	p1p2を端点とする線分とp3p4を端点とする線分の交差を判定する
	@retval true	交差する
	@retval false	交差しない */
// 参考 http://www5d.biglobe.ne.jp/~tomoya03/shtml/algorithm/IntersectionEX.htm
inline bool Intersect2Lines(OCPoint const& p1, OCPoint const& p2, OCPoint const& p3, OCPoint const& p4)
{
	// X座標によるラフチェック
	if( p1.x >= p2.x ){
		if( (p1.x < p3.x && p1.x < p4.x) || (p2.x > p3.x && p2.x > p4.x) ){
			// 交差しない
			return false;
		}
	}
	else {
		if( (p2.x < p3.x && p2.x < p4.x) || (p1.x > p3.x && p1.x > p4.x) ){
			// 交差しない
			return false;
		}
	}
	// Y座標によるラフチェック
	if( p1.y >= p2.y ){
		if( (p1.y < p3.y && p1.y < p4.y) || (p2.y > p3.y && p2.y > p4.y) ){
			// 交差しない
			return false;
		}
	}
	else {
		if( (p2.y < p3.y && p2.y < p4.y) || (p1.y > p3.y && p1.y > p4.y) ){
			// 交差しない
			return false;
		}
	}

	if( ((p1.x - p2.x) * (p3.y - p1.y) + (p1.y - p2.y) * (p1.x - p3.x)) *
		((p1.x - p2.x) * (p4.y - p1.y) + (p1.y - p2.y) * (p1.x - p4.x)) > 0.0 ){
			// 交差しない
			return false;
	}
	if( ((p3.x - p4.x) * (p1.y - p3.y) + (p3.y - p4.y) * (p3.x - p1.x)) *
		((p3.x - p4.x) * (p2.y - p3.y) + (p3.y - p4.y) * (p3.x - p2.x)) > 0.0 ){
			// 交差しない
			return false;
	}

	// 交差する
	return true;
}

//! 矩形
struct OCRect
{
	double left;
	double top;
	double right;
	double bottom;

	OCRect() : left(0), top(0), right(0), bottom(0) {}

	//! Constructor
	OCRect(double left, double top, double right, double bottom)
		: left(left), top(top), right(right), bottom(bottom)
	{}

#if defined(_MFC_VER)
	//! Constructor
	OCRect(CRect r)
		: left(r.left), top(r.top), right(r.right), bottom(r.bottom)
	{
	}
#endif

	//! 矩形の幅を取得する
	double Width() const
	{
		return right - left;
	}
	
	//! 矩形の高さを取得する
	double Height() const
	{
		return bottom - top;
	}

	void Normalize()
	{
		double tmpLeft = std::min<double>(left, right);
		double tmpTop = std::min<double>(top, bottom);
		double tmpRight = std::max<double>(left, right);
		double tmpBottom = std::max<double>(top, bottom);
		
		left = tmpLeft;
		top = tmpTop;
		right = tmpRight;
		bottom = tmpBottom;
	}

	bool IsEmpty() const
	{
		return IsZero(Width()) && IsZero(Height());
	}

#if defined(_MFC_VER)
	//! CRect型へのキャストをサポート
	//! CRect型の各点はint型なので小数点以下は切り捨てられる点に注意
	operator CRect()
	{
		return CRect(static_cast<int>(left), static_cast<int>(top), static_cast<int>(right), static_cast<int>(bottom));
	}
#endif

#if defined(_GDIPLUS_H)
	operator Gdiplus::RectF()
	{
		return Gdiplus::RectF((float)left, (float)top, (float)(right - left), (float)(bottom - top));
	}
#endif

	bool operator==(OCRect const& r)
	{
		return IsEqual(r.left, left) && IsEqual(r.top, top) && IsEqual(r.right, right) && IsEqual(r.bottom, bottom);
	}

	bool operator!=(OCRect const& r)
	{
		return !(*this == r);
	}

	//OCRect operator*(double v) const
	//{
	//	return OCRect(left * v, top * v, right * v, bottom * v);
	//}

	void Move(double x, double y)
	{
		Offset(x - left, y - top);
	}

	//! 矩形を移動する
	void Offset(double x, double y)
	{
		left += x; right += x;
		top += y; bottom += y;
	}

	//! 矩形を移動する
	template<class T>
	void Offset(T point)
	{
		Offset(point.x, point.y);
	}

	//! 点が矩形の内側にあるかどうかを調べる
	template<class T>
	bool PtInRect(T p) const
	{
		return left <= p.x && right >= p.x && top <= p.y && bottom >= p.y;
	}

	//! 点が矩形の内側にあるかどうかを調べる
	bool Contains(double x, double y) const
	{
		return left <= x && right >= x && top <= y && bottom >= y;
	}

	//! p1p2を端点とする線分と交差(内包)するか調べる
	//! true→交差(内包)する false→交差(内包)しない
	// 参考 http://mieyasu.hp.infoseek.co.jp/mie/game/g001-2.html
	bool IntersectLine(OCPoint const& p1, OCPoint const& p2) const
	{
		// p1がOCRect内に含まれるかを調べる
		if( Contains( p1.x, p1.y ) ){
			return true;
		}
		// p2がOCRect内に含まれるかを調べる
		if( Contains( p2.x, p2.y ) ){
			return true;
		}
		// 左上−右下の対角線との交差を調べる
		if( Intersect2Lines( p1, p2, OCPoint(left,top), OCPoint(right,bottom) ) ){
			return true;
		}
		// 左下−右上の対角線との交差を調べる
		if( Intersect2Lines( p1, p2, OCPoint(left,bottom), OCPoint(right,top) ) ){
			return true;
		}
		// 2本の対角線と交差しなければ、OCRectとも交差しない
		return false;
	}

	//! 入力矩形rとの重なりがあるか調べる
	//! true→重なる false→重ならない
	// 参考 http://blogs.wankuma.com/nagise/archive/2007/08/05/88902.aspx
	bool IntersectRects(OCRect const& r) const
	{
		// 矩形Aの座標
		// (ax1, ay1)───────────┐
		//      └───────────(ax2, ay2)
		// 矩形Bの座標
		// (bx1, by1)───────────┐
		//      └───────────(bx2, by2)
		// 図のようにA,Bふたつの矩形があるとします。 Aの左上角の座標から見て右下にBの右下角があり(図の黒矢印)、 
		// Aの右下角の座標から見て左上にBの左上角がある(図の赤矢印)場合にふたつの矩形は重なっているのです。

		double ax1 = std::min<double>(r.left, r.right);
		double ay1 = std::min<double>(r.top,  r.bottom);
		double ax2 = std::max<double>(r.left, r.right);
		double ay2 = std::max<double>(r.top,  r.bottom);
		double bx1 = std::min<double>(left, right);
		double by1 = std::min<double>(top,  bottom);
		double bx2 = std::max<double>(left, right);
		double by2 = std::max<double>(top,  bottom);

		return ( ((ax1 < bx2) && (ay1 < by2)) && ((ax2 > bx1) && (ay2 > by1)) );
	}

	OCPoint Center() const
	{
		return OCPoint(left + Width() / 2, top + Height() / 2);
	}
};

inline int GetScreenDpi()
{
	static int screen_dpi = 0;
	if (!screen_dpi) {
		HDC dc = ::GetDC(0);
		screen_dpi = ::GetDeviceCaps(dc, LOGPIXELSX);
		::ReleaseDC(0, dc);
	}
	return screen_dpi;
}

inline double MMToPx(double const mm, double const dpi = GetScreenDpi())
{
    double const INCH = 25.4059;
	double const PIXEL = dpi / INCH;
    return PIXEL * mm;
}

inline OCPoint MMToPx(OCPoint const& point)
{
	return OCPoint(MMToPx(point.x), MMToPx(point.y));
}

inline OCRect MMToPx(OCRect const& r)
{
	double const width = MMToPx(r.Width());
	double const height = MMToPx(r.Height());
	double const left = MMToPx(r.left);
	double const top = MMToPx(r.top);
	return OCRect(left, top, left + width, top + height);
}

inline double PxToMM(double px)
{
	double const INCH = 25.4059;
    return INCH / GetScreenDpi() * px;
}

inline OCPoint PxToMM(OCPoint const& pt)
{
	return OCPoint(PxToMM(pt.x), PxToMM(pt.y));
}

inline OCRect PxToMM(OCRect const& rect)
{
	return OCRect(PxToMM(rect.left), PxToMM(rect.top), PxToMM(rect.right), PxToMM(rect.bottom));
}

inline float GetPenWidth(float width, double const dpi = GetScreenDpi())
{
    double const mm = width * 0.3528f;
	double const pw = std::max<double>(MMToPx(mm, dpi) + 0.5, 1);
    return static_cast<float>(pw);
}

static double GetDpiRatio()
{
	return GetScreenDpi() / 96.0;
}

//! ポイントを論理単位に変換します
//! LOGFONT::lfHeightに値を設定する際などに使用します
inline int PointToDeviceUnit(int point)
{
	return -::MulDiv(point, GetScreenDpi(), 72);
}

inline void SetCursor(HWND hWnd, LPTSTR resource)
{
	::SetClassLongPtr(hWnd, GCL_HCURSOR, (__int3264)(LONG_PTR)::LoadCursor(NULL, resource));
}

//! マルチバイト文字列をユニコード文字列に変換します
inline std::wstring ToWideString(std::string const& str)
{
	//USES_CONVERSION;
	//return A2W(str.c_str());

	// 英語ロケール環境で日本語が文字化けする可能性があるので、Shift-JISコードページで変換して文字化けしないようにする
	std::wstring wStr;
	size_t i;
	int wsSize = MultiByteToWideChar(932 /* Shift-JIS */, 0, str.c_str(), -1, NULL, 0);
	wchar_t *ws = new wchar_t[wsSize];
	MultiByteToWideChar(932 /* Shift-JIS */, 0, str.c_str(), -1, ws, wsSize);
	wStr = ws;
	delete [] ws;

	return wStr;
}

//! ユニコード文字列をマルチバイト文字列に変換します
inline std::string ToMultiByteString(std::wstring const& str)
{
	//USES_CONVERSION;
	//return W2A(str.c_str());

	// 英語ロケール環境で日本語が文字化けする可能性があるので、Shift-JISコードページで変換して文字化けしないようにする
	std::string mbStr;
	size_t i;
	int mbsSize = WideCharToMultiByte(932 /* Shift-JIS */, 0, str.c_str(), -1, NULL, 0, NULL, NULL);
	char *mbs = new char[mbsSize];
	WideCharToMultiByte(932 /* Shift-JIS */, 0, str.c_str(), -1, mbs, mbsSize, NULL, NULL);
	mbStr = mbs;
	delete [] mbs;

	return mbStr;
}

#ifdef UNICODE
inline std::wstring FloatToString(float value)
#else
inline std::string FloatToString(float value)
#endif
{
	TCHAR buf[36];
	::_stprintf_s(buf, _T("%g"), value);
	return buf;
}

#ifdef UNICODE
inline std::wstring FloatToString(double value)
#else
inline std::string FloatToString(double value)
#endif
{
	TCHAR buf[36];
	::_stprintf_s(buf, _T("%g"), value);
	return buf;
}


#if defined(_MFC_VER)
inline CView* FindActiveView()
{
	if (CMDIFrameWnd* main = dynamic_cast<CMDIFrameWnd*>(AfxGetMainWnd())) {
		if (CMDIChildWnd* child = dynamic_cast<CMDIChildWnd*>(main->MDIGetActive())) {
			return child->GetActiveView();
		}
	}
	return NULL;
}
#endif

template<class T>
std::pair<T, T> MinMaxElement(T const* first, int count)
{
	std::pair<T, T> pr(FLT_MAX, -FLT_MAX);
	while (count--) {
		if (*first != FLT_MAX) {
			pr.first = std::min<T>(pr.first, *first);
			pr.second = std::max<T>(pr.second, *first);
		}
		++first;
	}
	return pr;
}

//#ifdef __AFXWIN_H__
#ifdef UNICODE
inline std::wstring GetExeDirectory()
#else
inline std::string GetExeDirectory()
#endif
{
	TCHAR buff[_MAX_PATH];
	::GetModuleFileName(NULL, buff, _MAX_PATH);

	TCHAR drive[_MAX_DRIVE], dir[_MAX_DIR], fname[_MAX_FNAME], ext[_MAX_EXT];
	::_tsplitpath_s(buff, drive, _MAX_DRIVE, dir, _MAX_DIR, fname, _MAX_FNAME, ext, _MAX_EXT);

	TCHAR rootDir[_MAX_PATH];
	::_tmakepath_s(rootDir, _MAX_PATH, drive, dir, NULL, NULL);

	return rootDir;
}
//#endif

// 引数のディレクトリのパスからディレクトリ名だけ抽出する
#ifdef UNICODE
inline std::wstring GetDirNameFromDirPath(std::wstring dirPath)
#else
inline std::string GetDirNameFromDirPath(std::string dirPath)
#endif
{
#ifdef UNICODE
	std::wstring dir = dirPath;
#else
	std::string dir = dirPath;
#endif

	// 末尾に\が付いている可能性があるので、末尾に\が付いている場合は削除する
	while (*dir.rbegin() == _T('\\'))
	{
		dir.erase(dir.size() - 1);
	}
	std::string::size_type pos = dir.find_last_of(_T("\\"));		
	if ( pos != std::string::npos) {
		dir = dir.substr(pos + 1);
	}

	return dir;
}

#include <atlstr.h>
#include <atltime.h>
#include <mmsystem.h>
#pragma comment(lib, "winmm.lib")
//<! ストップウォッチ、関数内の処理時間を測ります(デバッグ時のみ有効)
struct stop_watch
{
#ifdef DEBUG
	stop_watch(TCHAR const* msg = 0, bool display = true)
		: m_start(timeGetTime()), m_msg(msg), m_display(display)
	{
	}

	~stop_watch()
	{
		if (!m_display) { return; }

		double s = timeGetTime() - m_start;
		TCHAR buf[200] = {};
		if (m_msg) {
			::_stprintf_s(buf, _T("%f秒 - [%s]\n"), s / 1000.0, m_msg);
		}
		else {
			::_stprintf_s(buf, _T("%f秒\n"), s);
		}

		::OutputDebugString(buf);
	}

	TCHAR const* m_msg;
	bool m_display;
	std::clock_t m_start;
#else
	stop_watch(TCHAR const* msg = 0, bool display = true)
	{
	}
#endif
};
/*! 指定したフォルダ、またはファイルを削除します。
	なお、フォルダが指定された場合はその中にあるフォルダとファイルも全て削除します

	@param [in] filePath 削除するフォルダ、またはファイルのパス(例: C:\Program Files\)
*/
inline void DeleteFiles(LPCTSTR filePath)
{
	// dirPathが空の場合、どっか適当なフォルダを削除しだすので戻る
	if(::StrCmp(filePath, _T("")) == 0) { return; }

	TCHAR dir[_MAX_PATH];
	::_stprintf_s(dir, _MAX_PATH, _T("%s*"), filePath);
	WIN32_FIND_DATA fd;
	HANDLE hFile = ::FindFirstFile(dir, &fd);
	if(hFile == INVALID_HANDLE_VALUE)
	{
		::FindClose(hFile);
		return;
	}

	do
	{
		if(::StrCmp(fd.cFileName, _T(".")) == 0 || ::StrCmp(fd.cFileName, _T("..")) == 0)
		{
			continue;
		}

		if(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			// フォルダの場合
			TCHAR nextDir[_MAX_PATH];
			::_stprintf_s(nextDir, _MAX_PATH, _T("%s%s\\"), filePath, fd.cFileName);
			DeleteFiles(nextDir);
		}
		else
		{
			// ファイルの場合
			TCHAR file[_MAX_PATH];
			::_stprintf_s(file, _MAX_PATH, _T("%s%s"), filePath, fd.cFileName);
			::DeleteFile(file);
		}
	} while(::FindNextFile(hFile, &fd));

	::FindClose(hFile);
	::RemoveDirectory(filePath);
}

// 拡張子を変換します
inline std::wstring ChangeFileExt(std::wstring file, std::wstring const& ext)
{
	std::wstring curExt = ::PathFindExtension(file.c_str());
	if (curExt.length()) {
		file.resize(file.size() - curExt.size());
	}
	return file + ext;
}

//! オブジェクトが生きている間ウィンドウをロックします
/*! LockWindowUpdate APIを使用 */
struct OCLockWindow
{
	OCLockWindow(HWND wnd)	{ ::LockWindowUpdate(wnd); }
	~OCLockWindow()			{ ::LockWindowUpdate(0); }
};

//! オブジェクトが生きている間ウィンドウの再描画を抑止します
class OCNoRedraw
{
public:
	OCNoRedraw(HWND wnd) : m_wnd(wnd)	{ ::SendMessage(m_wnd, WM_SETREDRAW, 0, 0); }
	~OCNoRedraw()						{ ::SendMessage(m_wnd, WM_SETREDRAW, 1, 0); }

private:
	HWND m_wnd;
};

#endif