/*!	@file
	@date	2008-09-29
	@author	kikuchi

	@brief	OChartで設定可能な書式設定構造体(LOGFONTを包含する)
*/

#pragma once

// EditableShape.h より移動
namespace HorizontalAlign
{
	enum HorizontalAlign
	{
		Left = 1,
		Right,
		Center,
	};
}

namespace VerticalAlign
{
	enum VerticalAlign
	{
		Top = 1,
		Bottom,
		Center,
	};
}

struct TextFontSetting
{
	LOGFONT logFont;
	COLORREF fontColor;
	int scriptPos;
	HorizontalAlign::HorizontalAlign hAlign;
	VerticalAlign::VerticalAlign vAlign;
	bool autoSize;
	int lineSpace;
	int angle;
	RECT margin;
};