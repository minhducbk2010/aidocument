/*!	@file
	@brief	定数定義
*/
#pragma once

//! O-Chartで使用するフォントサイズの一覧
static int const OC_FONT_SIZE[] = { 6, 8, 9, 10, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28, 36, 48, 60, 72 };