/*!	@file
*/

DWORD WINAPI CreateDumpThread(void* data)
{
    TCHAR path[_MAX_PATH];
	::SHGetSpecialFolderPath(NULL, path, CSIDL_APPDATA, TRUE);
	::_tcscat_s(path, _T("\\Onosokki O-Chart3\\"));
	if (::_taccess_s(path, 0) != 0) {
		::CreateDirectory(path, 0);
	}

	::_tcscat_s(path, ::GetResourceString("ID_SOURCE_TEXT_O_CHARTERRORREPORT_DMP"));

	HANDLE hFile = ::CreateFile(path, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
	if (hFile == INVALID_HANDLE_VALUE) { return 0; }

	MINIDUMP_EXCEPTION_INFORMATION exceptionInfo = {};
	exceptionInfo.ThreadId = ::GetCurrentThreadId();
	exceptionInfo.ClientPointers = FALSE;
	exceptionInfo.ExceptionPointers = reinterpret_cast<EXCEPTION_POINTERS*>(data);

	::MiniDumpWriteDump(::GetCurrentProcess(), ::GetCurrentProcessId(), hFile,
		(MINIDUMP_TYPE)(MiniDumpNormal | MiniDumpWithHandleData), data ? &exceptionInfo : 0, 0, 0);
	
	::CloseHandle(hFile);

	return 0;
}

void ReportGLInfo()
{
    TCHAR path[_MAX_PATH];
	::SHGetSpecialFolderPath(NULL, path, CSIDL_APPDATA, TRUE);
	::_tcscat_s(path, _T("\\Onosokki O-Chart3\\"));
	if (::_taccess_s(path, 0) != 0) {
		::CreateDirectory(path, 0);
	}

	::_tcscat_s(path, _T("GLInfo.dmp"));

	HDC dc = ::GetDC(0);
	PIXELFORMATDESCRIPTOR pfd = {};
	::DescribePixelFormat(dc, 1, sizeof(pfd), &pfd);
	::ReleaseDC(0, dc);

	std::ofstream os(path, std::ios::binary);
	os.write(reinterpret_cast<char*>(&pfd), sizeof(pfd));
}

//! ミニダンプを作成します
/*!
	実行ファイルのあるフォルダにダンプファイルを作成します
	既存のファイルは上書きされます
*/
void CreateDump(EXCEPTION_POINTERS* exceptionInfo)
{
	DWORD dw;
	if (HANDLE hThread = ::CreateThread(0, 0, CreateDumpThread, exceptionInfo, 0, &dw)) {
		::WaitForSingleObject(hThread, INFINITE);
		::CloseHandle(hThread);
	}

	//ReportGLInfo();
}