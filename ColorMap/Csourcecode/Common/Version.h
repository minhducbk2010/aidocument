#pragma once

//! バージョン情報を格納するための構造体
struct Version
{
	unsigned int major;	//! メジャーバージョン
	unsigned int minor;	//! マイナーバージョン
	unsigned int release;	//! リリースバージョン
	unsigned int build;	//! ビルドバージョン

	Version()
		: major(0), minor(0), release(0), build(0)
	{
	}
	Version(unsigned int major, unsigned int minor, unsigned int release, unsigned int build)
		: major(major), minor(minor), release(release), build(build)
	{
	}

	bool operator== (Version const& other) const {
		return major == other.major
			&& minor == other.minor
			&& release == other.release
			&& build == other.build;
	}

	bool operator< (Version const& other) const {
		return
			(major < other.major) ||
			(major == other.major && minor < other.minor) ||
			(major == other.major && minor == other.minor && release < other.release) ||
			(major == other.major && minor == other.minor && release == other.release && build < other.build);
			
	}
	bool operator<= (Version const& other) const {
		return (*this < other) || (*this == other);
	}

	bool operator> (Version const& other) const {
		return !(*this < other);
	}
	bool operator>= (Version const& other) const {
		return (*this == other) || !(*this < other);
	}
};
