#pragma once

class IApplicable
{
public:
	virtual void OnApplication(int startStyle, int endStyle, int startSize,
		int endSize, int startAngle, int endAngle) = 0;
};