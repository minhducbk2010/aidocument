/*!	@file
	@date	2008-03-17
	@author	wada

	@brief	3Dグラフのインターフェイス
			4Dも含む...
*/
#pragma once

#include "IGraph.h"

class IPlotData;

class IGraph3D : public IGraph
{
public:
	virtual void SetDimension(int dim) = 0;
	virtual long GetDimension() const = 0;
	virtual void SetData(IPlotData* data) = 0;
	virtual void AddData(IPlotData* data) = 0;
	virtual void SetPlotKind(unsigned long kind) = 0;

	virtual void OnCommand(UINT id) = 0;
	virtual void GetState(UINT id, BOOL* enable, BOOL* checked) = 0;

	virtual int GetActivePlotIndex() const = 0;
	virtual COLORREF GetActivePlotColor() const = 0;

	//virtual AddPlot(unsigned long type, unsigned long fileNo, unsigned long xItem, unsigned long yItem, unsigned long zItem) = 0;
	virtual void AddPlot(unsigned long type, IPlotData* data) = 0;

	virtual void UpdateGraphImage() = 0;

	virtual void CloseViewer() = 0;

	virtual int SavePreEditState() = 0;
	virtual void RestoreEditState(int state) = 0;
};

class ISolidGraph
{
public:
	virtual void SetEnableColor(int index, bool enable) = 0;
	virtual void SetFillColor(int index, COLORREF color) = 0;
	virtual double GetMarkerSize(int index) const = 0;
	virtual void SetMarkerSize(int index, double size) = 0;
};

class IVectorGraph
{
public:
};

class IExtractive
{
public:
	virtual wchar_t const* GetXName() const = 0;
	virtual wchar_t const* GetXUnit() const = 0;
	virtual wchar_t const* GetYName() const = 0;
	virtual wchar_t const* GetYUnit() const = 0;
	virtual wchar_t const* GetZName() const = 0;
	virtual wchar_t const* GetZUnit() const = 0;

	virtual void ExtractX(float* x, float* y, float* z, float* t, size_t* length) const = 0;
	virtual void ExtractY(float* x, float* y, float* z, float* t, size_t* length) const = 0;
};