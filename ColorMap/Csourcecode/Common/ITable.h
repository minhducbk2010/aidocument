/*!	@file
	@date	2009-04-17
	@author	kikuchi

	@brief	表のインターフェース
			コマンド絡みでメインとやり取りするために作成
	@todo	TableBaseとのすみわけが微妙...
*/
#pragma once

struct RuleSetting;
class IShape;

//! 表関連のコマンド実行インタフェース
class ITableCommand
{
public:
	// Commandクラス削除時に、パラメータクラスも削除する必要がある
	virtual void Destroy() = 0;
	virtual bool Execute() = 0;
	virtual void Undo() = 0;
	virtual void Redo() = 0;
};

//! 表のインターフェイス
class ITable
{
public:
	//! 罫線のスタイルを設定します
	virtual void SetRuleSettings(const RuleSetting& rule, const CRect& range, RuleSetting* rules, int count) =  0;
	//! バイナリ形式で貼り付ける際にClone()が呼ばれないのでm_cellCopyingのフラグを操作するために使う
	virtual void IsSerializeBinary(bool saving) = 0;
	//! 切り取りコマンド実行時に、表を削除するかどうか
	virtual bool IsDeleteTable() = 0;
	//! セルが選択されているかどうかを取得
	virtual bool IsCellSelected() = 0;
	//! セルがコピーできるかを取得(表選択時は何もせずtrueを返します)
	virtual bool CanCopyCell(LPCTSTR targetName) const = 0;
	//! セルを切り取れるか取得
	virtual bool CanCutCell() const = 0;

	//! テキストの貼り付け
	virtual bool PasteTextToCell() = 0;
	//! オブジェクトの貼り付け
	virtual bool PasteObjectToCell(IShape* source) = 0;
	//! セル取得(結合されている部分はNULLが返る)
	virtual IShape* GetCell(size_t col, size_t row) = 0;
	//! 選択セル取得
	virtual IShape* GetSelectCell(size_t index) = 0;
	//! 選択セル数取得
	virtual size_t GetSelectCellCount() = 0;

	virtual size_t GetRowCountA() const = 0;
	virtual size_t GetColCountA() const = 0;

	//! 表の名称(OCTable::m_name)を取得
	virtual void GetTableName(wchar_t* name, size_t length) const = 0;
};

class ICell
{
public:
	virtual ITable* GetParentTable() const = 0;
};