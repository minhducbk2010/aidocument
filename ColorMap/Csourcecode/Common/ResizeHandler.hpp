/*!	@file
	@date	2008-03-24
	@author	wada

	@brief	リサイズハンドラ
			オブジェクトがリサイズ可能かどうかを示すボックスを描きます
*/
#pragma once

#include "ProtectEnum.h"

enum Hit
{
	TopLeft = 3,
	TopMiddle,
	TopRight,
	BottomLeft,
	BottomMiddle,
	BottomRight,
	MiddleLeft,
	MiddleRight,
};

static double const RH_THRESHOLD = 5.0; // 中央のリサイズハンドルを出すのに必要なオブジェクトのサイズ

template<class T>
class OCResizeHandler
{
public:
	//! Constructor
	//!@param	shape	リサイズ管理するオブジェクト
	OCResizeHandler(T* shape) : m_shape(shape), m_resizable(true)
	{
	}

	//! リサイズハンドルを描画します
	void Draw() const
	{
		Draw(m_shape->m_rect);
	}

	void Draw(OCRect const& mmr) const
	{
		if (!m_resizable) { return; }

		double xCenter = mmr.left + mmr.Width() / 2;
		double yCenter = mmr.top + mmr.Height() / 2;

		std::vector<OCRect> rects;
		rects.push_back(OCRect(mmr.left, mmr.top, mmr.left, mmr.top));
		rects.push_back(OCRect(mmr.right, mmr.top, mmr.right, mmr.top));
		rects.push_back(OCRect(mmr.left, mmr.bottom, mmr.left, mmr.bottom));
		rects.push_back(OCRect(mmr.right, mmr.bottom, mmr.right, mmr.bottom));
		if (mmr.Width() > RH_THRESHOLD) {
			rects.push_back(OCRect(xCenter, mmr.top, xCenter , mmr.top));
			rects.push_back(OCRect(xCenter, mmr.bottom, xCenter, mmr.bottom));
		}
		if (mmr.Height() > RH_THRESHOLD) {
			rects.push_back(OCRect(mmr.left, yCenter, mmr.left, yCenter));
			rects.push_back(OCRect(mmr.right, yCenter, mmr.right, yCenter));
		}

		for (size_t i = 0; i < rects.size(); ++i) {
			OCRect const& r = rects[i];
#ifdef OCHART_VIEWER
			m_shape->m_canvas->DrawResizeHandle(OCPoint(r.left, r.top), true/*常にロック状態とする*/);
#else
			if (m_shape->GetOwner()->SendMessage(PI_GET_EDITION, 0, 0) != Viewer) {
				m_shape->m_canvas->DrawResizeHandle(OCPoint(r.left, r.top), m_shape->IsLock() || m_shape->IsPositionLock());
			}
			else{
				m_shape->m_canvas->DrawResizeHandle(OCPoint(r.left, r.top), true/*常にロック状態とする*/);
			}
#endif
		}
	}

	int HitTest(double x, double y) const
	{
		if (m_shape->IsLock() || m_shape->IsPositionLock() || !m_resizable) { return HIT_NONE; }

#ifndef OCHART_VIEWER
		double const size = PxToMM(4.5);

		OCRect const& rect = m_shape->m_rect;
		if (m_shape->m_selected) {
			if (y >= rect.top - size && y <= rect.top + size) {
				if (x >= rect.left - size && x <= rect.left + size) {
					return TopLeft;
				}
				if (rect.Width() > RH_THRESHOLD) {
					double xCenter = rect.left + rect.Width() / 2;
					if (x >= xCenter - size && x <= xCenter + size) {
						return TopMiddle;
					}
				}
				if (x >= rect.right - size && x <= rect.right + size) {
					return TopRight;
				}
			}
			if (y >= rect.bottom - size && y <= rect.bottom + size) {
				if (x >= rect.left - size && x <= rect.left + size) {
					return BottomLeft;
				}
				if (rect.Width() > RH_THRESHOLD) {
					double xCenter = rect.left + rect.Width() / 2;
					if (x >= xCenter - size && x <= xCenter + size) {
						return BottomMiddle;
					}
				}
				if (x >= rect.right - size && x <= rect.right + size) {
					return BottomRight;
				}
			}
			if (rect.Height() > RH_THRESHOLD) {
				double yCenter = rect.top + rect.Height() / 2;
				if (y >= yCenter - size && y <= yCenter + size) {
					if (x >= rect.left - size && x <= rect.left + size) {
						return MiddleLeft;
					}
					if (x >= rect.right - size && x <= rect.right + size) {
						return MiddleRight;
					}
				}
			}
		}
#endif
		return HIT_NONE;
	}

	bool Resize(int type, double x, double y) const
	{
		if (!m_resizable) { return false; }

		switch (type)
		{
		case TopLeft:
			m_shape->m_rect.left += x;
			m_shape->m_rect.top += y;
			break;
		case TopMiddle:
			m_shape->m_rect.top += y;
			break;
		case TopRight:
			m_shape->m_rect.right += x;
			m_shape->m_rect.top += y;
			break;
		case BottomLeft:
			m_shape->m_rect.left += x;
			m_shape->m_rect.bottom += y;
			break;
		case BottomMiddle:
			m_shape->m_rect.bottom += y;
			break;
		case BottomRight:
			m_shape->m_rect.right += x;
			m_shape->m_rect.bottom += y;
			break;
		case MiddleLeft:
			m_shape->m_rect.left += x;
			break;
		case MiddleRight:
			m_shape->m_rect.right += x;
			break;
		}

		return true;
	}

//private:
	T* m_shape;
	bool m_resizable;
};