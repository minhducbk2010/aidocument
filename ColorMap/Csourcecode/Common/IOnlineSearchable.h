/*!	@file
	@date	2008-09-12
	@author	wada

	@brief	サーチカーソルでオンラインサーチが行えることを示すインターフェイス
*/
#pragma once

struct DrawArgs;

//! オン・ラインサーチ
class IOnlineSearchable
{
public:
	virtual void GetPointByValue(OCRect const& rect, OCPoint const& target, OCPoint* point, OCPoint3D* value) = 0;
};

#include "IPlotData.h"
//! サーチカーソルの表示対象となりうるオブジェクト
class ISearchable
{
public:
	virtual void BeginSearch(DrawArgs const& arg, bool interpolate, ExclusionScale exclude, OCRect const& rect) = 0;
	virtual int GetSearchIndexByXPosition(OCRect const& rect, double x) const = 0;
	virtual bool GetPointBySearchIndex(OCRect const& rect, int index, OCPoint* point, OCPoint3D* value) = 0;
	virtual int OffsetIndex(int currentIndex, int offset) const = 0;
	virtual size_t GetAbsoluteDataIndex(OCRect const& rect, double x) const = 0;
	virtual size_t GetAbsoluteDataIndex(size_t relativeIndex) const = 0;
	virtual void EndSearch() = 0;

	virtual bool IsEnabled() const = 0;
};