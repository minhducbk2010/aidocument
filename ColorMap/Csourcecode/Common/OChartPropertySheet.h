#pragma once

class COChartPropertySheet : public CBCGPPropertySheet
{
public:
	COChartPropertySheet(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
		: CBCGPPropertySheet(pszCaption, pParentWnd, iSelectPage) {}
	virtual void BuildPropPageArray()
	{
		__super::BuildPropPageArray();

		BYTE* ppsp = (BYTE*)(m_psh.ppsp);
		for (int i = 0; i < m_pages.GetSize(); ++i) {
			PROPSHEETPAGE& psp = reinterpret_cast<PROPSHEETPAGE&>(*ppsp);
			CDialogTemplate temp(psp.pResource);

			std::wstring const& font = ::GetResourceString("ID_SOURCE_TEXT_MS_UI_GOTHIC");
			int fontSize = ::_ttoi(::GetResourceString("ID_SOURCE_TEXT_DLG_MINI_FONTSIZE"));

			temp.SetFont(font.c_str(), fontSize);
			psp.pResource = (PROPSHEETPAGE_RESOURCE)temp.Detach();
			ppsp += m_psh.ppsp->dwSize;
		}
	}
};