#pragma once

class IPlotDrawer;
class IMessageReceiver;

class IPlotFactory
{
public:
	virtual void Destroy() = 0;

	virtual unsigned long GetId() const = 0;
	virtual unsigned long GetDimension() const = 0;
	virtual unsigned long GetPlotCount(unsigned long dimension) const = 0;
	virtual IPlotDrawer* Create(HWND owner, IMessageReceiver* plot, unsigned long dimension, unsigned long index) const = 0;
//	virtual wchar_t const* GetPlotName(unsigned long dimension, unsigned long index) const = 0;
	virtual IPlotDrawer* CreateBuildUpper() const = 0;

protected:
	virtual ~IPlotFactory() = 0 {};
};