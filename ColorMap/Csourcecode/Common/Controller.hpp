/*!	@file
	@date	2008-09-30
	@author	wada

	@brief	グラフの操作を行うためのダイアログ
	3D、ウォーターフォールで使用
*/
#pragma once

//! 埋め込み式エディタ
template<class Graph, int ResourceID>
class CController : public ATL::CDialogImpl<CController<Graph, ResourceID>>
{
public:
	enum { IDD = ResourceID };

	enum Mode
	{
		Rotate,
		Zoom,
		Move,
		Cursor,
	};

	BEGIN_MSG_MAP_EX(CController)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		MSG_WM_PAINT(OnPaint)
		MSG_WM_ERASEBKGND(OnEraseBkgnd)
		MSG_WM_SIZE(OnSize)
		MSG_WM_KILLFOCUS(OnKillFocus)
		MSG_WM_LBUTTONDOWN(OnLButtonDown)
		MSG_WM_MOUSEMOVE(OnMouseMove)
		MSG_WM_LBUTTONUP(OnLButtonUp)
		MSG_WM_RBUTTONDOWN(OnRButtonDown)
		MSG_WM_RBUTTONUP(OnRButtonUp)
		MSG_WM_KEYDOWN(OnKeyDown)
		MSG_WM_SHOWWINDOW(OnShowWindow)
		MSG_WM_SETCURSOR(OnSetCursor)
	END_MSG_MAP()

	explicit CController(Graph* graph)
		: m_graph(graph)
		, m_mouseDown(false), m_mode(Rotate)
		, m_draggable(false)
		, m_leftClickSelect(false)
	{
	}

	void EnableLeftClickSelect(bool enable)
	{
		m_leftClickSelect = enable;
	}

	void EnableDragging(bool enable)
	{
		m_draggable = enable;
	}

	void GetBitmap(WTL::CBitmap& img)
	{
		WTL::CDC memDC;
		memDC.CreateCompatibleDC();

		GetBitmap(memDC, img);

		memDC.DeleteDC();
	}

	void GetBitmap(WTL::CDC& memDC, WTL::CBitmap& img)
	{
		CRect r;
		GetClientRect(&r);

		BITMAPINFOHEADER bmpInfo = {sizeof(BITMAPINFOHEADER)};
		bmpInfo.biWidth = r.Width();
		bmpInfo.biHeight = r.Height();
		bmpInfo.biPlanes = 1;
		bmpInfo.biBitCount = 24;
		bmpInfo.biCompression = BI_RGB;

		void* data = 0;
		HBITMAP bmp = img.CreateDIBSection(memDC.m_hDC, reinterpret_cast<BITMAPINFO*>(&bmpInfo), DIB_RGB_COLORS, &data, 0, 0);
		if (bmp) {
			HBITMAP oldBmp = memDC.SelectBitmap(bmp);
			m_graph->Capture(memDC.m_hDC, r, true);
			memDC.SelectBitmap(oldBmp);
		}
	}

	Mode GetMode() const { return m_mode; }
	void SetMode(Mode mode) { m_mode = mode; }

private:
	LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
	{
		return FALSE;
	}

	LRESULT OnShowWindow(BOOL visible, int /*lParam*/)
	{
		if (visible) {
			SetFocus();
			m_dc = GetDC();
		}
		else {
			ReleaseDC(m_dc);
			m_dc = 0;
		}
		return TRUE;
	}

	void OnPaint(HDC dc)
	{
		if (!m_dc) { return; }

		CRect r;
		GetClientRect(&r);
#if 0
		m_graph->Capture(m_dc, r, false);
#else
		// グラフの後ろに半透明のウィンドウ(Layerd window)があると
		// テキストがちらつく問題があるため、一度ビットマップに描いてから転送する
		// パフォーマンスが気になっていたが、意外と遅くない?
		WTL::CDC memDC;
		memDC.CreateCompatibleDC();
		WTL::CBitmap bmp;
		GetBitmap(bmp);
		HBITMAP oldBmp = memDC.SelectBitmap(bmp.m_hBitmap);
		::BitBlt(m_dc, -1, -1, r.Width(), r.Height(), memDC.m_hDC, 0, 0, SRCCOPY);
		memDC.SelectBitmap(oldBmp);
		memDC.DeleteDC();
#endif

		SetMsgHandled(FALSE); // これがないとツールチップが出なくなる
	}

	BOOL OnEraseBkgnd(HDC dc)
	{
		// 背景は描かない
		return TRUE;
	}

	void OnSize(UINT type, WTL::CSize size)
	{
	}

	void OnKillFocus(HWND /*prev*/)
	{
	}

	void OnLButtonDown(UINT flags, CPoint point)
	{
		m_click = point;
		m_mouseDown = true;

		bool hitMarker = false;
		if (m_mode == Cursor) {
			m_graph->SetCursor(point.x, point.y);
		}
		else if (m_leftClickSelect) {
			hitMarker = PickUp(point);
		}

		if (!hitMarker) {
			m_graph->Click(point.x, point.y);
		}
		Invalidate(FALSE);

		SetFocus();
		SetCapture();
	}

	void OnMouseMove(UINT flags, CPoint point)
	{
		if (m_mouseDown) {
			CRect r;
			GetClientRect(&r);

			if (m_draggable && ::GetAsyncKeyState(VK_SHIFT) &&
				m_graph->DragMarker(OCPoint(point.x - m_click.x, m_click.y - point.y))) {
			}
			else {
				switch (m_mode)
				{
				case Rotate:
					{
						float y = 360.0f * (static_cast<float>(point.x - m_click.x) / r.Width());
						float x = 360.0f * (static_cast<float>(m_click.y - point.y) / r.Height());
						m_graph->OffsetRotate(-x, 0, y);
					}
					break;
				case Zoom:
					{
						double offset = (point.y > m_click.y) ? -0.08 : 0.08;
						m_graph->OffsetScale(offset);
					}
					break;
				case Move:
					{
						double xOffset = (double)(point.x - m_click.x) / 100;
						double yOffset = -((double)(point.y - m_click.y) / 100);
						m_graph->Translate(xOffset, yOffset);
					}
					break;
				case Cursor:
					m_graph->SetCursor(point.x, point.y);
					break;
				}
			}
			m_click = point;
			Invalidate(FALSE);
		}
	}
	void OnLButtonUp(UINT flags, CPoint point)
	{
		ReleaseCapture();

		if (m_draggable) {
			m_graph->EndMarkerDrag();
		}

		m_mouseDown = false;
	}

	void OnRButtonDown(UINT flags, CPoint point)
	{
		if (!m_leftClickSelect) {
			PickUp(point);
			Invalidate(FALSE);
		}
		else {
			CPoint pt = point;
			ClientToScreen(&pt);
			WPARAM wParam = reinterpret_cast<WPARAM>(m_hWnd);
			LPARAM lParam = MAKELPARAM(pt.x, pt.y);
			GetParent().SendMessage(WM_CONTEXTMENU, wParam, lParam);
		}
	}

	bool PickUp(CPoint const& point)
	{
		CRect r;
		GetClientRect(&r);

		HDC dc = GetDC();
		bool const ret = m_graph->PickUpData(dc, r, point.x, point.y);
		ReleaseDC(dc);

		return ret;
	}

	void OnRButtonUp(UINT /*flags*/, CPoint /*point*/)
	{
	}

	void OnKeyDown(TCHAR key, UINT repeats, UINT code)
	{
		switch (key)
		{
		case VK_LEFT:
		case VK_RIGHT:
		case VK_UP:
		case VK_DOWN:
			switch (m_mode)
			{
			case Rotate:
				{
					float y = 360.0f * (key == VK_UP || key == VK_DOWN) ? 0 : key == VK_LEFT ? -5 : 5;
					float x = 360.0f * (key == VK_LEFT || key == VK_RIGHT) ? 0 : key == VK_DOWN ? -5 : 5;
					m_graph->OffsetRotate(-x, 0, y);
					Invalidate(FALSE);
				}
				break;
			case Zoom:
				if (key == VK_UP || key == VK_DOWN)
				{
					double offset = (key == VK_UP) ? -0.08 : 0.08;
					m_graph->OffsetScale(offset);
					Invalidate(FALSE);
				}
				break;
			case Move:
				{
					double const offset = 0.05;
					double xOffset = (key == VK_UP || key == VK_DOWN) ? 0 : key == VK_LEFT ? -offset : offset;
					double yOffset = (key == VK_LEFT || key == VK_RIGHT) ? 0 : key == VK_DOWN ? -offset : offset;
					m_graph->Translate(xOffset, yOffset);
					Invalidate(FALSE);
				}
				break;
			case Cursor:
				{
					int index, line;
					m_graph->GetCursorIndex(&index, &line);
					switch (key)
					{
					case VK_UP:
						//--index; //!@todo ウォーターフォールだとこっちの方が良い...
						++index;
						break;
					case VK_DOWN:
						//++index;
						--index;
						break;
					case VK_LEFT:
						//--line;
						++line;
						break;
					case VK_RIGHT:
						//++line;
						--line;
						break;
					}
					m_graph->SelectIndex(index, line);
					Invalidate(FALSE);
				}
				break;
			}
			break;
		}
	}

	LRESULT OnSetCursor(HWND, UINT, UINT)
	{
		SetCursor(m_graph->GetCursor());
		//SetMsgHandled(FALSE);
		return 0;
	}

	//void OnKillFocus(HWND hWnd)
	//{
	//	m_graph->CloseViewer();
	//}

	//void OnContextMenu(HWND hWnd, CPoint pt)
	//{
	//	SetMsgHandled(FALSE);

	//	MessageBox(_T("OnContextMenu"), _T(""), MB_OK);

	//	WPARAM wParam = reinterpret_cast<WPARAM>(hWnd);
	//	LPARAM lParam = MAKELPARAM(pt.x, pt.y);
	//	GetParent().SendMessage(WM_CONTEXTMENU, wParam, lParam);
	//}

private:
	Graph* m_graph;
	Mode m_mode;

	HDC m_dc;

	bool m_draggable;
	bool m_mouseDown;
	CPoint m_click;

	// option
	bool m_leftClickSelect; //!< 左クリックでマーカーを選択するかどうか
};