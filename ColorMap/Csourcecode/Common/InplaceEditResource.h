//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by InPlaceEditor.rc
//
#define IDD_INPLACE_EDITOR              2001
#define IDC_INPLACE_RICHEDIT            2003
#define IDR_INPLACE_EDIT_POPUP_MENU     2013
#define IDC_INPLACE_RICHEDIT_INVISIBLE  2014
//#define IDD_SAMPLE                      2002
//#define IDC_BOLD                        2004
//#define IDC_ITALIC                      2005
//#define IDC_UNDERLINE                   2006
//#define IDC_STRIKE                      2007
//#define IDC_SUP                         2008
//#define IDC_SUB                         2009
//#define IDC_SHADOW                      2010
//#define IDC_COMBO_COLOR                 2011
//#define IDC_COMBO_SIZE                  2012

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
