#pragma once

#include "IShape.h"
#include "IMessageReceiver.h"
#include "Message.h"
#include "../Library/PluginUtility/InPlaceEditor.hpp"
#include "StringUtil.hpp"
#include "StringDefine.h"

#define SPACE _T(' ')
#define TAB _T('\t')
#define NULL_STR _T('\0')

//! EditableShape.cppから移動
inline bool EndPtrCheck(const wchar_t *endptr)
{
	if (endptr) {
		while (*endptr == SPACE || *endptr == TAB) {
			++endptr;
		}
		return *endptr == NULL_STR;
	}
	else {
		return true;
	}
}

inline std::wstring GetLinkText(IMessageReceiver* owner, int fileId, int linkId, StringFormat format = StringFormat::Digit, int decimalPlace = -1, bool useComma = false, bool forPrint = false)
{
	std::wstring text = forPrint ? GetPrintLinkTextImpl(owner, fileId, linkId) : GetLinkTextImpl(owner, fileId, linkId);
	wchar_t* err = 0;
	double value = ::wcstod(text.c_str(), &err);
	if (text != _T("") && EndPtrCheck(err)) {
		std::wstring const buf = DblToStr(value, format, decimalPlace);

		useComma = useComma && format == StringFormat::Digit;
		return useComma	? SetComma(buf) : buf;
	}
	return text;
}

// エディタに入ったテキストをリンクテキストに変換する
inline void ConvertLinkTextForEditor(IMessageReceiver* owner, CEditor* editor, StringFormat format, int decimalPlace, bool useComma, bool forPrint = false)
{
	// RichEdit20Aをunicode環境で使うと、基本的にテキストの読み出しはSJISになって、
	// 置き換え等は、unicodeで指定する必要がある

	wchar_t const* bTag = _T("<");
	wchar_t const* eTag = _T(">");
	FINDTEXT ft = {0, -1, bTag};
	editor->m_edit->SetSel(0, 0);

	std::vector<wchar_t> tmpstr;
	std::wstring dst;
	std::wstring strFileId;
	DWORD flag = FR_DOWN ;//| FR_WHOLEWORD;
	LONG begin = (LONG)::SendMessage(editor->m_edit->m_hWnd, EM_FINDTEXTW, flag, (LPARAM) & ft);
	while (begin != -1) {
		ft.lpstrText = eTag;
		LONG end = (LONG)::SendMessage(editor->m_edit->m_hWnd, EM_FINDTEXTW, flag, (LPARAM) & ft);
		if (end == -1 || end - begin <= 1) { break; }
		editor->m_edit->SetSel(begin + 1, end);
		tmpstr.resize(end - begin + 4);
		// RichEdit20Aからunicodeで文字を読みだすため、GetTextExを使用する
		GETTEXTEX gte = {0};
		gte.cb = tmpstr.size() * sizeof(wchar_t);	// 読み出しバッファのサイズ(バイト単位で指定すること)
		gte.flags = GT_SELECTION;	// 選択中の文字を読む
		gte.codepage = 1200;	// unicode指定
		editor->m_edit->GetTextEx(&gte, &tmpstr[0]);

		//＜＞の中に"-"があるかを判定
		bool checkText = false;  //"-"の有無フラグ
		int checkNum = 0;

		strFileId.clear();

		for(size_t i=0 ;i < tmpstr.size(); i++)
		{
			if(tmpstr[i] == '-')
			{
				checkText = true;
				checkNum = i;   //"-"の位置
			}
		}
		
		strFileId.resize(checkNum);		//ファイルID配列を"-"の前までと"\0"が入るサイズに確保

		//ファイルIDを取り出す
		for(int i = 0; i< checkNum; i++)
		{
			strFileId[i] = tmpstr[i];
		}

		strFileId[checkNum] = '\0';

		wchar_t *endptr = NULL;
		wchar_t *endptr_file = NULL;

		long linkId = ::wcstol(&tmpstr[checkNum + 1], &endptr, 10);					//"-"の次がリンクテキストID
		long fileId = ::wcstol(&strFileId[0] , &endptr_file, 10);  //"-"の前がファイルID

		//＜＞内のIDに該当するリンクテキストがある場合
		if (linkId > 0 && fileId > 0 && EndPtrCheck(endptr) && checkText == true && EndPtrCheck(endptr_file)) {	   //<>内が　1.数字　2."-"あり　
			//引数にファイルIDもプラス
			dst = GetLinkText(owner, fileId, linkId, format, decimalPlace, useComma, forPrint);

			// リンクテキストを置き換える
            editor->m_edit->SetSel(begin , end + 1);
			editor->m_edit->ReplaceSel(dst.c_str());
			editor->m_edit->GetSel(begin, end);	// endを検索開始位置(置き換え最終文字の次の文字)に進める
		}
		else
		{
			end++;	// endを検索開始位置(">"の次の文字)に進める
		}

		ft.chrg.cpMin = end;
		ft.lpstrText = bTag;
		begin = (LONG)::SendMessage(editor->m_edit->m_hWnd, EM_FINDTEXTW, flag, (LPARAM) & ft);
	}

	// カーソル位置を先頭に戻す (Dezie 2291 対応)
	// リンクテキストの置き換えが生じるので、選択領域、カーソル位置のバックアップはしない
	editor->m_edit->SetSel(0, 0);
}

// エディタに入ったテキストをリンクテキストに変換する(引数がIShape)
inline void ConvertLinkTextForEditor(IShape* shape, CEditor* editor, StringFormat format, int decimalPlace, bool useComma)
{
	ConvertLinkTextForEditor(shape->GetOwner(), editor, format, decimalPlace, useComma);
}

// エディタに入ったテキストを印刷用リンクテキストに変換する(引数がIShape)
inline void ConvertPrintLinkTextForEditor(IShape* shape, CEditor* editor, StringFormat format, int decimalPlace, bool useComma)
{
	ConvertLinkTextForEditor(shape->GetOwner(), editor, format, decimalPlace, useComma, true);
}

// std::wstringに入った文字列をリンクテキストに変換する
inline std::wstring ConvertLinkText(IMessageReceiver* owner, std::wstring str, StringFormat format = StringFormat::Digit, int decimalPlace = -1, bool useComma = false, bool isAE = false, bool forPrint = false)
{
	std::wstring::size_type begin, end;
	begin = end = 0;
	std::wstring convertText = STRING_NOTHING;

	format = isAE ? StringFormat::NotExponent : format;

	while(1)
	{
		// <を探す
		std::wstring::size_type findBegin = str.find(STIRNG_LEFT_ANGLE, begin);
		if (findBegin == std::wstring::npos) {
			convertText += str.substr(begin);
			break;
		}

		// <の後の>を探す
		std::wstring::size_type findEnd = str.find(STRING_RIGHT_ANGLE, findBegin);
		if (findEnd == std::wstring::npos) {
			convertText += str.substr(begin);
			break;
		}

		convertText += str.substr(begin, findBegin - begin);
		begin = findBegin;
		end = findEnd;
		
		// <>内の-を探す
		std::wstring::size_type findHyphen = str.find_first_of(STRING_HYPHEN, begin + 1, end - begin - 1);
		if (findHyphen >= end || findHyphen == std::wstring::npos ) { // -がないときに時々nposを返さないで、endの値を返すときがある
			convertText += str.substr(begin, end - begin + 1);
			begin = end + 1;
			continue;
		}

		// <と-の間と、-と>の間が整数か確かめる
		wchar_t *endptr = NULL;
		wchar_t *endptr_file = NULL;

		long linkId = ::wcstol(str.substr(findHyphen + 1, end - findHyphen - 1).c_str(), &endptr, 10);					//"-"の次がリンクテキストID
		long fileId = ::wcstol(str.substr(begin + 1, findHyphen - begin - 1).c_str() , &endptr_file, 10);  //"-"の前がファイルID

		//＜＞内のIDに該当するリンクテキストがある場合
		if (linkId > 0 && fileId > 0 && EndPtrCheck(endptr) && EndPtrCheck(endptr_file)) {
			if (isAE) { convertText += STRING_LEFT; }
			convertText += GetLinkText(owner, fileId, linkId, format, decimalPlace, useComma, forPrint);
			if (isAE) { convertText += STRING_RIGHT; }
			begin = end + 1;
		}
		else {
			convertText += str.substr(begin, end - begin + 1);
			begin = end + 1;
			continue;
		}
	}

	return convertText;
}

// std::wstringに入った文字列をリンクテキストに変換する(引数がIShape用)
inline std::wstring ConvertLinkText(IShape* shape, std::wstring str, StringFormat format = StringFormat::Digit, int decimalPlace = -1, bool useComma = false, bool isAE = false)
{
	return ConvertLinkText(shape->GetOwner(), str, format, decimalPlace, useComma, false);
}

// std::wstringに入った文字列を印刷用リンクテキストに変換する(引数がIShape用)
inline std::wstring ConvertPrintLinkText(IShape* shape, std::wstring str, StringFormat format = StringFormat::Digit, int decimalPlace = -1, bool useComma = false, bool isAE = false)
{
	return ConvertLinkText(shape->GetOwner(), str, format, decimalPlace, useComma, false, true);
}

// 条件付き書式の計算用にリンクテキストを変換する
inline std::wstring ConvertLinkTextForAE(IMessageReceiver* owner, std::wstring str)
{
	return ConvertLinkText(owner, str, StringFormat::NotExponent, -1, false, true);
}

// 条件付き書式の計算用にリンクテキストを変換する
inline std::wstring ConvertLinkTextForAE(IShape* shape, std::wstring str)
{
	return ConvertLinkText(shape->GetOwner(), str, StringFormat::NotExponent, -1, false, true);
}