#pragma once

#include <boost/foreach.hpp>
#include "find_file.hpp"
#include <shlobj.h>
#include <imagehlp.h>
#pragma comment(lib, "imagehlp.lib")
#include <boost/algorithm/string/trim.hpp>
#include "IMessageReceiver.h"

namespace
{
int CountFile(std::wstring const& pattern)
{
	int count = 0;
	find_file::find_file_range glob(pattern.c_str());
	BOOST_FOREACH(WIN32_FIND_DATA& file, glob) {
		file;
		++count;
	}
	return count;
}

int CountChildDirectory(std::wstring const& dir)
{
	int count = 0;
	find_file::find_file_range glob((dir + _T("*")).c_str());
	BOOST_FOREACH(WIN32_FIND_DATA& file, glob) {
		if (file.cFileName[0] == '.') { continue; }
		if ((file.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY){
			TCHAR odf[_MAX_PATH] = {};
			::_tcscpy_s(odf, dir.c_str());
			::PathAppend(odf, file.cFileName);
			::PathAppend(odf, _T("Index.odf"));
			if (::_taccess_s(odf, 0) == 0) {
				++count;
			}
		}
	}
	return count;
}


bool IsEnableFileName(std::wstring const& path)
{
	if (path.empty()) { return false; }

	for (size_t i = 0; i < path.size(); ++i) {
		UINT const c = ::PathGetCharType(path[i]);
		if (c == GCT_INVALID || (c & GCT_WILD) || (c & GCT_SEPARATOR)) { return false; }
	}

	return true;
}

std::wstring GetFilterPath(HWND owner, IMessageReceiver* receiver, std::wstring name)
{
	TCHAR dir[_MAX_PATH];
	SHGetSpecialFolderPath(NULL, dir, CSIDL_APPDATA, TRUE);
	::PathAppend(dir, _T("Onosokki Common\\OSeries\\ImportFilter\\"));

	if (!::PathFileExists(dir)) {
		// ディレクトリがなければ作る
		::MakeSureDirectoryPathExists(::ToMultiByteString(dir).c_str());
	}

	// 左右の空白は取り除く
	boost::trim(name);

	if (name.size() == 0) {
		std::wstring msg = ::GetResourceString(receiver, "ID_SOURCE_TEXT_ENTERNAME");
		MessageBox(owner, msg.c_str(), ::GetResourceString(receiver,"ID_SOURCE_TEXT_O_CHART"), MB_OK|MB_ICONWARNING);
		return std::wstring();
	}
	if (name.size() >= 17) {
		std::wstring msg = ::GetResourceString(receiver,"ID_SOURCE_TEXT_FILTERNAME");
		MessageBox(owner, msg.c_str(), ::GetResourceString(receiver,"ID_SOURCE_TEXT_O_CHART"), MB_OK|MB_ICONWARNING);
		return std::wstring();
	}

	if (!IsEnableFileName(name.c_str())) {
		std::wstring msg = ::GetResourceString(receiver,"ID_SOURCE_TEXT_CHANGENAME");
			MessageBox(owner, msg.c_str(), ::GetResourceString(receiver,"ID_SOURCE_TEXT_O_CHART"), MB_OK|MB_ICONWARNING);
		return std::wstring();
	}

	TCHAR path[_MAX_PATH];
	::_stprintf_s(path, _T("%s%s.ocf"), dir, name.c_str());

	if (::PathFileExists(path)) {
		std::wstring msg = ::GetResourceString(receiver,"ID_SOURCE_TEXT_SAVEFILTER");
		if (MessageBox(owner, msg.c_str(), ::GetResourceString(receiver,"ID_SOURCE_TEXT_O_CHART"), MB_YESNO|MB_ICONWARNING) != IDYES) {
			return std::wstring();
		}
	}
	else if (CountFile(std::wstring(dir) + _T("*.ocf")) >= 10) {
		std::wstring msg = ::GetResourceString(receiver,"ID_SOURCE_TEXT_NOSAVEFLTER");
		MessageBox(owner, msg.c_str(), ::GetResourceString(receiver,"ID_SOURCE_TEXT_O_CHART"), MB_OK|MB_ICONWARNING);
		return std::wstring();
	}

	return path;
}

//! 文字列をdoubleに変換
bool StringToDouble(std::wstring const& str, double& ret)
{
	if (str.empty()) { return false; }

	TCHAR* end = _T("");
	ret = ::_tcstod(str.c_str(), &end);
	CString strEnd = end;

	return (strEnd == _T(""));
}

}