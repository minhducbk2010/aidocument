/*!	@file
	@date	2008-10-25
	@author	wada

	@brief	メッセージボックス表示関数
*/

#pragma once

#include <tchar.h>
#include "Message.h"

namespace oc
{

//static bool g_displayed_message = true;
class MessageManager
{
public:
	MessageManager() : m_displayed_message(true) {}

	static MessageManager& GetInstance()
	{
		static MessageManager instance;
		return instance;
	}

	void SetDisplayedMessage(bool disp)
	{
		m_displayed_message = disp;
	}

	bool GetDisplayedMessage()
	{
		return m_displayed_message;
	}

	int MessageBox(HWND wnd, LPCTSTR text, LPCTSTR caption, UINT type)
	{
		if (m_displayed_message) {
			
			return ::MessageBox(wnd, text, caption, type);
		}
		return IDOK;
	}

private:
	MessageManager(MessageManager&);
	void operator=(MessageManager&);

private:
	bool m_displayed_message;
};

static inline void SetDisplayedMessage(bool disp)
{
	MessageManager::GetInstance().SetDisplayedMessage(disp);
}

static inline bool GetDisplayedMessage()
{
	return MessageManager::GetInstance().GetDisplayedMessage();
}

/*!	@brief	メッセージボックスを表示します
	@param[in]	wnd		オーナーウィンドウのハンドル
	@param[in]	text	メッセージボックス内のテキスト
	@param[in]	caption	メッセージボックスのタイトル
	@param[in]	type	メッセージボックスのスタイル
*/
static inline int MessageBox(HWND wnd, LPCTSTR text, LPCTSTR caption, UINT type)
{
	std::wstring msg = text;
	std::wstring captionStr = caption;
	return MessageManager::GetInstance().MessageBox(wnd, msg.c_str(), captionStr.c_str(), type);
}

//static inline int MessageBox(HWND wnd, LPCTSTR text, UINT type)
//{
//	std::wstring msg = text;
//	std::wstring captionStr = GetResourceString("ID_SOURCE_TEXT_O_CHART");
//	return oc::MessageBox(wnd, msg.c_str(), caption.c_str(), type);
//}

#if 0
int MessageBox(HWND wnd, UINT text, UINT type)
{
	TCHAR buffer[256];
	::LoadString(::AfxGetInstanceHandle(), text, buffer, _countof(buffer));
	MessageBox(wnd, buffer, ::GetResourceString("ID_SOURCE_TEXT_O_CHART"), rect.left), type);
}
#endif

}