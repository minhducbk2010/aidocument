/*!	@file
	@date	2008-03-27
	@author	wada

	@brief	プラグインの読み込みクラス
*/
#pragma once

#include <map>

typedef long (WINAPI * GetIdF)();

class OCPluginLoader
{
public:
	//! Destructor
	virtual ~OCPluginLoader()
	{
		DestroyInstance();
	}

protected:
	//! 読み込んだDLLがあれば、それを解放する
	void DestroyInstance()
	{
		for (module_map::iterator it = m_dll.begin(); it != m_dll.end(); ++it) {
			::FreeLibrary(it->second);
		}
		m_dll.clear();
	}

	void DoLoadPlugin(wchar_t const* category)
	{
		CString path;
		path.Format(_T("%sPlug-in\\%s\\*.dll"), GetDirectory(), category);
		CFileFind finder;
		BOOL exits = finder.FindFile(path);
		while (exits)
		{
			exits = finder.FindNextFile();
			if (finder.IsDots() || finder.IsDirectory()) { continue; }

			CString file = finder.GetFilePath();
			HMODULE module = ::LoadLibrary(file);
			if (module != 0) {
				GetIdF getId = (GetIdF)::GetProcAddress(module, "GetId");
				int const id = getId();

				ASSERT(m_dll.find(id) == m_dll.end() && ::GetResourceString("ID_SOURCE_TEXT_DUP_P_ID"));

				m_dll[id] = module;
			}
		}
	   finder.Close();
	}

	CString GetDirectory()
	{
		wchar_t buff[_MAX_PATH];
		::GetModuleFileName(::AfxGetInstanceHandle(), buff, _MAX_PATH);

		wchar_t drive[_MAX_DRIVE], dir[_MAX_DIR], fname[_MAX_FNAME], ext[_MAX_EXT];
		::_wsplitpath_s(buff, drive, _MAX_DRIVE, dir, _MAX_DIR, fname, _MAX_FNAME, ext, _MAX_EXT);

		wchar_t rootDir[_MAX_PATH];
		::_wmakepath_s(rootDir, _MAX_PATH, drive, dir, NULL, NULL);

		return rootDir;
	}

protected:
	typedef std::map<int, HMODULE> module_map; // <Id, ModuleHandle>
	module_map m_dll;
};