/*!	@file
	@date	2008-06-24
	@brief	プロット関連の定数定義ファイル
*/
#pragma once

namespace plot
{

enum ClippingType
{
	None,		//!< クリッピングしない
	XClip,		//!< X方向にのみクリッピング
	YClip,		//!< Y方向にのみクリッピング
	AllClip,	//!< 全方向にクリッピング
};

}