/*!	@file
	@date	2008-07-30
*/
#pragma once

#include <comutil.h> // _variant_t

class IItem;

//! ファイルインターフェイス
class IFile
{
public:
	virtual ~IFile() = 0 {};

	virtual unsigned int GetId() const = 0;

	virtual size_t ItemCount() const = 0;
	virtual size_t DataCount() const = 0;
	virtual size_t CalcResultItemCount() const = 0;

	virtual IItem* Item(size_t index) const = 0;

	virtual TCHAR const* GetFileName() const = 0;

	virtual int GetImporterId() const = 0;
	virtual TCHAR const* GetDataFileDirectory() const = 0;
};

namespace File {

//! 項目種別
enum ItemKind
{
	Numeric,	//!< 数値項目
	Text,		//!< 文字列項目
	Calc,		//!< 演算項目
};

}

//! 項目インターフェイス
class IItem
{
public:
	virtual ~IItem() = 0 {};

	virtual void SetFileName(wchar_t const* fileName) = 0;
	virtual wchar_t const* GetFileName() const = 0;

	virtual int GetId() const = 0;
	virtual File::ItemKind GetItemKind() const = 0;

	//! 項目名称を取得します
	virtual wchar_t const* GetName() const = 0;
	//! 項目名称を設定します
	virtual void SetName(wchar_t const* name) = 0;

	//! 単位名称を取得します
	virtual wchar_t const* GetUnit() const = 0;
	//! 単位名称を設定します
	virtual void SetUnit(wchar_t const* unit) = 0;

	//!@todo これファイルに移動する
	virtual size_t GetCommentRows() const = 0;
	virtual void AddComment(wchar_t const* comment) = 0;
	virtual void SetComment(size_t index, wchar_t const* comment) = 0;
	virtual wchar_t const* GetComment(size_t row) const = 0;

	virtual void Reserve(size_t size) = 0;

	//! データの末尾に引数の値を追加します
	virtual void AddFloatData(float value) = 0;
	virtual void Assign(float const* first, float const* last) = 0;

	//! データの任意の位置に引数の値を追加します
	virtual void InsertData(size_t index, _variant_t value) = 0;

	//! 任意の位置のデータを削除します
	virtual void DeleteData(size_t index) = 0;

	virtual void EnableLine(size_t index, bool enable) = 0;
	virtual bool IsLineEnable(size_t index) const = 0;
	virtual size_t GetEffectiveDataCount() const = 0;

	//! 保持しているデータ点数(=行数)を取得します
	virtual size_t GetDataCount() const = 0;

	virtual float GetFloatData(size_t index) const = 0;
	virtual wchar_t const* GetStringData(size_t index) const = 0;

	//! 指定行のデータを取得します
	virtual bool IsStringItem(size_t index) const = 0;

	//! このアイテムの保存ファイルを削除します
	virtual void DeleteItem() = 0;
};
