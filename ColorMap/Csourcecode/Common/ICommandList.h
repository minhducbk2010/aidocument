/*! @file
    @date   2008-05-20
    @author wada
*/
#pragma once

#include <string>

class ICommandList
{
public:
	virtual void Undo() = 0;
	virtual void Redo() = 0;

	virtual size_t GetUndoCommandCount() const = 0;
	virtual std::wstring GetUndoCommandName(size_t index) const = 0;
	virtual size_t GetRedoCommandCount() const = 0;
	virtual std::wstring GetRedoCommandName(size_t index) const = 0;
};