/*!	@file
	@brief	O-Chartで使用するリストコントロールの実装
			リストの外観を統一するため、リストはすべてこのクラスもしくはこのクラスから派生したものを使用してください
*/
#pragma once

#include "utility.h"

#ifdef IMPORTER
#define BASECLASS CListCtrl
#else
#define BASECLASS CBCGPListCtrl
#endif

namespace
{

///////////////////////////////////////////////////////////////////////////////
// 宣言

class COChartListCtrl : public BASECLASS
{
	DECLARE_DYNAMIC(COChartListCtrl)
public:
	COChartListCtrl();
	~COChartListCtrl();
	virtual void ManualInitialize();

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);

	virtual void PreSubclassWindow();
	

private:
	CImageList m_imageList;
};

///////////////////////////////////////////////////////////////////////////////
// 実装

IMPLEMENT_DYNAMIC(COChartListCtrl, BASECLASS)

COChartListCtrl::COChartListCtrl()
{
}

COChartListCtrl::~COChartListCtrl()
{
}

void COChartListCtrl::ManualInitialize()
{
	if (m_imageList.GetSafeHandle()) {
		m_imageList.DeleteImageList();
	}
	// ダミーのイメージリスト(行の高さを調整するのに使用)
	m_imageList.Create(1, static_cast<int>(15 * GetDpiRatio()), ILC_COLOR, 0, 0);
	SetImageList(&m_imageList, LVSIL_STATE);
}

void COChartListCtrl::PreSubclassWindow()
{
	__super::PreSubclassWindow();

	ManualInitialize();
}

BEGIN_MESSAGE_MAP(COChartListCtrl, BASECLASS)
	ON_WM_CREATE()
END_MESSAGE_MAP()

int COChartListCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	int const ret = __super::OnCreate(lpCreateStruct);

	return ret;
}

}