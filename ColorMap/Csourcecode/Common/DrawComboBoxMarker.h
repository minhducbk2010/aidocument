#pragma once

inline POINT MakePoint(int x, int y)
{
	POINT p = {x, y};
	return p;
}

static void __stdcall DrawComboMarker(HDC dc, int index, RECT const& r, UINT state)
{
	bool const selected = state & ODA_FOCUS;

	HPEN pen = ::CreatePen(PS_SOLID, 1, selected ? 0xffffff : 0);
	HPEN oldPen = (HPEN)::SelectObject(dc, pen);
	HBRUSH brush = (HBRUSH)::GetStockObject(NULL_BRUSH);
	HBRUSH oldBrush = (HBRUSH)::SelectObject(dc, brush);

	RECT rect = r;
	rect.top += 2;
	rect.bottom -= 2;
	int width = rect.bottom - rect.top;
	if (width % 2 != 0) {
		--width;
	}
	//rect.left = rect.right - (rect.right - rect.left) / 2 - width / 2;
	rect.left += 5;
	rect.right = rect.left + width;

	int const size = width;
	int const cx = rect.left + size / 2;
	int const cy = rect.top + size / 2;
	std::vector<POINT> pts;

	switch (index)
	{
	case Marker::Dot:
		{
			HGDIOBJ br = ::GetStockObject(BLACK_BRUSH);
			HGDIOBJ oldBr = ::SelectObject(dc, br);
			::Ellipse(dc, cx - 1, cy - 1, cx + 1, cy + 1);
			::SelectObject(dc, oldBr);
		}
		break;
	case Marker::Circle:
		{
			::Ellipse(dc, rect.left, rect.top, rect.right, rect.bottom);
		}
		break;
	case Marker::Rectangle:
		{
			::Rectangle(dc, rect.left, rect.top, rect.right, rect.bottom);
		}
		break;
	case Marker::Diamond:
		{
			pts.push_back(MakePoint(rect.left, cy));
			pts.push_back(MakePoint(cx, rect.top));
			pts.push_back(MakePoint(rect.right, cy));
			pts.push_back(MakePoint(cx, rect.bottom));
			::Polygon(dc, &pts[0], static_cast<int>(pts.size()));
		}
		break;
	case Marker::TriangleDown:
		{
			pts.push_back(MakePoint(rect.left, rect.top));
			pts.push_back(MakePoint(rect.right, rect.top));
			pts.push_back(MakePoint(cx, rect.bottom));
			::Polygon(dc, &pts[0], static_cast<int>(pts.size()));
		}
		break;
	case Marker::Triangleup:
		{
			pts.push_back(MakePoint(cx, rect.top));
			pts.push_back(MakePoint(rect.left, rect.bottom));
			pts.push_back(MakePoint(rect.right, rect.bottom));
			::Polygon(dc, &pts[0], static_cast<int>(pts.size()));
		}
		break;
	case Marker::Cross:
		{
			POINT p1, p2, p3, p4;
			p1.x = rect.left;
			p1.y = rect.top;
			p2.x = rect.right + 1;
			p2.y = rect.bottom + 1;
			p3.x = rect.left;
			p3.y = rect.bottom;
			p4.x = rect.right + 1;
			p4.y = rect.top - 1;
			::MoveToEx(dc, p1.x, p1.y, 0);
			::LineTo(dc, p2.x, p2.y);
			::MoveToEx(dc, p3.x, p3.y, 0);
			::LineTo(dc, p4.x, p4.y);
		}
		break;
	case Marker::Plus:
		{
			POINT p1, p2, p3, p4;
			p1.x = cx;
			p1.y = rect.top;
			p2.x = cx;
			p2.y = rect.bottom + 1;
			p3.x = rect.left;
			p3.y = cy;
			p4.x = rect.right + 1;
			p4.y = cy;
			::MoveToEx(dc, p1.x, p1.y, 0);
			::LineTo(dc, p2.x, p2.y);
			::MoveToEx(dc, p3.x, p3.y, 0);
			::LineTo(dc, p4.x, p4.y);
		}
		break;
	case Marker::DoubleCircle:
		{
			::Ellipse(dc, rect.left, rect.top, rect.right, rect.bottom);
			
			::InflateRect(&rect, -2, -2);
			::Ellipse(dc, rect.left, rect.top, rect.right, rect.bottom);
		}
		break;
	}

	::DeleteObject(::SelectObject(dc, oldPen));
	::SelectObject(dc, oldBrush);
}