#pragma once

#include <vector>
#include <windows.h>

class OCBufferStream
{
public:
	virtual ~OCBufferStream() = 0 {};

	virtual void Widening(__int64 size) = 0;
	virtual void Narrowing(__int64 size) = 0;
	virtual void* Map(__int64 offsetBytes = 0) const = 0;
};

class OCMemoryMappedFile : public OCBufferStream
{
public:
	OCMemoryMappedFile()
		: m_file(INVALID_HANDLE_VALUE)
	{
		m_fileSize.QuadPart = 512 + 512 + 1;
		Init(m_file);
	}

	OCMemoryMappedFile(TCHAR const* fileName)
	{
		m_file = ::CreateFile(fileName, GENERIC_READ|GENERIC_WRITE,
			FILE_SHARE_READ|FILE_SHARE_WRITE, 0, OPEN_EXISTING, 0, 0);
		Init(m_file);
	}

	OCMemoryMappedFile(HANDLE handle) : m_file(0)
	{
		Init(handle);
	}

	~OCMemoryMappedFile()
	{
		Close();

		if (m_file) {
			::CloseHandle(m_file);
			m_file = 0;
		}
	}

	void Widening(__int64 size)
	{
		Close();

		m_fileSize.QuadPart += size;

		m_map = ::CreateFileMapping(m_file, 0, PAGE_READWRITE, m_fileSize.HighPart, m_fileSize.LowPart, 0);
		m_address = ::MapViewOfFile(m_map, FILE_MAP_ALL_ACCESS, 0, 0, 0);
	}

	void Narrowing(__int64 size)
	{
		Close();

		m_fileSize.QuadPart -= size;

		m_map = ::CreateFileMapping(m_file, 0, PAGE_READWRITE, m_fileSize.HighPart, m_fileSize.LowPart, 0);
		m_address = ::MapViewOfFile(m_map, FILE_MAP_ALL_ACCESS, 0, 0, 0);
	}

	void Close()
	{
		::FlushViewOfFile(m_address, 0);
		::UnmapViewOfFile(m_address);
		::CloseHandle(m_map);

		m_address = 0;
		m_map = 0;
	}

	void* Map(__int64 offsetBytes = 0) const
	{
		return reinterpret_cast<void*>(reinterpret_cast<BYTE*>(m_address) + offsetBytes);
	}

	__int64 GetFileSize() const
	{
		return m_fileSize.QuadPart;
	}

private:
	void Init(HANDLE fileHandle)
	{
		if (fileHandle == INVALID_HANDLE_VALUE) {
			m_map = ::CreateFileMapping(fileHandle, 0, PAGE_READWRITE, m_fileSize.HighPart, m_fileSize.LowPart, 0);
		}
		else {
			m_map = ::CreateFileMapping(fileHandle, 0, PAGE_READWRITE, 0, 0, 0);
		}
		m_address = ::MapViewOfFile(m_map, FILE_MAP_ALL_ACCESS, 0, 0, 0);
		::GetFileSizeEx(fileHandle, &m_fileSize);
	}

private:
	HANDLE m_file;
	HANDLE m_map;
	void* m_address;
	LARGE_INTEGER m_fileSize;
};

template<class T>
class OCArrayStream : public OCBufferStream
{
public:
	virtual void Widening(__int64 size)
	{
		m_buf.resize(m_buf.size() + size / sizeof(T));
	}

	virtual void Narrowing(__int64 size)
	{
		m_buf.resize(m_buf.size() - size / sizeof(T));
	}

	virtual void* Map(__int64 offsetBytes = 0) const
	{
		return const_cast<void*>(reinterpret_cast<void const*>(&m_buf[0]));
	}

private:
	std::vector<T> m_buf;
};
