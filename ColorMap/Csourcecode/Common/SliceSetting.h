#pragma once

#include <windows.h>

class IShape;

//! スライスレベル(等高線)の設定
struct SliceLevel
{
    bool visible;       //!< ライン表示 ON/OFF
    double level;       //!< レベル値
    bool showLevel;     //!< 数値表示 ON/OFF
    int style;          //!< 線種
    float width;        //!< 線の太さ
    COLORREF color;     //!< 線の色

    SliceLevel()
        : visible(true), level(DBL_MAX)
		, showLevel(false), style(PS_SOLID), width(1), color(0)
    {
    }

	bool operator==(SliceLevel const& level) const
	{
		return this->visible == level.visible &&
			this->level == level.level &&
			this->showLevel == level.showLevel &&
			this->style == level.style &&
			this->width == level.width &&
			this->color == level.color;
	}
};

#pragma pack(push, 1)
struct ShapeSliceList
{
	ShapeSliceList()
	{
	}

	ShapeSliceList(ShapeSliceList const& from)
		: size(from.size), minimum(from.minimum), maximum(from.maximum), division(from.division)
		, legendIndex(from.legendIndex), limitedRange(from.limitedRange), autoSetting(from.autoSetting)
	{
		std::copy(from.levels, from.levels + from.size, levels);
	}

	~ShapeSliceList()
	{
	}

	ShapeSliceList& operator=(ShapeSliceList const& from)
	{
		size = from.size;
		minimum = from.minimum;
		maximum = from.maximum;
		division = from.division;
		legendIndex = from.legendIndex;
		limitedRange = from.limitedRange;
		autoSetting = from.autoSetting;

		//levels = new SliceLevel[from.size];
		std::copy(from.levels, from.levels + from.size, levels);
		return *this;
	}

	IShape* plot;
	SliceLevel levels[101];
	size_t size;
	double minimum;
	double maximum;
	double division;
	size_t legendIndex;
	BOOL limitedRange;	//!< データ範囲で制限
	BOOL autoSetting;	//!< 自動設定([リセット]したら真)
};
#pragma pack(pop)
