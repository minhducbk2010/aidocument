/*!	@file
	@date	2008-09-03
	@author	wada
*/
#pragma once

class IFilter;

struct AttachAxis
{
	size_t itemId;		//!< 軸に割り当てるデータ項目のインデックス(0〜)
	size_t axisId;		//!< 割り当て対象の軸ID
};

struct AddPlotSetting
{
	AddPlotSetting() : filter(0)
	{}
	virtual ~AddPlotSetting() = 0 {};

	size_t fileId;		//!< 使用するファイルのインデックス(0〜)
	AttachAxis xItem;	//!< X軸項目

	IFilter* filter;	//!< フィルタ条件
};

//! プロットを追加する際に使用する構造体(2D平面)
struct AddPlotSetting2D : public AddPlotSetting 
{
	virtual ~AddPlotSetting2D() {}
	
	std::vector<AttachAxis> yItems;		//!< Y軸項目
	/*
		0 : 散布図
		1 : 折れ線図
		2 : 曲線図
		3 : 棒グラフ
	*/
	size_t plotKind;					//!< プロット種別
};

//! プロットを追加する際に使用する構造体(3D)
struct AddPlotSetting3D : public AddPlotSetting
{
	virtual ~AddPlotSetting3D() {}

	AttachAxis yItem;					//!< Y軸項目
	AttachAxis zItems[5];				//!< Z軸項目
	int zItemCount;						//!< zItemsの要素数
	/*
		0 : カラー散布図
		1 : コンターマップ
		2 : 格子グラフ
	*/
	size_t plotKind;					//!< プロット種別
};

//! プロットを追加する際に使用する構造体(4D)
struct AddPlotSetting4D : public AddPlotSetting3D
{
	virtual ~AddPlotSetting4D() {}

	AttachAxis tItem;
};

//! プロットを追加する際に使用する構造体(ベクトル図)
struct AddPlotSettingVector : public AddPlotSetting
{
	virtual ~AddPlotSettingVector() {}
	AttachAxis yItem;	//!< Y軸項目
	size_t vxItemId;	//!< VX項目
	size_t vyItemId;	//!< VY項目
};