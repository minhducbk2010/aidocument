/*!	@file
	@date	2008-01-07
	@author	wada

	グループの抽象基底クラス
*/
#pragma once

//class IShape;
#include <vector>
#include <algorithm>
#include <boost/foreach.hpp>
#include <boost/bind.hpp>
#include "IShape.h"
#include "IText.h" 

class IGroup
{
public:
	virtual void Add(IShape* shape) = 0;
	virtual void Insert(IShape* shape, size_t index) = 0;
	virtual size_t Remove(IShape* shape) = 0;
	virtual size_t GetChildCount() const = 0;
	virtual IShape* GetChild(size_t index) const = 0;
	virtual void Clear() = 0;

	//! 自身の中にshapeがあるかどうかを返します
	virtual bool Include(IShape* shape) const = 0;

	virtual void Select(IShape* shape, bool recursive = false) = 0;
    virtual size_t GetIndex(IShape* shape) const = 0;

	virtual void BringToFront(IShape* child) = 0;
	virtual void SendToBack(IShape* child) = 0;
	virtual void ChangeZOrder(IShape* shape, size_t index) = 0;
};

class OCGroupBase : public IGroup
{
public:
	//! 引数のオブジェクトを先頭に移動します
	void BringToFront(IShape* child)
	{
		std::vector<IShape*>::iterator it = std::find(m_child.begin(), m_child.end(), child);
		if (it != m_child.end()) {
			m_child.erase(it);
			m_child.push_back(child);
		}
	}

	//! 引数のオブジェクトを末尾に移動します
	void SendToBack(IShape* child)
	{
		std::vector<IShape*>::iterator it = std::find(m_child.begin(), m_child.end(), child);
		if (it != m_child.end()) {
			m_child.erase(it);
			m_child.insert(m_child.begin(), child);
		}
	}

	//! 引数のオブジェクトのZオーダーを変更します
	void ChangeZOrder(IShape* shape, size_t index)
	{
		std::vector<IShape*>::iterator it = std::find(m_child.begin(), m_child.end(), shape);
		if (it != m_child.end()) {
			if (index < m_child.size()) {
				m_child.erase(it);
				m_child.insert(m_child.begin() + index, shape);
			}
		}
	}

    virtual size_t GetIndex(IShape* shape) const
    {
        std::vector<IShape*>::const_iterator it = std::find(m_child.begin(), m_child.end(), shape);
		if (it != m_child.end()) {
            return std::distance(m_child.begin(), it);
        }
        return 0;
    }

	virtual bool Include(IShape* shape) const
	{
		BOOST_FOREACH(IShape* child, m_child) {
			if (child == shape) { return true; }

			if (IGroup* grp = dynamic_cast<IGroup*>(child)) {
				return grp->Include(shape);
			}
		}
		return false;
	}

	virtual void Select(IShape* shape, bool recursive = false)
	{
		if (std::find(m_child.begin(), m_child.end(), shape) == m_child.end()) {
			return;
		}

		if (!recursive) {
			IShape* root = dynamic_cast<IShape*>(shape->GetParent()); // this
			while (root->GetParent()) { root = dynamic_cast<IShape*>(root->GetParent()); }
			root->SetSelected(false);
		}

		if (!shape) { return; }

		shape->SetSelected(true);
		m_selectedObjects.push_back(shape);

		IShape* self = dynamic_cast<IShape*>(this);
		if (IGroup* grp = self->GetParent()) {
			grp->Select(self, true);
		}
	}

	void Insert(IShape* shape, size_t index)
	{
		if (std::find(m_child.begin(), m_child.end(), shape) != m_child.end()) { return; }

		shape->SetParent(this);

		std::vector<IShape*>::iterator it = m_child.begin();
		std::advance(it, index);
		m_child.insert(it, shape);

		InsertAfter(shape, index);
	}

	void Clear()
	{
		for (size_t i = 0; i < m_child.size(); ++i) {
			m_child[i]->Destroy();
		}
		m_child.clear();
	}

	//! リンクテキストのリフレッシュ
	void RefreshText() 
	{ 
		BOOST_FOREACH(IShape* child, m_child) 
		{ 
			if(IRichText* texts = dynamic_cast<IRichText*>(child)) 
			{ 
					texts->RefreshText(); 
			} 
			// グループ内にもグループがいる可能性がある
			else if (OCGroupBase* group = dynamic_cast<OCGroupBase*>(child)) 
			{ 
					group->RefreshText(); 
			} 
		} 
	}

protected:
	virtual void InsertAfter(IShape* shape, size_t index) {shape, index;}

protected:
	std::vector<IShape*> m_child;
	std::vector<IShape*> m_selectedObjects;
};