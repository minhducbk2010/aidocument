/*!	@file
	@date	2008-09-19
	@author	wada
*/
#pragma once

#define _USE_MATH_DEFINES
#include <cmath>
#include "utility.h"

//===========================================================================
/*!	角度をラジアンに変換します。
*/
//===========================================================================
inline double DegToRad(double degree)
{
#ifndef M_PI
	#define M_PI       3.14159265358979323846
#endif
	return degree * (M_PI / 180.0);
}

static OCPoint GetDistancePoint(const OCPoint &pntST, const OCPoint &pntED, double dot)
{
	double d = pntST.Distance(pntED);

	double sx = pntST.x;
	double sy = pntST.y;
	double ex = pntED.x;
	double ey = pntED.y;

	double x = sx + ((dot * (ex - sx)) / d);
	double y = sy + ((dot * (ey - sy)) / d);

	return OCPoint(x, y);
}

//===========================================================================
//! ポイント内の座標をを回転する
/*!
    @param[in]  point   回転したい座標
    @param[in]  center  回転に使用する中心座標
    @param[in]  degree  回転角度(°)
 */
//===========================================================================
static OCPoint RotatePoint(OCPoint const& point, OCPoint const& center, double degree)
{
	double radian = DegToRad(degree);
	double cos = std::cos(radian);
	double sin = std::sin(radian);

	double sx = center.x - point.x;
	double sy = center.y - point.y;
	double rx = (sx * cos) - (sy * sin);
	double ry = (sx * sin) + (sy * cos);
	rx = center.x - rx;
	ry = center.y - ry;

	return OCPoint(rx, ry);
}

//===========================================================================
/*!	直線の傾き（ラジアン）を計算します。
*/
//===========================================================================
static double GetLineRad(OCPoint st, OCPoint ed)
{
	double x = ed.x - st.x;
	double y = ed.y - st.y;
	double len = ::sqrt(::pow(x, 2.0) + ::pow(y, 2.0));
	double rad = ::acos(x / len);
	if (y < 0.0) {
		rad = -rad;
	}
	return rad;
}

static OCRect RotateRect(OCRect const& rect, double degree)
{
	OCPoint lt(rect.left, rect.top), rb(rect.right, rect.bottom);
	lt = RotatePoint(lt, rect.Center(), degree);
	rb = RotatePoint(rb, rect.Center(), degree);

	return OCRect(lt.x, lt.y, rb.x, rb.y);
}
