#pragma once

class IMessageReceiver
{
public:
    virtual LRESULT SendMessage(unsigned int id, WPARAM wParam, LPARAM lParam) = 0;
};

