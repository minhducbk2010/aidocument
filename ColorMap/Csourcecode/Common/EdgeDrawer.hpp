/*!	@file
	@date	2008-06-23
	@author	kikuchi

	@brief	端点形状の描画処理の実装
*/

#pragma once

#define  _USE_MATH_DEFINES
#include <cmath>
#include <wtl/atlmisc.h>
#include <wtl/atlgdi.h>

#include "ICanvas.h"

enum EdgeDrawType
{
	AllEdgeDraw,	// 左右両方描画
	StartEdgeOnly,	// 左側のみ描画
	EndEdgeOnly,	// 右側のみ描画
};

class EdgeDrawer
{
public:
	EdgeDrawer()
		: m_lineColor(RGB(0, 0, 0)), m_lineStyle(0), m_lineWidth(1)
	{
	}
	EdgeDrawer(COLORREF color, int style, float width)
		: m_lineColor(color), m_lineStyle(style), m_lineWidth(width)
	{
	}

	~EdgeDrawer()
	{
	}

	void DrawShape(ICanvas* canvas, OCRect rect, int types, double sizes, int angles, int drawType)
	{
		unsigned int const saved = canvas->Save();

		double offset = rect.Width() / 10;
		double center = rect.Height() / 2;

		int stType = (drawType == AllEdgeDraw) ? LOWORD(types) : types;
		int edType = (drawType == AllEdgeDraw) ? HIWORD(types) : types;
		int stSize = (drawType == AllEdgeDraw) ? MMToPx(LOWORD(sizes)) : MMToPx(sizes);
		int edSize = (drawType == AllEdgeDraw) ? MMToPx(HIWORD(sizes)) : MMToPx(sizes);
		int stAngle = (drawType == AllEdgeDraw) ? LOWORD(angles) : angles;
		int edAngle = (drawType == AllEdgeDraw) ? HIWORD(angles) : angles;

		OCPoint st = OCPoint(rect.left + offset, rect.top + center);
		OCPoint ed = OCPoint(rect.right - offset, rect.top + center);

		canvas->SetPen(m_lineColor, m_lineWidth, PS_SOLID, 1);
		canvas->SetBackground(m_lineColor, FS_SOLID, 0);

		// ラインスタイルが無しの場合、端点は描画しない
		if (m_lineStyle != PS_NULL) {
			if (drawType != EndEdgeOnly) {
				switch (stType) {
					case 1:
						DrawArrow(canvas, st, ed, stSize, stAngle, false, false);
						break;
					case 2:
						DrawArrow(canvas, st, ed, stSize, stAngle, true, false);
						break;
					case 3:
						DrawArrow(canvas, st, ed, stSize, stAngle, true, true);
						break;
					case 4:
						DrawCircle(canvas, st, ed, stSize, false, true);
						break;
					case 5:
						DrawCircle(canvas, st, ed, stSize, true, true);
						break;
				}
			}
			if (drawType != StartEdgeOnly) {
				switch (edType) {
					case 1:
						DrawArrow(canvas, ed, st, edSize, edAngle, false, false);
						break;
					case 2:
						DrawArrow(canvas, ed, st, edSize, edAngle, true, false);
						break;
					case 3:
						DrawArrow(canvas, ed, st, edSize, edAngle, true, true);
						break;
					case 4:
						DrawCircle(canvas, ed, st, edSize, false, false);
						break;
					case 5:
						DrawCircle(canvas, ed, st, edSize, true, false);
						break;
				}
			}
		}

		canvas->SetPen(m_lineColor, m_lineWidth, m_lineStyle, 1);

		canvas->DrawLine(st, ed);

		canvas->Restore(saved);
	}

private:
	void DrawArrow(ICanvas* canvas, OCPoint& start, OCPoint end, double size, double rot, bool closed, bool filled)
	{
		OCPoint tmp = GetDistancePoint(start, end, size);
		
		OCPoint a = RotatePoint(tmp, start, rot);
		OCPoint aPnt[] =
		{
			a,
			start,
			RotatePoint(tmp, start, 0.0 - rot)
		};

		if (closed) {
			if (filled) {
				canvas->FillPolygon(aPnt, 3);
			}
			else {
				canvas->DrawPolygon(aPnt, 3);
			}

			double scale = std::sqrt(std::pow(a.x - start.x, 2.0) + std::pow(a.y - start.y, 2.0));
			double center = scale * cos(DegToRad(rot));
			double rad = GetLineRad(start, end);

			start.x = a.x;	// 計算すると誤差がでることがあるので、端のxをそのまま使う
			start.y += center * std::sin(rad);
		}
		else {
			canvas->DrawLines(aPnt, 3);
		}
	}

	void DrawCircle(ICanvas* canvas, OCPoint& start, OCPoint end, double size, bool filled, bool isStart)
	{
		OCPoint tmp = GetDistancePoint(start, end, size);
		double radius = size / 2.0;

		// 三角の頂点と合わせるため、半径分を差し引く
		start.x += (isStart) ? radius : -radius;
		OCRect rect(start.x - radius, start.y - radius,
			start.x + radius, start.y + radius);
		if (filled) {
			canvas->FillEllipse(rect);
		}
		else {
			canvas->DrawEllipse(rect);
		}

		double rad = GetLineRad(start, end);
		double yOff = radius * sin(rad);
		start.x = (isStart) ? rect.right : rect.left;	// 計算すると誤差が出ることがあるので内側の端をそのまま使う
		start.y += yOff;
	}

	OCPoint GetDistancePoint(const OCPoint& pntST, const OCPoint& pntED, double dot)
	{
		double d = GetDistance(pntST, pntED);

		double sx = pntST.x;
		double sy = pntST.y;
		double ex = pntED.x;
		double ey = pntED.y;

		double x = sx + ((dot * (ex - sx)) / d);
		double y = sy + ((dot * (ey - sy)) / d);

		return OCPoint(x, y);
	}

	double GetDistance(OCPoint pntA, OCPoint pntB)
	{
		double x = pntA.x - pntB.x;
		double y = pntA.y - pntB.y;
		double s = (x * x) + (y * y);
		double rval = ::sqrt(s);

		return rval;
	}

	OCPoint RotatePoint(OCPoint pntA, OCPoint pntC, double degree)
	{
		double radian = DegToRad(degree);
		double cos = ::cos(radian);
		double sin = ::sin(radian);

		double sx = pntC.x - pntA.x;
		double sy = pntC.y - pntA.y;
		double rx = (sx * cos) - (sy * sin);
		double ry = (sx * sin) + (sy * cos);
		rx = pntC.x - rx;
		ry = pntC.y - ry;

		return OCPoint(rx, ry);
	}

	double DegToRad(double degree)
	{
		return degree * (M_PI / 180.0);
	}

	double GetLineRad(OCPoint start, OCPoint end)
	{
		double x = end.x - start.x;
		double y = end.y - start.y;
		double len = ::sqrt(::pow(x, 2.0) + ::pow(y, 2.0));
		double rad = ::acos(x / len);
		if (y < 0.0) {
			rad = -rad;
		}
		return rad;
	}

private:
	int m_lineStyle;
	float m_lineWidth;
	COLORREF m_lineColor;
};