/*! @file
	@date	2008-06-17
	@author	wada
*/
#pragma once

class IShape;
class IGraphFile;
class OCGraphFile;
class IAxis;
class IFilter;
struct AddPlotSetting2D;
struct AddPlotSetting3D;
struct AddPlotSetting4D;
struct AddPlotSettingVector;
struct PropertyNode;
struct OCRect;

// AddPlotで追加したプロットをAddPlot呼び出し元に通知するためのインターフェース
class IPlotContainer
{
public:
	virtual void AddPlot(IShape* plot) = 0;
};

class IGraph
{
public:
	virtual ~IGraph() = 0 {};

	//! グラフの座標を初期化します
	virtual void InitRect(OCRect const& rect) = 0;

	//! ファイルの追加を行えるかを確認します
	virtual bool CanAddFile() const = 0;
	//! プロット追加を行えるか確認します
	virtual bool CanAddPlot() const = 0;
	//! 指定インデックス(ファイルID)のファイルが使用されているか確認します
	virtual bool CheckUseFile(size_t fileId) const = 0;
	
	//! 指定インデックス(ファイルID)のファイルをグラフに追加します
	virtual IGraphFile* AddFile(size_t fileId, bool copyPlot = true) = 0;
	virtual IGraphFile* AddFileNoClone(size_t fileId) = 0;

	virtual size_t GetFileCount() const = 0;
	virtual IGraphFile* GetFile(size_t index) const = 0;
	virtual IGraphFile* GetEmptyFile() const = 0;

	//! X軸の数を取得します
	virtual size_t GetXAxisCount() const = 0;
	//! 指定インデックスのX軸を取得します
	virtual IAxis* GetXAxis(size_t index) const = 0;

	//! Y軸の数を取得します
	virtual size_t GetYAxisCount() const = 0;
	//! 指定インデックスのY軸を取得します
	virtual IAxis* GetYAxis(size_t index) const = 0;

	//! Z軸の数を取得します
	virtual size_t GetZAxisCount() const = 0;
	//! 指定インデックスのZ軸を取得します
	virtual IAxis* GetZAxis(size_t index) const = 0;

	virtual void AllSelectAxis() {}//= 0;
	virtual void AllSelectPlot() {}//= 0;

	//! プロットを追加します
	virtual void AddPlot(AddPlotSetting2D const&, IPlotContainer*) = 0;
	virtual void AddPlot(AddPlotSetting3D const&, IPlotContainer*) = 0;
	//virtual void AddPlot(AddPlotSetting4D const&) { return std::vector<IShape*>(); }
	virtual void AddPlot(AddPlotSettingVector const&, IPlotContainer*) = 0;

	//! デジタルアレイ
	virtual bool IsUseDigitalArray() const = 0;
	virtual void ShowDigitalArray() = 0;

	//! データが三次元以上存在するか調べます
	virtual bool Is3D() const = 0;

	//! 引数のカラーパターンを使用しているかどうか調べます
	virtual bool CheckUseColorPattern(TCHAR const* path) = 0;
	//! カラーパターンを再読込し、グラフを再描画します
    virtual void UpdateColorPattern(TCHAR const* path) = 0;

	virtual void UpdateLock() = 0;
	virtual void UpdateUnlock() = 0;
};

class IDigitalmapHolder
{
public:
	virtual bool UseDigitalmap() const = 0;
	virtual void ShowDigitalmap() = 0;
};