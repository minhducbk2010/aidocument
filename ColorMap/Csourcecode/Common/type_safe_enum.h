#pragma once

/*!	@brief	Type Safe Enum
	@see http://en.wikibooks.org/wiki/More_C%2B%2B_Idioms/Type_Safe_Enum
*/
template<typename def, typename inner = typename def::type>
class safe_enum : public def
{
	inner val;
 
public:
	typedef typename def::type type;
 
	safe_enum(type v) : val(v) {}
	inner underlying() const { return val; }

	bool operator == (const safe_enum & s) const { return this->val == s.val; }
	bool operator != (const safe_enum & s) const { return this->val != s.val; }
	bool operator <  (const safe_enum & s) const { return this->val <  s.val; }
	bool operator <= (const safe_enum & s) const { return this->val <= s.val; }
	bool operator >  (const safe_enum & s) const { return this->val >  s.val; }
	bool operator >= (const safe_enum & s) const { return this->val >= s.val; }
};
