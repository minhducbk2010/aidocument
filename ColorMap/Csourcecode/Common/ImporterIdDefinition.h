#pragma once

// ImporterIdDefinition.h
// インポータプラグインIDの定義
// 各インポータプラグインのGetId()では、以下のenum値を返すこと

enum ImporterId
{
//	インポータ名称			インポータID
	CSV_IMPORTER_ID				=	1,
	EXCEL_IMPORTER_ID			=	2,
	DS_IMPORTER_ID				=	3,
	KY_IMPORTER_ID				=	4,
	FAMS_IMPORTER_ID			=	5,
	MESH_IMPORTER_ID			=	6,
// いすゞ 1/3 Octave (特注)		=	7,
// データベースファイル(特注)	=	8,
	CBD_IMPORTER_ID				=	9,
	MEID_IMPORTER_ID			=	10,

// 新しいインポートプラグインができた場合、原則下に追加される
};

enum ErrorCode
{
	IMPORT_SUCCESS = 1,
	IMPORT_CANCEL = 0,
	IMPORT_FAILED = -1,
	IMPORT_FILTER_UNMATCH = -2,
};