/*!	@file
	@date	2008-09-04
	@author	wada
*/
#pragma once

#include <comutil.h> // _variant_t
#include <boost/shared_ptr.hpp>
struct Formula;
class ISerializer;
class IFile;

enum LogicKind
{
	And,
	Or,
};

// AddConditionで使用する条件設定構造体
struct ConditionStruct
{
	enum Operations {
		Equal = 1, //!< と同じ(=)
		NotEqual, //!< と異なる(≠)
		Less, //!< より大きい(＞)
		LessEq, //!< 以上(≧)
		Greater, //!< より小さい(＜)
		GreaterEq, //!< 以下(≦)
	};

	short ItemIndex;	//!< 条件にかけるデータ項目のインデックス
	Operations Operation;	//!< 設定する条件
	double Value;	//!< 設定する基準数値
};

class IFilter
{
public:
	virtual ~IFilter() = 0 {};

	virtual size_t GetConditionCount() const = 0;
	virtual void Serialize(ISerializer* ar) = 0;
	virtual IFilter* Clone() = 0;

	//virtual void SetFormula(boost::shared_ptr<Formula> const& formula) = 0;
	//virtual boost::shared_ptr<Formula> const& GetFormula() const = 0;
	//virtual void SetData(std::vector<float const*> const& data, size_t items, size_t count) = 0;
	//! 最初のデータから検索を行います
	virtual size_t FindFirst() = 0;
	//! 次に条件に合致するデータを検索します
	virtual size_t FindNext() = 0;
	//! 使用しているファイルを変更します(isExtarctNumber: 列番号=true, 項目名称=false)
	virtual void SetFile(IFile* file, bool isExtractNumber) = 0;
	//! 条件がAndかOrかを取得します
	virtual int GetLogicKind() = 0;
	virtual void SetLogicKind(LogicKind kind) = 0;

	virtual bool AddCondition(ConditionStruct* condition) = 0;
	virtual bool ChangeCondition(int conditionNum, ConditionStruct* condition) = 0;
	virtual bool DeleteCondition(int conditionNum) = 0;
	virtual void AllDeleteCondition() = 0;

	//virtual bool IsEffective(float value) const {return false;}//= 0;
};