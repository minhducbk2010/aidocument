/*!	@file
	@date	2008-03-10
	@author	wada

	@brief	キャンバスのインターフェイス
*/
#pragma once

#include <windows.h>
#include <gdiplus.h>

struct OCRect;
struct OCPoint;

//! 塗りつぶし種別
enum FILLSTYLE
{
	FS_MIN = 0,
	FS_SOLID = 0,			//!< 単色で塗りつぶします
	FS_NONE,				//!< 塗りつぶしを行いません
	FS_HORIZONTAL_HATCH,	//!< 水平ハッチ
	FS_VERTICAL_HATCH,		//!< 垂直ハッチ
	FS_FDIAGONAL,			//!< 45度下向きハッチ
	FS_BDIAGONAL,			//!< 45度上向きハッチ
	FS_CROSS,				//!< 水平垂直ハッチ
	FS_CROSS_45,			//!< 45度クロスハッチ
	//FS_DOTTEDGRID,			//!< グリッド
	FS_GRADATION,			//!< グラデーションしながら塗りつぶします
	FS_MAX = FS_GRADATION,
};

//! 角の形
enum ROUND_TYPE
{
	RT_ELLIPSE,		//!< 丸
	RT_RECTANGLE,	//!< 四角
};

//! グラデーションの種別
enum GRADATION_KIND
{
	GK_RECT,
	GK_ELLIPSE,
	GK_ROUNDRECT,
};

//! グラデーションの方向
enum GRADATION_TYPE
{
	GT_LEFT_TO_RIGHT = 0,
	GT_LEFT_TO_RIGHT_SKEW_BOTTOM,
	GT_LEFT_TO_RIGHT_SKEW_TOP,
	GT_RIGHT_TO_LEFT,
	GT_RIGHT_TO_LEFT_SKEW_BOTTOM,
	GT_RIGHT_TO_LEFT_SKEW_TOP,
	GT_TOP_TO_BOTTOM,
	GT_BOTTOM_TO_TOP,
	GT_INNER,
};

//! キャンバスインターフェイス
class ICanvas
{
public:
	virtual ~ICanvas() = 0 {}

	//! 用紙の左上(0, 0)座標をmm単位で指定します
	virtual void SetOffset(double mmx, double mmY) = 0;
	virtual double GetOffsetX() const = 0;
	virtual double GetOffsetY() const = 0;

	/*! キャンバスとデバイスコンテキストを関連付けます
		この関数を使用した後に描画する各図形は、このデバイスコンテキストに描画されます
	*/
	virtual void Attach(HDC hDC) = 0;
	virtual void Attach(Gdiplus::Metafile* meta) = 0;
	virtual void Attach(HBITMAP hBitmap) = 0;
	//! デバイスコンテキストとの関連付けを解除します
	virtual void Detach() = 0;

	/*! 現在関連付けられているデバイスコンテキストを取得します
		この関数で得たHDCは不要になった時点で必ず ReleaseHDC で解放してください
	*/
	virtual HDC GetHDC() = 0;
	//! GetHDC関数で取得したデバイスコンテキストを解放します
	virtual void ReleaseHDC(HDC) = 0;

	virtual bool IsPrinting() const = 0;
	virtual void SetPrinting(bool printing) = 0;

	//! テキストのアンチエイリアシング設定
	virtual void SetTextAntialiasing(bool enable) = 0;
	//! テキスト以外のオブジェクトのアンチエイリアシング設定
	virtual void SetAntialiasing(bool enable) = 0;
	//! 角を丸めるかどうか
	virtual void SetRoundMode(bool mode) = 0;

	//! ズーム倍率を取得します
	virtual double GetZoomRatio() const = 0;
	//! ズーム倍率を設定します
	/*!	@param[in]	ratio	倍率(1 = 100% = 等倍) */
	virtual void SetZoomRatio(double ratio) = 0;

	//! 現在のキャンバスの状態を保存します
	virtual unsigned int Save() const = 0;
	//! キャンバスを以前保存した状態に戻します
	virtual void Restore(unsigned int state) = 0;

	virtual void Rotate(int angle, double x, double y) = 0;
	virtual void Translate(double xOffset, double yOffset) = 0;

	//@{ 矩形を描画します
	virtual bool DrawRectangle(OCRect const& rect) = 0;
	virtual bool DrawRectangle(double x, double y, double width, double height) = 0;
	virtual bool FillRectangle(OCRect const& rect) = 0;
	virtual bool DrawRoundRectangle(OCRect const& rect, float radius) = 0;
	virtual bool DrawRectangleShadow(OCRect const& rect, double offset, double blur, short angle, COLORREF color) const = 0;
	//@}

	//@{ 楕円を描画します
	virtual bool DrawEllipse(OCRect const& rect) = 0;
	virtual bool DrawEllipse(double x, double y, double width, double height) = 0;
	virtual bool FillEllipse(OCRect const& rect) = 0;
	//@}

	//@{ 吹き出しを描画します
	virtual bool DrawRoundBalloon(const OCPoint* points, float radius, int type) const = 0;
	virtual bool DrawEllipseBalloon(OCRect const& rect, OCPoint const& pitch) const = 0;
	//@}

	//! 矩形をグラデーションしながら塗りつぶします
	virtual bool GradientFill(OCRect const& rect, COLORREF begin, COLORREF end, GRADATION_KIND kind, GRADATION_TYPE direction) const = 0;

	//@{ 線を描画します
	virtual bool DrawLine(OCPoint const& p1, OCPoint const& p2) const = 0;
	virtual bool DrawLine(double x1, double y1, double x2, double y2) const = 0;
	//@}
	//! 配列同士を結んだ直線を描画します
	virtual bool DrawLines(OCPoint const* points, size_t count) const = 0;
	
	virtual bool DrawPolygon(OCPoint const* points, size_t count) const = 0;
	virtual bool FillPolygon(OCPoint const* points, size_t count) const = 0;

	virtual bool DrawCurve(OCPoint const* points, size_t count) = 0;
	virtual bool FillCurve(OCPoint const* points, size_t count) = 0;

	virtual OCPoint MeasureString(wchar_t const* text, long length) = 0;
	virtual bool DrawString(wchar_t const* text, long length, OCPoint const& point) = 0;
	// flag		0x1 → 横方向の揃えを中央にする(1文字描画の際、左に余白を生じるのを防ぐため)
	virtual bool DrawString(wchar_t const* text, long length, OCRect const& rect, int flag = 0) = 0;

	virtual bool DrawBitmap(HBITMAP hBitmap, double x, double y, double width, double height, short alpha) const = 0;
	virtual bool DrawTransparentBitmap(HBITMAP hBitmap, double x, double y, double width, double height, short alpha, COLORREF color) const = 0;
	virtual bool DrawBitmapFlip(HBITMAP hBitmap, double x, double y, double width, double height, short alpha, bool xFlip, bool yFlip) const = 0;
	virtual bool DrawTransparentBitmap(HBITMAP hBitmap, double x, double y, COLORREF color) const = 0;
	virtual bool DrawMetafile(HENHMETAFILE meta, double x, double y, double width, double height, short alpha, bool xFlip, bool yFlip) const = 0;

	//!@{ ラバーバンドを描画します(XORの線を描きます)
	virtual bool FillRubberBand(OCRect const& rect) = 0;
	virtual bool DrawRubberBand(OCRect const& rect) = 0;
	virtual bool DrawRubberBandCurve(OCPoint const* points, size_t count) = 0;
	virtual bool DrawRubberLine(OCPoint const& p1, OCPoint const& p2) const = 0;
	virtual bool DrawRubberLines(OCPoint const* points, size_t count) const = 0;
	virtual bool DrawRubberPolygon(OCPoint const* points, size_t count) = 0;
	virtual bool DrawRubberEllipseBalloon(OCRect const& rect, OCPoint const& pitch) = 0;
	//@}

	//!@{
	//! リサイズハンドルを描画します
	virtual bool DrawResizeHandle(OCPoint const& position, bool locked) const = 0;
	virtual bool DrawResizeBalloonHandle(OCPoint const& position, bool locked) const = 0;
	//@}

	//!@{
	//! マーカーを描画します
	// bFill : 中を塗りつぶすかどうか(◎対策)
	virtual bool DrawMarkerCircle(double x, double y, double size, bool bFill) = 0;
	virtual bool DrawMarkerRectangle(double x, double y, double size) = 0;
	// flag : 0 (Diamond), 1 (TriangleUp), 2 (TriangleDown)
	virtual bool DrawMarkerDiamondTriangle(double x, double y, double size, int flag) = 0;
	// flag : 0 (Cross), 1 (Plus)
	virtual bool DrawMarkerCrossPlus(double x, double y, double size, int flag) = 0;
	//@}

	//!@{ クリッピング関数
	virtual void SetClipping(OCRect const& rect) = 0;
	virtual void AddClipping(OCRect const& rect) = 0;
	virtual void SetClipping(OCPoint const* points, size_t count) = 0;
	virtual void SetExcludeClippingEllipse(OCRect const& rect) = 0;
	virtual void SetExcludeClipping(OCPoint const* points, size_t count) = 0;
	virtual void ResetClipping() = 0;
	//@}

	//!@{ 描画条件を設定する関数
	//! ペンの設定を行います
	virtual void SetPen(COLORREF color, float width, int style, int roundType) = 0;
	//! ペンの色を設定します
	virtual void SetPenColor(COLORREF color) = 0;
	//! ペンの太さを設定します
	virtual void SetPenWidth(float width) = 0;
	//! ペンの線種を設定します
	virtual void SetPenStyle(long style) = 0;
	//! 環境設定に含まれるカスタム線種を再読み込みします
	virtual void ReloadCustomLines() = 0;
	virtual void SetCustomLineDirectory(TCHAR const* directory) = 0;
	//! カスタム線種を設定します
	virtual void SetCustomLineStyle(float* offsets, unsigned long length) = 0;
	//! カスタム線種の数を取得します
	virtual long GetCustomStyleCount() const = 0;
	virtual long GetUserCustomStyleCount() const = 0;
	//! カスタム線種の名称を取得します
	virtual wchar_t const* GetCustomStyleName(long index) const = 0;
	//! カスタム線種を指定のフォルダに保存します
	virtual bool SaveCustomLines(TCHAR const* directory) const = 0;
	//! ブラシの設定を行います
	virtual void SetBackground(COLORREF color, FILLSTYLE style, short transparent) = 0;
	//! ブラシの背景色を設定します
	virtual void SetBackgroundColor(COLORREF color) = 0;
	//! ブラシのスタイルを設定します
	virtual void SetBackgroundStyle(FILLSTYLE style) = 0;
	//! 背景の透明度を設定します
	virtual void SetTransparent(short percent) = 0;
	virtual void SetPenTransparent(short percent) = 0;
	//! グラデーション用のブラシを作成します
	virtual void CreateGradationBrush(OCRect const& rect, double blur, COLORREF color) = 0;
	//! グラデーション条件を設定します
	virtual void SetGradationStyle(GRADATION_TYPE direction, COLORREF begin, COLORREF end) = 0;

	virtual void SetFont(wchar_t const* font, long size, long angle) = 0;
	virtual void SetFont(LOGFONT const* lf) = 0;
	virtual void SetFont2(LOGFONT const* lf) = 0;
	virtual void SetFontColor(COLORREF color) = 0;
	virtual long GetFontSize() const = 0;
	//@}

	virtual void CreatePath() = 0;
	virtual void ResetPath() = 0;

	virtual void CreateOffScreen(int width, int height) = 0;
	virtual void ReleaseOffScreen() = 0;
	virtual void BitBlt(HDC hDC, int x, int y) = 0;
	virtual bool GetThumbnail(HDC dc, int width, int height) const = 0;
	virtual bool HasOffScreen() const = 0;

	virtual bool LPtoDP(OCPoint* point, size_t count) = 0;
	virtual bool LPtoDP(OCRect* rect) = 0;

	virtual double GetDpi() const = 0;

	virtual void InitGraphics(Gdiplus::Graphics* g) const = 0;
};