/*!	@file
*/
#pragma once

#include "Message.h"

inline TCHAR const* GetResourceStringForPlugin(HWND parent, char const* const resourceId)
{
	IMessageReceiver* recv = reinterpret_cast<IMessageReceiver*>(::SendMessage(parent, PI_MSG_GET_DOCUMENT_POINTER, 0, 0));
	if (recv) {
		WPARAM id = reinterpret_cast<WPARAM>(resourceId);

		TCHAR const* retStr = reinterpret_cast<TCHAR const*>(recv->SendMessage(PI_MSG_GET_RESOURCE_STRING, id, 0));
		if (retStr != 0) {
			return retStr;
		}
	}
	return _T("");
}

/*!	@brief	ダイアログの子コントロールにリソースを設定します
	@example ::EnumChildWindows(m_hWnd, InitializeDialogResourceForPlugin, m_owner);

	@param[in]	lParam	IMessageReceiverのインスタンス
*/
inline BOOL CALLBACK InitializeDialogResourceForPlugin(HWND wnd, LPARAM lParam)
{
	IMessageReceiver* recv = reinterpret_cast<IMessageReceiver*>(lParam);

	TCHAR name[256] = {};
	::GetClassName(wnd, name, _countof(name));

#ifdef _MFC_VER
	// コンボボックスのアイテムを置換
	if (name == std::wstring(L"ComboBox")) {
		std::vector<std::wstring> list;
		CWnd* combo = CWnd::FromHandle(wnd);
		int const count = (int)combo->SendMessage(CB_GETCOUNT);
		for (int i = 0; i < count; ++i) {
			combo->SendMessage(CB_GETLBTEXT, i, reinterpret_cast<LPARAM>(name));
			std::string key = ::ToMultiByteString(name);
			std::wstring res = ::GetResourceString(recv, key.c_str());
			if (res != _T("")) {
				list.push_back(res);
			}
			else {
				// Keyがないときはそのまま入れる
				list.push_back(name);
			}
		}
		combo->SendMessage(CB_RESETCONTENT);
		for (size_t i = 0; i < list.size(); ++i) {
			combo->SendMessage(CB_ADDSTRING, 0, reinterpret_cast<LPARAM>(list[i].c_str()));
		}
	}
#endif

	// コントロールテキストにキーが格納されているので、そこから対応する文字列を取得し差し替える
	char id[256] = {};
	::GetWindowTextA(wnd, id, _countof(id));

	if (TCHAR const* const text = ::GetResourceString(recv, id)) {
		if (::_tcslen(text) > 0) {
			::SetWindowText(wnd, text);
		}
	}

	return TRUE;
}

inline void LocalizeDialogResourcePlugin(HWND wnd, IMessageReceiver* owner)
{
	LPARAM param = reinterpret_cast<LPARAM>(owner);

	InitializeDialogResourceForPlugin(wnd, param);
	::EnumChildWindows(wnd, InitializeDialogResourceForPlugin, param);
}

inline BOOL CALLBACK SetFontForPlugin(HWND wnd, LPARAM lParam)
{
	IMessageReceiver* recv = reinterpret_cast<IMessageReceiver*>(lParam);

	LOGFONT lf = {};
	lf.lfCharSet        = DEFAULT_CHARSET;
	lf.lfOutPrecision   = OUT_DEFAULT_PRECIS;
	lf.lfClipPrecision  = CLIP_DEFAULT_PRECIS;
	lf.lfQuality        = DEFAULT_QUALITY;
	lf.lfPitchAndFamily = FF_DONTCARE;

	std::wstring const& fontName = ::GetResourceString(recv, "ID_SOURCE_TEXT_MS_UI_GOTHIC");
	int fontSize = ::_ttoi(::GetResourceString(recv, "ID_SOURCE_TEXT_DLG_FONTSIZE"));
	lf.lfHeight = PointToDeviceUnit(fontSize);
	::wcscpy_s(lf.lfFaceName, _countof(lf.lfFaceName), fontName.c_str());

	HFONT font = ::CreateFontIndirect(&lf);
	::SendMessage(wnd, WM_SETFONT, reinterpret_cast<WPARAM>(font), FALSE);

	return TRUE;
}

inline void InitializeFontForPlugin(HWND wnd, IMessageReceiver* owner)
{
	LPARAM param = reinterpret_cast<LPARAM>(owner);

	SetFontForPlugin(wnd, param);
	::EnumChildWindows(wnd, SetFontForPlugin, param);
}

inline void SetMenuResourceForPlugin(HMENU menu, IMessageReceiver* owner)
{
	if (!menu) { return; }

	for (UINT i = 0; i < ::GetMenuItemCount(menu); ++i) {
		MENUITEMINFO info = {sizeof(info)};
		info.fMask = MIIM_STRING;
		::GetMenuItemInfo(menu, i, TRUE, &info);
		if (info.cch > 0) {
			++info.cch;

			std::wstring buf;
			buf.resize(info.cch + 1);
			info.dwTypeData = &buf[0];
			::GetMenuItemInfo(menu, i, TRUE, &info);

			std::string resourceId = ::ToMultiByteString(info.dwTypeData);
			std::wstring text = ::GetResourceString(owner, resourceId.c_str());
			info.dwTypeData = const_cast<LPWSTR>(text.c_str());
			::SetMenuItemInfo(menu, i, TRUE, &info);
		}
		SetMenuResourceForPlugin(::GetSubMenu(menu, i), owner);
	}
}

