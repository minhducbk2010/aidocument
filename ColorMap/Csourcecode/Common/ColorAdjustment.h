#pragma once

//===========================================================================
/*!	RGB -> HSV 変換
	色情報をRGB形式からHSV形式へ変換します
*/
//===========================================================================
inline void RGBtoHSV(const unsigned long rgb,
	unsigned short* h, unsigned char* s, unsigned char* v)
{
	unsigned char r = GetRValue(rgb);
    unsigned char g = GetGValue(rgb);
    unsigned char b = GetBValue(rgb);
	unsigned char rgb_lst[] = {r, g, b};

	unsigned char max = *std::max_element(rgb_lst, rgb_lst + _countof(rgb_lst));
	unsigned char min = *std::min_element(rgb_lst, rgb_lst + _countof(rgb_lst));
	unsigned char d = static_cast<unsigned char>(max - min);

    *v = max;
	if (d == 0) {
		// 色の最大と最小の差が0ということは、カラーコードが全て0 = 黒となる
		*s = 0;
		*h = 0;
		return;
	}

   	*s = (unsigned char)((unsigned short)d * 0xff / (unsigned short)max);
	unsigned short rt = static_cast<unsigned short>((unsigned short)max - (unsigned short)r * 60 / (unsigned short)d);
    unsigned short gt = static_cast<unsigned short>((unsigned short)max - (unsigned short)g * 60 / (unsigned short)d);
    unsigned short bt = static_cast<unsigned short>((unsigned short)max - (unsigned short)b * 60 / (unsigned short)d);

    if(r == max){
    	if(bt >= gt) {
        	*h = static_cast<unsigned short>(bt - gt);
        }
        else {
        	*h = static_cast<unsigned short>(bt + 360 - gt);
        }
    }
    else if(g == max) {
    	if(120 + rt >= bt) {
        	*h = static_cast<unsigned short>(120 + rt - bt);
        }
        else {
        	*h = static_cast<unsigned short>(120 + rt + 360 - bt);
        }
    }
    else {
    	if(240 + gt >= rt) {
        	*h = static_cast<unsigned short>(240 + gt - rt);
        }
        else {
        	*h = static_cast<unsigned short>(240 + gt + 360 - rt);
        }
    }
}

//===========================================================================
/*!	HSV -> RGB 変換
	色情報をHSV形式からRGB形式へ変換します
*/
//===========================================================================
inline void HSVtoRGB(unsigned long* rgb,
	const unsigned short h, const unsigned char s, const unsigned char v)
{
	unsigned char r, g, b;

	if(s == 0) {
    	r = v;
        g = v;
        b = v;
    }
    else {
        unsigned short ht = static_cast<unsigned short>(h * 6);
        unsigned short d = static_cast<unsigned short>(ht % 360);

        unsigned char t1 = (unsigned char)((unsigned short)v * (unsigned short)(0xff - s) / (unsigned short)0xff);
        unsigned char t2 = (unsigned char)((unsigned short)v * (unsigned short)(0xff - s * d / 360) / (unsigned short)0xff);
        unsigned char t3 = (unsigned char)((unsigned short)v * (unsigned short)(0xff - s * (360 - d) / 360) / (unsigned short)0xff);

        switch(ht / 360)
        {
        case 0:
        	r = v;
            g = t3;
            b = t1;
            break;
        case 1:
        	r = t2;
            g = v;
            b = t1;
            break;
        case 2:
        	r = t1;
            g = v;
            b = t3;
            break;
        case 3:
        	r = t1;
            g = t2;
            b = v;
            break;
        case 4:
        	r = t3;
            g = t1;
            b = v;
            break;
        default:
        	r = v;
            g = t1;
            b = t2;
        }
    }

    *rgb = RGB(r, g, b);
}

//===========================================================================
/*!	色を指定したパラメータで変更する
	元の色をHSV形式にし、引数の設定分変化させた結果をRGBで返します。
*/
//===========================================================================
inline unsigned long GetModifiedColor(const unsigned long src, int offsetH, int offsetS, int offsetV)
{
    unsigned short h;               // H(Hue/色相)
    unsigned char s, v;             // S(Saturation/彩度 0:白色〜255:派手)、V(Value/明度 0:暗い〜255:明るい)
    RGBtoHSV(src, &h, &s, &v);

	h = (h + offsetH) > 0 ? (h + offsetH) < USHRT_MAX ? (h + offsetH) : USHRT_MAX : 0;
	s = (s + offsetS) > 0 ? (s + offsetS) < UCHAR_MAX ? (s + offsetS) : UCHAR_MAX : 0;
	v = (v + offsetV) > 0 ? (v + offsetV) < UCHAR_MAX ? (v + offsetV) : UCHAR_MAX : 0;

	unsigned long rgb;
	HSVtoRGB(&rgb, h, s, v);
    
    return rgb;
}