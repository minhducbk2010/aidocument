/*!	@file
	@date	2008-07-31
*/

#pragma once

//! 軸のスケールタイプ
enum ScaleType
{
	// この内容を増やす際はZ軸のプロパティ入出力処理に注意すること
	// コンスタントに未対応なのでインデックスをずらす処理を入れている(BK)
	ST_LINEAR,		//!< リニア
	ST_LOG,			//!< 対数
	ST_DECIBEL,		//!< デシベル
	ST_CONSTANT,	//!< コンスタント
	ST_DEGREE,		//!< 角度
};

//! 軸目盛の向き
enum TickType
{
	TT_NONE,		//!< なし
	TT_INSIDE,		//!< 内向き
	TT_OUTSIDE,		//!< 外向き
	TT_CROSS,		//!< 交差
};