/*!	@file
	@brief	テキストオブジェクトの基底クラス
	@author	wada
	@date	2008-01-25
*/
#pragma once

//! テキスト
class IText
{
public:
	//! エディタを閉じます
	virtual void CloseEditor() = 0;
};

//! RTF
struct Rtf
{
	BYTE* temporaryBuffer;
	size_t length;
	size_t pos;
	bool heap;

	Rtf()
		: temporaryBuffer(0), length(0), pos(0), heap(false)
	{}
	Rtf(BYTE* buf, size_t length)
		: temporaryBuffer(buf), length(length), pos(0), heap(false)
	{}

	~Rtf()
	{
		if (heap) {
			delete[] temporaryBuffer;
			temporaryBuffer = 0;
		}
	}
};

class IRtfHolder
{
public:
	virtual Rtf* GetRtf() const = 0;
	virtual void SetRtf(Rtf* rtf) = 0;
	virtual TCHAR const* GetCurrentRtfText() = 0;
};

class IRichText : public IText
{
public:
	virtual bool PasteText() = 0;
	virtual void InsertLinkText(int fileId,int id) = 0;

	virtual BOOL IsEditing() const = 0; //! どのオブジェクトでもいいから編集中かどうか(自オブジェクトが編集中かどうかではないので注意！)
	virtual BOOL IsUseEffect(int effect) const = 0;
	virtual long GetHorzAlignment() const = 0;
	virtual long GetVertAlignment() const = 0;
	virtual BOOL GetFontName(wchar_t* name, long length) = 0;
	virtual long GetFontSize() const = 0;
	virtual COLORREF GetFontColor() const = 0;
	//! 行間を取得します
	virtual long GetLineSpace() const = 0;
	//! 
	virtual void AutoSize() = 0;
	virtual BOOL IsAutoSize() const = 0;
	virtual void SetText(wchar_t const* text) = 0;

	virtual long SavePreEditState() = 0;
	virtual void RestoreEditState(long state) = 0;

	virtual void RefreshText() = 0;

	//! エディタにフォーカスを移動します(エディタが表示されている場合のみ)
	//! @return エディタが非表示の場合falseを返します
	virtual bool SetFocus() = 0;
};