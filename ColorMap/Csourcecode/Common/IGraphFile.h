/*!	@file
	@date	2008-09-05
	@author	wada
*/
#pragma once

#include <boost/shared_ptr.hpp>

class IPlot;
class IFilter;

class IGraphFile
{
public:
	virtual IPlot* AddPlot(unsigned long type, unsigned long xItem, unsigned long yItem, boost::shared_ptr<IFilter> const& filter) = 0;
	virtual IPlot* Add3DPlot(unsigned long type, unsigned long xItem, unsigned long yItem, unsigned long zItem, boost::shared_ptr<IFilter> const& filter) = 0;
	virtual IPlot* Add4DPlot(unsigned long type, unsigned long xItem, unsigned long yItem, unsigned long zItem, unsigned long tItem,
		boost::shared_ptr<IFilter> const& filter) = 0;

	virtual bool IsEmpty() const = 0;
	virtual long GetFileId() const = 0;
	virtual void SetFileId(long id) = 0;

	virtual TCHAR const* GetImportSettingFile() const = 0;
	virtual int GetFileType() const = 0;
};