/*!	@file
	@date	2007-12-11
	@author	Wada

	@brief プラグインがエクスポートする関数の定義
	プラグインのみが使用する
*/
#pragma once

#ifndef SHAPE_TYPE
	#error SHAPE_TYPEが定義されていません
#endif
#ifndef SHAPE_ID
	#error SHAPE_IDが定義されていません
#endif
#ifndef SHAPE_NAME
	#error SHAPE_NAMEが定義されていません
#endif

#include "resource.h"
#include "../../../Common/Property.h"
#include "Message.h"

//! DLLのインスタンスハンドル
//! DllEntryPointの引数を必ずこの変数にセットしてください
static HMODULE g_hInstance;

// defined export functions
#define DLLEXPORT __declspec(dllexport)
extern "C"
{
	//! プラグインのインスタンスを作成します
	DLLEXPORT IShape* WINAPI CreateInstance(HWND wnd, IMessageReceiver* owner)
	{
#if defined(_MFC_VER)
		AFX_MANAGE_STATE(AfxGetStaticModuleState());
#endif
		return new SHAPE_TYPE(wnd, owner);
	}

	//! プラグインを解放します
	DLLEXPORT void WINAPI ReleaseInstance(IShape* ptr)
	{
#if defined(_MFC_VER)
		AFX_MANAGE_STATE(AfxGetStaticModuleState());
#endif
		ptr->Destroy();
	}

	//! プラグインの固有IDを取得します
	DLLEXPORT long WINAPI GetId()
	{
#if defined(_MFC_VER)
		AFX_MANAGE_STATE(AfxGetStaticModuleState());
#endif
		return SHAPE_ID;
	}

	//! プラグインの名称を取得します
	DLLEXPORT wchar_t const* WINAPI GetName(IMessageReceiver* owner)
	{
#if defined(_MFC_VER)
		AFX_MANAGE_STATE(AfxGetStaticModuleState());
#endif
		char buf[100] = {};
		if (::LoadStringA(g_hInstance, 100, buf, _countof(buf)) != 0) {
			return GetResourceString(owner, buf);
		}
		return GetResourceString(owner, SHAPE_NAME); //!@todo delete
	}

	HGLOBAL hResourceBuffer;

	IStream* WINAPI GetResourceStream(LPCTSTR pResourceName)
	{
		HRSRC hResource = FindResource(g_hInstance, pResourceName, _T("PNG"));

		if(!hResource)
		{
			return NULL;
		}
	    
		DWORD dwResourceSize = SizeofResource(g_hInstance, hResource);
	    
		if(!dwResourceSize)
		{
			return NULL;
		}
	    
		const void* pResourceData = LockResource(LoadResource(g_hInstance, hResource));
	    
		if(!pResourceData)
		{
			return NULL;
		}
	    
		hResourceBuffer = GlobalAlloc(GMEM_MOVEABLE, dwResourceSize);
	    
		if(!hResourceBuffer)
		{
			GlobalFree(hResourceBuffer);
			return NULL;
		}
	    
		void* pResourceBuffer = GlobalLock(hResourceBuffer);
	    
		if(!pResourceBuffer)
		{
			GlobalUnlock(hResourceBuffer);
			GlobalFree(hResourceBuffer);
			return NULL;
		}

		CopyMemory(pResourceBuffer, pResourceData, dwResourceSize);
		IStream* pIStream = NULL;

		if(CreateStreamOnHGlobal(hResourceBuffer, FALSE, &pIStream)==S_OK)
		{
			return pIStream;
		}
		return NULL;
	}

	//! ツールバーに表示されるオブジェクトのアイコンイメージを取得します
	DLLEXPORT IStream* WINAPI GetIconStream()
	{
		return GetResourceStream(MAKEINTRESOURCE(IDB_ICON));
	}

	DLLEXPORT IStream* WINAPI GetActiveIconStream()
	{
		return GetResourceStream(MAKEINTRESOURCE(IDB_ACTIVE_ICON));
	}

	DLLEXPORT IStream* WINAPI GetGrayIconStream()
	{
		return GetResourceStream(MAKEINTRESOURCE(IDB_GRAY_ICON));
	}

	DLLEXPORT void WINAPI ReleaseImageStream(IStream* stream)
	{
		if (stream) {
			stream->Release();
		}
		GlobalUnlock(hResourceBuffer);
		GlobalFree(hResourceBuffer);
	}
}


//////////////////////////////////////////////////////
#if 0
/*
    Draw
    DrawSelected
    HitTest
    BeginDrag
    Drag
    EndDrag
    GetCursorResource
    SetSelected
    GetSelected
    CreateProperty
    ReleaseProperty
    UpdateProperty
*/
	DLLEXPORT void WINAPI Draw(void* ptr, Gdiplus::Graphics* g)
	{
		OCRectangle* rect = reinterpret_cast<OCRectangle*>(ptr);
		rect->Draw(g);
	}

	DLLEXPORT void WINAPI DrawSelected(void* ptr, Gdiplus::Graphics* g)
	{
		OCRectangle* rect = reinterpret_cast<OCRectangle*>(ptr);
		rect->DrawSelected(g);
	}

	DLLEXPORT int WINAPI HitTest(void* ptr, double x, double y)
	{
		OCRectangle* rect = reinterpret_cast<OCRectangle*>(ptr);
		return rect->HitTest(x, y);
	}

	DLLEXPORT void WINAPI BeginDrag(void* ptr, int type, double x, double y)
	{
		OCRectangle* rect = reinterpret_cast<OCRectangle*>(ptr);
		rect->BeginDrag(type, x, y);
	}

	DLLEXPORT void WINAPI Drag(void* ptr, double x, double y)
	{
		OCRectangle* rect = reinterpret_cast<OCRectangle*>(ptr);
		rect->Drag(x, y);
	}

	DLLEXPORT void WINAPI EndDrag(void* ptr, double x, double y)
	{
		OCRectangle* rect = reinterpret_cast<OCRectangle*>(ptr);
		rect->EndDrag(x, y);
	}

	DLLEXPORT LPTSTR WINAPI GetCursorResource(void* ptr, double x, double y)
	{
		OCRectangle* rect = reinterpret_cast<OCRectangle*>(ptr);
		return rect->GetCursor(x, y);
	}

	DLLEXPORT void WINAPI SetSelected(void* ptr, BOOL select)
	{
		OCRectangle* rect = reinterpret_cast<OCRectangle*>(ptr);
		return rect->SetSelected(!!select);
	}

	DLLEXPORT BOOL WINAPI GetSelected(void* ptr)
	{
		OCRectangle* rect = reinterpret_cast<OCRectangle*>(ptr);
		return rect->GetSelected();
	}

	DLLEXPORT PropertyNode* WINAPI CreateProperty(void* ptr)
	{
		OCRectangle* rect = reinterpret_cast<OCRectangle*>(ptr);
		return rect->CreateProperty();
	}

	DLLEXPORT void WINAPI ReleaseProperty(void* ptr, PropertyNode* prop)
	{
		OCRectangle* rect = reinterpret_cast<OCRectangle*>(ptr);
		rect->ReleaseProperty(prop);
	}

	DLLEXPORT void WINAPI UpdateProperty(void* ptr, PropertyNode* prop)
	{
		OCRectangle* rect = reinterpret_cast<OCRectangle*>(ptr);
		rect->UpdateProperty(prop);
	}
#endif